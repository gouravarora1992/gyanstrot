// Generated by data binding compiler. Do not edit!
package com.scholar.engineering.banking.ssc.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.scholar.engineering.banking.ssc.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ActivityStudentAttendanceBinding extends ViewDataBinding {
  @NonNull
  public final ConstraintLayout constraintLayout6;

  @NonNull
  public final ImageView imgBack;

  @NonNull
  public final RecyclerView recylerAttendance;

  @NonNull
  public final Toolbar toolbar18;

  @NonNull
  public final TextView txtFromDate;

  @NonNull
  public final TextView txtTitle;

  @NonNull
  public final TextView txtToDate;

  protected ActivityStudentAttendanceBinding(Object _bindingComponent, View _root,
      int _localFieldCount, ConstraintLayout constraintLayout6, ImageView imgBack,
      RecyclerView recylerAttendance, Toolbar toolbar18, TextView txtFromDate, TextView txtTitle,
      TextView txtToDate) {
    super(_bindingComponent, _root, _localFieldCount);
    this.constraintLayout6 = constraintLayout6;
    this.imgBack = imgBack;
    this.recylerAttendance = recylerAttendance;
    this.toolbar18 = toolbar18;
    this.txtFromDate = txtFromDate;
    this.txtTitle = txtTitle;
    this.txtToDate = txtToDate;
  }

  @NonNull
  public static ActivityStudentAttendanceBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_student_attendance, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ActivityStudentAttendanceBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ActivityStudentAttendanceBinding>inflateInternal(inflater, R.layout.activity_student_attendance, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityStudentAttendanceBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_student_attendance, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ActivityStudentAttendanceBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ActivityStudentAttendanceBinding>inflateInternal(inflater, R.layout.activity_student_attendance, null, false, component);
  }

  public static ActivityStudentAttendanceBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ActivityStudentAttendanceBinding bind(@NonNull View view,
      @Nullable Object component) {
    return (ActivityStudentAttendanceBinding)bind(component, view, R.layout.activity_student_attendance);
  }
}
