// Generated by data binding compiler. Do not edit!
package com.scholar.engineering.banking.ssc.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.scholar.engineering.banking.ssc.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ActivityViewRequestBinding extends ViewDataBinding {
  @NonNull
  public final ImageView imgBack;

  @NonNull
  public final Toolbar toolbar18;

  @NonNull
  public final TextView txtTitle;

  @NonNull
  public final RecyclerView viewRequestRecyler;

  protected ActivityViewRequestBinding(Object _bindingComponent, View _root, int _localFieldCount,
      ImageView imgBack, Toolbar toolbar18, TextView txtTitle, RecyclerView viewRequestRecyler) {
    super(_bindingComponent, _root, _localFieldCount);
    this.imgBack = imgBack;
    this.toolbar18 = toolbar18;
    this.txtTitle = txtTitle;
    this.viewRequestRecyler = viewRequestRecyler;
  }

  @NonNull
  public static ActivityViewRequestBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_view_request, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ActivityViewRequestBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ActivityViewRequestBinding>inflateInternal(inflater, R.layout.activity_view_request, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityViewRequestBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_view_request, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ActivityViewRequestBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ActivityViewRequestBinding>inflateInternal(inflater, R.layout.activity_view_request, null, false, component);
  }

  public static ActivityViewRequestBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ActivityViewRequestBinding bind(@NonNull View view, @Nullable Object component) {
    return (ActivityViewRequestBinding)bind(component, view, R.layout.activity_view_request);
  }
}
