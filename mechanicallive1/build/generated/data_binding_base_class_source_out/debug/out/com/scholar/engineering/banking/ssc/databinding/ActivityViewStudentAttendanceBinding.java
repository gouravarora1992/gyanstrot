// Generated by data binding compiler. Do not edit!
package com.scholar.engineering.banking.ssc.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.scholar.engineering.banking.ssc.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ActivityViewStudentAttendanceBinding extends ViewDataBinding {
  @NonNull
  public final ConstraintLayout constraintLayout5;

  @NonNull
  public final ImageView imgBack;

  @NonNull
  public final Spinner spinnerSelectClass;

  @NonNull
  public final Spinner spinnerSelectSection;

  @NonNull
  public final RecyclerView studentRecyler;

  @NonNull
  public final TextView textView66;

  @NonNull
  public final Toolbar toolbar18;

  @NonNull
  public final TextView txtDate;

  @NonNull
  public final TextView txtTitle;

  protected ActivityViewStudentAttendanceBinding(Object _bindingComponent, View _root,
      int _localFieldCount, ConstraintLayout constraintLayout5, ImageView imgBack,
      Spinner spinnerSelectClass, Spinner spinnerSelectSection, RecyclerView studentRecyler,
      TextView textView66, Toolbar toolbar18, TextView txtDate, TextView txtTitle) {
    super(_bindingComponent, _root, _localFieldCount);
    this.constraintLayout5 = constraintLayout5;
    this.imgBack = imgBack;
    this.spinnerSelectClass = spinnerSelectClass;
    this.spinnerSelectSection = spinnerSelectSection;
    this.studentRecyler = studentRecyler;
    this.textView66 = textView66;
    this.toolbar18 = toolbar18;
    this.txtDate = txtDate;
    this.txtTitle = txtTitle;
  }

  @NonNull
  public static ActivityViewStudentAttendanceBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_view_student_attendance, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ActivityViewStudentAttendanceBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ActivityViewStudentAttendanceBinding>inflateInternal(inflater, R.layout.activity_view_student_attendance, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityViewStudentAttendanceBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_view_student_attendance, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ActivityViewStudentAttendanceBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ActivityViewStudentAttendanceBinding>inflateInternal(inflater, R.layout.activity_view_student_attendance, null, false, component);
  }

  public static ActivityViewStudentAttendanceBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ActivityViewStudentAttendanceBinding bind(@NonNull View view,
      @Nullable Object component) {
    return (ActivityViewStudentAttendanceBinding)bind(component, view, R.layout.activity_view_student_attendance);
  }
}
