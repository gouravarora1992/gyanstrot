// Generated by data binding compiler. Do not edit!
package com.scholar.engineering.banking.ssc.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.scholar.engineering.banking.ssc.R;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ActivityGroupChatBinding extends ViewDataBinding {
  @NonNull
  public final RecyclerView chatRecycler;

  @NonNull
  public final ConstraintLayout constraintLayout4;

  @NonNull
  public final ConstraintLayout constraintLayout7;

  @NonNull
  public final CircleImageView imageView27;

  @NonNull
  public final ImageView imageView28;

  @NonNull
  public final ImageView imgAttach;

  @NonNull
  public final ImageView imgBack;

  @NonNull
  public final ImageView imgMoreInfo;

  @NonNull
  public final EditText textView69;

  @NonNull
  public final Toolbar toolbar13;

  @NonNull
  public final TextView txtName;

  protected ActivityGroupChatBinding(Object _bindingComponent, View _root, int _localFieldCount,
      RecyclerView chatRecycler, ConstraintLayout constraintLayout4,
      ConstraintLayout constraintLayout7, CircleImageView imageView27, ImageView imageView28,
      ImageView imgAttach, ImageView imgBack, ImageView imgMoreInfo, EditText textView69,
      Toolbar toolbar13, TextView txtName) {
    super(_bindingComponent, _root, _localFieldCount);
    this.chatRecycler = chatRecycler;
    this.constraintLayout4 = constraintLayout4;
    this.constraintLayout7 = constraintLayout7;
    this.imageView27 = imageView27;
    this.imageView28 = imageView28;
    this.imgAttach = imgAttach;
    this.imgBack = imgBack;
    this.imgMoreInfo = imgMoreInfo;
    this.textView69 = textView69;
    this.toolbar13 = toolbar13;
    this.txtName = txtName;
  }

  @NonNull
  public static ActivityGroupChatBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_group_chat, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ActivityGroupChatBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ActivityGroupChatBinding>inflateInternal(inflater, R.layout.activity_group_chat, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityGroupChatBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_group_chat, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ActivityGroupChatBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ActivityGroupChatBinding>inflateInternal(inflater, R.layout.activity_group_chat, null, false, component);
  }

  public static ActivityGroupChatBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ActivityGroupChatBinding bind(@NonNull View view, @Nullable Object component) {
    return (ActivityGroupChatBinding)bind(component, view, R.layout.activity_group_chat);
  }
}
