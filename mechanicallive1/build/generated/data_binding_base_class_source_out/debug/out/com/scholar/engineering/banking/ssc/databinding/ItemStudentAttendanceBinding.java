// Generated by data binding compiler. Do not edit!
package com.scholar.engineering.banking.ssc.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.scholar.engineering.banking.ssc.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ItemStudentAttendanceBinding extends ViewDataBinding {
  @NonNull
  public final ImageView imgNitification;

  @NonNull
  public final TextView txtName;

  protected ItemStudentAttendanceBinding(Object _bindingComponent, View _root, int _localFieldCount,
      ImageView imgNitification, TextView txtName) {
    super(_bindingComponent, _root, _localFieldCount);
    this.imgNitification = imgNitification;
    this.txtName = txtName;
  }

  @NonNull
  public static ItemStudentAttendanceBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.item_student_attendance, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ItemStudentAttendanceBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ItemStudentAttendanceBinding>inflateInternal(inflater, R.layout.item_student_attendance, root, attachToRoot, component);
  }

  @NonNull
  public static ItemStudentAttendanceBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.item_student_attendance, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ItemStudentAttendanceBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ItemStudentAttendanceBinding>inflateInternal(inflater, R.layout.item_student_attendance, null, false, component);
  }

  public static ItemStudentAttendanceBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ItemStudentAttendanceBinding bind(@NonNull View view, @Nullable Object component) {
    return (ItemStudentAttendanceBinding)bind(component, view, R.layout.item_student_attendance);
  }
}
