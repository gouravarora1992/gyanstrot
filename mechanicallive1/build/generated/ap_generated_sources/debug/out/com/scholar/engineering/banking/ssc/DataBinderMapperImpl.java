package com.scholar.engineering.banking.ssc;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.scholar.engineering.banking.ssc.databinding.ActivityAddItemQueryBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ActivityApplyLeavesBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ActivityAttendanceBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ActivityChatBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ActivityGroupChatBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ActivityHomeStaffBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ActivityIssueSubmissonBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ActivityLeaveApplicationBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ActivityLeaveDetailBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ActivityPickUpRequestBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ActivitySearchUserBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ActivityStaffProfileBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ActivityStaffStudentAttendanceBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ActivityStudentAttendanceBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ActivitySupportSystemBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ActivityViewQueryBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ActivityViewRequestBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ActivityViewStudentAttendanceBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ChatRecyclerItemBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.FragmentStaffListBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.FragmentStudentListBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ItemLeaveRequestBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ItemStudentAttendanceBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ItemViewAttendanceBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.ItemViewRequestBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.StudentAttendanceRecyclerItemBindingImpl;
import com.scholar.engineering.banking.ssc.databinding.UserListRecylerBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYADDITEMQUERY = 1;

  private static final int LAYOUT_ACTIVITYAPPLYLEAVES = 2;

  private static final int LAYOUT_ACTIVITYATTENDANCE = 3;

  private static final int LAYOUT_ACTIVITYCHAT = 4;

  private static final int LAYOUT_ACTIVITYGROUPCHAT = 5;

  private static final int LAYOUT_ACTIVITYHOMESTAFF = 6;

  private static final int LAYOUT_ACTIVITYISSUESUBMISSON = 7;

  private static final int LAYOUT_ACTIVITYLEAVEAPPLICATION = 8;

  private static final int LAYOUT_ACTIVITYLEAVEDETAIL = 9;

  private static final int LAYOUT_ACTIVITYPICKUPREQUEST = 10;

  private static final int LAYOUT_ACTIVITYSEARCHUSER = 11;

  private static final int LAYOUT_ACTIVITYSTAFFPROFILE = 12;

  private static final int LAYOUT_ACTIVITYSTAFFSTUDENTATTENDANCE = 13;

  private static final int LAYOUT_ACTIVITYSTUDENTATTENDANCE = 14;

  private static final int LAYOUT_ACTIVITYSUPPORTSYSTEM = 15;

  private static final int LAYOUT_ACTIVITYVIEWQUERY = 16;

  private static final int LAYOUT_ACTIVITYVIEWREQUEST = 17;

  private static final int LAYOUT_ACTIVITYVIEWSTUDENTATTENDANCE = 18;

  private static final int LAYOUT_CHATRECYCLERITEM = 19;

  private static final int LAYOUT_FRAGMENTSTAFFLIST = 20;

  private static final int LAYOUT_FRAGMENTSTUDENTLIST = 21;

  private static final int LAYOUT_ITEMLEAVEREQUEST = 22;

  private static final int LAYOUT_ITEMSTUDENTATTENDANCE = 23;

  private static final int LAYOUT_ITEMVIEWATTENDANCE = 24;

  private static final int LAYOUT_ITEMVIEWREQUEST = 25;

  private static final int LAYOUT_STUDENTATTENDANCERECYCLERITEM = 26;

  private static final int LAYOUT_USERLISTRECYLER = 27;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(27);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.activity_add_item_query, LAYOUT_ACTIVITYADDITEMQUERY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.activity_apply_leaves, LAYOUT_ACTIVITYAPPLYLEAVES);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.activity_attendance, LAYOUT_ACTIVITYATTENDANCE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.activity_chat, LAYOUT_ACTIVITYCHAT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.activity_group_chat, LAYOUT_ACTIVITYGROUPCHAT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.activity_home_staff, LAYOUT_ACTIVITYHOMESTAFF);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.activity_issue_submisson, LAYOUT_ACTIVITYISSUESUBMISSON);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.activity_leave_application, LAYOUT_ACTIVITYLEAVEAPPLICATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.activity_leave_detail, LAYOUT_ACTIVITYLEAVEDETAIL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.activity_pick_up_request, LAYOUT_ACTIVITYPICKUPREQUEST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.activity_search_user, LAYOUT_ACTIVITYSEARCHUSER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.activity_staff_profile, LAYOUT_ACTIVITYSTAFFPROFILE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.activity_staff_student_attendance, LAYOUT_ACTIVITYSTAFFSTUDENTATTENDANCE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.activity_student_attendance, LAYOUT_ACTIVITYSTUDENTATTENDANCE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.activity_support_system, LAYOUT_ACTIVITYSUPPORTSYSTEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.activity_view_query, LAYOUT_ACTIVITYVIEWQUERY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.activity_view_request, LAYOUT_ACTIVITYVIEWREQUEST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.activity_view_student_attendance, LAYOUT_ACTIVITYVIEWSTUDENTATTENDANCE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.chat_recycler_item, LAYOUT_CHATRECYCLERITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.fragment_staff_list, LAYOUT_FRAGMENTSTAFFLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.fragment_student_list, LAYOUT_FRAGMENTSTUDENTLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.item_leave_request, LAYOUT_ITEMLEAVEREQUEST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.item_student_attendance, LAYOUT_ITEMSTUDENTATTENDANCE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.item_view_attendance, LAYOUT_ITEMVIEWATTENDANCE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.item_view_request, LAYOUT_ITEMVIEWREQUEST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.student_attendance_recycler_item, LAYOUT_STUDENTATTENDANCERECYCLERITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.scholar.engineering.banking.ssc.R.layout.user_list_recyler, LAYOUT_USERLISTRECYLER);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYADDITEMQUERY: {
          if ("layout/activity_add_item_query_0".equals(tag)) {
            return new ActivityAddItemQueryBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_add_item_query is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYAPPLYLEAVES: {
          if ("layout/activity_apply_leaves_0".equals(tag)) {
            return new ActivityApplyLeavesBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_apply_leaves is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYATTENDANCE: {
          if ("layout/activity_attendance_0".equals(tag)) {
            return new ActivityAttendanceBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_attendance is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYCHAT: {
          if ("layout/activity_chat_0".equals(tag)) {
            return new ActivityChatBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_chat is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYGROUPCHAT: {
          if ("layout/activity_group_chat_0".equals(tag)) {
            return new ActivityGroupChatBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_group_chat is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYHOMESTAFF: {
          if ("layout/activity_home_staff_0".equals(tag)) {
            return new ActivityHomeStaffBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_home_staff is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYISSUESUBMISSON: {
          if ("layout/activity_issue_submisson_0".equals(tag)) {
            return new ActivityIssueSubmissonBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_issue_submisson is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYLEAVEAPPLICATION: {
          if ("layout/activity_leave_application_0".equals(tag)) {
            return new ActivityLeaveApplicationBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_leave_application is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYLEAVEDETAIL: {
          if ("layout/activity_leave_detail_0".equals(tag)) {
            return new ActivityLeaveDetailBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_leave_detail is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYPICKUPREQUEST: {
          if ("layout/activity_pick_up_request_0".equals(tag)) {
            return new ActivityPickUpRequestBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_pick_up_request is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYSEARCHUSER: {
          if ("layout/activity_search_user_0".equals(tag)) {
            return new ActivitySearchUserBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_search_user is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYSTAFFPROFILE: {
          if ("layout/activity_staff_profile_0".equals(tag)) {
            return new ActivityStaffProfileBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_staff_profile is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYSTAFFSTUDENTATTENDANCE: {
          if ("layout/activity_staff_student_attendance_0".equals(tag)) {
            return new ActivityStaffStudentAttendanceBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_staff_student_attendance is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYSTUDENTATTENDANCE: {
          if ("layout/activity_student_attendance_0".equals(tag)) {
            return new ActivityStudentAttendanceBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_student_attendance is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYSUPPORTSYSTEM: {
          if ("layout/activity_support_system_0".equals(tag)) {
            return new ActivitySupportSystemBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_support_system is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYVIEWQUERY: {
          if ("layout/activity_view_query_0".equals(tag)) {
            return new ActivityViewQueryBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_view_query is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYVIEWREQUEST: {
          if ("layout/activity_view_request_0".equals(tag)) {
            return new ActivityViewRequestBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_view_request is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYVIEWSTUDENTATTENDANCE: {
          if ("layout/activity_view_student_attendance_0".equals(tag)) {
            return new ActivityViewStudentAttendanceBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_view_student_attendance is invalid. Received: " + tag);
        }
        case  LAYOUT_CHATRECYCLERITEM: {
          if ("layout/chat_recycler_item_0".equals(tag)) {
            return new ChatRecyclerItemBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for chat_recycler_item is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSTAFFLIST: {
          if ("layout/fragment_staff_list_0".equals(tag)) {
            return new FragmentStaffListBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_staff_list is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSTUDENTLIST: {
          if ("layout/fragment_student_list_0".equals(tag)) {
            return new FragmentStudentListBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_student_list is invalid. Received: " + tag);
        }
        case  LAYOUT_ITEMLEAVEREQUEST: {
          if ("layout/item_leave_request_0".equals(tag)) {
            return new ItemLeaveRequestBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for item_leave_request is invalid. Received: " + tag);
        }
        case  LAYOUT_ITEMSTUDENTATTENDANCE: {
          if ("layout/item_student_attendance_0".equals(tag)) {
            return new ItemStudentAttendanceBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for item_student_attendance is invalid. Received: " + tag);
        }
        case  LAYOUT_ITEMVIEWATTENDANCE: {
          if ("layout/item_view_attendance_0".equals(tag)) {
            return new ItemViewAttendanceBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for item_view_attendance is invalid. Received: " + tag);
        }
        case  LAYOUT_ITEMVIEWREQUEST: {
          if ("layout/item_view_request_0".equals(tag)) {
            return new ItemViewRequestBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for item_view_request is invalid. Received: " + tag);
        }
        case  LAYOUT_STUDENTATTENDANCERECYCLERITEM: {
          if ("layout/student_attendance_recycler_item_0".equals(tag)) {
            return new StudentAttendanceRecyclerItemBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for student_attendance_recycler_item is invalid. Received: " + tag);
        }
        case  LAYOUT_USERLISTRECYLER: {
          if ("layout/user_list_recyler_0".equals(tag)) {
            return new UserListRecylerBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for user_list_recyler is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(1);

    static {
      sKeys.put(0, "_all");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(27);

    static {
      sKeys.put("layout/activity_add_item_query_0", com.scholar.engineering.banking.ssc.R.layout.activity_add_item_query);
      sKeys.put("layout/activity_apply_leaves_0", com.scholar.engineering.banking.ssc.R.layout.activity_apply_leaves);
      sKeys.put("layout/activity_attendance_0", com.scholar.engineering.banking.ssc.R.layout.activity_attendance);
      sKeys.put("layout/activity_chat_0", com.scholar.engineering.banking.ssc.R.layout.activity_chat);
      sKeys.put("layout/activity_group_chat_0", com.scholar.engineering.banking.ssc.R.layout.activity_group_chat);
      sKeys.put("layout/activity_home_staff_0", com.scholar.engineering.banking.ssc.R.layout.activity_home_staff);
      sKeys.put("layout/activity_issue_submisson_0", com.scholar.engineering.banking.ssc.R.layout.activity_issue_submisson);
      sKeys.put("layout/activity_leave_application_0", com.scholar.engineering.banking.ssc.R.layout.activity_leave_application);
      sKeys.put("layout/activity_leave_detail_0", com.scholar.engineering.banking.ssc.R.layout.activity_leave_detail);
      sKeys.put("layout/activity_pick_up_request_0", com.scholar.engineering.banking.ssc.R.layout.activity_pick_up_request);
      sKeys.put("layout/activity_search_user_0", com.scholar.engineering.banking.ssc.R.layout.activity_search_user);
      sKeys.put("layout/activity_staff_profile_0", com.scholar.engineering.banking.ssc.R.layout.activity_staff_profile);
      sKeys.put("layout/activity_staff_student_attendance_0", com.scholar.engineering.banking.ssc.R.layout.activity_staff_student_attendance);
      sKeys.put("layout/activity_student_attendance_0", com.scholar.engineering.banking.ssc.R.layout.activity_student_attendance);
      sKeys.put("layout/activity_support_system_0", com.scholar.engineering.banking.ssc.R.layout.activity_support_system);
      sKeys.put("layout/activity_view_query_0", com.scholar.engineering.banking.ssc.R.layout.activity_view_query);
      sKeys.put("layout/activity_view_request_0", com.scholar.engineering.banking.ssc.R.layout.activity_view_request);
      sKeys.put("layout/activity_view_student_attendance_0", com.scholar.engineering.banking.ssc.R.layout.activity_view_student_attendance);
      sKeys.put("layout/chat_recycler_item_0", com.scholar.engineering.banking.ssc.R.layout.chat_recycler_item);
      sKeys.put("layout/fragment_staff_list_0", com.scholar.engineering.banking.ssc.R.layout.fragment_staff_list);
      sKeys.put("layout/fragment_student_list_0", com.scholar.engineering.banking.ssc.R.layout.fragment_student_list);
      sKeys.put("layout/item_leave_request_0", com.scholar.engineering.banking.ssc.R.layout.item_leave_request);
      sKeys.put("layout/item_student_attendance_0", com.scholar.engineering.banking.ssc.R.layout.item_student_attendance);
      sKeys.put("layout/item_view_attendance_0", com.scholar.engineering.banking.ssc.R.layout.item_view_attendance);
      sKeys.put("layout/item_view_request_0", com.scholar.engineering.banking.ssc.R.layout.item_view_request);
      sKeys.put("layout/student_attendance_recycler_item_0", com.scholar.engineering.banking.ssc.R.layout.student_attendance_recycler_item);
      sKeys.put("layout/user_list_recyler_0", com.scholar.engineering.banking.ssc.R.layout.user_list_recyler);
    }
  }
}
