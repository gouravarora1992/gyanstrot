package com.scholar.engineering.banking.ssc.databinding;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityStaffProfileBindingImpl extends ActivityStaffProfileBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.background, 1);
        sViewsWithIds.put(R.id.appBarLayout, 2);
        sViewsWithIds.put(R.id.toolbar, 3);
        sViewsWithIds.put(R.id.img_back, 4);
        sViewsWithIds.put(R.id.headertextid, 5);
        sViewsWithIds.put(R.id.switchprofile, 6);
        sViewsWithIds.put(R.id.iv_profile, 7);
        sViewsWithIds.put(R.id.personldetail, 8);
        sViewsWithIds.put(R.id.tvname, 9);
        sViewsWithIds.put(R.id.et_username, 10);
        sViewsWithIds.put(R.id.tv_email, 11);
        sViewsWithIds.put(R.id.et_email, 12);
        sViewsWithIds.put(R.id.tv_role, 13);
        sViewsWithIds.put(R.id.et_role, 14);
        sViewsWithIds.put(R.id.tv_location, 15);
        sViewsWithIds.put(R.id.switchid, 16);
        sViewsWithIds.put(R.id.tv_collegestudent, 17);
        sViewsWithIds.put(R.id.switch_collegestudent, 18);
        sViewsWithIds.put(R.id.lay_college, 19);
        sViewsWithIds.put(R.id.sp_department, 20);
        sViewsWithIds.put(R.id.sp_college, 21);
        sViewsWithIds.put(R.id.sp_stream, 22);
        sViewsWithIds.put(R.id.sp_year, 23);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityStaffProfileBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 24, sIncludes, sViewsWithIds));
    }
    private ActivityStaffProfileBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.appbar.AppBarLayout) bindings[2]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[1]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[5]
            , (android.widget.ImageView) bindings[4]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[7]
            , (android.widget.LinearLayout) bindings[19]
            , (android.widget.TextView) bindings[8]
            , (android.widget.Spinner) bindings[21]
            , (android.widget.Spinner) bindings[20]
            , (android.widget.Spinner) bindings[22]
            , (android.widget.Spinner) bindings[23]
            , (android.widget.Switch) bindings[18]
            , (android.widget.Switch) bindings[16]
            , (android.widget.TextView) bindings[6]
            , (androidx.appcompat.widget.Toolbar) bindings[3]
            , (android.widget.TextView) bindings[17]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[9]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}