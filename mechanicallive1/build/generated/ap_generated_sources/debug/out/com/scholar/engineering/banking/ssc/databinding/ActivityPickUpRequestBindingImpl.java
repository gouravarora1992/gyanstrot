package com.scholar.engineering.banking.ssc.databinding;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityPickUpRequestBindingImpl extends ActivityPickUpRequestBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.toolbar18, 1);
        sViewsWithIds.put(R.id.img_back, 2);
        sViewsWithIds.put(R.id.txt_title, 3);
        sViewsWithIds.put(R.id.scrollView2, 4);
        sViewsWithIds.put(R.id.textView29, 5);
        sViewsWithIds.put(R.id.textView30, 6);
        sViewsWithIds.put(R.id.spinner_admissionno, 7);
        sViewsWithIds.put(R.id.textView32, 8);
        sViewsWithIds.put(R.id.et_name, 9);
        sViewsWithIds.put(R.id.textView34, 10);
        sViewsWithIds.put(R.id.et_class, 11);
        sViewsWithIds.put(R.id.et_section, 12);
        sViewsWithIds.put(R.id.textView44, 13);
        sViewsWithIds.put(R.id.et_reason, 14);
        sViewsWithIds.put(R.id.textView45, 15);
        sViewsWithIds.put(R.id.constraintLayout2, 16);
        sViewsWithIds.put(R.id.button4, 17);
        sViewsWithIds.put(R.id.textView62, 18);
        sViewsWithIds.put(R.id.btn_submit, 19);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityPickUpRequestBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 20, sIncludes, sViewsWithIds));
    }
    private ActivityPickUpRequestBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.Button) bindings[19]
            , (android.widget.Button) bindings[17]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[16]
            , (android.widget.EditText) bindings[11]
            , (android.widget.EditText) bindings[9]
            , (android.widget.EditText) bindings[14]
            , (android.widget.EditText) bindings[12]
            , (android.widget.ImageView) bindings[2]
            , (android.widget.ScrollView) bindings[4]
            , (android.widget.Spinner) bindings[7]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[18]
            , (androidx.appcompat.widget.Toolbar) bindings[1]
            , (android.widget.TextView) bindings[3]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}