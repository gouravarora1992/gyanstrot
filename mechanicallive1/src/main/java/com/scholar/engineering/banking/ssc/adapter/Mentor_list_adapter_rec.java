package com.scholar.engineering.banking.ssc.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.Mentors_Champions.Mentor_common;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.isNull;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

/**
 * Created by surender on 3/14/2017.
*/

public class Mentor_list_adapter_rec extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    SharedPreferences pref,prefsignup;
    Context cs;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    ImageLoader imageLoader;
    JSONArray data;
    JSONObject object;
    String clas;

    public Mentor_list_adapter_rec(Context cs, JSONArray data,String clas) {
        this.cs=cs;
        this.data=data;
        this.clas=clas;

        prefsignup=cs.getSharedPreferences("myref",Context.MODE_PRIVATE);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(cs));

        options = new DisplayImageOptions.Builder()
                .displayer(new RoundedBitmapDisplayer(6))
                .showImageOnLoading(R.drawable.blankimage)
                .showImageForEmptyUri(R.drawable.congrrr)
                .showImageOnFail(R.drawable.congrrr)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

                View v0 = inflater.inflate(R.layout.mentor_itme_rec, viewGroup, false);

                viewHolder = new ViewHolder0(v0);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

                ViewHolder0 vh0 = (ViewHolder0) viewHolder;

                setData(vh0, position);
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    public void setData(ViewHolder0 v, final int p){

        try {

            object=data.getJSONObject(p);
            v.tv_name.setText(object.getString("name"));
            v.tv_follower.setText("Followers "+object.getString("followers"));
            v.tv_like.setText("Likes "+object.getString("likes"));

            if(isNull(object.getString("qualification")))
                v.tv_education.setText(object.getString("qualification"));

            if(isNull(object.getString("city")))
                v.tv_city.setText(object.getString("city"));

            String img="";
            if(object.getString("image").length()>4)
            Glide.with(cs).load(Constants.URL_Image+object.getString("image")).into(v.iv_user);

            Logg("userimage2",object.getString("image")+"");

//
            v.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                        try {

                            if(!clas.equalsIgnoreCase("Mentor_common")) {
                                object = data.getJSONObject(p);
                                Intent in = new Intent(cs, Mentor_common.class);
                                in.putExtra("email", object.getString("uid"));
                                cs.startActivity(in);
                            }
                        } catch (JSONException e) {

                        }
                }
            });

            Typeface font_demi = Typeface.createFromAsset(cs.getAssets(), "avenirnextdemibold.ttf");
            Typeface font_medium = Typeface.createFromAsset(cs.getAssets(), "avenirnextmediumCn.ttf");
            v.tv_name.setTypeface(font_demi);
            v.tv_education.setTypeface(font_medium);
            v.tv_like.setTypeface(font_demi);
            v.tv_follower.setTypeface(font_demi);
            v.tv_city.setTypeface(font_medium);

        } catch (JSONException e) {

        }

    }


    public class ViewHolder0 extends RecyclerView.ViewHolder {

        TextView tv_name,tv_education,tv_follower,tv_like,tv_city;
        CircleImageView iv_user;
        LinearLayout layout;

        public ViewHolder0(View bn) {
            super(bn);

            layout=(LinearLayout)bn.findViewById(R.id.layout);
            tv_name=(TextView)bn.findViewById(R.id.tv_name);
            tv_education=(TextView)bn.findViewById(R.id.tv_education);
            tv_city=(TextView)bn.findViewById(R.id.tv_city);
            tv_follower=(TextView)bn.findViewById(R.id.tv_follower);
            tv_like=(TextView)bn.findViewById(R.id.tv_like);
            iv_user=(CircleImageView)bn.findViewById(R.id.iv_user);
        }
    }
}
