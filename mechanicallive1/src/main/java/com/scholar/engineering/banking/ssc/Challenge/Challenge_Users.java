package com.scholar.engineering.banking.ssc.Challenge;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.scholar.engineering.banking.ssc.HomeActivity;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.EndlessRecyclerViewScrollListener;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Challenge_Users extends AppCompatActivity
{
	RecyclerView list;
	Choose_Opponent_list_Adapter adapter;
	ArrayList<Home_getset> homedata,homedata1;
	ProgressDialog progress;
	String title,image,st_search="";
	SwipeRefreshLayout swipeRefreshLayout;
	int index=1,size=0;
	ImageView iv_search;
	EditText et_search;
	RelativeLayout footerlayout;
	TextView loadmore;
	ProgressWheel wheelbar;
	NetworkConnection nw;


	public static String challenge_sub="";

	Typeface font_demi;
	Typeface font_medium;
	EndlessRecyclerViewScrollListener endless;
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.choose_opponent);

		nw=new NetworkConnection(this);

		font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");
		font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");


		Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setTitle("Select Opponent");
		Utility.applyFontForToolbarTitle(toolbar,this);

		list=(RecyclerView)findViewById(R.id.recyclerView);
		LinearLayoutManager mLayoutManager = new LinearLayoutManager(Challenge_Users.this);
		list.setLayoutManager(mLayoutManager);

		swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh_layout);

		et_search=(EditText)findViewById(R.id.et_search);
		iv_search=(ImageView)findViewById(R.id.iv_search);
		homedata=new ArrayList<>();

		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());

		swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

			@Override
			public void onRefresh() {
				endless.resetState();
				index=1;
				getUsers();
			}
		});

		if(getIntent().hasExtra("subject")){
			challenge_sub=getIntent().getStringExtra("subject");
		}

		getUsers();

		footerlayout=(RelativeLayout)findViewById(R.id.footer_layout);
		loadmore=(TextView)findViewById(R.id.moreButton);
		wheelbar=(ProgressWheel)findViewById(R.id.progress_wheel);

		endless = new EndlessRecyclerViewScrollListener(mLayoutManager) {
			@Override
			public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
				// Triggered only when new data needs to be appended to the list
				// Add whatever code is needed to append new items to the bottom of the list
				Logg("page",page+"");

				wheelbar.setVisibility(View.VISIBLE);

				if(size>9)
					getUsers();

			}
		};

		// Adds the scroll listener to RecyclerView
		list.addOnScrollListener(endless);


		iv_search.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(et_search.getText().toString().length()>0){
					st_search=et_search.getText().toString();
					index=1;
					getUsers();
				}
			}
		});

		et_search.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {


			}

			@Override
			public void afterTextChanged(Editable s) {
				endless.resetState();
				st_search=s.toString();
				index=1;
				Logg("length",s.length()+"");
				if(s.toString().length()>2)
					getUsers();
				else if(s.toString().length()==0){
					getUsers();
				}

			}
		});

	}

	private void getUsers() {

		Logg("pager11",index+"");
		if(!nw.isConnectingToInternet()) {
			internetconnection(0);
			return;
		}

		if(index==1&st_search.length()<1) {
			progress = ProgressDialog.show(Challenge_Users.this, null, null, true);
			progress.setContentView(R.layout.progressdialog);
			progress.setCanceledOnTouchOutside(false);
			progress.setCancelable(false);
			progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			progress.show();
		}

		RequestQueue queue= Volley.newRequestQueue(Challenge_Users.this);
		String url= Constants.URL_LV+"getopponents" ;
		url=url.replace(" ","%20");
		Logg("name",url);

		StringRequest request=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
			@Override
			public void onResponse(String s)
			{
				try {

					JSONObject res=new JSONObject(s);
					Logg("response users",s+"");
					if(res.getJSONObject("users").has("data")) {

						JSONArray jr = res.getJSONObject("users").getJSONArray("data");

						size=jr.length();

						if (index == 1) {

							if(res.has("subject"))
								challenge_sub=res.getString("subject");

							adapter = new Choose_Opponent_list_Adapter(Challenge_Users.this, jr);
							list.setAdapter(adapter);
							index++;
						}
						else{

							if(adapter.getItemCount()>9) {
								adapter.addItem(jr);
								adapter.notifyDataSetChanged();
								index++;
							}

						}
						if(progress!=null)
							if (progress.isShowing())
								progress.dismiss();

						swipeRefreshLayout.setRefreshing(false);
						wheelbar.setVisibility(View.GONE);
					}
					else
						wheelbar.setVisibility(View.GONE);

						if(progress!=null)
						if (progress.isShowing())
							progress.dismiss();

						swipeRefreshLayout.setRefreshing(false);

				}

				catch (JSONException e) {if(progress!=null)
					if (progress.isShowing())
						progress.dismiss();

					wheelbar.setVisibility(View.GONE);
					swipeRefreshLayout.setRefreshing(false);
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {
				CommonUtils.toast(Challenge_Users.this,CommonUtils.volleyerror(volleyError));
				if(progress!=null)
					if (progress.isShowing())
						progress.dismiss();

				wheelbar.setVisibility(View.GONE);
				swipeRefreshLayout.setRefreshing(false);
			}
		})
		{
			@Override
			protected Map<String, String> getParams()
			{
				Map<String, String> params = new HashMap<String, String>();
				params.put("page",index+"");
				params.put("email",Constants.User_Email);
				params.put("name",st_search);
				params.put("admission_no",Constants.addmissionno);
				Logg("getparams",params+"");
				return params;
			}
		};

		request.setShouldCache(false);

		int socketTimeout =0;//30 seconds - change to what you want
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		request.setRetryPolicy(policy);

		queue.add(request);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(item.getItemId()==android.R.id.home)
			finish();

		return super.onOptionsItemSelected(item);
	}


	public void internetconnection(final int i){

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.network_alert);
		dialog.show();

		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
		TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

		tv_tryagain.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
			dialog.dismiss();
			getUsers();
			}
		});

		iv_cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

	}

}
