package com.scholar.engineering.banking.ssc.Staff.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.scholar.engineering.banking.ssc.R;

import org.json.JSONArray;
import org.json.JSONException;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ViewQueryDetailsAdapter extends RecyclerView.Adapter<ViewQueryDetailsAdapter.ViewHolder> {

    private Context context;
    JSONArray itemName,quantity,purpose,purposeImage,cost;

    public ViewQueryDetailsAdapter(Context context, JSONArray purposeImage,JSONArray itemName, JSONArray quantity, JSONArray cost, JSONArray purpose) {
        this.context = context;
        this.purposeImage = purposeImage;
        this.itemName = itemName;
        this.quantity = quantity;
        this.cost = cost;
        this.purpose = purpose;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.view_query_details_recyler_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        try {
            Glide.with(context).load(purposeImage.getString(position)).into(holder.img_purpose);
            holder.txt_name.setText(itemName.getString(position));
            holder.txt_quantity.setText(quantity.getString(position));
            holder.txt_cost.setText(cost.getString(position));
            holder.txt_purpose.setText(purpose.getString(position));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return itemName.length();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_name;
        TextView txt_quantity;
        TextView txt_cost;
        TextView txt_purpose;
        ImageView img_purpose;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.txt_name = (TextView) itemView.findViewById(R.id.item_name);
            this.txt_quantity = (TextView) itemView.findViewById(R.id.txt_quantity);
            this.txt_cost = (TextView) itemView.findViewById(R.id.txt_cost);
            this.txt_purpose = (TextView) itemView.findViewById(R.id.txt_purpose);
            this.img_purpose = itemView.findViewById(R.id.img_purpose);
        }
    }


}
