package com.scholar.engineering.banking.ssc.Challenge;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.scholar.engineering.banking.ssc.HomeActivity;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.EndlessRecyclerViewScrollListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Challenge_frag extends Fragment {

    HomeActivity activity;
    View v;
    FloatingActionButton floatingActionMenu;

    Challenge_frag_adapter adapter;
    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView list;
    int index=1,item_count=0;
    ProgressWheel wheelbar;
    AppCompatTextView tv_startchallenge;
    Typeface font_demi;
    EndlessRecyclerViewScrollListener endless;


    public Challenge_frag() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        v= inflater.inflate(R.layout.challenge_frag, container, false);

        font_demi = Typeface.createFromAsset(activity.getAssets(), "avenirnextdemibold.ttf");

        swipeRefreshLayout=(SwipeRefreshLayout)v.findViewById(R.id.swipe_refresh_layout);
        floatingActionMenu=(FloatingActionButton)v.findViewById(R.id.menu2);
        wheelbar=(ProgressWheel)v.findViewById(R.id.progress_wheel);
        tv_startchallenge=(AppCompatTextView)v.findViewById(R.id.tv_startchallenge);

        tv_startchallenge.setTypeface(font_demi);

        list=(RecyclerView)v.findViewById(R.id.recyclerView);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(activity);
        list.setLayoutManager(mLayoutManager);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                endless.resetState();
                index=1;
                getData();
            }
        });

        endless = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                Logg("page",page+"");


                if(item_count>9) {
                    wheelbar.setVisibility(View.VISIBLE);
                    getData();
                }

            }
        };

        // Adds the scroll listener to RecyclerView

        list.addOnScrollListener(endless);

        tv_startchallenge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            Fragment prev = getFragmentManager().findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);

            getSubject(ft);

            }
        });

        floatingActionMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Fragment prev = getFragmentManager().findFragmentByTag("dialog");
                    if (prev != null) {
                        ft.remove(prev);
                    }
                    ft.addToBackStack(null);

                    getSubject(ft);

            }
        });

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity= (HomeActivity) context;
    }

    @Override
    public void onResume() {
        if(endless!=null)
        endless.resetState();

        index=1;
        getData();

        Logg("challenge_frag","onResume");

        super.onResume();
    }

    public void getData() {

        RequestQueue queue = Volley.newRequestQueue(activity);
        String url="";
        url = Constants.URL_LV+"getchallenge";
        url = url.replace(" ", "%20");
        Logg("getchallenge", url);

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {

                Logg("responsegetchallenge", s);

                swipeRefreshLayout.setRefreshing(false);

                try
                {

                    if(s.length()>0)
                    {
                        JSONObject arg0=new JSONObject(s);
                        if(arg0.getString("status").equalsIgnoreCase("success")) {

                            if(arg0.has("challenge")) {

                                if(arg0.getJSONArray("challenge").length()>0) {

                                    item_count=arg0.getJSONArray("challenge").length();

                                    if (index == 1) {
                                        floatingActionMenu.setVisibility(View.VISIBLE);
                                        list.setVisibility(View.VISIBLE);
                                        tv_startchallenge.setVisibility(View.GONE);
                                        adapter = new Challenge_frag_adapter(activity, arg0.getJSONArray("challenge"));
                                        list.setAdapter(adapter);
                                        index++;
                                    }
                                    else{

                                        adapter.addItem(arg0.getJSONArray("challenge"));
                                        adapter.notifyDataSetChanged();
                                        index++;
                                    }

                                }
                                else{
                                    list.setVisibility(View.GONE);
                                    tv_startchallenge.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                        else if(arg0.getString("status").equalsIgnoreCase("fail")){
                            item_count=0;
                            if(arg0.has("challenge")){
                                if(arg0.getJSONArray("challenge").length()<1&index==1){
                                    tv_startchallenge.setVisibility(View.VISIBLE);
                                    floatingActionMenu.setVisibility(View.GONE);
                                    list.setVisibility(View.GONE);
                                }
                            }
                        }
                        wheelbar.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);

                    }
                }

                catch (Exception e)
                {
                    item_count=0;
                    wheelbar.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                swipeRefreshLayout.setRefreshing(false);
                wheelbar.setVisibility(View.GONE);
                item_count=0;
                CommonUtils.toast(getActivity(),CommonUtils.volleyerror(volleyError));
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id",Constants.User_Email);
                map.put("admission_no",Constants.addmissionno);
                map.put("page",index+"");
                Logg("getchallenge params",map.toString());
                return map;
            }
        };

        request.setShouldCache(false);
        int socketTimeout = 20000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);

    }

    private void getSubject(final FragmentTransaction ft) {


        final ProgressDialog progress = ProgressDialog.show(activity, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        progress.show();


        RequestQueue queue= Volley.newRequestQueue(activity);

        String url= Constants.URL_LV+"gettopics" ;
        url=url.replace(" ","%20");
        Logg("getsubject url",url);

        StringRequest request=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s)
            {
                if(progress!=null)
                    if(progress.isShowing())
                    progress.dismiss();

                try {

                    JSONObject arg0=new JSONObject(s);

                    Logg("get_college res",s);
                    if(arg0.getString("status").contains("true")) {
                        ChallengeSubject frag = new ChallengeSubject();
                        Bundle args = new Bundle();
                        args.putString("data", arg0.getJSONArray("data")+"");
                        frag.setArguments(args);

                        getFragmentManager().beginTransaction().add(frag, "dialog").commitAllowingStateLoss();

                    }
                    else{
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(getActivity(),CommonUtils.volleyerror(volleyError));
                if(progress!=null)
                    if(progress.isShowing())
                        progress.dismiss();
            }
        })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email",Constants.User_Email);
                params.put("admission_no",Constants.addmissionno);
                params.put("type","1");
                Logg("getsubjects", params + "");
                return params;
            }
        };

        request.setShouldCache(false);
		int socketTimeout =0;//30 seconds - change to what you want
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		request.setRetryPolicy(policy);

        queue.add(request);
    }


}
