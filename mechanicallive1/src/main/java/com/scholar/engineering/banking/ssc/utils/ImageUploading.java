package com.scholar.engineering.banking.ssc.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;

import android.widget.ImageView;

import com.cocosw.bottomsheet.BottomSheet;
import com.scholar.engineering.banking.ssc.R;

import java.io.File;

import static com.scholar.engineering.banking.ssc.utils.Constants.MY_PERMISSIONS_REQUEST;
import static com.scholar.engineering.banking.ssc.utils.Constants.imageFilePath;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

/**
 * Created by surender on 8/12/2017.
*/

public class ImageUploading {

    private static BottomSheet mBottomSheetDialog;
    private static Context mContext;
    private static ImageView imageView;

    private static int REQUEST_CODE_CLICK_IMAGE = 1002, REQUEST_CODE_GALLERY_IMAGE = 1003;

    public ImageUploading(Context mContext,ImageView imageView) {
        this.mContext = mContext;
        this.imageView=imageView;
    }

    public void showOptionBottomSheetDialog() {

        String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

        if(!Utility.hasPermissions(mContext, PERMISSIONS)){
            Logg("checking","checking");

            ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST);

        }
        else {
            mBottomSheetDialog.show();
        }
    }

    public void createBottomSheetDialog() {

        BottomSheet.Builder builder = new BottomSheet.Builder((Activity) mContext).title("Choose Option").sheet(R
                .menu
                .image_selection_option).listener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case R.id.it_camera:
                        startIntent(false);
                        break;
                    case R.id.it_gallery:
                        startIntent(true);

                        break;
                    case R.id.it_cancel:

                        break;
                }
                CommonUtils.hideKeyboard((Activity) mContext);
            }
        });
        mBottomSheetDialog = builder.build();
    }

    public void startIntent(boolean isFromGallery) {
        if (!isFromGallery) {
            File imageFile = new File(imageFilePath);
            Uri imageFileUri = Uri.fromFile(imageFile); // convert path to Uri
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageFileUri);
            // set the image file name
            ((Activity)mContext).startActivityForResult(intent, REQUEST_CODE_CLICK_IMAGE);

        } else if (isFromGallery) {
            File imageFile = new File(imageFilePath);
            Uri imageFileUri = Uri.fromFile(imageFile); // convert path to Uri
            Intent intent = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageFileUri);   // set the image file name
            ((Activity)mContext).startActivityForResult(
                    Intent.createChooser(intent, "Select File"),
                    REQUEST_CODE_GALLERY_IMAGE);
        }
    }



//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_CLICK_IMAGE) {
//
//            new ImageCompression(imageView).execute(imageFilePath);
//
//        } else if (requestCode == REQUEST_CODE_GALLERY_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {
//
//            Uri uri = data.getData();
//            String[] projection = {MediaStore.Images.Media.DATA};
//            Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
//            cursor.moveToFirst();
//
//            int columnIndex = cursor.getColumnIndex(projection[0]);
//            final String picturePath = cursor.getString(columnIndex); // returns null
//            cursor.close();
//
//            //copy the selected file of gallery into app's sdcard folder and perform the compression operations on it.
//            //And override the original image with the newly resized image.
//
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    try {
//                        CommonUtils.copyFile(picturePath, imageFilePath);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
//
//            BitmapFactory.Options options = new BitmapFactory.Options();
//            options.inJustDecodeBounds = true;
//            Bitmap bmp = BitmapFactory.decodeFile(imageFilePath, options);
//
//            int actualHeight = options.outHeight;
//            int actualWidth = options.outWidth;
//
//            Logg("actualheight2",actualHeight+" "+actualWidth);
//
//            if(actualWidth>600 | actualHeight>600) {
//                new ImageCompression(imageView).execute(imageFilePath);
//            }
//            else{
//
//                getContentResolver().notifyChange(uri, null);
//                ContentResolver cr =getContentResolver();
//                Bitmap bitmap;
//
//                try
//                {
//
//                    bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, uri);
//                    //Logg("bitmap_h_w",bitmap.getHeight()+" "+bitmap.getWidth()+"");
//                    imageView.setImageBitmap(bitmap);
//
//                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
//                    byte[] byteArray = byteArrayOutputStream .toByteArray();
//
//                    String st_base46 = Base64.encodeToString(byteArray, Base64.DEFAULT);
////                    return Base64.encodeToString(byteArray, Base64.DEFAULT);
//
//                    Logg("base64",st_base46);
//
//                }
//                catch (Exception e)
//                {
//
//                }
//            }
//
//        }
//    }

}
