package com.scholar.engineering.banking.ssc.newScreens;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.Group_List_Activity;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Signup_LoginScreen extends AppCompatActivity implements View.OnClickListener {

    TextView submit,nametxt,et_dob;
    ImageView userphoto;
    String photo,name="",uemail;
    EditText et_addmissionno;
    NetworkConnection nw;
    Boolean b;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    ProgressDialog progress;
//    SharedPreferences pref;
//    SharedPreferences.Editor edit;

    UserSharedPreferences preferences;
    UserSharedPreferences playerpreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup__login_screen);

        nw=new NetworkConnection(Signup_LoginScreen.this);

        playerpreferences=new UserSharedPreferences(this,"playerpreferences");

        //initialise views
        nametxt=(TextView)findViewById(R.id.textView2);
        userphoto=(ImageView)findViewById(R.id.imageView1);
        et_addmissionno=findViewById(R.id.et_addmissionno);
        et_dob=findViewById(R.id.et_dob);
        et_dob.setOnClickListener(this);

        //preference of gmail data
        preferences=UserSharedPreferences.getInstance(this);

        photo= getIntent().getStringExtra("googleimage");
        name=getIntent().getStringExtra("googlename");
        uemail=getIntent().getStringExtra("googleemail");

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.blankimage)
                .showImageForEmptyUri(R.drawable.congrrr)
                .showImageOnFail(R.drawable.congrrr)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        if(name.length()>1)
            name = name.substring(0, 1).toUpperCase() + name.substring(1);
        nametxt.setText(name);

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(Signup_LoginScreen.this));
        imageLoader.getInstance().displayImage(photo, userphoto, options, animateFirstListener);

        submit=(TextView)findViewById(R.id.textView5);
        submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.textView5)
        {
            if(et_addmissionno.getText().toString().length()<1 && et_dob.getText().toString().length()<1)
                Toast.makeText(getApplicationContext(),"Please fill blanks field",Toast.LENGTH_LONG).show();
            else
            {
                if(nw.isConnectingToInternet())
                    volley();
                else
                    Toast.makeText(getApplicationContext(), "No Internet Connection",Toast.LENGTH_LONG).show();
            }
        }
        else if(v.getId()==R.id.et_dob)
        {
            datePicker();
        }
    }

    private int mYear, mMonth, mDay;
    void datePicker(){
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        final DecimalFormat mFormat= new DecimalFormat("00");
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        et_dob.setText(year + "-" +  mFormat.format(Double.valueOf((monthOfYear + 1))) + "-" + mFormat.format(Double.valueOf(dayOfMonth)));

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void volley() {

        if(!nw.isConnectingToInternet()) {
            internetconnection(0);
            return;
        }

        progress = ProgressDialog.show(Signup_LoginScreen.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progress.show();

        RequestQueue queue= Volley.newRequestQueue(getApplicationContext());

        String url= Constants.URL_LV+"student_login" ;
        url=url.replace(" ","%20");

        Logg("login url",url);

        StringRequest request=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s)
            {
                try {

                    progress.dismiss();
                    JSONObject arg0=new JSONObject(s);

                    Log.e("submit res",s);
                    Log.e("json",""+arg0.getJSONArray("sibling"));

                    Toast.makeText(Signup_LoginScreen.this,arg0.getString("message"),Toast.LENGTH_LONG).show();
                    if(arg0.getString("status").contains("200"))
                    {
                        progress.dismiss();

//                        edit=pref.edit();

                        preferences.setname(arg0.getString("name"));
                        preferences.setclass(arg0.getString("class"));
                        preferences.setsection(arg0.getString("section"));
                        preferences.setemail(arg0.getString("email"));
                        preferences.setaddmissiono(arg0.getString("admission_no"));
                        preferences.setdob(arg0.getString("dob"));
                        preferences.setsiblingdata(arg0.getString("sibling"));
                        preferences.setboolean(true);
                        preferences.setLoginType(Constants.studentUser);

//                        edit.commit();

                        ArrayList<String> admissionNumber = new ArrayList<>();
                        JSONArray jArray = arg0.getJSONArray("sibling");
                        if (jArray != null) {
                            for (int i=0;i<jArray.length();i++){
                                JSONObject jsonObject1 = jArray.getJSONObject(i);
                                String value1 = jsonObject1.optString("std_roll");
                                Log.e("std_roll",value1);
                                admissionNumber.add(value1);

                                preferences.setStdRoll(admissionNumber);
                            }
                        }

                        Intent in = new Intent(Signup_LoginScreen.this, Group_List_Activity.class);
                        in.putExtra("class","submit");
                        startActivity(in);
                        finish();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progress.dismiss();
                CommonUtils.toast(Signup_LoginScreen.this,CommonUtils.volleyerror(volleyError));
            }
        })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("admission_no",et_addmissionno.getText().toString());
                params.put("dob",et_dob.getText().toString());
                params.put("email",uemail);
                params.put("player_id",playerpreferences.getplayerid());

                Log.e("player_id",playerpreferences.getplayerid());
 //               params.put("email","garora211992@gmail.com");
//                params.put("email","jogender1190@gmail.com");
                Logg("params login",params.toString());
                return params;
            }
        };

        request.setShouldCache(false);

		int socketTimeout =0;
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		request.setRetryPolicy(policy);

        queue.add(request);
    }

    public void internetconnection(final int i){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

//				if(i==0)
//					//volley_state();
//				else
                if(i==1)
                    volley();

//				getData();

            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }
}
