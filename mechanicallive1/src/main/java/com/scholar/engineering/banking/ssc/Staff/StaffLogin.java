package com.scholar.engineering.banking.ssc.Staff;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.StaffLoginModel;
import com.scholar.engineering.banking.ssc.getset.RetrofitObjectAPI;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class StaffLogin extends AppCompatActivity implements View.OnClickListener {

    EditText et_email, et_pwd;
    Button btn_continue;
    NetworkConnection nw;
    UserSharedPreferences preferences;
    UserSharedPreferences playerpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_login);

        nw = new NetworkConnection(this);
        playerpreferences=new UserSharedPreferences(this,"playerpreferences");

        preferences=UserSharedPreferences.getInstance(this);
        et_email = findViewById(R.id.editTextTextEmailAddress);
        et_pwd = findViewById(R.id.editTextTextPassword);
        btn_continue = findViewById(R.id.button);

        btn_continue.setOnClickListener(this);
    }


    private boolean checkValidation(){
        boolean ret=true;
        String strUserEmail = et_email.getText().toString();
        String strUserPwd = et_pwd.getText().toString();

        if(TextUtils.isEmpty(strUserEmail)) {
            ret=false;
            Toast.makeText(this, "Please enter Email Address", Toast.LENGTH_LONG).show();
        }
        else if(TextUtils.isEmpty(strUserPwd)) {
            ret=false;
            Toast.makeText(this,"Please enter password", Toast.LENGTH_LONG).show();
        }
        return ret;
    }


    private void getStaffLogin() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<StaffLoginModel> call = service.staffLogin(et_email.getText().toString(), et_pwd.getText().toString(),playerpreferences.getplayerid());

        Logg("urlapiLogin", Constants.URL_LV + "data?email=" +
                et_email.getText().toString() + "&pwd=" + et_pwd.getText().toString());

        call.enqueue(new Callback<StaffLoginModel>() {

            @Override
            public void onResponse(Call<StaffLoginModel> call, retrofit2.Response<StaffLoginModel> response) {
                if (response.isSuccessful()) {

                    Toast.makeText(StaffLogin.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    try {
                        String date1 = response.body().getData().getCreatedAt();
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date date = null;
                        try {
                            date = dateFormat.parse(date1);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                        String dateStr = formatter.format(date);
                        Log.e("dateeee", dateStr);

                        preferences.setData(response.body().getData().getName(), response.body().getData().getEmail(),
                                response.body().getData().getRoleType(), dateStr, response.body().getData().getId(),
                                response.body().getData().getAssignclass(), response.body().getData().getAssignsection());
                        preferences.setStudentClass(response.body().getData().get_class());
                        preferences.setboolean(true);
                        preferences.setLoginType(Constants.staffUser);
                        Intent i = new Intent(StaffLogin.this, HomeStaff.class);
                        startActivity(i);
                    } catch (Exception e) {
                    }
                } else {
                    Toast.makeText(StaffLogin.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StaffLoginModel> call, Throwable t) {
                Log.e("fail",""+t.getMessage());
            }
        });
    }


    public void internetconnection() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getStaffLogin();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button:
                if(checkValidation()) {
                    getStaffLogin();
                }
                break;
        }
    }
}