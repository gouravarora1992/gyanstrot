package com.scholar.engineering.banking.ssc;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.scholar.engineering.banking.ssc.adapter.Champions_list_adapter;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.EndlessRecyclerViewScrollListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Champions_List extends Fragment{

	RecyclerView rv_honrizontal;
	int index=0;
	boolean prg=true;
	SwipeRefreshLayout swipeRefreshLayout;
	View v;
	ProgressWheel progressWheel;
	JSONArray data;
	Context context;

	@Override
	public void onAttach(Context context) {
		this.context=context;
		super.onAttach(context);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		v=inflater.inflate(R.layout.mentor_list_activity,container,false);

		rv_honrizontal=(RecyclerView)v.findViewById(R.id.recyclerView);
		LinearLayoutManager lay_Manager = new LinearLayoutManager(context);
		rv_honrizontal.setLayoutManager(lay_Manager);
		rv_honrizontal.setHasFixedSize(true);
//		rv_honrizontal.setNestedScrollingEnabled(false);
		swipeRefreshLayout = (SwipeRefreshLayout)v.findViewById(R.id.swipe_refresh_layout);
		progressWheel=(ProgressWheel)v.findViewById(R.id.progress_wheel);

		data=new JSONArray();

		EndlessRecyclerViewScrollListener endless = new EndlessRecyclerViewScrollListener(lay_Manager) {
			@Override
			public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
				Logg("page",page+"");
				index=page;
				progressWheel.setVisibility(View.VISIBLE);
			}
		};

		rv_honrizontal.addOnScrollListener(endless);


		swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				prg=false;
				index=0;
			}
		});

		return v;
	}


	public void deleteCache(Context context) {
		try {
			File dir = context.getCacheDir();
			deleteDir(dir);
		} catch (Exception e) {}
	}

	public boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
			return dir.delete();
		} else if(dir!= null && dir.isFile()) {
			return dir.delete();
		} else {
			return false;
		}
	}
}
