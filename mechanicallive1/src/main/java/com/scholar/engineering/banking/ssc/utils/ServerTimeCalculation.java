package com.scholar.engineering.banking.ssc.utils;

import android.content.Context;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class ServerTimeCalculation {

    public String getTime(Context context, String dt) {

        String time="";
        String getDate=dt;

            String server_format = getDate;    //server comes format ?
            String server_format1 = "2019-04-04T13:27:36.591Z";    //server comes format ?
            String myFormat = "yyyy-MM-dd HH:mm:ss"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            try {
                Date date = sdf.parse(server_format);
                String your_format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(date);
                String[] splitted = your_format.split(" ");
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                Date endDate = dateFormat.parse(your_format);
                Date startDate = Calendar.getInstance().getTime();
                long differenceDate = startDate.getTime() - endDate.getTime();
                String[] completeDate = splitted[0].split("-");
                String date1 = completeDate[0];
                String month = completeDate[1];
                String year = completeDate[2];
                int days_in_months = new GregorianCalendar(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(date1)).getActualMaximum(Calendar.DAY_OF_MONTH);


                long secounds = 1000;    // 1 secound
                long min = 60 * secounds;  // 1 min
                long hour = 3600000;      // 1 hour
                long day = 86400000;      // 1 days


                long monthdifference = differenceDate / (days_in_months * day);
                long daysDifference = differenceDate / day;
                long hourdifference = differenceDate / hour;
                long mindifference = differenceDate / min;
                long secoundsDiffer = differenceDate / secounds;
                Log.e("sec",""+secoundsDiffer);

                if (monthdifference > 0) time = monthdifference + " months ago";
                else if (daysDifference > 0) time = daysDifference + " days ago";
                else if (hourdifference > 0) time = hourdifference + " hours ago";
                else if (mindifference > 0) time = mindifference + " mins ago";
                else if (secoundsDiffer > 0) time = secoundsDiffer + " secs ago";
                else time="Now";

            } catch (Exception e) {
                System.out.println(e.toString()); //date format error
            }


        return time;
        }

}
