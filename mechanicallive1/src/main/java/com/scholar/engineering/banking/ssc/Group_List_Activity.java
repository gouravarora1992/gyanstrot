package com.scholar.engineering.banking.ssc;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.scholar.engineering.banking.ssc.adapter.Group_list_recycler_adap;
import com.scholar.engineering.banking.ssc.getset.gettr_settr;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.DatabaseHandler;
import com.scholar.engineering.banking.ssc.utils.EndlessRecyclerViewScrollListener;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Group_List_Activity extends AppCompatActivity {

    ActionBar bar;
    RecyclerView nlist;
    //ImageView submit;
    Group_list_recycler_adap d1;
    TextView tv_title;
    ProgressDialog dialog;
    ArrayList<gettr_settr> disc;
    public static int count = 0;
    String email, check_clas = "", cate_id = "";
    DatabaseHandler dbh;
    SwipeRefreshLayout swipeRefreshLayout;
    Toolbar toolbar;
    ArrayList<gettr_settr> grouplist;
    EndlessRecyclerViewScrollListener recyclerViewScrollListener;

    ProgressWheel wheelbar;
    RelativeLayout footerlayout;
    TextView loadmore;
    boolean prg = true;
    int index = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_list_activity);

        grouplist = new ArrayList<>();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Follow Group");

        tv_title = (TextView) findViewById(R.id.headertextid);
        tv_title.setVisibility(View.GONE);
        //nlist=(ListView)findViewById(R.id.discusslistid);

        nlist = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(Group_List_Activity.this);
        mLayoutManager.scrollToPositionWithOffset(0, 0);
        nlist.setLayoutManager(mLayoutManager);
//		nlist.setHasFixedSize(true);
//		nlist.setNestedScrollingEnabled(false);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

//		pref=getSharedPreferences("myref", 0);
        preferences = UserSharedPreferences.getInstance(this);
        cate_id = preferences.getcate_id();
//		Constants.User_Email=pref.getString(User_Email, "null");

        check_clas = getIntent().getStringExtra("class");

        Logg("check_class", check_clas);

        dbh = new DatabaseHandler(Group_List_Activity.this);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                recyclerViewScrollListener.resetState();
                prg = false;
                index = 1;
                disc = new ArrayList<gettr_settr>();
                volley(index);
            }
        });

//		grouplist=dbh.getfollowgroup();
//
//		if(grouplist.size()>0) {
//			for(int i=0;i<grouplist.size();i++)
//				//Logg("status "+i,grouplist.get(i).getStartdate());
//
//			d1 = new Group_list_recycler_adap(Group_List_Activity.this, grouplist, check_clas);
//			nlist.setAdapter(d1);
//		}

        footerlayout = (RelativeLayout) findViewById(R.id.footer_layout);
        loadmore = (TextView) findViewById(R.id.moreButton);
        wheelbar = (ProgressWheel) findViewById(R.id.progress_wheel);

        recyclerViewScrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                Logg("grouppage", page + "");
                index++;
                footerlayout.setVisibility(View.VISIBLE);
                volley(index);
            }
        };

        nlist.addOnScrollListener(recyclerViewScrollListener);

        disc = new ArrayList<gettr_settr>();
        volley(index);

    }

    public void volley(final int index) {

//		follow_group=new ArrayList<>();

        if (index == 1 & prg) {
            dialog = new ProgressDialog(Group_List_Activity.this);
            dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        RequestQueue queue = Volley.newRequestQueue(Group_List_Activity.this);
        JSONObject obj = new JSONObject();
        String url = Constants.URL + "v4/get_groups.php?email=" + Constants.User_Email + "&category_id=" + cate_id + "&index=" + index;
        Logg("Url", url);
        final JsonObjectRequest json = new JsonObjectRequest(Method.POST, url, obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                Logg("response", String.valueOf(res));
                try {
                    dialog.dismiss();

                    if (res.has("discuss")) {

                        JSONArray jr = res.getJSONArray("discuss");
//						if(grouplist.size()>0)
//						for (int i = 0; i < jr.length(); i++) {
//							JSONObject jsonobj = jr.getJSONObject(i);
//							dbh.delete_group(jsonobj.getInt("id"));
//						}

//						disc.clear();

                        if (index == 1) {
                            grouplist.clear();
                            count = 0;
                            for (int i = 0; i < jr.length(); i++) {

                                JSONObject jsonobj = jr.getJSONObject(i);
                                //Logg("followstatus "+i,jsonobj.getString("follow_status"));

//									dbh.groupinsert(jsonobj.getString("topic"),jsonobj.getString("comments"),jsonobj.getInt("id"),
//										jsonobj.getString("member"),jsonobj.getString("follow_status"),
//										jsonobj.getString("status"),jsonobj.getString("image"),jsonobj.getString("payment"),jsonobj.getString("payment_status"));

                                disc.add(new gettr_settr(jsonobj.getString("topic"), jsonobj.getInt("comments"), jsonobj.getInt("id"),
                                        jsonobj.getString("member"), jsonobj.getString("follow_status"),
                                        jsonobj.getString("status"), jsonobj.getString("image"), jsonobj.getString("payment"), jsonobj.getString("payment_status")));

                                grouplist.add(new gettr_settr(jsonobj.getString("topic"), jsonobj.getInt("comments"), jsonobj.getInt("id"),
                                        jsonobj.getString("member"), jsonobj.getString("follow_status"),
                                        jsonobj.getString("status"), jsonobj.getString("image"), jsonobj.getString("payment"), jsonobj.getString("payment_status")));

                                if (jsonobj.getString("follow_status").equalsIgnoreCase("follow")) {
                                    count++;
                                }
                            }
                            d1 = new Group_list_recycler_adap(Group_List_Activity.this, disc, check_clas);
                            nlist.setAdapter(d1);
//							d1.addItme(disc);
//							d1.notifyDataSetChanged();
//							index++;
                        } else {

                            disc = new ArrayList<>();

                            for (int i = 0; i < jr.length(); i++) {

                                JSONObject jsonobj = jr.getJSONObject(i);
                                //Logg("followstatus "+i,jsonobj.getString("follow_status"));

//									dbh.groupinsert(jsonobj.getString("topic"),jsonobj.getString("comments"),jsonobj.getInt("id"),
//										jsonobj.getString("member"),jsonobj.getString("follow_status"),
//										jsonobj.getString("status"),jsonobj.getString("image"),jsonobj.getString("payment"),jsonobj.getString("payment_status"));

                                disc.add(new gettr_settr(jsonobj.getString("topic"), jsonobj.getInt("comments"), jsonobj.getInt("id"),
                                        jsonobj.getString("member"), jsonobj.getString("follow_status"),
                                        jsonobj.getString("status"), jsonobj.getString("image"), jsonobj.getString("payment"), jsonobj.getString("payment_status")));

                                grouplist.add(new gettr_settr(jsonobj.getString("topic"), jsonobj.getInt("comments"), jsonobj.getInt("id"),
                                        jsonobj.getString("member"), jsonobj.getString("follow_status"),
                                        jsonobj.getString("status"), jsonobj.getString("image"), jsonobj.getString("payment"), jsonobj.getString("payment_status")));

                                if (jsonobj.getString("follow_status").equalsIgnoreCase("follow")) {
                                    count++;
                                }
                            }

                            d1.addItme(disc);
                            d1.notifyDataSetChanged();
//							d1=new Group_list_recycler_adap(Group_List_Activity.this,disc,check_clas);
//							nlist.setAdapter(d1);

//							d1.notifyDataSetChanged();
                        }

                    }

                    footerlayout.setVisibility(View.GONE);

                    swipeRefreshLayout.setRefreshing(false);

                } catch (JSONException e) {
                    swipeRefreshLayout.setRefreshing(false);
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                //Logg("error discuss", "e",e);
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        int socketTimeout = 50000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        json.setRetryPolicy(policy);
        queue.add(json);
    }

//	@Override
//	protected void onResume() {
//		super.onResume();
//		volley();
//	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main, menu);
        if (check_clas.equalsIgnoreCase("submit"))
            menu.findItem(R.id.shareid).setTitle("");
        else
            menu.findItem(R.id.shareid).setVisible(false);

        return true;
    }

    UserSharedPreferences preferences;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.shareid) {
        	if (grouplist.size() > 0) {
                Logg("count", count + "");
                if (count > 0) {
                    preferences.setgroupfollow(true);
                    Intent in = new Intent(Group_List_Activity.this, HomeActivity.class);
                    startActivity(in);
                    finish();
                    count = 0;
                } else
                    group_alert();
            } else {
                disc = new ArrayList<gettr_settr>();
                volley(index);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        deleteCache(this);
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) { }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }


    public void group_alert() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.group_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });

    }


}
