package com.scholar.engineering.banking.ssc.Staff.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ViewQueryModel;
import com.scholar.engineering.banking.ssc.Staff.ViewQueryDetails;

import org.json.JSONArray;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ViewQueryAdapter extends RecyclerView.Adapter<ViewQueryAdapter.ViewHolder> {

    private Context context;
    private ViewQueryModel listdata;

    public ViewQueryAdapter(Context context, ViewQueryModel listdata) {
        this.context = context;
        this.listdata = listdata;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_query,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            ViewQueryModel.Datum currPositionItem = listdata.getData().get(position);
            JSONArray itemArr = new JSONArray(currPositionItem.getItemName());
            JSONArray costArr = new JSONArray(currPositionItem.getCost());
            JSONArray purposeArr = new JSONArray(currPositionItem.getPurpose());
            JSONArray quantityArr = new JSONArray(currPositionItem.getQuantity());

            holder.txt_dept.setText(currPositionItem.getDepartment());
            holder.txt_name.setText(String.valueOf(itemArr.get(0)));
            holder.txt_cost.setText(String.valueOf(costArr.get(0)));
            holder.txt_purpose.setText(String.valueOf(purposeArr.get(0)));
            holder.txt_quantity.setText(String.valueOf(quantityArr.get(0)));
        } catch (Exception e){

        }

    }


    @Override
    public int getItemCount() {
        return listdata.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_dept;
        TextView txt_name;
        TextView txt_quantity;
        TextView txt_cost;
        TextView txt_purpose;
        ImageView img_view_more;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.txt_dept = (TextView) itemView.findViewById(R.id.txt_userName);
            this.txt_name = (TextView) itemView.findViewById(R.id.textView29);
            this.txt_quantity = (TextView) itemView.findViewById(R.id.textView31);
            this.txt_cost = (TextView) itemView.findViewById(R.id.textView33);
            this.txt_purpose = (TextView) itemView.findViewById(R.id.textView35);
            this.img_view_more = itemView.findViewById(R.id.img_view_more);

            img_view_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Bundle bundle = new Bundle();
                    Intent i =  new Intent(context, ViewQueryDetails.class);
                    bundle.putSerializable("data", listdata.getData().get(getAdapterPosition()));
                    i.putExtras(bundle);
                    context.startActivity(i);
                }
            });
        }
    }

}
