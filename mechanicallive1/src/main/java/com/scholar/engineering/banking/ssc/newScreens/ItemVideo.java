package com.scholar.engineering.banking.ssc.newScreens;

/**
 * Created by Swt on 9/16/2017.
 */

public class ItemVideo {
    int id,likecount;
    String video,title,video_desc,duration,videoId;
    Boolean like;


    public ItemVideo(int id, String video, String title, String video_desc, String duration, String videoId, Boolean like, int likecount)
    {
        this.id = id;
        this.video=video;
        this.title= title;
        this.video_desc = video_desc;
        this.duration = duration;
        this.videoId = videoId;
        this.like = like;
        this.likecount = likecount;
    }

    public int getLikecount() {
        return likecount;
    }

    public void setLikecount(int likecount) {
        this.likecount = likecount;
    }

    public Boolean getLike() {
        return like;
    }

    public void setLike(Boolean like) {
        this.like = like;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideo_desc() {
        return video_desc;
    }

    public void setVideo_desc(String video_desc) {
        this.video_desc = video_desc;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }
}
