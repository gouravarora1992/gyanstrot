package com.scholar.engineering.banking.ssc;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.scholar.engineering.banking.ssc.adapter.Home_RecyclerViewAdapter2;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.EndlessRecyclerViewScrollListener;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.toast;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.volleyerror;

/**
 * Created by surender on 4/7/2017.
 */
public class Topic_Search_Data extends AppCompatActivity {

    UserSharedPreferences preferences;
    ArrayList<Home_getset> homedata;
    RecyclerView homelist;
    public static Home_RecyclerViewAdapter2 adapter;
    SwipeRefreshLayout swipeRefreshLayout;

    int index = 0;
    String topic, fieldtype;
    ProgressDialog progress;
    Boolean aBoolean = true;

    TextView emptyview;

    NetworkConnection nw;
    EndlessRecyclerViewScrollListener endless;
    LinearLayoutManager mLayoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.topic_search_data);

        Logg("oncreate", "99");
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        nw = new NetworkConnection(this);
        emptyview = findViewById(R.id.emptyview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Utility.applyFontForToolbarTitle(toolbar, this);
        TextView tv_header = (TextView) findViewById(R.id.headertextid);

        preferences = UserSharedPreferences.getInstance(this);

        if (getIntent().hasExtra("topic")) {
            tv_header.setVisibility(View.GONE);
            Logg("fieldtypedata", getIntent().getStringExtra("topic") + "," + getIntent().getStringExtra("fieldtype"));
            topic = getIntent().getStringExtra("topic");
            fieldtype = getIntent().getStringExtra("fieldtype");
            getSupportActionBar().setTitle(topic);
        } else {
            tv_header.setText("");
        }

        Constants.Current_Activity = 1;

        index = 0;
        homedata = new ArrayList<>();
        homelist = (RecyclerView) findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(this);
        homelist.setLayoutManager(mLayoutManager);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                homedata = new ArrayList<>();
                index = 0;
                getData();

                endless = new EndlessRecyclerViewScrollListener(mLayoutManager) {
                    @Override
                    public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                        if (homedata.size() % 10 == 0) {
                            index++;
                            getData();
                        }
                    }
                };
                homelist.addOnScrollListener(endless);
            }
        });
        endless = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (homedata.size() % 10 == 0) {
                    index++;
                    getData();
                }
            }
        };
        homelist.addOnScrollListener(endless);

        getData();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logg("call_onDestroy()", "Call onDestroy()");
        deleteCache(this);
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    public void getData() {
        if (!nw.isConnectingToInternet()) {
            swipeRefreshLayout.setRefreshing(false);
            internetconnection();
            return;
        }

        if (index == 0) {
            progress = ProgressDialog.show(Topic_Search_Data.this, null, null, true);
            progress.setContentView(R.layout.progressdialog);
            progress.setCanceledOnTouchOutside(false);
            progress.setCancelable(false);
            progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progress.show();
        }

        aBoolean = true;

        RequestQueue queue = Volley.newRequestQueue(this);
        JSONObject obj = new JSONObject();
        try {
            obj.put("email", Constants.User_Email);
            obj.put("index", index);
            obj.put("fieldtype", fieldtype);
            obj.put("admission_no", Constants.addmissionno);
            obj.put("topic", topic);
            obj.put("class", preferences.getclass());
            obj.put("section", preferences.getsection());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url = Constants.URL_LV + "get_topic_data";
        Logg("get_topic_data", url + "," + obj);

        JsonObjectRequest json = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                swipeRefreshLayout.setRefreshing(false);
                if (progress.isShowing())
                    progress.dismiss();
                try {
                    if (res.has("data")) {
                        JSONArray jr = res.getJSONArray("data");
                        if(jr.length()>0) {
                            homelist.setVisibility(View.VISIBLE);
                            emptyview.setVisibility(View.GONE);
                            for (int i = 0; i < jr.length(); i++) {
                                JSONObject ob = jr.getJSONObject(i);

                                String time = Utility.getDate(ob.getString("timestamp"));
                                Home_getset object = new Home_getset();
                                object.setId(ob.getInt("id"));
                                object.setTimestamp(ob.getString("timestamp"));
                                object.setLikecount(ob.getInt("likes"));
                                object.setCommentcount(ob.getInt("comment"));
                                object.setViewcount(ob.getInt("view"));
                                object.setUid(ob.getString("uid"));
                                object.setPosttype(ob.getString("posttype"));
                                object.setGroupid(ob.getString("groupid"));
                                object.setFieldtype(ob.getString("field_type"));
                                object.setJsonfield(ob.getString("jsondata"));
                                object.setLikestatus(ob.getString("likestatus"));
                                object.setPosturl(ob.getString("posturl"));
                                object.setPostdescription(ob.getString("post_description"));
                                if (ob.has("AppVersion"))
                                    object.setAppVersion(ob.getString("AppVersion"));
                                homedata.add(object);
                            }

                            adapter = new Home_RecyclerViewAdapter2(Topic_Search_Data.this, homedata, "Topic_Search_Data");
                            homelist.setAdapter(adapter);
                        }
                        else
                        {
                            homelist.setVisibility(View.GONE);
                            emptyview.setVisibility(View.VISIBLE);
                        }
                    } else {
                        homelist.setVisibility(View.GONE);
                        emptyview.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    homelist.setVisibility(View.GONE);
                    emptyview.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                if (index == 0)
                    if (progress.isShowing())
                        progress.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                homelist.setVisibility(View.GONE);
                emptyview.setVisibility(View.VISIBLE);
                toast(Topic_Search_Data.this, volleyerror(arg0));
            }
        });
        json.setShouldCache(false);
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        json.setRetryPolicy(policy);
        queue.add(json);
    }

    public void internetconnection() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getData();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

}
