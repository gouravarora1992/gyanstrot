package com.scholar.engineering.banking.ssc.Staff.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeaveGettingModel {

    @SerializedName("status")
    @Expose
    public Boolean status;
    @SerializedName("data")
    @Expose
    public Data data;
    @SerializedName("message")
    @Expose
    public String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class Data {

        @SerializedName("id")
        @Expose
        public Long id;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("email_verified_at")
        @Expose
        public Object emailVerifiedAt;
        @SerializedName("password")
        @Expose
        public String password;
        @SerializedName("remember_token")
        @Expose
        public Object rememberToken;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("updated_at")
        @Expose
        public String updatedAt;
        @SerializedName("role")
        @Expose
        public String role;
        @SerializedName("permission")
        @Expose
        public String permission;
        @SerializedName("showpassword")
        @Expose
        public String showpassword;
        @SerializedName("role_type")
        @Expose
        public Object roleType;
        @SerializedName("sick_leave")
        @Expose
        public Integer sickLeave;
        @SerializedName("casual_leave")
        @Expose
        public Integer casualLeave;
        @SerializedName("annual_leave")
        @Expose
        public Integer annualLeave;
        @SerializedName("player_id")
        @Expose
        public Object playerId;
        @SerializedName("masterscol")
        @Expose
        public Object masterscol;
        @SerializedName("cousume_casual_leave")
        @Expose
        public Integer cousumeCasualLeave;
        @SerializedName("cousume_sick_leave")
        @Expose
        public Integer cousumeSickLeave;
        @SerializedName("cousume_annual_leave")
        @Expose
        public Integer cousumeAnnualLeave;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getEmailVerifiedAt() {
            return emailVerifiedAt;
        }

        public void setEmailVerifiedAt(Object emailVerifiedAt) {
            this.emailVerifiedAt = emailVerifiedAt;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public Object getRememberToken() {
            return rememberToken;
        }

        public void setRememberToken(Object rememberToken) {
            this.rememberToken = rememberToken;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getPermission() {
            return permission;
        }

        public void setPermission(String permission) {
            this.permission = permission;
        }

        public String getShowpassword() {
            return showpassword;
        }

        public void setShowpassword(String showpassword) {
            this.showpassword = showpassword;
        }

        public Object getRoleType() {
            return roleType;
        }

        public void setRoleType(Object roleType) {
            this.roleType = roleType;
        }

        public Integer getSickLeave() {
            return sickLeave;
        }

        public void setSickLeave(Integer sickLeave) {
            this.sickLeave = sickLeave;
        }

        public Integer getCasualLeave() {
            return casualLeave;
        }

        public void setCasualLeave(Integer casualLeave) {
            this.casualLeave = casualLeave;
        }

        public Integer getAnnualLeave() {
            return annualLeave;
        }

        public void setAnnualLeave(Integer annualLeave) {
            this.annualLeave = annualLeave;
        }

        public Object getPlayerId() {
            return playerId;
        }

        public void setPlayerId(Object playerId) {
            this.playerId = playerId;
        }

        public Object getMasterscol() {
            return masterscol;
        }

        public void setMasterscol(Object masterscol) {
            this.masterscol = masterscol;
        }

        public Integer getCousumeCasualLeave() {
            return cousumeCasualLeave;
        }

        public void setCousumeCasualLeave(Integer cousumeCasualLeave) {
            this.cousumeCasualLeave = cousumeCasualLeave;
        }

        public Integer getCousumeSickLeave() {
            return cousumeSickLeave;
        }

        public void setCousumeSickLeave(Integer cousumeSickLeave) {
            this.cousumeSickLeave = cousumeSickLeave;
        }

        public Integer getCousumeAnnualLeave() {
            return cousumeAnnualLeave;
        }

        public void setCousumeAnnualLeave(Integer cousumeAnnualLeave) {
            this.cousumeAnnualLeave = cousumeAnnualLeave;
        }
    }

}
