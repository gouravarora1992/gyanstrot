package com.scholar.engineering.banking.ssc.newScreens;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.scholar.engineering.banking.ssc.Group_List_Activity;
import com.scholar.engineering.banking.ssc.HomeActivity;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.HomeStaff;
import com.scholar.engineering.banking.ssc.Staff.StaffLogin;
import com.scholar.engineering.banking.ssc.adapter.PagerAdapter_circleindication;
import com.scholar.engineering.banking.ssc.utils.CirclePageIndicator;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.content.Intent;

import android.os.Handler;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class SplashScreen extends AppCompatActivity implements OnClickListener {

    private static final int RC_SIGN_IN = 0;
    int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

//    SharedPreferences share;
//    SharedPreferences.Editor edit;
//    String email="";
//    String personName,personPhotoUrl;

    ProgressBar p;
    Typeface font_demi;
    Typeface font_medium;

    TextView txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8;

    TextView btnSignIn,btnStaffLogin;
    public static TextView tv_title, tv_descrition;
    ViewPager viewPager;
    PagerAdapter_circleindication adapter;
    CirclePageIndicator indicator;
    NetworkConnection nw;

    Handler handler;

    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 500;  //delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3000;

    UserSharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_new);

        preferences = UserSharedPreferences.getInstance(this);

        p = (ProgressBar) findViewById(R.id.progressBar);
        nw = new NetworkConnection(SplashScreen.this);

        final Boolean bb = preferences.getboolean();
        final String loginType = preferences.getLogintype();
        final String isUserLogin = preferences.getIsUserLogin();

//        String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
//        Constants.deviceId = md5(android_id).toUpperCase();

        if (bb) {
            if (loginType.equals(Constants.studentUser)) {
                if (preferences.getgroupfollow()) {
                    Intent in = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(in);
                    finish();
                } else if (preferences.getcategoryfollow()) {
                    Intent in = new Intent(getApplicationContext(), Group_List_Activity.class);
                    in.putExtra("class", "submit");
                    startActivity(in);
                    finish();
                } else {
                    Intent in = new Intent(getApplicationContext(), Group_List_Activity.class);
                    in.putExtra("class", "submit");
                    startActivity(in);
                    finish();
                }
            } else if (loginType.equals(Constants.staffUser)) {
                Intent in = new Intent(getApplicationContext(), HomeStaff.class);
                startActivity(in);
                finish();
            }
        } else {
            if (Build.VERSION.SDK_INT >= 23) {
                getDummyContactWrapper();
            }
            // Making notification bar transparent
            if (Build.VERSION.SDK_INT >= 21) {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            }

            font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");
            font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");

            tv_title = (TextView) findViewById(R.id.tv_title);
            tv_descrition = (TextView) findViewById(R.id.tv_description);

            tv_title.setTypeface(font_demi);
            tv_descrition.setTypeface(font_medium);

            txt1 = (TextView) findViewById(R.id.textView1);
            txt2 = (TextView) findViewById(R.id.textView2);
            txt3 = (TextView) findViewById(R.id.textView3);
            txt4 = (TextView) findViewById(R.id.textView4);
            txt5 = (TextView) findViewById(R.id.textView5);
            txt6 = (TextView) findViewById(R.id.textView6);
            txt7 = (TextView) findViewById(R.id.textView7);
            txt8 = (TextView) findViewById(R.id.textView8);
            btnSignIn = (TextView) findViewById(R.id.btn_sign_in);
            btnStaffLogin = findViewById(R.id.btn_staff_login);

            btnSignIn.setTypeface(font_demi);

            viewPager = (ViewPager) findViewById(R.id.viewPager);
            adapter = new PagerAdapter_circleindication(getSupportFragmentManager());
            viewPager.setAdapter(adapter);
            indicator = (CirclePageIndicator) findViewById(R.id.indicator);
            indicator.setViewPager(viewPager);

            btnSignIn.setOnClickListener(this);
            btnStaffLogin.setOnClickListener(this);

            handler = new Handler();

            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == 3) {
                        currentPage = 0;
                    }
                    viewPager.setCurrentItem(currentPage++, true);
                }
            };

            timer = new Timer(); // This will create a new Thread
            timer.schedule(new TimerTask() { // task to be scheduled

                @Override
                public void run() {
                    handler.post(Update);
                }
            }, DELAY_MS, PERIOD_MS);

            indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }
                @Override
                public void onPageSelected(int position) {
                    if (position == 0) {
                        tv_title.setText("Online Tests");
                        tv_descrition.setText("Know your rank & score");
                    } else if (position == 1) {
                        tv_title.setText("Study Material");
                        tv_descrition.setText("Free Latest Notes");
                    } else if (position == 2) {
                        tv_title.setText("Discussion Groups");
                        tv_descrition.setText("Let's help each other");
                    }
                }
                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        if (nw.isConnectingToInternet()) {
            switch (v.getId()) {
                case R.id.btn_sign_in:
                    p.setVisibility(View.VISIBLE);
                    signInWithGplus();
                    break;

                case R.id.btn_staff_login:
                    Intent i = new Intent(getApplicationContext(), StaffLogin.class);
                    startActivity(i);
                    break;
            }
        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    GoogleSignInClient mGoogleSignInClient;
    private void signInWithGplus() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        Logg("completed task", completedTask.isComplete() + "," + completedTask.isCanceled());
        try {
            if (completedTask.isSuccessful()) {

                GoogleSignInAccount acct = completedTask.getResult(ApiException.class);
                Logg("account data", acct.toString());

                String personPhotoUrl = "";
                if (acct.getPhotoUrl() != null) {
                    personPhotoUrl = acct.getPhotoUrl().toString();
                }

                Intent in = new Intent(getApplicationContext(), Signup_LoginScreen.class);
                in.putExtra("googlename", acct.getDisplayName());
                in.putExtra("googleimage", personPhotoUrl);
                in.putExtra("googleemail", acct.getEmail());
                startActivity(in);
                finish();

            } else {
                Logg("exc3eption", completedTask.getException().toString());
                Toast.makeText(this, completedTask.getException().toString(), Toast.LENGTH_SHORT).show();
            }
        } catch (ApiException e) { }
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            if (!shouldShowRequestPermissionRationale(permission))
                return false;
        }
        return true;
    }

    private void getDummyContactWrapper() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, android.Manifest.permission.ACCESS_NETWORK_STATE))
            permissionsNeeded.add("ACCESS_NETWORK_STATE");
        if (!addPermission(permissionsList, android.Manifest.permission.INTERNET))
            permissionsNeeded.add("INTERNET");
        if (!addPermission(permissionsList, Manifest.permission.CLEAR_APP_CACHE))
            permissionsNeeded.add("CLEAR_APP_CACHE");
        if (!addPermission(permissionsList, android.Manifest.permission.ACCESS_WIFI_STATE))
            permissionsNeeded.add("ACCESS_WIFI_STATE");
//        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
//            permissionsNeeded.add("LOCATION");
//        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
//            permissionsNeeded.add("LOCATION");
        requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
        return;
    }
}
