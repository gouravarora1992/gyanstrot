package com.scholar.engineering.banking.ssc.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.userprofile.UserProfile;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.isNull;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

/**
 * Created by surender on 3/14/2017.
*/

public class Champions_list_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

//    SharedPreferences pref,prefsignup;
    Context cs;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    JSONArray data;
    ImageLoader imageLoader;
    String clas;
    JSONObject object;

    public void addItme(JSONArray data1){
        for(int i=0;i<data1.length();i++){
            try {
                this.data.put(data1.getJSONObject(i));
            } catch (JSONException e) {

            }
        }
    }

    public Champions_list_adapter(Context cs,JSONArray data,String clas) {
        this.cs=cs;
        this.data=data;
        this.clas=clas;
        Logg("dagta",this.data+" ");

//        if(cs!=null)
//        prefsignup=cs.getSharedPreferences("myref",Context.MODE_PRIVATE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

                View v0 = inflater.inflate(R.layout.champions_itme, viewGroup, false);

                viewHolder = new ViewHolder0(v0);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

                ViewHolder0 vh0 = (ViewHolder0) viewHolder;

                setData(vh0, position);

    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    public void setData(ViewHolder0 v, final int p){

        try {

            object=data.getJSONObject(p);
            v.tv_name.setText(object.getString("name"));
            if(isNull(object.getString("cashback")))
                v.tv_follower.setText("Cashback ₹ "+object.getString("cashback"));
            else
                v.tv_follower.setText("Cashback ₹ 0");

            v.tv_marks.setText("Marks "+object.getString("marks"));
            v.tv_collage.setText(object.getString("collage"));

            String img="";
            if(object.getString("userimage").contains("hellotopper.in"))
                img=object.getString("userimage").replace("hellotopper.in","hellotopper.com");
            else
                img=object.getString("userimage");

            Glide.with(cs).load(img).into(v.iv_user);

            Logg("userimage2",object.getString("userimage")+"");

            v.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                        try {

                            object=data.getJSONObject(p);

                            Intent in = new Intent(cs, UserProfile.class);
                            in.putExtra("email", object.getString("uid"));
                            cs.startActivity(in);

                        } catch (JSONException e) {

                        }

                }
            });


            Typeface font_demi = Typeface.createFromAsset(cs.getAssets(), "avenirnextdemibold.ttf");
            Typeface font_medium = Typeface.createFromAsset(cs.getAssets(), "avenirnextmediumCn.ttf");
            v.tv_name.setTypeface(font_demi);
            v.tv_collage.setTypeface(font_medium);
            v.tv_marks.setTypeface(font_medium);
            v.tv_follower.setTypeface(font_medium);


        } catch (JSONException e) {

        }

    }

    public class ViewHolder0 extends RecyclerView.ViewHolder {

        TextView tv_name,tv_follower,tv_marks,tv_collage;
        CircleImageView iv_user;
        LinearLayout layout;

        public ViewHolder0(View bn) {
            super(bn);

            layout=(LinearLayout)bn.findViewById(R.id.layout);
            tv_name=(TextView)bn.findViewById(R.id.tv_name);
            tv_follower=(TextView)bn.findViewById(R.id.tv_follower);
            tv_marks=(TextView)bn.findViewById(R.id.tv_marks);
            tv_collage=(TextView)bn.findViewById(R.id.tv_collage);
            iv_user=(CircleImageView)bn.findViewById(R.id.iv_user);

        }
    }
}
