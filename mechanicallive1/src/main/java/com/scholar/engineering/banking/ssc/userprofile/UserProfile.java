package com.scholar.engineering.banking.ssc.userprofile;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.newScreens.StudentProfile;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

//import android.support.v7.app.ActionBarActivity;

public class UserProfile extends AppCompatActivity {

    private ViewPager viewPager;
    TextView tv_name,tv_collage,tv_comment,tv_reward;
    String savedcont_id="";
    TextView tv_edit;
    SharedPreferences pref;
    CircleImageView iv_user;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    LinearLayout lay_phone;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_user_profile);

        Typeface font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");
        Typeface font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");

        pref=getSharedPreferences("pref",MODE_PRIVATE);


        viewPager = (ViewPager) findViewById(R.id.viewpager);

        tv_name=(TextView)findViewById(R.id.tv_username);
        tv_comment=(TextView)findViewById(R.id.tv_comment);
        tv_reward=(TextView)findViewById(R.id.tv_reward);

        tv_collage=(TextView)findViewById(R.id.tv_collage);
        tv_edit=(TextView) findViewById(R.id.tv_editprofle);
        lay_phone=(LinearLayout)findViewById(R.id.lay_phone);

        tv_name.setTypeface(font_demi);
        tv_comment.setTypeface(font_demi);
        tv_reward.setTypeface(font_demi);
        tv_collage.setTypeface(font_medium);

        iv_user=(CircleImageView)findViewById(R.id.iv_user);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.user_default)
                .showImageForEmptyUri(R.drawable.user_default)
                .showImageOnFail(R.drawable.user_default)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setTitleTextColor(R.color.text_darkgray);
        getSupportActionBar().setTitle("");

        getuserDetails();

    }

    private void initViewPagerAndTabs(String name) {

       PremiumTest_UserHistory pr_frag= new PremiumTest_UserHistory();
       Bundle b=new Bundle();
       b.putString("user_profile_id",getIntent().getStringExtra("student_id"));
       b.putString("name",name);
       pr_frag.setArguments(b);

//        User_Comment_In_Profile uc_frag= new User_Comment_In_Profile();
//        uc_frag.setArguments(b);

        PagerAdapter1 pagerAdapter = new PagerAdapter1(getSupportFragmentManager());
        pagerAdapter.addFragment(pr_frag, "TASKS");
        pagerAdapter.addFragment(new Followed_Groups(), "GROUPS");
//        pagerAdapter.addFragment(uc_frag, "POSTS");

        viewPager.setAdapter(pagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.setOffscreenPageLimit(3);

            viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    class PagerAdapter1 extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter1(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }

    public void setData(JSONObject obj){
        try {

            JSONObject obj_details=obj.getJSONObject("data");
            tv_name.setText(obj_details.getString("Student_name"));
            tv_collage.setText(obj_details.getString("Student_class")+"-"+obj_details.getString("Student_section"));

            tv_comment.setText(obj_details.getInt("cashback")+"");
            tv_reward.setText(obj_details.getInt("marks")+"");

            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.init(ImageLoaderConfiguration.createDefault(UserProfile.this));

            if(!obj_details.getString("image").equalsIgnoreCase("")) {
                imageLoader.getInstance().displayImage(obj_details.getString("image").replace("hellotopper.in","hellotopper.com"), iv_user, options, animateFirstListener);
            }

            initViewPagerAndTabs(obj_details.getString("Student_name"));


        } catch (JSONException e) {
            Log.e("exception","",e);
        }
    }

    private void getuserDetails() {

        final ProgressDialog progress = ProgressDialog.show(UserProfile.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progress.show();

        String url=Constants.URL_LV+"getuserdetails";
        Logg("getuserdetails", url);

        RequestQueue que= Volley.newRequestQueue(UserProfile.this);
        final StringRequest obj1=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {

                Logg("getuserdetails response",s);
                try {
                    JSONObject res=new JSONObject(s);
                    if(res.has("data")) {
                        setData(res);
                    }
                }
                catch (JSONException e) { }

                progress.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                CommonUtils.toast(UserProfile.this,CommonUtils.volleyerror(e));
                progress.dismiss();
            }
        }){

            @Override
            protected Map<String,String> getParams(){

                Map<String,String> params = new HashMap<String, String>();
                params.put("email",getIntent().getStringExtra("student_id"));
                params.put("admission_no",getIntent().getStringExtra("admission_no"));
                Logg("getuserdetails",params+"");
                return params;
            }
        };

        obj1.setShouldCache(false);
        int socketTimeout =30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        obj1.setRetryPolicy(policy);

        que.add(obj1);

    }

}