package com.scholar.engineering.banking.ssc.newScreens;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class NotesVideos extends AppCompatActivity {
    Context context;

    Toolbar toolbar;
    TextView toolbartitle;

    UserSharedPreferences sharedPreferences;
    ProgressDialog pd;

    RecyclerView recyclerView;
    ArrayList<ItemVideo> itemVideos;
    YoutubeVideoAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_videos);
        context = NotesVideos.this;
        intilise();
    }

    private void intilise() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbartitle = (TextView) toolbar.findViewById(R.id.headertextid);
        toolbartitle.setText("Notes Videos");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedPreferences = UserSharedPreferences.getInstance(context);
        pd = new ProgressDialog(context);
        pd.setMessage("loading");
        pd.setCancelable(false);

        itemVideos = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.rv_asans);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new YoutubeVideoAdapter(itemVideos, context);
        recyclerView.setAdapter(adapter);
        sendRequest();

    }

    @Override
    protected void onDestroy() {
        if (pd != null && pd.isShowing()) {
            pd.dismiss();
        }
        super.onDestroy();
    }

    private void sendRequest() {

        pd.show();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_LV+"get_media",

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("response",response);
                        pd.dismiss();
                        try {
                            JSONObject object = new JSONObject(response);
                            JSONArray ara = object.getJSONArray("data");
                            for (int i = 0; i < ara.length(); i++) {
                                JSONObject obj = ara.getJSONObject(i);
                                int  urilength = (obj.getString("videouri").length());
                                String uri = obj.getString("videouri");
                                Log.e("urilength",""+urilength);
                                Log.e("uri",uri);

                       //         String vedioid=obj.getString("videouri").substring(30,urilength);
                                String vedioid=obj.getString("videouri").substring(uri.lastIndexOf("/")+1);
                                Log.e("vedioid---->",vedioid);
                                itemVideos.add(new ItemVideo(obj.getInt("id"),
                                        obj.getString("videouri"), obj.getString("subject"),
                                        obj.getString("description"), obj.getString("link"),
                                        vedioid, false, 0));
                            }
                            adapter = new YoutubeVideoAdapter(itemVideos, context);
                            recyclerView.setAdapter(adapter);
                        } catch (JSONException e) {
                            e.printStackTrace();

                            pd.dismiss();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        CommonUtils.volleyerror(error);
                        pd.dismiss();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", sharedPreferences.getemail());
                params.put("admission_no", sharedPreferences.getaddmissiono());
                Log.e("get_media", params + "");
                return params;
            }
        };

        requestQueue.add(stringRequest);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
