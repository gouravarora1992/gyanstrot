package com.scholar.engineering.banking.ssc.Staff.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ViewQueryModel {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class Datum implements Serializable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("item_name")
        @Expose
        private String itemName;
        @SerializedName("quantity")
        @Expose
        private String quantity;
        @SerializedName("cost")
        @Expose
        private String cost;
        @SerializedName("purpose")
        @Expose
        private String purpose;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("justification")
        @Expose
        private String justification;
        @SerializedName("last_date")
        @Expose
        private String lastDate;
        @SerializedName("existing_stock")
        @Expose
        private String existingStock;
        @SerializedName("last_purchase")
        @Expose
        private String lastPurchase;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private Object updatedAt;
        @SerializedName("isActive")
        @Expose
        private Object isActive;
        @SerializedName("essential")
        @Expose
        private Object essential;
        @SerializedName("quotations")
        @Expose
        private Object quotations;
        @SerializedName("total_amount")
        @Expose
        private Object totalAmount;
        @SerializedName("quan_comp")
        @Expose
        private Object quanComp;
        @SerializedName("budget")
        @Expose
        private Object budget;
        @SerializedName("amount_spend")
        @Expose
        private Object amountSpend;
        @SerializedName("balane_available")
        @Expose
        private Object balaneAvailable;
        @SerializedName("active")
        @Expose
        private Object active;
        @SerializedName("sanctioned")
        @Expose
        private Object sanctioned;
        @SerializedName("comments")
        @Expose
        private Object comments;
        @SerializedName("purpose_image")
        @Expose
        private String purposeImage;
        @SerializedName("addministrative_image")
        @Expose
        private Object addministrativeImage;
        @SerializedName("department_head")
        @Expose
        private String departmentHead;
        @SerializedName("department")
        @Expose
        private String department;
        @SerializedName("general_remarks")
        @Expose
        private Object generalRemarks;
        @SerializedName("admin_active")
        @Expose
        private Object adminActive;
        @SerializedName("total_cost")
        @Expose
        private String totalCost;
        @SerializedName("approval_date")
        @Expose
        private Object approvalDate;
        @SerializedName("approved_person")
        @Expose
        private Object approvedPerson;
        @SerializedName("need_purchase")
        @Expose
        private String needPurchase;
        @SerializedName("final_cost")
        @Expose
        private String finalCost;
        @SerializedName("accountant_person")
        @Expose
        private Object accountantPerson;
        @SerializedName("accountant_approval_date")
        @Expose
        private Object accountantApprovalDate;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getCost() {
            return cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }

        public String getPurpose() {
            return purpose;
        }

        public void setPurpose(String purpose) {
            this.purpose = purpose;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getJustification() {
            return justification;
        }

        public void setJustification(String justification) {
            this.justification = justification;
        }

        public String getLastDate() {
            return lastDate;
        }

        public void setLastDate(String lastDate) {
            this.lastDate = lastDate;
        }

        public String getExistingStock() {
            return existingStock;
        }

        public void setExistingStock(String existingStock) {
            this.existingStock = existingStock;
        }

        public String getLastPurchase() {
            return lastPurchase;
        }

        public void setLastPurchase(String lastPurchase) {
            this.lastPurchase = lastPurchase;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public Object getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(Object updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getIsActive() {
            return isActive;
        }

        public void setIsActive(Object isActive) {
            this.isActive = isActive;
        }

        public Object getEssential() {
            return essential;
        }

        public void setEssential(Object essential) {
            this.essential = essential;
        }

        public Object getQuotations() {
            return quotations;
        }

        public void setQuotations(Object quotations) {
            this.quotations = quotations;
        }

        public Object getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(Object totalAmount) {
            this.totalAmount = totalAmount;
        }

        public Object getQuanComp() {
            return quanComp;
        }

        public void setQuanComp(Object quanComp) {
            this.quanComp = quanComp;
        }

        public Object getBudget() {
            return budget;
        }

        public void setBudget(Object budget) {
            this.budget = budget;
        }

        public Object getAmountSpend() {
            return amountSpend;
        }

        public void setAmountSpend(Object amountSpend) {
            this.amountSpend = amountSpend;
        }

        public Object getBalaneAvailable() {
            return balaneAvailable;
        }

        public void setBalaneAvailable(Object balaneAvailable) {
            this.balaneAvailable = balaneAvailable;
        }

        public Object getActive() {
            return active;
        }

        public void setActive(Object active) {
            this.active = active;
        }

        public Object getSanctioned() {
            return sanctioned;
        }

        public void setSanctioned(Object sanctioned) {
            this.sanctioned = sanctioned;
        }

        public Object getComments() {
            return comments;
        }

        public void setComments(Object comments) {
            this.comments = comments;
        }

        public String getPurposeImage() {
            return purposeImage;
        }

        public void setPurposeImage(String purposeImage) {
            this.purposeImage = purposeImage;
        }

        public Object getAddministrativeImage() {
            return addministrativeImage;
        }

        public void setAddministrativeImage(Object addministrativeImage) {
            this.addministrativeImage = addministrativeImage;
        }

        public String getDepartmentHead() {
            return departmentHead;
        }

        public void setDepartmentHead(String departmentHead) {
            this.departmentHead = departmentHead;
        }

        public String getDepartment() {
            return department;
        }

        public void setDepartment(String department) {
            this.department = department;
        }

        public Object getGeneralRemarks() {
            return generalRemarks;
        }

        public void setGeneralRemarks(Object generalRemarks) {
            this.generalRemarks = generalRemarks;
        }

        public Object getAdminActive() {
            return adminActive;
        }

        public void setAdminActive(Object adminActive) {
            this.adminActive = adminActive;
        }

        public String getTotalCost() {
            return totalCost;
        }

        public void setTotalCost(String totalCost) {
            this.totalCost = totalCost;
        }

        public Object getApprovalDate() {
            return approvalDate;
        }

        public void setApprovalDate(Object approvalDate) {
            this.approvalDate = approvalDate;
        }

        public Object getApprovedPerson() {
            return approvedPerson;
        }

        public void setApprovedPerson(Object approvedPerson) {
            this.approvedPerson = approvedPerson;
        }

        public String getNeedPurchase() {
            return needPurchase;
        }

        public void setNeedPurchase(String needPurchase) {
            this.needPurchase = needPurchase;
        }

        public String getFinalCost() {
            return finalCost;
        }

        public void setFinalCost(String finalCost) {
            this.finalCost = finalCost;
        }

        public Object getAccountantPerson() {
            return accountantPerson;
        }

        public void setAccountantPerson(Object accountantPerson) {
            this.accountantPerson = accountantPerson;
        }

        public Object getAccountantApprovalDate() {
            return accountantApprovalDate;
        }

        public void setAccountantApprovalDate(Object accountantApprovalDate) {
            this.accountantApprovalDate = accountantApprovalDate;
        }

    }

}
