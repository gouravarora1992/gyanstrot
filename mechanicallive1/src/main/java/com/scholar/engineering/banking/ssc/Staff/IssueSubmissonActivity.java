package com.scholar.engineering.banking.ssc.Staff;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.IssueTypeModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.PickUpRequestModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.StudentInfoModel;
import com.scholar.engineering.banking.ssc.databinding.ActivityIssueSubmissonBinding;
import com.scholar.engineering.banking.ssc.getset.RetrofitObjectAPI;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.GetPath;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class IssueSubmissonActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityIssueSubmissonBinding binding;
    NetworkConnection nw;
    final int  PERMISSION_REQUEST_CODE=200;
    private final static int TAKE_PICTURE = 100;
    private static final int PICK_GALLERY = 102;
    private String imagePath;
    private File file = null;
    private Uri mImageCaptureUri;
    List<IssueTypeModel.Datum> IssueTypeArr;
    UserSharedPreferences preferences;
    UserSharedPreferences playerpreferences;
    Integer id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_issue_submisson);

        nw = new NetworkConnection(IssueSubmissonActivity.this);
        playerpreferences=new UserSharedPreferences(IssueSubmissonActivity.this,"playerpreferences");
        preferences=UserSharedPreferences.getInstance(IssueSubmissonActivity.this);
        id = preferences.getId();

        binding.btnSubmit.setOnClickListener(this);
        binding.button4.setOnClickListener(this);
        binding.imgBack.setOnClickListener(this);

        getIssueType();

        binding.etAddmissionno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!binding.etAddmissionno.getText().toString().isEmpty()) {
                    getStudentInfo();
                }
            }
        });

        //camera
        if (!checkPermission()) {
            requestPermision();
        }

    }

    //permission
    private void requestPermision() {
        ActivityCompat.requestPermissions(IssueSubmissonActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
    }

    private boolean checkPermission() {

        int READ_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(IssueSubmissonActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        int WRITE_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(IssueSubmissonActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (WRITE_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else if (READ_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    //DailogForImageFromCameraAndGallery
    public void profileBottomLayout() {
        LayoutInflater layoutInflater = (LayoutInflater) IssueSubmissonActivity.this.getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.upload_pic_layout, null);
        ImageView camera = (ImageView) popupView.findViewById(R.id.camera);
        ImageView gallery = (ImageView) popupView.findViewById(R.id.gallery);

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(IssueSubmissonActivity.this);
        alertDialog.setTitle("Upload Photo");
        alertDialog.setView(popupView);
        final AlertDialog dialog = alertDialog.show();
        alertDialog.setCancelable(true);

        //ImageFromCamera
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPermission()) {
                    imagePath = System.currentTimeMillis() + ".png";
                    takePictureFromCamera(IssueSubmissonActivity.this, imagePath);
                } else {
                    requestPermision();
                }
                dialog.dismiss();
            }

        });

        //ImageFromGallery
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPermission()) {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_GALLERY);
                } else {
                    requestPermision();
                }
                dialog.dismiss();
            }
        });
    }

    //take picture from camera
    private void takePictureFromCamera(Context context, String imagePath) {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            mImageCaptureUri = Uri.fromFile(new File(IssueSubmissonActivity.this.getExternalFilesDir("temp"), imagePath));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, TAKE_PICTURE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && permissions[0].equals(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) && permissions[1].equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&  grantResults[1] == PackageManager.PERMISSION_GRANTED ) {
                        Log.e("granted","WRITE_EXTERNAL_STORAGE \n READ_EXTERNAL_STORAGE");
                        // do what you want;
                    }else{
                        Log.e("denied","WRITE_EXTERNAL_STORAGE \n READ_EXTERNAL_STORAGE");
                    }
                }
                break;
            default:
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //ImageFromCamera
        if (requestCode == TAKE_PICTURE) {
            if (resultCode == RESULT_OK) {
                try {
                    file = new File(IssueSubmissonActivity.this.getExternalFilesDir("temp"), imagePath);
                    String path = file.getPath();
                    Log.e("path",path);
                    File file1=new Compressor(IssueSubmissonActivity.this).compressToFile(new File(path));
                    String newPath = file1.getPath();
                    Log.e("newPath",newPath);
                    binding.textView62.setText(file1.getName());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        //ImageFromGallery
        else if (requestCode == PICK_GALLERY) {
            if (resultCode == RESULT_OK) {
                try {
                    if (data != null) {
                        Uri filePath = data.getData();
                        imagePath= GetPath.getPath(IssueSubmissonActivity.this,filePath);
                        Log.e("pathGallery", ""+ imagePath);
                        if (imagePath!=null) {
                            File file1 = new File(imagePath);
                            Log.e("fileName", "" + file1.getName());
                            binding.textView62.setText(file1.getName());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == 1 && resultCode == RESULT_CANCELED) {

            }
        }
    }


    private boolean checkValidation(){
        boolean ret=true;
        String strAdmissionNo = binding.etAddmissionno.getText().toString();
        String strUserName = binding.etName.getText().toString();
        String strUserClass = binding.etClass.getText().toString();
        String strUserSection = binding.etSection.getText().toString();

        if(TextUtils.isEmpty(strAdmissionNo)) {
            ret=false;
            Toast.makeText(IssueSubmissonActivity.this, "Please enter your admission number.", Toast.LENGTH_LONG).show();
        }
        else if(TextUtils.isEmpty(strUserName)) {
            ret=false;
            Toast.makeText(IssueSubmissonActivity.this, "Please enter your name.", Toast.LENGTH_LONG).show();
        }
        else if(TextUtils.isEmpty(strUserClass)) {
            ret=false;
            Toast.makeText(IssueSubmissonActivity.this,"Please enter class.", Toast.LENGTH_LONG).show();
        }
        else if(TextUtils.isEmpty(strUserSection)) {
            ret=false;
            Toast.makeText(IssueSubmissonActivity.this,"Please enter section.", Toast.LENGTH_LONG).show();
        }
        else if (binding.spinnerIssuetype.getSelectedItem().toString().trim() == "Select Issue Type"){
            ret=false;
            Toast.makeText(IssueSubmissonActivity.this,"Please enter issue type.", Toast.LENGTH_LONG).show();
        }
        return ret;
    }


    private void getIssueType() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<IssueTypeModel> call = service.getIssueType();

        call.enqueue(new Callback<IssueTypeModel>() {

            @Override
            public void onResponse(Call<IssueTypeModel> call, retrofit2.Response<IssueTypeModel> response) {
                if (response.isSuccessful()) {

                    IssueTypeArr = response.body().getData();
                    ArrayList<String> items = new ArrayList<>();
                    items.add("Select Issue Type");
                    for(int i= 0;i<IssueTypeArr.size();i++){
                        Log.e("dataaa",""+IssueTypeArr.get(i).getType());
                        items.add(IssueTypeArr.get(i).getType());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(IssueSubmissonActivity.this,
                            android.R.layout.simple_spinner_item,items);

                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spinnerIssuetype.setAdapter(adapter);
                } else {
                    Toast.makeText(IssueSubmissonActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<IssueTypeModel> call, Throwable t) {
                Toast.makeText(IssueSubmissonActivity.this,"failure",Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void getStudentInfo() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<StudentInfoModel> call = service.getStudentInfo(Integer.valueOf(binding.etAddmissionno.getText().toString()));

        Logg("urlapistudentinfo", Constants.URL_LV + "data?std_roll=" + binding.etAddmissionno.getText().toString());

        call.enqueue(new Callback<StudentInfoModel>() {

            @Override
            public void onResponse(Call<StudentInfoModel> call, retrofit2.Response<StudentInfoModel> response) {
                if (response.isSuccessful()) {
                    StudentInfoModel server_response = response.body();
                    if (server_response.getData()!=null) {
                        binding.etName.setText(server_response.getData().getStudentName());
                        binding.etSection.setText(server_response.getData().getStudentSection());
                        binding.etClass.setText(server_response.getData().getStudentClass());
                    } else {
                        binding.etName.setText("");
                        binding.etSection.setText("");
                        binding.etClass.setText("");
                    }
                } else {
                    Toast.makeText(IssueSubmissonActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StudentInfoModel> call, Throwable t) {
                Log.e("fail",t.getMessage());
            }
        });
    }


    private void getSumbitRequest() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        MultipartBody.Part requestImage = null;
        if(imagePath!=null){
            file = new File(imagePath);
            Log.e("Register",""+file.getName());
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            requestImage = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        }

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<PickUpRequestModel> call = service.submitRequest(id,binding.etAddmissionno.getText().toString(),binding.spinnerIssuetype.getSelectedItem().toString(),binding.etName.getText().toString(),
                binding.etSection.getText().toString(),binding.etClass.getText().toString(),imagePath,binding.etDescription.getText().toString());

        Logg("issueraised", Constants.URL_LV + "data?id=" + id +"data?std_roll=" + binding.etAddmissionno.getText().toString() + "data?issueType=" + binding.spinnerIssuetype.getSelectedItem().toString() +
                "data?name=" + binding.etName.getText().toString() + "data?section=" + binding.etSection.getText().toString() + "data?class=" + binding.etClass.getText().toString() +
                "data?image" + imagePath + "data?description=" + binding.etDescription.getText().toString());
        
        call.enqueue(new Callback<PickUpRequestModel>() {

            @Override
            public void onResponse(Call<PickUpRequestModel> call, retrofit2.Response<PickUpRequestModel> response) {
                if (response.isSuccessful()) {
                    startActivity(new Intent(IssueSubmissonActivity.this,ViewRequestActivity.class)
                            .putExtra("calledFromStaff",ViewRequestActivity.class.getSimpleName()));
                    Toast.makeText(IssueSubmissonActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(IssueSubmissonActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PickUpRequestModel> call, Throwable t) {
                Log.e("fail",t.getMessage());
            }
        });
    }


    public void internetconnection() {
        final Dialog dialog = new Dialog(IssueSubmissonActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getIssueType();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_submit:
                if (checkValidation()){
                    getSumbitRequest();
                }
                break;

            case R.id.button4:
                profileBottomLayout();
                break;

            case R.id.img_back:
                onBackPressed();
                break;
        }
    }
}