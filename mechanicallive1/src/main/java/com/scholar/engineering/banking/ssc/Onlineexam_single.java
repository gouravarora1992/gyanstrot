package com.scholar.engineering.banking.ssc;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.scholar.engineering.banking.ssc.adapter.Home_RecyclerViewAdapter2;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Onlineexam_single extends AppCompatActivity
{

	RecyclerView list;
	public static Home_RecyclerViewAdapter2 adapter;
	ArrayList<Home_getset> homedata;
	ProgressDialog progress;
	SwipeRefreshLayout swipeRefreshLayout;
	int index=0,postid=0;
	boolean prg=true;
	RelativeLayout footerlayout;
	TextView loadmore;
	ProgressWheel wheelbar;
	NetworkConnection nw;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.jobs_all);

		nw=new NetworkConnection(this);

		Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setTitle("Test");
		Utility.applyFontForToolbarTitle(toolbar,this);
		list=(RecyclerView)findViewById(R.id.recyclerView);
		LinearLayoutManager mLayoutManager = new LinearLayoutManager(Onlineexam_single.this);
		mLayoutManager.scrollToPositionWithOffset(0,0);
		list.setLayoutManager(mLayoutManager);
		list.setHasFixedSize(true);
		list.setNestedScrollingEnabled(false);

		postid=getIntent().getIntExtra("postid",0);

		swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh_layout);

		homedata=new ArrayList<>();

		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());

		swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				prg=false;
				getData();
			}
		});

		getData();

		footerlayout=(RelativeLayout)findViewById(R.id.footer_layout);
		loadmore=(TextView)findViewById(R.id.moreButton);
		wheelbar=(ProgressWheel)findViewById(R.id.progress_wheel);

	}



	public void getData() {

		if (!nw.isConnectingToInternet()) {
			swipeRefreshLayout.setRefreshing(false);
			internetconnection(0);
			return;
		}

		if (index == 0&prg){
			progress = ProgressDialog.show(Onlineexam_single.this, null, null, true);
		progress.setContentView(R.layout.progressdialog);
		progress.setCanceledOnTouchOutside(false);
		progress.setCancelable(false);
		progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		progress.show();
		}

		RequestQueue queue= Volley.newRequestQueue(this);
		JSONObject obj=new JSONObject();
		String url= Constants.URL+"newapi2_04/get_singletest.php?email="+ Constants.User_Email+"&id="+postid;

		Logg("checkversion_url",url);

		JsonObjectRequest json=new JsonObjectRequest(Method.POST, url, obj,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res) {
				// TODO Auto-generated method stub
				try {

					if(res.has("data")) {

						JSONArray jr = res.getJSONArray("data");

						if(index==0)
							homedata.clear();


						for (int i = 0; i < jr.length(); i++) {
							JSONObject ob = jr.getJSONObject(i);

//							String time = Utility.getDate(ob.getString("timestamp"));

							Home_getset object=new Home_getset();
							object.setId(ob.getInt("id"));
							object.setTimestamp(ob.getString("timestamp"));
							object.setLikecount(ob.getInt("likes"));
							object.setCommentcount(ob.getInt("comment"));
							object.setViewcount(ob.getInt("view"));
							object.setUid(ob.getString("uid"));
							object.setPosttype(ob.getString("posttype"));
							object.setGroupid(ob.getString("groupid"));
							object.setFieldtype(ob.getString("field_type"));
							object.setJsonfield(ob.getString("jsondata"));
							object.setLikestatus(ob.getString("likestatus"));
							object.setPosturl(ob.getString("posturl"));
							object.setPostdescription(ob.getString("post_description"));
							homedata.add(object);
						}

						//Logg("data.size", homedata.size() + "");
						adapter = new Home_RecyclerViewAdapter2(Onlineexam_single.this, homedata,"onlineexam_single");
						list.setAdapter(adapter);
					}

					if(progress!=null)
					if(progress.isShowing())
						progress.dismiss();

					swipeRefreshLayout.setRefreshing(false);
				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					//Logg("checkversion exp",e.getMessage());
					if(progress!=null)
					if(progress.isShowing())
						progress.dismiss();

					swipeRefreshLayout.setRefreshing(false);
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub

				//Logg("checkversion error", String.valueOf(arg0));
				if(progress!=null)
				if(progress.isShowing())
					progress.dismiss();

				swipeRefreshLayout.setRefreshing(false);
			}
		});
		queue.add(json);
	}

	public void internetconnection(final int i){

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.network_alert);
		dialog.show();

		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
		TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

		tv_tryagain.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.dismiss();
				swipeRefreshLayout.setRefreshing(true);
				getData();

			}
		});

		iv_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(item.getItemId()==android.R.id.home)
			finish();

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Logg("call_onDestroy()","Call onDestroy()");
		deleteCache(this);
	}


	@Override
	public void onBackPressed() {

		if(Constants.Check_Branch_IO){
			Intent in =new Intent(Onlineexam_single.this,HomeActivity.class);
			startActivity(in);
			finish();
		}
		else
			super.onBackPressed();
	}

	public void deleteCache(Context context) {
		try {
			File dir = context.getCacheDir();
			deleteDir(dir);
		} catch (Exception e) {}
	}

	public boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
			return dir.delete();
		} else if(dir!= null && dir.isFile()) {
			return dir.delete();
		} else {
			return false;
		}
	}

}
	

