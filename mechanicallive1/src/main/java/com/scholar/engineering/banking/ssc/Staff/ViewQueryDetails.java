package com.scholar.engineering.banking.ssc.Staff;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ViewQueryModel;
import com.scholar.engineering.banking.ssc.Staff.adapter.ViewQueryDetailsAdapter;

import org.json.JSONArray;
import org.json.JSONException;

public class ViewQueryDetails extends AppCompatActivity implements View.OnClickListener {

    ViewQueryModel.Datum viewQuery;
    RecyclerView recylerViewQueryDetails;
    ImageView img_back;
    JSONArray itemArr,costArr,purposeArr,quantityArr,purposeImageArr;
    String itemName, quantity, cost, purpose, purposeImage;
    TextView txt_department, txt_department_head, txt_person_requires, txt_request_date, txt_person_approved, txt_approved_date, txt_admin_approved_date, txt_justification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_query_details);

        recylerViewQueryDetails = findViewById(R.id.recyclerView2);
        txt_department = findViewById(R.id.txt_department);
        txt_department_head = findViewById(R.id.txt_department_head);
        txt_person_requires = findViewById(R.id.txt_person_requires);
        txt_request_date = findViewById(R.id.txt_request_date);
        txt_person_approved = findViewById(R.id.txt_person_approved);
        txt_approved_date = findViewById(R.id.txt_approved_date);
        txt_admin_approved_date = findViewById(R.id.txt_admin_approved_date);
        txt_justification = findViewById(R.id.txt_justification);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);

        Intent i = this.getIntent();
        Bundle bundle = i.getExtras();

        viewQuery = (ViewQueryModel.Datum) getIntent().getSerializableExtra("data");
        String dept = viewQuery.getDepartment();
        String deptHead = viewQuery.getDepartmentHead();
        String justification = viewQuery.getJustification();
        String approvalDate = String.valueOf(viewQuery.getApprovalDate());
        String person = String.valueOf(viewQuery.getApprovedPerson());
        String adminApprovedDate = String.valueOf(viewQuery.getApprovedPerson());
        itemName = viewQuery.getItemName();
        quantity = viewQuery.getQuantity();
        purpose = viewQuery.getPurpose();
        purposeImage = viewQuery.getPurposeImage();
        cost = viewQuery.getCost();

        txt_department.setText(dept);
        txt_department_head.setText(deptHead);
        txt_justification.setText(justification);
        txt_person_approved.setText(person);
        txt_approved_date.setText(approvalDate);
        txt_admin_approved_date.setText(adminApprovedDate);

        Log.e("PurposeImage",viewQuery.getPurposeImage());
        Log.e("ItemName",viewQuery.getItemName());
        Log.e("Quantity",viewQuery.getQuantity());
        Log.e("cost",viewQuery.getCost());
        Log.e("Purpose",viewQuery.getPurpose());

        try {
            purposeImageArr = new JSONArray(viewQuery.getPurposeImage());
            itemArr = new JSONArray(viewQuery.getItemName());
            quantityArr = new JSONArray(viewQuery.getQuantity());
            costArr = new JSONArray(viewQuery.getCost());
            purposeArr = new JSONArray(viewQuery.getPurpose());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        setUpData();
    }

    private void setUpData() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ViewQueryDetails.this);
        recylerViewQueryDetails.setLayoutManager(linearLayoutManager);
        ViewQueryDetailsAdapter viewQueryDetailsAdapter = new ViewQueryDetailsAdapter(ViewQueryDetails.this, purposeImageArr, itemArr,
                quantityArr, costArr, purposeArr);
        recylerViewQueryDetails.setAdapter(viewQueryDetailsAdapter);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back:
                onBackPressed();
                break;
        }
    }
}