package com.scholar.engineering.banking.ssc.newScreens.QueryForms

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.RetryPolicy
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.scholar.engineering.banking.ssc.R
import com.scholar.engineering.banking.ssc.newScreens.NotesPdfGetSet
import com.scholar.engineering.banking.ssc.newScreens.adapters.QueryResponselistAdapter
import com.scholar.engineering.banking.ssc.newScreens.adapters.QuerylistAdapter
import com.scholar.engineering.banking.ssc.utils.CommonUtils
import com.scholar.engineering.banking.ssc.utils.Constants
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class QueryResponseDetails : AppCompatActivity() {
    var toolbar: Toolbar? = null
    var toolbartitle: TextView? = null

    lateinit var context: Context
    lateinit var sharedPreferences: UserSharedPreferences
    var pd: ProgressDialog? = null

    lateinit var recyclerView: RecyclerView
    lateinit var itemVideos: ArrayList<QueryGetSet>
    lateinit var adapter: QueryResponselistAdapter

    var queryid:String=""
    lateinit var feedback: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_query_response_details)

        context = this
        toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        toolbartitle = toolbar?.findViewById(R.id.headertextid) as TextView
        toolbartitle?.setText("Query Response")


        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        toolbar!!.setNavigationOnClickListener(View.OnClickListener {
            finish()
        })


        sharedPreferences = UserSharedPreferences.getInstance(this)
        pd = ProgressDialog(this)
        pd?.setMessage("loading")
        pd?.setCancelable(false)


        var data=intent.getStringExtra("postdata")
        val obj=JSONObject(data)
        queryid=obj.getString("id")
        Log.e("queryid",queryid+"0000")
        var ticketno = findViewById(R.id.ticketno) as TextView
        ticketno.setText("Ticket No: "+obj.getString("ticketno"))
        var issue = findViewById(R.id.issue) as TextView
        issue.setText("Issue: "+obj.getString("issue"))
        var issuedetail = findViewById(R.id.issuedetail) as TextView
        issuedetail.setText("Details: "+obj.getString("details"))
        var status = findViewById(R.id.status) as TextView
        var feedback = findViewById(R.id.feedback) as Button
        if(obj.getString("status")=="2") {
            status.setText("Status: Complete")
            feedback.visibility=View.VISIBLE
        }
        else
        {
            status.setText("Status: Pending")
            feedback.visibility=View.GONE
        }

        itemVideos = ArrayList()
        recyclerView = findViewById(R.id.recyclerviewreponse)
        recyclerView.setHasFixedSize(true)
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = linearLayoutManager
        getItemList()


        feedback = findViewById(R.id.feedback)
        feedback.setOnClickListener {

            val myRoundedBottomSheet = Feedbackform_BottomSheetDialog()
            var bundle = Bundle()
            bundle.putString("id",obj.getString("id"))
            myRoundedBottomSheet.arguments = (bundle)
            myRoundedBottomSheet.show(supportFragmentManager, myRoundedBottomSheet.tag)
        }

    }

    private fun getItemList() {
        val requestQueue = Volley.newRequestQueue(this)
        val stringRequest: StringRequest = object : StringRequest(Method.POST, Constants.URL_LV+"supportresponse",
                Response.Listener { response ->
                    Log.e("response",response)
                    try {
                        val dataobject = JSONObject(response)
                        if(dataobject.getString("status").equals("true"))
                        {
                            val ara = dataobject.getJSONArray("data")
                            for (i in 0 until ara.length()) {
                                val obj = ara.getJSONObject(i)
                                itemVideos.add(QueryGetSet(obj.getString("response"),obj.getString("created_at"),
                                        obj.getString("created_at"),
                                        obj.getString("response"),
                                        obj.getString("created_at"),
                                obj.toString()))
                            }
                            adapter = QueryResponselistAdapter(context, itemVideos)
                            recyclerView.adapter = adapter
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error ->
                    CommonUtils.volleyerror(error)
                }
        ) {
            override fun getParams(): Map<String, String> {
                val params: MutableMap<String, String> = HashMap()
                params["id"] = queryid
                Log.e("get_assignments", params.toString() + "")
                return params
            }
        }
        stringRequest.setShouldCache(false)

        val socketTimeout = 0
        val policy: RetryPolicy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.setRetryPolicy(policy)

        requestQueue.add(stringRequest)
    }

}