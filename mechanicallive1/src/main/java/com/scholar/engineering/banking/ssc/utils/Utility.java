package com.scholar.engineering.banking.ssc.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;

import androidx.core.app.ActivityCompat;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;

import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.numberprogressbar.NumberProgressBar;
import com.jsibbold.zoomage.ZoomageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.Job_details;
import com.scholar.engineering.banking.ssc.News_Webview;
import com.scholar.engineering.banking.ssc.Play_new;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Transaction;
import com.scholar.engineering.banking.ssc.Webview_internal_link;
import com.scholar.engineering.banking.ssc.Webview_plus_html;
import com.scholar.engineering.banking.ssc.fragment.Home_fragment;
import com.scholar.engineering.banking.ssc.fragment.Premium_Test_Fragment;
import com.scholar.engineering.banking.ssc.getset.Home_getset;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.TextUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.SharingHelper;
import io.branch.referral.util.ContentMetadata;
import io.branch.referral.util.LinkProperties;
import io.branch.referral.util.ShareSheetStyle;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;


/**
 * Created by surender on 22-03-2016.
 */

public class Utility {
    // convert from bitmap to byte array

    public static TextView progtext, tv_like, tv_liketext, tv_comment, tv_share;
    public static NumberProgressBar bar;
    public static String URL, url, test_id;
    public static JSONObject json;
    public static Context c;
    public static NetworkConnection nw;
    public static String testname = "";
    public static int payment = 0;
    public static boolean b = false;
    public static ImageView iv_like;
    public static AppCompatImageView job_image;
    public static int likecount = 0;
    public static Home_getset data;
    public static Dialog dialog;
    public static String checkclass = "";
    public static ProgressDialog progress;
    public static ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    public static DisplayImageOptions options;
    public static ImageLoader imageLoader;

    public static boolean checkGPSStatus(Context mContext) {
        LocationManager locationManager = null;
        boolean gps_enabled = true;
        if (locationManager == null) {
            locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        }
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            gps_enabled = true;
        else
            gps_enabled = false;
        Logg("gps_enabled", ":2:" + gps_enabled);
        return gps_enabled;
    }

    public static boolean checkGPSPermission(Context mContext) {
        boolean gps_enabled = true;
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            gps_enabled = true;
        else
            gps_enabled = false;
        return gps_enabled;
    }

    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }
    // convert from byte array to bitmap

    public static Bitmap getPhoto(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    public static int getToolbarHeight(Context context) {
        final TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                new int[]{R.attr.actionBarSize});
        int toolbarHeight = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        return toolbarHeight;
    }

    public static int getTabsHeight(Context context) {
        return (int) context.getResources().getDimension(R.dimen.tab_height);
    }

    public static String getDate(String OurDate) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(OurDate);
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate = dateFormatter.format(value);
            //Log.d("OurDate", OurDate);
        } catch (Exception e) {
            //Logg("exception_time","e",e);
//            OurDate = "00-00-0000 00:00";
        }
        //Logg("date_time",OurDate);


        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        Logg("current_time", formattedDate);
        return timediff(OurDate, formattedDate);

    }

    public static String timediff(String prevdate, String curdate) {

        Date d11 = null;
        Date d2 = null;

        String[] dd1 = prevdate.split(" ");
        String[] date1 = dd1[0].split("-");

        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");

        try {
            d11 = format.parse(prevdate);
            d2 = format.parse(curdate);

            //in milliseconds
            long diff = d2.getTime() - d11.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            String pd = dd1[0];

            String cd;//=cal.get(Calendar.YEAR)+"-"+(cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.DAY_OF_MONTH);
            String[] cdarr = curdate.split(" ");
            cd = cdarr[0];
            String[] date2 = cd.split("-");

            int years = 0, months = 0, days = 0;

            years = Integer.parseInt(date2[0]) - Integer.parseInt(date1[0]);
            months = Integer.parseInt(date2[1]) - Integer.parseInt(date1[1]);
            days = Integer.parseInt(date2[2]) - Integer.parseInt(date1[2]);

            years = (months < 0) ? years - 1 : years;
            months = (months < 0) ? 12 + (months) : months;
            months = (days < 0) ? months - 1 : months;
            days = (days < 0) ? 30 + days : days;

            //	//Logg("di time= "," hrs= "+diffHours+" min= "+diffMinutes+" "+" years=  "+years+" month=  "+months+" days= "+days+" pd= "+pd+" cd= "+cd);

            if (years > 0) {
                if (years < 2)
                    return (String.valueOf(years) + " Year ago");
                else
                    return (String.valueOf(years) + " Years ago");
            } else if (months > 0 & years < 1) {
                if (months < 2)
                    return (String.valueOf(months) + " Month ago");
                else
                    return (String.valueOf(months) + " Months ago");
                //newsdateedit.putString("postdate"+i,String.valueOf(months)+"Months ago");
            } else if (days > 0 & months < 1) {
                if (days < 2)
                    return (String.valueOf(days) + " Day ago");
                else
                    return (String.valueOf(days) + " Days ago");
                //newsdateedit.putString("postdate"+i,String.valueOf(months)+" Days ago");
            } else if (diffHours > 0 & days < 1) {
                if (diffHours < 2)
                    return (String.valueOf(diffHours) + " Hour ago");
                else
                    return (String.valueOf(diffHours) + " Hours ago");
                //newsdateedit.putString("postdate"+i,String.valueOf(months)+" Hours ago");
            } else if (diffMinutes > 0 & diffHours < 1) {
                if (diffMinutes < 2)
                    return (String.valueOf(diffMinutes) + " Minute ago");
                else
                    return (String.valueOf(diffMinutes) + " Minutes ago");
                //newsdateedit.putString("postdate"+i,String.valueOf(months)+" Minutes ago");
            } else if (diffSeconds > 0 & diffMinutes < 1) {
                return ("0 Minute ago");
                //newsdateedit.putString("postdate"+i,"0 Minutes ago");
            }

        } catch (Exception e) {
        }
        return ("1 Minute ago");
    }

    public static void jobDetails(Context context, final Home_getset obj) {
        JSONObject jsonobj = null;
        c = context;
        data = obj;
        Intent in = new Intent(c, Job_details.class);
        c.startActivity(in);

    }

    public static int checkFile(String url) {

        Logg("inside check file", url);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        HttpClient client = new DefaultHttpClient();
        HttpHead headMethod = new HttpHead(url);
        HttpResponse response = null;

        try {
            response = client.execute(headMethod);

            //Logg("response_code",response+" response");
            Logg("response_code", response.getStatusLine().getStatusCode() + " response");
            //Logg("response_code",response.getStatusLine().getReasonPhrase()+" response");

            return response.getStatusLine().getStatusCode();

        } catch (IOException e) {
            return 0;
        }

    }

    public static void startTest(final Context c, final String test_id1) {
        JSONObject obj;

        Utility.c = c;

        test_id = test_id1;

        // Testname=Testname1;

        Constants.ImageDirectory = test_id.replaceAll(" ", "");
        Log.e("ImageDirectory>>>>>",Constants.ImageDirectory);

        if (dirChecker(Constants.ImageDirectory)) {
            Intent in = new Intent(Utility.c, Play_new.class);
            in.putExtra("testid", test_id);
            in.putExtra("type", "1");
            Utility.c.startActivity(in);
            //((Activity)c).finish();
        } else
            downloadpdf(c, "");

//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(c);
//        alertDialogBuilder.setMessage("Start Test");
//        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
////                Intent in = new Intent(c,Generalinst.class);
////                c.startActivity(in);
//                if(dirChecker(Constants.ImageDirectory)){
//                    Intent in = new Intent(Utility.c, Play_new.class);
//                    in.putExtra("testid",test_id);
//                    Utility.c.startActivity(in);
//                    ((Activity)c).finish();
//                }
//                else
//                downloadpdf(c,"");
//
//                dialog.dismiss();
//            }
//        });
//
//        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });
//        alertDialogBuilder.setCancelable(false);
//        alertDialogBuilder.show();


    }

    public static void downloadpdf(final Context c, String url1) {

        url = Constants.URL_Image + "admin/tests_images/" + Constants.ImageDirectory + ".zip";

        Logg("downloadpdf","call "+url);

        dialog = new Dialog(c);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.test_download);

        // inflate and adjust layout
        // dialog.setContentView(layout1);

        bar = (NumberProgressBar) dialog.findViewById(R.id.numberbar1);
        //adView=(AdView)dialog.findViewById(R.id.adView);
        dialog.show();
        dialog.setCancelable(true);

        //progtext=(TextView)dialog.findViewById(R.id.progresstextid);

        URL = url;

        String[] urlarr = URL.split("/");
        int len = urlarr.length;
        //Toast.makeText(c,"len "+len,200).show();
        URL = urlarr[len - 1];

        Logg("urlllll", URL);
        new DownloadFile().execute(url);

    }

    public static class DownloadFile extends AsyncTask<String, Integer, String> {

        String filepath;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() { }
            }, 5000);
        }

        @Override
        protected String doInBackground(String... Url) {
            try {
                java.net.URL url = new URL(Url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();

                // Detect the file lenghth
                int fileLength = connection.getContentLength();

                // Locate storage location
                filepath = Environment.getExternalStorageDirectory().getAbsolutePath();
                //.getPath();

                Logg("filepathfilepath",filepath+" , "+URL);

                File myDir = new File(filepath + "/GyanStrot/");
                myDir.mkdirs();
                File file = new File(myDir, URL);
                Log.e("file",""+file);
                Log.e("filePath",file.getAbsolutePath());
                Logg("filepathfilepath2",filepath+" , "+URL);
                if (file.exists()) {
                    Logg("filepathfilepath3",filepath+" , "+URL);
//                     file.delete();
                } else {
                    // Download the file
                    Logg("filepathfilepath4",filepath+" , "+URL);
                    InputStream input = new BufferedInputStream(url.openStream());

                    // Save the downloaded file

                    OutputStream output = new FileOutputStream(file);

                    byte data[] = new byte[1024];
                    long total = 0;
                    int count;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        // Publish the progress
                        publishProgress((int) (total * 100 / fileLength));
                        output.write(data, 0, count);
                    }
                    // Close connection
                    output.flush();
                    output.close();
                    input.close();
                }
            } catch (Exception e) {
              Log.e("exception","",e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            boolean b = dirChecker(Constants.ImageDirectory);
            //unpackZip(filepath + "/Topper's Club/",Constants.ImageDirectory+".zip");
            Logg("filepathfilepath6",b+"");
            if (b) {

                if (checkclass.equalsIgnoreCase("homefragment")) {
                    if (Home_fragment.adapter != null)
                        Home_fragment.adapter.notifyDataSetChanged();
                    Utility.startTest(c, test_id);
                } else if (checkclass.equalsIgnoreCase("premiumtest_all")) {
                    if (Premium_Test_Fragment.adapter != null)
                        Premium_Test_Fragment.adapter.notifyDataSetChanged();
                    Utility.startTest(c, test_id);
                }
//                else if(checkclass.equalsIgnoreCase("onlinetest")) {
//                    if(Onlineexam.adapter!=null)
//                    Onlineexam.adapter.notifyDataSetChanged();
//                }
//                else if(checkclass.equalsIgnoreCase("onlineexamfilter")) {
//                    if(OnlineExam_filter.adapter!=null)
//                        OnlineExam_filter.adapter.notifyDataSetChanged();
//                }
                else if (checkclass.equalsIgnoreCase("result")) {
                    Intent in = new Intent(Utility.c, Play_new.class);
                    in.putExtra("testid", test_id);
                    in.putExtra("type", "2");
                    Utility.c.startActivity(in);
                    //((AppCompatActivity)c).finish();
                }
                else
                {
                    Logg("filepathfilepath5",filepath+" , "+URL);
                }

                dialog.dismiss();

            }

        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // Update the progress dialog
            bar.setProgress(progress[0]);
            Logg("progress", progress[0].toString());
            // progtext.setText(String.valueOf(progress[0])+"%");

        }
    }

    public static String _zipFile;
    public static String _location;

    public static boolean dirChecker(String dir) {
        _zipFile = Environment.getExternalStorageDirectory() + "/GyanStrot/" + dir + ".zip"; //your zip file location
        _location = Environment.getExternalStorageDirectory() + "/GyanStrot/"; // destination folder location

        File f = new File(_location + dir);

        if (!f.isDirectory()) {
            f.mkdirs();
        }

        return unzip();
    }

    public static boolean checkdownload(String dir) {

        dir = dir.replaceAll(" ", "");

        _location = Environment.getExternalStorageDirectory() + "/GyanStrot/"; // destination folder location

        File f = new File(_location + dir + ".zip");

        if (!f.exists()) {
            //Logg("file://","file not exist");
            return false;
            //f.mkdirs();
        } else {
            //Logg("file://","file exist");
            return true;
        }
    }

    public static boolean unzip() {
        try {
            FileInputStream fin = new FileInputStream(_zipFile);
            ZipInputStream zin = new ZipInputStream(fin);
            ZipEntry ze = null;
            while ((ze = zin.getNextEntry()) != null) {
                Log.v("Decompress", "Unzipping " + ze.getName());

                if (ze.isDirectory()) {
                    dirChecker(ze.getName());
                } else {
                    FileOutputStream fout = new FileOutputStream(_location + ze.getName());
                    BufferedOutputStream bufout = new BufferedOutputStream(fout);
                    byte[] buffer = new byte[1024];
                    int read = 0;
                    while ((read = zin.read(buffer)) != -1) {
                        bufout.write(buffer, 0, read);
                    }

                    bufout.close();

                    zin.closeEntry();
                    fout.close();
                }

            }
            zin.close();


            Log.d("Unzip", "Unzipping complete. path :  " + _location);
        } catch (Exception e) {
            //Logg("Decompress", "unzip", e);

            Log.d("Unzip", "Unzipping failed");
            return false;
        }
        return true;
    }

    public static boolean checkPayment(Context c, JSONObject jsonobj) {

        boolean b = false;
        try {
            if (jsonobj.getBoolean("user_payment")) {
                b = true;
            } else {
                if (jsonobj.getString("payment").equalsIgnoreCase("free")) {
                    b = true;
                } else {
                    b = false;
                }
            }
        } catch (JSONException e) {

        }

        return b;
    }

    public static void payPayment(final Context c, JSONObject jsonobj, int testid) {

        test_id = String.valueOf(testid);

        try {

            payment = Integer.valueOf(jsonobj.getString("payment"));
            testname = jsonobj.getString("testname");

        } catch (JSONException e) {

        }

        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) c).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        final Dialog dialog = new Dialog(c);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.online_exam_dialog);
        dialog.show();

        TextView tv_wallet = (TextView) dialog.findViewById(R.id.walletpointid);
        TextView tv_online = (TextView) dialog.findViewById(R.id.onlineid);
        TextView tv_testname = (TextView) dialog.findViewById(R.id.testnameid);
        TextView tv_exam_fee = (TextView) dialog.findViewById(R.id.examfeesid);
        LinearLayout layout = (LinearLayout) dialog.findViewById(R.id.layoutid);

        layout.setMinimumWidth(width);

        if (payment < 10)
            tv_wallet.setVisibility(View.GONE);

        tv_testname.setText(testname + " is Paid");
        tv_exam_fee.setText("Pay Rs. " + payment + " using");
        tv_wallet.setText("Wallet (Left Rs. " + Constants.Point + ")");

        tv_wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (payment <= Integer.valueOf(Constants.Point)) {
                    volleyinsertpaymentinfo(c);
                    dialog.cancel();
                } else
                    Toast.makeText(c, "No enough amount in wallet, Please select Online Banking", Toast.LENGTH_LONG).show();
            }
        });

        tv_online.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Transaction t = new Transaction(c, "2", test_id, payment);
                t.PAYMENT_REQUEST(c);
//             Intent in=new Intent(c,Transaction.class);
//                in.putExtra("postid",test_id);
//                in.putExtra("field_type","2");
//                in.putExtra("amount",payment);
//                c.startActivity(in);

                dialog.cancel();
            }
        });

    }

    public static void volleyinsertpaymentinfo(final Context c) {
        final ProgressDialog dialog = new ProgressDialog(c, R.style.AppTheme);
        dialog.setMessage("Loading..Please wait.");
        dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();

        RequestQueue queue = Volley.newRequestQueue(c);
        JSONObject json = new JSONObject();

        String url = Constants.URL + "newapi2_04/payment_by_wallet.php?amount=" + payment + "&email=" +
                Constants.User_Email + "&id=" + test_id + "&field_type=2";
        url = url.replaceAll(" ", "%20");

        JsonObjectRequest jsonreq = new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                //Logg("res",String.valueOf(res));
                // TODO Auto-generated method stub
                String s;

                try {
                    if (res.getString("status").equals("success")) {

                        if (res.getString("response").equalsIgnoreCase("payment success")) {
                            int pt = Integer.parseInt(res.getString("amount_left"));
                            Constants.Point = String.valueOf(pt);
                            Toast.makeText(c, "Success", Toast.LENGTH_LONG).show();
                        }

                        if (Utility.checkFile(Constants.URL + "admin/tests_images/" + test_id.replaceAll(" ", "") + ".zip") == 200) {
                            Utility.startTest(c, test_id);
                        } else {
                            Intent in = new Intent(c, Play_new.class);
                            in.putExtra("testid", test_id);
                            in.putExtra("type", "3");
                            c.startActivity(in);
                        }
//                        alert(des,pos);
                    } else if (res.getString("response").equalsIgnoreCase("amount insufficient")) {
                        Toast.makeText(c, "Insufficient Balance", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(c, "Can't Payment", Toast.LENGTH_SHORT).show();
                    }

                    dialog.dismiss();
                } catch (JSONException e) {
                    dialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg) {
                //Logg("error", String.valueOf(arg));
                dialog.dismiss();
                Toast.makeText(c, "Network Problem", Toast.LENGTH_SHORT).show();
            }


        });

        queue.add(jsonreq);
    }

    public static void payPayment_Post(final Context c) {

        test_id = String.valueOf(data.getId());

        try {

            json = new JSONObject(data.getJsonfield());

            payment = Integer.valueOf(json.getString("payment"));
//            testname=jsonobj.getString("testname");

        } catch (JSONException e) {

        }

        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) c).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        final Dialog dialog = new Dialog(c);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.online_exam_dialog);
        dialog.show();

        TextView tv_wallet = (TextView) dialog.findViewById(R.id.walletpointid);
        final TextView tv_online = (TextView) dialog.findViewById(R.id.onlineid);
        TextView tv_testname = (TextView) dialog.findViewById(R.id.testnameid);
        TextView tv_exam_fee = (TextView) dialog.findViewById(R.id.examfeesid);
        LinearLayout layout = (LinearLayout) dialog.findViewById(R.id.layoutid);

        layout.setMinimumWidth(width);


        tv_testname.setText("This Post is Paid");
        tv_exam_fee.setText("Pay Rs. " + payment + " using");
        tv_wallet.setText("Wallet (Left Rs. " + Constants.Point + ")");

        tv_wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (payment <= Integer.valueOf(Constants.Point)) {
                    payPayment_for_post(c);
                    dialog.cancel();
                } else {
                    Toast.makeText(c, "No enough amount in wallet, Please select Online Banking", Toast.LENGTH_LONG).show();
                }
            }
        });

        tv_online.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    JSONObject jsondata = new JSONObject();
                    jsondata.put("amount", payment);
                    jsondata.put("payment by", "online");

                    Transaction t = new Transaction(c, "1", test_id, payment);
                    t.PAYMENT_REQUEST(c);

//                    Intent in=new Intent(c,Transaction.class);
//                    in.putExtra("postid",test_id);
//                    in.putExtra("field_type","1");
//                    in.putExtra("amount",payment);
//                    c.startActivity(in);

                } catch (JSONException e) {

                }

                dialog.cancel();
            }
        });

    }

    public static void payPayment_for_post(final Context c) {
        b = false;

        final ProgressDialog dialog = new ProgressDialog(c, R.style.AppTheme);
        dialog.setMessage("Loading..Please wait.");
        dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();

        RequestQueue queue = Volley.newRequestQueue(c);
        JSONObject json = new JSONObject();

        String url = Constants.URL + "newapi2_04/payment_by_wallet.php?amount=" + payment + "&email=" +
                Constants.User_Email + "&id=" + test_id + "&field_type=1";
        url = url.replaceAll(" ", "%20");

        JsonObjectRequest jsonreq = new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                //Logg("res",String.valueOf(res));
                // TODO Auto-generated method stub
                String s;

                try {
                    if (res.getString("status").equals("success")) {
                        openPost(c);
                    } else if (res.getString("response").equalsIgnoreCase("amount insufficient")) {
                        Toast.makeText(c, "Insufficient Balance", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(c, "Can't Payment", Toast.LENGTH_SHORT).show();

                    }

                    dialog.dismiss();
                } catch (JSONException e) {
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg) {
                //Logg("error", String.valueOf(arg));
                dialog.dismiss();

                Toast.makeText(c, "Network Problem", Toast.LENGTH_SHORT).show();
            }


        });

        queue.add(jsonreq);

    }

    public static void openPost(Context c) {

        try {

            Logg("openlink", "hello");
            json = new JSONObject(data.getJsonfield());
            if (json.has("post_type")) {
                if (json.getString("post_type").equalsIgnoreCase("wordpress")) {

                    Intent in = new Intent(c, Webview_internal_link.class);
                    in.putExtra("postid", data.getId());

                    //    in.putExtra("posturl", data.getPosturl());
                    c.startActivity(in);

                    if (Constants.Check_adapter.equalsIgnoreCase("News_related_post_adapter")) {
                        Constants.Check_adapter = "";
                        ((Activity) c).finish();
                    }
                } else if (json.getString("post_type").equalsIgnoreCase("news")) {
                    Logg("news", "news");

                    Intent in = new Intent(c, Webview_plus_html.class);
                    in.putExtra("postid", data.getId());
                    c.startActivity(in);

                    if (Constants.Check_adapter.equalsIgnoreCase("News_related_post_adapter")) {
                        Constants.Check_adapter = "";
                        ((Activity) c).finish();
                    }
                } else {
                    Intent in = new Intent(c, News_Webview.class);
                    in.putExtra("postid", data.getId());
                    c.startActivity(in);

                    if (Constants.Check_adapter.equalsIgnoreCase("News_related_post_adapter")) {
                        Constants.Check_adapter = "";
                        ((Activity) c).finish();
                    }
                }
            } else {
                if (data.getPosturl().contains("hellotopper.com") | data.getPosturl().contains("sandeepchaudhary.in")) {

                    //Logg("sandeepchaudhary.in", "sandeepchaudhary.in");

                    Intent in = new Intent(c, Webview_internal_link.class);
                    in.putExtra("postid", data.getId());

                    //    in.putExtra("posturl", data.getPosturl());
                    c.startActivity(in);

                    if (Constants.Check_adapter.equalsIgnoreCase("News_related_post_adapter")) {
                        Constants.Check_adapter = "";
                        ((Activity) c).finish();
                    }

                } else if (data.getPosturl().equalsIgnoreCase("news")) {

                    //Logg("webview+html", "webview+html");
                    Logg("news", "news");
                    Intent in = new Intent(c, Webview_plus_html.class);
                    in.putExtra("postid", data.getId());
                    c.startActivity(in);

                    if (Constants.Check_adapter.equalsIgnoreCase("News_related_post_adapter")) {
                        Constants.Check_adapter = "";
                        ((Activity) c).finish();
                    }

                } else {
                    //Logg("external_link", "external_link");
                    Intent in = new Intent(c, News_Webview.class);
                    in.putExtra("postid", data.getId());
                    c.startActivity(in);

                    if (Constants.Check_adapter.equalsIgnoreCase("News_related_post_adapter")) {
                        Constants.Check_adapter = "";
                        ((Activity) c).finish();
                    }
                }
            }

        } catch (JSONException e) {

        }
    }

    public static void shareIntent(Context c, JSONObject object) {
        String title = "", description = "", image = "";
        try {
            title = object.getString("title");
            description = object.getString("description");
            image = object.getString("image");
            Logg("desc", description);

        } catch (JSONException e) {

        }

//        BranchUniversalObject branchUniversalObject = new BranchUniversalObject()
//                .setCanonicalIdentifier("item/12345")
//                .setTitle(title)
//                .setContentDescription("Hello Topper")
//                .setContentImageUrl(image)
//                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
//                .addContentMetadata((new ContentMetadata().addCustomMetadata("jsondata", object+"")));
//                .addContentMetadata("property2", "red");

        BranchUniversalObject branchUniversalObject = new BranchUniversalObject()
                .setCanonicalIdentifier("content/12345")
                .setTitle(title)
                .setContentDescription("Gyan Strot")
                .setContentImageUrl(image)
                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
//                .setLocalIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .setContentMetadata(new ContentMetadata().addCustomMetadata("jsondata", object + ""));


        LinkProperties linkProperties = new LinkProperties()
                .setChannel("facebook")
                .setFeature("sharing")
                .addControlParameter("$desktop_url", "")
                .addControlParameter("$android_url", "https://play.google.com/store/apps/details?id=com.schoolapp.gyanstrot");

        branchUniversalObject.generateShortUrl(c, linkProperties, new Branch.BranchLinkCreateListener() {
            @Override
            public void onLinkCreate(String url, BranchError error) {
                if (error == null) {
                    //Logg("MyApp", "got my Branch link to share: " + url);
                }
            }
        });

        ShareSheetStyle shareSheetStyle = new ShareSheetStyle(c, "Check this out!", description)
                .setCopyUrlStyle(c.getResources().getDrawable(android.R.drawable.ic_menu_send), "Copy", "")
                .setMoreOptionStyle(c.getResources().getDrawable(android.R.drawable.ic_menu_search), "")
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.FACEBOOK)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.EMAIL)
                .setAsFullWidthStyle(true)
                .setSharingTitle("Share With");

        branchUniversalObject.showShareSheet((Activity) c, linkProperties, shareSheetStyle,
                new Branch.BranchLinkShareListener() {
                    @Override
                    public void onShareLinkDialogLaunched() {
                    }

                    @Override
                    public void onShareLinkDialogDismissed() {
                    }

                    @Override
                    public void onLinkShareResponse(String sharedLink, String sharedChannel, BranchError error) {
                    }

                    @Override
                    public void onChannelSelected(String channelName) {
                    }
                });

        // branchUniversalObject.listOnGoogleSearch(c);

//        String PACKAGE_NAME = c.getPackageName();
//        Intent share = new Intent(android.content.Intent.ACTION_SEND);
//        share.setType("text/plain");
//        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
//        share.putExtra(Intent.EXTRA_SUBJECT, Constants.APPNAME);
//        //                    share.putExtra(Intent.EXTRA_TEXT, data.get(position).getGroupname() +" Post "+data.get(position).getComment()+" Vacancy "+
//        //                            data.get(position).getName()+ ", For more detail download " + Constants.APPNAME + " app" + " " + "play.google.com/store/apps/details?id=" + PACKAGE_NAME);
//        c.startActivity(Intent.createChooser(share, "Share link!"));

    }

    public static String scaleDown(Bitmap realImage, float maxImageSize, boolean filter) {

//        float ratio = Math.min(
//                (float) maxImageSize / realImage.getWidth(),
//                (float) maxImageSize / realImage.getHeight());
//        int width = Math.round((float) ratio * realImage.getWidth());
//        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, 1000,
                1000, filter);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        newBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        //st_base46 = Base64.encodeToString(byteArray, Base64.DEFAULT);

        return Base64.encodeToString(byteArray, Base64.DEFAULT);

    }

    public static File createTemporaryFile(String part, String ext) throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    public static String grabImage(Context c, ImageView imageView, Uri selectedImage) {
        c.getContentResolver().notifyChange(selectedImage, null);
        ContentResolver cr = c.getContentResolver();
        Bitmap bitmap;

        try {
            bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, selectedImage);
            //Logg("bitmap_h_w",bitmap.getHeight()+" "+bitmap.getWidth()+"");
            imageView.setImageBitmap(bitmap);

            return scaleDown(bitmap, 0, true);
        } catch (Exception e) {

            return "";
        }
    }

    public static void blockuser(final Context c, final String msg) {

        Utility.c = c;

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(c);
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.show();

    }

    public static boolean checkURL(CharSequence input) {
        if (TextUtils.isEmpty(input)) {
            return false;
        }
        Pattern URL_PATTERN = Patterns.WEB_URL;
        boolean isURL = URL_PATTERN.matcher(input).matches();
        if (!isURL) {
            String urlString = input + "";
            if (URLUtil.isNetworkUrl(urlString)) {
                try {
                    new URL(urlString);
                    isURL = true;
                } catch (Exception e) {
                }
            }
        }
        return isURL;
    }

    public static void Payment_success(final Context c) {

        final Dialog dialog = new Dialog(c);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.payment_success_dialog);
        dialog.show();

        TextView tv_goback = (TextView) dialog.findViewById(R.id.tv_goback);
        LinearLayout layout = (LinearLayout) dialog.findViewById(R.id.layoutid);

        layout.setMinimumWidth(Constants.Width);

        tv_goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();
                ((Activity) c).finish();

            }
        });
    }

    public static void Zoom_Image(final Context c, String imagepath) {

        final Dialog dialog = new Dialog(c, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.zoom_image);
        dialog.show();

        ZoomageView imageview = (ZoomageView) dialog.findViewById(R.id.myZoomageView);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.blankimage)
                .showImageForEmptyUri(R.drawable.congrrr)
                .showImageOnFail(R.drawable.congrrr)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        imageLoader.getInstance().displayImage(imagepath, imageview, options, animateFirstListener);

    }

    public static void applyFontForToolbarTitle(Toolbar toolbar, Context context) {

        //   Toolbar toolbar = (Toolbar) context.findViewById(R.id.app_bar);
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                Typeface titleFont = Typeface.
                        createFromAsset(context.getAssets(), "avenirnextdemibold.ttf");
                if (tv.getText().equals(toolbar.getTitle())) {
                    tv.setTypeface(titleFont);
                    break;
                }
            }
        }
    }

    public static void check_key(Context c, Home_getset obj, String adap) {

        data = obj;
        checkclass = adap;

        try {
            JSONObject object = new JSONObject(data.getJsonfield());

            Logg("object", object + "");

            if (object.has("ans_status_new")) {
                Logg("ans_status_new", object.getString("ans_status_new"));
                Constants.Ans_Status = object.getString("ans_status_new");
            } else {
                Logg("ans_status", "Yes");
                Constants.Ans_Status = "Yes";
            }

            if (object.has("offline_status")) {
                Logg("offline_status", object.getString("offline_status"));
                Constants.offline_status = object.getString("offline_status");
            } else {
                Logg("offline_status", "no");
                Constants.offline_status = "no";
            }

            if (object.has("negative_marks")) {
                Constants.Negative_Ans = object.getString("negative_marks");
            } else
                Constants.Negative_Ans = "yes";

            if (object.has("secret_key")) {

                if (!object.getString("secret_key").equalsIgnoreCase("0")) {
                    DatabaseHandler dbh = new DatabaseHandler(c);

                    if (dbh.get_testkey(data.getId() + "") > 0) {
                        openTest(c, data);
                    } else
                        insertKey(c, data);
                } else {
                    openTest(c, data);
                }
            } else {
                openTest(c, data);
            }

        } catch (JSONException e) {
        }


    }

    public static void insertKey(final Context c, final Home_getset home) {

        final Dialog dialog = new Dialog(c);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.insert_online_testkey);
        dialog.show();

        TextView tv_submit = (TextView) dialog.findViewById(R.id.tv_submit);
        TextView tv_cancle = (TextView) dialog.findViewById(R.id.tv_cancel);

        final EditText et_key = (EditText) dialog.findViewById(R.id.et_key);

        LinearLayout layout = (LinearLayout) dialog.findViewById(R.id.layoutid);

        layout.setMinimumWidth(Constants.Width);

        tv_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();
//                ((Activity) c).finish();

            }
        });

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    JSONObject object = new JSONObject(home.getJsonfield());

                    if (et_key.getText().toString().length() < 1) {
                        Toast.makeText(c, "Enter Key", Toast.LENGTH_SHORT).show();
                        return;
                    } else if (et_key.getText().toString().equalsIgnoreCase(object.getString("secret_key"))) {

                        DatabaseHandler dbh = new DatabaseHandler(c);
                        dbh.insert_testkey(home.getId() + "");

                        openTest(c, home);
                        dialog.dismiss();
                    } else {
                        Toast.makeText(c, "Invalid Key", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    private static void openTest(final Context c, Home_getset home) {

        JSONObject jsonobj = null;
        try {

//            Utility.checkclass="homefragment";

            Logg("home.get>>", home.getFieldtype() + "");

            jsonobj = new JSONObject(home.getJsonfield());

            if (Utility.checkPayment(c, jsonobj)) {
                if (Utility.checkFile(Constants.URL + "admin/tests_images/" + home.getId() + ".zip") == 200) {
                    jsonobj = new JSONObject(home.getJsonfield());
                    Utility.startTest(c, String.valueOf(home.getId()));
                } else {

                    Logg("home.get", "no image");
                    Intent in = new Intent(c, Play_new.class);
                    in.putExtra("testid", String.valueOf(home.getId()));
                    in.putExtra("type", "4");
                    c.startActivity(in);
                }
            } else {
                Utility.data = home;
                Utility.payPayment(c, jsonobj, home.getId());
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("exception", "", e);
        }

    }

    public static String ConvertDate(String pre_dt) {

        String dateStr = pre_dt;//"21/20/2011";

        DateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date date = null;

        try {

            date = srcDf.parse(dateStr);

            DateFormat destDf = new SimpleDateFormat("dd-MMM-yyyy");

            return dateStr = destDf.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String ConvertDate1(String pre_dt) {

        String dateStr = pre_dt;//"21/20/2011";

        DateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date date = null;

        try {

            date = srcDf.parse(dateStr);

            DateFormat destDf = new SimpleDateFormat("dd-MMM-yyyy HH:mm");

            return dateStr = destDf.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static boolean hasPermissions(Context context, String... permissions) {

        boolean b = true;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    b = false;
                }
            }
        }
        return b;
    }

}