package com.scholar.engineering.banking.ssc;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
//import com.androidquery.AQuery;
import com.bumptech.glide.Glide;
//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.NativeExpressAdView;
import com.scholar.engineering.banking.ssc.adapter.Replies_on_group_comment_adap;
import com.scholar.engineering.banking.ssc.adapter.Result_Test_Adapter;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.ImageCompression;
import com.scholar.engineering.banking.ssc.utils.ImageUploading;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;
import static com.scholar.engineering.banking.ssc.utils.Constants.imageFilePath;
import static com.scholar.engineering.banking.ssc.utils.Constants.st_base46;
import static com.scholar.engineering.banking.ssc.utils.Utility.ConvertDate1;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.Utility.getDate;

public class Result_new extends AppCompatActivity implements OnClickListener {
    Intent detail1;
    EditText et_comment;
    TextView tv_commentcount, tv_like, tv_title, tv_rank, tv_marks, tv_accuracy, tv_attempted, tv_correct;
    ImageView nimage, camera_img, iv_like;
    AppCompatImageView postcom;
    String title, comment, image, des = "";
    RecyclerView list, rv_test;
    TextView text, text1, text2, text3, text4, tv_mentorname, tv_follow;
    String url, email, mentor_email = "";
    LinearLayout related_quiz, comments, lay_mentors;
    Replies_on_group_comment_adap adap;
    Result_Test_Adapter Test_adap;
    String postid;
    ProgressDialog progress;
    Home_getset data;
    int like_int = 0, comment_int = 0, right = 0, wrong = 0, unattempt = 0, index = 0, total_question = 0;
    LinearLayout lay_commentpost, lay_like, lay_comment, lay_share;
    String st_imagename;
    Uri selectedImage;
    Bitmap bitmap;
    RelativeLayout loadmore_lay;
    NetworkConnection nw;
    TextView moreButton, tv_checksolution;
    TextView Share, tv_result;
    JSONArray jsonArray;
    JSONObject jsonObject;
    Typeface font_demi, font_medium;

    CircleImageView iv_mentor;
    ImageUploading ab;
    private final int REQUEST_CODE_CLICK_IMAGE = 1002, REQUEST_CODE_GALLERY_IMAGE = 1003;
    Toolbar resultToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_new);


        resultToolbar = findViewById(R.id.resultToolbar);
        setSupportActionBar(resultToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Play_new.Counter = 0;
        st_base46 = "";

        Constants.imageFilePath = CommonUtils.getFilename();

        nw = new NetworkConnection(this);

        font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");
        font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");
        list = (RecyclerView) findViewById(R.id.rv_comment);

        rv_test = (RecyclerView) findViewById(R.id.rv_test);
        tv_result = (TextView) findViewById(R.id.tv_result);

        text = (TextView) findViewById(R.id.text);
        text1 = (TextView) findViewById(R.id.text1);
        related_quiz = (LinearLayout) findViewById(R.id.related_quiz_layout);
        comments = (LinearLayout) findViewById(R.id.comments_layout);
        text2 = (TextView) findViewById(R.id.text2);
        text3 = (TextView) findViewById(R.id.text3);
        text4 = (TextView) findViewById(R.id.text4);
        Share = (TextView) findViewById(R.id.tv_shareid);
        tv_checksolution = (TextView) findViewById(R.id.tv_checksolution);
        iv_mentor = (CircleImageView) findViewById(R.id.iv_user);
        tv_mentorname = (TextView) findViewById(R.id.tv_mentorname);
        tv_follow = (TextView) findViewById(R.id.tv_follwers);
        lay_mentors = (LinearLayout) findViewById(R.id.lay_mentors);

        Share.setTypeface(font_medium);
        text.setTypeface(font_demi);
        text1.setTypeface(font_demi);
        text2.setTypeface(font_demi);
        text3.setTypeface(font_demi);
        text4.setTypeface(font_demi);
        tv_checksolution.setTypeface(font_medium);
        tv_follow.setTypeface(font_medium);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(Result_new.this);
        mLayoutManager.scrollToPositionWithOffset(0, 0);
        list.setLayoutManager(mLayoutManager);
        list.setHasFixedSize(true);
        list.setNestedScrollingEnabled(false);
        LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(Result_new.this);
        mLayoutManager1.scrollToPositionWithOffset(0, 0);
        rv_test.setLayoutManager(mLayoutManager1);
        rv_test.setHasFixedSize(true);
        rv_test.setNestedScrollingEnabled(false);
        //Logg("solar1 oncr","solar1 on create");

        tv_commentcount = (TextView) findViewById(R.id.tv_commentid);
        tv_commentcount.setTypeface(font_medium);

        tv_like = (TextView) findViewById(R.id.tv_likeid);
        tv_like.setTypeface(font_medium);
        tv_title = (TextView) findViewById(R.id.tv_title);
        moreButton = (TextView) findViewById(R.id.moreButton);
        moreButton.setTypeface(font_medium);
        tv_title.setTypeface(font_demi);
        loadmore_lay = (RelativeLayout) findViewById(R.id.footer_layout);

        et_comment = (EditText) findViewById(R.id.comtextid);
        et_comment.setTypeface(font_medium);

        nimage = (ImageView) findViewById(R.id.newsimage);

        postcom = (AppCompatImageView) findViewById(R.id.postcomid);
        iv_like = (ImageView) findViewById(R.id.likeimageid);

        camera_img = (ImageView) findViewById(R.id.iv_camera_id);

        lay_comment = (LinearLayout) findViewById(R.id.lay_comment);
        lay_like = (LinearLayout) findViewById(R.id.lay_like);
        lay_share = (LinearLayout) findViewById(R.id.lay_shareid);
        lay_commentpost = (LinearLayout) findViewById(R.id.lay_commentpost);

        moreButton = (TextView) findViewById(R.id.moreButton);
        tv_rank = (TextView) findViewById(R.id.tv_rank);
        tv_marks = (TextView) findViewById(R.id.tv_marks);
        tv_accuracy = (TextView) findViewById(R.id.tv_accuracy);
        tv_attempted = (TextView) findViewById(R.id.tv_attempt);
        tv_correct = (TextView) findViewById(R.id.tv_correct);
        tv_rank.setTypeface(font_demi);
        tv_marks.setTypeface(font_demi);
        tv_accuracy.setTypeface(font_demi);
        tv_attempted.setTypeface(font_medium);
        tv_correct.setTypeface(font_medium);
        detail1 = getIntent();
        right = detail1.getIntExtra("correct", 0);
        unattempt = detail1.getIntExtra("unattempt", 0);
        wrong = detail1.getIntExtra("wrong", 0);
        total_question = detail1.getIntExtra("totalquestion", 0);
        title = detail1.getStringExtra("testname");
        tv_title.setText(title);

        int att = right + wrong; // total attempts
        float marks;
        if (Constants.Negative_Ans.equalsIgnoreCase("yes")) {
            marks = (float) (wrong * .25);  // count negative marks
            marks = right - marks;
            tv_marks.setText(marks + "/" + total_question);
        } else {
//			marks = right;
            tv_marks.setText(right + "/" + total_question);
        }

        int ac;

        if (att == 0) {
            ac = 0;
        } else
            ac = ((Integer) (right * 100) / att);

        tv_correct.setText("Correct: " + right);
        tv_attempted.setText("Attempted: " + att);
//		tv_marks.setText(marks+"/"+total_question);
        tv_accuracy.setText(ac + "");
        email = User_Email;

        if (Build.VERSION.SDK_INT >= 24) {
            try {
                java.lang.reflect.Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {

            }
        }

        if (Constants.Ans_Status.equalsIgnoreCase("no"))
            tv_checksolution.setVisibility(View.GONE);

        tv_checksolution.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {


                Constants.checksolution = false;

                finish();

            }
        });

        try {

            jsonObject = new JSONObject(detail1.getStringExtra("response"));
            //setData(jsonObject);
            Log.e("response>>",detail1.getStringExtra("response"));
            mentor_email = jsonObject.getString("uid");


            if (mentor_email.length() > 4) {
                JSONObject mentor_obj = jsonObject.getJSONObject("mentor_data");
                tv_mentorname.setText(mentor_obj.getString("name"));

                //set followers

                if (mentor_obj.getString("image").length() > 4)
                    Glide.with(Result_new.this).load(Constants.URL_Image + mentor_obj.getString("image")).into(iv_mentor);

                lay_mentors.setVisibility(View.VISIBLE);

                if (jsonObject.has("followed")) {
                    if (jsonObject.getString("followed").equalsIgnoreCase("follow")) {
                        tv_follow.setText("Following");
                    } else {
                        tv_follow.setText("Follow");
                    }
                }

                tv_follow.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Follow();
                    }
                });
            } else {
                lay_mentors.setVisibility(View.GONE);
            }

//            JSONObject rank = new JSONObject(jsonObject.getString("rank"));

            postid = jsonObject.getString("testid");

//            des = "I have secured " + rank.getString("rank") + "/" + rank.getString("candidate") + " rank in this Quiz & challenge you to beat my score. Attempt this QUIZ and send me screenshot of your result.";
//
//            tv_rank.setText(rank.getString("rank") + "/" + rank.getString("candidate"));

            comment_int = Integer.valueOf(jsonObject.getString("comment"));
            like_int = Integer.valueOf(jsonObject.getString("likes"));

            checkResultstatus(jsonObject.getString("premium"), new JSONObject(jsonObject.getString("jsondata")));

            Log.e("jsonObject",""+jsonObject);

            JSONObject o = new JSONObject(jsonObject.getString("jsondata"));

            if (o.has("add_comment")) {
                if (o.getString("add_comment").equalsIgnoreCase("off")) {
                    lay_comment.setVisibility(View.GONE);
                }
            }

            setComment(comment_int);

            setLike(like_int);

            if (jsonObject.getString("likestatus").equals("like")) {
                iv_like.setImageResource(R.drawable.ic_bulb_filled);
                tv_like.setTextColor(getResources().getColor(R.color.like_text_color));
            } else {
                iv_like.setImageResource(R.drawable.ic_bulb_lightup);
                tv_like.setTextColor(getResources().getColor(R.color.text_lightgray));
            }

            ArrayList<Object> homedata = new ArrayList<>();

            if (jsonObject.has("test")) {

                JSONArray array = new JSONArray(jsonObject.getString("test"));

                for (int i = 0; i < array.length(); i++) {
                    JSONObject ob = array.getJSONObject(i);

                    Logg("object>>>>", ob + "");
                    String time = getDate(ob.getString("timestamp"));

                    Home_getset object = new Home_getset();
                    object.setId(ob.getInt("id"));
                    object.setTimestamp(time);
                    object.setLikecount(ob.getInt("likes"));
                    object.setCommentcount(ob.getInt("comment"));
                    object.setViewcount(ob.getInt("view"));
                    object.setUid(ob.getString("uid"));
                    object.setPosttype(ob.getString("posttype"));
                    object.setGroupid(ob.getString("groupid"));
                    object.setFieldtype(ob.getString("field_type"));
                    object.setJsonfield(ob.getString("jsondata"));
                    object.setLikestatus("like");
                    object.setPosturl(ob.getString("posturl"));
                    object.setPostdescription(ob.getString("post_description"));
                    if (ob.has("AppVersion"))
                        object.setAppVersion(ob.getString("AppVersion"));
                    homedata.add(object);
                }

                related_quiz.setVisibility(View.VISIBLE);
                Test_adap = new Result_Test_Adapter(this, homedata);
                rv_test.setAdapter(Test_adap);
            } else {
                related_quiz.setVisibility(View.GONE);

            }

            volleylist(postid);

        } catch (JSONException e) {
            Log.e("exception","",e);
        }

        jsonArray = new JSONArray();

        postcom.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (et_comment.getText().length() > 0) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(et_comment.getWindowToken(), 0);

                    if (st_base46.length() > 1)
                        imagevolley();
                    else
                        volleycominsert();

                } else
                    Toast.makeText(getApplicationContext(), "Please enter comment", Toast.LENGTH_SHORT).show();
            }
        });

        ab = new ImageUploading(this, camera_img);
        ab.createBottomSheetDialog();
        camera_img.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ab.showOptionBottomSheetDialog();
            }
        });

        nw = new NetworkConnection(Result_new.this);

        lay_like.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!nw.isConnectingToInternet()) {
                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
                    return;
                }
                volleylike_post();
            }
        });

        lay_comment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (lay_commentpost.getVisibility() == View.VISIBLE) {
                    lay_commentpost.setVisibility(View.GONE);
                } else {
                    lay_commentpost.setVisibility(View.VISIBLE);
                }

            }
        });

        lay_share.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                JSONObject shareobj = new JSONObject();

                try {

                    shareobj.put("class", "Online_Test");
                    shareobj.put("title", title);
                    shareobj.put("description", des);
                    shareobj.put("image", Constants.URL + "newapi2_04/applogo.png");
                    shareobj.put("id", postid);

                } catch (JSONException e) {
                }

                Utility.shareIntent(Result_new.this, shareobj);

            }
        });

        moreButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if (jsonArray.length() > 0) {

                        JSONObject obj = jsonArray.getJSONObject(0);

                        index = obj.getInt("id");

                    } else {
                        index = 0;
                    }

                    volleylist(postid);

                } catch (JSONException e) {
                }
            }
        });
    }

    public void checkResultstatus(String prem, JSONObject object) {

        if (prem.equalsIgnoreCase("0")) {
            try {
                if (object.getString("result_declare").equalsIgnoreCase("no")) {
                    String date = object.getString("end_date") + ":00";
                    tv_result.setText("Result will be announce\non " + ConvertDate1(date));
                    tv_rank.setText("Pending");
                    tv_rank.setTextSize(12f);
                } else {
                    tv_result.setVisibility(View.GONE);
                }
            } catch (JSONException e) {

            }
        } else {

            tv_result.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logg("call_onDestroy()", "Call onDestroy()");
        deleteCache(this);
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public void setComment(int com) {

        tv_commentcount.setText("Comments (" + com + ")");

        if (com < 10) {
        } else if (com < 100) {
            tv_commentcount.setTextSize(12f);
        } else {
            tv_commentcount.setTextSize(11f);
        }
    }

    public void setLike(int lk) {
        if (lk > 0) {
            tv_like.setText(getResources().getString(R.string.like) + " (" + lk + ")");
        } else {
            tv_like.setText(getResources().getString(R.string.like));
        }
        if (lk < 10) {
        } else if (lk < 100) {
            tv_like.setTextSize(12f);
        } else {
            tv_like.setTextSize(11f);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_CLICK_IMAGE) {

            new ImageCompression(camera_img).execute(imageFilePath);

        } else if (requestCode == REQUEST_CODE_GALLERY_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

//            String[] projection = {MediaStore.Images.Media.DATA};
//            Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
//            cursor.moveToFirst();
//            int columnIndex = cursor.getColumnIndex(projection[0]);
            File myFile = new File(uri.getPath());

            final String picturePath = myFile.getAbsolutePath();//cursor.getString(columnIndex); // returns null
            // cursor.close();

            //copy the selected file of gallery into app's sdcard folder and perform the compression operations on it.
            //And override the original image with the newly resized image.

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        CommonUtils.copyFile(picturePath, imageFilePath);
                    } catch (IOException e) {

                    }
                }
            });

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bmp = BitmapFactory.decodeFile(imageFilePath, options);

            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;

            Logg("actualheight2", actualHeight + " " + actualWidth);

            if (actualWidth > 600 | actualHeight > 600) {
                new ImageCompression(camera_img).execute(imageFilePath);
            } else {

                getContentResolver().notifyChange(uri, null);
                ContentResolver cr = getContentResolver();
                Bitmap bitmap;

                try {
                    bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, uri);
                    //Logg("bitmap_h_w",bitmap.getHeight()+" "+bitmap.getWidth()+"");
                    camera_img.setImageBitmap(bitmap);

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();

                    st_base46 = Base64.encodeToString(byteArray, Base64.DEFAULT);
//                    return Base64.encodeToString(byteArray, Base64.DEFAULT);

                    Logg("base64", st_base46);

                } catch (Exception e) {

                }
            }
        }
    }

    public void volleylist(final String postid) {
        progress = ProgressDialog.show(Result_new.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(true);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        if (!nw.isConnectingToInternet()) {
//            swipeRefreshLayout.setRefreshing(false);
            internetconnection(1);
            return;
        }
        if (index == 0)
            progress.show();

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        JSONObject obj = new JSONObject();
        try {
            obj.put("postid", postid);
            obj.put("index", index);
            obj.put("email", Constants.User_Email);
            obj.put("admission_no", Constants.addmissionno);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Constants.URL_LV + "get_common_comment";
        Log.e("url",url);

        JsonObjectRequest json = new JsonObjectRequest(Method.POST, url, obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                // TODO Auto-generated method stub

                //Logg("respon",String.valueOf(res));


                try {

                    JSONArray jr = res.getJSONArray("data");

                    if (jr.length() >= 2)
                        loadmore_lay.setVisibility(View.VISIBLE);
                    else
                        loadmore_lay.setVisibility(View.GONE);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonobj = jsonArray.getJSONObject(i);
                        jr.put(jsonobj);

                    }

                    jsonArray = new JSONArray(new ArrayList<>());

                    for (int i = 0; i < jr.length(); i++)
                        jsonArray.put(jr.getJSONObject(i));


                    if (progress.isShowing())
                        progress.dismiss();

                    adap = new Replies_on_group_comment_adap(Result_new.this, jsonArray);
                    list.setAdapter(adap);


                } catch (JSONException e) {
                    if (progress.isShowing())
                        progress.dismiss();

                    loadmore_lay.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                if (progress.isShowing())
                    progress.dismiss();
                loadmore_lay.setVisibility(View.GONE);
            }
        });


        json.setShouldCache(false);
        int socketTimeout = 20000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        json.setRetryPolicy(policy);
        queue.add(json);
    }

    public void imagevolley() {
        progress = ProgressDialog.show(Result_new.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(true);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        if (!nw.isConnectingToInternet()) {
            internetconnection(2);
            return;
        }

        progress.show();
        st_imagename = String.valueOf(System.currentTimeMillis());
        comment = et_comment.getText().toString();

        RequestQueue queue = Volley.newRequestQueue(Result_new.this);
        String url = Constants.URL_LV + "insert_common_comment_image";
        Logg("name", url);

        StringRequest request = new StringRequest(Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                Logg("response", s);
                try {
                    progress.dismiss();
                    if (s.length() > 0) {
                        JSONObject obj = new JSONObject(s);
                        s = obj.getString("status");
                        if (s.equalsIgnoreCase("success")) {
                            Toast.makeText(Result_new.this, "Upload Successfully", Toast.LENGTH_LONG).show();
                            et_comment.setText("");
                            st_base46 = "";
                            camera_img.setImageResource(R.drawable.camera40);

                            jsonArray = new JSONArray(new ArrayList<String>());
                            index = 0;

                            comment_int++;
                            setComment(comment_int);

                            volleylist(postid);

                        } else if (obj.getString("status").equalsIgnoreCase("block"))
                            Toast.makeText(Result_new.this, obj.getString("response"), Toast.LENGTH_LONG).show();
                        else {
                            Toast.makeText(Result_new.this, "Somthing is Wrong Can't Upload, Please Try Againe", Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (Exception e) {
                    progress.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(Result_new.this, CommonUtils.volleyerror(volleyError));
                progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("st_base64", st_base46);
                params.put("imagename", st_imagename);
                params.put("postid", postid + "");
                params.put("posttype", "1");
                params.put("email", email);
                params.put("comment", comment.replaceAll(" ", "%20"));
                params.put("admission_no", Constants.addmissionno);
                return params;
            }
        };

        request.setShouldCache(false);
        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void volleycominsert() {
        progress = ProgressDialog.show(Result_new.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(true);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        if (!nw.isConnectingToInternet()) {
            internetconnection(1);
            return;
        }

        if (index == 0)
            progress.show();

        String url = Constants.URL_LV + "insert_common_comment";
        Log.e("insert_common_comment", url);
        comment = et_comment.getText().toString();
        RequestQueue que = Volley.newRequestQueue(getApplicationContext());

        StringRequest obj1 = new StringRequest(Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String res) {
                int size = res.length();
                progress.dismiss();

                if (size > 0) {

                    try {
                        JSONObject obj = new JSONObject(res);

                        if (obj.getString("status").equals("success")) {
                            et_comment.setText("");
                            Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();

                            jsonArray = new JSONArray(new ArrayList<String>());
                            index = 0;

                            comment_int++;
                            setComment(comment_int);
                            volleylist(postid);
                        } else if (obj.getString("status").equalsIgnoreCase("block"))
                            Toast.makeText(Result_new.this, obj.getString("response"), Toast.LENGTH_LONG).show();
                        else {
                            Toast.makeText(getApplicationContext(), "Somthing is wrong, Can't insert comment", Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        progress.dismiss();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                progress.dismiss();
                CommonUtils.toast(Result_new.this, CommonUtils.volleyerror(e));
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("postid", postid + "");
                params.put("posttype", "1");
                params.put("comment", comment);
                params.put("email", email);
                params.put("admission_no", Constants.addmissionno);
                return params;
            }
        };

        obj1.setShouldCache(false);
        int socketTimeout = 0;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        obj1.setRetryPolicy(policy);
        que.add(obj1);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Play_new.h.sendEmptyMessage(0);
    }

    public void volleylike_post() {

        RequestQueue queue = Volley.newRequestQueue(Result_new.this);
        JSONObject json = new JSONObject();
        try {
            json.put("id", postid);
            json.put("email", Constants.User_Email);
            json.put("admission_no", Constants.addmissionno);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Constants.URL_LV + "like_post";
        Logg("likeapi", url + " , " + json.toString());
        JsonObjectRequest jsonreq = new JsonObjectRequest(Method.POST, url, json, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                try {

                    if (res.getString("scalar").equals("like")) {
                        like_int++;
                        iv_like.setImageResource(R.drawable.ic_bulb_filled);
                        tv_like.setTextColor(getResources().getColor(R.color.like_text_color));
                        setLike(like_int);

                    } else if (res.getString("scalar").equals("dislike")) {
                        iv_like.setImageResource(R.drawable.ic_bulb_lightup);
                        tv_like.setTextColor(getResources().getColor(R.color.text_lightgray));
                        like_int--;
                        setLike(like_int);
                    } else {
                        Toast.makeText(Result_new.this, "Not done", Toast.LENGTH_SHORT).show();
                    }
                    progress.dismiss();
                } catch (JSONException e) {
                    progress.dismiss();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg) {
                progress.dismiss();
                CommonUtils.toast(Result_new.this, CommonUtils.volleyerror(arg));
            }
        });
        jsonreq.setShouldCache(false);
        int socketTimeout = 0;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonreq.setRetryPolicy(policy);
        queue.add(jsonreq);
    }

    public void Follow() {

        progress = ProgressDialog.show(Result_new.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        progress.show();

        RequestQueue queue = Volley.newRequestQueue(Result_new.this);
        String url = "";

        url = Constants.URL + "v3/follow_mentors.php?";

        url = url.replace(" ", "%20");

        //Logg("name", url);

        StringRequest request = new StringRequest(Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                //Logg("response", s);

                try {

                    progress.dismiss();

                    if (s.length() > 0) {
                        JSONObject obj = new JSONObject(s);

                        if (obj.has("status")) {
                            String sts = obj.getString("status");
                            if (sts.equalsIgnoreCase("success")) {
                                if (obj.getString("response").equalsIgnoreCase("unfollow")) {
                                    tv_follow.setText("Follow");
                                } else {
                                    tv_follow.setText("Following");
                                }
                            }
                        }

                    }
                } catch (Exception e) {
                    progress.dismiss();
                    Logg("e", e + "");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(Result_new.this, CommonUtils.volleyerror(volleyError));
                progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                //id="+psid+"&comment="+cmnt+"&user="+username+"&collage="+collage+"&image="+image+"&email="+mail;

                Map<String, String> params = new HashMap<String, String>();
                params.put("mentor_id", mentor_email);
                params.put("user_id", email);

                //Logg("img222222", st_base46);

                return params;
            }
        };

        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    public void internetconnection(final int i) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                if (i == 0)
                    volleylist(postid);
                else if (i == 1)
                    volleycominsert();
                else if (i == 2)
                    imagevolley();

//				getData();

            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();

        return super.onOptionsItemSelected(item);
    }
}
