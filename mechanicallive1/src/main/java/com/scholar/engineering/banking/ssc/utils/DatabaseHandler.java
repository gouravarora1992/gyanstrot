package com.scholar.engineering.banking.ssc.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.getset.gettr_settr;

import java.util.ArrayList;

/**
 * Created by surender on 1/6/2016.
*/

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "database1.db";
    SQLiteDatabase db;
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(
                "create table groups1 " +
                  "(groupname text,comment text,id integer,member text,follow_status text,status text,image text,payment text,payment_status text)");

        db.execSQL(
                "create table postquestion " +
                        "(id integer,groupid text,groupname text,timestamp text,name text,date text,question text,optiona text,optionb text,optionc text,optiond text,answer text,uimage text,queimage text,useranswer text,type text)");
            db.execSQL(
                "create table user_answer " +
                        "(id integer,answer text)");


        db.execSQL(
                "create table test_key " +
                        "(testid text)");

        // save content table
        db.execSQL(
                "create table common_table " +
                        "(id integer,timestamp text,like integer,comment integer,view integer,uid text," +
                        "posttype text,groupid text,field_type text,jsondata text,likestatus text,posturl text,post_description text)");

        //store data for offline
        db.execSQL(
                "create table offline_table " +
                        "(id integer,timestamp text,like integer,comment integer,view integer,uid text," +
                        "posttype text,groupid text,field_type text,jsondata text,likestatus text,posturl text,post_description text)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS postquestion");
        //Logg("drob successfull", "drop postquestion");

        db.execSQL("DROP TABLE IF EXISTS groups1");
        //Logg("drob successfull", "drop groups1");

        db.execSQL("DROP TABLE IF EXISTS user_answer");

        db.execSQL("DROP TABLE IF EXISTS test_key");
        //Logg("drob successfull", "drop user_answer");

        db.execSQL("DROP TABLE IF EXISTS common_table");
        //Logg("drob successfull", "drop common_table");

        onCreate(db);
    }

    public void databaseopen(){
       db = this.getWritableDatabase();
    }
    public void databaseclose(){
        db.close();
    }

    public void updatepostquestion(int id,String ans)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("useranswer", ans);

        db.update("postquestion", contentValues, "id = ? ", new String[]{Integer.toString(id)});

        db.close();
        //return true;
    }

    public void groupinsert(String name,String com,int id,String mem, String fol_sts,String sts,
                            String image,String pay,String pay_status) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("groupname", name);
        contentValues.put("comment", com);
        contentValues.put("id", id);
        contentValues.put("member", mem);
        contentValues.put("follow_status", fol_sts);
        contentValues.put("status", sts);
        contentValues.put("image", image);
        contentValues.put("payment", pay);
        contentValues.put("payment_status",pay_status);

        db.insert("groups1", null, contentValues);
        db.close();
    }
//
    public void offline_content(Home_getset data) {
        //get_offlinecontent_single(data.getId());

        ContentValues contentValues = new ContentValues();
        contentValues.put("id", data.getId());
        contentValues.put("timestamp", data.getTimestamp());
        contentValues.put("like", data.getLikecount());
        contentValues.put("comment", data.getCommentcount());
        contentValues.put("view", data.getViewcount());
        contentValues.put("uid", data.getUid());
        contentValues.put("posttype", data.getPosttype());
        contentValues.put("groupid", data.getGroupid());
        contentValues.put("field_type", data.getFieldtype());
        contentValues.put("jsondata", data.getJsonfield());
        contentValues.put("likestatus", data.getLikestatus());
        contentValues.put("posturl", data.getPosturl()+"");
        contentValues.put("post_description", data.getPostdescription()+"");

        db.insert("offline_table", null, contentValues);
//        db.close();

    }

    public void get_offlinecontent_single(int id) {

        SQLiteDatabase db = this.getWritableDatabase();

        int i=0;

        Cursor cursor = null;

        try {

            cursor = db.rawQuery("SELECT likestatus FROM offline_table WHERE id=?", new String[] {id + ""});

            if(cursor.getCount() > 0) {
                delete_offlinecontent(id);
            }

            //Logg("offline_table_get","common_table "+i);

        }finally {
            if(cursor!=null)
            cursor.close();
        }
    }

    public void delete_offlinecontent(int id) {

        SQLiteDatabase db = this.getWritableDatabase();

       // db.rawQuery("delete from offline_table where id =" + id, null);

        //db.delete("offline_table", "id = ? ", new String[]{Integer.toString(id)});

        db.execSQL("delete from offline_table");

        db.close();

    }

    public ArrayList<Home_getset> get_offlinecontent() {

        ArrayList<Home_getset> array_list = new ArrayList<>();

        //SQLiteDatabase db = this.getReadableDatabase();
        String id = "false";
        Cursor res = db.rawQuery("select * from offline_table", null);
        try{
            if (res!=null && res.moveToFirst()){
                while (res.isAfterLast() == false) {

                    Home_getset object=new Home_getset();
                    object.setId(res.getInt(0));
                    object.setTimestamp(res.getString(1));
                    object.setLikecount(res.getInt(2));
                    object.setCommentcount(res.getInt(3));
                    object.setViewcount(res.getInt(4));
                    object.setUid(res.getString(5));
                    object.setPosttype(res.getString(6));
                    object.setGroupid(res.getString(7));
                    object.setFieldtype(res.getString(8));
                    object.setJsonfield(res.getString(9));
                    object.setLikestatus(res.getString(10));
                    object.setPosturl(res.getString(11));
                    object.setPostdescription(res.getString(12));

                    array_list.add(object);
                    res.moveToNext();
                }
            }

        }finally {
            if(res!=null)
                res.close();
        }

//        db.close();

        return array_list;
    }

    public void save_content(Home_getset data) {

//        db.execSQL(
//                "create table common_table " +
//                        "(id integer,timestamp text,field_type integer,groupid text,like integer," +
//                        "comment integer,view integer," +
//                        "posttype text,uid text,posturl text,post_description text,jsondata text)");


        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", data.getId());
        contentValues.put("timestamp", data.getTimestamp());
        contentValues.put("like", data.getLikecount());
        contentValues.put("comment", data.getCommentcount());
        contentValues.put("view", data.getViewcount());
        contentValues.put("uid", data.getUid());
        contentValues.put("posttype", data.getPosttype());
        contentValues.put("groupid", data.getGroupid());
        contentValues.put("field_type", data.getFieldtype());
        contentValues.put("jsondata", data.getJsonfield());
        contentValues.put("likestatus", data.getLikestatus());
        contentValues.put("posturl", data.getPosturl()+"");
        contentValues.put("post_description", data.getPostdescription()+"");



        db.insert("common_table", null, contentValues);
        db.close();
    }

    public ArrayList<Home_getset> get_savecontent() {

        ArrayList<Home_getset> array_list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        String id = "false";
        Cursor res = db.rawQuery("select * from common_table", null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {



//            array_list.add(new Home_getset(res.getInt(0),res.getString(1),res.getInt(2),res.getInt(3),res.getInt(4),
//                    res.getString(6), res.getString(5), res.getString(7),
//                    res.getString(8),res.getString(9),res.getString(10),res.getString(11),res.getString(12)));

            Home_getset object=new Home_getset();
            object.setId(res.getInt(0));
            object.setTimestamp(res.getString(1));
            object.setLikecount(res.getInt(2));
            object.setCommentcount(res.getInt(3));
            object.setViewcount(res.getInt(4));
            object.setPosttype(res.getString(6));
            object.setUid(res.getString(5));
            object.setGroupid(res.getString(7));
            object.setFieldtype(res.getString(8));
            object.setJsonfield(res.getString(9));
            object.setLikestatus(res.getString(10));
            object.setPosturl(res.getString(11));
            object.setPostdescription(res.getString(12));

            array_list.add(object);



            res.moveToNext();
        }
        db.close();
        return array_list;
    }

    public void update_commontable(int id,String colm, int val)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(colm, val);

        db.update("common_table", contentValues, "id = ? ", new String[]{Integer.toString(id)});
        //return true;

        db.close();
    }

    public void update_likestatus(int id, String val)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put("likestatus", val);

        db.update("common_table", contentValues, "id = ? ", new String[]{Integer.toString(id)});
        //return true;
        db.close();
    }

    public void update_record(Home_getset data)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("likestatus", data.getLikestatus());
        contentValues.put("like", data.getLikecount());
        contentValues.put("comment", data.getCommentcount());
        contentValues.put("view", data.getViewcount());

        db.update("common_table", contentValues, "id = ? ", new String[]{Integer.toString(data.getId())});
        //return true;
        db.close();
    }

    public int get_savecontent_single(int id) {

        SQLiteDatabase db = this.getWritableDatabase();

        int i=0;

        Cursor cursor = null;

        try {

            cursor = db.rawQuery("SELECT likestatus FROM common_table WHERE id=?", new String[] {id + ""});

                if(cursor.getCount() > 0) {
                    i=1;
                }

            //Logg("common_table_get","common_table "+i);

            return i;

        }finally {
            if(cursor!=null)
            cursor.close();
        }
    }

    public void delete_savecontent(int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.rawQuery("delete from common_table where id =" + id, null);

        db.delete("common_table", "id = ? ", new String[]{Integer.toString(id)});

        db.close();
    }

    public void postquestion_insert(int id,String ans) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("answer", ans);

        db.insert("user_answer", null, contentValues);
        db.close();
        //Logg("postquestion_insert","postquestion_insert");
    }

    public String postquestion_get(int id) {

        String ans="ua";
        SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = null;

            try {
                cursor = db.rawQuery("SELECT answer FROM user_answer WHERE id=?", new String[] {id + ""});
                if(cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    ans = cursor.getString(cursor.getColumnIndex("answer"));
                }

                //Logg("postquestion_insert","postquestion_get "+ans);

                return ans;
            }finally {
                if(cursor!=null)
                cursor.close();
            }
    }

    public ArrayList<gettr_settr> getfollowgroup() {
        ArrayList<gettr_settr> array_list = new ArrayList<gettr_settr>();

        SQLiteDatabase db = this.getReadableDatabase();
        String id = "false";
        Cursor res = db.rawQuery("select * from groups1", null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            String c=res.getString(1);
            array_list.add(new gettr_settr(res.getString(0),Integer.parseInt(c),
                    res.getInt(2), res.getString(3), res.getString(4),
                    res.getString(5),res.getString(6),res.getString(7),res.getString(8)));
            res.moveToNext();
        }

        db.close();
        return array_list;
    }

    public void unfollowgroup(int id,String status) {
        SQLiteDatabase db = this.getWritableDatabase();

        // db.delete("delete from orderlist where id= "+'"id"', null);

        ContentValues contentValues = new ContentValues();

        contentValues.put("follow_status", status);
        db.update("groups1", contentValues, "id = ? ", new String[]{Integer.toString(id)});
        db.close();
    }

    public void delete_group(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
       // db.rawQuery("delete from groups where id =" + id, null);
//        //Logg("delete workshop", "delete");
       // db.delete("groups", "id = ? ", new String[]{Integer.toString(id)});

        db.execSQL("delete from groups1");

        db.close();
    }

    public void insert_testkey(String ans) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("testid", ans);

        db.insert("test_key", null, contentValues);

        db.close();
        //Logg("postquestion_insert","postquestion_insert");
    }

    public int get_testkey(String ans) {

        SQLiteDatabase db = this.getWritableDatabase();

        int i=0;

        Cursor cursor = null;

        try {

            cursor = db.rawQuery("SELECT testid FROM test_key WHERE testid=?", new String[] {ans});

            if(cursor.getCount() > 0)
            {
                i=1;
            }

            //Logg("common_table_get","common_table "+i);

            return i;

        }finally {
            if(cursor!=null)
            cursor.close();
        }
    }
}
