package com.scholar.engineering.banking.ssc.Staff.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.SupportSystemModel;
import com.scholar.engineering.banking.ssc.Staff.ViewSupportDetails;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SupportSystemAdapter extends RecyclerView.Adapter<SupportSystemAdapter.ViewHolder> {

    private Context context;
    private SupportSystemModel systemModel;

    public SupportSystemAdapter(Context context, SupportSystemModel systemModel) {
        this.context = context;
        this.systemModel = systemModel;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.support_recyler_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        SupportSystemModel.Assigned assignedModel = systemModel.getAssigned().get(position);
        if (systemModel.getAssigned().get(position)!=null) {
            holder.name.setText(assignedModel.getName());
            holder.ticketno.setText(assignedModel.getTicketno());
            holder.issue.setText(assignedModel.getIssue());
            holder.section.setText(assignedModel.getStdSection());
            holder.tv_class.setText(assignedModel.getStdClass());

            try {
                String date1 = assignedModel.getCreatedAt();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = null;
                try {
                    date = dateFormat.parse(date1);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                DateFormat formatter = new SimpleDateFormat("dd-MM-yy");
                String dateStr = formatter.format(date);
                holder.date.setText(dateStr);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public int getItemCount() {
        return systemModel.getAssigned().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, date, ticketno;
        TextView tv_class;
        TextView section;
        TextView issue;
        TextView teacherName;
        ImageView img_action;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.ticketno = (TextView) itemView.findViewById(R.id.txt_userName);
            this.date = (TextView) itemView.findViewById(R.id.textView29);
            this.name = (TextView) itemView.findViewById(R.id.textView31);
            this.tv_class = (TextView) itemView.findViewById(R.id.textView33);
            this.section = (TextView) itemView.findViewById(R.id.textView35);
            this.issue = (TextView) itemView.findViewById(R.id.text5);
            this.teacherName = (TextView) itemView.findViewById(R.id.text50);
            this.img_action = (ImageView) itemView.findViewById(R.id.img_view_more);

            img_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Bundle bundle = new Bundle();
                    Intent i =  new Intent(context, ViewSupportDetails.class);
                    bundle.putSerializable("assigned", systemModel.getAssigned().get(getAdapterPosition()));
                    i.putExtra("fromAssigned", SupportSystemAdapter.class.getSimpleName());
                    i.putExtras(bundle);
                    context.startActivity(i);
                }
            });
        }
    }

}
