package com.scholar.engineering.banking.ssc.Staff;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ChatModelClass;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.LeaveDetailModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.StaffListModel;
import com.scholar.engineering.banking.ssc.Staff.adapter.ChatAdapter;
import com.scholar.engineering.banking.ssc.Staff.adapter.StaffListAdapter;
import com.scholar.engineering.banking.ssc.databinding.FragmentStaffListBinding;
import com.scholar.engineering.banking.ssc.getset.RetrofitObjectAPI;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class StaffListFragment extends Fragment {

    FragmentStaffListBinding binding;
    StaffListAdapter staffListAdapter;
    NetworkConnection nw;
    List<StaffListModel.Datum> staffList;

    public StaffListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentStaffListBinding.inflate(inflater, container, false);

        nw = new NetworkConnection(getActivity());
        staffList = new ArrayList<>();
        getStaffList();

        binding.searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                getStaffSearch(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return binding.getRoot();
    }

    private void setData(List<StaffListModel.Datum> datumList){
        staffListAdapter = new StaffListAdapter(getActivity(),datumList);
        binding.recycleView.setHasFixedSize(true);
        binding.recycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.recycleView.setAdapter(staffListAdapter);
    }


    private void getStaffList() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<StaffListModel> call = service.getStaffList();

        Logg("staffList", Constants.URL_LV);

        call.enqueue(new Callback<StaffListModel>() {

            @Override
            public void onResponse(Call<StaffListModel> call, retrofit2.Response<StaffListModel> response) {
                if (response.isSuccessful()) {
                    staffList = response.body().getData();
                    setData(staffList);
                } else if (response.code()==500){
                    Toast.makeText(getActivity(), "Internal Server Error", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StaffListModel> call, Throwable t) {
                Log.e("fail",t.getMessage());
            }
        });
    }


    private void getStaffSearch(String abcd) {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<StaffListModel> call = service.getStaffSearch(abcd);

        Logg("staffListSearch", Constants.URL_LV + "staffData?" +abcd);

        call.enqueue(new Callback<StaffListModel>() {

            @Override
            public void onResponse(Call<StaffListModel> call, retrofit2.Response<StaffListModel> response) {
                if (response.isSuccessful()) {
                    staffList.clear();
                    setData(response.body().getData());
                } else if (response.code()==500){
                    Toast.makeText(getActivity(), "Internal Server Error", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StaffListModel> call, Throwable t) {
                Log.e("fail",t.getMessage());
            }
        });
    }


    public void internetconnection() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getStaffList();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

}