package com.scholar.engineering.banking.ssc.Challenge;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.HomeActivity;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.toast;

/**
 * Created by delaine on 18/1/18.
*/

public class Blocked_Users_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context c;
    JSONArray data;

    DisplayImageOptions options;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    ImageLoader imageLoader;

    Typeface font_demi;
    Typeface font_medium;

    public Blocked_Users_Adapter(Context c, JSONArray data) {

        this.c=c;
        this.data=data;


        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.user_default)
                .showImageForEmptyUri(R.drawable.user_default)
                .showImageOnFail(R.drawable.user_default)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        font_medium = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");
        font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");


    }

    public void addItem(JSONArray array1){
        for(int i=0;i<array1.length();i++){
            try {
                data.put(array1.getJSONObject(i));
            } catch (JSONException e) {

            }
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = null;

            final Context c=parent.getContext();

            RecyclerView.ViewHolder vh = null;

                    View  view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.blocked_user_adapter, parent, false);
                    vh= new ViewHolder1(view1);

            return vh;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

                ViewHolder1 vh1= (ViewHolder1) holder;
                setChallenge(vh1,position);

    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    public class ViewHolder1 extends RecyclerView.ViewHolder{
        // challenge layout

        TextView tv_unblock,tv_username,tv_college;

        LinearLayout layout;
        CircleImageView imageView;

        public ViewHolder1(View v) {
            super(v);

            tv_unblock=(TextView) v.findViewById(R.id.tv_unblock);
            tv_college=(TextView)v.findViewById(R.id.tv_college);
             tv_username=(TextView)v.findViewById(R.id.tv_username);
            imageView=(CircleImageView)v.findViewById(R.id.iv_challenge);

            tv_username.setTypeface(font_demi);
            tv_unblock.setTypeface(font_medium);
            tv_college.setTypeface(font_medium);

//            layout=(LinearLayout)v.findViewById(R.id.layout);

        }
    }

    public void setChallenge(ViewHolder1 vh1, final int p){

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(c));

        try {
            object=data.getJSONObject(p);

            vh1.tv_username.setText(object.getString("name"));
            vh1.tv_college.setText(object.getString("college"));

            if (object.getString("image").length()>4)
                imageLoader.getInstance().displayImage(object.getString("image"), vh1.imageView, options, animateFirstListener);

            vh1.tv_unblock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        object=data.getJSONObject(p);
                        group_alert(object.getString("opponent_id"),object.getString("name"),p);
                    } catch (JSONException e) {

                    }

                }
            });

        } catch (JSONException e) {

        }
    }

    JSONObject object;

    public void VolleyAccept(final String opp_id, final int p) {

        RequestQueue queue = Volley.newRequestQueue(c);
        String url="";
            url = Constants.URL_LV+"unblockuser";//reject accept api
        url = url.replace(" ", "%20");
        Logg("name", url);

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {

                Logg("response", s);
                try
                {

                    if(s.length()>0)
                    {
                        JSONObject arg0=new JSONObject(s);
                        if(arg0.getString("status").equalsIgnoreCase("true")) {
                                data.remove(p);
                                notifyDataSetChanged();
                        }
                        else if(arg0.getString("status").equalsIgnoreCase("false")){
                            toast(c,arg0.getString("message"));
                        }
                    }
                }

                catch (Exception e)
                {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                toast(c,CommonUtils.volleyerror(volleyError));
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", Constants.User_Email);
                map.put("opponent_id",opp_id);

                Logg("map",map.toString());

                return map;

            }
        };

        int socketTimeout = 20000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);

    }


    public void group_alert(final String opp_id,String name, final int p){

        final Dialog dialog = new Dialog(c);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.ubblock_user_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView tv_text=(TextView)dialog.findViewById(R.id.text);

        TextView tv_yes=(TextView)dialog.findViewById(R.id.tv_yes);
        TextView tv_cancel=(TextView)dialog.findViewById(R.id.tv_cancel);

        tv_text.setText("Do you want to unblock "+name);

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });

        tv_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               VolleyAccept(opp_id, p);
               dialog.dismiss();

            }
        });

    }


}

