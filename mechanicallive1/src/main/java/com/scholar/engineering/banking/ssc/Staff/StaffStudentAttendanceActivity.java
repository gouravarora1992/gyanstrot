package com.scholar.engineering.banking.ssc.Staff;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.SubmitAttendanceModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ViewAttendanceModel;
import com.scholar.engineering.banking.ssc.Staff.adapter.StaffStudentAttendanceAdapter;
import com.scholar.engineering.banking.ssc.databinding.ActivityStaffStudentAttendanceBinding;
import com.scholar.engineering.banking.ssc.getset.RetrofitObjectAPI;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class StaffStudentAttendanceActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityStaffStudentAttendanceBinding binding;
    NetworkConnection nw;
    UserSharedPreferences preferences;
    UserSharedPreferences playerpreferences;
    Integer id;
    String currentDate, assignClass, assignSection, values, currentTime;
    StaffStudentAttendanceAdapter mAdapter;
    List<String> selectedItem = new ArrayList<String>();
    JSONObject row = new JSONObject();
    public static CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_staff_student_attendance);

        nw = new NetworkConnection(StaffStudentAttendanceActivity.this);
        playerpreferences=new UserSharedPreferences(StaffStudentAttendanceActivity.this,"playerpreferences");
        preferences=UserSharedPreferences.getInstance(StaffStudentAttendanceActivity.this);
        id = preferences.getId();
        assignClass = preferences.getAssignClass();
        assignSection = preferences.getAssignSection();

        binding.txtClass.setText(assignClass);
        binding.txtSection.setText(assignSection);

        Log.e("id===>>",""+id);
        checkBox = findViewById(R.id.select_checkbox);
        binding.imgBack.setOnClickListener(this);
        binding.button7.setOnClickListener(this);

        getCurrentDate();
        getcurrentTime();
        getViewAttendance();
    }

    private void getCurrentDate(){
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        currentDate = df.format(c);
        binding.txtDate.setText(currentDate);
        Log.e("datee",currentDate);
    }

    public void getcurrentTime(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
        currentTime = dateFormat.format(new Date()).toString();
        Log.e("formattedTime",currentTime);
        binding.txtTime.setText(currentTime);
    }


    private void setData(ViewAttendanceModel list) {
        if (list.getPresentstudent().size()==0){
            for (int i=0;i<list.getData().size();i++){
                list.getPresentstudent().add(list.getData().get(i).getStdRoll());
            }
        }
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        binding.recyclerView3.setLayoutManager(mLayoutManager);
        mAdapter = new StaffStudentAttendanceAdapter(this, list);
        binding.recyclerView3.setAdapter(mAdapter);
    }

    public void notifyData(){
        binding.recyclerView3.post(new Runnable()
        {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
            }
        });

    }

    private void getViewAttendance() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<ViewAttendanceModel> call = service.getViewStudentAttendance(assignClass, assignSection, "2021-05-08");

        Logg("viewAttendance", Constants.URL_LV + "data?classss=" + assignClass
                + "&section=" +assignSection + "$date" + currentDate);

        call.enqueue(new Callback<ViewAttendanceModel>() {

            @Override
            public void onResponse(Call<ViewAttendanceModel> call, retrofit2.Response<ViewAttendanceModel> response) {
                if (response.isSuccessful()) {
                    Log.e("response",""+response.body().getPresentstudent().size());
                    setData(response.body());
                } else {
                    Toast.makeText(StaffStudentAttendanceActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ViewAttendanceModel> call, Throwable t) {
                Log.e("fail",t.getMessage());
            }
        });
    }


    private void getAttendanceSubmit() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);

        for (int i=0;i<selectedItem.size();i++) {
            try {
                row.put(selectedItem.get(i), "1");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        Call<SubmitAttendanceModel> call = service.getAttendanceSubmit(assignClass,assignSection,"2021-05-08", row);
        Logg("submitAttendance>>>", Constants.URL_LV + "#classname" + assignClass + "&classsection" + assignSection +
                "&date" + currentDate + "studentData?" +row);

        call.enqueue(new Callback<SubmitAttendanceModel>() {

            @Override
            public void onResponse(Call<SubmitAttendanceModel> call, retrofit2.Response<SubmitAttendanceModel> response) {
                if (response.isSuccessful()) {
                    Log.e("response","" + response.body().getMessage());
                    finish();
                    Toast.makeText(StaffStudentAttendanceActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(StaffStudentAttendanceActivity.this, "" + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SubmitAttendanceModel> call, Throwable t) {
                Log.e("fail",t.getMessage());
            }
        });
    }


    public void internetconnection() {

        final Dialog dialog = new Dialog(StaffStudentAttendanceActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getViewAttendance();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.button7:
                if (mAdapter.getData().size()>0) {
                    selectedItem = mAdapter.getData();
                    Log.e("selectedItem>>",""+mAdapter.getData());
                    Log.e("sizeee>>>",""+mAdapter.getData().size());
                    getAttendanceSubmit();
                } else {
                    Toast.makeText(StaffStudentAttendanceActivity.this,"Please mark attendance first",Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }

}