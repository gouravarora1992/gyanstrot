package com.scholar.engineering.banking.ssc.Challenge;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.scholar.engineering.banking.ssc.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by surender on 3/14/2017.
*/

public class Challenge_Result_List_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context c;
    JSONArray user_arr,opp_arr,que_arr;
    String challenge_id;

    Typeface font_demi;
    Typeface font_medium;

    public Challenge_Result_List_Adapter(Context c, JSONArray que_arr, JSONArray user_arr, JSONArray opp_arr, String challenge_id) {
        this.c = c;
        this.que_arr=que_arr;
        this.user_arr=user_arr;
        this.opp_arr=opp_arr;
        this.challenge_id=challenge_id;

        font_medium = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");
        font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v0=null;

        v0 = inflater.inflate(R.layout.challenge_result_list_item, viewGroup, false);

        viewHolder = new ViewHolder0(v0);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

                ViewHolder0 vh0 = (ViewHolder0) viewHolder;

                setComment(vh0, position);
    }

    @Override
    public int getItemCount() {
        return que_arr.length();
    }

    private void setComment(final ViewHolder0 vh0, final int p) {

        try {
            if(user_arr.getString(p).equalsIgnoreCase("W")|user_arr.getString(p).equalsIgnoreCase("U")){
                vh0.iv_ans_lu.setImageResource(R.drawable.ic_cancel);
            }
            else
                vh0.iv_ans_lu.setImageResource(R.drawable.ic_checked);

            if(opp_arr.getString(p).equalsIgnoreCase("W")|opp_arr.getString(p).equalsIgnoreCase("U")){
                vh0.iv_ans_opp.setImageResource(R.drawable.ic_cancel);
            }
            else
                vh0.iv_ans_opp.setImageResource(R.drawable.ic_checked);

            vh0.tv_que_no.setText((p+1)+"");

        } catch (JSONException e) {

        }

        vh0.iv_seeresponse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Intent in = new Intent(c, See_Response_Question.class);
                    in.putExtra("question_id",que_arr.getInt(p));
                    in.putExtra("challenge_id",challenge_id);
                    in.putExtra("type","challenge");
                    c.startActivity(in);
                } catch (JSONException e) {

                }
            }
        });

    }

    public class ViewHolder0 extends RecyclerView.ViewHolder {

        LinearLayout layout;
        TextView tv_que_no;
        ImageView iv_ans_lu,iv_ans_opp,iv_seeresponse;
        public ViewHolder0(View b) {
            super(b);
            iv_ans_lu=(ImageView)b.findViewById(R.id.imageView_lu);
            iv_ans_opp=(ImageView)b.findViewById(R.id.imageView_opp);
            tv_que_no=(TextView)b.findViewById(R.id.tv_question_no);
            iv_seeresponse=(ImageView)b.findViewById(R.id.iv_seeresponse);
            tv_que_no.setTypeface(font_medium);

        }
    }

}
