package com.scholar.engineering.banking.ssc.Staff;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.SupportSystemModel;
import com.scholar.engineering.banking.ssc.Staff.adapter.SupportSystemAdapter;
import com.scholar.engineering.banking.ssc.getset.RetrofitObjectAPI;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import static android.content.Context.MODE_PRIVATE;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class SupportAssignedFragment extends Fragment {

    RecyclerView recyclerView_assigned;
    TextView txt_not_found;
    NetworkConnection nw;
    Integer id;
    UserSharedPreferences preferences;
    UserSharedPreferences playerpreferences;

    public SupportAssignedFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_support_assigned, container, false);

        recyclerView_assigned = v.findViewById(R.id.support_assigned);
        txt_not_found = v.findViewById(R.id.tv_notfound);
        nw = new NetworkConnection(getActivity());
        playerpreferences=new UserSharedPreferences(getActivity(),"playerpreferences");
        preferences=UserSharedPreferences.getInstance(getActivity());

        id = preferences.getId();
        Log.e("id>>",""+id);

        getSupportSystem();

        return v;
    }

    private void setUpData(SupportSystemModel supportSystemModel) {
        if (supportSystemModel.getAssigned().size()>0) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView_assigned.setLayoutManager(linearLayoutManager);
            SupportSystemAdapter supportSystemAdapter = new SupportSystemAdapter(getActivity(), supportSystemModel);
            recyclerView_assigned.setAdapter(supportSystemAdapter);
        } else {
            recyclerView_assigned.setVisibility(View.GONE);
            txt_not_found.setVisibility(View.VISIBLE);
        }
    }


    private void getSupportSystem() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<SupportSystemModel> call = service.getSupportSystem(id);

        Logg("urlapiassignedsupportsystem", Constants.URL_LV + "data?id=" + id);

        call.enqueue(new Callback<SupportSystemModel>() {

            @Override
            public void onResponse(Call<SupportSystemModel> call, retrofit2.Response<SupportSystemModel> response) {
                if (response.isSuccessful()) {
                    if (response.body().getMessage().equalsIgnoreCase("success")) {
                        setUpData(response.body());
                        Log.e("assignedData", String.valueOf(response.body().getAssigned()));
                    } else if (response.body().getMessage().equalsIgnoreCase("Not permission")) {
                        Toast.makeText(getActivity(), "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SupportSystemModel> call, Throwable t) {
                Log.e("fail",t.getMessage());
            }
        });
    }


    public void internetconnection() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getSupportSystem();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }


}