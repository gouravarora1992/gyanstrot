package com.scholar.engineering.banking.ssc;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.scholar.engineering.banking.ssc.adapter.Home_RecyclerViewAdapter2;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.newScreens.StudentProfile;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.DatabaseHandler;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.Utility.getDate;

public class Profile_common extends AppCompatActivity implements OnClickListener
{
	DatabaseHandler dbh;
	int index=0;
	String st_email;
	ArrayList<Home_getset> homedata;
	RecyclerView homelist;
	TextView tv_editprofile,tv_adddocument,tv_name,tv_collage,tv_state,tv_comment,tv_reward,tv_phone,tv_postby;
	Home_RecyclerViewAdapter2 adapter;
	CircleImageView iv_user;
	ProgressDialog progress;
	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
	private DisplayImageOptions options;
	LinearLayout lay_phone;
	RelativeLayout footerlayout;
	TextView loadmore;
	ProgressWheel wheelbar;

	NetworkConnection nw;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.profile_common);

		nw=new NetworkConnection(this);

		Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("Profile");
		Utility.applyFontForToolbarTitle(toolbar,this);
		TextView headertext=(TextView)findViewById(R.id.headertextid);
		headertext.setVisibility(View.GONE);

		dbh=new DatabaseHandler(Profile_common.this);
		homelist=(RecyclerView)findViewById(R.id.recyclerView);
		LinearLayoutManager mLayoutManager = new LinearLayoutManager(Profile_common.this);
		homelist.setLayoutManager(mLayoutManager);
		homelist.setHasFixedSize(true);
		homelist.setNestedScrollingEnabled(false);

		st_email=getIntent().getStringExtra("email");

		tv_editprofile=(TextView)findViewById(R.id.tv_editprofle);
		tv_adddocument=(TextView)findViewById(R.id.tv_adddocuments);
		tv_name=(TextView)findViewById(R.id.tv_username);
		tv_collage=(TextView)findViewById(R.id.tv_collage);
		tv_phone=(TextView)findViewById(R.id.tv_phone);
		tv_state=(TextView)findViewById(R.id.tv_state);
		tv_comment=(TextView)findViewById(R.id.tv_comment);
		tv_reward=(TextView)findViewById(R.id.tv_reward);
		tv_postby=(TextView)findViewById(R.id.tv_postby);

		lay_phone=(LinearLayout)findViewById(R.id.lay_phone);

		iv_user=(CircleImageView)findViewById(R.id.iv_user);


		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.user_default)
				.showImageForEmptyUri(R.drawable.user_default)
				.showImageOnFail(R.drawable.user_default)
				.cacheInMemory(false)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.build();


//		nw=new NetworkConnection(Profile_common.this);

		homedata=new ArrayList<>();


		tv_name.setText("");
		tv_collage.setText("");
		tv_phone.setText("");
		tv_state.setText("");
		tv_comment.setText("");
		tv_reward.setText("");


		if(nw.isConnectingToInternet())
		getData();
		else {
			Toast.makeText(Profile_common.this, "No Internet Connection", Toast.LENGTH_LONG).show();
		finish();
		}

		footerlayout=(RelativeLayout)findViewById(R.id.footer_layout);
		loadmore=(TextView)findViewById(R.id.moreButton);
		wheelbar=(ProgressWheel)findViewById(R.id.progress_wheel);

		index=0;

		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());

		tv_editprofile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent in = new Intent(Profile_common.this, StudentProfile.class);
				startActivity(in);
			}
		});


		footerlayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

				Home_getset	home= (Home_getset) homedata.get(homedata.size()-2);

				index= home.getId();

					getData();

				loadmore.setText("");
				wheelbar.setVisibility(View.VISIBLE);

			}
		});

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Logg("call_onDestroy()","Call onDestroy()");
		deleteCache(this);
	}

	public void deleteCache(Context context) {
		try {
			File dir = context.getCacheDir();
			deleteDir(dir);
		} catch (Exception e) {}
	}

	public boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
			return dir.delete();
		} else if(dir!= null && dir.isFile()) {
			return dir.delete();
		} else {
			return false;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(item.getItemId()==android.R.id.home)
			finish();

		return super.onOptionsItemSelected(item);
	}

	public void setData(JSONObject obj){
		try {

			tv_name.setText(obj.getString("name"));
			tv_collage.setText(obj.getString("college"));
			tv_phone.setText(obj.getString("mobile"));
			tv_state.setText(obj.getString("state"));
			tv_comment.setText(obj.getInt("comment")+" Comments");
			tv_reward.setText(obj.getInt("reward")+" Rewards");
			tv_postby.setText("Post by "+obj.getString("name"));

			if(obj.getString("email").equals(Constants.User_Email)) {
				tv_editprofile.setVisibility(View.VISIBLE);
				lay_phone.setVisibility(View.VISIBLE);
			}
			else {
				lay_phone.setVisibility(View.GONE);
				tv_editprofile.setVisibility(View.GONE);
			}

			ImageLoader imageLoader = ImageLoader.getInstance();
			imageLoader.init(ImageLoaderConfiguration.createDefault(Profile_common.this));

			if(!obj.getString("image").equalsIgnoreCase("")) {
				imageLoader.getInstance().displayImage(obj.getString("image"), iv_user, options, animateFirstListener);
			}


		} catch (JSONException e) {

		}
	}

	public void getData()
	{

		if(!nw.isConnectingToInternet()) {
//            swipeRefreshLayout.setRefreshing(false);
			internetconnection(0);
			return;
		}

			progress = ProgressDialog.show(Profile_common.this, null, null, true);
			progress.setContentView(R.layout.progressdialog);
			progress.setCanceledOnTouchOutside(false);
			progress.setCancelable(false);
			progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		RequestQueue queue= Volley.newRequestQueue(Profile_common.this);
		JSONObject obj=new JSONObject();
		String url= Constants.URL+"newapi2_04/getusercomment.php?email="+ st_email+"&index="+index;

		//Logg("checkversion_url",url);

		JsonObjectRequest json=new JsonObjectRequest(Request.Method.POST, url, obj,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res) {

				// TODO Auto-generated method stub
				try {

					if(res.has("userdetails")){
						setData(res.getJSONObject("userdetails"));
					}

					if(res.has("data")){

						JSONArray jr = res.getJSONArray("data");

						if (jr.length() < 10)
							footerlayout.setVisibility(View.GONE);
						else {
							footerlayout.setVisibility(View.VISIBLE);
							loadmore.setText("Load Previous..");
							wheelbar.setVisibility(View.GONE);
						}

						for (int i = 0; i < jr.length(); i++) {
							JSONObject ob = jr.getJSONObject(i);

							String time = getDate(ob.getString("timestamp"));

							Home_getset object=new Home_getset();
							object.setId(ob.getInt("id"));
							object.setTimestamp(time);
							object.setLikecount(ob.getInt("likes"));
							object.setCommentcount(ob.getInt("comment"));
							object.setViewcount(ob.getInt("view"));
							object.setUid(ob.getString("uid"));
							object.setPosttype(ob.getString("posttype"));
							object.setGroupid(ob.getString("groupid"));
							object.setFieldtype(ob.getString("field_type"));
							object.setJsonfield(ob.getString("jsondata"));
							object.setLikestatus(ob.getString("likestatus"));
							object.setPosturl(ob.getString("posturl"));
							object.setPostdescription(ob.getString("post_description"));
							if(ob.has("AppVersion"))
								object.setAppVersion(ob.getString("AppVersion"));
							homedata.add(object);
						}

						//Logg("data.size", homedata.size() + "");

//						addNativeExpressAds();
//						setUpAndLoadNativeExpressAds();
						adapter = new Home_RecyclerViewAdapter2(Profile_common.this, homedata,"Profile_common");
						homelist.setAdapter(adapter);

					}
					else
						footerlayout.setVisibility(View.GONE);

					if(progress!=null)
						progress.dismiss();
				}
				catch (JSONException e) {
					footerlayout.setVisibility(View.GONE);

					if(progress!=null)
						progress.dismiss();
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				//Logg("checkversion error", String.valueOf(arg0));
				footerlayout.setVisibility(View.GONE);

				if(progress!=null)
					progress.dismiss();
			}
		});
		queue.add(json);
	}

	public void internetconnection(final int i){

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.network_alert);
		dialog.show();

		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
		TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

		tv_tryagain.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.dismiss();

					getData();

//				getData();

			}
		});

		iv_cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

	}

	@Override
	public void onClick(View v) {

	}
}
