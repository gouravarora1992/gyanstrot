package com.scholar.engineering.banking.ssc;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;

import androidx.core.widget.NestedScrollView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.scholar.engineering.banking.ssc.adapter.Replies_on_group_comment_adap;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.ImageCompression;
import com.scholar.engineering.banking.ssc.utils.ImageUploading;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.toast;
import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;
import static com.scholar.engineering.banking.ssc.utils.Constants.imageFilePath;
import static com.scholar.engineering.banking.ssc.utils.Constants.st_base46;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Comment_Common_homepage extends AppCompatActivity {

    int psid = 0, index = 0;
    RecyclerView replist;
    EditText comtext;
    ProgressDialog progress;
    ImageView compost, camera_img;
    Replies_on_group_comment_adap adap;
    Boolean cb = false;
    String st_imagename, posttype = "1";
    LinearLayoutManager mLayoutManager;
    RelativeLayout loadmore_lay;
    TextView moreButton, tv_header;
    JSONArray jsonArray;
    NestedScrollView nestedscroll;
    Toolbar toolbar;
    SwipeRefreshLayout swipeRefreshLayout;
    NetworkConnection nw;
    ImageUploading ab;
    private final int REQUEST_CODE_CLICK_IMAGE = 1002, REQUEST_CODE_GALLERY_IMAGE = 1003;
    RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_comment_homepage);

        st_base46 = "";
        Constants.imageFilePath = CommonUtils.getFilename();
        nw = new NetworkConnection(this);

        progress = ProgressDialog.show(Comment_Common_homepage.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        queue = Volley.newRequestQueue(getApplicationContext());

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Comments");

        Utility.applyFontForToolbarTitle(toolbar, this);

        nestedscroll = (NestedScrollView) findViewById(R.id.nestedscroll);
        replist = (RecyclerView) findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(Comment_Common_homepage.this);
        replist.setLayoutManager(mLayoutManager);
        replist.setNestedScrollingEnabled(false);
        replist.setHasFixedSize(true);

        if (Build.VERSION.SDK_INT >= 24) {
            try {
                java.lang.reflect.Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
            }
        }

        comtext = (EditText) findViewById(R.id.comtextid);
        compost = (ImageView) findViewById(R.id.postcomid);
        camera_img = (ImageView) findViewById(R.id.iv_camera_id);
        tv_header = (TextView) findViewById(R.id.headertextid);
        tv_header.setVisibility(View.GONE);

        moreButton = (TextView) findViewById(R.id.moreButton);
        loadmore_lay = (RelativeLayout) findViewById(R.id.footer_layout);

        jsonArray = new JSONArray();

        psid = getIntent().getIntExtra("postid", 0);

        posttype = getIntent().getStringExtra("posttype");


        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                index = 0;
                volleylist();
            }
        });

        ab = new ImageUploading(this, camera_img);
        ab.createBottomSheetDialog();

        camera_img.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ab.showOptionBottomSheetDialog();
            }
        });

        volleylist();

        compost.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String s = comtext.getText().toString();
                if (s.length() < 1) {
                    Toast.makeText(getApplicationContext(), "Enter comments", Toast.LENGTH_SHORT).show();
                } else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(comtext.getWindowToken(), 0);
                    cb = true;

                    if (st_base46.length() > 1)
                        imagevolley();
                    else
                        volleycominsert();
                }
            }
        });


        moreButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    if (jsonArray.length() > 0) {

                        JSONObject obj = jsonArray.getJSONObject(0);

                        index = obj.getInt("id");
                    } else {
                        index = 0;
                    }

                    volleylist();

                } catch (JSONException e) {

                }
            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_CLICK_IMAGE) {

            new ImageCompression(camera_img).execute(imageFilePath);

        } else if (requestCode == REQUEST_CODE_GALLERY_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

//            String[] projection = {MediaStore.Images.Media.DATA};
//            Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
//            cursor.moveToFirst();
//            int columnIndex = cursor.getColumnIndex(projection[0]);
            File myFile = new File(uri.getPath());

            final String picturePath = myFile.getAbsolutePath();//cursor.getString(columnIndex); // returns null
            // cursor.close();

            //copy the selected file of gallery into app's sdcard folder and perform the compression operations on it.
            //And override the original image with the newly resized image.

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        CommonUtils.copyFile(picturePath, imageFilePath);
                    } catch (IOException e) {

                    }
                }
            });

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bmp = BitmapFactory.decodeFile(imageFilePath, options);

            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;

            Logg("actualheight2", actualHeight + " " + actualWidth);

            if (actualWidth > 600 | actualHeight > 600) {
                new ImageCompression(camera_img).execute(imageFilePath);
            } else {

                getContentResolver().notifyChange(uri, null);
                ContentResolver cr = getContentResolver();
                Bitmap bitmap;

                try {
                    bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, uri);
                    //Logg("bitmap_h_w",bitmap.getHeight()+" "+bitmap.getWidth()+"");
                    camera_img.setImageBitmap(bitmap);

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();

                    st_base46 = Base64.encodeToString(byteArray, Base64.DEFAULT);
//                    return Base64.encodeToString(byteArray, Base64.DEFAULT);

                    Logg("base64", st_base46);

                } catch (Exception e) {

                }
            }
        }
    }

    public void volleylist() {
        progress.show();

        if (!nw.isConnectingToInternet()) {

            swipeRefreshLayout.setRefreshing(false);
            internetconnection(0);
            return;
        }


        JSONObject obj = new JSONObject();
        try {
            obj.put("postid", psid);
            obj.put("index", index);
            obj.put("email", Constants.User_Email);
            obj.put("admission_no", Constants.addmissionno);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Constants.URL_LV + "get_common_comment";
        JsonObjectRequest json = new JsonObjectRequest(Method.POST, url, obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                try {
                    Logg("response comment",res+"");
                    if ((progress != null) && progress.isShowing()) {
                        progress.dismiss();
                    }

                    progress.dismiss();
                    JSONArray jr = res.getJSONArray("data");

                    if (jr.length() >= 10)
                        loadmore_lay.setVisibility(View.VISIBLE);
                    else
                        loadmore_lay.setVisibility(View.GONE);

                    if (index == 0) {
                        jsonArray = new JSONArray(new ArrayList<String>());
                        jsonArray = jr;
                        adap = new Replies_on_group_comment_adap(Comment_Common_homepage.this, jsonArray);
                        replist.setAdapter(adap);
                        nestedscroll.fullScroll(View.FOCUS_DOWN);
                    } else {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            jr.put(jsonArray.getJSONObject(i)); //new JSONArray(new ArrayList<>());
                        }

                        jsonArray = new JSONArray(new ArrayList<String>());
                        jsonArray = jr;

                        adap = new Replies_on_group_comment_adap(Comment_Common_homepage.this, jr);
                        replist.setAdapter(adap);
                        nestedscroll.fullScroll(View.FOCUS_DOWN);
                    }
                    swipeRefreshLayout.setRefreshing(false);
                } catch (JSONException e) {
                    progress.dismiss();
                    swipeRefreshLayout.setRefreshing(false);
                    loadmore_lay.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                progress.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                loadmore_lay.setVisibility(View.GONE);
            }
        });
        json.setShouldCache(false);
        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        json.setRetryPolicy(policy);
        queue.add(json);
    }

    public void imagevolley() {
        if (!nw.isConnectingToInternet()) {
            swipeRefreshLayout.setRefreshing(false);
            internetconnection(1);
            return;
        }
        progress.show();
        st_imagename = String.valueOf(System.currentTimeMillis());

        String url = Constants.URL_LV + "insert_common_comment_image";
        Logg("insert_common_comment_image", url);

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                progress.dismiss();

                try {
                    if (s.length() > 0) {
                        JSONObject obj = new JSONObject(s);

                        if (obj.getString("status").equalsIgnoreCase("success")) {

                            Toast.makeText(Comment_Common_homepage.this, "Upload Successfully", Toast.LENGTH_LONG).show();
                            comtext.setText("");
                            st_base46 = "";
                            camera_img.setImageResource(R.drawable.camera40);
                            index = 0;
                            volleylist();
                        } else if (obj.getString("status").equalsIgnoreCase("block"))
                            Toast.makeText(Comment_Common_homepage.this, obj.getString("response"), Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(Comment_Common_homepage.this, "Something is wrong can't post comment", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    progress.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(Comment_Common_homepage.this, CommonUtils.volleyerror(volleyError));
                progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("st_base64", st_base46);
                params.put("imagename", st_imagename);
                params.put("postid", psid + "");
                params.put("posttype", posttype);
                params.put("email", User_Email);
                params.put("comment", comtext.getText().toString());
                params.put("admission_no", Constants.addmissionno);
                return params;
            }
        };
        request.setShouldCache(false);
        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void volleycominsert() {
        if (!nw.isConnectingToInternet()) {
            swipeRefreshLayout.setRefreshing(false);
            internetconnection(2);
            return;
        }
        progress.show();

        String url = Constants.URL_LV + "insert_common_comment";
        Logg("insert_common_comment", url);

        StringRequest obj1 = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String res) {
                progress.dismiss();
                int size = res.length();
                if (size > 0) {

                    try {
                        JSONObject obj = new JSONObject(res);

                        if (obj.getString("status").equalsIgnoreCase("success")) {
                            Toast.makeText(Comment_Common_homepage.this, "Post Successfully", Toast.LENGTH_LONG).show();
                            comtext.setText("");
                            index = 0;
                            volleylist();
                        } else if (obj.getString("status").equalsIgnoreCase("block"))
                            Toast.makeText(Comment_Common_homepage.this, obj.getString("response"), Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(Comment_Common_homepage.this, obj.getString("response"), Toast.LENGTH_LONG).show();

                    } catch (JSONException e) {
                        progress.dismiss();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                toast(Comment_Common_homepage.this, CommonUtils.volleyerror(e));
                progress.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("postid", psid + "");
                params.put("comment", comtext.getText().toString());
                params.put("email", Constants.User_Email);
                params.put("admission_no", Constants.addmissionno);
                return params;
            }
        };

        obj1.setShouldCache(false);
        int socketTimeout = 0;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        obj1.setRetryPolicy(policy);

        queue.add(obj1);

    }

    public void internetconnection(final int i) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

//        JSONObject json;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();
                if (i == 2) {
                    volleycominsert();
                } else if (i == 1) {
                    imagevolley();
                } else {
                    swipeRefreshLayout.setRefreshing(true);
                    volleylist();
                }

            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logg("call_onDestroy()", "Call onDestroy()");
        deleteCache(this);
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

}
