package com.scholar.engineering.banking.ssc.newScreens;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.scholar.engineering.banking.ssc.BuildConfig;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Swt on 9/16/2017.
 */

public class YoutubeVideoAdapter extends RecyclerView.Adapter<YoutubeVideoAdapter.ViewHolder>
{
    Context context;
    UserSharedPreferences userSharedPreferences;

    ArrayList<ItemVideo> itemVideos;

    AlertDialog.Builder alertdialog;

    public YoutubeVideoAdapter(ArrayList<ItemVideo> itemVideos, Context context) {

       this.itemVideos = itemVideos;
       this.context = context;
       userSharedPreferences=new UserSharedPreferences(context);
    }


    @Override
    public YoutubeVideoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.videosapdateritemyoutube, parent, false);
        return new ViewHolder(itemView) ;
    }
    public static final String DEVELOPER_KEY = BuildConfig.YOUTUBEAPIKEY;
    @Override
    public void onBindViewHolder(final YoutubeVideoAdapter.ViewHolder holder, final int position)
    {
        holder.title.setText(itemVideos.get(position).getTitle());
        holder.desc.setText(Html.fromHtml(itemVideos.get(position).getVideo_desc()));
        holder.playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(YouTubeApiServiceUtil.isYouTubeApiServiceAvailable(context).equals(YouTubeInitializationResult.SUCCESS)){
                    //This means that your device has the Youtube API Service (the app) and you are safe to launch it.

                    ItemVideo item = itemVideos.get(position);
                    Log.e("video",itemVideos.get(position).getVideoId());
                    Intent intent = YouTubeStandalonePlayer.createVideoIntent((NotesVideos)context,
                            DEVELOPER_KEY, item.getVideoId(),
                            100,     //after this time, video will start automatically
                            true,               //autoplay or not
                            false);

                    ((NotesVideos)context).startActivity(intent);
                }else{
                    // Log the outcome, take necessary measure, like playing the video in webview :)
                    Toast.makeText(context, "install youtube", Toast.LENGTH_SHORT).show();
                }

            }
        });

        holder.youTubeThumbnailView.initialize(DEVELOPER_KEY,new Youtube(context,itemVideos.get(position),holder.relativeLayoutOverYouTubeThumbnailView));

    }

    @Override
    public int getItemCount() {
        Log.e("itemsize",itemVideos.size()+"");
        return itemVideos.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        RelativeLayout relativeLayoutOverYouTubeThumbnailView;
        YouTubeThumbnailView youTubeThumbnailView;
        ImageView playButton,likebtn;
        TextView title,likecount,desc,time,likevideo;
        LinearLayout lkelayout,sharelyt,share;

        public ViewHolder(View itemView)
        {
            super(itemView);

            playButton=(ImageView)itemView.findViewById(R.id.btnYoutube_player);
            title = (TextView)itemView.findViewById(R.id.videotitle);
            time = (TextView)itemView.findViewById(R.id.time);
            desc = (TextView)itemView.findViewById(R.id.desc);
            relativeLayoutOverYouTubeThumbnailView = (RelativeLayout) itemView.findViewById(R.id.relativeLayout_over_youtube_thumbnail);
            youTubeThumbnailView = (YouTubeThumbnailView) itemView.findViewById(R.id.youtube_thumbnail);

        }

        @Override
        public void onClick(View v) {
            ItemVideo item = itemVideos.get(getLayoutPosition());
            Intent intent = YouTubeStandalonePlayer.createVideoIntent((NotesVideos) context,
                    DEVELOPER_KEY, item.getVideoId(),
                    100,     //after this time, video will start automatically
                    true,               //autoplay or not
                    false);

            context.startActivity(intent);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
