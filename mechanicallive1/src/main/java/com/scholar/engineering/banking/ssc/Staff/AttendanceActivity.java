package com.scholar.engineering.banking.ssc.Staff;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.databinding.ActivityAttendanceBinding;

public class AttendanceActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityAttendanceBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_attendance);

        binding.imgBack.setOnClickListener(this);
        binding.addAttendanceCardview.setOnClickListener(this);
        binding.viewAttendanceCardview.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.view_attendance_cardview:
                startActivity(new Intent(AttendanceActivity.this, ViewStudentAttendanceActivity.class));
                break;

            case R.id.add_attendance_cardview:
                startActivity(new Intent(AttendanceActivity.this, StaffStudentAttendanceActivity.class));
                break;
        }
    }
}