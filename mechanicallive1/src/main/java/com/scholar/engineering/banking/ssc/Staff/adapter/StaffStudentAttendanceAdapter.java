package com.scholar.engineering.banking.ssc.Staff.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ViewAttendanceModel;
import com.scholar.engineering.banking.ssc.Staff.StaffStudentAttendanceActivity;
import com.scholar.engineering.banking.ssc.databinding.ItemStudentAttendanceBinding;

import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class StaffStudentAttendanceAdapter extends RecyclerView.Adapter<StaffStudentAttendanceAdapter.ViewHolder> {

    private Context context;
    private static ViewAttendanceModel studentList;
    Boolean status;
    private static List<String> presentStudent;
    private List<String> selectedStudent;

    public StaffStudentAttendanceAdapter(Context context, ViewAttendanceModel studentList) {
        this.context = context;
        this.studentList = studentList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemStudentAttendanceBinding binding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                (R.layout.item_student_attendance),parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        holder.binding.txtName.setText(studentList.getData().get(position).getStudentName());

        Log.e("presentStudent>>>>",""+studentList.getPresentstudent());

            if (studentList.getPresentstudent().contains(studentList.getData().get(position).getStdRoll())) {
                status = true;
                holder.binding.imgNitification.setImageResource(R.drawable.slider_on);
            } else {
                status = false;
                holder.binding.imgNitification.setImageResource(R.drawable.slider_off);
            }



  //      holder.binding.imgNitification.setChecked(status);

//        holder.binding.imgNitification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//
//                status = isChecked;
//                Log.e("status",""+status);
//
//                if (status) {
//                    studentList.getPresentstudent().add(0,studentList.getData().get(position).getStdRoll());
////                    studentList.setPresentstudent(selectedStudent);
////                    holder.binding.imgNitification.setChecked(status);
//                } else {
//                    studentList.getPresentstudent().remove(studentList.getPresentstudent().indexOf(studentList.getData().get(position).getStdRoll()));
////                    studentList.getPresentstudent().remove(selectedStudent);
////                    holder.binding.imgNitification.setChecked(status);
//                }
//                Log.e("list", "" + studentList.getPresentstudent()+" - "+studentList.getPresentstudent().indexOf(studentList.getData().get(position).getStdRoll()));
//                Log.e("size", "" + studentList.getPresentstudent().size());
//                ((StudentAttendanceActivity)context).notifyData();
//            }
//        });

        holder.binding.imgNitification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("size>>>>",""+studentList.getPresentstudent().size());
                if (studentList.getPresentstudent().contains(studentList.getData().get(position).getStdRoll())) {
                    studentList.getPresentstudent().remove(studentList.getPresentstudent().indexOf(studentList.getData().get(position).getStdRoll()));
                } else {
                    studentList.getPresentstudent().add(0,studentList.getData().get(position).getStdRoll());
                    //      Log.e("size", "" + studentList.getPresentstudent().size());
                }

                Log.e("list", "" + studentList.getPresentstudent()+" - "+studentList.getPresentstudent().indexOf(studentList.getData().get(position).getStdRoll()));
                Log.e("size", "" + studentList.getPresentstudent().size());
                notifyDataSetChanged();
                }

        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return studentList.getData().size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public ItemStudentAttendanceBinding binding;

        public ViewHolder(@NonNull final ItemStudentAttendanceBinding binding) {
            super(binding.getRoot());
            this.binding=binding;

        }
    }

    public static List<String> getData(){
        return studentList.getPresentstudent();
    }
}
