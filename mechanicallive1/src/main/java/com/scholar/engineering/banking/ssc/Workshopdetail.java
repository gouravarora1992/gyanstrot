package com.scholar.engineering.banking.ssc;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
//import com.androidquery.AQuery;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Random;

import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Workshopdetail extends AppCompatActivity
{
	Context c;
	Intent detail1;
	LinearLayout layout1;
	String st_title,comment,image,date,username,collag,imgurl,email;
	TextView title,location,link,cost,time,description,descritext,phonetext,register;
	ImageView locationim,linkim,costim,timeim,workim,sapim1,sapim2,phoneimage;

	ScrollView sv;
	String amount, orderId;
	int postid;
	ProgressDialog progress;
	String phone_no;
	NetworkConnection nw;

	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
	private DisplayImageOptions options;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.workshopdetail);

		Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		getSupportActionBar().setTitle("Event");
		Utility.applyFontForToolbarTitle(toolbar,this);
		TextView header=(TextView)findViewById(R.id.headertextid);
//		header.setText("Event");
		header.setVisibility(View.GONE);

		nw=new NetworkConnection(this);

		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.blankimage)
				.showImageForEmptyUri(R.drawable.congrrr)
				.showImageOnFail(R.drawable.congrrr)
				.cacheInMemory(false)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.build();

		Random r = new Random();
		Integer randomNum = r.nextInt(9999999 - 0) + 0;

		//Integer randomNum = ServiceUtility.randInt(0, 9999999);
		orderId=randomNum.toString();
		
		layout1=(LinearLayout)findViewById(R.id.layout1);
		
		sv=(ScrollView)findViewById(R.id.scrollview1);
		title=(TextView)findViewById(R.id.topicname);
		//tdesc=(TextView)findViewById(R.id.descriptionid);
		//tdetail=(TextView)findViewById(R.id.detailid);
		location=(TextView)findViewById(R.id.locationtext);
		link=(TextView)findViewById(R.id.linktext);
		cost=(TextView)findViewById(R.id.paymenttext);
		time=(TextView)findViewById(R.id.timetext);
		descritext=(TextView)findViewById(R.id.descriptiontext);
		description=(TextView)findViewById(R.id.topicdescri);
		
		register=(TextView)findViewById(R.id.registerid);
	    phonetext=(TextView)findViewById(R.id.phonenoid);
		
		locationim=(ImageView)findViewById(R.id.locationimage);
		linkim=(ImageView)findViewById(R.id.linkimage);
		costim=(ImageView)findViewById(R.id.paymentimage);
		timeim=(ImageView)findViewById(R.id.timeimage);
		workim=(ImageView)findViewById(R.id.workshopimage);
		sapim1=(ImageView)findViewById(R.id.sepraterimage);
		sapim2=(ImageView)findViewById(R.id.sepraterimage2);
		phoneimage=(ImageView)findViewById(R.id.phoneimageid);

		setTypeface();
		//img6.setOnClickListener(this);
		detail1=getIntent();
		postid=detail1.getIntExtra("postid",0);

//		username=pref.getString("name", "");
//		collag=pref.getString("collage", "");
//		imgurl=pref.getString("image","");
		email=User_Email;

		get_Post(postid);

		register.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

//				if(pref.getBoolean("boolean",false)) {
//					Intent in = new Intent(Workshopdetail.this,Workshop_Register.class);
//					in.putExtra("amount",amount);
//					in.putExtra("postid",postid);
//					startActivity(in);
//				}
//				else{
//					Intent in = new Intent(Workshopdetail.this, SplashScreen.class);
//					startActivity(in);
//					finish();
//				}
			}
		});
		
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Logg("call_onDestroy()","Call onDestroy()");
		deleteCache(this);
	}

	public void deleteCache(Context context) {
		try {
			File dir = context.getCacheDir();
			deleteDir(dir);
		} catch (Exception e) {}
	}

	public boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
			return dir.delete();
		} else if(dir!= null && dir.isFile()) {
			return dir.delete();
		} else {
			return false;
		}
	}

	public void setData(JSONArray array) {
		try {

			JSONObject object=array.getJSONObject(0);

			JSONObject jsonobj = new JSONObject(object.getString("jsondata"));

//			if(jsonobj.has("add_comment")){
//				if(jsonobj.getString("add_comment").equalsIgnoreCase("off")){
//					vh1.commentlay.setVisibility(View.GONE);
//
//					vh1.lay_share.setGravity(Gravity.CENTER);
//					vh1.lay_like.setGravity(Gravity.CENTER);
//				}
//			}

			amount=jsonobj.getString("fees");

			title.setText(jsonobj.getString("title"));
			location.setText(jsonobj.getString("location"));
			link.setText(jsonobj.getString("link"));
			cost.setText(jsonobj.getString("fees"));
			time.setText(jsonobj.getString("date"));
			descritext.setText(Html.fromHtml(object.getString("post_description")));
			image=jsonobj.getString("image");
			phone_no=jsonobj.getString("phone");

//			if(phone_no.length()>0)
				phonetext.setText(phone_no);

			if(image.length()>1){
				ImageLoader imageLoader = ImageLoader.getInstance();
				imageLoader.init(ImageLoaderConfiguration.createDefault(Workshopdetail.this));
				imageLoader.getInstance().displayImage(Constants.URL_Image + "workshopimage/" + image, workim, options, animateFirstListener);
			}

//			in.putExtra("date", data.getTimestamp());
//			in.putExtra("title", json.getString("title"));
//			in.putExtra("description", data.getPostdescription()); // form fees
//			in.putExtra("like", data.getLikecount());// website
//			in.putExtra("image", json.getString("pic"));
//			in.putExtra("postid", data.getId());
//			in.putExtra("comment", data.getCommentcount());
//			in.putExtra("likestatus", data.getLikestatus());
//			in.putExtra("position", "0");
//			in.putExtra("by", json.getString("writer"));

		} catch (JSONException e) {

		}
	}

	public void get_Post(final int postid) {


		if(!nw.isConnectingToInternet()) {
			internetconnection();
			return;
		}

		progress = ProgressDialog.show(Workshopdetail.this, null, null, true);
		progress.setContentView(R.layout.progressdialog);
		progress.setCanceledOnTouchOutside(false);
		progress.setCancelable(false);
		progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		progress.show();

		RequestQueue queue= Volley.newRequestQueue(getApplicationContext());
		JSONObject obj=new JSONObject();

		String url=Constants.URL+"newapi2_04/getPost.php?postid="+postid+"&email="+email;

		//Logg("url list",url);

		JsonObjectRequest json=new JsonObjectRequest(Request.Method.POST, url, obj,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res) {
				// TODO Auto-generated method stub

				//Logg("respon",String.valueOf(res));

				try {

					JSONArray jr=res.getJSONArray("data");

					setData(jr);

					progress.dismiss();
				}
				catch (JSONException e) {
					progress.dismiss();
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				//Logg("error", String.valueOf(arg0));
				progress.dismiss();

			}
		});

		queue.add(json);

	}

	@Override
	public void onBackPressed() {

		if(Constants.Check_Branch_IO){
			Intent in =new Intent(Workshopdetail.this,HomeActivity.class);
			startActivity(in);
			finish();
		}
		else
			super.onBackPressed();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(item.getItemId()== android.R.id.home)
			finish();
		return super.onOptionsItemSelected(item);
	}

	private  void setTypeface()
	{

		Typeface font_demi = Typeface.createFromAsset(Workshopdetail.this.getAssets(), "avenirnextdemibold.ttf");
		title.setTypeface(font_demi);
		register.setTypeface(font_demi);
		description.setTypeface(font_demi);

		Typeface font_meduim = Typeface.createFromAsset(Workshopdetail.this.getAssets(), "avenirnextmediumCn.ttf");
		location.setTypeface(font_meduim);
		link.setTypeface(font_meduim);
		cost.setTypeface(font_meduim);
		time.setTypeface(font_meduim);
		descritext.setTypeface(font_meduim);
		phonetext.setTypeface(font_meduim);

	}

	public void internetconnection(){

		final Dialog dialog = new Dialog(Workshopdetail.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.network_alert);
		dialog.show();

//        JSONObject json;

		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
		TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

		tv_tryagain.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

//				index=0;
//				swipeRefreshLayout.setRefreshing(true);
				dialog.dismiss();
				get_Post(postid);

			}
		});

		iv_cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

	}

}
