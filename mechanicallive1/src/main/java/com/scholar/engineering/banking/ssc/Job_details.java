package com.scholar.engineering.banking.ssc;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

/**
 * Created by surender on 4/19/2017.
 */
public class Job_details extends AppCompatActivity {

    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    ImageLoader imageLoader;
    NetworkConnection nw;
    AppCompatImageView job_image;
    ImageView iv_like;
    TextView tv_like,tv_comment,tv_share;
    JSONObject json,jsonobj;
    WebView wv;
    int likecount=0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.job_details_dialog);

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Job Details");
        Utility.applyFontForToolbarTitle(toolbar,this);


        nw=new NetworkConnection(this);

        options = new DisplayImageOptions.Builder()
                .displayer(new RoundedBitmapDisplayer(50))
                .showImageOnLoading(R.drawable.blankimage)
                .showImageForEmptyUri(R.drawable.congrrr)
                .showImageOnFail(R.drawable.congrrr)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        Typeface font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");
        Typeface font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");
        LinearLayout lay_like=(LinearLayout)findViewById(R.id.liketextid);
        LinearLayout lay_comment=(LinearLayout)findViewById(R.id.lay_comment);
        LinearLayout lay_share=(LinearLayout)findViewById(R.id.ly_shareid);
        job_image=(AppCompatImageView)findViewById(R.id.iv_jobimage);


        iv_like=(ImageView)findViewById(R.id.likeimageid);
        tv_like=(TextView)findViewById(R.id.tv_like);
        tv_comment=(TextView)findViewById(R.id.newscommentid);
        tv_share=(TextView)findViewById(R.id.tv_shareid);
        tv_like.setTypeface(font_medium);
        tv_comment.setTypeface(font_medium);
        tv_share.setTypeface(font_medium);


        wv=(WebView)findViewById(R.id.webview);
        TextView tv_title=(TextView)findViewById(R.id.tv_jobtitle);
        TextView tv_post=(TextView)findViewById(R.id.tv_post);
        tv_post.setTypeface(font_medium);
        tv_title.setTypeface(font_demi);
        wv.getSettings().setJavaScriptEnabled(true);

        wv.setWebChromeClient(new WebChromeClient() {
        });

        try {

            if(Utility.data==null) {
                finish();
                return;
            }

            jsonobj=new JSONObject(Utility.data.getJsonfield());


            if(jsonobj.has("add_comment")){
                if(jsonobj.getString("add_comment").equalsIgnoreCase("off")){

                    lay_comment.setVisibility(View.GONE);

//                    vh1.lay_share.setGravity(Gravity.CENTER);
//                    vh1.lay_like.setGravity(Gravity.CENTER);
                }
            }

            likecount=Utility.data.getLikecount();
            tv_like.setText(likecount+getResources().getString(R.string.like));
            tv_comment.setText(Utility.data.getCommentcount()+" Comments");
            if(Utility.data.getLikestatus().equals("like"))
            {
                iv_like.setImageResource(R.drawable.ic_bulb_filled);
                tv_like.setTextColor(getResources().getColor(R.color.like_text_color));
            }
            else
            {
                iv_like.setImageResource(R.drawable.ic_bulb_lightup);
                tv_like.setTextColor(getResources().getColor(R.color.text_lightgray));
            }

            tv_title.setText(jsonobj.getString("title"));
            tv_post.setText(jsonobj.getString("postname"));

            if(!Utility.data.getPostdescription().equalsIgnoreCase("null"))
                wv.loadDataWithBaseURL(null,Constants.phle+Utility.data.getPostdescription()+Constants.baad , "text/html", "utf-8",null);
            else
                wv.loadUrl(jsonobj.getString("website"));

            if(!jsonobj.getString("image").equalsIgnoreCase("null")){
//                imageLoader.init(ImageLoaderConfiguration.createDefault(c));
                imageLoader.getInstance().displayImage(Constants.URL_Image+"jobsimage/"+jsonobj.getString("image"), job_image, options, animateFirstListener);
            }

            lay_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(Job_details.this, Comment_Common_homepage.class);
                    in.putExtra("posttype","3");
                    in.putExtra("postid",Utility.data.getId());
                    startActivity(in);
                }
            });
            lay_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    nw=new NetworkConnection(Job_details.this);
                    if(!nw.isConnectingToInternet()) {
                        Toast.makeText(Job_details.this,"No Internet Connection",Toast.LENGTH_LONG).show();
                        return;
                    }
                    volleylike_post(Utility.data.getId());
                }
            });

            lay_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JSONObject shareobj=new JSONObject();

                    try {
                        json = new JSONObject(Utility.data.getJsonfield());

                        shareobj.put("class","Job_details_via_link");
                        shareobj.put("title",json.getString("title"));
                        shareobj.put("description",json.getString("postname"));
                        shareobj.put("image",Constants.URL+"jobsimage/"+json.getString("image"));
                        shareobj.put("id",Utility.data.getId());
//

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Utility.shareIntent(Job_details.this,shareobj);

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logg("call_onDestroy()","Call onDestroy()");
        deleteCache(this);
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {}
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
            super.onBackPressed();
    }

    public void volleylike_post(int postid)
    {

        final ProgressDialog progress = ProgressDialog.show(Job_details.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        progress.show();

        RequestQueue queue= Volley.newRequestQueue(Job_details.this);
        JSONObject json =new JSONObject();
        try {
            json.put("id",postid);
            json.put("email",Constants.User_Email);
            json.put("admission_no",Constants.addmissionno);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Constants.URL_LV + "like_post";
        Logg("likeapi",url+" , "+json.toString());
        JsonObjectRequest jsonreq=new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                try {

                    if(res.getString("scalar").equals("like")) {
//                        Toast.makeText(c,"success",Toast.LENGTH_SHORT).show();

                        likecount++;
                        tv_like.setText(likecount+getResources().getString(R.string.like));
//                        setLike(likecount);
                        iv_like.setImageResource(R.drawable.ic_bulb_filled);
                        tv_like.setTextColor(getResources().getColor(R.color.like_text_color));

                    }
                    else if(res.getString("scalar").equals("dislike")){

                        iv_like.setImageResource(R.drawable.ic_bulb_lightup);
                        tv_like.setTextColor(getResources().getColor(R.color.text_lightgray));

//                        Toast.makeText(c,"success",Toast.LENGTH_SHORT).show();

                        likecount--;
                        if(likecount>0) {
                            tv_like.setText(likecount + getResources().getString(R.string.like));
                        }
                        else
                        {
                            tv_like.setText(getResources().getString(R.string.like));
                        }
//                        setLike(likecount);
                    }
                    else {
                        Toast.makeText(Job_details.this,"Not done",Toast.LENGTH_SHORT).show();
                    }

                    progress.dismiss();

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    progress.dismiss();
                }

            }
        },new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg)
            {
                //Logg("error", String.valueOf(arg));
                progress.dismiss();
                Toast.makeText(Job_details.this,"Network Problem", Toast.LENGTH_SHORT).show();
            }

        });

        queue.add(jsonreq);
    }


}
