package com.scholar.engineering.banking.ssc;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.bumptech.glide.Glide;
import com.google.gson.JsonParser;
import com.scholar.engineering.banking.ssc.MaterialSpinner.MaterialSpinner;
import com.scholar.engineering.banking.ssc.getset.Question_getset_new;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

/**
 * Created by surender on 3/24/2017.
 */

public class Play_new extends AppCompatActivity implements View.OnClickListener {

    LinearLayout lay_optiona,lay_optionb,lay_optionc,lay_optiond,lay_optione,lay_question;

    TextView tv_question,tv_optiona,tv_optionb,tv_optionc,tv_optiond,tv_optione,tv_hint,tv_solution;

    TextView tv_cir_optiona,tv_cir_optionb,tv_cir_optionc,tv_cir_optiond,tv_cir_optione;

    TextView tv_timer,tv_next,tv_back,tv_questioncount;

    LinearLayout lay_pallet,lay_hint,lay_solution;
    ArrayList<Question_getset_new> filterArraylist;
    public  static  int Counter=0;

    HorizontalScrollView horizontal_sv;
    ScrollView scrollView;
    int i=0;
    Handler handler;
    public static Handler h;

    ArrayList<Question_getset_new> data;
    Typeface font_demi,font_medium;
    MaterialSpinner sp_testcategory;
    ArrayList<String> sub_test_list;
    ArrayList<Integer> sub_test_count_list;
    String user_ans,cort_ans,st_time="0",st_total_que="0",login_useremail="",addmissionno="",test_id,testname;

    ImageView iv_hint,iv_question,iv_solution,iv_optiona,iv_optionb,iv_optionc,iv_optiond,iv_optione;
    ImageView iv_check_ansa,iv_check_ansb,iv_check_ansc,iv_check_ansd,iv_check_anse;

    int net_count=0,aa,count=0,backpress=0,wrong_ans=0,unattempt=0,correct=0,doneBAckPress=0;
    float right_ans=0.0f;
    CountDownTimer timer;
    TimeUnit am;
    TimeUnit as;
    long time_left=0;
    long aa2=0;
    Bitmap bitmap;
    String photoPath = Environment.getExternalStorageDirectory()+"/GyanStrot/";
    ProgressDialog progress;
    boolean b_hint=false,b_que=false,check_result=false;
    NetworkConnection nw;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Constants.checksolution=true;

        setContentView(R.layout.play_new);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Logg("oncreate>>",getIntent().getStringExtra("type"));
        handler=new Handler();

        h = new Handler() {

            public void handleMessage(Message msg) {
                super.handleMessage(msg);

                switch(msg.what) {
                    case 0:
                        finish();
                        break;
                }
            }
        };


        data=new ArrayList<>();

        nw=new NetworkConnection(this);

        //wv_hint=(WebView)findViewById(R.id.webview_hint);

        font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");
        font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");
        photoPath=photoPath+Constants.ImageDirectory+"/";
        login_useremail=User_Email;
        addmissionno=Constants.addmissionno;

        test_id=getIntent().getStringExtra("testid");
        Log.e("testid",test_id);

        lay_optiona=(LinearLayout)findViewById(R.id.lay_optiona);
        lay_optionb=(LinearLayout)findViewById(R.id.lay_optionb);
        lay_optionc=(LinearLayout)findViewById(R.id.lay_optionc);
        lay_optiond=(LinearLayout)findViewById(R.id.lay_optiond);
        lay_optione=(LinearLayout)findViewById(R.id.lay_optione);
        lay_question=(LinearLayout)findViewById(R.id.lay_question);
        lay_hint=(LinearLayout)findViewById(R.id.lay_hint);
        lay_solution=(LinearLayout)findViewById(R.id.lay_solution);
        tv_optiona=(TextView)findViewById(R.id.tv_optiona);
        tv_optionb=(TextView)findViewById(R.id.tv_optionb);
        tv_optionc=(TextView)findViewById(R.id.tv_optionc);
        tv_optiond=(TextView)findViewById(R.id.tv_optiond);
        tv_optione=(TextView)findViewById(R.id.tv_optione);
        tv_question=(TextView)findViewById(R.id.tv_question);
        tv_hint=(TextView)findViewById(R.id.tv_questionhint);
        tv_solution=(TextView)findViewById(R.id.tv_questionsolution);
        tv_cir_optiona=(TextView)findViewById(R.id.tv_circle_a);
        tv_cir_optionb=(TextView)findViewById(R.id.tv_circle_b);
        tv_cir_optionc=(TextView)findViewById(R.id.tv_circle_c);
        tv_cir_optiond=(TextView)findViewById(R.id.tv_circle_d);
        tv_cir_optione=(TextView)findViewById(R.id.tv_circle_e);
        scrollView=(ScrollView)findViewById(R.id.scrollView);

        tv_cir_optiona.setTypeface(font_demi);
        tv_cir_optionb.setTypeface(font_demi);
        tv_cir_optionc.setTypeface(font_demi);
        tv_cir_optiond.setTypeface(font_demi);
        tv_cir_optione.setTypeface(font_demi);
        tv_optiona.setTypeface(font_medium);
        tv_optionb.setTypeface(font_medium);
        tv_optionc.setTypeface(font_medium);
        tv_optiond.setTypeface(font_medium);
        tv_optione.setTypeface(font_medium);
        tv_question.setTypeface(font_medium);
        tv_hint.setTypeface(font_medium);
        tv_solution.setTypeface(font_medium);
        iv_check_ansa=(ImageView)findViewById(R.id.iv_option_a);
        iv_check_ansb=(ImageView)findViewById(R.id.iv_option_b);
        iv_check_ansc=(ImageView)findViewById(R.id.iv_option_c);
        iv_check_ansd=(ImageView)findViewById(R.id.iv_option_d);
        iv_check_anse=(ImageView)findViewById(R.id.iv_option_e);
        iv_check_ansa.setVisibility(View.GONE);
        iv_check_ansb.setVisibility(View.GONE);
        iv_check_ansc.setVisibility(View.GONE);
        iv_check_ansd.setVisibility(View.GONE);
        iv_check_anse.setVisibility(View.GONE);

        iv_optiona=(ImageView)findViewById(R.id.iv_optiona);
        iv_optionb=(ImageView)findViewById(R.id.iv_optionb);
        iv_optionc=(ImageView)findViewById(R.id.iv_optionc);
        iv_optiond=(ImageView)findViewById(R.id.iv_optiond);
        iv_optione=(ImageView)findViewById(R.id.iv_optione);

        iv_question=(ImageView)findViewById(R.id.iv_question);
        iv_hint=(ImageView)findViewById(R.id.iv_questionhint);
        iv_solution=(ImageView)findViewById(R.id.iv_solution);

        tv_back=(TextView)findViewById(R.id.tv_back);
        tv_next=(TextView)findViewById(R.id.tv_next);
        tv_timer=(TextView)findViewById(R.id.tv_timer);
        tv_back.setTypeface(font_demi);
        tv_next.setTypeface(font_demi);
        tv_timer.setTypeface(font_demi);
        sp_testcategory=(MaterialSpinner) findViewById(R.id.sp_testcategory);

        lay_pallet=(LinearLayout)findViewById(R.id.lay_pallet);

        horizontal_sv=(HorizontalScrollView)findViewById(R.id.horizontalsvid);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        Constants.Width = (displayMetrics.widthPixels+40);
        Constants.Height = displayMetrics.heightPixels;

        getQuestion();

        tv_back.setOnClickListener(this);
        tv_next.setOnClickListener(this);
        lay_optiona.setOnClickListener(this);
        lay_optionb.setOnClickListener(this);
        lay_optionc.setOnClickListener(this);
        lay_optiond.setOnClickListener(this);
        lay_optione.setOnClickListener(this);
        lay_question.setOnClickListener(this);
        tv_hint.setOnClickListener(this);

        lay_optiona.setClickable(false);
        lay_optionb.setClickable(false);
        lay_optionc.setClickable(false);
        lay_optiond.setClickable(false);
        lay_optione.setClickable(false);
        tv_back.setClickable(false);
        tv_next.setClickable(false);
        lay_question.setClickable(false);
        tv_hint.setClickable(false);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logg("call_onDestroy()","Call onDestroy()");

        if(timer!=null)
            timer.cancel();

        deleteCache(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        if (doneBAckPress==1)
//        {
//            doneBAckPress=0;
//        }
//        else
//        {
//            killingApp("pause");
//
//        }


    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Logg("newintent","newintent");
        Toast.makeText(this, "newintent", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        doneBAckPress=1;
        if (doneBAckPress==1)
        {
            killingApp("back");
        }

    }

    public void killingApp(String text)
    {
        if (text.equalsIgnoreCase("pause"))
        {
            if (Constants.checksolution) {

                if (Counter == 1) {
                    right_ans = 0.0f;
                    unattempt = 0;
                    wrong_ans = 0;

                    for (int i = 0; i < data.size(); i++) {

                        if (data.get(i).getAnswer().equalsIgnoreCase(data.get(i).getUser_ans())) {
                            right_ans++;
                        } else if (data.get(i).getUser_ans().equalsIgnoreCase("no")) {
                            unattempt++;
                        } else
                            wrong_ans++;
                    }

//                    if(isFinishing())
//                    onPauseFinish();

                } else if (Counter == 0) {
                    Counter++;
//                    if(!isFinishing())
//                    onPauseAlert();
//                    Toast.makeText(this, "Don't pause next time test will close", Toast.LENGTH_SHORT).show();
                }
            }
        }

        else if (text.equalsIgnoreCase("back"))
        {

            if (Constants.checksolution) {
                if(backpress==0)
                    super.onBackPressed();
                else {
                    alert("Do you want to submit test?");

                }
            }

            else
                super.onBackPressed();
        }
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {}
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    @Override
    public void onClick(View v) {

        TableData td=(TableData)v.getTag();

        if(td!=null) {

            View vi = new View(Play_new.this);
            vi = lay_pallet.getChildAt(count);
            TextView t11=(TextView)vi.findViewById(R.id.que_no);
            //Logg("value","1");
            t11.setTextColor(Color.WHITE);
//            t11.setBackgroundResource(R.drawable.butgray_light);

            if(data.get(count).getUser_ans().equalsIgnoreCase("no")) {
                t11.setBackgroundResource(R.drawable.but_review);
            }
            else
                t11.setBackgroundResource(R.drawable.butgray_light);

            lay_solution.setVisibility(View.GONE);

            count=td.tv_tag;

            if(count!=0)
                backpress=count;


            if (count < data.size()){

                vi = lay_pallet.getChildAt(count);
                TextView tv_selected=(TextView)vi.findViewById(R.id.que_no);
                //Logg("value","2");
                tv_selected.setBackgroundResource(R.drawable.purple_button_bgnd);
                tv_selected.setTextColor(Color.WHITE);
                if(count>5)
                    horizontal_sv.scrollBy(75,45);

                setQuestion();
            }


            if(count==data.size()-1)
                tv_next.setText("Submit");
            else
                tv_next.setText("Next");

        }
        if(v.getId()==R.id.tv_next){

            lay_solution.setVisibility(View.GONE);
            scrollView.scrollTo(0, scrollView.getTop());

//            scrollView.getViewTreeObserver().removeOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                @Override
//                public void onGlobalLayout() {
//
//                    scrollView.post(new Runnable() {
//                        public void run() {
//                            scrollView.fullScroll(View.FOCUS_UP);
//                        }
//                    });
//                }
//            });

            if(count>5)
                horizontal_sv.scrollBy(68,45);

            if(count<data.size()-1) {

                View vi = new View(Play_new.this);
                vi = lay_pallet.getChildAt(count);
                TextView t11=(TextView)vi.findViewById(R.id.que_no);
                //Logg("value","3");
                t11.setTextColor(Color.WHITE);

//                t11.setBackgroundResource(R.drawable.butgray_light);

                if(data.get(count).getUser_ans().equalsIgnoreCase("no")) {
                    t11.setBackgroundResource(R.drawable.but_review);
                }
                else
                    t11.setBackgroundResource(R.drawable.butgray_light);

                count++;
                backpress++;
                vi = lay_pallet.getChildAt(count);
                TextView tv_selected=(TextView)vi.findViewById(R.id.que_no);
                tv_selected.setBackgroundResource(R.drawable.purple_button_bgnd);
                tv_selected.setTextColor(Color.WHITE);
                setQuestion();

                if(count==data.size()-1)
                    tv_next.setText("Submit");
                else
                    tv_next.setText("Next");
            }

            else if(count==data.size()-1){
                View vi = new View(Play_new.this);
                vi = lay_pallet.getChildAt(count);

                TextView t11=(TextView)vi.findViewById(R.id.que_no);
                //Logg("value","4");

                t11.setTextColor(Color.WHITE);

                if(data.get(count).getUser_ans().equalsIgnoreCase("no")) {
                    t11.setBackgroundResource(R.drawable.but_review);
                }
                else
                    t11.setBackgroundResource(R.drawable.butgray_light);

//                t11.setBackgroundResource(R.drawable.butgray_light);

//                count++;

                // alert("Test Complete! View Result");

//                if(Constants.checksolution)

                successAlert();

            }
        }

        else if (v.getId()==R.id.tv_back){

//            scrollView.getViewTreeObserver().removeOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                @Override
//                public void onGlobalLayout() {
//
//                    scrollView.post(new Runnable() {
//                        public void run() {
//                            scrollView.fullScroll(View.FOCUS_UP);
//                        }
//                    });
//                }
//            });


//            if(count==data.size()-1)
//                tv_next.setText("Submit");
//            else

            tv_next.setText("Next");

            horizontal_sv.scrollBy(-68,-45);

            lay_solution.setVisibility(View.GONE);

            if(count==data.size()) {

                View vi = new View(Play_new.this);
//                vi = lay_pallet.getChildAt(count);
//                TextView t11=(TextView)vi.findViewById(R.id.que_no);
//                t11.setBackgroundResource(R.drawable.butgreen30);

                count--;

//                View vi = new View(Play_new.this);

                vi = lay_pallet.getChildAt(count);
                TextView tv_selected=(TextView)vi.findViewById(R.id.que_no);
                //Logg("value","5");
                tv_selected.setTextColor(Color.WHITE);
                tv_selected.setBackgroundResource(R.drawable.purple_button_bgnd);

                setQuestion();
            }
            else if(count>0){
                View vi = new View(Play_new.this);
                vi = lay_pallet.getChildAt(count);
                TextView t11=(TextView)vi.findViewById(R.id.que_no);
                //Logg("value","6");
                t11.setTextColor(Color.WHITE);

//                t11.setBackgroundResource(R.drawable.butgray_light);

                if(data.get(count).getUser_ans().equalsIgnoreCase("no")) {
                    t11.setBackgroundResource(R.drawable.but_review);
                }
                else
                    t11.setBackgroundResource(R.drawable.butgray_light);

                count--;
//                View vi = new View(Play_new.this);

                vi = lay_pallet.getChildAt(count);
                TextView tv_selected=(TextView)vi.findViewById(R.id.que_no);
                tv_selected.setBackgroundResource(R.drawable.purple_button_bgnd);
                tv_selected.setTextColor(Color.WHITE);
                setQuestion();
            }
        }
        else if(v.getId()==R.id.lay_optiona){

            String real_opt=data.get(count).getAns_a();

            Logg("real_ans",real_opt);

            data.get(count).setUser_ans(real_opt);

            if(Constants.Ans_Status.equalsIgnoreCase("yes"))
                checkAnswer(data.get(count).getAnswer(),real_opt,lay_optiona,iv_check_ansa);
            else {
                checkAnswer_ifno(real_opt,lay_optiona);
            }
        }
        else if(v.getId()==R.id.lay_optionb) {

            String real_opt=data.get(count).getAns_b();

            Logg("real_ans",real_opt);

            data.get(count).setUser_ans(real_opt);

            if(Constants.Ans_Status.equalsIgnoreCase("yes"))

                checkAnswer(data.get(count).getAnswer(),real_opt,lay_optionb,iv_check_ansb);
            else {
                checkAnswer_ifno(real_opt,lay_optionb);
            }
        }
        else if(v.getId()==R.id.lay_optionc) {

            String real_opt=data.get(count).getAns_c();

            Logg("real_ans",real_opt);

            data.get(count).setUser_ans(real_opt);

//            data.get(count).setUser_ans("c");

            if(Constants.Ans_Status.equalsIgnoreCase("yes"))
                checkAnswer(data.get(count).getAnswer(),real_opt,lay_optionc,iv_check_ansc);
            else {
                checkAnswer_ifno(real_opt,lay_optionc);
            }
        }
        else if(v.getId()==R.id.lay_optiond) {

            String real_opt=data.get(count).getAns_d();

            Logg("real_ans",real_opt);

            data.get(count).setUser_ans(real_opt);

//            data.get(count).setUser_ans("d");

            if(Constants.Ans_Status.equalsIgnoreCase("yes"))
                checkAnswer(data.get(count).getAnswer(),real_opt,lay_optiond,iv_check_ansd);
            else {
                checkAnswer_ifno(real_opt,lay_optiond);
            }
        }
        else if(v.getId()==R.id.lay_optione) {
            String real_opt=data.get(count).getAns_e();

            Logg("real_ans",real_opt);

            data.get(count).setUser_ans(real_opt);

//            data.get(count).setUser_ans("e");

            if(Constants.Ans_Status.equalsIgnoreCase("yes"))
                checkAnswer(data.get(count).getAnswer(),real_opt,lay_optione,iv_check_anse);
            else {
                checkAnswer_ifno(real_opt,lay_optione);
            }
        }
        else if(v.getId()==R.id.tv_questionhint){
            if(b_hint) {
                b_hint = false;
                tv_hint.setCompoundDrawablesWithIntrinsicBounds(0,0,0,R.drawable.arrow_down);

            }
            else {
                b_hint = true;
                tv_hint.setCompoundDrawablesWithIntrinsicBounds(0,0,0,R.drawable.arrow_up);

            }

            checkHint();
        }
        else if(v.getId()==R.id.lay_question){
            if(b_que) {
                b_que = false;
                tv_question.setCompoundDrawablesWithIntrinsicBounds(0,0,0,R.drawable.arrow_down);
            }
            else {
                b_que = true;
                tv_question.setCompoundDrawablesWithIntrinsicBounds(0,0,0,R.drawable.arrow_up);
            }
            if(count<=(data.size()-1))
                checkQuestion(count);
            else{
                if(data.size()>0)
                    checkQuestion(count-1);
            }
        }
    }

    private void successAlert()
    {

        if(isFinishing())
            return;

        final Dialog dialog=new Dialog(Play_new.this,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.success_dialog);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        TextView text,text1;
        text=(TextView)dialog.findViewById(R.id.text);
        text1=(TextView)dialog.findViewById(R.id.text1);
        final TextView Submit=(TextView)dialog.findViewById(R.id.tv_submit);
        TextView Cancel=(TextView)dialog.findViewById(R.id.tv_cancel);
        Cancel.setVisibility(View.VISIBLE);

//        if(Constants.checksolution)
//            dialog.setCancelable(false);

        if(!Constants.checksolution){

            text.setText("Review Completed !");
            text1.setText("Now share your marks on next page to interact with other students");
            Submit.setText("VIEW RESULT");
        }
        else{
            text1.setText("Test Completed Submit Your Result");
        }

        text.setTypeface(font_medium);
        text1.setTypeface(font_medium);
        Submit.setTypeface(font_demi);
        Cancel.setTypeface(font_demi);

        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                right_ans=0.0f;
                unattempt=0;
                wrong_ans=0;
                Counter=3;

                for(int i=0;i<data.size();i++)
                {

                    if(data.get(i).getAnswer().equalsIgnoreCase(data.get(i).getUser_ans()))
                    {
                        right_ans++;
                    }
                    else if(data.get(i).getUser_ans().equalsIgnoreCase("no"))
                    {
                        unattempt++;
                    }
                    else
                        wrong_ans++;
                }


                if(!nw.isConnectingToInternet()) {
                    net_count=3;
                    Toast.makeText(Play_new.this,"Enable Your Internet Connection",Toast.LENGTH_SHORT).show();
                    return;
                }
                else {
//                    Submit.setClickable(false);
                    if(!check_result) {
                        volleyupdateresult(true);
                        dialog.dismiss();
                    }
                }

            }
        });
    }

    public static class TableData{

        public final int tv_tag;

        public TableData(int tag) {
            tv_tag= tag;
        }

    }

    public void setPalletNo(int size){

        for(int i=0;i<size;i++)
        {
            View child = getLayoutInflater().inflate(R.layout.questionview, null);
            tv_questioncount=(TextView)child.findViewById(R.id.que_no);
            tv_questioncount.setText(String.valueOf(i+1));
            tv_questioncount.setTag(new TableData((i)));
//            tv1.setTag(new TableData((i)));
            tv_questioncount.setOnClickListener(Play_new.this);
            lay_pallet.addView(child);
        }
    }

    public void checkAnswer_ifno(String user_ans,LinearLayout layout){

        //Logg("check_answer",cort_ans+" user_ans  "+user_ans);

//        lay_optiona.setClickable(false);
//        lay_optionb.setClickable(false);
//        lay_optionc.setClickable(false);
//        lay_optiond.setClickable(false);
//        lay_optione.setClickable(false);

        lay_optiona.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optionb.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optionc.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optiond.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optione.setBackgroundColor(Color.parseColor("#00000000"));



//            if (user_ans.equalsIgnoreCase("a")) {
        layout.setBackgroundColor(Color.parseColor("#11000000"));
//                iv_check_ansa.setImageResource(R.drawable.ic_wrong);
//                iv_check_ansa.setVisibility(View.VISIBLE);
//            } else if (user_ans.equalsIgnoreCase("b")) {
//                lay_optionb.setBackgroundColor(Color.parseColor("#11000000"));
////                iv_check_ansb.setImageResource(R.drawable.ic_wrong);
////                iv_check_ansb.setVisibility(View.VISIBLE);
//            } else if (user_ans.equalsIgnoreCase("c")) {
//                lay_optionc.setBackgroundColor(Color.parseColor("#11000000"));
////                iv_check_ansc.setImageResource(R.drawable.ic_wrong);
////                iv_check_ansc.setVisibility(View.VISIBLE);
//            } else if (user_ans.equalsIgnoreCase("d")) {
//                lay_optiond.setBackgroundColor(Color.parseColor("#11000000"));
////                iv_check_ansd.setImageResource(R.drawable.ic_wrong);
////                iv_check_ansd.setVisibility(View.VISIBLE);
//            } else if (user_ans.equalsIgnoreCase("e")) {
//                lay_optione.setBackgroundColor(Color.parseColor("#11000000"));
////                iv_check_anse.setImageResource(R.drawable.ic_wrong);
////                iv_check_anse.setVisibility(View.VISIBLE);
//            }

    }

    public void checkAnswer(String cort_ans,String user_ans,LinearLayout layout, ImageView iv_icon){

        //Logg("check_answer",cort_ans+" user_ans  "+user_ans);

        lay_optiona.setClickable(false);
        lay_optionb.setClickable(false);
        lay_optionc.setClickable(false);
        lay_optiond.setClickable(false);
        lay_optione.setClickable(false);

        lay_optiona.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optionb.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optionc.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optiond.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optione.setBackgroundColor(Color.parseColor("#00000000"));

        iv_check_ansa.setVisibility(View.GONE);
        iv_check_ansb.setVisibility(View.GONE);
        iv_check_ansc.setVisibility(View.GONE);
        iv_check_ansd.setVisibility(View.GONE);
        iv_check_anse.setVisibility(View.GONE);

        if(Constants.Ans_Status.equalsIgnoreCase("yes")) {
            if (cort_ans.equalsIgnoreCase(user_ans)) {

//                if (cort_ans.equalsIgnoreCase("a")) {
                layout.setBackgroundColor(Color.parseColor("#33009900"));
                iv_icon.setImageResource(R.drawable.ic_right);
                iv_icon.setVisibility(View.VISIBLE);

//                } else if (cort_ans.equalsIgnoreCase("b")) {
//                    lay_optionb.setBackgroundColor(Color.parseColor("#33009900"));
//                    iv_check_ansb.setImageResource(R.drawable.ic_right);
//                    iv_check_ansb.setVisibility(View.VISIBLE);
//                } else if (cort_ans.equalsIgnoreCase("c")) {
//                    lay_optionc.setBackgroundColor(Color.parseColor("#33009900"));
//                    iv_check_ansc.setImageResource(R.drawable.ic_right);
//                    iv_check_ansc.setVisibility(View.VISIBLE);
//                } else if (cort_ans.equalsIgnoreCase("d")) {
//                    lay_optiond.setBackgroundColor(Color.parseColor("#33009900"));
//                    iv_check_ansd.setImageResource(R.drawable.ic_right);
//                    iv_check_ansd.setVisibility(View.VISIBLE);
//                } else if (cort_ans.equalsIgnoreCase("e")) {
//                    lay_optione.setBackgroundColor(Color.parseColor("#33009900"));
//                    iv_check_anse.setImageResource(R.drawable.ic_right);
//                    iv_check_anse.setVisibility(View.VISIBLE);
//                }

            } else {
                if (cort_ans.equalsIgnoreCase("a")) {
                    if(data.get(count).getAns_a().equalsIgnoreCase("a")) {
                        lay_optiona.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansa.setImageResource(R.drawable.ic_right);
                        iv_check_ansa.setVisibility(View.VISIBLE);
                    }
                    else if(data.get(count).getAns_b().equalsIgnoreCase("a")){
                        lay_optionb.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansb.setImageResource(R.drawable.ic_right);
                        iv_check_ansb.setVisibility(View.VISIBLE);
                    }
                    else if(data.get(count).getAns_c().equalsIgnoreCase("a")){
                        lay_optionc.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansc.setImageResource(R.drawable.ic_right);
                        iv_check_ansc.setVisibility(View.VISIBLE);
                    }
                    else if(data.get(count).getAns_d().equalsIgnoreCase("a")){
                        lay_optiond.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansd.setImageResource(R.drawable.ic_right);
                        iv_check_ansd.setVisibility(View.VISIBLE);
                    }
                    else if(data.get(count).getAns_e().equalsIgnoreCase("a")){
                        lay_optione.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_anse.setImageResource(R.drawable.ic_right);
                        iv_check_anse.setVisibility(View.VISIBLE);
                    }

                } else if (cort_ans.equalsIgnoreCase("b")) {

                    if(data.get(count).getAns_a().equalsIgnoreCase("b")) {
                        lay_optiona.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansa.setImageResource(R.drawable.ic_right);
                        iv_check_ansa.setVisibility(View.VISIBLE);
                    }
                    else if(data.get(count).getAns_b().equalsIgnoreCase("b")){
                        lay_optionb.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansb.setImageResource(R.drawable.ic_right);
                        iv_check_ansb.setVisibility(View.VISIBLE);
                    }
                    else if(data.get(count).getAns_c().equalsIgnoreCase("b")){
                        lay_optionc.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansc.setImageResource(R.drawable.ic_right);
                        iv_check_ansc.setVisibility(View.VISIBLE);
                    }
                    else if(data.get(count).getAns_d().equalsIgnoreCase("b")){
                        lay_optiond.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansd.setImageResource(R.drawable.ic_right);
                        iv_check_ansd.setVisibility(View.VISIBLE);
                    }
                    else if(data.get(count).getAns_e().equalsIgnoreCase("b")){
                        lay_optione.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_anse.setImageResource(R.drawable.ic_right);
                        iv_check_anse.setVisibility(View.VISIBLE);
                    }
//                    lay_optionb.setBackgroundColor(Color.parseColor("#33009900"));
//                    iv_check_ansb.setImageResource(R.drawable.ic_right);
//                    iv_check_ansb.setVisibility(View.VISIBLE);
                } else if (cort_ans.equalsIgnoreCase("c")) {

                    if(data.get(count).getAns_a().equalsIgnoreCase("c")) {
                        lay_optiona.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansa.setImageResource(R.drawable.ic_right);
                        iv_check_ansa.setVisibility(View.VISIBLE);
                    }
                    else if(data.get(count).getAns_b().equalsIgnoreCase("c")){
                        lay_optionb.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansb.setImageResource(R.drawable.ic_right);
                        iv_check_ansb.setVisibility(View.VISIBLE);
                    }
                    else if(data.get(count).getAns_c().equalsIgnoreCase("c")){
                        lay_optionc.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansc.setImageResource(R.drawable.ic_right);
                        iv_check_ansc.setVisibility(View.VISIBLE);
                    }
                    else if(data.get(count).getAns_d().equalsIgnoreCase("c")){
                        lay_optiond.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansd.setImageResource(R.drawable.ic_right);
                        iv_check_ansd.setVisibility(View.VISIBLE);
                    }
                    else if(data.get(count).getAns_e().equalsIgnoreCase("c")){
                        lay_optione.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_anse.setImageResource(R.drawable.ic_right);
                        iv_check_anse.setVisibility(View.VISIBLE);
                    }

//                    lay_optionc.setBackgroundColor(Color.parseColor("#33009900"));
//                    iv_check_ansc.setImageResource(R.drawable.ic_right);
//                    iv_check_ansc.setVisibility(View.VISIBLE);
                } else if (cort_ans.equalsIgnoreCase("d")) {

                    if(data.get(count).getAns_a().equalsIgnoreCase("d")) {
                        lay_optiona.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansa.setImageResource(R.drawable.ic_right);
                        iv_check_ansa.setVisibility(View.VISIBLE);
                    }
                    else if(data.get(count).getAns_b().equalsIgnoreCase("d")){
                        lay_optionb.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansb.setImageResource(R.drawable.ic_right);
                        iv_check_ansb.setVisibility(View.VISIBLE);
                    }
                    else if(data.get(count).getAns_c().equalsIgnoreCase("d")){
                        lay_optionc.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansc.setImageResource(R.drawable.ic_right);
                        iv_check_ansc.setVisibility(View.VISIBLE);
                    }
                    else if(data.get(count).getAns_d().equalsIgnoreCase("d")){
                        lay_optiond.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansd.setImageResource(R.drawable.ic_right);
                        iv_check_ansd.setVisibility(View.VISIBLE);
                    }
                    else if(data.get(count).getAns_e().equalsIgnoreCase("d")){
                        lay_optione.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_anse.setImageResource(R.drawable.ic_right);
                        iv_check_anse.setVisibility(View.VISIBLE);
                    }

//                    lay_optiond.setBackgroundColor(Color.parseColor("#33009900"));
//                    iv_check_ansd.setImageResource(R.drawable.ic_right);
//                    iv_check_ansd.setVisibility(View.VISIBLE);
                } else if (cort_ans.equalsIgnoreCase("e")) {
                    if(data.get(count).getAns_a().equalsIgnoreCase("e")) {
                        lay_optiona.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansa.setImageResource(R.drawable.ic_right);
                        iv_check_ansa.setVisibility(View.VISIBLE);
                    }
                    else if(data.get(count).getAns_b().equalsIgnoreCase("e")){
                        lay_optionb.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansb.setImageResource(R.drawable.ic_right);
                        iv_check_ansb.setVisibility(View.VISIBLE);
                    }
                    else if(data.get(count).getAns_c().equalsIgnoreCase("e")){
                        lay_optionc.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansc.setImageResource(R.drawable.ic_right);
                        iv_check_ansc.setVisibility(View.VISIBLE);
                    }
                    else if(data.get(count).getAns_d().equalsIgnoreCase("e")){
                        lay_optiond.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansd.setImageResource(R.drawable.ic_right);
                        iv_check_ansd.setVisibility(View.VISIBLE);
                    }
                    else if(data.get(count).getAns_e().equalsIgnoreCase("e")){
                        lay_optione.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_anse.setImageResource(R.drawable.ic_right);
                        iv_check_anse.setVisibility(View.VISIBLE);
                    }
//                    lay_optione.setBackgroundColor(Color.parseColor("#33009900"));
//                    iv_check_anse.setImageResource(R.drawable.ic_right);
//                    iv_check_anse.setVisibility(View.VISIBLE);
                }

//                if (user_ans.equalsIgnoreCase("a")) {
                layout.setBackgroundColor(Color.parseColor("#33ff0000"));
                iv_icon.setImageResource(R.drawable.ic_wrong);
                iv_icon.setVisibility(View.VISIBLE);
//                } else if (user_ans.equalsIgnoreCase("b")) {
//                    lay_optionb.setBackgroundColor(Color.parseColor("#33ff0000"));
//                    iv_check_ansb.setImageResource(R.drawable.ic_wrong);
//                    iv_check_ansb.setVisibility(View.VISIBLE);
//                } else if (user_ans.equalsIgnoreCase("c")) {
//                    lay_optionc.setBackgroundColor(Color.parseColor("#33ff0000"));
//                    iv_check_ansc.setImageResource(R.drawable.ic_wrong);
//                    iv_check_ansc.setVisibility(View.VISIBLE);
//                } else if (user_ans.equalsIgnoreCase("d")) {
//                    lay_optiond.setBackgroundColor(Color.parseColor("#33ff0000"));
//                    iv_check_ansd.setImageResource(R.drawable.ic_wrong);
//                    iv_check_ansd.setVisibility(View.VISIBLE);
//                } else if (user_ans.equalsIgnoreCase("e")) {
//                    lay_optione.setBackgroundColor(Color.parseColor("#33ff0000"));
//                    iv_check_anse.setImageResource(R.drawable.ic_wrong);
//                    iv_check_anse.setVisibility(View.VISIBLE);
//                }

            }
        }
        else{

//            if (user_ans.equalsIgnoreCase("a")) {
            layout.setBackgroundColor(Color.parseColor("#33000000"));

//                iv_check_ansa.setImageResource(R.drawable.ic_wrong);
//                iv_check_ansa.setVisibility(View.VISIBLE);
//            } else if (user_ans.equalsIgnoreCase("b")) {
//                lay_optionb.setBackgroundColor(Color.parseColor("#33000000"));
////                iv_check_ansb.setImageResource(R.drawable.ic_wrong);
////                iv_check_ansb.setVisibility(View.VISIBLE);
//            } else if (user_ans.equalsIgnoreCase("c")) {
//                lay_optionc.setBackgroundColor(Color.parseColor("#33000000"));
////                iv_check_ansc.setImageResource(R.drawable.ic_wrong);
////                iv_check_ansc.setVisibility(View.VISIBLE);
//            } else if (user_ans.equalsIgnoreCase("d")) {
//                lay_optiond.setBackgroundColor(Color.parseColor("#33000000"));
////                iv_check_ansd.setImageResource(R.drawable.ic_wrong);
////                iv_check_ansd.setVisibility(View.VISIBLE);
//            } else if (user_ans.equalsIgnoreCase("e")) {
//                lay_optione.setBackgroundColor(Color.parseColor("#33000000"));
////                iv_check_anse.setImageResource(R.drawable.ic_wrong);
////                iv_check_anse.setVisibility(View.VISIBLE);
//            }

        }

        if(Constants.Ans_Status.equalsIgnoreCase("yes")|!Constants.checksolution)
            checkSolution();

    }

    public void setQuestion(){

        //String photoPath = Environment.getExternalStorageDirectory()+"/Topper's Club/images/maths.png";
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        String curr_item=sub_test_list.get(sp_testcategory.getSelectedIndex()).trim();
        Logg("curr_item",curr_item+"");
//        Logg("selecteditemm",sub_test_list.indexOf(data.get(count).getSub_name())+" "+data.get(count).getSub_name().trim()+" size "+sp_testcategory.getItems().size());

        if(!curr_item.equalsIgnoreCase(data.get(count).getSub_name().trim()))
            if(sub_test_list.indexOf(data.get(count).getSub_name().trim())<sp_testcategory.getItems().size())
                sp_testcategory.setSelectedIndex(sub_test_list.indexOf(data.get(count).getSub_name().trim()));

        iv_hint.setVisibility(View.GONE);
        iv_solution.setVisibility(View.GONE);
        iv_optiona.setVisibility(View.GONE);
        iv_optionb.setVisibility(View.GONE);
        iv_optionc.setVisibility(View.GONE);
        iv_optiond.setVisibility(View.GONE);
        iv_optione.setVisibility(View.GONE);
        iv_question.setVisibility(View.GONE);

        lay_optiona.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optionb.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optionc.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optiond.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optione.setBackgroundColor(Color.parseColor("#00000000"));

        iv_check_ansa.setVisibility(View.GONE);
        iv_check_ansb.setVisibility(View.GONE);
        iv_check_ansc.setVisibility(View.GONE);
        iv_check_ansd.setVisibility(View.GONE);
        iv_check_anse.setVisibility(View.GONE);

        if(data.get(count).getOptione().equalsIgnoreCase("null")&data.get(count).getOptione_img().equalsIgnoreCase("null")){
            lay_optione.setVisibility(View.GONE);
        }
        else {
            lay_optione.setVisibility(View.VISIBLE);

            if(!data.get(count).getOptione_img().equalsIgnoreCase("null")){

                iv_optione.setVisibility(View.VISIBLE);

                String []arr=data.get(count).getOptione_img().split("/");

                int len=arr.length;

                String photoPath1;
                photoPath1=photoPath+arr[len-1];

                //Logg("getOptione_img",photoPath1);

                bitmap = BitmapFactory.decodeFile(photoPath1, options);
                if(bitmap!=null){
                    iv_optione.setImageBitmap(bitmap);
                }
                // set Image
            }
        }

        if(data.get(count).getHint().equalsIgnoreCase("null")&data.get(count).getHint_img().equalsIgnoreCase("null")) {
            lay_hint.setVisibility(View.GONE);
        }
        else if(data.get(count).getHint().length()<=0 &data.get(count).getHint_img().equalsIgnoreCase("null")) {
            lay_hint.setVisibility(View.GONE);
        }
        else {
            lay_hint.setVisibility(View.VISIBLE);

        }

        Logg("image_toset",Constants.URL_Image+(data.get(count).getQuestion_img().replaceAll(" ", "%20")));
        if(!data.get(count).getQuestion_img().equalsIgnoreCase("null")){
            iv_question.setVisibility(View.VISIBLE);

            String []arr=data.get(count).getQuestion_img().split("/");
            int len=arr.length;
            String photoPath1;
            photoPath1=photoPath+arr[len-1];
            Logg("getQuestion_img",photoPath1);
            // photoPath=photoPath+data.get(count).getQuestion_img();
            bitmap = BitmapFactory.decodeFile(photoPath1, options);
            if(bitmap!=null){

                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;

                //Logg("imageHeight",imageHeight+" be");
                //Logg("imageWidht",imageWidth+" be");

                int h;
                h=Constants.Width/imageWidth;
                h=(int) h*imageHeight;

                //Logg("ration_height",h+"  "+Constants.Width);

                ViewGroup.LayoutParams params = iv_question.getLayoutParams();
                int width,height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, h, getResources().getDisplayMetrics());
                width=(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, imageWidth, getResources().getDisplayMetrics());

//                width= (int) (width*0.5);
//                height= (int) (height*0.5);

//                params.width =width;
                params.height = h;
                //Logg("imageHeight",height+" be");
                //Logg("imageWidth",imageWidth+" be");

                iv_question.setLayoutParams(params);

                iv_question.setImageBitmap(bitmap);
            }
        }

        Logg("image_toset2",Constants.URL_Image+(data.get(count).getOptiona_img().replaceAll(" ", "%20")));
        if(!data.get(count).getOptiona_img().equalsIgnoreCase("null")){
            iv_optiona.setVisibility(View.VISIBLE);

            String []arr=data.get(count).getOptiona_img().split("/");
            int len=arr.length;
            String photoPath1;
            photoPath1=photoPath+arr[len-1];
            //Logg("getHint_img",photoPath1);
            // photoPath=photoPath+data.get(count).getOptiona_img();
            bitmap = BitmapFactory.decodeFile(photoPath1, options);
            if(bitmap!=null){
                iv_optiona.setImageBitmap(bitmap);
            }
        }

        if(!data.get(count).getOptionb_img().equalsIgnoreCase("null")){
            iv_optionb.setVisibility(View.VISIBLE);
            String []arr=data.get(count).getOptionb_img().split("/");
            int len=arr.length;
            String photoPath1;
            photoPath1=photoPath+arr[len-1];
            //Logg("getOptionb_img",photoPath1);
//            photoPath=photoPath+data.get(count).getOptionb_img();
            bitmap = BitmapFactory.decodeFile(photoPath1, options);
            if(bitmap!=null){
                iv_optionb.setImageBitmap(bitmap);
            }
        }

        if(!data.get(count).getOptionc_img().equalsIgnoreCase("null")){
            iv_optionc.setVisibility(View.VISIBLE);
            String []arr=data.get(count).getOptionc_img().split("/");
            int len=arr.length;
            String photoPath1;
            photoPath1=photoPath+arr[len-1];

            //Logg("getOptionc_img",photoPath1);
//            photoPath=photoPath+data.get(count).getOptionc_img();
            bitmap = BitmapFactory.decodeFile(photoPath1, options);
            if(bitmap!=null){
                iv_optionc.setImageBitmap(bitmap);
            }

        }

        if(!data.get(count).getOptiond_img().equalsIgnoreCase("null")){
            iv_optiond.setVisibility(View.VISIBLE);
            String []arr=data.get(count).getOptiond_img().split("/");

            int len=arr.length;
            String photoPath1;
            photoPath1=photoPath+arr[len-1];

            //Logg("getOptiond_img",photoPath1);
            //photoPath=photoPath+data.get(count).getOptiond_img();
            bitmap = BitmapFactory.decodeFile(photoPath1, options);
            if(bitmap!=null){
                iv_optiond.setImageBitmap(bitmap);
            }
        }

        Log.e("options",count+" = "+data.get(count).getOptiona()+" - "+data.get(count).getOptionb()+" -"+data.get(count).getOptionc()+" -"
        +data.get(count).getOptiond()+" - "+data.get(count).getOptione()+"\n");
        String question = Html.fromHtml(data.get(count).getQuestion()).toString();
        tv_question.setText(question.replaceAll("\\\\",""));

        tv_optiona.setText(/*Html.fromHtml(*/data.get(count).getOptiona());

        tv_optionb.setText(/*Html.fromHtml(*/data.get(count).getOptionb());

        tv_optionc.setText(/*Html.fromHtml(*/data.get(count).getOptionc());

        tv_optiond.setText(/*Html.fromHtml(*/data.get(count).getOptiond());

        tv_optione.setText(/*Html.fromHtml(*/data.get(count).getOptione());

        String hint = Html.fromHtml(data.get(count).getHint()).toString();
        tv_hint.setText(hint.replaceAll("\\\\",""));

        if(data.get(count).getSolution().equalsIgnoreCase("null"))
            tv_solution.setText("");
        else
            tv_solution.setText(Html.fromHtml(data.get(count).getSolution()));

        lay_optiona.setClickable(true);
        lay_optionb.setClickable(true);
        lay_optionc.setClickable(true);
        lay_optiond.setClickable(true);
        lay_optione.setClickable(true);

        if(!Constants.checksolution){
            lay_optiona.setClickable(false);
            lay_optionb.setClickable(false);
            lay_optionc.setClickable(false);
            lay_optiond.setClickable(false);
            lay_optione.setClickable(false);

            checkSolution();
        }

        user_ans=data.get(count).getUser_ans();

        cort_ans=data.get(count).getAnswer();

        if(!user_ans.equalsIgnoreCase("no")){

//            lay_optiona.setClickable(false);
//            lay_optionb.setClickable(false);
//            lay_optionc.setClickable(false);
//            lay_optiond.setClickable(false);
//            lay_optione.setClickable(false);

            if(data.get(count).getSolution().equalsIgnoreCase("null")&data.get(count).getSolution_img().equalsIgnoreCase("null")) {
                tv_solution.setVisibility(View.GONE);
            }
            else
                tv_solution.setVisibility(View.VISIBLE);

            if(user_ans.equalsIgnoreCase(data.get(count).getAns_a()))
                checkAnswer(data.get(count).getAnswer(),user_ans,lay_optiona,iv_check_ansa);
            else if(user_ans.equalsIgnoreCase(data.get(count).getAns_b()))
                checkAnswer(data.get(count).getAnswer(),user_ans,lay_optionb,iv_check_ansb);
            else if(user_ans.equalsIgnoreCase(data.get(count).getAns_c()))
                checkAnswer(data.get(count).getAnswer(),user_ans,lay_optionc,iv_check_ansc);
            else if(user_ans.equalsIgnoreCase(data.get(count).getAns_d()))
                checkAnswer(data.get(count).getAnswer(),user_ans,lay_optiond,iv_check_ansd);
            else if(user_ans.equalsIgnoreCase(data.get(count).getAns_e()))
                checkAnswer(data.get(count).getAnswer(),user_ans,lay_optione,iv_check_anse);

//            if(Constants.Ans_Status.equalsIgnoreCase("yes")|!Constants.checksolution)
//            {
//
//                if (cort_ans.equalsIgnoreCase(user_ans)) {
//
//                    if (user_ans.equalsIgnoreCase("a")) {
//                        lay_optiona.setBackgroundColor(Color.parseColor("#33009900"));
//                        iv_check_ansa.setImageResource(R.drawable.ic_right);
//                        iv_check_ansa.setVisibility(View.VISIBLE);
//                    } else if (user_ans.equalsIgnoreCase("b")) {
//                        lay_optionb.setBackgroundColor(Color.parseColor("#33009900"));
//                        iv_check_ansb.setImageResource(R.drawable.ic_right);
//                        iv_check_ansb.setVisibility(View.VISIBLE);
//                    } else if (user_ans.equalsIgnoreCase("c")) {
//                        lay_optionc.setBackgroundColor(Color.parseColor("#33009900"));
//                        iv_check_ansc.setImageResource(R.drawable.ic_right);
//                        iv_check_ansc.setVisibility(View.VISIBLE);
//                    } else if (user_ans.equalsIgnoreCase("d")) {
//                        lay_optiond.setBackgroundColor(Color.parseColor("#33009900"));
//                        iv_check_ansd.setImageResource(R.drawable.ic_right);
//                        iv_check_ansd.setVisibility(View.VISIBLE);
//                    } else if (user_ans.equalsIgnoreCase("e")) {
//                        lay_optione.setBackgroundColor(Color.parseColor("#33009900"));
//                        iv_check_anse.setImageResource(R.drawable.ic_right);
//                        iv_check_anse.setVisibility(View.VISIBLE);
//                    }
//
//                } else {
//                    if (user_ans.equalsIgnoreCase("a")) {
//                        lay_optiona.setBackgroundColor(Color.parseColor("#33ff0000"));
//                        iv_check_ansa.setImageResource(R.drawable.ic_wrong);
//                        iv_check_ansa.setVisibility(View.VISIBLE);
//                    } else if (user_ans.equalsIgnoreCase("b")) {
//                        lay_optionb.setBackgroundColor(Color.parseColor("#33ff0000"));
//                        iv_check_ansb.setImageResource(R.drawable.ic_wrong);
//                        iv_check_ansb.setVisibility(View.VISIBLE);
//                    } else if (user_ans.equalsIgnoreCase("c")) {
//                        lay_optionc.setBackgroundColor(Color.parseColor("#33ff0000"));
//                        iv_check_ansc.setImageResource(R.drawable.ic_wrong);
//                        iv_check_ansc.setVisibility(View.VISIBLE);
//                    } else if (user_ans.equalsIgnoreCase("d")) {
//                        lay_optiond.setBackgroundColor(Color.parseColor("#33ff0000"));
//                        iv_check_ansd.setImageResource(R.drawable.ic_wrong);
//                        iv_check_ansd.setVisibility(View.VISIBLE);
//                    } else if (user_ans.equalsIgnoreCase("e")) {
//                        lay_optione.setBackgroundColor(Color.parseColor("#33ff0000"));
//                        iv_check_anse.setImageResource(R.drawable.ic_wrong);
//                        iv_check_anse.setVisibility(View.VISIBLE);
//                    }
//
//
//                    if (cort_ans.equalsIgnoreCase("a")) {
//                        lay_optiona.setBackgroundColor(Color.parseColor("#33009900"));
//                        iv_check_ansa.setImageResource(R.drawable.ic_right);
//                        iv_check_ansa.setVisibility(View.VISIBLE);
//                    } else if (cort_ans.equalsIgnoreCase("b")) {
//                        lay_optionb.setBackgroundColor(Color.parseColor("#33009900"));
//                        iv_check_ansb.setImageResource(R.drawable.ic_right);
//                        iv_check_ansb.setVisibility(View.VISIBLE);
//                    } else if (cort_ans.equalsIgnoreCase("c")) {
//                        lay_optionc.setBackgroundColor(Color.parseColor("#33009900"));
//                        iv_check_ansc.setImageResource(R.drawable.ic_right);
//                        iv_check_ansc.setVisibility(View.VISIBLE);
//                    } else if (cort_ans.equalsIgnoreCase("d")) {
//                        lay_optiond.setBackgroundColor(Color.parseColor("#33009900"));
//                        iv_check_ansd.setImageResource(R.drawable.ic_right);
//                        iv_check_ansd.setVisibility(View.VISIBLE);
//                    } else if (cort_ans.equalsIgnoreCase("e")) {
//                        lay_optione.setBackgroundColor(Color.parseColor("#33009900"));
//                        iv_check_anse.setImageResource(R.drawable.ic_right);
//                        iv_check_anse.setVisibility(View.VISIBLE);
//                    }
//                }
//
//                checkSolution();
//            }
//            else{
//
//                if (user_ans.equalsIgnoreCase("a")) {
//                    lay_optiona.setBackgroundColor(Color.parseColor("#33000000"));
////                iv_check_ansa.setImageResource(R.drawable.ic_wrong);
////                iv_check_ansa.setVisibility(View.VISIBLE);
//                } else if (user_ans.equalsIgnoreCase("b")) {
//                    lay_optionb.setBackgroundColor(Color.parseColor("#33000000"));
////                iv_check_ansb.setImageResource(R.drawable.ic_wrong);
////                iv_check_ansb.setVisibility(View.VISIBLE);
//                } else if (user_ans.equalsIgnoreCase("c")) {
//                    lay_optionc.setBackgroundColor(Color.parseColor("#33000000"));
////                iv_check_ansc.setImageResource(R.drawable.ic_wrong);
////                iv_check_ansc.setVisibility(View.VISIBLE);
//                } else if (user_ans.equalsIgnoreCase("d")) {
//                    lay_optiond.setBackgroundColor(Color.parseColor("#33000000"));
////                iv_check_ansd.setImageResource(R.drawable.ic_wrong);
////                iv_check_ansd.setVisibility(View.VISIBLE);
//                } else if (user_ans.equalsIgnoreCase("e")) {
//                    lay_optione.setBackgroundColor(Color.parseColor("#33000000"));
////                iv_check_anse.setImageResource(R.drawable.ic_wrong);
////                iv_check_anse.setVisibility(View.VISIBLE);
//                }
//
//            }

        }
        else if(!Constants.checksolution){

            if (cort_ans.equalsIgnoreCase(data.get(count).getAns_a())) {
                lay_optiona.setBackgroundColor(Color.parseColor("#33009900"));
//                iv_check_ansa.setImageResource(R.drawable.ic_wrong);
//                iv_check_ansa.setVisibility(View.VISIBLE);
            } else if (cort_ans.equalsIgnoreCase(data.get(count).getAns_b())) {
                lay_optionb.setBackgroundColor(Color.parseColor("#33009900"));
//                iv_check_ansb.setImageResource(R.drawable.ic_wrong);
//                iv_check_ansb.setVisibility(View.VISIBLE);
            } else if (cort_ans.equalsIgnoreCase(data.get(count).getAns_c())) {
                lay_optionc.setBackgroundColor(Color.parseColor("#33009900"));
//                iv_check_ansc.setImageResource(R.drawable.ic_wrong);
//                iv_check_ansc.setVisibility(View.VISIBLE);
            } else if (cort_ans.equalsIgnoreCase(data.get(count).getAns_d())) {
                lay_optiond.setBackgroundColor(Color.parseColor("#33009900"));
//                iv_check_ansd.setImageResource(R.drawable.ic_wrong);
//                iv_check_ansd.setVisibility(View.VISIBLE);
            } else if (cort_ans.equalsIgnoreCase(data.get(count).getAns_e())) {
                lay_optione.setBackgroundColor(Color.parseColor("#33009900"));
//                iv_check_anse.setImageResource(R.drawable.ic_wrong);
//                iv_check_anse.setVisibility(View.VISIBLE);
            }
        }

        checkHint();
        checkQuestion(count);
    }

    public void checkHint(){

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        if(data.get(count).getHint().equalsIgnoreCase("null")) {
            tv_hint.setText("");

            if(!data.get(count).getHint_img().equalsIgnoreCase("null")) {
//              iv_hint.setVisibility(View.VISIBLE);
                b_hint=true;
            }

        }

        if(b_hint)
            tv_hint.setMaxLines(1000);
        else
            tv_hint.setMaxLines(2);

        if(!data.get(count).getHint_img().equalsIgnoreCase("null")){

            if(b_hint) {
                iv_hint.setVisibility(View.VISIBLE);
                String[] arr = data.get(count).getHint_img().split("/");
                int len = arr.length;
                String photoPath1;
                photoPath1 = photoPath + arr[len - 1];

                //Logg("getHint_img", photoPath1);
                // photoPath=photoPath+data.get(count).getHint_img();

                bitmap = BitmapFactory.decodeFile(photoPath1, options);
                if (bitmap != null) {

                    int imageHeight = options.outHeight;
                    int imageWidth = options.outWidth;

                    //Logg("imageHeight",imageHeight+" be");
                    //Logg("imageWidht",imageWidth+" be");
                    int h;
                    h=Constants.Width/imageWidth;
                    h=(int) h*imageHeight;
                    //Logg("ration_height",h+"  "+Constants.Width);
                    ViewGroup.LayoutParams params = iv_hint.getLayoutParams();
                    int width,height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, h, getResources().getDisplayMetrics());
                    width=(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, imageWidth, getResources().getDisplayMetrics());
//                    width= (int) (width*0.5);
//                    height= (int) (height*0.5);

//                    params.width =width;
                    params.height = h;

                    //Logg("imageHeight",height+" be");
                    //Logg("imageWidth",imageWidth+" be");

                    iv_hint.setLayoutParams(params);

                    iv_hint.setImageBitmap(bitmap);
                }
            }
            else
                iv_hint.setVisibility(View.GONE);
        }
    }

    public void checkSolution(){

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        if(data.get(count).getSolution().equalsIgnoreCase("null")&data.get(count).getSolution_img().equalsIgnoreCase("null")) {
            lay_solution.setVisibility(View.GONE);
        }
        else {

            lay_solution.setVisibility(View.VISIBLE);

            if(!data.get(count).getSolution_img().equalsIgnoreCase("null")){

                iv_solution.setVisibility(View.VISIBLE);

                String []arr=data.get(count).getSolution_img().split("/");
                int len=arr.length;
                String photoPath1;
                photoPath1=photoPath+arr[len-1];

                //Logg("getSolution_img",photoPath1);
                // photoPath=photoPath+data.get(count).getSolution_img();
                bitmap = BitmapFactory.decodeFile(photoPath1, options);
                if(bitmap!=null){

                    int imageHeight = options.outHeight;
                    int imageWidth = options.outWidth;

                    //Logg("imageHeight",imageHeight+" be");
                    //Logg("imageWidht",imageWidth+" be");

                    int h;
                    h=Constants.Width/imageWidth;
                    h=(int) h*imageHeight;
                    //Logg("ration_height",h+"  "+Constants.Width);
                    ViewGroup.LayoutParams params = iv_solution.getLayoutParams();
                    int width,height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, h, getResources().getDisplayMetrics());
                    width=(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, imageWidth, getResources().getDisplayMetrics());
//                    width= (int) (width*0.5);
//                    height= (int) (height*0.5);


//                    params.width =width;
                    params.height = h;
                    //Logg("imageHeight",height+" be");
                    //Logg("imageWidth",imageWidth+" be");

                    iv_solution.setLayoutParams(params);

                    iv_solution.setImageBitmap(bitmap);

                }
            }
            //scroll down
        }
//        scrollView.fullScroll(View.FOCUS_DOWN);

//        scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
        tv_question.setMaxLines(2);
        tv_hint.setMaxLines(2);

        ObjectAnimator objectAnimator = ObjectAnimator.ofInt(scrollView, "scrollY", 0, 2000).setDuration(1000);
        objectAnimator.start();
//        for(i=0;i<50;i++) {
//
//           handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    //Logg("post_delayed","post_delayed"+i);
////                        scrollView.fullScroll(View.FOCUS_DOWN);
//                    scrollView.scrollBy(15,10);
////                    scrollView.scrollTo(0, scrollView.getBottom());
//                }
//            }, 500);
//        }
//                scrollView.post(new Runnable() {
//                    public void run() {
//                        scrollView.fullScroll(View.FOCUS_DOWN);
//                    }
//                });
//            }
//        });

    }

    public void checkQuestion(int count){

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        if(data.get(count).getQuestion().equalsIgnoreCase("null")) {
            tv_question.setText("");
        }
        else {
            if(b_que)
                tv_question.setMaxLines(500);
            else
                tv_question.setMaxLines(2);
        }

        if(!data.get(count).getQuestion_img().equalsIgnoreCase("null")){

            if(b_que) {
                iv_question.setVisibility(View.VISIBLE);

                String[] arr = data.get(count).getQuestion_img().split("/");
                int len = arr.length;
                String photoPath1;
                photoPath1 = photoPath + arr[len - 1];

                //Logg("getSolution_img", photoPath1);
                //photoPath=photoPath+data.get(count).getSolution_img();
                bitmap = BitmapFactory.decodeFile(photoPath1, options);
                if (bitmap != null) {

                    int imageHeight = options.outHeight;
                    int imageWidth = options.outWidth;

                    //Logg("imageHeight",imageHeight+" be");
                    //Logg("imageWidht",imageWidth+" be");

                    int h;
                    h=Constants.Width/imageWidth;
                    h=(int) h*imageHeight;

                    //Logg("after_height",h+"");
                    //Logg("phone_width",Constants.Width+"");

                    ViewGroup.LayoutParams params = iv_question.getLayoutParams();
                    int width,height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, h, getResources().getDisplayMetrics());
                    width=(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, imageWidth, getResources().getDisplayMetrics());
//                        width= (int) (width*0.5);
//                        height= (int) (height*0.5);

//                        params.width =width;

                    params.height = h;

                    //Logg("imageHeight",height+" be");
                    //Logg("imageWidth",imageWidth+" be");

                    iv_question.setLayoutParams(params);

                    iv_question.setImageBitmap(bitmap);

                }
            }
            else
                iv_question.setVisibility(View.GONE);

        }
        else{
            iv_question.setVisibility(View.GONE);
        }
    }

    public void checkResult(){

        for(int i=0;i<data.size();i++){

            cort_ans=data.get(i).getAnswer();
            user_ans=data.get(i).getUser_ans();
            if(cort_ans.equalsIgnoreCase(user_ans)){
                right_ans++;
            }
            else if(user_ans.equalsIgnoreCase("no")){
                unattempt++;
            }
            else{
                wrong_ans++;
            }
        }
    }

//    public void getQuestion() {
//
//        ApiEndpoint apiEndpoint = EndpointFactory.getApiEndpoint(this);
//
//            Call<Question> call = apiEndpoint.getQuestion();
//
//        call.enqueue(new Callback<Question>() {
//
//                @Override
//                public void onResponse(Call<Question> call, Response<Question> response) {
////                    Log.d("dd", "Total number of questions fetched : " + response.body().getQuestions().size());
//                    Question question = response.body();
//
//                    List<Question> list=question.getData();
//
//                    setPalletNo(list.size());
//
//                   // setQuestion();
//
//                    //Logg("question data",list.toString());
//                }
//
//                @Override
//                public void onFailure(Call<Question> call, Throwable t) {
//                    //Logg("log", "Got error : " + t.getLocalizedMessage());
//                }
//            });
//
//    }

    public void getQuestion()
    {


        if(!nw.isConnectingToInternet()) {
//            swipeRefreshLayout.setRefreshing(false);
            internetconnection(0);
            return;
        }

        progress = ProgressDialog.show(Play_new.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        if(!this.isFinishing())
        progress.show();

        RequestQueue queue= Volley.newRequestQueue(getApplicationContext());
        data=new ArrayList<>();

        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("testid",test_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url= Constants.URL_LV+"get_test_question";

        Logg("url",url+"");

        JsonObjectRequest request=new JsonObjectRequest(Request.Method.POST,url, jsonObject,new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {

                Logg("response test",res.toString());
                try {

                    if (res.has("response")) {

                        if (res.getString("response").equalsIgnoreCase("success")){

                            if(res.has("test_record")) {

                                setTimer(res.getJSONObject("test_record"));

                                if (res.has("data")) {
                                    JSONArray array = res.getJSONArray("data");

                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject obj = array.getJSONObject(i);

                                        JSONObject jsondata = new JSONObject(obj.getString("jsondata"));

                                        JSONObject obj_data = jsondata.getJSONObject("data");

//                                        String newObject = obj_data.toString().replaceAll("\\\\","");
//                                        Log.e("newObject",newObject);
                            //            JSONObject object = new JSONObject(jsondata.getJSONObject(obj_data));

                                        JSONObject obj_que = obj_data.getJSONObject("question");
                                        JSONObject obj_a = obj_data.getJSONObject("option a");
                                        JSONObject obj_b = obj_data.getJSONObject("option b");
                                        JSONObject obj_c = obj_data.getJSONObject("option c");
                                        JSONObject obj_d = obj_data.getJSONObject("option d");
                                        JSONObject obj_e = obj_data.getJSONObject("option e");

                                        obj_a.put("option","a");
                                        obj_b.put("option","b");
                                        obj_c.put("option","c");
                                        obj_d.put("option","d");
                                        obj_e.put("option","e");

                                        JSONArray opt_arr=new JSONArray();

                                        opt_arr.put(obj_a);
                                        opt_arr.put(obj_b);
                                        opt_arr.put(obj_c);
                                        opt_arr.put(obj_d);

                                        if(!obj_e.getString("value").equalsIgnoreCase("null")
                                                &!obj_e.getString("value").equalsIgnoreCase("")
                                                &obj_e.getString("value")!=null)
                                            opt_arr.put(obj_e);

                                        opt_arr=shuffleJsonArray(opt_arr);

                                        obj_a=opt_arr.getJSONObject(0);
                                        obj_b=opt_arr.getJSONObject(1);
                                        obj_c=opt_arr.getJSONObject(2);
                                        obj_d=opt_arr.getJSONObject(3);

                                        if(opt_arr.length()>4)
                                            obj_e=opt_arr.getJSONObject(4);


                                        JSONObject obj_ans = obj_data.getJSONObject("answer");
                                        JSONObject obj_hint = obj_data.getJSONObject("hint");
                                        JSONObject obj_sol = obj_data.getJSONObject("solution");

                                        data.add(new Question_getset_new( obj.getInt("id"), obj.getString("testid"),
                                                obj.getString("sub_test_name"), obj_hint.getString("value"), obj_que.getString("value"),
                                                obj_a.getString("value"), obj_b.getString("value"), obj_c.getString("value"),
                                                obj_d.getString("value"), obj_e.getString("value"), obj_ans.getString("value"),
                                                obj_sol.getString("value"), obj_hint.getString("image"), obj_que.getString("image"),
                                                obj_a.getString("image"), obj_b.getString("image"), obj_c.getString("image"),
                                                obj_d.getString("image"), obj_e.getString("image"), obj_sol.getString("image"), "no",
                                                obj_a.getString("option"),obj_b.getString("option"),obj_c.getString("option"),
                                                obj_d.getString("option"),obj_e.getString("option")));
                                        Log.e("options>>",i+" = "+obj_a.getString("value")+" ="+obj_b.getString("value")+" ="+
                                                obj_c.getString("value")+" ="+obj_d.getString("value")+" ="+obj_e.getString("value"));
                                    }

                                    setPalletNo(data.size());

                                    setQuestion();
                                    lay_optiona.setClickable(true);
                                    lay_optionb.setClickable(true);
                                    lay_optionc.setClickable(true);
                                    lay_optiond.setClickable(true);
                                    lay_optione.setClickable(true);

                                    tv_back.setClickable(true);
                                    tv_next.setClickable(true);
                                    lay_question.setClickable(true);
                                    tv_hint.setClickable(true);

                                    progress.dismiss();

                                    if(nw.isConnectingToInternet()) {
                                        if(Constants.offline_status.equalsIgnoreCase("yes"))
                                            MobileData_warning();
                                    }
                                }
                            }

                        }
                        if (res.getString("response").equalsIgnoreCase("no question")) {
                            Toast.makeText(Play_new.this,"No Question! We update soon",Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                    progress.dismiss();

                } catch (JSONException e) {
                    progress.dismiss();
                    Log.e("error","",e);
                }
            }
        },new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                progress.dismiss();
                CommonUtils.toast(Play_new.this, CommonUtils.volleyerror(arg0));
            }
        });

        request.setShouldCache(false);
        int socketTimeout = 50000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    public void internetconnection(final int i){

        if(isFinishing())
            return;

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
//                swipeRefreshLayout.setRefreshing(true);
                getQuestion();

            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getQuestion();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!Constants.checksolution){
            Logg("onResume", "onResume_call");
            count = 0;
//          setPalletNo(data.size());

            horizontal_sv.scrollTo(0,horizontal_sv.getTop());

            setQuestion();
        }


//        else if (Constants.checksolution){
//            Logg("onResume", "onResume_call");
//            count = 0;
////          setPalletNo(data.size());
//            if(data.size()>0){
//                for(int i=0;i<data.size();i++){
//                    data.get(i).setUser_ans("no");
//                }
//
//                lay_pallet.removeAllViews();
//                setPalletNo(data.size());
//                setQuestion();
//            }
//        }


    }

    public void setTimer(JSONObject obj){

        //Logg("setTimer_call","set_timer_call");

        //Logg("josndata",obj+"");

        sub_test_list=new ArrayList<>();
        sub_test_count_list=new ArrayList<>();
        JSONObject json,jsondata;
        JSONArray test_array;
        try {

            jsondata=new JSONObject(obj.getString("jsondata"));
            test_array=new JSONArray(obj.getString("post_description"));

            //Logg("josndata1",jsondata+"");
            //Logg("josnarray",test_array+"");
            testname=jsondata.getString("testname");
            st_time=jsondata.getString("time");
            st_total_que=jsondata.getString("question");

            //Logg("setTimer_call "+st_time,"set_timer_call "+st_total_que);

            for(int i=0;i<test_array.length();i++){

                JSONObject ob=test_array.getJSONObject(i);

                sub_test_list.add(ob.getString("test").trim());
                sub_test_count_list.add(Integer.valueOf(ob.getString("question_no")));

            }

            sp_testcategory.setItems(sub_test_list);


            sp_testcategory.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                @Override
                public void onItemSelected(MaterialSpinner view, int p, long id, Object item) {

                    int position=0;

                    if(sub_test_list.size()==1|p==0){

                        View vi = new View(Play_new.this);
                        vi = lay_pallet.getChildAt(count);
                        TextView t11=(TextView)vi.findViewById(R.id.que_no);
//                        t11.setBackgroundResource(R.drawable.butgray_light);
                        t11.setTextColor(Color.WHITE);

                        if(data.get(count).getUser_ans().equalsIgnoreCase("no")) {
                            t11.setBackgroundResource(R.drawable.but_review);
                        }
                        else
                            t11.setBackgroundResource(R.drawable.butgray_light);

                        count=0;

                        vi = lay_pallet.getChildAt(count);
                        TextView tv_selected=(TextView)vi.findViewById(R.id.que_no);
                        tv_selected.setBackgroundResource(R.drawable.purple_button_bgnd);
                        t11.setTextColor(Color.WHITE);
                        //count=0;

                        int scr=70*count;

                        horizontal_sv.scrollTo(scr,horizontal_sv.getTop());


                        setQuestion();
                    }
                    else{

                        for(int i=0;i<p;i++){
                            position=position+sub_test_count_list.get(i);
                        }

                        View vi = new View(Play_new.this);
                        vi = lay_pallet.getChildAt(count);
                        TextView t11=(TextView)vi.findViewById(R.id.que_no);
//                        t11.setBackgroundResource(R.drawable.butgray_light);
                        t11.setTextColor(Color.WHITE);

                        if(data.get(count).getUser_ans().equalsIgnoreCase("no")) {
                            t11.setBackgroundResource(R.drawable.but_review);
                        }
                        else
                            t11.setBackgroundResource(R.drawable.butgray_light);

                        backpress=count=position;


                        if(count==data.size()-1)
                            tv_next.setText("Submit");
                        else
                            tv_next.setText("Next");

                        vi = lay_pallet.getChildAt(count);
                        TextView tv_selected=(TextView)vi.findViewById(R.id.que_no);
                        tv_selected.setBackgroundResource(R.drawable.purple_button_bgnd);
                        t11.setTextColor(Color.WHITE);

                        int scr=70*count;

                        horizontal_sv.scrollTo(scr,horizontal_sv.getTop());

                        setQuestion();
                    }

                }
            });

        } catch (JSONException e) {

        }

        aa=Integer.valueOf(st_time);

        aa2=aa*60000;

        timer=new CountDownTimer(aa2, 1000) {

            @TargetApi(Build.VERSION_CODES.GINGERBREAD)
            @SuppressLint("NewApi")
            public void onTick(long millisUntilFinished) {

                tv_timer.setText(""+String.format("%d:%d",
                        TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                am=TimeUnit.MINUTES;
                as=TimeUnit.SECONDS;

                time_left=millisUntilFinished;

                if(Constants.offline_status.equalsIgnoreCase("yes")) {
                    if (nw.isConnectingToInternet()) {
                        if (net_dialog != null)
                            if (!net_dialog.isShowing())
                                MobileData_warning();
                    }
                }

            }

            public void onFinish() {
                tv_timer.setText("Done!");
//                            volleyupdateapplicant();
                // alert("Time Up");

                timer.cancel();

                if(!isFinishing())
                    timeOutDialog();
            }

        }.start();

    }

    private void timeOutDialog() {

        if(isFinishing())
            return;

        final Dialog dialog=new Dialog(Play_new.this,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.time_out_dialog);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        TextView Submit=(TextView)dialog.findViewById(R.id.tv_submit);
        TextView text,text1;
        text=(TextView)dialog.findViewById(R.id.text);
        text1=(TextView)dialog.findViewById(R.id.text1);
        text.setTypeface(font_medium);
        text1.setTypeface(font_medium);
        Submit.setTypeface(font_demi);

        text1.setText("Your session has expired.");

        right_ans=0;
        unattempt=0;
        wrong_ans=0;

        for(int i=0;i<data.size();i++)
        {

            if(data.get(i).getAnswer().equalsIgnoreCase(data.get(i).getUser_ans()))
            {
                right_ans++;
            }
            else if(data.get(i).getUser_ans().equalsIgnoreCase("no"))
            {
                unattempt++;
            }
            else
                wrong_ans++;
        }

//        volleyupdateresult(false);

        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!nw.isConnectingToInternet()) {
                    Toast.makeText(Play_new.this,"Enable Your Internet Connection",Toast.LENGTH_SHORT).show();
                    return;
                }

                volleyupdateresult(true);

                dialog.dismiss();

            }
        });

    }

    Dialog dialogP;


    public void alert(String s)
    {

        if(isFinishing())
            return;

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Play_new.this);

        alertDialogBuilder.setMessage(s);

        alertDialogBuilder.setPositiveButton("YES",new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                right_ans=0;
                unattempt=0;
                wrong_ans=0;

                for(int i=0;i<data.size();i++)
                {

                    if(data.get(i).getAnswer().equalsIgnoreCase(data.get(i).getUser_ans()))
                    {
                        right_ans++;
                    }
                    else if(data.get(i).getUser_ans().equalsIgnoreCase("no"))
                    {
                        unattempt++;
                    }
                    else
                        wrong_ans++;
                }

                if(!nw.isConnectingToInternet()) {
                    net_count=3;
                    Toast.makeText(Play_new.this,"No Internet Connection",Toast.LENGTH_SHORT).show();
                    return;
                }

                dialog.dismiss();
                if(timer!=null) {
                    timer.cancel();
                }

                volleyupdateresult(true);


            }
        });

        alertDialogBuilder.setNegativeButton("NO",new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                doneBAckPress=0;
                dialog.dismiss();

            }
        });

        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.show();

    }

    Dialog net_dialog;
    public void MobileData_warning()
    {

        if(isFinishing())
            return;

        net_dialog=new Dialog(Play_new.this,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        net_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        net_dialog.setContentView(R.layout.onpause_alert);
        Window window = net_dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        net_dialog.setCanceledOnTouchOutside(false);
        net_dialog.setCancelable(false);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        TextView Submit=(TextView)net_dialog.findViewById(R.id.tv_submit);
        TextView text,text1;
        text=(TextView)net_dialog.findViewById(R.id.text);
        text1=(TextView)net_dialog.findViewById(R.id.text1);
        text.setTypeface(font_medium);
        text1.setTypeface(font_medium);
        Submit.setTypeface(font_demi);

        String msg="Please disconnect your internet connection to continue the test";

        if(net_count>0&net_count<3) {
            msg = "Please disconnect your internet connection otherwise your test will be submitted automatically";
            net_dialog.show();
        }
        else if(net_count==0)
            net_dialog.show();
            //        else if(net_count==2)
//            msg="Disable your network connection for play continue. Next time test will be submitted automatically";
        else if(net_count==3){

            right_ans=0;
            unattempt=0;
            wrong_ans=0;

            for(int i=0;i<data.size();i++)
            {

                if(data.get(i).getAnswer().equalsIgnoreCase(data.get(i).getUser_ans()))
                {
                    right_ans++;
                }
                else if(data.get(i).getUser_ans().equalsIgnoreCase("no"))
                {
                    unattempt++;
                }
                else
                    wrong_ans++;
            }

            if(!check_result)
                volleyupdateresult(true);
        }

        text1.setText(msg);

        Submit.setText("Continue");

        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!nw.isConnectingToInternet()) {

                    net_dialog.dismiss();
                    net_count++;

                }
                else {
                    Toast.makeText(Play_new.this, "Disable Network Connection", Toast.LENGTH_SHORT).show();

//                    if(net_count>=3){
//
//                        right_ans=0;
//
//                        for(int i=0;i<data.size();i++)
//                        {
//
//                            if(data.get(i).getAnswer().equalsIgnoreCase(data.get(i).getUser_ans()))
//                            {
//                                right_ans++;
//                            }
//                            else if(data.get(i).getUser_ans().equalsIgnoreCase("no"))
//                            {
//                                unattempt++;
//                            }
//                            else
//                                wrong_ans++;
//                        }
//
//
//                        volleyupdateresult(true);
//                    }
                }
            }
        });

    }

    public void volleyupdateresult(final boolean bb)
    {

        Logg("total time",aa2+" left "+time_left);
        final long timetaken=aa2-time_left;
        Logg("time taken",(aa2-time_left)+"");



        correct= (int) right_ans;
        Logg("right_ans",right_ans+"");
        Logg("wrong_ans",wrong_ans+"");

        if(Constants.Negative_Ans.equalsIgnoreCase("yes")) {
            float marks = (float) (wrong_ans * .25);  // count negative marks
            right_ans = right_ans - marks;
        }

        Logg("right_ans1",right_ans+"");

        progress = ProgressDialog.show(Play_new.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        progress.show();

        RequestQueue queue=Volley.newRequestQueue(getApplicationContext());
        String url=Constants.URL_LV+"insert_result";
        Log.e("url>>",url);
        StringRequest request=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s)
            {
                try {
                    JSONObject res=new JSONObject(s);
                    Log.e("result_res",res+"");
                    if(res.getString("response").equalsIgnoreCase("Successfully")){
                        if (timer!=null)
                            timer.cancel();

                        check_result=true;

                        if(bb) {
                            if (dialogP!=null)
                            {
                                dialogP.dismiss();
                            }

                            Intent in = new Intent(Play_new.this, Result_new.class);
                            in.putExtra("response", res.toString());
                            in.putExtra("correct", correct);
                            in.putExtra("testname", testname);
                            in.putExtra("unattempt", unattempt);
                            in.putExtra("wrong", wrong_ans);
                            in.putExtra("totalquestion", data.size());
                            startActivity(in);

                        }
                    }
                    else if(res.getString("status").equalsIgnoreCase("error"))
                    {
                        Toast.makeText(Play_new.this,res.getString("response"),Toast.LENGTH_LONG).show();
                    }
                    progress.dismiss();
                }
                catch (JSONException e) {
                    e.printStackTrace();
                    progress.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(Play_new.this,CommonUtils.volleyerror(volleyError));
                progress.dismiss();

            }
        })
        {
            @Override
            protected Map<String, String> getParams()
            {

                Map<String, String> params = new HashMap<String, String>();
                params.put("marks",right_ans+"");
                params.put("uid",login_useremail);
                params.put("testid",test_id);
                params.put("time",timetaken+"");
                params.put("admission_no", Constants.addmissionno);
                Logg("resultsubmit params", params + "");
                return params;
            }
        };

        request.setShouldCache(false);
        int socketTimeout = 50000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    public JSONArray shuffleJsonArray (JSONArray array) {
        // Implementing Fisher–Yates shuffle
        Random rnd = new Random();
        for (int i = array.length()-1; i >= 0; i--)
        {
            try {
                int j = rnd.nextInt(i + 1);
                // Simple swap
                Object object = array.get(j);
                array.put(j, array.get(i));

                array.put(i, object);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return array;
    }

}