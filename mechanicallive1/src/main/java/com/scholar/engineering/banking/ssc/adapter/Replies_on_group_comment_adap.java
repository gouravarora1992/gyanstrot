package com.scholar.engineering.banking.ssc.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
//import com.scholar.engineering.banking.ssc.Profile;
import com.scholar.engineering.banking.ssc.R;
//import com.scholar.engineering.banking.ssc.Replies;
import com.scholar.engineering.banking.ssc.Replies_Common_Comment_homepage;
import com.scholar.engineering.banking.ssc.userprofile.UserProfile;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.DatabaseHandler;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.TouchImageView;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.toast;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.volleyerror;
import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;
import static com.scholar.engineering.banking.ssc.utils.Constants.addmissionno;

/**
 * Created by surender on 3/14/2017.
 */

public class Replies_on_group_comment_adap extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context c;
    JSONArray data;
    JSONObject jsonobj, jsonfield;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options, optionsuser;

    ImageLoader imageLoader;

    int likecount = 0;
    NetworkConnection nw;

    public Replies_on_group_comment_adap(Context c, JSONArray data) {
        this.c = c;
        this.data = data;
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(c));
        nw = new NetworkConnection(c);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.blankimage)
                .showImageForEmptyUri(R.drawable.congrrr)
                .showImageOnFail(R.drawable.congrrr)
                .cacheInMemory(false)
                .cacheOnDisk(false)
                .imageScaleType(ImageScaleType.EXACTLY)
                .considerExifParams(true)
                .build();

        optionsuser = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.user_default)
                .showImageForEmptyUri(R.drawable.user_default)
                .showImageOnFail(R.drawable.user_default)
                .cacheInMemory(false)
                .cacheOnDisk(false)
                .considerExifParams(true)
                .build();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v0 = inflater.inflate(R.layout.replies_list_item_new, viewGroup, false);
        viewHolder = new ViewHolder0(v0);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ViewHolder0 vh0 = (ViewHolder0) viewHolder;
        setComment(vh0, position);
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    private void setComment(final ViewHolder0 vh0, final int p) {

        try {

            Typeface font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");
            Typeface font_medium = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");
            jsonobj = data.getJSONObject(p);

            jsonfield = new JSONObject(jsonobj.getString("jsondata"));
            String time = Utility.getDate(jsonobj.getString("timestamp"));

            vh0.text.setText(jsonobj.getString("comment") + " Reply");

            vh0.tv_time.setText(time);

            vh0.tv_collage.setText(jsonfield.getString("collage").replaceAll("%20", " ") + "-" +
                    jsonfield.getString("section").replaceAll("%20", " "));

            vh0.tv_uname.setText(jsonfield.getString("name").replaceAll("%20", " "));

            vh0.tv_time.setTypeface(font_medium);
            vh0.tv_collage.setTypeface(font_medium);
            vh0.tv_uname.setTypeface(font_demi);
            vh0.tv_comment.setTypeface(font_medium);
            vh0.tv_rewardtext.setTypeface(font_medium);
            vh0.text.setTypeface(font_medium);

            String com = jsonfield.getString("comments");

            com = com.replaceAll("%20", " ");

            vh0.tv_comment.setText(com);

            if (User_Email.equalsIgnoreCase(jsonobj.getString("uid"))) {
                vh0.tv_rewardtext.setText(" Delete");
                vh0.tv_rewardtext.setTextColor(c.getResources().getColor(R.color.greytextcolor));
                vh0.rew_image.setImageResource(R.drawable.ic_delete_grey_new);
            } else {
                if (jsonobj.getString("likestatus").equalsIgnoreCase("like")) {
                    vh0.tv_rewardtext.setText(c.getResources().getString(R.string.like) + " (" + jsonobj.getString("likecount") + ")");
                    vh0.tv_rewardtext.setTextColor(c.getResources().getColor(R.color.like_text_color));
                    vh0.rew_image.setImageResource(R.drawable.ic_bulb_filled);
                } else {
                    vh0.tv_rewardtext.setText(c.getResources().getString(R.string.like) + " (" + jsonobj.getString("likecount") + ")");
                    vh0.tv_rewardtext.setTextColor(c.getResources().getColor(R.color.greytextcolor));
                    vh0.rew_image.setImageResource(R.drawable.ic_bulb_lightup);
                }
            }

            if (!jsonfield.getString("userimage").equalsIgnoreCase("null")) {
                imageLoader.getInstance().displayImage(jsonfield.getString("userimage"), vh0.userimg, optionsuser, animateFirstListener);
            } else
                vh0.userimg.setImageResource(R.drawable.user_default);

            Logg("image replies", Constants.URL_Image + jsonfield.getString("com_image"));
            if (!jsonfield.getString("com_image").equalsIgnoreCase("null")) {
                Glide.with(c).load(Constants.URL_Image + jsonfield.getString("com_image")).error(R.drawable.congrrr).into(vh0.cimage);
                vh0.cimage.setVisibility(View.VISIBLE);
            } else
                vh0.cimage.setVisibility(View.GONE);

            vh0.userimg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        jsonobj = data.getJSONObject(p);
                        Intent in = new Intent(c, UserProfile.class);
                        in.putExtra("email", jsonobj.getString("uid"));
                        c.startActivity(in);
                    } catch (JSONException e) {

                    }
                }
            });

            vh0.cimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (vh0.cimage.getDrawable() == null) {
                    } else {
                        imagedialog(vh0.cimage);
                    }
                }
            });

            vh0.rewarlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (!nw.isConnectingToInternet()) {
                        Toast.makeText(c, "No Internet Connection", Toast.LENGTH_LONG).show();
                        return;
                    }


                    try {
                        jsonobj = data.getJSONObject(p);

                        if (User_Email.equalsIgnoreCase(jsonobj.getString("uid"))) {
                            deletcomment(p);
                        } else {
                            volleylike_post(p);
                        }

                    } catch (JSONException e) {

                    }
                }
            });

            vh0.reply_lay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        jsonobj = data.getJSONObject(p);
                        Intent in = new Intent(c, Replies_Common_Comment_homepage.class);
                        in.putExtra("commentid", jsonobj.getString("id"));
                        c.startActivity(in);

                    } catch (JSONException e) {
                    }

                }
            });

        } catch (JSONException e) {
            Log.e("exception", "", e);
        }

    }

    public class ViewHolder0 extends RecyclerView.ViewHolder {

        TextView tv_uname;
        TextView tv_collage;
        TextView tv_time;
        TextView tv_comment;
        TextView tv_rewardtext;
        ImageView cimage;
        ImageView rew_image;
        CircleImageView userimg;
        TextView text;
        LinearLayout rewarlay, reply_lay;

        public ViewHolder0(View b) {
            super(b);
            tv_uname = (TextView) b.findViewById(R.id.nameid);
            tv_comment = (TextView) b.findViewById(R.id.commentid);
            userimg = (CircleImageView) b.findViewById(R.id.userimageid);
            rew_image = (ImageView) b.findViewById(R.id.rewardimageid);
            cimage = (ImageView) b.findViewById(R.id.iv_com_image_id);
            rewarlay = (LinearLayout) b.findViewById(R.id.rewardlayid);
            reply_lay = (LinearLayout) b.findViewById(R.id.reply_layid);
            tv_collage = (TextView) b.findViewById(R.id.collageid);
            tv_time = (TextView) b.findViewById(R.id.timeid);
            tv_rewardtext = (TextView) b.findViewById(R.id.rewardtextid);
            text = (TextView) b.findViewById(R.id.replytextid);

        }
    }

    public void deletcomment(final int a) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(c);

        alertDialogBuilder.setMessage("You want to delete this comment");
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                volleydeletereplycomment(a);

            }
        });

        alertDialogBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.show();

    }

    public void volleydeletereplycomment(final int pos) {

        String psid = null, repliesid = null;
        try {
            jsonobj = data.getJSONObject(pos);
            psid = jsonobj.getString("postid");
            repliesid = jsonobj.getString("id");

        } catch (JSONException e) {
        }

        RequestQueue queue = Volley.newRequestQueue(c);
        JSONObject json = new JSONObject();
        try {
            json.put("id", psid);
            json.put("email", User_Email);
            json.put("admission_no", addmissionno);
            json.put("psid", repliesid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Logg("partam",json+"");
        String url = Constants.URL_LV + "replies_delete";
        JsonObjectRequest jsonreq = new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject res) {

                try {
                    if (res.getString("scalar").equals("Record deleted successfully") == true) {
                        Toast.makeText(c, "Comment deleted", Toast.LENGTH_SHORT).show();
                        data.remove(pos);
                        notifyDataSetChanged();

                    } else {
                        Toast.makeText(c, "Try Again", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg) {
                toast(c, volleyerror(arg));
            }

        });

        jsonreq.setShouldCache(false);
        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonreq.setRetryPolicy(policy);
        queue.add(jsonreq);
    }

    public void volleylike_post(final int p) {

        String psid = "", repliesid = "";
        try {
            jsonobj = data.getJSONObject(p);
            psid = jsonobj.getString("postid");
            likecount = jsonobj.getInt("likecount");
            repliesid = jsonobj.getString("id");
        } catch (JSONException e) {
        }

        RequestQueue queue = Volley.newRequestQueue(c);
        JSONObject json = new JSONObject();
        try {
            json.put("id", psid);
            json.put("email", User_Email);
            json.put("admission_no", addmissionno);
            json.put("psid", repliesid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url = Constants.URL_LV + "like_replies";
        Logg("like_url", url+","+json);
        JsonObjectRequest jsonreq = new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {

                try {

                    if (res.getString("scalar").equals("like")) {

                        likecount++;
                        jsonobj.put("likestatus", "like");
                        jsonobj.put("likecount", likecount);

                        data.put(p, jsonobj);
                        notifyItemChanged(p);
                    } else if (res.getString("scalar").equals("dislike")) {

                        if (likecount > 0)
                            likecount--;
                        jsonobj.put("likestatus", "dislike");
                        jsonobj.put("likecount", likecount);

                        data.put(p, jsonobj);
                        notifyItemChanged(p);
                    } else {
                        Toast.makeText(c, "Not done", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError arg) {
                toast(c, volleyerror(arg));
            }
        });
        jsonreq.setShouldCache(false);
        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonreq.setRetryPolicy(policy);
        queue.add(jsonreq);
    }

    public void imagedialog(ImageView im) {
        final Dialog dialog = new Dialog(c);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.imagedialog1);

        LinearLayout imglarge = (LinearLayout) dialog.findViewById(R.id.dialogimageid);
        ImageView cancel = (ImageView) dialog.findViewById(R.id.crossid);

        Bitmap bmp = ((BitmapDrawable) im.getDrawable()).getBitmap();
        TouchImageView img = new TouchImageView(c);
        img.setImageBitmap(bmp);
        img.setMaxZoom(4f);
        img.setMinimumWidth(Constants.Width);
        img.setMinimumHeight(Constants.Height);
        imglarge.addView(img);

        dialog.show();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }


}
