package com.scholar.engineering.banking.ssc.adapter;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;

import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.numberprogressbar.NumberProgressBar;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.Comment_Common_homepage;
import com.scholar.engineering.banking.ssc.File_download;
import com.scholar.engineering.banking.ssc.Jobs_all;
import com.scholar.engineering.banking.ssc.News;
import com.scholar.engineering.banking.ssc.Onlineexam;
import com.scholar.engineering.banking.ssc.Pdf_view_Webview;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Topic_Search_Data;
import com.scholar.engineering.banking.ssc.Workshop_Register;
import com.scholar.engineering.banking.ssc.Workshopdetail;
import com.scholar.engineering.banking.ssc.Workshoplist;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.DatabaseHandler;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.scholar.engineering.banking.ssc.HomeActivity.obj_champions;
import static com.scholar.engineering.banking.ssc.HomeActivity.obj_menotrs;
import static com.scholar.engineering.banking.ssc.utils.Constants.MY_PERMISSIONS_REQUEST;
import static com.scholar.engineering.banking.ssc.utils.Utility.ConvertDate;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.Utility.check_key;
import static com.scholar.engineering.banking.ssc.utils.Utility.hasPermissions;

/**
 * Created by surender on 3/14/2017.
 */

public class Home_RecyclerViewAdapter2 extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    Context c;
    Home_getset home_obj;
    ArrayList<Home_getset> data;
    TextView progtext, tv_openfile;
    NumberProgressBar bar;
    JSONObject jsonobj;
    String  URL;
    String userans = "ua", rightans = "";
    JSONObject jsonpoll;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options, optionsuser;

    DatabaseHandler dbh;
    int id, opt_a = 10, opt_b = 0, opt_c = 0, opt_d = 0, perc = 0;
    public static ProgressDialog progress;
    private static final int ITEMS_PER_AD = 5;
    private static final int AD = 9;
    String clas;
//    ViewHolder0 vh0;
    ViewHolder1 vh1;
    ViewHolder2 vh2;
    ViewHolder3 vh3;
    ViewHolder4 vh4;
    ViewHolder5 vh5;
//    ViewHolder7 vh7;
//    ViewHolder8 vh8;
    NativeExpressAdViewHolder viewHolder1;
    Champions_Holder champions_holder;
    Mentor_Holder mentor_holder;
    Banner_Holder banner_holder;
    NetworkConnection nw;
    ImageLoader imageLoader;
    Typeface font_demi;
    Typeface font_medium;
    int homeVersion = 0;
    //    JSONObject obj_banner=new JSONObject();
    JSONArray banner_array, banner_position;

    Banner_list_adapter_rec banner_list_adapter;
    Champions_list_rec_adapter champions_list_adapter;
    Mentor_list_adapter_rec mentor_list_adapter;


    public void addSingleItem(int p, Home_getset data, JSONArray banner_array) {
        this.data.add(p, data);
        this.banner_array = banner_array;
        Logg("positioninserted", p + "");
        notifyItemInserted(p);
    }

    public void addItme(ArrayList<Home_getset> data1) {

        nw = new NetworkConnection(c);
        Logg("addItem ", clas);
        if (clas.equalsIgnoreCase("homefragment")) {
            if (obj_champions != null) {
                Logg("addItem ", obj_champions + "");
                for (int i = 0; i < data1.size(); i++) {

                    Logg("postid " + i, data1.get(i).getId() + " ");
                    if (i != 0)
                        if (i % 5 == 0) {

                            Random rand = new Random();
                            int n = rand.nextInt(2) + 1;
                            Home_getset obj = new Home_getset();

                            if (n == 2 & obj_menotrs != null) {
                                obj.setFieldtype("11");
                            }
//                            else if(n==3&banner_array!=null) {
//                                obj.setFieldtype("12");
//                            }
                            else if (n == 1) {
                                obj.setFieldtype("10");
                            }

                            data.add(obj);
                        }
                    data.add(data1.get(i));
                }
            } else {
                data.addAll(data1);
            }
        } else
            data.addAll(data1);

    }

    public Home_RecyclerViewAdapter2(Context c, ArrayList<Home_getset> data1, String clas) {
        this.c = c;
        this.clas = clas;
        dbh = new DatabaseHandler(c);

        data = new ArrayList<>();

        font_medium = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");
        font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.blankimage)
                .showImageForEmptyUri(R.drawable.congrrr)
                .showImageOnFail(R.drawable.congrrr)
                .cacheInMemory(false)
                .imageScaleType(ImageScaleType.EXACTLY)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        optionsuser = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.user_default)
                .showImageForEmptyUri(R.drawable.user_default)
                .showImageOnFail(R.drawable.user_default)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(c));

        nw = new NetworkConnection(c);

        if (clas.equalsIgnoreCase("homefragment")) {


            if (obj_champions != null) {
                Logg("Home_RecyclerView", obj_champions + "");
                for (int i = 0; i < data1.size(); i++) {

                    if (i != 0) {

                        if (i % 5 == 0) {
                            Random rand = new Random();
                            int n = rand.nextInt(2) + 1;
                            Home_getset obj = new Home_getset();

                            if (n == 2 & obj_menotrs != null) {
                                obj.setFieldtype("11");
                            } else if (n == 1) {
                                obj.setFieldtype("10");
                            }

                            data.add(obj);
                        }
                    }

                    data.add(data1.get(i));
                }
            } else
                data.addAll(data1);

//            if(banner_array==null)
//                getBanner(email,data.size());

        } else
            data.addAll(data1);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
//            case 0:
//                View v0 = inflater.inflate(R.layout.home_followgroup_comment_layout, viewGroup, false);
//                viewHolder = new ViewHolder0(v0);
//                break;

            case 1:
                View v1 = inflater.inflate(R.layout.home_news_postbyuserlayout, viewGroup, false);
                viewHolder = new ViewHolder1(v1);
                break;

            case 2:
                View v2 = inflater.inflate(R.layout.home_new_onlinetestlayout, viewGroup, false);
                viewHolder = new ViewHolder2(v2);
                break;

            case 3:
                View v3 = inflater.inflate(R.layout.home_newjoblayout, viewGroup, false);
                viewHolder = new ViewHolder3(v3);
                break;

            case 4:
                View v4 = inflater.inflate(R.layout.home_pdflayout_new, viewGroup, false);
                viewHolder = new ViewHolder4(v4);
                break;

            case 5:
                View v5 = inflater.inflate(R.layout.home_workshoplayout, viewGroup, false);
                viewHolder = new ViewHolder5(v5);
                break;

            case 7:
//                View v7 = inflater.inflate(R.layout.home_postquestion_layout_new, viewGroup, false);
//                viewHolder = new ViewHolder7(v7);
//                break;
            case 8:
//                View v8 = inflater.inflate(R.layout.home_metadata_link_layout, viewGroup, false);
//                viewHolder = new ViewHolder8(v8);
//                break;
            case 9:
                View v9 = inflater.inflate(R.layout.home_list_header, viewGroup, false);
                viewHolder = new NativeExpressAdViewHolder(v9);
                break;
            case 10:
                View v10 = inflater.inflate(R.layout.champions_recyclerview, viewGroup, false);
                viewHolder = new Champions_Holder(v10);
                break;
            case 11:
                View v11 = inflater.inflate(R.layout.mentor_recyclerview, viewGroup, false);
                viewHolder = new Mentor_Holder(v11);
                break;
            case 12:
                View v12 = inflater.inflate(R.layout.mentor_recyclerview, viewGroup, false);
                viewHolder = new Banner_Holder(v12);
                break;
            default:
                View v = inflater.inflate(R.layout.home_new_onlinetestlayout, viewGroup, false);
                viewHolder = new ViewHolder5(v);
                break;
        }
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        switch (viewHolder.getItemViewType()) {
//            case 0:
//                vh0 = (ViewHolder0) viewHolder;
//                setComment(vh0, position);
//                break;
            case 1:
                vh1 = (ViewHolder1) viewHolder;
                //Logg("field_type","1");
                setPost(vh1, position);
                break;
            case 2:
                vh2 = (ViewHolder2) viewHolder;
                //Logg("field_type","2");
                setOnlinetest(vh2, position);
                break;
            case 3:
                vh3 = (ViewHolder3) viewHolder;
                //Logg("field_type","3");
                setJobs(vh3, position);
                break;
            case 4:
                vh4 = (ViewHolder4) viewHolder;
                //Logg("field_type","4");
                setPdf(vh4, position);
                break;

            case 5:
                vh5 = (ViewHolder5) viewHolder;
                //Logg("field_type","5");
                setWorkshop(vh5, position);
                break;
//            case 7:
//                vh7 = (ViewHolder7) viewHolder;
//                //Logg("field_type","7");
//                setQuestion(vh7, position);
//                break;
//            case 8:
//                vh8 = (ViewHolder8) viewHolder;
//                //Logg("field_type","8");
//                setMetaData(vh8, position);
//                break;
            case 9:

                viewHolder1 = (NativeExpressAdViewHolder) viewHolder;
                viewHolder1.joblay.setOnClickListener(this);
                viewHolder1.testlay.setOnClickListener(this);
                viewHolder1.post_lay.setOnClickListener(this);
                viewHolder1.event_lay.setOnClickListener(this);
                viewHolder1.pdf_lay.setOnClickListener(this);
                break;
            case 10:
                champions_holder = (Champions_Holder) viewHolder;
                setChampions(champions_holder, position);
                break;
            case 11:
                mentor_holder = (Mentor_Holder) viewHolder;
                setMentor(mentor_holder, position);
                break;
            case 12:
                banner_holder = (Banner_Holder) viewHolder;
                setBanner(banner_holder, position);

        }
    }

    @Override
    public int getItemCount() {
        Logg("listsize", data.size() + "");
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (position == 0 & clas.equalsIgnoreCase("homefragment")) {
            return 9;
        } else {
            Home_getset obj = (Home_getset) data.get(position);
            Logg("getfieldty", position + " - " + obj);
            if (obj != null) {
                if (obj.getId() != 0) {
                    if (obj.getFieldtype().equalsIgnoreCase("10")) {
                        return 10;
                    } else if (obj.getFieldtype().equalsIgnoreCase("11")) {
                        return 11;
                    } else if (obj.getFieldtype().equalsIgnoreCase("12")) {
                        return 12;
                    } else if (obj.getFieldtype().equalsIgnoreCase("0")) {
                        return 0;
                    } else if (obj.getFieldtype().equalsIgnoreCase("1")) {
                        return 1;
                    } else if (obj.getFieldtype().equalsIgnoreCase("2")) {
                        return 2;
                    } else if (obj.getFieldtype().equalsIgnoreCase("3")) {
                        return 3;
                    } else if (obj.getFieldtype().equalsIgnoreCase("4")) {
                        return 4;
                    } else if (obj.getFieldtype().equalsIgnoreCase("5")) {
                        return 5;
                    } else if (obj.getFieldtype().equalsIgnoreCase("7")) {
                        return 7;
                    } else if (obj.getFieldtype().equalsIgnoreCase("8")) {
                        return 8;
                    }
                }
            }

            return -1;
        }
    }

//    private void setComment(final ViewHolder0 vh0, final int p) {
//
//        try {
//
//            final Home_getset home = (Home_getset) data.get(p);
//
//            jsonobj = new JSONObject(home.getJsonfield());
//
//            vh0.tv_time.setText(getDate(home.getTimestamp()));
//            vh0.tv_collage.setText(jsonobj.getString("collage"));
//            vh0.tv_uname.setText(jsonobj.getString("name"));
//            String com = jsonobj.getString("comments");
//            com = com.replaceAll("%20", " ");
//            vh0.tv_comm.setText(com);
//
//            vh0.tv_reward_count.setText(jsonobj.getString("userreward") + " Reward");
//
//            vh0.tv_replies_count.setText(home.getCommentcount() + " Comments");
//
//            vh0.tv_member.setText(jsonobj.getString("groupmember") + " Members");
//            vh0.tv_groupname.setText(jsonobj.getString("groupname"));
//            if (home.getLikecount() > 0) {
//                vh0.tv_likecount.setText(home.getLikecount() + c.getResources().getString(R.string.like));
//            } else {
//                vh0.tv_likecount.setText(c.getResources().getString(R.string.like));
//            }
//
//            if (Constants.User_Email.equalsIgnoreCase(home.getUid())) {
//                vh0.iv_delete.setVisibility(View.VISIBLE);
//            } else {
//                vh0.iv_delete.setVisibility(View.GONE);
//            }
//
//            if (home.getLikestatus().equals("like")) {
//                vh0.iv_like.setImageResource(R.drawable.ic_bulb_filled);
//                vh0.tv_likecount.setTextColor(c.getResources().getColor(R.color.like_text_color));
//            } else {
//                vh0.iv_like.setImageResource(R.drawable.ic_bulb_lightup);
//                vh0.tv_likecount.setTextColor(c.getResources().getColor(R.color.text_lightgray));
//            }
//
//            if (dbh.get_savecontent_single(home.getId()) > 0)
//                vh0.iv_save.setImageResource(R.drawable.ic_bookmark_black_24dp);
//            else
//                vh0.iv_save.setImageResource(R.drawable.ic_bookmark_1_512);
//
//            vh0.lay_posttype.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    try {
//
//                        if (!clas.equalsIgnoreCase("UserProfile")) {
//                            Intent in = new Intent(c, UserProfile.class);
//                            in.putExtra("email", home.getUid());
//                            c.startActivity(in);
//                        }
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
//
//            vh0.like_lay.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View view) {
//
//                    if (!nw.isConnectingToInternet()) {
//                        Toast.makeText(c, "No Internet Connection", Toast.LENGTH_LONG).show();
//                        return;
//                    }
//
//                    volleylike_comment(p);
//
//                    int rew = 0, l = home.getLikecount();
//
//                    try {
//                        jsonobj = new JSONObject(home.getJsonfield());
//                        rew = Integer.valueOf(jsonobj.getString("userreward"));
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                    if (home.getLikestatus().equals("like")) {
//
//                        rew--;
//
//                        if (l > 0)
//                            l--;
//                        if (l > 0) {
//                            vh0.tv_likecount.setText(l + c.getResources().getString(R.string.like));
//                        } else {
//                            vh0.tv_likecount.setText(c.getResources().getString(R.string.like));
//                        }
//                        vh0.iv_like.setImageResource(R.drawable.ic_bulb_lightup);
//                        vh0.tv_likecount.setTextColor(c.getResources().getColor(R.color.text_lightgray));
//
//                    } else {
//
//                        l++;
//                        rew++;
//                        vh0.tv_likecount.setText(l + c.getResources().getString(R.string.like));
//
//                        vh0.iv_like.setImageResource(R.drawable.ic_bulb_filled);
//                        vh0.tv_likecount.setTextColor(c.getResources().getColor(R.color.like_text_color));
//                    }
//
//                }
//            });
//
//            vh0.iv_delete.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    deletcomment(p);
//                }
//            });
//
//            vh0.replylay.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Intent in = new Intent(c, Replies.class);
//
//                    try {
//
//                        jsonobj = new JSONObject(home.getJsonfield());
//                        in.putExtra("psid", home.getId());
//                        c.startActivity(in);
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        //Logg("json_exception","e",e);
//                    }
//                }
//            });
//
//            vh0.tv_comm.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Intent in = new Intent(c, Replies.class);
//
//                    try {
//
//                        jsonobj = new JSONObject(home.getJsonfield());
//                        in.putExtra("psid", home.getId());
//                        c.startActivity(in);
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        //Logg("json_exception","e",e);
//                    }
//                }
//            });
//
//            vh0.tv_groupname.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    try {
//                        if (Constants.Current_Activity == 100) {
//                            jsonobj = new JSONObject(home.getJsonfield());
//
//                            Intent disitem = new Intent(c, Group_Profile.class);
//                            disitem.putExtra("groupname", jsonobj.getString("groupname"));
//                            disitem.putExtra("pid", home.getGroupid());
////                        disitem.putExtra("totalcom", home.getCommentcount());
//                            c.startActivity(disitem);
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
////                    //Logg("groupname ", d1.getText().toString() + " postid " + dtopics.get(an).getId() + "");
//
//                }
//            });
//
//            // leave == share
//            vh0.leave.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    //alert(home.getGroupid());
//
//                    try {
//                        JSONObject shareobj = new JSONObject();
//
//                        jsonobj = new JSONObject(home.getJsonfield());
//
//                        shareobj.put("class", "Replies");
//                        shareobj.put("title", jsonobj.getString("name"));
//                        shareobj.put("description", jsonobj.getString("comments"));
//                        shareobj.put("image", Constants.URL + jsonobj.getString("com_image"));
//                        shareobj.put("id", home.getId());
//
//                        Utility.shareIntent(c, shareobj);
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
//
//            if (!jsonobj.getString("userimage").equalsIgnoreCase("null")) {
////                ImageLoader imageLoader = ImageLoader.getInstance();
////                imageLoader.init(ImageLoaderConfiguration.createDefault(c));isRunning=true;
//
////                loadImage(vh0.userimg,jsonobj.getString("userimage"));
//
//                imageLoader.getInstance().displayImage(jsonobj.getString("userimage"), vh0.userimg, optionsuser, animateFirstListener);
//
//                Logg("userimage1", jsonobj.getString("userimage") + "");
//
//            }
//            if (!jsonobj.getString("com_image").equalsIgnoreCase("null")) {
//
//                //Logg("comment_image",jsonobj.getString("com_image"));
////                ImageLoader imageLoader = ImageLoader.getInstance();
////                imageLoader.init(ImageLoaderConfiguration.createDefault(c));
////                loadImage(vh0.cimage,Constants.URL_Image + jsonobj.getString("com_image"));
//                imageLoader.getInstance().displayImage(Constants.URL_Image + jsonobj.getString("com_image"), vh0.cimage, options, animateFirstListener);
//                vh0.cimage.setVisibility(View.VISIBLE);
//            } else
//                vh0.cimage.setVisibility(View.GONE);
//
//
//            vh0.cimage.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (vh0.cimage.getDrawable() == null) {
//                    } else {
//
//                        try {
//                            jsonobj = new JSONObject(home.getJsonfield());
//                            Utility.Zoom_Image(c, Constants.URL_Image + jsonobj.getString("com_image"));
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
////                        imagedialog(vh0.cimage);
//                    }
//                }
//            });
//
//            vh0.iv_save.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    //Logg("field_type_",home.getFieldtype()+"");
//                    if (dbh.get_savecontent_single(home.getId()) < 1)
//                        savecontent(home, vh0.iv_save);
//                    else {
//                        //already saved this item
//                    }
//                }
//            });
//
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//    }

    private void setPost(final ViewHolder1 vh1, final int p) {

        final Home_getset home = (Home_getset) data.get(p);

        vh1.commentcount.setText(home.getCommentcount() + " Comments");
        if (home.getLikecount() > 0) {
            vh1.likecount.setText(home.getLikecount() + c.getResources().getString(R.string.like));
        } else {
            vh1.likecount.setText(c.getResources().getString(R.string.like));
        }

        vh1.viewcount.setText(home.getViewcount() + " Views");
        vh1.newstype.setText(home.getPosttype());

        vh1.date.setText(getDate(home.getTimestamp()));

        vh1.tv_lightuptext.setText("");
        // vh1.newscomment.setText("Comments");

        //Logg("like_post_position",p+" "+home.getLikestatus());

        if (home.getLikestatus().equals("like")) {
            vh1.likeimage.setImageResource(R.drawable.ic_bulb_filled);
            vh1.likecount.setTextColor(c.getResources().getColor(R.color.like_text_color));
        } else {
            vh1.likeimage.setImageResource(R.drawable.ic_bulb_lightup);
            vh1.likecount.setTextColor(c.getResources().getColor(R.color.text_lightgray));
        }

        if (dbh.get_savecontent_single(home.getId()) > 0)
            vh1.iv_save.setImageResource(R.drawable.ic_bookmark_black_24dp);
        else
            vh1.iv_save.setImageResource(R.drawable.ic_bookmark_1_512);

        try {

            // Typeface font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");
            vh1.title.setTypeface(font_demi);
            vh1.newstype.setTypeface(font_demi);
            // Typeface font_meduim = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");
            vh1.commentcount.setTypeface(font_medium);
            vh1.likecount.setTypeface(font_medium);
            vh1.viewcount.setTypeface(font_medium);
            vh1.descr.setTypeface(font_medium);
            vh1.date.setTypeface(font_medium);
            vh1.postby.setTypeface(font_medium);
            vh1.tv_price.setTypeface(font_medium);
            vh1.tv_lightuptext.setTypeface(font_medium);
            vh1.tv_share.setTypeface(font_medium);

            jsonobj = new JSONObject(home.getJsonfield());

            vh1.title.setText(jsonobj.getString("title"));
            vh1.descr.setText(Html.fromHtml(home.getPostdescription() + ""));
            vh1.postby.setText(jsonobj.getString("writer"));

            if (jsonobj.has("add_comment")) {
                if (jsonobj.getString("add_comment").equalsIgnoreCase("off")) {
                    vh1.commentlay.setVisibility(View.GONE);

                    vh1.lay_share.setGravity(Gravity.CENTER);
                    vh1.lay_like.setGravity(Gravity.CENTER);
                } else {
                    vh1.commentlay.setVisibility(View.VISIBLE);
                    vh1.lay_like.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                    vh1.lay_share.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                }

            } else {
                vh1.commentlay.setVisibility(View.VISIBLE);
                vh1.lay_like.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                vh1.lay_share.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
            }


            if (jsonobj.getBoolean("user_payment")) {
                vh1.tv_price.setVisibility(View.GONE);
            } else {
                if (jsonobj.getString("payment").equalsIgnoreCase("free")) {
                    /*vh1.tv_price.setText(jsonobj.getString("payment"));*/
                    StringBuilder sb = new StringBuilder(jsonobj.getString("payment"));
                    sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
                    vh1.tv_price.setText("FREE");
                    //vh1.tv_price.setText(sb.toString() + "");
                } else {
                    vh1.tv_price.setText("Rs. " + jsonobj.getString("payment"));
                }
            }

            if (jsonobj.getString("thumbnail").length() > 1) {
//                ImageLoader imageLoader = ImageLoader.getInstance();
//                imageLoader.init(ImageLoaderConfiguration.createDefault(c));

//                loadImage(vh1.thumnainls,Constants.URL_Image + "newsimage/" + jsonobj.getString("thumbnail"));

                imageLoader.getInstance().displayImage(Constants.URL_Image + "newsimage/" + jsonobj.getString("thumbnail"), vh1.thumnainls, options, animateFirstListener);
            }

            if (jsonobj.getString("pic").length() > 1) {
                if (jsonobj.getString("pic").equalsIgnoreCase("default_pic.png"))
                    vh1.newsimage.setVisibility(View.GONE);
                else
                    imageLoader.getInstance().displayImage(Constants.URL_Image + "newsimage/" + jsonobj.getString("pic"), vh1.newsimage, options, animateFirstListener);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            //Logg("json exception news","e",e);

        }
        vh1.lay_posttype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!clas.equalsIgnoreCase("Topic_Search_Data")) {
                    Intent in = new Intent(c, Topic_Search_Data.class);
                    in.putExtra("topic", home.getPosttype());
                    in.putExtra("fieldtype", "1");
                    c.startActivity(in);
                }
            }
        });

        vh1.lay_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!nw.isConnectingToInternet()) {
                    Toast.makeText(c, "No Internet Connection", Toast.LENGTH_LONG).show();
                    return;
                }

                volleylike_post(p);
                int l = home.getLikecount();
                if (home.getLikestatus().equals("like")) {

//                    int l= home.getLikecount();
                    if (l > 0)
                        l--;

//                    vh1.likecount.setText(l+c.getResources().getString(R.string.like));
                    if (l > 0) {
                        vh1.likecount.setText(l + c.getResources().getString(R.string.like));
                    } else {
                        vh1.likecount.setText(c.getResources().getString(R.string.like));
                    }

                    vh1.likeimage.setImageResource(R.drawable.ic_bulb_lightup);
                    vh1.likecount.setTextColor(c.getResources().getColor(R.color.text_lightgray));
                } else {

                    l++;
                    vh1.likecount.setText(l + c.getResources().getString(R.string.like));
                    vh1.likeimage.setImageResource(R.drawable.ic_bulb_filled);
                    vh1.likecount.setTextColor(c.getResources().getColor(R.color.like_text_color));
                }

            }
        });

        vh1.newslayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    jsonobj = new JSONObject(home.getJsonfield());
                    Utility.data = home;
                    if (Utility.checkPayment(c, jsonobj)) {
                        Utility.openPost(c);
                    } else {
                        Utility.payPayment_Post(c);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        vh1.commentlay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Intent in = new Intent(c, Comment_Common_homepage.class);
                in.putExtra("posttype", "1");
                in.putExtra("postid", home.getId());
                c.startActivity(in);
            }
        });

        vh1.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    jsonobj = new JSONObject(home.getJsonfield());
                    Utility.data = home;
                    if (Utility.checkPayment(c, jsonobj)) {
//                        Utility.data=home;
                        Utility.openPost(c);
                    } else {

//                        Utility.data=home;
                        Utility.payPayment_Post(c);
                        // //Logg("payment_response",b+"  ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    //Logg("exception","e",e);
                }
                //Logg("click_click","click");
            }
        });

        vh1.lay_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                JSONObject shareobj = new JSONObject();

                String value = "";
                String[] valuearr;

                try {
                    jsonobj = new JSONObject(home.getJsonfield());
                    value = String.valueOf(Html.fromHtml(Html.fromHtml(home.getPostdescription() + "").toString()));
                    valuearr = value.split(System.lineSeparator(), 2);
                    value = valuearr[0];


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (home.getPosturl().toString().contains("sandeepchaudhary.in") | data.get(p).getPosturl().contains("hellotopper.com")) {

                    try {
                        shareobj.put("class", "Webview_internal_link");
                        shareobj.put("title", jsonobj.getString("title"));
                        shareobj.put("description", " ");
                        shareobj.put("image", Constants.URL + "newsimage/" + jsonobj.getString("pic"));
                        shareobj.put("id", home.getId());
//                        shareobj.put("posturl",home.getPosturl());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//                    in.putExtra("id", data.getId());
//                    in.putExtra("posturl", data.getPosturl());
                } else if (home.getPosturl().toString().equalsIgnoreCase("news")) {
                    try {

                        shareobj.put("class", "Webview_plus_html");
                        shareobj.put("title", jsonobj.getString("title"));
                        shareobj.put("description", " ");
                        shareobj.put("image", Constants.URL + "newsimage/" + jsonobj.getString("pic"));
                        shareobj.put("id", home.getId());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                    try {

                        shareobj.put("class", "News_Webview");
                        shareobj.put("title", jsonobj.getString("title"));
                        shareobj.put("description", " ");
                        shareobj.put("image", Constants.URL + "newsimage/" + jsonobj.getString("pic"));
                        shareobj.put("id", home.getId());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                Utility.shareIntent(c, shareobj);

            }
        });

        vh1.iv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Logg("field_type_",home.getFieldtype()+"");
                if (dbh.get_savecontent_single(home.getId()) < 1)
                    savecontent(home, vh1.iv_save);
            }
        });

    }

    private void setOnlinetest(final ViewHolder2 vh2, final int p) {

        final Home_getset home = (Home_getset) data.get(p);

        try {


            jsonobj = new JSONObject(home.getJsonfield());

            if (jsonobj.has("add_comment")) {
                if (jsonobj.getString("add_comment").equalsIgnoreCase("off")) {
                    vh2.commentlay.setVisibility(View.GONE);

                    vh2.lay_share.setGravity(Gravity.CENTER);
                    vh2.lay_like.setGravity(Gravity.CENTER);
                } else {
                    vh2.commentlay.setVisibility(View.VISIBLE);
                    vh2.lay_like.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                    vh2.lay_share.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                }

            } else {

                vh2.commentlay.setVisibility(View.VISIBLE);
                vh2.lay_like.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                vh2.lay_share.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
            }

            vh2.tv_examname.setText(jsonobj.getString("testname"));
            vh2.tv_time.setText(jsonobj.getString("time") + " Minutes");
            vh2.tv_question.setText(jsonobj.getString("question") + " Questions");
            vh2.tv_postby.setText(jsonobj.getString("writer"));
            // vh2.tv_likecount.setText(home.getLikecount()+c.getResources().getString(R.string.like));
            vh2.tv_date.setText(ConvertDate(home.getTimestamp()));

            if (home.getLikecount() > 0) {
                vh2.tv_likecount.setText(home.getLikecount() + c.getResources().getString(R.string.like));
            } else {
                vh2.tv_likecount.setText(c.getResources().getString(R.string.like));
            }
            vh2.tv_commentcount.setText(home.getCommentcount() + " Comments");
            vh2.tv_applicant.setText(home.getViewcount() + "");
            vh2.text.setText(home.getPosttype());

            // Typeface font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");

            // Typeface font_medium = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");
            vh2.share_text.setTypeface(font_medium);
            vh2.tv_applicant.setTypeface(font_demi);
            vh2.tv_postby.setTypeface(font_medium);
            vh2.tv_likecount.setTypeface(font_medium);
            vh2.tv_commentcount.setTypeface(font_medium);
            vh2.tv_start.setTypeface(font_demi);
            vh2.tv_price.setTypeface(font_medium);
            vh2.text.setTypeface(font_demi);
            vh2.text1.setTypeface(font_demi);
            vh2.tv_examname.setTypeface(font_demi);
            vh2.tv_time.setTypeface(font_medium);
            vh2.tv_question.setTypeface(font_medium);

            if (Integer.parseInt(jsonobj.getString("rank")) > 0) {
                vh2.tv_start.setText("Rank " + jsonobj.getString("rank") + "/" + jsonobj.getString("candidate"));
            } else if (Utility.checkdownload(home.getId() + "")) {
                vh2.tv_start.setText("START NOW");
            }


            if (jsonobj.getBoolean("user_payment")) {
                if (Integer.parseInt(jsonobj.getString("rank")) > 0) {
                    vh2.tv_start.setText("Rank " + jsonobj.getString("rank") + "/" + jsonobj.getString("candidate"));
                } else if (Utility.checkdownload(home.getId() + "")) {
                    vh2.tv_start.setText("START NOW");
                }
            } else {
                if (jsonobj.getString("payment").equalsIgnoreCase("free"))
                    vh2.tv_price.setText(jsonobj.getString("payment"));
                else {
                    vh2.tv_price.setText("Rs. " + jsonobj.getString("payment"));
                }
            }

            if (home.getLikestatus().equals("like")) {
                vh2.likeimage.setImageResource(R.drawable.ic_bulb_filled);
                vh2.tv_likecount.setTextColor(c.getResources().getColor(R.color.like_text_color));
            } else {
                vh2.likeimage.setImageResource(R.drawable.ic_bulb_lightup);
                vh2.tv_likecount.setTextColor(c.getResources().getColor(R.color.text_lightgray));
            }

        } catch (JSONException e) {
            e.printStackTrace();
            //Logg("json exception test","e",e);
        }

        vh2.lay_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Logg("onlinetest like","onlinetest like click");

                if (!nw.isConnectingToInternet()) {
                    Toast.makeText(c, "No Internet Connection", Toast.LENGTH_LONG).show();
                    return;
                }

                volleylike_post(p);
                int l = home.getLikecount();
                if (home.getLikestatus().equals("like")) {

//                    int l= home.getLikecount();
                    if (l > 0)
                        l--;

                    //   vh2.tv_likecount.setText(l+c.getResources().getString(R.string.like));
                    if (home.getLikecount() > 0) {
                        vh2.tv_likecount.setText(l + c.getResources().getString(R.string.like));
                    } else {
                        vh2.tv_likecount.setText(c.getResources().getString(R.string.like));
                    }
                    vh2.likeimage.setImageResource(R.drawable.ic_bulb_lightup);
                    vh2.tv_likecount.setTextColor(c.getResources().getColor(R.color.text_lightgray));
                } else {
//                    int l= home.getLikecount();
                    l++;
                    vh2.tv_likecount.setText(l + c.getResources().getString(R.string.like));

                    vh2.likeimage.setImageResource(R.drawable.ic_bulb_filled);
                    vh2.tv_likecount.setTextColor(c.getResources().getColor(R.color.like_text_color));
                }

            }
        });

        vh2.lay_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!clas.equalsIgnoreCase("Topic_Search_Data")) {
                    Intent in = new Intent(c, Topic_Search_Data.class);
                    in.putExtra("topic", home.getPosttype());
                    in.putExtra("fieldtype", "2");
                    c.startActivity(in);
                }
            }
        });

        vh2.layout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {

                    String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};

                    if (!hasPermissions(c, PERMISSIONS)) {

                        ActivityCompat.requestPermissions((Activity) c, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                MY_PERMISSIONS_REQUEST);

                    } else {

                        JSONObject jsonobj1 = new JSONObject(home.getJsonfield());
                        if (jsonobj1.has("appversion")) {

                            homeVersion = jsonobj1.getInt("appversion");

                            PackageInfo pInfo = c.getPackageManager().getPackageInfo(c.getPackageName(), 0);
                            int version = pInfo.versionCode;


                            if (homeVersion == 0) {
                                check_key(c, home, "homefragment");
                            } else {
                                if (homeVersion > version) {
                                    Toast.makeText(c, "Need to update app first", Toast.LENGTH_SHORT).show();

                                } else {
                                    check_key(c, home, "homefragment");

                                }
                            }
                        } else {
                            check_key(c, home, "homefragment");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        vh2.commentlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(c, Comment_Common_homepage.class);
                in.putExtra("posttype", "2");
                in.putExtra("postid", home.getId());
                c.startActivity(in);

            }
        });

        vh2.lay_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                JSONObject shareobj = new JSONObject();
                String des = "";

                try {
                    jsonobj = new JSONObject(home.getJsonfield());

                    if (Integer.parseInt(jsonobj.getString("rank")) > 0) {
                        des = "I have secured " + jsonobj.getString("rank") + "/" + jsonobj.getString("candidate") + " rank in this Quiz & challenge you to beat my score. Attempt this QUIZ and send me screenshot of your result.";
                    } else {
                        des = "Hi, check this new Quiz uploaded on Gyan Strot App. Attempt to self assess your understanding on the topic. If you like the quiz, press the like button and you may share it with your friends using the share button.";
//                        des="I am challenging you to attempt this QUIZ & beat my score. Let’s we both attempt this & share our results by screenshots.";
                    }

                    shareobj.put("class", "Online_Test");
                    shareobj.put("title", jsonobj.getString("testname"));
                    shareobj.put("description", des);
                    shareobj.put("image", "");
                    shareobj.put("id", home.getId());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Utility.shareIntent(c, shareobj);


            }
        });

    }

    private void setJobs(final ViewHolder3 vh3, final int p) {

        final Home_getset home = (Home_getset) data.get(p);

        if (home.getLikestatus().equals("like")) {
            vh3.likeimage.setImageResource(R.drawable.ic_bulb_filled);
            vh3.tv_likecount.setTextColor(c.getResources().getColor(R.color.like_text_color));
        } else {
            vh3.likeimage.setImageResource(R.drawable.ic_bulb_lightup);
            vh3.tv_likecount.setTextColor(c.getResources().getColor(R.color.text_lightgray));
        }

        if (dbh.get_savecontent_single(home.getId()) > 0)
            vh3.iv_save.setImageResource(R.drawable.ic_bookmark_black_24dp);
        else
            vh3.iv_save.setImageResource(R.drawable.ic_bookmark_1_512);

        try {
            // Typeface font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");
            vh3.tv_title.setTypeface(font_demi);
            vh3.tv_jobheadertitle.setTypeface(font_demi);
            //Typeface font_meduim = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");
            vh3.tv_lastdate.setTypeface(font_medium);
            vh3.tv_commentcount.setTypeface(font_medium);
            vh3.tv_likecount.setTypeface(font_medium);
            vh3.tv_post.setTypeface(font_medium);
            vh3.tv_postname.setTypeface(font_medium);
            vh3.tv_share.setTypeface(font_medium);
            vh3.tv_lightuptext.setTypeface(font_medium);
            vh3.tv_lightuptext.setText("");

            jsonobj = new JSONObject(home.getJsonfield());


            if (jsonobj.has("add_comment")) {
                if (jsonobj.getString("add_comment").equalsIgnoreCase("off")) {
                    vh3.commentlay.setVisibility(View.GONE);

                    vh3.lay_share.setGravity(Gravity.CENTER);
                    vh3.lay_like.setGravity(Gravity.CENTER);
                } else {
                    vh3.commentlay.setVisibility(View.VISIBLE);
                    vh3.lay_like.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                    vh3.lay_share.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                }

            } else {
                vh3.commentlay.setVisibility(View.VISIBLE);
                vh3.lay_like.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                vh3.lay_share.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
            }

            vh3.tv_title.setText(jsonobj.getString("title"));
            vh3.tv_postname.setText(jsonobj.getString("postname"));
            vh3.tv_post.setText(jsonobj.getString("post") + " Vacancies");
            // vh3.tv_likecount.setText(home.getLikecount()+c.getResources().getString(R.string.like));
            if (home.getLikecount() > 0) {
                vh3.tv_likecount.setText(home.getLikecount() + c.getResources().getString(R.string.like));
            } else {
                vh3.tv_likecount.setText(c.getResources().getString(R.string.like));
            }
            vh3.tv_commentcount.setText(home.getCommentcount() + " Comments");
            //vh3.tv_viewcount.setText(home.getViewcount()+" Views");
            vh3.tv_lastdate.setText("Last Date " + jsonobj.getString("lastdate"));

        } catch (JSONException e) {
            e.printStackTrace();
            //Logg("json exception test","e",e);
        }

//        ImageLoader imageLoader = ImageLoader.getInstance();

        try {
            if (!jsonobj.getString("image").equalsIgnoreCase("null")) {
//                imageLoader.init(ImageLoaderConfiguration.createDefault(c));

//                loadImage( vh3.iv_jobimage,Constants.URL_Image+"jobsimage/"+jsonobj.getString("image"));
                imageLoader.getInstance().displayImage(Constants.URL_Image + "jobsimage/" + jsonobj.getString("image"), vh3.iv_jobimage, options, animateFirstListener);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        vh3.lay_posttype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!clas.equalsIgnoreCase("Jobs_all")) {
                    Intent i = new Intent(c, Jobs_all.class);
                    //i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    c.startActivity(i);
                } else {
                    Utility.jobDetails(c, data.get(p));
                }
            }
        });

        vh3.lay_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Logg("job like","job like click");

                if (!nw.isConnectingToInternet()) {
                    Toast.makeText(c, "No Internet Connection", Toast.LENGTH_LONG).show();
                    return;
                }

                volleylike_post(p);
                int l = home.getLikecount();

                if (home.getLikestatus().equals("like")) {

//                    int l= home.getLikecount();
                    if (l > 0)
                        l--;

                    //   vh3.tv_likecount.setText(l+c.getResources().getString(R.string.like));
                    if (home.getLikecount() > 0) {
                        vh3.tv_likecount.setText(l + c.getResources().getString(R.string.like));
                    } else {
                        vh3.tv_likecount.setText(c.getResources().getString(R.string.like));
                    }
                    vh3.likeimage.setImageResource(R.drawable.ic_bulb_lightup);
                    vh3.tv_likecount.setTextColor(c.getResources().getColor(R.color.text_lightgray));
                } else {
//                    int l= home.getLikecount();
                    l++;
                    vh3.tv_likecount.setText(l + c.getResources().getString(R.string.like));

                    vh3.likeimage.setImageResource(R.drawable.ic_bulb_filled);
                    vh3.tv_likecount.setTextColor(c.getResources().getColor(R.color.like_text_color));
                }

            }
        });

        vh3.lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Logg("home.get",home.getFieldtype()+"");
                Utility.jobDetails(c, data.get(p));

            }
        });

        vh3.lay_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject shareobj = new JSONObject();

                try {
                    jsonobj = new JSONObject(home.getJsonfield());

                    shareobj.put("class", "Job_details_via_link");
                    shareobj.put("title", jsonobj.getString("title"));
                    shareobj.put("description", jsonobj.getString("postname"));
                    shareobj.put("image", Constants.URL + "jobsimage/" + jsonobj.getString("image"));
                    shareobj.put("id", home.getId());
//

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Utility.shareIntent(c, shareobj);

            }
        });

//        vh3.iv_savecont.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                //Logg("field_type_",home.getFieldtype()+"");
//                //savecontent(home);
//            }
//        });

        vh3.commentlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(c, Comment_Common_homepage.class);
                in.putExtra("posttype", "3");
                in.putExtra("postid", home.getId());
                c.startActivity(in);

            }
        });

        vh3.iv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Logg("field_type_",home.getFieldtype()+"");
                if (dbh.get_savecontent_single(home.getId()) < 1)
                    savecontent(home, vh3.iv_save);
            }
        });

    }

    private void setPdf(final ViewHolder4 vh4, final int p) {
        setTypefaceforPDF(vh4);

        final Home_getset home = (Home_getset) data.get(p);

        if (home.getLikestatus().equals("like")) {
            vh4.likeimage.setImageResource(R.drawable.ic_bulb_filled);
            vh4.tv_likecount.setTextColor(c.getResources().getColor(R.color.like_text_color));
        } else {
            vh4.likeimage.setImageResource(R.drawable.ic_bulb_lightup);
            vh4.tv_likecount.setTextColor(c.getResources().getColor(R.color.text_lightgray));
        }
        if (dbh.get_savecontent_single(home.getId()) > 0)
            vh4.iv_save.setImageResource(R.drawable.ic_bookmark_black_24dp);
        else
            vh4.iv_save.setImageResource(R.drawable.ic_bookmark_1_512);


        try {
            // Typeface font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");
            //Typeface font_medium = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");
            vh4.tv_title.setTypeface(font_demi);
            vh4.tv_postedby.setTypeface(font_medium);
            vh4.tv_likecount.setTypeface(font_medium);
            vh4.tv_commentcount.setTypeface(font_medium);
            vh4.tv_download.setTypeface(font_demi);
            vh4.tv_view.setTypeface(font_demi);
            vh4.tv_text.setTypeface(font_demi);
            vh4.tv_date.setTypeface(font_medium);
            vh4.tv_share.setTypeface(font_medium);
            vh4.tv_comment.setTypeface(font_medium);
            vh4.tv_like.setTypeface(font_medium);

            jsonobj = new JSONObject(home.getJsonfield());

            vh4.tv_date.setText(Utility.ConvertDate(data.get(p).getTimestamp()));

            if (jsonobj.has("add_comment")) {
                if (jsonobj.getString("add_comment").equalsIgnoreCase("off")) {
                    vh4.commentlay.setVisibility(View.GONE);

                    vh4.lay_share.setGravity(Gravity.CENTER);
                    vh4.lay_like.setGravity(Gravity.CENTER);
                } else {
                    vh4.commentlay.setVisibility(View.VISIBLE);
                    vh4.lay_like.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                    vh4.lay_share.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                }

            } else {
                vh4.commentlay.setVisibility(View.VISIBLE);
                vh4.lay_like.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                vh4.lay_share.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
            }

            vh4.tv_title.setText(jsonobj.getString("title"));
            vh4.tv_postedby.setText(jsonobj.getString("writer"));

            // vh4.tv_likecount.setText(home.getLikecount()+c.getResources().getString(R.string.like));
            if (home.getLikecount() > 0) {
                vh4.tv_likecount.setText(home.getLikecount() + c.getResources().getString(R.string.like));
            } else {
                vh4.tv_likecount.setText(c.getResources().getString(R.string.like));
            }
            vh4.tv_commentcount.setText(home.getCommentcount() + " Comments");
            // vh4.tv_viewcount.setText(home.getViewcount()+" Views");

        } catch (JSONException e) {
            e.printStackTrace();
            //Logg("json exception test","e",e);
        }

        vh4.lay_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Logg("pdf like","pdf like click");

                if (!nw.isConnectingToInternet()) {
                    Toast.makeText(c, "No Internet Connection", Toast.LENGTH_LONG).show();
                    return;
                }

                volleylike_post(p);
                int l = home.getLikecount();

                if (home.getLikestatus().equals("like")) {

//                    int l= home.getLikecount();
                    if (l > 0)
                        l--;

                    // vh4.tv_likecount.setText(l+c.getResources().getString(R.string.like));
                    if (l > 0) {
                        vh4.tv_likecount.setText(l + c.getResources().getString(R.string.like));
                    } else {
                        vh4.tv_likecount.setText(c.getResources().getString(R.string.like));
                    }
                    vh4.likeimage.setImageResource(R.drawable.ic_bulb_lightup);
                    vh4.tv_likecount.setTextColor(c.getResources().getColor(R.color.text_lightgray));
                } else {
//                    int l= home.getLikecount();
                    l++;
                    vh4.tv_likecount.setText(l + c.getResources().getString(R.string.like));

                    vh4.likeimage.setImageResource(R.drawable.ic_bulb_filled);
                    vh4.tv_likecount.setTextColor(c.getResources().getColor(R.color.like_text_color));
                }

            }
        });

        vh4.tv_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JSONObject json = null;
                try {

                    json = new JSONObject(home.getJsonfield());
//                    downloadpdf(json.getString("link"));

                    ////Logg("external_link",pdflink);
                    Intent in = new Intent(c, Pdf_view_Webview.class);
                    in.putExtra("postid", home.getId());
//                    in.putExtra("comment", home.getCommentcount());
//                    in.putExtra("like", home.getLikecount());
//                    in.putExtra("likestatus", home.getLikestatus());
//                    in.putExtra("newsurl",json.getString("link"));

                    c.startActivity(in);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        vh4.lay_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject json = null;
                try {

                    String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};

                    if (!hasPermissions(c, PERMISSIONS)) {
                        ActivityCompat.requestPermissions((Activity) c, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                MY_PERMISSIONS_REQUEST);

                    } else {

                        home_obj = home;
                        json = new JSONObject(home.getJsonfield());
                        downloadpdf(json.getString("link"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

//                pdf_confirmationdialog(p);

            }
        });


        vh4.commentlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(c, Comment_Common_homepage.class);
                in.putExtra("posttype", "4");
                in.putExtra("postid", home.getId());
                c.startActivity(in);

            }
        });

        vh4.lay_share.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                JSONObject shareobj = new JSONObject();

                try {
                    jsonobj = new JSONObject(home.getJsonfield());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    shareobj.put("class", "Pdf_view_Webview");
                    shareobj.put("title", jsonobj.getString("title"));
                    shareobj.put("description", "I find this file very important for you. You can download or view it in Gyan Strot app.");
                    shareobj.put("image", "");
                    shareobj.put("id", home.getId());
//                    shareobj.put("comment", home.getCommentcount());
//                    shareobj.put("like", home.getLikecount());
//                    shareobj.put("likestatus", "dislike");
//                    shareobj.put("newsurl",jsonobj.getString("link"));
//                        shareobj.put("posturl",home.getPosturl());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Utility.shareIntent(c, shareobj);

            }
        });

        vh4.iv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dbh.get_savecontent_single(home.getId()) < 1)
                    savecontent(home, vh4.iv_save);
                else {
                    //already saved this item
                }
            }
        });

    }

    private void setWorkshop(final ViewHolder5 vh5, final int p) {

        final Home_getset home = (Home_getset) data.get(p);

        if (home.getLikestatus().equals("like")) {
            vh5.likeimage.setImageResource(R.drawable.ic_bulb_filled);
            vh5.tv_likecount.setTextColor(c.getResources().getColor(R.color.like_text_color));
        } else {
            vh5.likeimage.setImageResource(R.drawable.ic_bulb_lightup);
            vh5.tv_likecount.setTextColor(c.getResources().getColor(R.color.text_lightgray));
        }

        try {
            //Typeface font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");
            //Typeface font_medium = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");

            vh5.tv_share.setTypeface(font_medium);
            vh5.tv_comment.setTypeface(font_medium);
            vh5.tv_like.setTypeface(font_medium);
            vh5.tv_title.setTypeface(font_demi);
            vh5.tv_description.setTypeface(font_medium);
            vh5.tv_date.setTypeface(font_medium);
            vh5.tv_location.setTypeface(font_medium);
            vh5.tv_viewcount.setTypeface(font_medium);
            vh5.tv_commentcount.setTypeface(font_medium);
            vh5.tv_likecount.setTypeface(font_medium);
            vh5.tv_explore.setTypeface(font_medium);
            vh5.tv_register.setTypeface(font_demi);

            jsonobj = new JSONObject(home.getJsonfield());


            if (jsonobj.has("add_comment")) {
                if (jsonobj.getString("add_comment").equalsIgnoreCase("off")) {
                    vh5.commentlay.setVisibility(View.GONE);

                    vh5.lay_share.setGravity(Gravity.CENTER);
                    vh5.lay_like.setGravity(Gravity.CENTER);
                } else {
                    vh5.commentlay.setVisibility(View.VISIBLE);
                    vh5.lay_like.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                    vh5.lay_share.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                }

            } else {
                vh5.commentlay.setVisibility(View.VISIBLE);
                vh5.lay_like.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                vh5.lay_share.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
            }

            vh5.tv_title.setText(jsonobj.getString("title"));
            vh5.tv_date.setText(jsonobj.getString("date"));
            vh5.tv_description.setText(Html.fromHtml(home.getPostdescription() + ""));
            vh5.tv_location.setText(jsonobj.getString("location"));
            //   vh5.tv_likecount.setText(home.getLikecount()+c.getResources().getString(R.string.like));
            if (home.getLikecount() > 0) {
                vh5.tv_likecount.setText(home.getLikecount() + c.getResources().getString(R.string.like));
            } else {
                vh5.tv_likecount.setText(c.getResources().getString(R.string.like));
            }
            vh5.tv_commentcount.setText(home.getCommentcount() + " Comments");
            vh5.tv_viewcount.setText(home.getViewcount() + " Views");

        } catch (JSONException e) {
            e.printStackTrace();
            //Logg("json exception test","e",e);
        }

//        ImageLoader imageLoader = ImageLoader.getInstance();
//        imageLoader.init(ImageLoaderConfiguration.createDefault(c));

        try {
//            loadImage(vh5.iv_workimage,Constants.URL_Image + "workshopimage/" + jsonobj.getString("image"));
            imageLoader.getInstance().displayImage(Constants.URL_Image + "workshopimage/" + jsonobj.getString("image"), vh5.iv_workimage, options, animateFirstListener);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        vh5.lay_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Logg("workshop like","workshop like click");

                if (!nw.isConnectingToInternet()) {
                    Toast.makeText(c, "No Internet Connection", Toast.LENGTH_LONG).show();
                    return;
                }

                volleylike_post(p);
                int l = home.getLikecount();

                if (home.getLikestatus().equals("like")) {

//                    int l= home.getLikecount();
                    if (l > 0)
                        l--;
                    if (l > 0) {
                        vh5.tv_likecount.setText(l + c.getResources().getString(R.string.like));
                    } else {
                        vh5.tv_likecount.setText(c.getResources().getString(R.string.like));
                    }
                    // vh5.tv_likecount.setText(l+c.getResources().getString(R.string.like));

                    vh5.likeimage.setImageResource(R.drawable.ic_bulb_lightup);
                    vh5.tv_likecount.setTextColor(c.getResources().getColor(R.color.text_lightgray));
                } else {
//                    int l= home.getLikecount();
                    l++;
                    vh5.tv_likecount.setText(l + c.getResources().getString(R.string.like));

                    vh5.likeimage.setImageResource(R.drawable.ic_bulb_filled);
                    vh5.tv_likecount.setTextColor(c.getResources().getColor(R.color.like_text_color));
                }

            }
        });

        vh5.lay_workshop.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                try {

                    jsonobj = new JSONObject(home.getJsonfield());

                    Intent in = new Intent(c, Workshopdetail.class);
                    in.putExtra("postid", home.getId());
                    c.startActivity(in);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        vh5.tv_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JSONObject jsonobj = null;
                try {
                    jsonobj = new JSONObject(home.getJsonfield());

                    Intent in = new Intent(c, Workshop_Register.class);
                    in.putExtra("amount", jsonobj.getString("fees"));
                    in.putExtra("postid", home.getId());
                    c.startActivity(in);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


        vh5.commentlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(c, Comment_Common_homepage.class);
                in.putExtra("posttype", "5");
                in.putExtra("postid", home.getId());
                c.startActivity(in);

            }
        });

        vh5.lay_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject shareobj = new JSONObject();
                String value = "";
                String[] valuearr;

                try {
                    jsonobj = new JSONObject(home.getJsonfield());
                    value = String.valueOf(Html.fromHtml(Html.fromHtml(jsonobj.getString("description")).toString()));
                    valuearr = value.split(System.lineSeparator(), 2);
                    value = valuearr[0];

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    shareobj.put("class", "Workshopdetail");
                    shareobj.put("title", jsonobj.getString("title"));
                    shareobj.put("description", value);
                    shareobj.put("image", Constants.URL + "workshopimage/" + jsonobj.getString("image"));
                    shareobj.put("id", home.getId());
//                        shareobj.put("posturl",home.getPosturl());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Utility.shareIntent(c, shareobj);

            }
        });

    }

//    private void setQuestion(final ViewHolder7 vh7, final int p) {
//
//        final Home_getset home = (Home_getset) data.get(p);
//
//        try {
//
//            // Typeface font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");
//            vh7.uname.setTypeface(font_demi);
//            vh7.question.setTypeface(font_demi);
//            vh7.groupname.setTypeface(font_demi);
//            vh7.opta.setTypeface(font_demi);
//            vh7.optb.setTypeface(font_demi);
//            vh7.optc.setTypeface(font_demi);
//            vh7.optd.setTypeface(font_demi);
//            vh7.attempt.setTypeface(font_demi);
//
//            //Typeface font_medium = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");
//            vh7.time.setTypeface(font_medium);
//            vh7.tv_likecount.setTypeface(font_medium);
//            vh7.tv_commentcount.setTypeface(font_medium);
//            vh7.coll.setTypeface(font_medium);
//            vh7.tv_reward.setTypeface(font_medium);
//            jsonobj = new JSONObject(home.getJsonfield());
//
//            int attempt_count = 0;
//
//            if (!jsonobj.getString("answerpoll").equalsIgnoreCase("null")) {
//                jsonpoll = new JSONObject(jsonobj.getString("answerpoll"));
//
//                //Logg("jsonpoll_count", jsonpoll + "");
//                //Logg("jsonpoll_count", jsonpoll + "");
//
//                if (jsonpoll.has("a"))
//                    attempt_count = attempt_count + Integer.parseInt(jsonpoll.getString("a"));
//                if (jsonpoll.has("b"))
//                    attempt_count = attempt_count + Integer.parseInt(jsonpoll.getString("b"));
//                if (jsonpoll.has("c"))
//                    attempt_count = attempt_count + Integer.parseInt(jsonpoll.getString("c"));
//                if (jsonpoll.has("d"))
//                    attempt_count = attempt_count + Integer.parseInt(jsonpoll.getString("d"));
//            }
//            vh7.attempt.setText("Attempts " + attempt_count);
//
//
//            if (home.getLikestatus().equals("like")) {
//                vh7.likeimage.setImageResource(R.drawable.ic_bulb_filled);
//                vh7.tv_likecount.setTextColor(c.getResources().getColor(R.color.like_text_color));
//            } else {
//                vh7.likeimage.setImageResource(R.drawable.ic_bulb_lightup);
//                vh7.tv_likecount.setTextColor(c.getResources().getColor(R.color.text_lightgray));
//            }
//
//            vh7.tv_reward.setText(jsonobj.getString("userreward") + " Reward");
//
//            // vh7.tv_likecount.setText(home.getLikecount() +c.getResources().getString(R.string.like));
//            if (home.getLikecount() > 0) {
//                vh7.tv_likecount.setText(home.getLikecount() + c.getResources().getString(R.string.like));
//            } else {
//                vh7.tv_likecount.setText(c.getResources().getString(R.string.like));
//            }
//
//
//            vh7.tv_commentcount.setText(home.getCommentcount() + " Comments");
//
//            rightans = jsonobj.getString("ans");
//
//            vh7.groupname.setText(jsonobj.getString("groupname"));
//            vh7.coll.setText(jsonobj.getString("collage"));
//
//            vh7.uname.setText(jsonobj.getString("username"));
//
//            vh7.time.setText(getDate(home.getTimestamp()));
//
//            vh7.question.setText(Html.fromHtml(jsonobj.getString("question")));
//            vh7.opta.setText(Html.fromHtml(jsonobj.getString("a")));
//            vh7.optb.setText(Html.fromHtml(jsonobj.getString("b")));
//
//            if (!jsonobj.getString("c").equalsIgnoreCase("")) {
//                vh7.clay.setVisibility(View.VISIBLE);
//                vh7.optc.setText(Html.fromHtml(jsonobj.getString("c")));
//            } else
//                vh7.clay.setVisibility(View.GONE);
//
//            if (!jsonobj.getString("d").equalsIgnoreCase("")) {
//                vh7.dlay.setVisibility(View.VISIBLE);
//                vh7.optd.setText(Html.fromHtml(jsonobj.getString("d")));
//            } else
//                vh7.dlay.setVisibility(View.GONE);
//
//            if (!jsonobj.getString("post_image").equalsIgnoreCase("null")) {
//                if (jsonobj.getString("post_image").length() > 1) {
//                    vh7.imagelayout.setVisibility(View.VISIBLE);
////                    ImageLoader imageLoader = ImageLoader.getInstance();
////                    imageLoader.init(ImageLoaderConfiguration.createDefault(c));
//
////                    loadImage(vh7.queimage,Constants.URL_Image + jsonobj.getString("post_image"));
//                    imageLoader.getInstance().displayImage(Constants.URL_Image + jsonobj.getString("post_image"), vh7.queimage, options, animateFirstListener);
//                    Logg("imageurl_url", Constants.URL + jsonobj.getString("post_image"));
//                } else {
//                    vh7.imagelayout.setVisibility(View.GONE);
//                }
//            } else {
//                vh7.imagelayout.setVisibility(View.GONE);
//            }
//
//            if (jsonobj.getString("uimage").length() > 1) {
////                ImageLoader imageLoader = ImageLoader.getInstance();
////                imageLoader.init(ImageLoaderConfiguration.createDefault(c));
////                loadImage(vh7.userimage,jsonobj.getString("uimage"));
//                imageLoader.getInstance().displayImage(jsonobj.getString("uimage"), vh7.userimage, optionsuser, animateFirstListener);
//            }
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        vh7.lay_posttype.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
////                        jsonobj=new JSONObject(home.getJsonfield());
//                    if (!clas.equalsIgnoreCase("UserProfile")) {
//                        Intent in = new Intent(c, UserProfile.class);
//                        in.putExtra("email", home.getUid());
//                        c.startActivity(in);
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        });
//
//        vh7.lay_like.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //Logg("question like", "question like click");
//
//                if (!nw.isConnectingToInternet()) {
//                    Toast.makeText(c, "No Internet Connection", Toast.LENGTH_LONG).show();
//                    return;
//                }
//
//                volleylike_post(p);
//                int l = home.getLikecount();
//
//                if (home.getLikestatus().equals("like")) {
//
////                    int l=home.getLikecount();
//                    if (l > 0)
//                        l--;
//                    if (l > 0) {
//                        vh7.tv_likecount.setText(l + c.getResources().getString(R.string.like));
//                    } else {
//                        vh7.tv_likecount.setText(c.getResources().getString(R.string.like));
//                    }
//                    //vh7.tv_likecount.setText(l + c.getResources().getString(R.string.like));
////                    data.set(p,home);
//                    vh7.likeimage.setImageResource(R.drawable.ic_bulb_lightup);
//                    vh7.tv_likecount.setTextColor(c.getResources().getColor(R.color.text_lightgray));
//                } else {
//
////                    int l=home.getLikecount();
//                    l++;
//
//                    vh7.tv_likecount.setText(l + c.getResources().getString(R.string.like));
//                    vh7.likeimage.setImageResource(R.drawable.ic_bulb_filled);
//                    vh7.tv_likecount.setTextColor(c.getResources().getColor(R.color.like_text_color));
//                }
//
//            }
//        });
//
//        vh7.groupname.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                try {
//                    if (Constants.Current_Activity == 100) {
//                        jsonobj = new JSONObject(home.getJsonfield());
//
//                        Intent disitem = new Intent(c, Group_Profile.class);
//                        disitem.putExtra("groupname", jsonobj.getString("groupname"));
//                        disitem.putExtra("pid", home.getGroupid());
////                        disitem.putExtra("totalcom", home.getCommentcount());
//                        c.startActivity(disitem);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//
//        vh7.lay_share.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                JSONObject shareobj = new JSONObject();
//
//                try {
//
//                    jsonobj = new JSONObject(home.getJsonfield());
//
//                    shareobj.put("class", "HomeActivity");
//                    shareobj.put("title", jsonobj.getString("question"));
//                    shareobj.put("description", "");
//                    shareobj.put("image", "");
//                    shareobj.put("id", home.getId());
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                Utility.shareIntent(c, shareobj);
//
//            }
//        });
//
//        vh7.commentlay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent in = new Intent(c, Comment_Common_homepage.class);
//                in.putExtra("posttype", "7");
//                in.putExtra("postid", home.getId());
//                c.startActivity(in);
//
//            }
//        });
//
//        vh7.queimage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (vh7.queimage.getDrawable() == null) {
//                } else {
//
//                    try {
//                        jsonobj = new JSONObject(home.getJsonfield());
//                        Utility.Zoom_Image(c, Constants.URL_Image + jsonobj.getString("post_image"));
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                    //imagedialog(vh7.queimage);
//                }
//            }
//        });
//
//        userans = dbh.postquestion_get(home.getId());
//
//        //Logg("rightanswer", rightans);
//        //Logg("useranser=", userans + " ");
//
//        if (!rightans.equalsIgnoreCase("null")) {
//            if (!userans.equalsIgnoreCase("ua")) {
//
//                try {
//
//                    if (!jsonobj.getString("answerpoll").equalsIgnoreCase("null"))
//                        jsonpoll = new JSONObject(jsonobj.getString("answerpoll"));
//                    else
//                        jsonpoll = new JSONObject();
//
//                    if (userans.equalsIgnoreCase("a")) {
//                        if (rightans.equalsIgnoreCase("a")) {
//                            if (jsonpoll.has("a")) {
//                                perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("a")));
//                                vh7.bar_a.setProgress(perc);
//                                vh7.bar_a.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_green));
//                                if (jsonpoll.has("b")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("b")));
//                                    vh7.bar_b.setProgress(perc);
//                                    vh7.bar_b.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("c")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("c")));
//                                    vh7.bar_c.setProgress(perc);
//                                    vh7.bar_c.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("d")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("d")));
//
//                                    vh7.bar_d.setProgress(perc);
//                                    vh7.bar_d.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                            } else {
//                                vh7.bar_a.setProgress(0);
//                                vh7.bar_a.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_green));
//                                if (jsonpoll.has("b")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("b")));
//                                    vh7.bar_b.setProgress(perc);
//                                    vh7.bar_b.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("c")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("c")));
//                                    vh7.bar_c.setProgress(perc);
//                                    vh7.bar_c.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("d")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("d")));
//                                    vh7.bar_d.setProgress(perc);
//                                    vh7.bar_d.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//
//                            }
////                    vh7.alay.setBackgroundColor(Color.parseColor("#33009900"));
//                        } else {
//                            if (jsonpoll.has("a")) {
//                                perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("a")));
//                                vh7.bar_a.setProgress(perc);
//                                vh7.bar_a.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_red));
//                                if (jsonpoll.has("b")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("b")));
//                                    vh7.bar_b.setProgress(perc);
//                                    vh7.bar_b.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("c")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("c")));
//                                    vh7.bar_c.setProgress(perc);
//                                    vh7.bar_c.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("d")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("d")));
//                                    vh7.bar_d.setProgress(perc);
//                                    vh7.bar_d.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                            } else {
//                                vh7.bar_a.setProgress(0);
//                                vh7.bar_a.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_red));
//
//                                if (jsonpoll.has("b")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("b")));
//                                    vh7.bar_b.setProgress(perc);
//                                    vh7.bar_b.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("c")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("c")));
//                                    vh7.bar_c.setProgress(perc);
//                                    vh7.bar_c.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("d")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("d")));
//                                    vh7.bar_d.setProgress(perc);
//                                    vh7.bar_d.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//
//                            }
//                            //vh7.alay.setBackgroundColor(Color.parseColor("#33ff0000"));
//                        }
//                    }
//                    if (userans.equalsIgnoreCase("b")) {
//                        if (rightans.equalsIgnoreCase("b")) {
//                            if (jsonpoll.has("b")) {
//                                perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("b")));
//                                vh7.bar_b.setProgress(perc);
//                                vh7.bar_b.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_green));
//                                if (jsonpoll.has("a")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("a")));
//                                    vh7.bar_a.setProgress(perc);
//                                    vh7.bar_a.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("c")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("c")));
//                                    vh7.bar_c.setProgress(perc);
//                                    vh7.bar_c.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("d")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("d")));
//                                    vh7.bar_d.setProgress(perc);
//                                    vh7.bar_d.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                            } else {
//                                vh7.bar_b.setProgress(0);
//                                vh7.bar_b.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_green));
//
//                                if (jsonpoll.has("a")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("a")));
//                                    vh7.bar_a.setProgress(perc);
//                                    vh7.bar_a.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("c")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("c")));
//                                    vh7.bar_c.setProgress(perc);
//                                    vh7.bar_c.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("d")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("d")));
//                                    vh7.bar_d.setProgress(perc);
//                                    vh7.bar_d.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                            }
//                            //vh7.blay.setBackgroundColor(Color.parseColor("#33009900"));
//                        } else {
//                            if (jsonpoll.has("b")) {
//                                perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("b")));
//                                vh7.bar_b.setProgress(perc);
//                                vh7.bar_b.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_red));
//
//                                if (jsonpoll.has("a")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("a")));
//                                    vh7.bar_a.setProgress(perc);
//                                    vh7.bar_a.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("c")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("c")));
//                                    vh7.bar_c.setProgress(perc);
//                                    vh7.bar_c.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("d")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("d")));
//                                    vh7.bar_d.setProgress(perc);
//                                    vh7.bar_d.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                            } else {
//                                vh7.bar_b.setProgress(0);
//                                vh7.bar_b.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_red));
//                                if (jsonpoll.has("a")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("a")));
//                                    vh7.bar_a.setProgress(perc);
//                                    vh7.bar_a.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("c")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("c")));
//                                    vh7.bar_c.setProgress(perc);
//                                    vh7.bar_c.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("d")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("d")));
//                                    vh7.bar_d.setProgress(perc);
//                                    vh7.bar_d.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                            }
//                            // vh7.blay.setBackgroundColor(Color.parseColor("#33ff0000"));
//                        }
//                    }
//                    if (userans.equalsIgnoreCase("c")) {
//                        if (rightans.equalsIgnoreCase("c")) {
//                            if (jsonpoll.has("c")) {
//                                perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("c")));
//                                vh7.bar_c.setProgress(perc);
//                                vh7.bar_c.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_green));
//                                if (jsonpoll.has("a")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("a")));
//                                    vh7.bar_a.setProgress(perc);
//                                    vh7.bar_a.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("b")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("b")));
//                                    vh7.bar_b.setProgress(perc);
//                                    vh7.bar_b.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("d")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("d")));
//                                    vh7.bar_d.setProgress(perc);
//                                    vh7.bar_d.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                            } else {
//                                vh7.bar_c.setProgress(0);
//                                vh7.bar_c.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_green));
//                                if (jsonpoll.has("a")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("a")));
//                                    vh7.bar_a.setProgress(perc);
//                                    vh7.bar_a.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("b")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("b")));
//                                    vh7.bar_b.setProgress(perc);
//                                    vh7.bar_b.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("d")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("d")));
//                                    vh7.bar_d.setProgress(perc);
//                                    vh7.bar_d.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                            }
//                            //vh7.clay.setBackgroundColor(Color.parseColor("#33009900"));
//                        } else {
//                            if (jsonpoll.has("c")) {
//                                perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("c")));
//                                vh7.bar_c.setProgress(perc);
//                                vh7.bar_c.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_red));
//
//                                if (jsonpoll.has("a")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("a")));
//                                    vh7.bar_a.setProgress(perc);
//                                    vh7.bar_a.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("b")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("b")));
//                                    vh7.bar_b.setProgress(perc);
//                                    vh7.bar_b.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("d")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("d")));
//                                    vh7.bar_d.setProgress(perc);
//                                    vh7.bar_d.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//
//                            } else {
//                                vh7.bar_c.setProgress(1);
//                                vh7.bar_c.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_red));
//
//                                if (jsonpoll.has("a")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("a")));
//                                    vh7.bar_a.setProgress(perc);
//                                    vh7.bar_a.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("b")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("b")));
//                                    vh7.bar_b.setProgress(perc);
//                                    vh7.bar_b.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("d")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("d")));
//                                    vh7.bar_d.setProgress(perc);
//                                    vh7.bar_d.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//
//                            }
////                    vh7.clay.setBackgroundColor(Color.parseColor("#33ff0000"));
//                        }
//                    }
//                    if (userans.equalsIgnoreCase("d")) {
//                        if (rightans.equalsIgnoreCase("d")) {
//                            if (jsonpoll.has("d")) {
//                                perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("d")));
//                                vh7.bar_d.setProgress(perc);
//                                vh7.bar_d.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_green));
//
//                                if (jsonpoll.has("a")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("a")));
//                                    vh7.bar_a.setProgress(perc);
//                                    vh7.bar_a.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("b")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("b")));
//                                    vh7.bar_b.setProgress(perc);
//                                    vh7.bar_b.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("c")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("c")));
//                                    vh7.bar_c.setProgress(perc);
//                                    vh7.bar_c.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//
//                            } else {
//                                vh7.bar_d.setProgress(0);
//                                vh7.bar_d.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_green));
//                                if (jsonpoll.has("a")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("a")));
//                                    vh7.bar_a.setProgress(perc);
//                                    vh7.bar_a.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("b")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("b")));
//                                    vh7.bar_b.setProgress(perc);
//                                    vh7.bar_b.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("c")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("c")));
//                                    vh7.bar_c.setProgress(perc);
//                                    vh7.bar_c.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                            }
//
//
////                    vh7.dlay.setBackgroundColor(Color.parseColor("#33009900"));
//                        } else {
//                            if (jsonpoll.has("d")) {
//                                perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("d")));
//                                vh7.bar_d.setProgress(perc);
//                                vh7.bar_d.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_red));
//
//                                if (jsonpoll.has("a")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("a")));
//                                    vh7.bar_a.setProgress(perc);
//                                    vh7.bar_a.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("b")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("b")));
//                                    vh7.bar_b.setProgress(perc);
//                                    vh7.bar_b.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("c")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("c")));
//                                    vh7.bar_c.setProgress(perc);
//                                    vh7.bar_c.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                            } else {
//                                vh7.bar_d.setProgress(0);
//                                vh7.bar_d.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_red));
//
//                                if (jsonpoll.has("a")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("a")));
//                                    vh7.bar_a.setProgress(perc);
//                                    vh7.bar_a.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("b")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("b")));
//                                    vh7.bar_b.setProgress(perc);
//                                    vh7.bar_b.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                                if (jsonpoll.has("c")) {
//                                    perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("c")));
//                                    vh7.bar_c.setProgress(perc);
//                                    vh7.bar_c.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                                }
//                            }
//                        }
//                    }
//
//                    if (rightans.equalsIgnoreCase("a")) {
//                        //Logg("jsonpoll_data "+p+"  "+userans+" "+rightans,jsonpoll+"");
//
//                        if (jsonpoll.has("a")) {
//                            perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("a")));
//                            vh7.bar_a.setProgress(perc);
//                            vh7.tv_per_a.setText(perc + "%");
//                            vh7.bar_a.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_green));
//                        } else {
//                            vh7.bar_a.setProgress(100);
//                            vh7.bar_a.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_green));
//                        }
//                        //vh7.alay.setBackgroundColor(Color.parseColor("#33009900"));
//                    }
//                    if (rightans.equalsIgnoreCase("b")) {
//                        //Logg("jsonpoll_data "+p+"  "+userans+" "+rightans,jsonpoll+"");
//
//                        if (jsonpoll.has("b")) {
//                            perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("b")));
//                            vh7.bar_b.setProgress(perc);
//                            vh7.tv_per_b.setText(perc + "%");
//                            vh7.bar_b.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_green));
//                        } else {
//                            vh7.bar_b.setProgress(100);
//                            vh7.bar_b.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_green));
//                        }
//                        // vh7.blay.setBackgroundColor(Color.parseColor("#33009900"));
//                    }
//
//                    if (rightans.equalsIgnoreCase("c")) {
//                        //Logg("jsonpoll_data "+p+"  "+userans+" "+rightans,jsonpoll+"");
//
//                        if (jsonpoll.has("c")) {
//                            perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("c")));
//                            vh7.bar_c.setProgress(perc);
//                            vh7.tv_per_c.setText(perc + "%");
//                            vh7.bar_c.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_green));
//                        } else {
//                            vh7.bar_c.setProgress(100);
//                            vh7.bar_c.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_green));
//                        }
//
//                        //vh7.clay.setBackgroundColor(Color.parseColor("#33009900"));
//
//                    }
//
//                    if (rightans.equalsIgnoreCase("d")) {
//                        //Logg("jsonpoll_data "+p+"  "+userans+" "+rightans,jsonpoll+"");
//
//                        if (jsonpoll.has("d")) {
//                            perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("d")));
//                            vh7.bar_d.setProgress(perc);
//                            vh7.bar_d.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_green));
//                            vh7.tv_per_d.setText(perc + "%");
//                        } else {
//                            //perc=percentagecount(jsonpoll,Integer.parseInt(jsonpoll.getString("d")));
//                            vh7.bar_d.setProgress(100);
////                            vh7.tv_per_a.setText(perc+"%");
//                            vh7.bar_d.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_green));
//                        }
////                    vh7.dlay.setBackgroundColor(Color.parseColor("#33009900"));
//                    }
//
//                    if (jsonpoll.has("a")) {
//                        //Logg("jsonpoll_data_perc a"+p+"  ",jsonpoll.getString("a")+"");
//
//                        perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("a")));
//                        vh7.tv_per_a.setText(perc + "%");
//                    } else {
//                        vh7.tv_per_a.setText("0%");
//                    }
//                    if (jsonpoll.has("b")) {
//
//                        //Logg("jsonpoll_data_perc b"+p+"  ",jsonpoll.getString("b")+"");
//
//                        perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("b")));
//                        vh7.tv_per_b.setText(perc + "%");
//
//                    } else {
//                        vh7.tv_per_b.setText("0%");
//                    }
//                    if (jsonpoll.has("c")) {
//                        //Logg("jsonpoll_data_perc c"+p+"  ",jsonpoll.getString("c")+"");
//
//                        perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("c")));
//                        vh7.tv_per_c.setText(perc + "%");
//                    } else {
//                        vh7.tv_per_c.setText("0%");
//                    }
//                    if (jsonpoll.has("d")) {
//                        //Logg("jsonpoll_data_perc d"+p+"  ",jsonpoll.getString("d")+"");
//
//                        perc = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("d")));
//                        vh7.tv_per_d.setText(perc + "%");
//                    } else {
//                        vh7.tv_per_d.setText("0%");
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    //Logg("json_exception","e",e);
//                }
//
//            }
//        } else {
//
//            //user not give correct answer
//
//            try {
//
//                int per = 0;
//
//                if (!jsonobj.getString("answerpoll").equalsIgnoreCase("null"))
//                    jsonpoll = new JSONObject(jsonobj.getString("answerpoll"));
//                else
//                    jsonpoll = new JSONObject();
//
//                if (!userans.equalsIgnoreCase("ua")) {
//                    if (jsonpoll.has("a")) {
//                        per = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("a")));
//                        vh7.bar_a.setProgress(per);
//                        vh7.bar_a.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                        vh7.tv_per_a.setText(per + "%");
//                    } else {
//                        vh7.bar_a.setProgress(0);
//                        vh7.bar_a.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                        vh7.tv_per_a.setText("0%");
//                    }
//
//                    if (jsonpoll.has("b")) {
//                        per = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("b")));
//                        vh7.bar_b.setProgress(per);
//                        vh7.bar_b.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                        vh7.tv_per_b.setText(per + "%");
//                    } else {
//                        vh7.bar_b.setProgress(0);
//                        vh7.bar_b.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                        vh7.tv_per_b.setText("0%");
//                    }
//                    if (jsonpoll.has("c")) {
//                        per = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("c")));
//                        vh7.bar_c.setProgress(per);
//                        vh7.bar_c.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                        vh7.tv_per_c.setText(per + "%");
//                    } else {
//                        vh7.bar_c.setProgress(0);
//                        vh7.bar_c.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                        vh7.tv_per_c.setText("0%");
//                    }
//
//                    if (jsonpoll.has("d")) {
//                        per = percentagecount(jsonpoll, Integer.parseInt(jsonpoll.getString("d")));
//                        vh7.bar_d.setProgress(per);
//                        vh7.bar_d.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                        vh7.tv_per_d.setText(per + "%");
//                    } else {
//                        vh7.bar_d.setProgress(0);
//                        vh7.bar_d.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_gray));
//                        vh7.tv_per_d.setText("0%");
//                    }
//                }
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//
//        vh7.alay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                String ans = null;
//                try {
//
//                    jsonobj = new JSONObject(home.getJsonfield());
//                    ans = jsonobj.getString("ans");
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                userans = dbh.postquestion_get(home.getId());
//                Logg("useranswer", userans + " " + ans);
//
//                if (userans.equalsIgnoreCase("ua")) {
//
//                    setanswer(home, p, "a");
//                    volleyinsertanswer(home, "a", p);
//                }
//            }
//        });
//
//        vh7.blay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String ans = null;
//                try {
//
//                    jsonobj = new JSONObject(home.getJsonfield());
//                    ans = jsonobj.getString("ans");
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                userans = dbh.postquestion_get(home.getId());
//
//                Logg("useranswer", userans + " " + ans);
//
//                if (userans.equalsIgnoreCase("ua")) {
//
//                    setanswer(home, p, "b");
//                    volleyinsertanswer(home, "b", p);
//                }
//            }
//        });
//        vh7.clay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                String ans = null;
//                try {
//
//                    jsonobj = new JSONObject(home.getJsonfield());
//                    ans = jsonobj.getString("ans");
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                userans = dbh.postquestion_get(home.getId());
//
//                Logg("useranswer", userans + " " + ans);
//
//                if (userans.equalsIgnoreCase("ua")) {
//                    setanswer(home, p, "c");
//                    volleyinsertanswer(home, "c", p);
//                }
//            }
//        });
//        vh7.dlay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String ans = null;
//                try {
//
//                    jsonobj = new JSONObject(home.getJsonfield());
//                    ans = jsonobj.getString("ans");
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                userans = dbh.postquestion_get(home.getId());
//
//                Logg("useranswer", userans + " " + ans);
//
//                if (userans.equalsIgnoreCase("ua")) {
//
//                    setanswer(home, p, "d");
//                    volleyinsertanswer(home, "d", p);
//
//                }
//            }
//        });
//
//    }

//    private void setMetaData(final ViewHolder8 vh0, final int p) {
//
//        final Home_getset home = (Home_getset) data.get(p);
//
//        try {
//
//            //Typeface font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");
//            vh0.tv_uname.setTypeface(font_demi);
//            vh0.tv_groupname.setTypeface(font_demi);
//            //Typeface font_medium = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");
//            vh0.tv_collage.setTypeface(font_medium);
//            vh0.tv_time.setTypeface(font_medium);
//            vh0.tv_reward_count.setTypeface(font_medium);
//            vh0.tv_likecount.setTypeface(font_medium);
//            vh0.tv_replies_count.setTypeface(font_medium);
//            vh0.tv_member.setTypeface(font_medium);
//            vh0.tv_share.setTypeface(font_medium);
//
//            vh0.tv_comm.setTypeface(font_demi);
//            vh0.tv_description.setTypeface(font_medium);
//
//            jsonobj = new JSONObject(home.getJsonfield());
//
//            vh0.tv_time.setText(getDate(home.getTimestamp()));
//            vh0.tv_collage.setText(jsonobj.getString("collage"));
//            vh0.tv_uname.setText(jsonobj.getString("name"));
//            String com = jsonobj.getString("comments");
//            com = com.replaceAll("%20", " ");
//            vh0.tv_comm.setText(com);
//            vh0.tv_description.setText(home.getPostdescription() + "");
//
//            vh0.tv_reward_count.setText(jsonobj.getString("userreward") + " Reward");
//
//            if (home.getCommentcount() > 0)
//                vh0.tv_replies_count.setText(home.getCommentcount() + " Comments");
//            else
//                vh0.tv_replies_count.setText(home.getCommentcount() + " Comments");
//
//            vh0.tv_member.setText(jsonobj.getString("groupmember") + " Members");
//            vh0.tv_groupname.setText(jsonobj.getString("groupname"));
//            // vh0.tv_likecount.setText(home.getLikecount()+c.getResources().getString(R.string.like));
//            if (home.getLikecount() > 0) {
//                vh0.tv_likecount.setText(home.getLikecount() + c.getResources().getString(R.string.like));
//            } else {
//                vh0.tv_likecount.setText(c.getResources().getString(R.string.like));
//            }
//            //Logg("like_comment_position",p+" "+home.getLikestatus());
//
//            if (Constants.User_Email.equalsIgnoreCase(home.getUid())) {
//                vh0.iv_delete.setVisibility(View.VISIBLE);
////                vh0.tv_liketext.setText("Delete");
////                vh0.iv_like.setImageResource(R.drawable.ic_delete);
//            } else {
//                vh0.iv_delete.setVisibility(View.GONE);
//            }
//
//            if (home.getLikestatus().equals("like")) {
//                vh0.iv_like.setImageResource(R.drawable.ic_bulb_filled);
//                vh0.tv_likecount.setTextColor(c.getResources().getColor(R.color.like_text_color));
//            } else {
//                vh0.iv_like.setImageResource(R.drawable.ic_bulb_lightup);
//                vh0.tv_likecount.setTextColor(c.getResources().getColor(R.color.text_lightgray));
//            }
//
//            vh0.iv_save.setVisibility(View.GONE);
//
//            if (dbh.get_savecontent_single(home.getId()) > 0)
//                vh0.iv_save.setImageResource(R.drawable.ic_bookmark_black_24dp);
//            else
//                vh0.iv_save.setImageResource(R.drawable.ic_bookmark_1_512);
//
//
//            vh0.lay_posttype.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    try {
////                        jsonobj=new JSONObject(home.getJsonfield());
//                        if (!clas.equalsIgnoreCase("UserProfile")) {
//                            Intent in = new Intent(c, UserProfile.class);
//                            in.putExtra("email", home.getUid());
//                            c.startActivity(in);
//                        }
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
//
//            vh0.like_lay.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View view) {
//
//                    if (!nw.isConnectingToInternet()) {
//                        Toast.makeText(c, "No Internet Connection", Toast.LENGTH_LONG).show();
//                        return;
//                    }
//
//                    volleylike_comment(p);
//
//
//                    int rew = 0, l = home.getLikecount();
//
//                    try {
//                        jsonobj = new JSONObject(home.getJsonfield());
//                        rew = Integer.valueOf(jsonobj.getString("userreward"));
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//
//                    if (home.getLikestatus().equals("like")) {
//
////                        int l= home.getLikecount();
//
//                        if (l > 0)
//                            l--;
//
//                        //  vh0.tv_likecount.setText(l+c.getResources().getString(R.string.like));
//                        if (l > 0) {
//                            vh0.tv_likecount.setText(l + c.getResources().getString(R.string.like));
//                        } else {
//                            vh0.tv_likecount.setText(c.getResources().getString(R.string.like));
//                        }
//                        vh0.iv_like.setImageResource(R.drawable.ic_bulb_lightup);
//                        vh0.tv_likecount.setTextColor(c.getResources().getColor(R.color.text_lightgray));
//                    } else {
////                        int l= home.getLikecount();
//                        l++;
//                        vh0.tv_likecount.setText(l + c.getResources().getString(R.string.like));
//
//                        vh0.iv_like.setImageResource(R.drawable.ic_bulb_filled);
//                        vh0.tv_likecount.setTextColor(c.getResources().getColor(R.color.like_text_color));
//                    }
//
//                    vh0.tv_reward_count.setText(rew + " Reward");
//
//                }
//            });
//
//            vh0.iv_delete.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    deletcomment(p);
//                }
//            });
//
//            vh0.replylay.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Intent in = new Intent(c, Replies.class);
//
//                    try {
//
//                        jsonobj = new JSONObject(home.getJsonfield());
//                        in.putExtra("psid", home.getId());
//                        c.startActivity(in);
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        //Logg("json_exception","e",e);
//                    }
//                }
//            });
//
//            vh0.tv_comm.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Intent in = new Intent(c, Meta_Data_Details.class);
//                    in.putExtra("postid", home.getId());
//                    c.startActivity(in);
//                }
//            });
//
//            vh0.tv_description.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Intent in = new Intent(c, Meta_Data_Details.class);
//                    in.putExtra("postid", home.getId());
//                    c.startActivity(in);
//                }
//            });
//
//
//            vh0.tv_groupname.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    try {
//                        if (Constants.Current_Activity == 100) {
//                            jsonobj = new JSONObject(home.getJsonfield());
//
//                            Intent disitem = new Intent(c, Group_Profile.class);
//                            disitem.putExtra("groupname", jsonobj.getString("groupname"));
//                            disitem.putExtra("pid", home.getGroupid());
////                        disitem.putExtra("totalcom", home.getCommentcount());
//                            c.startActivity(disitem);
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
//
//            //leave==share
//            vh0.leave.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    //alert(home.getGroupid());
//
//                    JSONObject shareobj = new JSONObject();
//
//                    String value = "";
//                    String[] valuearr;
//
//                    try {
//                        jsonobj = new JSONObject(home.getJsonfield());
//                        value = String.valueOf(Html.fromHtml(Html.fromHtml(home.getPostdescription() + "").toString()));
//                        valuearr = value.split(System.lineSeparator(), 2);
//                        value = valuearr[0];
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                    try {
//                        shareobj.put("class", "Meta_Data_Details");
//                        shareobj.put("title", jsonobj.getString("comments"));
//                        shareobj.put("description", " ");
//                        shareobj.put("image", jsonobj.getString("com_image"));
//                        shareobj.put("id", home.getId());
//
//                        Utility.shareIntent(c, shareobj);
//
////                        shareobj.put("posturl",home.getPosturl());
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                }
//            });
//
//            if (!jsonobj.getString("userimage").equalsIgnoreCase("null")) {
//
//                imageLoader.getInstance().displayImage(jsonobj.getString("userimage"), vh0.userimg, optionsuser, animateFirstListener);
//            }
//            if (!jsonobj.getString("com_image").equalsIgnoreCase("null")) {
//
//                imageLoader.getInstance().displayImage(jsonobj.getString("com_image"), vh0.cimage, options, animateFirstListener);
//                vh0.cimage.setVisibility(View.VISIBLE);
//            } else
//                vh0.cimage.setVisibility(View.GONE);
//
//
//            vh0.cimage.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent in = new Intent(c, Meta_Data_Details.class);
//                    in.putExtra("postid", home.getId());
//                    c.startActivity(in);
//                }
//            });
//
//            vh0.iv_save.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    //Logg("field_type_",home.getFieldtype()+"");
//                    if (dbh.get_savecontent_single(home.getId()) < 1)
//                        savecontent(home, vh0.iv_save);
//                    else {
//                        //already saved this item
//                    }
//                }
//            });
//
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//    }

    private void setChampions(final Champions_Holder vh5, final int p) {

//        final Home_getset home= (Home_getset)data.get(p);

//        vh5.tv_text.setText("Our Champions ");
        try {
//            Logg("obj_cham")
            champions_list_adapter = new Champions_list_rec_adapter(c, obj_champions.getJSONArray("champions"), clas);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        vh5.rv_honrizontal.setAdapter(champions_list_adapter);

    }

    private void setBanner(final Banner_Holder vh5, final int p) {

//        final Home_getset home= (Home_getset)data.get(p);

//        vh5.tv_header.setText("Our Mentors ");
        try {

            banner_list_adapter = new Banner_list_adapter_rec(c, banner_array, clas);
            vh5.rv_honrizontal.setAdapter(banner_list_adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setMentor(final Mentor_Holder vh5, final int p) {

        final Home_getset home = (Home_getset) data.get(p);

//        vh5.tv_header.setText("Our Mentors");

        try {
            mentor_list_adapter = new Mentor_list_adapter_rec(c, obj_menotrs.getJSONArray("data"), clas);
            vh5.rv_honrizontal.setAdapter(mentor_list_adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setanswer(Home_getset data1, int p, String ans) {

        JSONObject obj = new JSONObject();
        JSONObject ans_obj = new JSONObject();
        try {

//            Home_getset data1=data.get(p);

            obj = new JSONObject(data1.getJsonfield());

            if (obj.has("answerpoll")) {

                Logg("jsonobjcet", obj + " ");

                if (!obj.getString("answerpoll").equalsIgnoreCase("null"))
                    ans_obj = new JSONObject(obj.getString("answerpoll"));

                if (ans_obj.has(ans)) {
                    int count = Integer.parseInt(ans_obj.getString(ans));
                    count++;
                    ans_obj.put(ans, count + "");
                } else
                    ans_obj.put(ans, "1");
            } else {
                //JSONObject ans_obj=new JSONObject();
                ans_obj.put(ans, "1");
            }

            obj.put("answerpoll", ans_obj);

            data1.setJsonfield(obj + "");

            dbh.postquestion_insert(data1.getId(), ans);

            data.set(p, data1);

            notifyItemChanged(p);

        } catch (JSONException e) {
            e.printStackTrace();
            Logg("setanser_", e + "");
        }
    }

    public int percentagecount(JSONObject obj, int val) {

        int per = 0, total;
        opt_a = 0;
        opt_b = 0;
        opt_c = 0;
        opt_d = 0;

        try {

            if (obj.has("a"))
                opt_a = Integer.parseInt(obj.getString("a"));
            if (obj.has("b"))
                opt_b = Integer.parseInt(obj.getString("b"));
            if (obj.has("c"))
                opt_c = Integer.parseInt(obj.getString("c"));
            if (obj.has("d"))
                opt_d = Integer.parseInt(obj.getString("d"));

            per = val * 100;
            total = opt_a + opt_b + opt_c + opt_d;
            per = (Integer) per / total;

            //Logg("percentages right"+val,obj+" percentage "+per);

        } catch (JSONException e) {
            e.printStackTrace();
            //Logg("jsonexception_perc","e",e);
        }

        return per;
    }

    @Override
    public void onClick(View v) {


        if (v.getId() == R.id.group_layid) {
//            Intent i = new Intent(c, Category_List_Activity.class);
//            i.putExtra("class", "mainpg");
//            c.startActivity(i);
        } else if (v.getId() == R.id.event_layid) {
            Intent i = new Intent(c, Workshoplist.class);
            c.startActivity(i);
        } else if (v.getId() == R.id.test_layid) {
            Intent i = new Intent(c, Onlineexam.class);
            c.startActivity(i);
        } else if (v.getId() == R.id.job_layid) {
            Intent i = new Intent(c, Jobs_all.class);
            c.startActivity(i);
        } else if (v.getId() == R.id.post_layid) {
            Intent i = new Intent(c, News.class);
            c.startActivity(i);
        } else if (v.getId() == R.id.pdf_layid) {
            Intent i = new Intent(c, File_download.class);
            c.startActivity(i);
        }
    }

    public class Champions_Holder extends RecyclerView.ViewHolder {

        RecyclerView rv_honrizontal;
        TextView tv_text;

        Champions_Holder(View view) {
            super(view);

            rv_honrizontal = (RecyclerView) view.findViewById(R.id.recyclerView);
            LinearLayoutManager lay_Manager = new LinearLayoutManager(c);
            lay_Manager.setOrientation(LinearLayoutManager.HORIZONTAL);
            rv_honrizontal.setLayoutManager(lay_Manager);
            rv_honrizontal.setHasFixedSize(true);
            rv_honrizontal.setNestedScrollingEnabled(false);
            tv_text = (TextView) view.findViewById(R.id.tv_text);

//            setIsRecyclable(false);

        }
    }

    public class Mentor_Holder extends RecyclerView.ViewHolder {

        RecyclerView rv_honrizontal;
        TextView tv_header;

        Mentor_Holder(View view) {
            super(view);

            tv_header = (TextView) view.findViewById(R.id.tv_header);
            rv_honrizontal = (RecyclerView) view.findViewById(R.id.recyclerView);
            LinearLayoutManager lay_Manager = new LinearLayoutManager(c);
            lay_Manager.setOrientation(LinearLayoutManager.HORIZONTAL);
            rv_honrizontal.setLayoutManager(lay_Manager);
            rv_honrizontal.setHasFixedSize(true);
            rv_honrizontal.setNestedScrollingEnabled(false);

//            setIsRecyclable(false);

        }
    }

    public class Banner_Holder extends RecyclerView.ViewHolder {

        RecyclerView rv_honrizontal;
        TextView tv_header;

        Banner_Holder(View view) {
            super(view);

            tv_header = (TextView) view.findViewById(R.id.tv_header);
            tv_header.setVisibility(View.GONE);

            rv_honrizontal = (RecyclerView) view.findViewById(R.id.recyclerView);
            LinearLayoutManager lay_Manager = new LinearLayoutManager(c);
            lay_Manager.setOrientation(LinearLayoutManager.HORIZONTAL);
            rv_honrizontal.setLayoutManager(lay_Manager);
            rv_honrizontal.setHasFixedSize(true);
            rv_honrizontal.setNestedScrollingEnabled(false);

//            setIsRecyclable(false);

        }
    }

    public class NativeExpressAdViewHolder extends RecyclerView.ViewHolder {

        LinearLayout joblay, testlay, managegroup_lay, event_lay, post_lay, pdf_lay;

        NativeExpressAdViewHolder(View view) {
            super(view);

            pdf_lay = (LinearLayout) view.findViewById(R.id.pdf_layid);
            post_lay = (LinearLayout) view.findViewById(R.id.post_layid);
            joblay = (LinearLayout) view.findViewById(R.id.job_layid);
            testlay = (LinearLayout) view.findViewById(R.id.test_layid);
            event_lay = (LinearLayout) view.findViewById(R.id.event_layid);

        }
    }

//    public class ViewHolder0 extends RecyclerView.ViewHolder {
//
//        //  **************************  Set Group Comment  ********************
//
//        TextView tv_uname;
//        TextView tv_collage;
//        TextView tv_time;
//        TextView tv_reward_count;
//        TextView tv_replies_count;
//        TextView tv_member;
//        TextView tv_likecount;
//        TextView tv_liketext;
//        final TextView tv_comm;
//        TextView tv_groupname;
//        ImageView iv_like;
//        ImageView cimage;
//        ImageView iv_save;
//        ImageView iv_delete;
//        CircleImageView userimg;
//        LinearLayout leave, lay_posttype;
//        LinearLayout replylay;
//        LinearLayout like_lay;
//
//        public ViewHolder0(View v) {
//            super(v);
//
//            tv_uname = (TextView) v.findViewById(R.id.usernameid);
//            tv_collage = (TextView) v.findViewById(R.id.collegeid);
//            tv_time = (TextView) v.findViewById(R.id.timeid);
//            tv_reward_count = (TextView) v.findViewById(R.id.rewardid);
//            tv_replies_count = (TextView) v.findViewById(R.id.tv_commentcount);
//            tv_member = (TextView) v.findViewById(R.id.memberid);
//            tv_likecount = (TextView) v.findViewById(R.id.tv_liketext);
//            tv_liketext = (TextView) v.findViewById(R.id.tv_liketext);
//            tv_comm = (TextView) v.findViewById(R.id.commentid);
//            tv_groupname = (TextView) v.findViewById(R.id.groupnameid);
//            iv_like = (ImageView) v.findViewById(R.id.iv_like);
//            cimage = (ImageView) v.findViewById(R.id.commentimageid);
//            userimg = (CircleImageView) v.findViewById(R.id.userimage_id);
//            leave = (LinearLayout) v.findViewById(R.id.leave_layid);
//            lay_posttype = (LinearLayout) v.findViewById(R.id.lay_posttype);
//            replylay = (LinearLayout) v.findViewById(R.id.replylayid);
//            like_lay = (LinearLayout) v.findViewById(R.id.rewardlayid);
//            iv_save = (ImageView) v.findViewById(R.id.savecomment_id);
//            iv_delete = (ImageView) v.findViewById(R.id.iv_delete);
//
////            setIsRecyclable(false);
//
//            tv_uname.setTypeface(font_demi);
//            tv_groupname.setTypeface(font_demi);
//            // Typeface font_medium = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");
//            tv_collage.setTypeface(font_medium);
//            tv_time.setTypeface(font_medium);
//            tv_reward_count.setTypeface(font_medium);
//            tv_likecount.setTypeface(font_medium);
//            tv_replies_count.setTypeface(font_medium);
//            tv_member.setTypeface(font_medium);
//            tv_comm.setTypeface(font_medium);
//
//
//            lay_posttype.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Home_getset home = data.get(getAdapterPosition());
//                    try {
////                        jsonobj=new JSONObject(home.getJsonfield());
//                        if (!clas.equalsIgnoreCase("UserProfile")) {
//                            Intent in = new Intent(c, UserProfile.class);
//                            in.putExtra("email", home.getUid());
//                            c.startActivity(in);
//                        }
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
//
//        }
//    }

    public class ViewHolder1 extends RecyclerView.ViewHolder {

        //  **************************  Set News/Post  ********************

        TextView title;
        TextView descr;
        TextView date;
        TextView postby;
        TextView newstype;
        TextView tv_price;
        ImageView savenews;
        LinearLayout lay_share;
        TextView likecount;
        LinearLayout lay_like;
        TextView commentcount;
        TextView newscomment;
        TextView viewcount;
        LinearLayout newslayout;
        LinearLayout commentlay, lay_posttype;
        CircleImageView thumnainls;
        ImageView newsimage;
        ImageView iv_save;
        ImageView likeimage;
        TextView tv_share, tv_lightuptext;

        public ViewHolder1(View v) {
            super(v);

            title = (TextView) v.findViewById(R.id.titleid);
            descr = (TextView) v.findViewById(R.id.descriptionid);
            date = (TextView) v.findViewById(R.id.timeid);
            postby = (TextView) v.findViewById(R.id.tv_postby);
            tv_price = (TextView) v.findViewById(R.id.tv_price);
            newstype = (TextView) v.findViewById(R.id.newstypeid);
            savenews = (ImageView) v.findViewById(R.id.savenews_id);
            lay_share = (LinearLayout) v.findViewById(R.id.lay_shareid);
            lay_posttype = (LinearLayout) v.findViewById(R.id.lay_posttype);
            likecount = (TextView) v.findViewById(R.id.likeid);
            lay_like = (LinearLayout) v.findViewById(R.id.liketextid);
            commentcount = (TextView) v.findViewById(R.id.commentid);
            newscomment = (TextView) v.findViewById(R.id.newscommentid);
            viewcount = (TextView) v.findViewById(R.id.tv_view);
            newslayout = (LinearLayout) v.findViewById(R.id.newslayoutid);
            commentlay = (LinearLayout) v.findViewById(R.id.lay_comment);
            thumnainls = (CircleImageView) v.findViewById(R.id.thumnailsid);
            newsimage = (ImageView) v.findViewById(R.id.imageid);
            likeimage = (ImageView) v.findViewById(R.id.likeimageid);
            iv_save = (ImageView) v.findViewById(R.id.savenews_id);
            tv_share = (TextView) v.findViewById(R.id.homenewsshareid);
            tv_lightuptext = (TextView) v.findViewById(R.id.tv_lightup);
//            setIsRecyclable(false);
        }

    }

    public class ViewHolder2 extends RecyclerView.ViewHolder {

        //  **************************  Set Onlie Test  ********************

        TextView tv_examname;
        TextView tv_applicant;
        TextView tv_time, tv_date;
        TextView tv_question;
        TextView tv_start;
        TextView tv_postby;
        TextView tv_price;
        TextView tv_likecount;
        LinearLayout lay_like;
        TextView tv_commentcount, text, text1, share_text;
        //TextView tv_viewcount;
        LinearLayout layout;
        LinearLayout commentlay, lay_header;
        ImageView likeimage;
        LinearLayout lay_share;


        public ViewHolder2(View v) {
            super(v);

            tv_date = (TextView) v.findViewById(R.id.tv_date);
            text = (TextView) v.findViewById(R.id.text);
            text1 = (TextView) v.findViewById(R.id.text1);
            share_text = (TextView) v.findViewById(R.id.tv_shareid);
            tv_examname = (TextView) v.findViewById(R.id.examnameid);
            tv_applicant = (TextView) v.findViewById(R.id.applicantid);
            tv_time = (TextView) v.findViewById(R.id.timeid);
            tv_question = (TextView) v.findViewById(R.id.questionid);
            tv_start = (TextView) v.findViewById(R.id.startnowid);
            tv_postby = (TextView) v.findViewById(R.id.postedbyid);
            tv_price = (TextView) v.findViewById(R.id.tv_price);
            tv_likecount = (TextView) v.findViewById(R.id.tv_likeid);
            lay_like = (LinearLayout) v.findViewById(R.id.liketextid);
            tv_commentcount = (TextView) v.findViewById(R.id.commentid);
            lay_share = (LinearLayout) v.findViewById(R.id.ly_shareid);
            //TextView tv_viewcount = (TextView) v.findViewById(R.id.tv_view);
            layout = (LinearLayout) v.findViewById(R.id.layout);
            commentlay = (LinearLayout) v.findViewById(R.id.lay_comment);
            likeimage = (ImageView) v.findViewById(R.id.likeimageid);
            lay_header = (LinearLayout) v.findViewById(R.id.lay_header);
//            setIsRecyclable(false);
        }
    }

    public class ViewHolder3 extends RecyclerView.ViewHolder {

        //  **************************  Set New Jobs  ********************

        TextView tv_title;
        TextView tv_postname;
        TextView tv_post;
        LinearLayout lay;
        LinearLayout lay_share;
        ImageView iv_jobimage;
        ImageView iv_jobicon;
        ImageView iv_savecont;
        TextView tv_likecount;
        TextView tv_lastdate;
        TextView tv_commentcount;
        TextView tv_viewcount;
        LinearLayout lay_like, lay_posttype;
        LinearLayout newslayout;
        LinearLayout commentlay;
        ImageView likeimage;
        ImageView iv_save;
        TextView tv_share, tv_jobheadertitle, tv_lightuptext;

        public ViewHolder3(View v) {
            super(v);

            tv_title = (TextView) v.findViewById(R.id.titleid);
            tv_postname = (TextView) v.findViewById(R.id.post_nameid);
            tv_post = (TextView) v.findViewById(R.id.post_id);
            lay = (LinearLayout) v.findViewById(R.id.joblayoutid);
            lay_share = (LinearLayout) v.findViewById(R.id.ly_shareid);
            lay_posttype = (LinearLayout) v.findViewById(R.id.lay_posttype);
            iv_jobimage = (ImageView) v.findViewById(R.id.jobimagehome_id);
            iv_jobicon = (ImageView) v.findViewById(R.id.jobimage_id);
            iv_savecont = (ImageView) v.findViewById(R.id.savecontentjob_id);
            tv_likecount = (TextView) v.findViewById(R.id.tv_likeid);
            tv_commentcount = (TextView) v.findViewById(R.id.commentid);
            tv_viewcount = (TextView) v.findViewById(R.id.tv_view);
            tv_lastdate = (TextView) v.findViewById(R.id.tv_lastdate);
            lay_like = (LinearLayout) v.findViewById(R.id.liketextid);
//            newslayout=(LinearLayout)v.findViewById(R.id.newslayoutid);
            commentlay = (LinearLayout) v.findViewById(R.id.lay_comment);
            likeimage = (ImageView) v.findViewById(R.id.likeimageid);
            iv_save = (ImageView) v.findViewById(R.id.savecontentjob_id);
            tv_jobheadertitle = (TextView) v.findViewById(R.id.tv_jobheader);
            tv_share = (TextView) v.findViewById(R.id.tv_shareid);
            tv_lightuptext = (TextView) v.findViewById(R.id.tv_lightup);

//            setIsRecyclable(false);
        }
    }

    public class ViewHolder4 extends RecyclerView.ViewHolder {

        //  **************************  Set Pdf Download  ********************
        TextView tv_title;
        TextView tv_postedby;
        LinearLayout lay_download;
        TextView tv_likecount;
        TextView tv_commentcount;
        TextView tv_viewcount;
        LinearLayout lay_share;
        LinearLayout lay_like;
        LinearLayout newslayout;
        LinearLayout commentlay;
        ImageView likeimage;
        ImageView iv_save;

        TextView tv_view;
        TextView tv_download, tv_text;

        TextView tv_date, tv_like, tv_comment, tv_share;


        public ViewHolder4(View v) {
            super(v);

            tv_title = (TextView) v.findViewById(R.id.pdfnameid);
            tv_postedby = (TextView) v.findViewById(R.id.postedbypdfid);
            lay_download = (LinearLayout) v.findViewById(R.id.pdfdownloadid);
            tv_likecount = (TextView) v.findViewById(R.id.tv_likeid);
            tv_commentcount = (TextView) v.findViewById(R.id.commentid);
            tv_viewcount = (TextView) v.findViewById(R.id.tv_view);
            lay_share = (LinearLayout) v.findViewById(R.id.ly_shareid);
            lay_like = (LinearLayout) v.findViewById(R.id.liketextid);
            newslayout = (LinearLayout) v.findViewById(R.id.newslayoutid);
            commentlay = (LinearLayout) v.findViewById(R.id.lay_comment);
            likeimage = (ImageView) v.findViewById(R.id.likeimageid);
            iv_save = (ImageView) v.findViewById(R.id.savecontentpdf_id);
            tv_view = (TextView) v.findViewById(R.id.tv_view);

            tv_text = (TextView) v.findViewById(R.id.text);
            tv_date = (TextView) v.findViewById(R.id.tv_lastdate);
            tv_like = (TextView) v.findViewById(R.id.text1);
            tv_comment = (TextView) v.findViewById(R.id.newscommentid);
            tv_share = (TextView) v.findViewById(R.id.tv_shareid);
            tv_download = (TextView) v.findViewById(R.id.tv_download);
//            setIsRecyclable(false);
        }

    }

    public class ViewHolder5 extends RecyclerView.ViewHolder {

        //  **************************  Set Workshop  ********************

        TextView tv_title;
        TextView tv_description;
        TextView tv_date;
        TextView tv_location;
        TextView tv_explore;
        ImageView iv_workimage;
        TextView tv_likecount;
        TextView tv_commentcount;
        TextView tv_viewcount, tv_register;
        LinearLayout lay_share;
        LinearLayout lay_like;
        LinearLayout newslayout;
        LinearLayout commentlay;
        LinearLayout lay_workshop;
        ImageView likeimage;
        TextView tv_like, tv_comment, tv_share;

        public ViewHolder5(View v) {
            super(v);

            tv_title = (TextView) v.findViewById(R.id.titleid);
            tv_description = (TextView) v.findViewById(R.id.descriptionid);
            tv_date = (TextView) v.findViewById(R.id.dateid);
            tv_location = (TextView) v.findViewById(R.id.locationid);
            tv_explore = (TextView) v.findViewById(R.id.exploreid);
            iv_workimage = (ImageView) v.findViewById(R.id.imageid);
            tv_likecount = (TextView) v.findViewById(R.id.tv_likeid);
            tv_commentcount = (TextView) v.findViewById(R.id.commentid);
            tv_viewcount = (TextView) v.findViewById(R.id.tv_view);
            lay_share = (LinearLayout) v.findViewById(R.id.ly_shareid);
            lay_like = (LinearLayout) v.findViewById(R.id.liketextid);
            newslayout = (LinearLayout) v.findViewById(R.id.newslayoutid);
            lay_workshop = (LinearLayout) v.findViewById(R.id.lay_workshop);
            commentlay = (LinearLayout) v.findViewById(R.id.lay_comment);
            likeimage = (ImageView) v.findViewById(R.id.likeimageid);
            tv_like = (TextView) v.findViewById(R.id.text1);
            tv_register = (TextView) v.findViewById(R.id.tv_register);
            tv_comment = (TextView) v.findViewById(R.id.newscommentid);
            tv_share = (TextView) v.findViewById(R.id.tv_shareid);

//            setIsRecyclable(false);
            /*
             */
        }

    }

//    public class ViewHolder7 extends RecyclerView.ViewHolder {
//
//        //  **************************  Set Question  ********************
//
//        TextView question, attempt;
//        TextView uname;
//        TextView coll;
//        TextView time;
//        TextView opta;
//        TextView optb;
//        TextView optc;
//        TextView optd;
//        TextView a_cir;
//        TextView b_cir;
//        TextView c_cir;
//        TextView d_cir;
//        TextView groupname;
//        ImageView queimage;
//        CircleImageView userimage;
//        LinearLayout imagelayout;
//        LinearLayout alay;
//        LinearLayout blay;
//        LinearLayout clay;
//        LinearLayout dlay;
//
//        TextView tv_likecount, tv_reward;
//        TextView tv_commentcount;
//        LinearLayout lay_share;
//        LinearLayout lay_like, lay_posttype;
//        LinearLayout commentlay;
//        ImageView likeimage;
//        TextView tv_per_a, tv_per_b, tv_per_c, tv_per_d;
//        ProgressBar bar_a, bar_b, bar_c, bar_d;
//
//        public ViewHolder7(View v) {
//            super(v);
//
//            tv_likecount = (TextView) v.findViewById(R.id.tv_liketext);
//            tv_commentcount = (TextView) v.findViewById(R.id.tv_commentcount);
//            lay_share = (LinearLayout) v.findViewById(R.id.lay_shareid);
//            lay_like = (LinearLayout) v.findViewById(R.id.rewardlayid);
//            commentlay = (LinearLayout) v.findViewById(R.id.replylayid);
//            lay_posttype = (LinearLayout) v.findViewById(R.id.lay_posttype);
//
//            likeimage = (ImageView) v.findViewById(R.id.iv_like);
//            tv_reward = (TextView) v.findViewById(R.id.rewardid);
//
//            attempt = (TextView) v.findViewById(R.id.numberofattempts);
//            question = (TextView) v.findViewById(R.id.questionid);
//            uname = (TextView) v.findViewById(R.id.usernameid);
//            coll = (TextView) v.findViewById(R.id.collegeid);
//            time = (TextView) v.findViewById(R.id.timeid);
//            opta = (TextView) v.findViewById(R.id.dc_optionaid);
//            optb = (TextView) v.findViewById(R.id.dc_optionbid);
//            optc = (TextView) v.findViewById(R.id.dc_optioncid);
//            optd = (TextView) v.findViewById(R.id.dc_optiondid);
//
//            a_cir = (TextView) v.findViewById(R.id.dc_aid);
//            b_cir = (TextView) v.findViewById(R.id.dc_bid);
//            c_cir = (TextView) v.findViewById(R.id.dc_cid);
//            d_cir = (TextView) v.findViewById(R.id.dc_did);
//
//            groupname = (TextView) v.findViewById(R.id.groupnameid);
//            queimage = (ImageView) v.findViewById(R.id.commentimageid);
//            userimage = (CircleImageView) v.findViewById(R.id.userimage_id);
//            imagelayout = (LinearLayout) v.findViewById(R.id.imagelayoutid);
//
//            alay = (LinearLayout) v.findViewById(R.id.dc_optionalayid);
//            blay = (LinearLayout) v.findViewById(R.id.dc_optionblayid);
//            clay = (LinearLayout) v.findViewById(R.id.dc_optionclayid);
//            dlay = (LinearLayout) v.findViewById(R.id.dc_optiondlayid);
//
//            tv_per_a = (TextView) v.findViewById(R.id.dc_optiona_p_id);
//            tv_per_b = (TextView) v.findViewById(R.id.dc_optionb_p_id);
//            tv_per_c = (TextView) v.findViewById(R.id.dc_optionc_p_id);
//            tv_per_d = (TextView) v.findViewById(R.id.dc_optiond_p_id);
//
//            bar_a = (ProgressBar) v.findViewById(R.id.progress_a);
//            bar_b = (ProgressBar) v.findViewById(R.id.progress_b);
//            bar_c = (ProgressBar) v.findViewById(R.id.progress_c);
//            bar_d = (ProgressBar) v.findViewById(R.id.progress_d);
//
////            setIsRecyclable(false);
//        }
//    }

//    public class ViewHolder8 extends RecyclerView.ViewHolder {
//
//        //  **************************  Set metadata link  ********************
//
//        TextView tv_uname;
//        TextView tv_description;
//        TextView tv_collage;
//        TextView tv_time;
//        TextView tv_reward_count;
//        TextView tv_replies_count;
//        TextView tv_member;
//        TextView tv_likecount;
//        TextView tv_liketext;
//        final TextView tv_comm;
//        TextView tv_groupname;
//        ImageView iv_like;
//        ImageView cimage;
//        TextView tv_share;
//        ImageView iv_save;
//        ImageView iv_delete;
//        CircleImageView userimg;
//        LinearLayout leave;
//        LinearLayout replylay, lay_posttype;
//        LinearLayout like_lay;
//
//
//        public ViewHolder8(View v) {
//            super(v);
//            tv_share = (TextView) v.findViewById(R.id.tv_share);
//            tv_description = (TextView) v.findViewById(R.id.tv_description);
//            tv_uname = (TextView) v.findViewById(R.id.usernameid);
//            tv_collage = (TextView) v.findViewById(R.id.collegeid);
//            tv_time = (TextView) v.findViewById(R.id.timeid);
//            tv_reward_count = (TextView) v.findViewById(R.id.rewardid);
//            tv_replies_count = (TextView) v.findViewById(R.id.tv_commentcount);
//            tv_member = (TextView) v.findViewById(R.id.memberid);
//            tv_likecount = (TextView) v.findViewById(R.id.tv_liketext);
//            tv_liketext = (TextView) v.findViewById(R.id.tv_liketext);
//            tv_comm = (TextView) v.findViewById(R.id.commentid);
//            tv_groupname = (TextView) v.findViewById(R.id.groupnameid);
//            iv_like = (ImageView) v.findViewById(R.id.iv_like);
//            cimage = (ImageView) v.findViewById(R.id.commentimageid);
//            userimg = (CircleImageView) v.findViewById(R.id.userimage_id);
//            leave = (LinearLayout) v.findViewById(R.id.leave_layid);
//            lay_posttype = (LinearLayout) v.findViewById(R.id.lay_posttype);
//            replylay = (LinearLayout) v.findViewById(R.id.replylayid);
//            like_lay = (LinearLayout) v.findViewById(R.id.rewardlayid);
//            iv_save = (ImageView) v.findViewById(R.id.savecomment_id);
//            iv_delete = (ImageView) v.findViewById(R.id.iv_delete);
//        }
//    }

    public void savecontent(Home_getset data, ImageView iv_save) {
        ArrayList<Home_getset> list = dbh.get_savecontent();
        //Logg("list_size",list.size()+" 0");
        if (list.size() < 100) {
            dbh.save_content(data);
            iv_save.setImageResource(R.drawable.ic_bookmark_black_24dp);
//            notifyDataSetChanged();
        } else {
            Toast.makeText(c, "Maximum limit 100", Toast.LENGTH_LONG).show();
        }
    }

//    public void volleylike_comment(final int p) {
//
//        final Home_getset home = (Home_getset) data.get(p);
//
//        RequestQueue queue = Volley.newRequestQueue(c);
//        JSONObject json = new JSONObject();
//        String url = Constants.URL + "newapi2_04/likecomment.php?id=" + home.getId() + "&email=" + Constants.User_Email + "&email1=" + home.getUid();
//
//        //Logg("comment_like",url);
//        JsonObjectRequest jsonreq = new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject res) {
//
//// //Logg("res",String.valueOf(res));
//                // TODO Auto-generated method stub
//
//                try {
//
//                    JSONObject jsonobj1 = new JSONObject(home.getJsonfield());
//
//                    if (res.getString("scalar").equals("like")) {
////                        Toast.makeText(c,"success",Toast.LENGTH_SHORT).show();
//                        int l = home.getLikecount();
//
//                        l++;
//
//                        //Logg("like_value",l+"");
//
//                        home.setLikecount(l);
//                        home.setLikestatus("like");
//
//                        int rew = Integer.valueOf(jsonobj1.getString("userreward"));
//                        rew++;
//
//                        jsonobj1.put("userreward", String.valueOf(rew));
//
//                        home.setJsonfield(jsonobj1 + "");
//
//                        data.set(p, home);
//
////                        notifyItemChanged(p,null);
//
////                        tv_likecount.setText(l+c.getResources().getString(R.string.like));
////
////                        iv_like.setImageResource(R.drawable.ic_bulb_filled);
////                        tv_likecount.setTextColor(c.getResources().getColor(R.color.like_text_color));
////                        tv_reward_count.setText(rew+" Reward");
//
////                        notifyDataSetChanged();
//                    } else if (res.getString("scalar").equals("dislike")) {
////                       0 Toast.makeText(c,"success",Toast.LENGTH_SHORT).show();
//                        int l = home.getLikecount();
//                        if (l > 0)
//                            l--;
//
//                        //Logg("like_value",l+"");
//
//                        home.setLikecount(l);
//                        home.setLikestatus("dislike");
//
//                        int rew = Integer.valueOf(jsonobj1.getString("userreward"));
//                        rew--;
//
//                        jsonobj1.put("userreward", String.valueOf(rew));
//
//                        home.setJsonfield(jsonobj1 + "");
//
////                        notifyItemChanged(p,null);
//
////                        if(l>0) {
////                            tv_likecount.setText(l + c.getResources().getString(R.string.like));
////                        }
////                        else
////                        {
////                            tv_likecount.setText(c.getResources().getString(R.string.like));
////                        }
////                        iv_like.setImageResource(R.drawable.ic_bulb_lightup);
////                        tv_likecount.setTextColor(c.getResources().getColor(R.color.text_lightgray));
////                        tv_reward_count.setText(rew+" Reward");
//
////                        notifyDataSetChanged();
//
//                    } else {
//                        Toast.makeText(c, "Not done", Toast.LENGTH_SHORT).show();
//                    }
//
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError arg) {
//                //Logg("error", String.valueOf(arg));
//
//                Toast.makeText(c, "Network Problem", Toast.LENGTH_SHORT).show();
//            }
//
//        });
//
//        queue.add(jsonreq);
//    }

    public void volleylike_post(final int p) {

        final Home_getset home = (Home_getset) data.get(p);

        RequestQueue queue = Volley.newRequestQueue(c);
        JSONObject json = new JSONObject();
        try {
            json.put("id",home.getId());
            json.put("email",Constants.User_Email);
            json.put("admission_no",Constants.addmissionno);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Constants.URL_LV + "like_post";
        Logg("likeapi",url+" , "+json.toString());
        JsonObjectRequest jsonreq = new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {

                try {

                    if (res.getString("scalar").equals("like")) {
                        int l = home.getLikecount();
                        l++;
                        home.setLikecount(l);
                        home.setLikestatus("like");
                        data.set(p, home);

                    } else if (res.getString("scalar").equals("dislike")) {

                        int l = home.getLikecount();
                        if (l > 0)
                            l--;
                        home.setLikecount(l);
                        home.setLikestatus("dislike");

                        data.set(p, home);
                    } else {
                        Toast.makeText(c, "Not done", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg) {
                Toast.makeText(c, "Network Problem", Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(jsonreq);
    }

//    public void deletcomment(final int a) {
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(c);
//
//        alertDialogBuilder.setMessage("You want to delete this comment");
//        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                volleydeletcomment(a);
//            }
//        });
//
//        alertDialogBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
//
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });
//        alertDialogBuilder.setCancelable(false);
//        alertDialogBuilder.show();
//
//    }
//
//    public void volleydeletcomment(final int a) {
//        final Home_getset home = (Home_getset) data.get(a);
//
//        RequestQueue queue = Volley.newRequestQueue(c);
//
//        JSONObject json = new JSONObject();
//
//        String url = Constants.URL + "newapi2_04/delete_groupcomment.php?id=" + home.getId() + "&pid=" +
//                home.getGroupid() + "&email=" + Constants.User_Email;
//
//        //Logg("url",url);
//
//        JsonObjectRequest jsonreq = new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject res) {
//                //Logg("res",String.valueOf(res));
//                // TODO Auto-generated method stub
//                String s;
//                try {
//                    if (res.getString("scalar").equals("Record deleted successfully") == true) {
//                        Toast.makeText(c, "Comment deleted", Toast.LENGTH_SHORT).show();
//                        data.remove(a);
//                        notifyDataSetChanged();
//                    } else {
//                        Toast.makeText(c, "Try Again", Toast.LENGTH_SHORT).show();
//                    }
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError arg) {
//                //Logg("error", String.valueOf(arg));
//                Toast.makeText(c, "Network Problem", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        queue.add(jsonreq);
//    }

//    public void volleyinsertanswer(final Home_getset data1, final String ans, final int pos) {
//
////        final ProgressDialog progress = ProgressDialog.show(c, null, null, true);
////        progress.setContentView(R.layout.progressdialog);
////        progress.setCanceledOnTouchOutside(false);
////        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
////		progress.show();
//
//        RequestQueue queue = Volley.newRequestQueue(c);
//        JSONObject json = new JSONObject();
//        String url = Constants.URL + "newapi2_04/insert_answer.php?postid=" + data1.getId() + "&answer=" + ans + "&email=" + Constants.User_Email;
//        url = url.replaceAll(" ", "%20");
//        //Logg("answer_url",url);
//        JsonObjectRequest jsonreq = new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject res) {
//// //Logg("res",String.valueOf(res));
//                // TODO Auto-generated method stub
//                String s;
//                try {
//
//                    if (res.getString("status").equals("success") == true) {
//
//                        JSONObject obj = new JSONObject(data1.getJsonfield());
//
//                        obj.put("answerpoll", res.getString("answerpoll"));
//
//                        data1.setJsonfield(obj + "");
//
//                        dbh.postquestion_insert(data1.getId(), ans);
//
//                        data.set(pos, data1);
//
////                        notifyItemChanged(pos);
//
//                    } else if (res.getString("status").equals("error") == true) {
////                        Toast.makeText(c, "Can't Submit Your Answer", Toast.LENGTH_SHORT).show();
//
//                    }
//
////                    progress.dismiss();
//
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
////                    progress.dismiss();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError arg) {
//                //Logg("error", String.valueOf(arg));
////                Toast.makeText(c,"Network Problem", Toast.LENGTH_SHORT).show();
////                progress.dismiss();
//            }
//        });
//
//        queue.add(jsonreq);
//    }

    public void downloadpdf(final String url) {

        Dialog dialog = new Dialog(c);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Rect displayRectangle = new Rect();
        Window window = dialog.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout1 = inflater.inflate(R.layout.stuffdownload, null);
        dialog.setContentView(layout1);

        bar = (NumberProgressBar) dialog.findViewById(R.id.numberbar1);
        dialog.show();
        dialog.setCancelable(true);
        LinearLayout layout = (LinearLayout) dialog.findViewById(R.id.ad_layout);
        progtext = (TextView) dialog.findViewById(R.id.progresstextid);

        tv_openfile = (TextView) dialog.findViewById(R.id.tv_open);

//        AdView adView = new AdView(c);
//        adView.setAdSize(customAdSize);
//        AdRequest adRequest = new AdRequest.Builder().build();
////		 adView.setAdSize(AdSize.BANNER); // ca-app-pub-1199699174096361/2213820534
//        adView.setAdUnitId("ca-app-pub-7412511161357713/9470861589");
//        layout.addView(adView);
//        adView.loadAd(adRequest);

        URL = url;

        String[] urlarr = URL.split("/");
        int len = urlarr.length;
        //Toast.makeText(c,"len "+len,200).show();
        URL = urlarr[len - 1];

        CountDownTimer timer = new CountDownTimer(1 * 4000, 1000) {

            //int m=300000/60;
            //int s=300000/60;

            @TargetApi(Build.VERSION_CODES.GINGERBREAD)
            @SuppressLint("NewApi")
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                new DownloadFile().execute(url);
            }

        }.start();

        tv_openfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                File file = new File(Environment.getExternalStorageDirectory(),
                        "/Gyan Strot/" + URL);
                Uri path = Uri.fromFile(file);
                Intent pdfOpenintent = new Intent(Intent.ACTION_VIEW);
                pdfOpenintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                pdfOpenintent.setDataAndType(path, "application/pdf");
                try {
                    c.startActivity(pdfOpenintent);
                } catch (ActivityNotFoundException e) {

                }
            }
        });

    }

    private class DownloadFile extends AsyncTask<String, Integer, String> {

        String filepath;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            filepath = Environment.getExternalStorageDirectory().getAbsolutePath();
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub

                }
            }, 5000);


        }

        @Override
        protected String doInBackground(String... Url) {
            try {
                java.net.URL url = new URL(Url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();

                // Detect the file lenghth
                int fileLength = connection.getContentLength();

                // Locate storage location

                //.getPath();

                File myDir = new File(filepath + "/Gyan Strot");
                myDir.mkdirs();
                File file = new File(myDir, URL);
                if (file.exists())
                    file.delete();

                // Download the file
                InputStream input = new BufferedInputStream(url.openStream());

                // Save the downloaded file

                OutputStream output = new FileOutputStream(file);

                byte data[] = new byte[1024];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // Publish the progress
                    publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }

                // Close connection
                output.flush();
                output.close();
                input.close();
            } catch (Exception e) {
                // Error Log
                //Logg("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // Update the progress dialog
            bar.setProgress(progress[0]);

            progtext.setText(String.valueOf(progress[0]) + "%");

            // Dismiss the progress dialog
            //mProgressDialog.dismiss();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            tv_openfile.setVisibility(View.VISIBLE);

        }


    }

    private void setTypefaceforPDF(ViewHolder4 vh4) { }

    private String getDate(String OurDate) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(OurDate);
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate = dateFormatter.format(value);
            //Log.d("OurDate", OurDate);
        } catch (Exception e) {
            //Logg("exception_time","e",e);
//            OurDate = "00-00-0000 00:00";
        }
        //Logg("date_time",OurDate);


        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());

        return timediff(OurDate, formattedDate);

    }

    public String timediff(String prevdate, String curdate) {

        java.util.Date d11 = null;
        java.util.Date d2 = null;

        String[] dd1 = prevdate.split(" ");
        String[] date1 = dd1[0].split("-");

        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");

        try {
            d11 = format.parse(prevdate);
            d2 = format.parse(curdate);

            //in milliseconds
            long diff = d2.getTime() - d11.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            String pd = dd1[0];

            String cd;//=cal.get(Calendar.YEAR)+"-"+(cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.DAY_OF_MONTH);
            String[] cdarr = curdate.split(" ");
            cd = cdarr[0];
            String[] date2 = cd.split("-");

            int years = 0, months = 0, days = 0;

            years = Integer.parseInt(date2[0]) - Integer.parseInt(date1[0]);
            months = Integer.parseInt(date2[1]) - Integer.parseInt(date1[1]);
            days = Integer.parseInt(date2[2]) - Integer.parseInt(date1[2]);

            years = (months < 0) ? years - 1 : years;
            months = (months < 0) ? 12 + (months) : months;
            months = (days < 0) ? months - 1 : months;
            days = (days < 0) ? 30 + days : days;

            //	//Logg("di time= "," hrs= "+diffHours+" min= "+diffMinutes+" "+" years=  "+years+" month=  "+months+" days= "+days+" pd= "+pd+" cd= "+cd);

            if (years > 0) {
                return (String.valueOf(years) + " Years ago");
            } else if (months > 0 & years < 1) {

                return (String.valueOf(months) + " Months ago");
                //newsdateedit.putString("postdate"+i,String.valueOf(months)+"Months ago");
            } else if (days > 0 & months < 1) {
                return (String.valueOf(days) + " Days ago");
                //newsdateedit.putString("postdate"+i,String.valueOf(months)+" Days ago");
            } else if (diffHours > 0 & days < 1) {
                return (String.valueOf(diffHours) + " Hours ago");
                //newsdateedit.putString("postdate"+i,String.valueOf(months)+" Hours ago");
            } else if (diffMinutes > 0 & diffHours < 1) {
                return (String.valueOf(diffMinutes) + " Minutes ago");
                //newsdateedit.putString("postdate"+i,String.valueOf(months)+" Minutes ago");
            } else if (diffSeconds > 0 & diffMinutes < 1) {
                return ("0 Minutes ago");
                //newsdateedit.putString("postdate"+i,"0 Minutes ago");
            }

        } catch (Exception e) {
            e.printStackTrace();
            ////Logg("di time error= ",e+"");
        }
        return ("1 Minutes ago");
    }

//    public void getBanner(String email, final int size)
//    {
//
//        RequestQueue queue= Volley.newRequestQueue(c);
//        final JSONObject obj=new JSONObject();
//        String url= Constants.URL+"v4/get_banner_location.php?email="+ email;
//
//        Logg("get_banner",url);
//
//        JsonObjectRequest json=new JsonObjectRequest(Request.Method.POST, url, obj,new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject res) {
//                // TODO Auto-generated method stub
//                try {
//

//                        }
//
//                        setBanner(size);
//
////                        if(banner_array.length()>0) {
////                            Home_getset obj = new Home_getset();
////
////                            obj.setFieldtype("12");
////
////                            if(data.size()>5) {
////                                data.add(1, obj);
////                                data.add(5, obj);
//////                                notifyItemChanged(5);
////                                notifyDataSetChanged();
////                            }
////                            else if(data.size()>1) {
////                                data.add(1, obj);
//////                                notifyItemChanged(1);
////                                notifyDataSetChanged();
////                            }
////                        }
//                    }
//
//                }
//                catch (Exception e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                    Logg("getbanner exc", e+"");
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError arg0) {
//                // TODO Auto-generated method stub
//                Logg("getbanner error", String.valueOf(arg0));
//
//            }
//        });
//        queue.add(json);
//    }

//    public void setBanner(int size){
//
//        Log.e("call","setBanner");
//
//        if(banner_array.length()>0){
//            Log.e("call","setBanner "+banner_array.length()+" "+banner_position.length());
//            for(int i=0;i<banner_position.length();i++){
//                try {
//                    JSONObject object=banner_position.getJSONObject(i);
//
//                    Log.e("call","setBanner "+object);
//
//                    if(object.getInt("position")<=size){
//                        if(!object.getBoolean("status")){
//
//
//                            Log.e("datasize1",data.size()+"");
//                            Home_getset obj = new Home_getset();
//                            obj.setFieldtype("12");
//                            data.add(object.getInt("position"),obj);
//
//                            Log.e("datasize2",data.size()+"");
//
////                            notifyItemChanged(object.getInt("position"));
//                        }
//                    }
//                    else
//                        break;
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            notifyDataSetChanged();
////            notifyItemRangeChanged(1,data.size());
//
////    notifyDataSetChanged();
//            Log.e("datasize3",data.size()+"");
//
//
//        }
//    }

}