package com.scholar.engineering.banking.ssc.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;

import androidx.fragment.app.Fragment;
import androidx.core.view.MenuItemCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.scholar.engineering.banking.ssc.HomeActivity;
import com.scholar.engineering.banking.ssc.Post_comment_ingroup;
import com.scholar.engineering.banking.ssc.Post_link_in_group;
import com.scholar.engineering.banking.ssc.Post_question;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Share_and_Earn;
import com.scholar.engineering.banking.ssc.adapter.Home_RecyclerViewAdapter2;
import com.scholar.engineering.banking.ssc.getset.HomeBean;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.getset.RetrofitObjectAPI;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.DatabaseHandler;
import com.scholar.engineering.banking.ssc.utils.EndlessRecyclerViewScrollListener;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Home_fragment extends Fragment implements OnClickListener {

    ArrayList<Home_getset> homedata;
    ArrayList<String> homeid;

    DatabaseHandler dbh;
    int searchlength = 0;
    RecyclerView homelist;
    public static Home_RecyclerViewAdapter2 adapter;
    SwipeRefreshLayout swipeRefreshLayout;
    int index = 0;
    String st_searchtext = "";
    Boolean aBoolean = true;
    Boolean isRunning = true;

    NetworkConnection nw;

    EndlessRecyclerViewScrollListener endless;
    HomeActivity activity;
    LinearLayoutManager mLayoutManager;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (HomeActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.home_fragment, container, false);

        Logg("oncreatehome", "11");
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        nw = new NetworkConnection(activity);
        setHasOptionsMenu(true);

        homeid = new ArrayList<>();
        homedata = new ArrayList<>();
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        homelist = (RecyclerView) view.findViewById(R.id.recyclerView_homefrag);
        mLayoutManager = new LinearLayoutManager(activity);
        homelist.setLayoutManager(mLayoutManager);
        endless = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                Logg("homefragmentpage", index + " , " + homedata.size());
                if ((homedata.size()-1) % 10 == 0) {
                    index++;
                    getData("1");
                }
            }
        };
        homelist.addOnScrollListener(endless);

        dbh = new DatabaseHandler(activity);
        dbh.databaseopen();
        homedata = dbh.get_offlinecontent();
//        homedata.add(0, null);
        dbh.databaseclose();

        homelist.setItemViewCacheSize(9);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                index = 0;
                homedata = new ArrayList<>();
                if (aBoolean)
                    getData("2");
//                else
//                    getSearch_Data(st_searchtext);

                endless = new EndlessRecyclerViewScrollListener(mLayoutManager) {
                    @Override
                    public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                        Logg("homefragmentpage", index + "");
                        if ((homedata.size()-1) % 10 == 0) {
                            index++;
                            getData("1");
                        }
                    }
                };
                homelist.addOnScrollListener(endless);

            }
        });

//		getBanner();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        Constants.Current_Activity = 100;
        Logg("Constants.checkrefresh", Constants.checkrefresh.toString());
        if (Constants.checkrefresh) {
            endless.resetState();
            index = 0;
            getData("3");
            Constants.checkrefresh = false;
        }
        getData("1");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.shareid:

                Intent in = new Intent(activity, Share_and_Earn.class);
                startActivity(in);

                break;

        }

        Drawable drawable = item.getIcon();
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).start();
        }

        return super.onOptionsItemSelected(item);
    }

    SearchView searchView;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

//        inflater.inflate(R.menu.menu, menu);
//        MenuItem item = menu.findItem(R.id.search);
//
//        MenuItem playMenu = menu.findItem(R.id.shareid);
//
//        if (Constants.Width <= 600)
//            playMenu.setIcon(R.drawable.dollar);
//        else
//            playMenu.setIcon(R.drawable.dollar);
//
//        searchView = new SearchView(((HomeActivity) activity).getSupportActionBar().getThemedContext());
//
//        searchView.setQueryHint("Search Post");
//
//        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
//        MenuItemCompat.setActionView(item, searchView);
//
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                searchlength = query.length();
//
//                homedata.clear();
//                //index=0;
//                st_searchtext = query;
//                getSearch_Data(st_searchtext);
//
//                InputMethodManager in = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
//                in.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
//
//                return true;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//
//                if (searchlength > 0)
//                    if (newText.length() < 1) {
//                        homedata.clear();
//                        getData("4");
//                    }
//
//                return true;
//            }
//        });
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public void internetconnection() {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                index = 0;
                swipeRefreshLayout.setRefreshing(true);
                dialog.dismiss();
                index = 0;
                getData("5");

            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    private void getData(String from) {

        Logg("calltype", from);

        deleteCache(activity);
        if (!nw.isConnectingToInternet()) {

            if (Constants.viewpagercurrentitem == 0) {
                swipeRefreshLayout.setRefreshing(false);
                internetconnection();
            }

            return;
        }
        if (Constants.checkhome) {
            swipeRefreshLayout.setRefreshing(true);
        }

        aBoolean = true;

        Constants.checkhome = false;

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<HomeBean> call = service.getHomeGetsetCall(Constants.User_Email, Constants.addmissionno, index);

        Logg("urlapihomefragment", Constants.URL_LV + "homedata?email=" +
                Constants.User_Email + "&admission_no=" + Constants.addmissionno + "&index=" + index);

        call.enqueue(new Callback<HomeBean>() {

            @Override
            public void onResponse(Call<HomeBean> call, retrofit2.Response<HomeBean> response) {
                Logg("call_on_response", "on_response " + response.body().getData());
                swipeRefreshLayout.setRefreshing(false);


                if (index == 0) {
                    homedata.clear();

                    if (response.body() != null)
                        if (response.body().getData() != null)
                            if (response.body().getData().size() > 0)
                                dbh.delete_offlinecontent(0);

                    homedata.add(0, null);
                }
                dbh.databaseopen();
                try {
                    Logg("homedatasize0", response.body().getData().size() + "");
                    for (int i = 0; i < response.body().getData().size(); i++) {
                        Home_getset home = new Home_getset();
                        home = response.body().getData().get(i);
                        homeid.add(home.getId() + "");
                        homedata.add(home);
                        dbh.offline_content(home);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }

                dbh.databaseclose();
                adapter = new Home_RecyclerViewAdapter2(activity, homedata, "homefragment");
                homelist.setAdapter(adapter);
                Logg("homedatasize1", homedata.size() + "");

//                } else {

//                    Logg("homedatasize2", response.body().getData().size() + "");
//                    listSizemain=listSizemain+response.body().getData().size();
//                    dbh.databaseopen();
//                    for (int i = 0; i < response.body().getData().size(); i++) {
//                        Home_getset home = new Home_getset();
//                        home = response.body().getData().get(i);
//                        homeid.add(home.getId() + "");
//                        homedata.add(home);
//                        dbh.offline_content(home);
//                    }
//                    dbh.databaseclose();
//                    adapter.addItme(homedata);
//                    adapter.notifyDataSetChanged();
//
//                    Logg("homedatasize3", homedata.size() + "");
//                }

//				setBanner(adapter.getItemCount());
                isRunning = true;
            }

            @Override
            public void onFailure(Call<HomeBean> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                isRunning = true;
            }
        });
    }

    private String getDate(String OurDate) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(OurDate);
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate = dateFormatter.format(value);
            //Log.d("OurDate", OurDate);
        } catch (Exception e) {
            //Logg("exception_time","e",e);
//            OurDate = "00-00-0000 00:00";
        }
        //Logg("date_time",OurDate);


        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());

        return timediff(OurDate, formattedDate);

    }

    public String timediff(String prevdate, String curdate) {

        java.util.Date d11 = null;
        java.util.Date d2 = null;

        String[] dd1 = prevdate.split(" ");
        String[] date1 = dd1[0].split("-");

        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");

        try {
            d11 = format.parse(prevdate);
            d2 = format.parse(curdate);

            //in milliseconds
            long diff = d2.getTime() - d11.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            String pd = dd1[0];

            String cd;//=cal.get(Calendar.YEAR)+"-"+(cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.DAY_OF_MONTH);
            String[] cdarr = curdate.split(" ");
            cd = cdarr[0];
            String[] date2 = cd.split("-");

            int years = 0, months = 0, days = 0;

            years = Integer.parseInt(date2[0]) - Integer.parseInt(date1[0]);
            months = Integer.parseInt(date2[1]) - Integer.parseInt(date1[1]);
            days = Integer.parseInt(date2[2]) - Integer.parseInt(date1[2]);

            years = (months < 0) ? years - 1 : years;
            months = (months < 0) ? 12 + (months) : months;
            months = (days < 0) ? months - 1 : months;
            days = (days < 0) ? 30 + days : days;

            //	//Logg("di time= "," hrs= "+diffHours+" min= "+diffMinutes+" "+" years=  "+years+" month=  "+months+" days= "+days+" pd= "+pd+" cd= "+cd);

            if (years > 0) {
                return (String.valueOf(years) + " Years ago");
            } else if (months > 0 & years < 1) {

                return (String.valueOf(months) + " Months ago");
                //newsdateedit.putString("postdate"+i,String.valueOf(months)+"Months ago");
            } else if (days > 0 & months < 1) {
                return (String.valueOf(days) + " Days ago");
                //newsdateedit.putString("postdate"+i,String.valueOf(months)+" Days ago");
            } else if (diffHours > 0 & days < 1) {
                return (String.valueOf(diffHours) + " Hours ago");
                //newsdateedit.putString("postdate"+i,String.valueOf(months)+" Hours ago");
            } else if (diffMinutes > 0 & diffHours < 1) {
                return (String.valueOf(diffMinutes) + " Minutes ago");
                //newsdateedit.putString("postdate"+i,String.valueOf(months)+" Minutes ago");
            } else if (diffSeconds > 0 & diffMinutes < 1) {
                return ("0 Minutes ago");
                //newsdateedit.putString("postdate"+i,"0 Minutes ago");
            }

        } catch (Exception e) {
            e.printStackTrace();
            ////Logg("di time error= ",e+"");
        }
        return ("1 Minutes ago");
    }

//    public void getSearch_Data(String text) {
//        aBoolean = false;
//        RequestQueue queue = Volley.newRequestQueue(activity);
//        JSONObject obj = new JSONObject();
//        String url = Constants.URL + "newapi2_04/get_searchdata.php?email=" + Constants.User_Email + "&index=" + index + "&text=" + text;
//        Logg("search_url", url);
//
//        JsonObjectRequest json = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject res) {
//                swipeRefreshLayout.setRefreshing(false);
//                try {
//
//                    if (res.has("data")) {
//                        JSONArray jr = res.getJSONArray("data");
//
//                        if (homedata.size() > 0)
//                            homedata.clear();
////						homedata.add(0,null);
//
//                        for (int i = 0; i < jr.length(); i++) {
//                            JSONObject ob = jr.getJSONObject(i);
//
//                            String time = getDate(ob.getString("timestamp"));
//
//                            Home_getset object = new Home_getset();
//                            object.setId(ob.getInt("id"));
//                            object.setTimestamp(ob.getString("timestamp"));
//                            object.setLikecount(ob.getInt("likes"));
//                            object.setCommentcount(ob.getInt("comment"));
//                            object.setViewcount(ob.getInt("view"));
//                            object.setUid(ob.getString("uid"));
//                            object.setPosttype(ob.getString("posttype"));
//                            object.setGroupid(ob.getString("groupid"));
//                            object.setFieldtype(ob.getString("field_type"));
//                            object.setJsonfield(ob.getString("jsondata"));
//                            object.setLikestatus(ob.getString("likestatus"));
//                            object.setPosturl(ob.getString("posturl"));
//                            object.setPostdescription(ob.getString("post_description"));
//                            if (ob.has("AppVersion"))
//                                object.setAppVersion(ob.getString("AppVersion"));
//
//                            Logg("fieldtype111133", object.getFieldtype());
//                            homedata.add(object);
//                        }
//
//                        adapter = new Home_RecyclerViewAdapter2(activity, homedata, "homefragment");
//                        homelist.setAdapter(adapter);
//                    }
//                    isRunning = true;
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError arg0) {
//                swipeRefreshLayout.setRefreshing(false);
//            }
//        });
//        queue.add(json);
//    }

    @Override
    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.post:
//                Intent in = new Intent(activity, Post_comment_ingroup.class);
//                in.putExtra("groupname", "null");
//                in.putExtra("id", 0);
//                startActivity(in);
//                break;
//            case R.id.link:
//                Intent in1 = new Intent(activity, Post_link_in_group.class);
//                in1.putExtra("groupname", "null");
//                in1.putExtra("id", 0);
//                startActivity(in1);
//                break;
//            case R.id.question:
//                Intent in2 = new Intent(activity, Post_question.class);
//                in2.putExtra("groupname", "null");
//                in2.putExtra("id", 0);
//                startActivity(in2);
//                break;
//        }
    }

//    JSONArray banner_array, banner_position;
//	public void getBanner()
//	{
//
//		RequestQueue queue= Volley.newRequestQueue(activity);
//		final JSONObject obj=new JSONObject();
//		String url= Constants.URL+"v4/get_banner_location.php?email="+ Constants.User_Email;
//		Logg("get_banner",url);
//
//		JsonObjectRequest json=new JsonObjectRequest(Request.Method.POST, url, obj,new Response.Listener<JSONObject>() {
//
//			@Override
//			public void onResponse(JSONObject res) {
//				try {
//					Logg("getbanner res",res+"");
//
//					if(res.has("data")) {
//						banner_position=new JSONArray();
//
//						banner_array = res.getJSONArray("data");
//
//						JSONArray b_pos=new JSONArray(res.getString("banner_position"));
//
//                        Logg("banner_position",b_pos+" "+b_pos.length());
//
//						for(int i=0;i<b_pos.length();i++){
//							JSONObject object=new JSONObject();
//							object.put("position",b_pos.getInt(i));
//							object.put("status",false);
//							banner_position.put(object);
//						}
//
//						if(adapter!=null)
//							setBanner(adapter.getItemCount());
//
////                        if(banner_array.length()>0) {
////                            Home_getset obj = new Home_getset();
////
////                            obj.setFieldtype("12");
////
////                            if(data.size()>5) {
////                                data.add(1, obj);
////                                data.add(5, obj);
//////                                notifyItemChanged(5);
////                                notifyDataSetChanged();
////                            }
////                            else if(data.size()>1) {
////                                data.add(1, obj);
//////                                notifyItemChanged(1);
////                                notifyDataSetChanged();
////                            }
////                        }
//					}
//
//				}
//				catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//					Logg("getbanner exc", e+"");
//				}
//			}
//		}, new Response.ErrorListener() {
//
//			@Override
//			public void onErrorResponse(VolleyError arg0) {
//				// TODO Auto-generated method stub
//				Logg("getbanner error", String.valueOf(arg0));
//
//			}
//		});
//		queue.add(json);
//	}
//
//	public void setBanner(int size){
//
//		if(banner_array!=null)
//			if(banner_array.length()>0){
//                Logg("call","setBanner "+banner_array.length()+" "+banner_position.length());
//				for(int i=0;i<banner_position.length();i++){
//					try {
//						JSONObject object=banner_position.getJSONObject(i);
//
//                        Logg("call","setBanner "+object);
//
//						if(object.getInt("position")<=size){
//							if(!object.getBoolean("status")){
//								object.put("status",true);
//								banner_position.put(i,object);
//								Home_getset obj = new Home_getset();
//								obj.setFieldtype("12");
//
//								adapter.addSingleItem(object.getInt("position"),obj,banner_array);
//								Logg("inserted_item",object.getInt("position")+"");
////							adapter.notifyItemInserted(object.getInt("position"));
//
////							data.add(object.getInt("position"),obj);
//
//
////                            notifyItemInserted(object.getInt("position"));
//							}
//						}
//						else
//							break;
//
//					} catch (JSONException e) {
//						e.printStackTrace();
//					}
//				}
//
////            notifyItemRangeChanged(1,data.size());
//
////			adapter.notifyDataSetChanged();
//
//			}
//	}
//

}