package com.scholar.engineering.banking.ssc.newScreens.QueryForms

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.RatingBar.OnRatingBarChangeListener
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.RetryPolicy
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.scholar.engineering.banking.ssc.R
import com.scholar.engineering.banking.ssc.newScreens.QueryForms.RounderBorderDialog.RoundedBottomSheetDialogFragment
import com.scholar.engineering.banking.ssc.utils.CommonUtils
import com.scholar.engineering.banking.ssc.utils.Constants
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class Feedbackform_BottomSheetDialog: RoundedBottomSheetDialogFragment(){


//    lateinit var list:ArrayList<CityModel>
    var page:Int=1
    lateinit var view1:View
    lateinit var pref: UserSharedPreferences
    lateinit var type:String
    var numStars:Float=0F
    lateinit var etfeedback:EditText
    lateinit var bundle: Bundle
    lateinit var rat:RatingBar
    lateinit var submit:Button
    lateinit var close:ImageView
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view= inflater.inflate(R.layout.feedbackform, container, false)
        view1=view
        bundle=arguments!!
        etfeedback=view.findViewById<EditText>(R.id.etfeedback)
        rat=view.findViewById<RatingBar>(R.id.rBar)
        rat.onRatingBarChangeListener = OnRatingBarChangeListener { ratingBar, rating, fromUser ->
            numStars = rating
            Log.e("numstars",numStars.toString()+"0000")
        }
        submit=view.findViewById<Button>(R.id.submit)
        submit.setOnClickListener {
            if(numStars!=0F)
                sendQuery()
            else
                Toast.makeText(context,"Please give rating", Toast.LENGTH_SHORT).show()
        }
        close=view.findViewById<ImageView>(R.id.close)
        close.setOnClickListener{
            dismiss()
        }
        return  view;
    }

    var pd: ProgressDialog? = null
    private fun sendQuery() {
        pd = ProgressDialog(context)
        pd?.setMessage("loading")
        pd?.setCancelable(false)
        pd!!.show()
        val requestQueue = Volley.newRequestQueue(context)
        val stringRequest: StringRequest = object : StringRequest(Method.POST, Constants.URL_LV+"addfeedback",
                Response.Listener { response ->
                    pd!!.dismiss()
                    Log.e("response",response)
                    try {
                        val dataobject = JSONObject(response)
                        Toast.makeText(context,dataobject.getString("message"), Toast.LENGTH_SHORT).show()
                        dismiss()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        pd!!.dismiss()
                    }
                },
                Response.ErrorListener { error ->
                    CommonUtils.volleyerror(error)
                    pd!!.dismiss()
                }
        ) {
            override fun getParams(): Map<String, String> {
                val params: MutableMap<String, String> = HashMap()
                params["id"] = bundle.getString("id")!!
                params["rating"] = numStars.toString()
                params["comment"] = etfeedback.text.toString()
                Log.e("get_assignments", params.toString() + "")
                return params
            }
        }
        stringRequest.setShouldCache(false)

        val socketTimeout = 0
        val policy: RetryPolicy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.setRetryPolicy(policy)

        requestQueue.add(stringRequest)
    }
}