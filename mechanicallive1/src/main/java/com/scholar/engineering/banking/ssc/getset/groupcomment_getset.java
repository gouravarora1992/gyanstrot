package com.scholar.engineering.banking.ssc.getset;

/**
 * Created by surender on 02-04-2016.
 */
public class groupcomment_getset  {

    int id,psid,like;

    String groupname,comment,name,college,reward,member,reply,date,image,email,type,cimage,timestamp,likestatus;

    public groupcomment_getset(int id,String timestamp,String likestatus,int like, String groupname, String comment, String name, String college, String reward, String member, String reply, String date, String image,String email,int psid,String cimage, String type) {
        this.id = id;
        this.timestamp=timestamp;
        this.likestatus=likestatus;
        this.like=like;
        this.groupname = groupname;
        this.comment = comment;
        this.name = name;
        this.college = college;
        this.reward = reward;
        this.member = member;
        this.reply = reply;
        this.date = date;
        this.image = image;
        this.email=email;
        this.psid=psid;
        this.cimage=cimage;
        this.type = type;
    }

    public String getLikestatus() {
        return likestatus;
    }

    public void setLikestatus(String likestatus) {
        this.likestatus = likestatus;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getCimage() {
        return cimage;
    }

    public void setCimage(String cimage) {
        this.cimage = cimage;
    }

    public int getPsid() {
        return psid;
    }

    public void setPsid(int psid) {
        this.psid = psid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
