package com.scholar.engineering.banking.ssc.getset;

/**
 * Created by surender on 2/22/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Home_getset {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("likes")
    @Expose
    private int likecount;
    @SerializedName("comment")
    @Expose
    private int commentcount;
    @SerializedName("view")
    @Expose
    private int viewcount;
    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("posttype")
    @Expose
    private String posttype;
    @SerializedName("groupid")
    @Expose
    private String groupid;
    @SerializedName("field_type")
    @Expose
    private String fieldtype;
    @SerializedName("jsondata")
    @Expose
    private String jsonfield;
    @SerializedName("likestatus")
    @Expose
    private String likestatus;
    @SerializedName("post_description")
    @Expose
    private String postdescription;

    @SerializedName("posturl")
    @Expose
    private String posturl;

    @SerializedName("appversion")
    @Expose
    private String AppVersion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getFieldtype() {
        return fieldtype;
    }

    public void setFieldtype(String fieldtype) {
        this.fieldtype = fieldtype;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public int getLikecount() {
        return likecount;
    }

    public void setLikecount(int likecount) {
        this.likecount = likecount;
    }

    public int getCommentcount() {
        return commentcount;
    }

    public void setCommentcount(int commentcount) {
        this.commentcount = commentcount;
    }

    public int getViewcount() {
        return viewcount;
    }

    public void setViewcount(int viewcount) {
        this.viewcount = viewcount;
    }

    public String getPosttype() {
        return posttype;
    }

    public void setPosttype(String posttype) {
        this.posttype = posttype;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPosturl() {
        return posturl;
    }

    public void setPosturl(String posturl) {
        this.posturl = posturl;
    }

    public String getPostdescription() {
        return postdescription;
    }

    public void setPostdescription(String postdescription) {
        this.postdescription = postdescription;
    }

    public String getJsonfield() {
        return jsonfield;
    }

    public void setJsonfield(String jsonfield) {
        this.jsonfield = jsonfield;
    }

    public String getLikestatus() {
        return likestatus;
    }

    public void setLikestatus(String likestatus) {
        this.likestatus = likestatus;
    }

    public String getAppVersion() {
        return AppVersion;
    }

    public void setAppVersion(String appVersion) {
        AppVersion = appVersion;
    }
}