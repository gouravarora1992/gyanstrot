package com.scholar.engineering.banking.ssc.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.scholar.engineering.banking.ssc.fragment.FourthView;
import com.scholar.engineering.banking.ssc.fragment.SecondView;
import com.scholar.engineering.banking.ssc.fragment.ThirdView;


public class PagerAdapter_circleindication extends FragmentPagerAdapter {

	public PagerAdapter_circleindication(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int arg0) {

		switch (arg0) {
		case 0:

//			tv_title.setText("Online Tests");
//			tv_descrition.setText("Know your rank & score");
			return new ThirdView();
//
		case 1:
//			tv_title.setText("Study Material");
//            tv_descrition.setText("Free Latest Notes");
			return new SecondView();
		case 2:
//	tv_title.setText("Discussion Groups");
//			tv_descrition.setText("Let's help each other");
			return new FourthView();
		default:
			break;
		}
		return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 3;
	}

}
