package com.scholar.engineering.banking.ssc.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

/**
 * Created by surender on 8/14/2017.
 */

public class ImageCompression extends AsyncTask<String, Void, String> {

    ImageView imageView;

    public ImageCompression(ImageView imageView) {
        this.imageView = imageView;
    }

    @Override
    protected String doInBackground(String... strings) {
        if (strings.length == 0 || strings[0] == null)

            return null;

        Logg("strings[0]",strings[0]+"");

        return CommonUtils.compressImage(strings[0]);
    }

    protected void onPostExecute(String imagePath) {
        // imagePath is path of new compressed image.
        Bitmap bmp= BitmapFactory.decodeFile(new File(imagePath).getAbsolutePath());
        //Logg("bitmap_size",bmp.getByteCount()+" w "+bmp.getWidth()+" h "+bmp.getHeight());
        imageView.setImageBitmap(bmp);
    }
}
