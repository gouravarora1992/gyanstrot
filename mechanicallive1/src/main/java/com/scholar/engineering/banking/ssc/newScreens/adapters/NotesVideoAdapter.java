package com.scholar.engineering.banking.ssc.newScreens.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.newScreens.NotesPDF;
import com.scholar.engineering.banking.ssc.newScreens.NotesVideos;
import com.scholar.engineering.banking.ssc.newScreens.Youtube;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import java.util.ArrayList;

public class NotesVideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context cs;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    ImageLoader imageLoader;
    ArrayList data;
    UserSharedPreferences preferences;

    public NotesVideoAdapter(Context cs, ArrayList list) {
        this.cs = cs;
        this.data = list;

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(cs));
        preferences=UserSharedPreferences.getInstance(cs);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.blankimage)
                .showImageForEmptyUri(R.drawable.congrrr)
                .showImageOnFail(R.drawable.congrrr)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View v0 = inflater.inflate(R.layout.notesvideoitem, viewGroup, false);
        viewHolder = new ViewHolder0(v0);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        ViewHolder0 vh0 = (ViewHolder0) viewHolder;

        setData(vh0, position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(ViewHolder0 v, final int p) {


            Glide.with(cs).load(cs.getResources().getDrawable(R.drawable.ic_folder)).load(v.iv_banner);
            v.tvname.setText(data.get(p).toString());
            v.classec.setVisibility(View.GONE);
            v.tvselected.setVisibility(View.GONE);
            v.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(p==0)
                    {
                        cs.startActivity(new Intent(cs, NotesVideos.class));
                    }
                    else if(p==1)
                    {
                        cs.startActivity(new Intent(cs, NotesPDF.class).putExtra("type","assigments"));
                    }
                    else if(p==2)
                    {
                        cs.startActivity(new Intent(cs, NotesPDF.class).putExtra("type","notes"));
                    }
                }
            });

    }

    public class ViewHolder0 extends RecyclerView.ViewHolder {

        ImageView iv_banner,tvselected;
        TextView tvname,classec;

        public ViewHolder0(View bn) {
            super(bn);
            iv_banner = (ImageView) bn.findViewById(R.id.iv_banner);
            tvname=bn.findViewById(R.id.tvname);
            classec=bn.findViewById(R.id.classec);
            tvselected=bn.findViewById(R.id.tvselected);
        }
    }

}
