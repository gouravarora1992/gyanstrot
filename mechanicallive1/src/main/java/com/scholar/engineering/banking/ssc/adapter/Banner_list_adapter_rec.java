package com.scholar.engineering.banking.ssc.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.StrictMode;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

/**
 * Created by surender on 3/14/2017.
*/

public class Banner_list_adapter_rec extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context cs;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    ImageLoader imageLoader;
    JSONArray data;
    JSONObject object;
    String clas;

    public Banner_list_adapter_rec(Context cs, JSONArray data, String clas) {
        this.cs=cs;
        this.data=data;
        this.clas=clas;

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(cs));

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.blankimage)
                .showImageForEmptyUri(R.drawable.congrrr)
                .showImageOnFail(R.drawable.congrrr)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

                View v0 = inflater.inflate(R.layout.banner_itme_rec, viewGroup, false);

                viewHolder = new ViewHolder0(v0);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

                ViewHolder0 vh0 = (ViewHolder0) viewHolder;

                setData(vh0, position);
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    public void setData(ViewHolder0 v, final int p){

        try {

            object=data.getJSONObject(p);

            Logg("if1",Constants.URL_Image+"banner_image/"+object.getString("image"));
            if(object.getString("image").length()>4) {
                Logg("if",Constants.URL_Image+"banner_image/"+object.getString("image"));
                imageLoader.getInstance().displayImage(Constants.URL_Image + "banner_image/" + object.getString("image"), v.iv_banner, options, animateFirstListener);

                //                Glide.with(cs).load("http://hellotopper.com/mechanicalinsider/banner_image/9428733.jpg").into(v.iv_banner);

                v.iv_banner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {

                            if(data.getJSONObject(p).getString("link").length()>5) {
                            if(URLIsReachable(data.getJSONObject(p).getString("link"))) {

                                bannerView(data.getJSONObject(p).getString("id"));

                                Intent myWebLink = new Intent(Intent.ACTION_VIEW);
                                //                          myWebLink.setComponent(new ComponentName("com.android.browser", "com.android.browser.BrowserActivity"));
                                myWebLink.setData(Uri.parse(data.getJSONObject(p).getString("link")));
                                cs.startActivity(myWebLink);
                            }
                            else{
                                Logg("url","not valid url");
                            }

                            }
                        } catch (JSONException e) {

                        }
                    }
                });
            }



        } catch (JSONException e) {

        }

    }

    public class ViewHolder0 extends RecyclerView.ViewHolder {

        ImageView iv_banner;

        public ViewHolder0(View bn) {
            super(bn);
            iv_banner=(ImageView)bn.findViewById(R.id.iv_banner);
        }
    }


    public boolean URLIsReachable(String urlString)
    {

        try
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url
                    .openConnection();
            int responseCode = urlConnection.getResponseCode();
            urlConnection.disconnect();
            // Toast.makeText(c,"code= "+responseCode,Toast.LENGTH_LONG).show();
            if(responseCode==200)
                return true;
            else
                return false;
        } catch (MalformedURLException e)
        {
            return false;
        } catch (IOException e)
        {
            return false;
        }
    }


    public void bannerView(final String id) {

        RequestQueue queue = Volley.newRequestQueue(cs);
        String url="";

        url = Constants.URL_LV +"banner_views";

        url = url.replace(" ", "%20");

        Logg("name", url);

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                Logg("response", s);
                try {
                    if(s.length()>0)
                    {
                        JSONObject res=new JSONObject(s);

                    }
                }

                catch (Exception e)
                {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(cs,CommonUtils.volleyerror(volleyError));
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("userid", Constants.User_Email);
                params.put("pid",id);

                Logg("banner_views",params.toString());

                return params;

            }
        };

        int socketTimeout = 10000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

}
