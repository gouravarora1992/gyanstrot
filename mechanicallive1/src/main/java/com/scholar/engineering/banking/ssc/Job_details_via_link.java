package com.scholar.engineering.banking.ssc;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

/**
 * Created by surender on 4/19/2017.
 */
public class Job_details_via_link extends AppCompatActivity {

//    TextView tv_title,tv_postname,tv_commentcount,tv_like,tv_share;
//    LinearLayout  lay_commentpost,lay_like,lay_comment,lay_share;
//    WebView wv;
//    int postid,likecount=0,commentcount=0;
//    ImageView iv_job,iv_like;
//    String username,collag,email,imgurl;
//    JSONObject object;
//    ProgressDialog progress;
//    boolean b=false;
//
//    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
//    private DisplayImageOptions options;
//
//    NetworkConnection nw;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.job_details_via_link);
//
//        nw=new NetworkConnection(this);
//
//        wv=(WebView)findViewById(R.id.webview);
//        tv_title=(TextView)findViewById(R.id.tv_jobtitle);
//        tv_postname=(TextView)findViewById(R.id.tv_postname);
//        iv_job=(ImageView)findViewById(R.id.iv_jobimage);
//
//        iv_like=(ImageView)findViewById(R.id.likeimageid);
//
//        tv_commentcount=(TextView)findViewById(R.id.tv_commentid);
//
//        tv_like=(TextView)findViewById(R.id.tv_like);
//
//        lay_comment=(LinearLayout)findViewById(R.id.lay_comment);
//        lay_like=(LinearLayout)findViewById(R.id.lay_like);
//        lay_share=(LinearLayout)findViewById(R.id.lay_shareid);
//        lay_commentpost=(LinearLayout)findViewById(R.id.lay_commentpost);
//
//        wv.getSettings().setJavaScriptEnabled(true);
//
//        wv.setWebViewClient(new Callback());
//
//        options = new DisplayImageOptions.Builder()
//                .showImageOnLoading(R.drawable.blankimage)
//                .showImageForEmptyUri(R.drawable.congrrr)
//                .showImageOnFail(R.drawable.congrrr)
//                .cacheInMemory(false)
//                .cacheOnDisk(true)
//                .considerExifParams(true)
//                .build();
//
//        postid=getIntent().getIntExtra("postid",0);
//
////        pref =getSharedPreferences("myref", 0);
////        username=pref.getString("name", "");
////        collag=pref.getString("collage", "");
////        imgurl=pref.getString("image","");
////        email=User_Email;
////        b=pref.getBoolean("boolean", false);
//
//        get_Post(postid);
//
//
//        lay_like.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(!nw.isConnectingToInternet()) {
//                    Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
//                    return;
//                }
//                if(pref.getBoolean("boolean",false))
//                    volleylike_post();
//                else{
//                    Intent in = new Intent(Job_details_via_link.this, SplashScreen.class);
//                    startActivity(in);
//                    finish();
//                }
//
////                volleylike_post();
//            }
//        });
//
//        lay_comment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if(pref.getBoolean("boolean",false))
//                {
//                    Intent in = new Intent(Job_details_via_link.this, Comment_Common_homepage.class);
//                    in.putExtra("posttype","3");
//                    in.putExtra("postid",postid);
//                    startActivity(in);
//
//                }
//                else{
//                    Intent in = new Intent(Job_details_via_link.this, SplashScreen.class);
//                    startActivity(in);
//                    finish();
//                }
//
//            }
//        });
//
//        lay_share.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                JSONObject shareobj=new JSONObject();
//
//                try {
//                    JSONObject json = new JSONObject(object.getString("jsondata"));
//
//                    shareobj.put("class","Job_details_via_link");
//                    shareobj.put("title",json.getString("title"));
//                    shareobj.put("description",json.getString("postname"));
//                    shareobj.put("image",Constants.URL+"jobsimage/"+json.getString("image"));
//                    shareobj.put("id",object.getInt("id"));
////
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                Utility.shareIntent(Job_details_via_link.this,shareobj);
//
//
//            }
//        });
//
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        Logg("call_onDestroy()","Call onDestroy()");
//        deleteCache(this);
//    }
//
//    public void deleteCache(Context context) {
//        try {
//            File dir = context.getCacheDir();
//            deleteDir(dir);
//        } catch (Exception e) {}
//    }
//
//    public boolean deleteDir(File dir) {
//        if (dir != null && dir.isDirectory()) {
//            String[] children = dir.list();
//            for (int i = 0; i < children.length; i++) {
//                boolean success = deleteDir(new File(dir, children[i]));
//                if (!success) {
//                    return false;
//                }
//            }
//            return dir.delete();
//        } else if(dir!= null && dir.isFile()) {
//            return dir.delete();
//        } else {
//            return false;
//        }
//    }
//
//
//    public void volleylike_post()
//    {
//
//        progress = ProgressDialog.show(Job_details_via_link.this, null, null, true);
//        progress.setContentView(R.layout.progressdialog);
//        progress.setCanceledOnTouchOutside(false);
//        progress.setCancelable(false);
//        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
////        progress.show();
//
//        RequestQueue queue= Volley.newRequestQueue(Job_details_via_link.this);
//        JSONObject json =new JSONObject();
//        String url= Constants.URL+"newapi2_04/like_post.php?id="+postid+"&email="+email;//+"&email1="+data.get(p).getUid()
//
//        //Logg("like_url",url);
//        JsonObjectRequest jsonreq=new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject res) {
//// //Logg("res",String.valueOf(res));
//                // TODO Auto-generated method stub
//
//                try {
//
//                    if(res.getString("scalar").equals("like")) {
////                        Toast.makeText(Job_details_via_link.this,"success",Toast.LENGTH_SHORT).show();
//
//                        likecount++;
//                        setLike(likecount);
//                        iv_like.setImageResource(R.drawable.ic_bulb_filled);
//
//                    }
//                    else if(res.getString("scalar").equals("dislike")){
//
//                        iv_like.setImageResource(R.drawable.ic_bulb_lightup);
//
////                        Toast.makeText(Job_details_via_link.this,"success",Toast.LENGTH_SHORT).show();
//
//                        likecount--;
//                        setLike(likecount);
//                    }
//                    else {
//                        Toast.makeText(Job_details_via_link.this,"Not done",Toast.LENGTH_SHORT).show();
//                    }
//
//                    progress.dismiss();
//
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                    progress.dismiss();
//                }
//
//            }
//        },new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError arg)
//            {
//
//                //Logg("error", String.valueOf(arg));
//
//                Toast.makeText(Job_details_via_link.this,"Network Problem", Toast.LENGTH_SHORT).show();
//                progress.dismiss();
//            }
//
//
//        });
//
//        queue.add(jsonreq);
//    }
//
//    public void setData(JSONArray array)
//    {
//        String image="";
//        try {
//
//            object=array.getJSONObject(0);
//
//            JSONObject jsonobj = new JSONObject(object.getString("jsondata"));
//            likecount=object.getInt("likes");
//            commentcount=object.getInt("comment");
//            //Logg("website_url",jsonobj.getString("website")+" url");
//
//            if(!object.getString("post_description").equalsIgnoreCase("null"))
//                wv.loadDataWithBaseURL(null,Constants.phle+object.getString("post_description")+Constants.baad , "text/html", "utf-8",null);
//            else
//                wv.loadUrl(jsonobj.getString("website"));
//
//
//            if(jsonobj.has("add_comment")){
//                if(jsonobj.getString("add_comment").equalsIgnoreCase("off")){
//                    lay_comment.setVisibility(View.GONE);
//
////                    vh1.lay_share.setGravity(Gravity.CENTER);
////                    vh1.lay_like.setGravity(Gravity.CENTER);
//                }
//            }
//
//
//           // wv.loadUrl(jsonobj.getString("website"));
//
//            tv_title.setText(jsonobj.getString("title"));
//            tv_postname.setText(jsonobj.getString("postname"));
//            image=jsonobj.getString("image");
////
//            if(image.length()>1){
//                ImageLoader imageLoader = ImageLoader.getInstance();
//                imageLoader.init(ImageLoaderConfiguration.createDefault(this));
//                imageLoader.getInstance().displayImage(Constants.URL_Image + "jobsimage/" + image, iv_job, options, animateFirstListener);
//            }
//
//            if(object.getString("likestatus").equals("like")) {
//                iv_like.setImageResource(R.drawable.ic_bulb_filled);
//                tv_like.setTextColor(getResources().getColor(R.color.like_text_color));
//            }
//            else {
//
//                iv_like.setImageResource(R.drawable.ic_bulb_lightup);
//                tv_like.setTextColor(getResources().getColor(R.color.text_lightgray));
//            }
//            setLike(likecount);
//            setComment(commentcount);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//            //Logg("setdata_exc","e",e);
//        }
//    }
//
//    public void setComment(int com){
//
//        tv_commentcount.setText("Comments ("+com+")");
//
//        if(com<10)
//        {}
//        else if(com<100){
//            tv_commentcount.setTextSize(12f);
//        }
//        else{
//            tv_commentcount.setTextSize(11f);
//        }
//    }
//
//    public void setLike(int lk){
//
////		tv_like.setText(" Light Up ("+detail1.getIntExtra("like",0)+")");
//          if(lk>0) {
//              tv_like.setText(getResources().getString(R.string.like) + " (" + lk + ")");
//          }
//          else
//          {
//              tv_like.setText(getResources().getString(R.string.like));
//          }
//
//        if(lk<10)
//        {}
//        else if(lk<100){
//            tv_like.setTextSize(12f);
//        }
//        else{
//            tv_like.setTextSize(11f);
//        }
//    }
//
//    public void get_Post(int postid)
//    {
//
//        if(!nw.isConnectingToInternet()) {
////			 swipeRefreshLayout.setRefreshing(false);
//            internetconnection(0);
//            return;
//        }
//
//        progress = ProgressDialog.show(Job_details_via_link.this, null, null, true);
//        progress.setContentView(R.layout.progressdialog);
//        progress.setCanceledOnTouchOutside(false);
//        progress.setCancelable(false);
//        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        progress.show();
//
//        RequestQueue queue= Volley.newRequestQueue(getApplicationContext());
//        JSONObject obj=new JSONObject();
//
//        String url=Constants.URL+"newapi2_04/getPost.php?postid="+postid+"&email="+email;
//
//        //Logg("url list",url);
//
//        JsonObjectRequest json=new JsonObjectRequest(Request.Method.POST, url, obj,new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject res) {
//                // TODO Auto-generated method stub
//
//                //Logg("respon",String.valueOf(res));
//
//                try {
//
//                    JSONArray jr=res.getJSONArray("data");
//
//                    setData(jr);
//
//                    progress.dismiss();
//                }
//                catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                    //Logg("no",e.getMessage());
//                    progress.dismiss();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError arg0) {
//                // TODO Auto-generated method stub
//                //Logg("error", String.valueOf(arg0));
//                progress.dismiss();
//
//            }
//        });
//
//        queue.add(json);
//
//    }
//
//    public void internetconnection(final int i){
//
//        final Dialog dialog = new Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.network_alert);
//        dialog.show();
//
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//
//        ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
//        TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);
//
//        tv_tryagain.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                dialog.dismiss();
//
//                get_Post(postid);
//
//            }
//        });
//
//        iv_cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//
//    }
//
//    @Override
//    public void onBackPressed() {
//
//        if(Constants.Check_Branch_IO){
//            Intent in =new Intent(Job_details_via_link.this,HomeActivity.class);
//            startActivity(in);
//            finish();
//        }
//        else
//            super.onBackPressed();
//    }
//
//    private class Callback extends WebViewClient {
//        @Override
//        public boolean shouldOverrideUrlLoading(
//                WebView view, String url) {
//            return(false);
//        }
//
//        @Override
//        public void onPageStarted(WebView view, String url, Bitmap favicon) {
//            super.onPageStarted(view, url, favicon);
//            //Logg("onPagestarted",url+"");
//
//        }
//
//        @Override
//        public void onPageFinished(WebView view, String url) {
//            super.onPageFinished(view, url);
//            //Logg("onPageFinished "+postid,url+"");
//
//            // bar.setVisibility(View.GONE);
//
//        }
//    }

}
