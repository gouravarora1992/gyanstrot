package com.scholar.engineering.banking.ssc;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
//import com.androidquery.AQuery;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.newScreens.SplashScreen;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Pdf_view_Webview extends AppCompatActivity {
    Context c;
    Intent detail1;
    String title, comment, image, date, username, collag, imgurl, newsurl;
    RecyclerView list;
    TextView tv_commentcount, tv_like;

    String url, email;
    ProgressDialog progress;
    LinearLayout layout;
    WebView wv;
    ProgressBar bar;
    int postid, commentcount = 0, likecount = 0;
    ImageView iv_like;
    Home_getset data;
    LinearLayout lay_like, lay_comment, lay_share;
    String likestatus = "";
    NetworkConnection nw;

    ProgressDialog dialog;

    UserSharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pdf_view_webview);

        nw = new NetworkConnection(this);
        wv = (WebView) findViewById(R.id.webViewid);

        tv_commentcount = (TextView) findViewById(R.id.tv_commentid);
        tv_like = (TextView) findViewById(R.id.tv_like);
        iv_like = (ImageView) findViewById(R.id.likeimageid);
        lay_comment = (LinearLayout) findViewById(R.id.lay_comment);
        lay_like = (LinearLayout) findViewById(R.id.lay_like);
        lay_share = (LinearLayout) findViewById(R.id.lay_shareid);

        detail1 = getIntent();

        postid = detail1.getIntExtra("postid", 0);

        pref = UserSharedPreferences.getInstance(this);
//		pref =getSharedPreferences("myref", 0);
//
//		username=pref.getString("name", "");
//		collag=pref.getString("collage", "");
//		imgurl=pref.getString("image","");
        email = User_Email;

        wv.setWebViewClient(new Callback());
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setBuiltInZoomControls(true);

        dialog = new ProgressDialog(Pdf_view_Webview.this);
        dialog.setMessage("Loading..Please wait.");
        dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        dialog.setCanceledOnTouchOutside(true);

        ////Logg("pdf_link",newsurl);

//		wv.getSettings().setAppCacheMaxSize( 5 * 1024 * 1024 ); // 5MB
//		wv.getSettings().setAppCachePath( getApplicationContext().getCacheDir().getAbsolutePath());
//		wv.getSettings().setAllowFileAccess( true );
//		wv.getSettings().setAppCacheEnabled( true );
//		wv.getSettings().setJavaScriptEnabled( true );
//		wv.getSettings().setCacheMode( WebSettings.LOAD_DEFAULT ); // load online by default

        NetworkConnection nc = new NetworkConnection(Pdf_view_Webview.this);

        if (!nc.isConnectingToInternet()) {
            wv.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        }

        get_Post(postid);

//		wv.loadUrl(newsurl+"");

        lay_like.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!nw.isConnectingToInternet()) {
                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
                    return;
                }
                if (pref.getboolean())
                    volleylike_post();
                else {
                    Intent in = new Intent(Pdf_view_Webview.this, SplashScreen.class);
                    startActivity(in);
                    finish();
                }
            }
        });

        lay_comment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (pref.getboolean()) {
                    Intent in = new Intent(Pdf_view_Webview.this, Comment_Common_homepage.class);
                    in.putExtra("posttype", "4");
                    in.putExtra("postid", postid);
                    startActivity(in);
                } else {
                    Intent in = new Intent(Pdf_view_Webview.this, SplashScreen.class);
                    startActivity(in);
                    finish();
                }


            }
        });

        lay_share.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                JSONObject shareobj = new JSONObject();


                try {

                    shareobj.put("class", "Pdf_view_Webview");
                    shareobj.put("title", title);
                    shareobj.put("description", "I find this file very important for you. You can download or view it in Gyan Strot app.");
                    shareobj.put("image", Constants.URL + "newapi2_04/applogo.png");
                    shareobj.put("id", postid);

                } catch (JSONException e) {
                }

                Utility.shareIntent(Pdf_view_Webview.this, shareobj);
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logg("call_onDestroy()", "Call onDestroy()");
        deleteCache(this);
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public void setData(JSONArray array) {
        String image = "";
        JSONObject object = null;

        try {

            object = array.getJSONObject(0);

            JSONObject jsonobj = new JSONObject(object.getString("jsondata"));

            if (jsonobj.has("add_comment")) {
                if (jsonobj.getString("add_comment").equalsIgnoreCase("off")) {
                    lay_comment.setVisibility(View.GONE);
//
//					vh1.lay_share.setGravity(Gravity.CENTER);
//					vh1.lay_like.setGravity(Gravity.CENTER);
                }
            }

            likecount = object.getInt("likes");
            commentcount = object.getInt("comment");
            title = jsonobj.getString("title");
            //Logg("website_url",jsonobj.getString("link")+" url");

            wv.loadUrl("http://docs.google.com/gview?embedded=true&url=" + jsonobj.getString("link"));

            // wv.loadUrl(jsonobj.getString("website"));

            if (object.getString("likestatus").equals("like")) {
                iv_like.setImageResource(R.drawable.ic_bulb_filled);
                tv_like.setTextColor(getResources().getColor(R.color.like_text_color));
            } else {
                tv_like.setTextColor(getResources().getColor(R.color.text_lightgray));
                iv_like.setImageResource(R.drawable.ic_bulb_lightup);
            }
            //Logg("like_comment",likecount+" "+commentcount);

            setLike(likecount);
            setComment(commentcount);

        } catch (JSONException e) {

        }
    }

    public void setComment(int com) {

        tv_commentcount.setText("Comments (" + com + ")");

        if (com < 10) {
        } else if (com < 100) {
            tv_commentcount.setTextSize(12f);
        } else {
            tv_commentcount.setTextSize(11f);
        }
    }

    public void setLike(int lk) {

        tv_like.setText(getResources().getString(R.string.like) + " (" + lk + ")");

        if (lk < 10) {
        } else if (lk < 100) {
            tv_like.setTextSize(12f);
        } else {
            tv_like.setTextSize(11f);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    private class Callback extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return (false);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            if (progress != null && progress.isShowing())
                progress.dismiss();
            //Logg("onpage_started",url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            //Logg("onpage_finished",url);

        }
    }

    @Override
    public void onBackPressed() {

        if (Constants.Check_Branch_IO) {
            Intent in = new Intent(this, HomeActivity.class);
            startActivity(in);
            finish();
        } else
            super.onBackPressed();
    }

    public void internetconnection(final int i) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                get_Post(postid);

            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    public void get_Post(int postid) {

        if (!nw.isConnectingToInternet()) {
//			 swipeRefreshLayout.setRefreshing(false);
            internetconnection(0);
            return;
        }

        progress = ProgressDialog.show(Pdf_view_Webview.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(true);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progress.show();

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        JSONObject obj = new JSONObject();

        String url = Constants.URL + "newapi2_04/getPost.php?postid=" + postid + "&email=" + email;

        //Logg("url list",url);

        JsonObjectRequest json = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                // TODO Auto-generated method stub

                //Logg("respon",String.valueOf(res));

                try {

                    JSONArray jr = res.getJSONArray("data");

                    setData(jr);

//					progress.dismiss();
                } catch (JSONException e) {
                    progress.dismiss();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                // TODO Auto-generated method stub
                //Logg("error", String.valueOf(arg0));
                progress.dismiss();

            }
        });

        queue.add(json);

    }

    public void volleylike_post() {
        progress = ProgressDialog.show(Pdf_view_Webview.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

//			progress.show();

        RequestQueue queue = Volley.newRequestQueue(Pdf_view_Webview.this);
        JSONObject json = new JSONObject();
        try {
            json.put("id",postid);
            json.put("email",Constants.User_Email);
            json.put("admission_no",Constants.addmissionno);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Constants.URL_LV + "like_post";
        Logg("likeapi",url+" , "+json.toString());
        JsonObjectRequest jsonreq = new JsonObjectRequest(Method.POST, url, json, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                // TODO Auto-generated method stub

                try {

                    if (res.getString("scalar").equals("like")) {
//						Toast.makeText(Pdf_view_Webview.this,"success",Toast.LENGTH_SHORT).show();

                        likecount++;
                        setLike(likecount);

                        iv_like.setImageResource(R.drawable.ic_bulb_filled);
                        tv_like.setTextColor(getResources().getColor(R.color.like_text_color));

                    } else if (res.getString("scalar").equals("dislike")) {

                        iv_like.setImageResource(R.drawable.ic_bulb_lightup);
                        tv_like.setTextColor(getResources().getColor(R.color.text_lightgray));
//						Toast.makeText(Pdf_view_Webview.this,"success",Toast.LENGTH_SHORT).show();

                        likecount--;
                        setLike(likecount);

                    } else {
                        Toast.makeText(Pdf_view_Webview.this, "Not done", Toast.LENGTH_SHORT).show();
                    }

                    progress.dismiss();

                } catch (JSONException e) {
                    progress.dismiss();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg) {
                //Logg("error", String.valueOf(arg));
                progress.dismiss();
                Toast.makeText(Pdf_view_Webview.this, "Network Problem", Toast.LENGTH_SHORT).show();
            }


        });

        queue.add(jsonreq);
    }

}
