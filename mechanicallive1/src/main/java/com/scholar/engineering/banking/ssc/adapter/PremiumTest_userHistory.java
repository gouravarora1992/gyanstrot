package com.scholar.engineering.banking.ssc.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;

import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.AdView;
//import com.google.android.gms.ads.NativeExpressAdView;
//import com.google.android.gms.ads.VideoOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.Comment_Common_homepage;
import com.scholar.engineering.banking.ssc.newScreens.SplashScreen;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.DatabaseHandler;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;
import static com.scholar.engineering.banking.ssc.utils.Utility.ConvertDate;

/*
 * Created by surender on 3/14/2017.
 */

public class PremiumTest_userHistory extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    NetworkConnection nw;
    Context c;
    ArrayList<Home_getset> data = new ArrayList<>();
    ArrayList<Home_getset> data1;
    JSONObject jsonobj, jsonfield;
    String email, URL, check;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;

    DatabaseHandler dbh;

    int cl, id;

    UserSharedPreferences pref;

    public void addItem(ArrayList<Home_getset> data) {
        this.data.addAll(data);
    }

    public PremiumTest_userHistory(Context c, ArrayList<Home_getset> data, String clas) {
        this.c = c;
        this.data = data;
        this.check = clas;

        email = User_Email;

        //Logg("user_email",email+" ");
        dbh = new DatabaseHandler(c);

        nw = new NetworkConnection(c);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.blankimage)
                .showImageForEmptyUri(R.drawable.congrrr)
                .showImageOnFail(R.drawable.congrrr)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

//        for(int i=0;i<this.data1.size();i++){
//            if(i%5==0)
//            {
//                data.add(null);
//            }
//            data.add(this.data1.get(i));
//        }

        pref = UserSharedPreferences.getInstance(c);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

//        switch (viewType) {
//            case 0:
        View v0 = inflater.inflate(R.layout.premiumtest_userhistory_item, viewGroup, false);
        viewHolder = new ViewHolder2(v0);

//                break;
//            case 1:
//                //Logg("Viewtype", "viewtype 1");
//                View v1 = inflater.inflate(R.layout.bannerad, viewGroup, false);
//                viewHolder = new NativeExpressAdViewHolder(v1);
//                break;
//            default:
//                View v = inflater.inflate(R.layout.bannerad, viewGroup, false);
//                viewHolder = new NativeExpressAdViewHolder(v);
//                break;
//        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        ViewHolder2 vh0 = (ViewHolder2) viewHolder;
        setOnlinetest(vh0, position);

//        switch (viewHolder.getItemViewType()) {
//            case 0:
//                ViewHolder2 vh0 = (ViewHolder2) viewHolder;
//                setOnlinetest(vh0, position);
//                break;
//            case 1:
//                NativeExpressAdViewHolder viewHolder1= (NativeExpressAdViewHolder) viewHolder;
//                AdRequest request = new AdRequest.Builder().build();
//                viewHolder1.adView.setVideoOptions(new VideoOptions.Builder()
//                        .setStartMuted(true)
//                        .build());
//                viewHolder1.adView.loadAd(request);
//                break;
//        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

//    @Override
//    public int getItemViewType(int position) {
//
//        if(data.get(position)==null)
//            return 1;
//        else
//        {
//            return 0;
//        }
//    }

    private void setOnlinetest(ViewHolder2 vh2, final int p) {

        try {

            jsonobj = new JSONObject(data.get(p).getJsonfield());

            if (jsonobj.has("add_comment")) {
                if (jsonobj.getString("add_comment").equalsIgnoreCase("off")) {
                    vh2.commentlay.setVisibility(View.GONE);

                    vh2.lay_share.setGravity(Gravity.CENTER);
                    vh2.lay_like.setGravity(Gravity.CENTER);
                }
            }

            vh2.text.setText("Premium Series");
            vh2.tv_examname.setText(jsonobj.getString("testname"));
            vh2.tv_time.setText(jsonobj.getString("time") + " Minutes");
            vh2.tv_question.setText(jsonobj.getString("question") + " Questions");
            vh2.tv_postby.setText(jsonobj.getString("writer"));
            vh2.tv_date.setText(ConvertDate(data.get(p).getTimestamp()));

            if (data.get(p).getLikecount() > 0) {
                vh2.tv_likecount.setText(data.get(p).getLikecount() + c.getResources().getString(R.string.like));
            } else {
                vh2.tv_likecount.setText(c.getResources().getString(R.string.like));
            }
            vh2.tv_commentcount.setText(data.get(p).getCommentcount() + " Comments");
            vh2.tv_applicant.setText(data.get(p).getViewcount() + "");

            Typeface font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");

            Typeface font_medium = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");
            vh2.share_text.setTypeface(font_medium);
            vh2.tv_applicant.setTypeface(font_demi);
            vh2.tv_postby.setTypeface(font_medium);
            vh2.tv_likecount.setTypeface(font_medium);
            vh2.tv_commentcount.setTypeface(font_medium);
            vh2.tv_start.setTypeface(font_demi);
            vh2.tv_price.setTypeface(font_medium);
            vh2.text.setTypeface(font_demi);
            vh2.text1.setTypeface(font_demi);
            vh2.tv_examname.setTypeface(font_demi);
            vh2.tv_time.setTypeface(font_medium);
            vh2.tv_question.setTypeface(font_medium);
            vh2.tv_casbback.setTypeface(font_medium);

            if (jsonobj.getString("result_declare").equalsIgnoreCase("no")) {

                vh2.tv_start.setText("Result Pending");
                vh2.tv_start.setTextSize(12f);

            } else {
                if (Integer.parseInt(jsonobj.getString("rank")) > 0) {
                    vh2.tv_start.setText("Rank " + jsonobj.getString("rank") + "/" + jsonobj.getString("candidate"));


                    if ((!jsonobj.getString("points").equalsIgnoreCase("0") &
                            !jsonobj.getString("points").equalsIgnoreCase(""))) {
                        vh2.tv_casbback.setText("Cashback " + jsonobj.getString("points"));
                        vh2.tv_casbback.setVisibility(View.VISIBLE);
                    } else
                        vh2.tv_casbback.setVisibility(View.GONE);
                } else {
//                    vh2.tv_start.setText("Your Rank 1");
                }
            }

//            else if(Utility.checkdownload(jsonobj.getString("testname"))){
//                vh2.tv_start.setText("START NOW");
//            }

            if (jsonobj.getBoolean("user_payment")) {
                vh2.tv_price.setText("");
                // vh2.tv_price.setVisibility(View.GONE);
            } else {
                if (jsonobj.getString("payment").equalsIgnoreCase("free"))
                    vh2.tv_price.setText(jsonobj.getString("payment"));
                else {
                    vh2.tv_price.setText("Rs. " + jsonobj.getString("payment"));
                }
            }

//            if(data.get(p).getLikestatus().equals("like"))
//                vh2.likeimage.setImageResource(R.drawable.ic_bulb_filled);
//            else
//                vh2.likeimage.setImageResource(R.drawable.ic_bulb_lightup);

            if (data.get(p).getLikestatus().equals("like")) {
                vh2.likeimage.setImageResource(R.drawable.ic_bulb_filled);
                vh2.tv_likecount.setTextColor(c.getResources().getColor(R.color.like_text_color));
            } else {
                vh2.likeimage.setImageResource(R.drawable.ic_bulb_lightup);
                vh2.tv_likecount.setTextColor(c.getResources().getColor(R.color.greytextcolor));
            }

        } catch (JSONException e) {
            e.printStackTrace();
            //Logg("json exception test","e",e);
        }

        vh2.lay_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Logg("onlinetest like","onlinetest like click");
                if (!nw.isConnectingToInternet()) {
                    Toast.makeText(c, "No Internet Connection", Toast.LENGTH_LONG).show();
                    return;
                }
                if (pref.getboolean())
                    volleylike_post(p);
                else {
                    Intent in = new Intent(c, SplashScreen.class);
                    c.startActivity(in);
                    ((Activity) c).finish();
                }
            }
        });

        vh2.tv_start.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

//                check_key(c,data.get(p));

            }
        });

        vh2.commentlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(c, Comment_Common_homepage.class);
                in.putExtra("posttype", "2");
                in.putExtra("postid", data.get(p).getId());
                c.startActivity(in);

            }
        });

        vh2.lay_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                JSONObject shareobj = new JSONObject();

                String des = "";

                try {

                    jsonobj = new JSONObject(data.get(p).getJsonfield());
                    if (!jsonobj.getString("rank").equalsIgnoreCase(""))
                        if (Integer.parseInt(jsonobj.getString("rank")) > 0) {
                            des = "I have secured " + jsonobj.getString("rank") + "/" + jsonobj.getString("candidate") + " rank in this Quiz & challenge you to beat my score. Attempt this QUIZ and send me screenshot of your result.";
                        } else {
//                        des="Hi, check this new Quiz uploaded on Gyan Strot App. Attempt to self assess your understanding on the topic. If you like the quiz, press the like button and you may share it with your friends using the share button."
                            des = "I am challenging you to attempt this QUIZ & beat my score. Let’s we both attempt this & share our results by screenshots.";
                        }

                    shareobj.put("class", "Online_Test");
                    shareobj.put("title", jsonobj.getString("testname"));
                    shareobj.put("description", des);
                    shareobj.put("image", Constants.URL + "newapi2_04/applogo.png");
                    shareobj.put("id", data.get(p).getId());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Utility.shareIntent(c, shareobj);

            }
        });

    }

    public class ViewHolder2 extends RecyclerView.ViewHolder {

        //  **************************  Set Onlie Test  ********************

        TextView tv_examname;
        TextView tv_applicant;
        TextView tv_time;
        TextView tv_question;
        TextView tv_start;
        TextView tv_postby;
        TextView tv_price, tv_casbback;
        TextView tv_likecount, tv_date;
        LinearLayout lay_like;
        TextView tv_commentcount, text, text1, share_text;
        //TextView tv_viewcount;
        LinearLayout newslayout;
        LinearLayout commentlay;
        ImageView likeimage;
        LinearLayout lay_share;


        public ViewHolder2(View v) {
            super(v);

            tv_casbback = (TextView) v.findViewById(R.id.tv_cashback);

            tv_date = (TextView) v.findViewById(R.id.tv_date);
            text = (TextView) v.findViewById(R.id.text);
            text1 = (TextView) v.findViewById(R.id.text1);
            share_text = (TextView) v.findViewById(R.id.tv_shareid);
            tv_examname = (TextView) v.findViewById(R.id.examnameid);
            tv_applicant = (TextView) v.findViewById(R.id.applicantid);
            tv_time = (TextView) v.findViewById(R.id.timeid);
            tv_question = (TextView) v.findViewById(R.id.questionid);
            tv_start = (TextView) v.findViewById(R.id.startnowid);
            tv_postby = (TextView) v.findViewById(R.id.postedbyid);
            tv_price = (TextView) v.findViewById(R.id.tv_price);
            tv_likecount = (TextView) v.findViewById(R.id.tv_likeid);
            lay_like = (LinearLayout) v.findViewById(R.id.liketextid);
            tv_commentcount = (TextView) v.findViewById(R.id.commentid);
            lay_share = (LinearLayout) v.findViewById(R.id.ly_shareid);
            //TextView tv_viewcount = (TextView) v.findViewById(R.id.tv_view);
            newslayout = (LinearLayout) v.findViewById(R.id.newslayoutid);
            commentlay = (LinearLayout) v.findViewById(R.id.lay_comment);
            likeimage = (ImageView) v.findViewById(R.id.likeimageid);

        }
    }

    public void volleylike_post(final int p) {

        RequestQueue queue = Volley.newRequestQueue(c);
        JSONObject json = new JSONObject();
        try {
            json.put("id",data.get(p).getId());
            json.put("email",Constants.User_Email);
            json.put("admission_no",Constants.addmissionno);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Constants.URL_LV + "like_post";
        Logg("likeapi",url+" , "+json.toString());
        JsonObjectRequest jsonreq = new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
// //Logg("res",String.valueOf(res));
                // TODO Auto-generated method stub

                try {

                    if (res.getString("scalar").equals("like")) {
//                        Toast.makeText(c,"success",Toast.LENGTH_SHORT).show();
                        int l = data.get(p).getLikecount();
                        l++;
                        //Logg("like_value",l+"");
                        data.get(p).setLikecount(l);
                        data.get(p).setLikestatus("like");
//                        notifyDataSetChanged();
                        notifyItemChanged(p);
                    } else if (res.getString("scalar").equals("dislike")) {

//                        Toast.makeText(c,"success",Toast.LENGTH_SHORT).show();
                        int l = data.get(p).getLikecount();
                        if (l > 0)
                            l--;
                        //Logg("like_value",l+"");
                        data.get(p).setLikecount(l);
                        data.get(p).setLikestatus("dislike");
//                        notifyDataSetChanged();
                        notifyItemChanged(p);
                    } else {
                        Toast.makeText(c, "Not done", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg) {
                //Logg("error", String.valueOf(arg));

                Toast.makeText(c, "Network Problem", Toast.LENGTH_SHORT).show();
            }


        });

        queue.add(jsonreq);
    }

}
