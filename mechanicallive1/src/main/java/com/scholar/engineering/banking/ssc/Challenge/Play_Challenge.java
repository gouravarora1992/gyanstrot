package com.scholar.engineering.banking.ssc.Challenge;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.scholar.engineering.banking.ssc.HomeActivity;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.getset.Question_getset_new;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.scholar.engineering.banking.ssc.Challenge.Choose_Opponent_list_Adapter.challenge_id;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.toast;
import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

/**
 * Created by surender on 3/24/2017.
 */

public class Play_Challenge extends AppCompatActivity implements View.OnClickListener {

    LinearLayout lay_optiona, lay_optionb, lay_optionc, lay_optiond, lay_optione, lay_question;

    TextView tv_question, tv_optiona, tv_optionb, tv_optionc, tv_optiond, tv_optione, tv_hint;

    TextView tv_cir_optiona, tv_cir_optionb, tv_cir_optionc, tv_cir_optiond, tv_cir_optione;

    TextView tv_timer, tv_next, tv_name_lu, tv_name_opp, tv_questioncount;
    LinearLayout lay_hint;

    ScrollView scrollView;
    public static int user_count = 0, opp_count = 0;

    ArrayList<Question_getset_new> data;
    Typeface font_demi, font_medium;

    String login_useremail = "";

    ImageView iv_hint, iv_question, iv_optiona, iv_optionb, iv_optionc, iv_optiond, iv_optione;
    ImageView iv_check_ansa, iv_check_ansb, iv_check_ansc, iv_check_ansd, iv_check_anse;


    int count = 0, per_que_time = 0;
    CountDownTimer timer;
    long time_left;
    long aa2;
    Bitmap bitmap;
    JSONObject object = null, obj_login, obj_opponent;
    String photoPath = Environment.getExternalStorageDirectory() + "/GyanStrot/";

    ProgressDialog progress;
    CircleImageView imageView_lu, imageView_opp;
    boolean b_hint = false, b_que = false, check_result = false;
    String owner_id = "";
    NetworkConnection nw;
    RequestOptions options;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_challenge);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Logg("oncreate", "oncreate_call");

        data = new ArrayList<>();

        nw = new NetworkConnection(this);

        font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");
        font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");


        photoPath = photoPath + Constants.ImageDirectory + "/";
        login_useremail = User_Email;

        imageView_lu = (CircleImageView) findViewById(R.id.imageView_lu);
        imageView_opp = (CircleImageView) findViewById(R.id.imageView_opp);
        tv_name_lu = (TextView) findViewById(R.id.tv_name_lu);
        tv_name_opp = (TextView) findViewById(R.id.tv_name_opp);
        tv_questioncount = (TextView) findViewById(R.id.tv_questioncount);

        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        if (getIntent().hasExtra("data")) {

            try {

                object = new JSONObject(getIntent().getStringExtra("data"));
                JSONArray dt_array = object.getJSONArray("data");

                if (dt_array.getJSONObject(0).getString("email").equalsIgnoreCase(Constants.User_Email)) {
                    obj_login = dt_array.getJSONObject(0);
                    obj_opponent = dt_array.getJSONObject(1);
                } else {
                    obj_login = dt_array.getJSONObject(1);
                    obj_opponent = dt_array.getJSONObject(0);
                }


                tv_name_lu.setText(obj_login.getString("Student_name"));

                if (obj_login.has("image"))
                    if (obj_login.getString("image").length() > 4)
                        Glide.with(this)
                                .load(obj_login.getString("image"))
                                .apply(options)
                                .into(imageView_lu);

                tv_name_opp.setText(obj_opponent.getString("Student_name"));

                if (obj_opponent.has("image"))
                    if (obj_opponent.getString("image").length() > 4)
                        Glide.with(this)
                                .load(obj_opponent.getString("image"))
                                .apply(options)
                                .into(imageView_opp);


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        lay_optiona = (LinearLayout) findViewById(R.id.lay_optiona);
        lay_optionb = (LinearLayout) findViewById(R.id.lay_optionb);
        lay_optionc = (LinearLayout) findViewById(R.id.lay_optionc);
        lay_optiond = (LinearLayout) findViewById(R.id.lay_optiond);
        lay_optione = (LinearLayout) findViewById(R.id.lay_optione);
        lay_question = (LinearLayout) findViewById(R.id.lay_question);
        lay_hint = (LinearLayout) findViewById(R.id.lay_hint);
        tv_optiona = (TextView) findViewById(R.id.tv_optiona);
        tv_optionb = (TextView) findViewById(R.id.tv_optionb);
        tv_optionc = (TextView) findViewById(R.id.tv_optionc);
        tv_optiond = (TextView) findViewById(R.id.tv_optiond);
        tv_optione = (TextView) findViewById(R.id.tv_optione);
        tv_question = (TextView) findViewById(R.id.tv_question);
        tv_hint = (TextView) findViewById(R.id.tv_questionhint);
        tv_cir_optiona = (TextView) findViewById(R.id.tv_circle_a);
        tv_cir_optionb = (TextView) findViewById(R.id.tv_circle_b);
        tv_cir_optionc = (TextView) findViewById(R.id.tv_circle_c);
        tv_cir_optiond = (TextView) findViewById(R.id.tv_circle_d);
        tv_cir_optione = (TextView) findViewById(R.id.tv_circle_e);
        scrollView = (ScrollView) findViewById(R.id.scrollView);

        tv_cir_optiona.setTypeface(font_demi);
        tv_cir_optionb.setTypeface(font_demi);
        tv_cir_optionc.setTypeface(font_demi);
        tv_cir_optiond.setTypeface(font_demi);
        tv_cir_optione.setTypeface(font_demi);
        tv_optiona.setTypeface(font_medium);
        tv_optionb.setTypeface(font_medium);
        tv_optionc.setTypeface(font_medium);
        tv_optiond.setTypeface(font_medium);
        tv_optione.setTypeface(font_medium);
        tv_question.setTypeface(font_medium);
        tv_hint.setTypeface(font_medium);
        iv_check_ansa = (ImageView) findViewById(R.id.iv_option_a);
        iv_check_ansb = (ImageView) findViewById(R.id.iv_option_b);
        iv_check_ansc = (ImageView) findViewById(R.id.iv_option_c);
        iv_check_ansd = (ImageView) findViewById(R.id.iv_option_d);
        iv_check_anse = (ImageView) findViewById(R.id.iv_option_e);
        iv_check_ansa.setVisibility(View.GONE);
        iv_check_ansb.setVisibility(View.GONE);
        iv_check_ansc.setVisibility(View.GONE);
        iv_check_ansd.setVisibility(View.GONE);
        iv_check_anse.setVisibility(View.GONE);

        iv_optiona = (ImageView) findViewById(R.id.iv_optiona);
        iv_optionb = (ImageView) findViewById(R.id.iv_optionb);
        iv_optionc = (ImageView) findViewById(R.id.iv_optionc);
        iv_optiond = (ImageView) findViewById(R.id.iv_optiond);
        iv_optione = (ImageView) findViewById(R.id.iv_optione);

        iv_question = (ImageView) findViewById(R.id.iv_question);
        iv_hint = (ImageView) findViewById(R.id.iv_questionhint);
        tv_next = (TextView) findViewById(R.id.tv_next);
        tv_timer = (TextView) findViewById(R.id.tv_timer);
        tv_next.setTypeface(font_demi);
        tv_timer.setTypeface(font_demi);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        Constants.Width = (displayMetrics.widthPixels + 40);
        Constants.Height = displayMetrics.heightPixels;

        getQuestion();

        tv_next.setOnClickListener(this);
        lay_optiona.setOnClickListener(this);
        lay_optionb.setOnClickListener(this);
        lay_optionc.setOnClickListener(this);
        lay_optiond.setOnClickListener(this);
        lay_optione.setOnClickListener(this);
        lay_question.setOnClickListener(this);
        tv_hint.setOnClickListener(this);

        lay_optiona.setClickable(false);
        lay_optionb.setClickable(false);
        lay_optionc.setClickable(false);
        lay_optiond.setClickable(false);
        lay_optione.setClickable(false);
        tv_next.setClickable(false);
        lay_question.setClickable(false);
        tv_hint.setClickable(false);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logg("call_onDestroy()", "Call onDestroy()");

        if (timer != null)
            timer.cancel();

        deleteCache(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Logg("newintent", "newintent");
        Toast.makeText(this, "newintent", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        exitchallenge();
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.tv_next) {

            scrollView.scrollTo(0, scrollView.getTop());

            if (count < data.size() - 1) {
                Logg("getuserans", data.get(count).getUser_ans() + " , ");

                if (data.get(count).getUser_ans().equalsIgnoreCase("no")) {
                    //insert users answer
                    toast(Play_Challenge.this,"Please select an option");
//                    Map<String, String> map = new HashMap<>();
//                    map.put("user_id", login_useremail);
//
//                    if (count == (data.size() - 1))
//                        map.put("last_ques", "yes");
//                    else
//                        map.put("last_ques", "no");
//
//                    map.put("type", "challenge");
//                    map.put("admission_no", Constants.addmissionno);
//                    try {
//                        map.put("ques_id", data.get(count).getId() + "");
//                        map.put("challenge_id", challenge_id);
//                        map.put("time", (aa2 - time_left) + "");
//                        map.put("ques_answer", user_ans);
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                    map.put("ques_status", "U");
//
//                    insertAnswer(map);
                } else {
                    count++;
                    setQuestion();
                }
                tv_next.setText("Next");
            } else if (count == data.size() - 1) {
                successAlert();
            }
        } else if (v.getId() == R.id.lay_optiona) {

            String real_opt = data.get(count).getAns_a();

            Logg("real_ans", real_opt);

            data.get(count).setUser_ans(real_opt);

            checkAnswer(data.get(count).getAnswer(), real_opt, lay_optiona, iv_check_ansa);

        } else if (v.getId() == R.id.lay_optionb) {

            String real_opt = data.get(count).getAns_b();

            Logg("real_ans", real_opt);

            data.get(count).setUser_ans(real_opt);

            checkAnswer(data.get(count).getAnswer(), real_opt, lay_optionb, iv_check_ansb);

        } else if (v.getId() == R.id.lay_optionc) {

            String real_opt = data.get(count).getAns_c();

            Logg("real_ans", real_opt);

            data.get(count).setUser_ans(real_opt);

            checkAnswer(data.get(count).getAnswer(), real_opt, lay_optionc, iv_check_ansc);
        } else if (v.getId() == R.id.lay_optiond) {

            String real_opt = data.get(count).getAns_d();

            Logg("real_ans", real_opt);

            data.get(count).setUser_ans(real_opt);

            checkAnswer(data.get(count).getAnswer(), real_opt, lay_optiond, iv_check_ansd);

        } else if (v.getId() == R.id.lay_optione) {

            String real_opt = data.get(count).getAns_e();

            Logg("real_ans", real_opt);

            data.get(count).setUser_ans(real_opt);

            checkAnswer(data.get(count).getAnswer(), real_opt, lay_optione, iv_check_anse);

        }
    }

    private void successAlert() {

        if (isFinishing())
            return;

        final Dialog dialog = new Dialog(Play_Challenge.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.success_dialog);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        TextView text, text1;
        text = (TextView) dialog.findViewById(R.id.text);
        text1 = (TextView) dialog.findViewById(R.id.text1);
        final TextView Submit = (TextView) dialog.findViewById(R.id.tv_submit);
        TextView Cancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        Cancel.setVisibility(View.VISIBLE);

        text.setTypeface(font_medium);
        text1.setTypeface(font_medium);
        Submit.setTypeface(font_demi);
        Cancel.setTypeface(font_demi);

        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if (owner_id.equalsIgnoreCase(Constants.User_Email)) {
                        Intent in = new Intent(Play_Challenge.this, Waiting_for_Opponent.class);
                        in.putExtra("data", object + "");
                        startActivity(in);
                        finish();
                    } else
                        finish();

                } catch (Exception e) {
                    e.printStackTrace();
                }

                dialog.dismiss();
            }
        });

        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (owner_id.equalsIgnoreCase(Constants.User_Email)) {
                    Intent in = new Intent(Play_Challenge.this, Waiting_for_Opponent.class);
                    in.putExtra("data", object + "");
                    startActivity(in);
                    finish();
                } else
                    finish();

                dialog.dismiss();

            }
        });
    }

    public void checkAnswer(String cort_ans, String user_ans, LinearLayout layout, ImageView iv_icon) {

        Logg("user_ans", user_ans);
        if (user_ans != null) {
            lay_optiona.setClickable(false);
            lay_optionb.setClickable(false);
            lay_optionc.setClickable(false);
            lay_optiond.setClickable(false);
            lay_optione.setClickable(false);

            lay_optiona.setBackgroundColor(Color.parseColor("#00000000"));
            lay_optionb.setBackgroundColor(Color.parseColor("#00000000"));
            lay_optionc.setBackgroundColor(Color.parseColor("#00000000"));
            lay_optiond.setBackgroundColor(Color.parseColor("#00000000"));
            lay_optione.setBackgroundColor(Color.parseColor("#00000000"));

            iv_check_ansa.setVisibility(View.GONE);
            iv_check_ansb.setVisibility(View.GONE);
            iv_check_ansc.setVisibility(View.GONE);
            iv_check_ansd.setVisibility(View.GONE);
            iv_check_anse.setVisibility(View.GONE);

            Map<String, String> map = new HashMap<>();
            map.put("user_id", login_useremail);

            if (count == (data.size() - 1))
                map.put("last_ques", "yes");
            else
                map.put("last_ques", "no");

            map.put("type", "challenge");
            map.put("admission_no", Constants.addmissionno);
            try {
                map.put("ques_id", data.get(count).getId() + "");
                map.put("challenge_id", challenge_id);
                map.put("time", (aa2 - time_left) + "");
                map.put("ques_answer", user_ans);

            } catch (Exception e) {
                e.printStackTrace();
            }

            if (cort_ans.equalsIgnoreCase(user_ans)) {
                map.put("ques_status", "R");
                user_count++;
                insertAnswer(map);

                layout.setBackgroundColor(Color.parseColor("#33009900"));
                iv_icon.setImageResource(R.drawable.ic_right);
                iv_icon.setVisibility(View.VISIBLE);

            } else {

                Logg("cort_ans", cort_ans);
                map.put("ques_status", "W");
                insertAnswer(map);

                if (cort_ans.equalsIgnoreCase("a")) {
                    if (data.get(count).getAns_a().equalsIgnoreCase("a")) {
                        lay_optiona.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansa.setImageResource(R.drawable.ic_right);
                        iv_check_ansa.setVisibility(View.VISIBLE);
                    } else if (data.get(count).getAns_b().equalsIgnoreCase("a")) {
                        lay_optionb.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansb.setImageResource(R.drawable.ic_right);
                        iv_check_ansb.setVisibility(View.VISIBLE);
                    } else if (data.get(count).getAns_c().equalsIgnoreCase("a")) {
                        lay_optionc.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansc.setImageResource(R.drawable.ic_right);
                        iv_check_ansc.setVisibility(View.VISIBLE);
                    } else if (data.get(count).getAns_d().equalsIgnoreCase("a")) {
                        lay_optiond.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansd.setImageResource(R.drawable.ic_right);
                        iv_check_ansd.setVisibility(View.VISIBLE);
                    } else if (data.get(count).getAns_e().equalsIgnoreCase("a")) {
                        lay_optione.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_anse.setImageResource(R.drawable.ic_right);
                        iv_check_anse.setVisibility(View.VISIBLE);
                    }

                } else if (cort_ans.equalsIgnoreCase("b")) {

                    if (data.get(count).getAns_a().equalsIgnoreCase("b")) {
                        lay_optiona.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansa.setImageResource(R.drawable.ic_right);
                        iv_check_ansa.setVisibility(View.VISIBLE);
                    } else if (data.get(count).getAns_b().equalsIgnoreCase("b")) {
                        lay_optionb.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansb.setImageResource(R.drawable.ic_right);
                        iv_check_ansb.setVisibility(View.VISIBLE);
                    } else if (data.get(count).getAns_c().equalsIgnoreCase("b")) {
                        lay_optionc.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansc.setImageResource(R.drawable.ic_right);
                        iv_check_ansc.setVisibility(View.VISIBLE);
                    } else if (data.get(count).getAns_d().equalsIgnoreCase("b")) {
                        lay_optiond.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansd.setImageResource(R.drawable.ic_right);
                        iv_check_ansd.setVisibility(View.VISIBLE);
                    } else if (data.get(count).getAns_e().equalsIgnoreCase("b")) {
                        lay_optione.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_anse.setImageResource(R.drawable.ic_right);
                        iv_check_anse.setVisibility(View.VISIBLE);
                    }
                } else if (cort_ans.equalsIgnoreCase("c")) {

                    if (data.get(count).getAns_a().equalsIgnoreCase("c")) {
                        lay_optiona.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansa.setImageResource(R.drawable.ic_right);
                        iv_check_ansa.setVisibility(View.VISIBLE);
                    } else if (data.get(count).getAns_b().equalsIgnoreCase("c")) {
                        lay_optionb.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansb.setImageResource(R.drawable.ic_right);
                        iv_check_ansb.setVisibility(View.VISIBLE);
                    } else if (data.get(count).getAns_c().equalsIgnoreCase("c")) {
                        lay_optionc.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansc.setImageResource(R.drawable.ic_right);
                        iv_check_ansc.setVisibility(View.VISIBLE);
                    } else if (data.get(count).getAns_d().equalsIgnoreCase("c")) {
                        lay_optiond.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansd.setImageResource(R.drawable.ic_right);
                        iv_check_ansd.setVisibility(View.VISIBLE);
                    } else if (data.get(count).getAns_e().equalsIgnoreCase("c")) {
                        lay_optione.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_anse.setImageResource(R.drawable.ic_right);
                        iv_check_anse.setVisibility(View.VISIBLE);
                    }

                } else if (cort_ans.equalsIgnoreCase("d")) {

                    if (data.get(count).getAns_a().equalsIgnoreCase("d")) {
                        lay_optiona.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansa.setImageResource(R.drawable.ic_right);
                        iv_check_ansa.setVisibility(View.VISIBLE);
                    } else if (data.get(count).getAns_b().equalsIgnoreCase("d")) {
                        lay_optionb.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansb.setImageResource(R.drawable.ic_right);
                        iv_check_ansb.setVisibility(View.VISIBLE);
                    } else if (data.get(count).getAns_c().equalsIgnoreCase("d")) {
                        lay_optionc.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansc.setImageResource(R.drawable.ic_right);
                        iv_check_ansc.setVisibility(View.VISIBLE);
                    } else if (data.get(count).getAns_d().equalsIgnoreCase("d")) {
                        lay_optiond.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansd.setImageResource(R.drawable.ic_right);
                        iv_check_ansd.setVisibility(View.VISIBLE);
                    } else if (data.get(count).getAns_e().equalsIgnoreCase("d")) {
                        lay_optione.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_anse.setImageResource(R.drawable.ic_right);
                        iv_check_anse.setVisibility(View.VISIBLE);
                    }

                } else if (cort_ans.equalsIgnoreCase("e")) {
                    if (data.get(count).getAns_a().equalsIgnoreCase("e")) {
                        lay_optiona.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansa.setImageResource(R.drawable.ic_right);
                        iv_check_ansa.setVisibility(View.VISIBLE);
                    } else if (data.get(count).getAns_b().equalsIgnoreCase("e")) {
                        lay_optionb.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansb.setImageResource(R.drawable.ic_right);
                        iv_check_ansb.setVisibility(View.VISIBLE);
                    } else if (data.get(count).getAns_c().equalsIgnoreCase("e")) {
                        lay_optionc.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansc.setImageResource(R.drawable.ic_right);
                        iv_check_ansc.setVisibility(View.VISIBLE);
                    } else if (data.get(count).getAns_d().equalsIgnoreCase("e")) {
                        lay_optiond.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_ansd.setImageResource(R.drawable.ic_right);
                        iv_check_ansd.setVisibility(View.VISIBLE);
                    } else if (data.get(count).getAns_e().equalsIgnoreCase("e")) {
                        lay_optione.setBackgroundColor(Color.parseColor("#33009900"));
                        iv_check_anse.setImageResource(R.drawable.ic_right);
                        iv_check_anse.setVisibility(View.VISIBLE);
                    }
                }
                layout.setBackgroundColor(Color.parseColor("#33ff0000"));
                iv_icon.setImageResource(R.drawable.ic_wrong);
                iv_icon.setVisibility(View.VISIBLE);
            }

            try {
                if (object.has("user_ans_status"))
                    if (object.getString("user_ans_status").equalsIgnoreCase("R"))
                        opp_count++;
            } catch (JSONException e) {

            }

            tv_questioncount.setText(user_count + " : " + opp_count);
        }
    }

    public void setQuestion() {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        iv_hint.setVisibility(View.GONE);
        iv_optiona.setVisibility(View.GONE);
        iv_optionb.setVisibility(View.GONE);
        iv_optionc.setVisibility(View.GONE);
        iv_optiond.setVisibility(View.GONE);
        iv_optione.setVisibility(View.GONE);
        iv_question.setVisibility(View.GONE);

        lay_optiona.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optionb.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optionc.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optiond.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optione.setBackgroundColor(Color.parseColor("#00000000"));

        iv_check_ansa.setVisibility(View.GONE);
        iv_check_ansb.setVisibility(View.GONE);
        iv_check_ansc.setVisibility(View.GONE);
        iv_check_ansd.setVisibility(View.GONE);
        iv_check_anse.setVisibility(View.GONE);

        if (data.get(count).getOptione().equalsIgnoreCase("null") & data.get(count).getOptione_img().equalsIgnoreCase("null")) {
            lay_optione.setVisibility(View.GONE);
        } else {
            lay_optione.setVisibility(View.VISIBLE);

            if (!data.get(count).getOptione_img().equalsIgnoreCase("null")) {

                iv_optione.setVisibility(View.VISIBLE);

                String[] arr = data.get(count).getOptione_img().split("/");

                int len = arr.length;

                String photoPath1;
                photoPath1 = photoPath + arr[len - 1];

                bitmap = BitmapFactory.decodeFile(photoPath1, options);
                if (bitmap != null) {
                    iv_optione.setImageBitmap(bitmap);
                }
            }
        }

        if (data.get(count).getHint().equalsIgnoreCase("null") & data.get(count).getHint_img().equalsIgnoreCase("null")) {
            lay_hint.setVisibility(View.GONE);
        } else if (data.get(count).getHint().length() <= 0 & data.get(count).getHint_img().equalsIgnoreCase("null")) {
            lay_hint.setVisibility(View.GONE);
        } else {
            lay_hint.setVisibility(View.VISIBLE);

        }

        if (!data.get(count).getQuestion_img().equalsIgnoreCase("null")) {
            iv_question.setVisibility(View.VISIBLE);

            String[] arr = data.get(count).getQuestion_img().split("/");
            int len = arr.length;
            String photoPath1;
            photoPath1 = photoPath + arr[len - 1];

            bitmap = BitmapFactory.decodeFile(photoPath1, options);
            if (bitmap != null) {

                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;

                int h;
                h = Constants.Width / imageWidth;
                h = (int) h * imageHeight;

                ViewGroup.LayoutParams params = iv_question.getLayoutParams();

                params.height = h;

                iv_question.setLayoutParams(params);

                iv_question.setImageBitmap(bitmap);
            }
        }

        if (!data.get(count).getOptiona_img().equalsIgnoreCase("null")) {
            iv_optiona.setVisibility(View.VISIBLE);

            String[] arr = data.get(count).getOptiona_img().split("/");
            int len = arr.length;
            String photoPath1;
            photoPath1 = photoPath + arr[len - 1];

            bitmap = BitmapFactory.decodeFile(photoPath1, options);
            if (bitmap != null) {
                iv_optiona.setImageBitmap(bitmap);
            }
        }

        if (!data.get(count).getOptionb_img().equalsIgnoreCase("null")) {
            iv_optionb.setVisibility(View.VISIBLE);
            String[] arr = data.get(count).getOptionb_img().split("/");
            int len = arr.length;
            String photoPath1;
            photoPath1 = photoPath + arr[len - 1];

            bitmap = BitmapFactory.decodeFile(photoPath1, options);
            if (bitmap != null) {
                iv_optionb.setImageBitmap(bitmap);
            }
        }

        if (!data.get(count).getOptionc_img().equalsIgnoreCase("null")) {
            iv_optionc.setVisibility(View.VISIBLE);
            String[] arr = data.get(count).getOptionc_img().split("/");
            int len = arr.length;
            String photoPath1;
            photoPath1 = photoPath + arr[len - 1];

            bitmap = BitmapFactory.decodeFile(photoPath1, options);
            if (bitmap != null) {
                iv_optionc.setImageBitmap(bitmap);
            }

        }

        if (!data.get(count).getOptiond_img().equalsIgnoreCase("null")) {
            iv_optiond.setVisibility(View.VISIBLE);
            String[] arr = data.get(count).getOptiond_img().split("/");

            int len = arr.length;
            String photoPath1;
            photoPath1 = photoPath + arr[len - 1];

            bitmap = BitmapFactory.decodeFile(photoPath1, options);
            if (bitmap != null) {
                iv_optiond.setImageBitmap(bitmap);
            }
        }

        tv_question.setText(Html.fromHtml(data.get(count).getQuestion()));

        tv_optiona.setText(Html.fromHtml(data.get(count).getOptiona()));

        tv_optionb.setText(Html.fromHtml(data.get(count).getOptionb()));

        tv_optionc.setText(Html.fromHtml(data.get(count).getOptionc()));

        tv_optiond.setText(Html.fromHtml(data.get(count).getOptiond()));

        tv_optione.setText(Html.fromHtml(data.get(count).getOptione()));

        tv_hint.setText(Html.fromHtml(data.get(count).getHint()));

        lay_optiona.setClickable(true);
        lay_optionb.setClickable(true);
        lay_optionc.setClickable(true);
        lay_optiond.setClickable(true);
        lay_optione.setClickable(true);

        if (timer != null) {
            timer.cancel();
            setTimer(per_que_time);
        } else
            setTimer(per_que_time);


    }

    public void getQuestion() {

        if (!nw.isConnectingToInternet()) {

            internetconnection(0);

            return;
        }

        progress = ProgressDialog.show(Play_Challenge.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        if (!this.isFinishing())
            progress.show();

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        data = new ArrayList<>();
        String url = Constants.URL_LV + "challange_questions";
        Logg("url", url + "");

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Logg("challenge_questions", s + "");
                try {

                    JSONObject res = new JSONObject(s);

                    if (res.has("status")) {

                        if (res.getString("status").equalsIgnoreCase("true")) {

                            if (res.has("data")) {

                                per_que_time = res.getInt("per_que_time");
                                owner_id = res.getString("owner_id");

                                if (owner_id.equalsIgnoreCase(Constants.User_Email)) {
                                    user_count = res.getInt("user_count");
                                    opp_count = res.getInt("opponent_count");
                                } else {
                                    user_count = res.getInt("opponent_count");
                                    opp_count = res.getInt("user_count");
                                }

                                tv_questioncount.setText(user_count + " : " + opp_count);

                                JSONArray array = res.getJSONArray("data");

                                for (int i = 0; i < array.length(); i++) {

                                    JSONObject obj = array.getJSONObject(i);

                                    JSONObject jsondata = new JSONObject(obj.getString("jsondata"));

                                    JSONObject obj_data = jsondata.getJSONObject("data");

                                    JSONObject obj_que = obj_data.getJSONObject("question");
                                    JSONObject obj_a = obj_data.getJSONObject("option a");
                                    JSONObject obj_b = obj_data.getJSONObject("option b");
                                    JSONObject obj_c = obj_data.getJSONObject("option c");
                                    JSONObject obj_d = obj_data.getJSONObject("option d");
                                    JSONObject obj_e = obj_data.getJSONObject("option e");

                                    obj_a.put("option", "a");
                                    obj_b.put("option", "b");
                                    obj_c.put("option", "c");
                                    obj_d.put("option", "d");
                                    obj_e.put("option", "e");

                                    JSONArray opt_arr = new JSONArray();

                                    opt_arr.put(obj_a);
                                    opt_arr.put(obj_b);
                                    opt_arr.put(obj_c);
                                    opt_arr.put(obj_d);

                                    if (!obj_e.getString("value").equalsIgnoreCase("null")
                                            & !obj_e.getString("value").equalsIgnoreCase("")
                                            & obj_e.getString("value") != null)
                                        opt_arr.put(obj_e);

                                    opt_arr = shuffleJsonArray(opt_arr);

                                    obj_a = opt_arr.getJSONObject(0);
                                    obj_b = opt_arr.getJSONObject(1);
                                    obj_c = opt_arr.getJSONObject(2);
                                    obj_d = opt_arr.getJSONObject(3);

                                    if (opt_arr.length() > 4)
                                        obj_e = opt_arr.getJSONObject(4);


                                    JSONObject obj_ans = obj_data.getJSONObject("answer");
                                    JSONObject obj_hint = obj_data.getJSONObject("hint");
                                    JSONObject obj_sol = obj_data.getJSONObject("solution");

                                    data.add(new Question_getset_new(obj.getInt("id"), obj.getString("testid"),
                                            obj.getString("sub_test_name"), obj_hint.getString("value"), obj_que.getString("value"),
                                            obj_a.getString("value"), obj_b.getString("value"), obj_c.getString("value"),
                                            obj_d.getString("value"), obj_e.getString("value"), obj_ans.getString("value"),
                                            obj_sol.getString("value"), obj_hint.getString("image"), obj_que.getString("image"),
                                            obj_a.getString("image"), obj_b.getString("image"), obj_c.getString("image"),
                                            obj_d.getString("image"), obj_e.getString("image"), obj_sol.getString("image"), "no",
                                            obj_a.getString("option"), obj_b.getString("option"), obj_c.getString("option"),
                                            obj_d.getString("option"), obj_e.getString("option")));
                                }

                                setQuestion();

                                lay_optiona.setClickable(true);
                                lay_optionb.setClickable(true);
                                lay_optionc.setClickable(true);
                                lay_optiond.setClickable(true);
                                lay_optione.setClickable(true);
                                tv_next.setClickable(true);
                                lay_question.setClickable(true);
                                tv_hint.setClickable(true);

                                progress.dismiss();

                            }
                        } else if (res.getString("status").equalsIgnoreCase("false")) {

                            if (res.getString("message").equalsIgnoreCase("challenge not found"))
                                Toast.makeText(Play_Challenge.this, "Challenge has been expired", Toast.LENGTH_SHORT).show();
                            else
                                Toast.makeText(Play_Challenge.this, "Something went wrong please try again", Toast.LENGTH_SHORT).show();

                            finish();
                        }
                    }

                    progress.dismiss();

                } catch (JSONException e) {
                    progress.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(Play_Challenge.this, CommonUtils.volleyerror(volleyError));
                progress.dismiss();
                Logg("error", volleyError.toString());

            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("challenge_id", challenge_id + "");
                params.put("user_id", login_useremail);
                Logg("getquestion params", params + "");
                return params;
            }
        };

        request.setShouldCache(false);
        int socketTimeout = 50000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void insertAnswer(final Map<String, String> map) {

        RequestQueue queue = Volley.newRequestQueue(Play_Challenge.this);

        String url = Constants.URL_LV + "answer_chall87";

        url = url.replace(" ", "%20");
        Logg("name", url);

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                try {

                    JSONObject arg0 = new JSONObject(s);

                    Logg("answer_chall87 res", s);
                    //  if(arg0.getS)
                    if (arg0.getString("status").contains("true")) {

                        if (map.get("ques_status").equalsIgnoreCase("U")) {
                            if (count < data.size() - 1) {
                                count++;
                                setQuestion();
                            }
                        } else {

                        }
                    } else {
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(Play_Challenge.this, CommonUtils.volleyerror(volleyError));
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params = map;
                Logg("insert answer params", params + "");
                return params;
            }
        };

        // queue.add(request);

        int socketTimeout = 0;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        queue.add(request);
    }

    public void internetconnection(final int i) {

        if (isFinishing())
            return;

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
//                swipeRefreshLayout.setRefreshing(true);
                getQuestion();

            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void setTimer(int time) {

        aa2 = time * 1000;  //1 minute=60000 ms, 1 sec =1000 ms

        timer = new CountDownTimer(aa2, 1000) {

            @TargetApi(Build.VERSION_CODES.GINGERBREAD)
            @SuppressLint("NewApi")
            public void onTick(long millisUntilFinished) {

                tv_timer.setText("" + String.format("%d:%d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
//                am=TimeUnit.MINUTES;
//                as=TimeUnit.SECONDS;

                time_left = millisUntilFinished;

            }

            public void onFinish() {
                // set next question

                timer.cancel();

                if (count == data.size() - 1) {
                    successAlert();
                } else {
                    count++;
                    setQuestion();
                }
            }

        }.start();

    }

    public JSONArray shuffleJsonArray(JSONArray array) {
        // Implementing Fisher–Yates shuffle
        Random rnd = new Random();
        for (int i = array.length() - 1; i >= 0; i--) {
            try {
                int j = rnd.nextInt(i + 1);
                // Simple swap
                Object object = array.get(j);
                array.put(j, array.get(i));

                array.put(i, object);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return array;
    }

    public void exitchallenge() {

        androidx.appcompat.app.AlertDialog.Builder alertDialog = new androidx.appcompat.app.AlertDialog.Builder(Play_Challenge.this);

        alertDialog.setMessage("Do You Want To Stop This Challenge?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

}