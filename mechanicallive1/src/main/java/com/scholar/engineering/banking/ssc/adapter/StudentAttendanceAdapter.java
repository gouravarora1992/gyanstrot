package com.scholar.engineering.banking.ssc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.google.gson.JsonArray;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ViewStudentAttendanceListModel;
import com.scholar.engineering.banking.ssc.databinding.StudentAttendanceRecyclerItemBinding;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class StudentAttendanceAdapter extends RecyclerView.Adapter<StudentAttendanceAdapter.ViewHolder> {

    private Context context;
    private ViewStudentAttendanceListModel attendanceListModel;
    JSONArray jsonArray;

    public StudentAttendanceAdapter(Context context, ViewStudentAttendanceListModel attendanceListModel, JSONArray jsonArray) {
        this.context = context;
        this.attendanceListModel = attendanceListModel;
        this.jsonArray= jsonArray;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        StudentAttendanceRecyclerItemBinding binding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                (R.layout.student_attendance_recycler_item),parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        try {
            holder.binding.txtDates.setText(jsonArray.getJSONObject(position).getString("date"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            if(jsonArray.getJSONObject(position).getString("status").equalsIgnoreCase("present")){
                holder.binding.imgStatus.setImageResource(R.drawable.present);
            } else if (jsonArray.getJSONObject(position).getString("status").equalsIgnoreCase("absent")){
                holder.binding.imgStatus.setImageResource(R.drawable.absent);
            } else if (jsonArray.getJSONObject(position).getString("status").equalsIgnoreCase("holiday")){
                holder.binding.imgStatus.setImageResource(R.drawable.holiday);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public StudentAttendanceRecyclerItemBinding binding;

        public ViewHolder(@NonNull final StudentAttendanceRecyclerItemBinding binding) {
            super(binding.getRoot());
            this.binding=binding;

        }
    }
}
