package com.scholar.engineering.banking.ssc.Staff;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.AddItemModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.DeptModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.PurposeImageModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.StockModel;
import com.scholar.engineering.banking.ssc.databinding.ActivityAddItemQueryBinding;
import com.scholar.engineering.banking.ssc.getset.RetrofitObjectAPI;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.GetPath;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class AddItemQueryActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener{

    ActivityAddItemQueryBinding binding;
    TextView tv_choose_file, tv_existing_quantity, tv_rate, et_purchased, et_final_cost, text_purchased, txt_finalcost;
    EditText et_quantity, et_cost, et_purpose, et_justification;
    Spinner spinner_item_name;
    Button btn_file;
    private static final int PICK_GALLERY = 102;
    NetworkConnection nw;
    List<DeptModel.Datum> departmentArr;
    List<StockModel.Datum> stockArr;
    File file;
    String purposeImage;
    Integer id , last_purchase, final_cost;
    String quantity = "";
    String rate = "";
    ImageView img_close;
    ArrayList<AddItemModel.item> itemModels = new ArrayList<>();
    ArrayList<String> img = new ArrayList<String>();
    ArrayList<String> newImage = new ArrayList<String>();
    JSONArray jsonArray = new JSONArray();
    UserSharedPreferences preferences;
    UserSharedPreferences playerpreferences;
    ProgressDialog progress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_add_item_query);

        binding.imgBack.setOnClickListener(this);
        nw = new NetworkConnection(AddItemQueryActivity.this);
        progress = new ProgressDialog(AddItemQueryActivity.this);
        playerpreferences=new UserSharedPreferences(AddItemQueryActivity.this,"playerpreferences");
        preferences=UserSharedPreferences.getInstance(AddItemQueryActivity.this);
        String role = preferences.getRole();
        id = preferences.getId();
        String date = preferences.getDate();

        Log.e("role-->",role);
        Log.e("id===>>",""+id);
        Log.e("date-->",date);

        if (date!=null) {
            binding.textView31.setText(date);
        }
        binding.textView33.setText(role);

        binding.button3.setOnClickListener(this);
        binding.button2.setOnClickListener(this);
        binding.txtAdd.setOnClickListener(this);

        addView();
        getDepartment();
        getStock();

    }

    //permission
    private void requestPermision() {
        ActivityCompat.requestPermissions(AddItemQueryActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
    }

    private boolean checkPermission() {

        int READ_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(AddItemQueryActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int WRITE_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(AddItemQueryActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (WRITE_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else if (READ_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //ImageFromGallery
        if (requestCode == PICK_GALLERY) {
            if (resultCode == RESULT_OK) {
                try {
                    if (data != null) {
                        Uri filePath = data.getData();
                        String path= GetPath.getPath(AddItemQueryActivity.this,filePath);
                        Log.e("path", ""+ path);
                        uploadPurposeImage(path);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = AddItemQueryActivity.this.managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    private boolean checkValidation(){
        boolean ret=true;
        String strJustification = binding.editTextTextPersonName2.getText().toString();

        if (binding.textView35.getSelectedItem().toString().trim() == "Select Department") {
            ret=false;
            Toast.makeText(AddItemQueryActivity.this, "Please select department", Toast.LENGTH_SHORT).show();
        } else if(TextUtils.isEmpty(strJustification)) {
            ret=false;
            Toast.makeText(AddItemQueryActivity.this,"Please enter justification", Toast.LENGTH_LONG).show();
        }

        return ret;
    }

    private void getDepartment() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<DeptModel> call = service.getDepartment();

        call.enqueue(new Callback<DeptModel>() {

            @Override
            public void onResponse(Call<DeptModel> call, retrofit2.Response<DeptModel> response) {
                if (response.isSuccessful()) {

                    departmentArr = response.body().getData();
                    ArrayList<String> items = new ArrayList<>();
                    items.add("Select Department");
                    for(int i= 0;i<departmentArr.size();i++){
                        Log.e("dataaa",""+departmentArr.get(i).getDeptName());
                        items.add(departmentArr.get(i).getDeptName());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddItemQueryActivity.this,
                            android.R.layout.simple_spinner_item,items);

                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.textView35.setAdapter(adapter);
                    binding.textView35.setOnItemSelectedListener(AddItemQueryActivity.this);

                } else {
                    Toast.makeText(AddItemQueryActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DeptModel> call, Throwable t) {
                Toast.makeText(AddItemQueryActivity.this,"failure",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getStock() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<StockModel> call = service.getStock();

        call.enqueue(new Callback<StockModel>() {

            @Override
            public void onResponse(Call<StockModel> call, retrofit2.Response<StockModel> response) {
                if (response.isSuccessful()) {

                    stockArr = response.body().getData();
                    ArrayList<String> items = new ArrayList<>();
                    items.add("Select Item Name");
                    for(int i= 0;i<stockArr.size();i++){
                        Log.e("dataaa",""+stockArr.get(i).getItemName());
                        items.add(stockArr.get(i).getItemName());
                    }
                    ArrayAdapter<String>adapter = new ArrayAdapter<String>(AddItemQueryActivity.this,
                            android.R.layout.simple_spinner_item,items);

                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner_item_name.setAdapter(adapter);
                    spinner_item_name.setOnItemSelectedListener(AddItemQueryActivity.this);

                } else {
                    Toast.makeText(AddItemQueryActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StockModel> call, Throwable t) {
                Toast.makeText(AddItemQueryActivity.this,"failure",Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void uploadPurposeImage(String path) {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
        //      showDialog(getActivity(),"Wait while loading...");

        MultipartBody.Part requestImage = null;
        if(path!=null){
            file = new File(path);
            Log.e("Register",""+file.getName());
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            requestImage = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://139.59.90.236:82/api/").addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<PurposeImageModel> call = service.uploadImage(requestImage);

        Logg("urlapipurposeImage", "http://139.59.90.236:82/api/" + "data?image=" + requestImage);

        call.enqueue(new Callback<PurposeImageModel>() {

            @Override
            public void onResponse(Call<PurposeImageModel> call, retrofit2.Response<PurposeImageModel> response) {
                if (response.isSuccessful()) {
                    progress.dismiss();
                    Log.e("response",""+response.body().getPath());
                    Log.e("response",""+response.body().getImage());
                    purposeImage = response.body().getImage();
                    tv_choose_file.setText(purposeImage);
                    img.add(purposeImage);
                    Log.e("imgArr", ""+img.size()+" - "+purposeImage+" - "+img.get(img.size()-1));
                    Log.e("response msg",""+response.body().getMessage());
                    Toast.makeText(AddItemQueryActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("msg",""+response.message());
                    Toast.makeText(AddItemQueryActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PurposeImageModel> call, Throwable t) {
                Log.e("onFailure",""+t.getMessage());
            }
        });
    }


    private void addItemQuery() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<AddItemModel> call = service.addItem(id, String.valueOf(jsonArray), binding.editTextTextPersonName2.getText().toString(), binding.textView35.getSelectedItem().toString(), binding.editTextTextPersonName6.getText().toString());

        Logg("urlapihomefragment", Constants.URL_LV + "data?id=" + id + "&item=" + String.valueOf(jsonArray)
                + "&justification=" + binding.editTextTextPersonName2.getText().toString() + "&department=" + binding.textView35.getSelectedItem().toString()
                + "&departmentHead=" +  binding.editTextTextPersonName6.getText().toString());

        call.enqueue(new Callback<AddItemModel>() {
            @Override
            public void onResponse(Call<AddItemModel> call, retrofit2.Response<AddItemModel> response) {
                if (response.isSuccessful()) {
                    showDialogAlert();
                    Log.e("msg",""+response.body().getMessage());
                    Toast.makeText(AddItemQueryActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                } else {
                    Log.e("errorrr",""+response.body());
                    Toast.makeText(AddItemQueryActivity.this, ""+response.body(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddItemModel> call, Throwable t) {
                Log.e("fail",""+t);
                Toast.makeText(AddItemQueryActivity.this,"failure",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showDialogAlert() {
        final Dialog dailogLogout = new Dialog(AddItemQueryActivity.this, android.R.style.Theme_Black_NoTitleBar);
        dailogLogout.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dailogLogout.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dailogLogout.setContentView(R.layout.dialog_alert);
        dailogLogout.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dailogLogout.setCanceledOnTouchOutside(true);

        TextView txt_no = dailogLogout.findViewById(R.id.textView135);

        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dailogLogout.dismiss();
                startActivity(new Intent(AddItemQueryActivity.this,ViewQueryActivity.class));
//                ViewQueryFragment someFragment = new ViewQueryFragment();
//                FragmentTransaction transaction = getFragmentManager().beginTransaction();
//                transaction.replace(R.id.container, someFragment);
//                transaction.addToBackStack(null);
//                transaction.commit();
            }
        });

        dailogLogout.show();
    }


    private boolean checkIfValidAndRead() {
        itemModels.clear();
        boolean result = true;
        Log.e("checkIfValidAndRead:", binding.itemLayout.getChildCount() + " ");

        for (int i = 0; i < binding.itemLayout.getChildCount(); i++) {
            Log.e("checkIfValidAndRead:", binding.itemLayout.getChildCount() + " ");
            View view = binding.itemLayout.getChildAt(i);

            spinner_item_name = view.findViewById(R.id.editTextTextPersonName);
            et_quantity = view.findViewById(R.id.editTextNumber);

            et_cost = view.findViewById(R.id.editTextNumber2);
            et_purpose = view.findViewById(R.id.editTextTextPersonName3);
            tv_choose_file = view.findViewById(R.id.textView46);
            btn_file = view.findViewById(R.id.button4);
            tv_existing_quantity = view.findViewById(R.id.textView48);
            tv_rate = view.findViewById(R.id.textView50);
            text_purchased = view.findViewById(R.id.textView51);
            et_purchased = view.findViewById(R.id.textView52);
            txt_finalcost = view.findViewById(R.id.textView53);
            et_final_cost = view.findViewById(R.id.textView54);

            AddItemModel.item addItemModel = new AddItemModel.item();

            if (!et_quantity.getText().toString().equals("")) {
                addItemModel.setQuantity(et_quantity.getText().toString());
                Log.e("Quantity", et_quantity.getText().toString() + " - " + i);
            } else {
                result = false;
                Toast.makeText(AddItemQueryActivity.this, "Please enter quantity", Toast.LENGTH_LONG).show();
                //       break;
            }

            if (!et_cost.getText().toString().equals("")) {
                addItemModel.setCost(et_cost.getText().toString());
                Log.e("Cost", et_cost.getText().toString() + " - " + i);
            } else {
                result = false;
                Toast.makeText(AddItemQueryActivity.this, "Please enter cost", Toast.LENGTH_LONG).show();
                //        break;
            }

            if (!et_purpose.getText().toString().equals("")) {
                addItemModel.setPurpose(et_purpose.getText().toString());
                Log.e("Purpose", et_purpose.getText().toString() + " - " + i);
            } else {
                result = false;
                Toast.makeText(AddItemQueryActivity.this, "Please enter purpose", Toast.LENGTH_LONG).show();
                //        break;
            }

            if (spinner_item_name.getSelectedItemPosition() != 0) {
                addItemModel.setItemName(spinner_item_name.getSelectedItem().toString());
                Log.e("ItemName", spinner_item_name.getSelectedItem().toString() + " - " + i);
            } else {
                result = false;
                Toast.makeText(AddItemQueryActivity.this, "Select Item name", Toast.LENGTH_LONG).show();
                //        break;
            }

            if (!tv_existing_quantity.getText().toString().equals("")) {
                addItemModel.setExistingStock(tv_existing_quantity.getText().toString());
                Log.e("existingQuantity", tv_existing_quantity.getText().toString() + " - " + i);
            } else {
                result = false;
                //        break;
            }

            if (!tv_rate.getText().toString().equals("")) {
                addItemModel.setNeedPurchase(tv_rate.getText().toString());
                Log.e("rate", tv_rate.getText().toString() + " - " + i);
            } else {
                result = false;
                //        break;
            }

            if (!tv_choose_file.getText().toString().equals("No file chosen")){
                addItemModel.setImage(img.get(i)+""+(i+1));
                Log.e("Image", img.get(i) + " - " + i);
                Log.e("file", file.getName() + " - " + i);
            } else {
                result = false;
                //        break;
            }

            if (!et_purchased.getText().toString().equals("")) {
                addItemModel.setLastPurchase(String.valueOf(et_purchased.getText().toString()));
                Log.e("last_purchase", et_purchased.getText().toString() + " - " + i);
            } else {
                result = false;
                //        break;
            }

            if (!et_final_cost.getText().toString().equals("")) {
                addItemModel.setFinalCost(String.valueOf(et_final_cost.getText().toString()));
                Log.e("finalCost", et_final_cost.getText().toString() + " - " + i);
            } else {
                result = false;
                //        break;
            }

            itemModels.add(addItemModel);

        }
        Log.e( "checkI size",itemModels.size()+" -" );
        for (int j = 0; j < itemModels.size(); j++) {
            JSONObject jsonObject = new JSONObject();
//            Log.e( "getItemName: ", itemModels.get(j).getItemName());
            try {
                jsonObject.put("item_name", itemModels.get(j).getItemName());
                jsonObject.put("quantity", itemModels.get(j).getQuantity());
                jsonObject.put("cost", itemModels.get(j).getCost());
                jsonObject.put("purpose", itemModels.get(j).getPurpose());
                jsonObject.put("image", itemModels.get(j).getImage());
                jsonObject.put("existing_stock", itemModels.get(j).getExistingStock());
                jsonObject.put("last_purchase", itemModels.get(j).getLastPurchase());
                jsonObject.put("need_purchase", itemModels.get(j).getNeedPurchase());
                jsonObject.put("final_cost", itemModels.get(j).getFinalCost());

            } catch (JSONException e) {
            }
            jsonArray.put(jsonObject);
            Log.e("JSONArray", String.valueOf(jsonArray));
        }

        return result;
    }


    private void addView(){
        final View itemView = getLayoutInflater().inflate(R.layout.add_item_view,null,false);
        img_close = itemView.findViewById(R.id.imageView15);
        spinner_item_name = itemView.findViewById(R.id.editTextTextPersonName);
        et_quantity = itemView.findViewById(R.id.editTextNumber);
        et_cost = itemView.findViewById(R.id.editTextNumber2);
        et_purpose = itemView.findViewById(R.id.editTextTextPersonName3);
        tv_choose_file = itemView.findViewById(R.id.textView46);
        btn_file = itemView.findViewById(R.id.button4);
        tv_existing_quantity = itemView.findViewById(R.id.textView48);
        tv_rate = itemView.findViewById(R.id.textView50);
        text_purchased = itemView.findViewById(R.id.textView51);
        et_purchased = itemView.findViewById(R.id.textView52);
        txt_finalcost = itemView.findViewById(R.id.textView53);
        et_final_cost = itemView.findViewById(R.id.textView54);

        btn_file.setOnClickListener(this);
        getStock();

        et_quantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!et_quantity.getText().toString().isEmpty() && !tv_existing_quantity.getText().toString().isEmpty()) {
                    if (Integer.parseInt(et_quantity.getText().toString()) > Integer.parseInt(tv_existing_quantity.getText().toString())) {
                        last_purchase = Integer.parseInt(et_quantity.getText().toString()) - Integer.parseInt(tv_existing_quantity.getText().toString());
                        text_purchased.setVisibility(View.VISIBLE);
                        et_purchased.setVisibility(View.VISIBLE);
                        txt_finalcost.setVisibility(View.VISIBLE);
                        et_final_cost.setVisibility(View.VISIBLE);
                        if (!et_cost.getText().toString().isEmpty()) {
                            final_cost = Integer.parseInt(et_quantity.getText().toString()) * Integer.parseInt(et_cost.getText().toString());
                        }
                        et_purchased.setText(String.valueOf(last_purchase));
                        et_final_cost.setText(String.valueOf(final_cost));
                        Log.e("last_purchase", String.valueOf(last_purchase));
                        Log.e("final_cost", String.valueOf(final_cost));
                    }
                    else {
                        text_purchased.setVisibility(View.GONE);
                        et_purchased.setVisibility(View.GONE);
                        txt_finalcost.setVisibility(View.GONE);
                        et_final_cost.setVisibility(View.GONE);
                    }
                }

            }
        });

        et_cost.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!et_quantity.getText().toString().isEmpty() && !et_cost.getText().toString().isEmpty() && !tv_existing_quantity.getText().toString().isEmpty()) {
                    if (Integer.parseInt(et_quantity.getText().toString()) > Integer.parseInt(tv_existing_quantity.getText().toString())) {
                       //et_quantity---> et_purchased(changed)
                        final_cost = Integer.parseInt(et_purchased.getText().toString()) * Integer.parseInt(et_cost.getText().toString());
                        et_final_cost.setText(String.valueOf(final_cost));
                    }
                }
                Log.e("approximateCost", String.valueOf(et_cost.getText().toString()));
                Log.e("finalCost", String.valueOf(final_cost));
            }
        });

        if (binding.itemLayout.getChildCount()==0){
            img_close.setVisibility(View.GONE);
        }

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LinearLayout)itemView.getParent()).removeView(itemView);
            }
        });
        binding.itemLayout.addView(itemView);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
        Log.e("parent", ""+parent.getId());

        if(parent.getId() == R.id.textView35) {
            String deptHeadName = "";
            if (position != 0) {
                deptHeadName = departmentArr.get(position - 1).getDeptHead();
                Log.e("deptHead", deptHeadName);
                binding.textView45.setVisibility(View.VISIBLE);
                binding.editTextTextPersonName6.setVisibility(View.VISIBLE);
                binding.editTextTextPersonName6.setText(deptHeadName);
            }
        } else if (parent.getId() == R.id.editTextTextPersonName){
            if (position != 0) {
                quantity = stockArr.get(position - 1).getQuantity();
                rate = stockArr.get(position - 1).getCost();
                tv_existing_quantity.setText(quantity);
                tv_rate.setText(rate);
                Log.e("quantity--->",quantity);
                Log.e("ratee--->",rate);
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // TODO Auto-generated method stub
    }

    public void internetconnection() {

        final Dialog dialog = new Dialog(AddItemQueryActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getDepartment();
                getStock();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button2:
                if (checkValidation()){
                    if (checkIfValidAndRead()){
                        addItemQuery();
                    }
                }
                break;

            case R.id.button3:
                et_quantity.setText("");
                et_cost.setText("");
                et_purpose.setText("");
                et_justification.setText("");
                tv_existing_quantity.setText("");
                tv_rate.setText("");
                et_purchased.setText("");
                et_final_cost.setText("");
                break;

            case R.id.button4:
                if (checkPermission()) {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_GALLERY);
                } else {
                    requestPermision();
                }
                break;

            case R.id.txt_add:
                addView();
                break;

            case R.id.img_back:
                onBackPressed();
                break;
        }
    }
}