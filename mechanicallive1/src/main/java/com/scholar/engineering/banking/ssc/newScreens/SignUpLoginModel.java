package com.scholar.engineering.banking.ssc.newScreens;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SignUpLoginModel {

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("status")
    @Expose
    public Long status;
    @SerializedName("sibling")
    @Expose
    public List<Sibling> sibling = null;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("class")
    @Expose
    public String _class;
    @SerializedName("section")
    @Expose
    public String section;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("admission_no")
    @Expose
    public String admissionNo;
    @SerializedName("dob")
    @Expose
    public String dob;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public List<Sibling> getSibling() {
        return sibling;
    }

    public void setSibling(List<Sibling> sibling) {
        this.sibling = sibling;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdmissionNo() {
        return admissionNo;
    }

    public void setAdmissionNo(String admissionNo) {
        this.admissionNo = admissionNo;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public static class Sibling {

        @SerializedName("id")
        @Expose
        public Long id;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("parent_id")
        @Expose
        public String parentId;
        @SerializedName("password")
        @Expose
        public String password;
        @SerializedName("xp")
        @Expose
        public Long xp;
        @SerializedName("level")
        @Expose
        public Long level;
        @SerializedName("std_roll")
        @Expose
        public String stdRoll;
        @SerializedName("Student_name")
        @Expose
        public String studentName;
        @SerializedName("Student_class")
        @Expose
        public String studentClass;
        @SerializedName("Student_section")
        @Expose
        public String studentSection;
        @SerializedName("school_id")
        @Expose
        public Long schoolId;
        @SerializedName("image")
        @Expose
        public Object image;
        @SerializedName("isActive")
        @Expose
        public Long isActive;
        @SerializedName("player_id")
        @Expose
        public Object playerId;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getParentId() {
            return parentId;
        }

        public void setParentId(String parentId) {
            this.parentId = parentId;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public Long getXp() {
            return xp;
        }

        public void setXp(Long xp) {
            this.xp = xp;
        }

        public Long getLevel() {
            return level;
        }

        public void setLevel(Long level) {
            this.level = level;
        }

        public String getStdRoll() {
            return stdRoll;
        }

        public void setStdRoll(String stdRoll) {
            this.stdRoll = stdRoll;
        }

        public String getStudentName() {
            return studentName;
        }

        public void setStudentName(String studentName) {
            this.studentName = studentName;
        }

        public String getStudentClass() {
            return studentClass;
        }

        public void setStudentClass(String studentClass) {
            this.studentClass = studentClass;
        }

        public String getStudentSection() {
            return studentSection;
        }

        public void setStudentSection(String studentSection) {
            this.studentSection = studentSection;
        }

        public Long getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(Long schoolId) {
            this.schoolId = schoolId;
        }

        public Object getImage() {
            return image;
        }

        public void setImage(Object image) {
            this.image = image;
        }

        public Long getIsActive() {
            return isActive;
        }

        public void setIsActive(Long isActive) {
            this.isActive = isActive;
        }

        public Object getPlayerId() {
            return playerId;
        }

        public void setPlayerId(Object playerId) {
            this.playerId = playerId;
        }
    }

}
