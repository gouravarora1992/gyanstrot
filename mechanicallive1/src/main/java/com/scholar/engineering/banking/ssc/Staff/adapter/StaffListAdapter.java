package com.scholar.engineering.banking.ssc.Staff.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ChatModelClass;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.StaffListModel;
import com.scholar.engineering.banking.ssc.databinding.ChatRecyclerItemBinding;
import com.scholar.engineering.banking.ssc.databinding.UserListRecylerBinding;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class StaffListAdapter extends RecyclerView.Adapter<StaffListAdapter.ViewHolder> {

    private Context context;
    private List<StaffListModel.Datum> staffList;

    public StaffListAdapter(Context context, List<StaffListModel.Datum> staffList) {
        this.context = context;
        this.staffList = staffList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        UserListRecylerBinding binding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),(R.layout.user_list_recyler),parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.binding.txtName.setText(staffList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return staffList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public UserListRecylerBinding binding;

        public ViewHolder(@NonNull UserListRecylerBinding binding) {
            super(binding.getRoot());
            this.binding=binding;

        }
    }

}
