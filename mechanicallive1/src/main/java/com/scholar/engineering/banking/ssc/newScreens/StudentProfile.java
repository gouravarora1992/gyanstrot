package com.scholar.engineering.banking.ssc.newScreens;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

//import com.androidquery.AQuery;
//import com.facebook.accountkit.AccountKitLoginResult;
//import com.facebook.accountkit.PhoneNumber;
//import com.facebook.accountkit.ui.AccountKitActivity;
//import com.facebook.accountkit.ui.AccountKitConfiguration;
//import com.facebook.accountkit.ui.LoginType;
//import com.facebook.accountkit.ui.SkinManager;
//import com.facebook.accountkit.ui.UIManager;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
//import com.sachinvarma.easylocation.EasyLocationInit;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;
import com.scholar.engineering.banking.ssc.utils.Utility;
//import com.soundcloud.android.crop.Crop;

//import org.greenrobot.eventbus.EventBus;
//import org.greenrobot.eventbus.Subscribe;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

public class StudentProfile extends AppCompatActivity
{
    CircleImageView iv_profile;

    TextView switchprofile;
    TextView personldetail,tvname,et_username,tvclass,tvclassection,tv_email,tvemailaddress,tv_collage,et_collage,
            tv_DOB,tvDOB;
    ImageLoader imageLoader;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    NetworkConnection nw;

    Context mcoxt;

    UserSharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_edit1);

        mcoxt = StudentProfile.this;
        preferences=UserSharedPreferences.getInstance(this);

        nw = new NetworkConnection(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Utility.applyFontForToolbarTitle(toolbar, this);


        iv_profile = (CircleImageView) findViewById(R.id.iv_profile);
        personldetail=findViewById(R.id.personldetail);
        tvname = (TextView) findViewById(R.id.tvname);
        et_username = (TextView) findViewById(R.id.et_username);
        tvclass=findViewById(R.id.tvclass);
        tvclassection=findViewById(R.id.tvclassection);
        tv_email=findViewById(R.id.tv_email);
        tvemailaddress=findViewById(R.id.tvemailaddress);
        tv_collage = (TextView) findViewById(R.id.tv_collage);
        et_collage = (TextView) findViewById(R.id.et_collage);
        tv_DOB=findViewById(R.id.tv_DOB);
        tvDOB=findViewById(R.id.tvDOB);


        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.user_default)
                .showImageForEmptyUri(R.drawable.user_default)
                .showImageOnFail(R.drawable.user_default)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();



        switchprofile=findViewById(R.id.switchprofile);
        if(preferences.getsiblingdata().length()>0) {
            switchprofile.setVisibility(View.VISIBLE);
            switchprofile.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mcoxt, SwitchProfile.class);
                    startActivity(intent);
                }
            });
        }
        else
        {
            switchprofile.setVisibility(View.GONE);
        }

        setTypeface();
    }

    @Override
    protected void onResume() {
        super.onResume();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(mcoxt));


        et_username.setText(preferences.getname());
        tvclassection.setText(preferences.getclass() + "-" + preferences.getsection());
        tvemailaddress.setText(preferences.getemail());
        et_collage.setText(preferences.getaddmissiono());
        tvDOB.setText(preferences.getdob());

        if (preferences.getimage().length() > 0) {
            imageLoader.getInstance().displayImage(preferences.getimage(), iv_profile, options, animateFirstListener);
        }

    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }


    private void setTypeface() {

        Typeface font_demi = Typeface.createFromAsset(mcoxt.getAssets(), "avenirnextdemibold.ttf");
        Typeface font_meduim = Typeface.createFromAsset(mcoxt.getAssets(), "avenirnextmediumCn.ttf");

        personldetail.setTypeface(font_demi);
        switchprofile.setTypeface(font_demi);
        tvclass.setTypeface(font_meduim);
        tvname.setTypeface(font_meduim);
        tv_email.setTypeface(font_meduim);
        tv_collage.setTypeface(font_meduim);
        tv_DOB.setTypeface(font_meduim);
        tvclassection.setTypeface(font_demi);
        et_username.setTypeface(font_demi);
        tvemailaddress.setTypeface(font_demi);
        et_collage.setTypeface(font_demi);
        tvDOB.setTypeface(font_demi);
    }

    public void internetconnection(final int i) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()== android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}