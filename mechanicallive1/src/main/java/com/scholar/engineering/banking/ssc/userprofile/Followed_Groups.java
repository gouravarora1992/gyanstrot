package com.scholar.engineering.banking.ssc.userprofile;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.adapter.Follow_Group_Adapter;
import com.scholar.engineering.banking.ssc.getset.gettr_settr;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Followed_Groups extends Fragment
{
	RecyclerView homelist;
	UserSharedPreferences pref;
	NetworkConnection nw;
	ArrayList<gettr_settr> disc;
	Follow_Group_Adapter d1;
	View v;
	String cate_id;
	SwipeRefreshLayout swipeRefreshLayout;
	Context context;

	@Override
	public void onAttach(Context context) {
		this.context=context;
		super.onAttach(context);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		v=inflater.inflate(R.layout.followed_groups,container,false);

		nw=new NetworkConnection(context);

		homelist=(RecyclerView)v.findViewById(R.id.recyclerView);
		LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
		homelist.setLayoutManager(mLayoutManager);
		homelist.setHasFixedSize(true);
		homelist.setNestedScrollingEnabled(false);

		swipeRefreshLayout = (SwipeRefreshLayout)v.findViewById(R.id.swipe_refresh_layout);

		pref=UserSharedPreferences.getInstance(getActivity());
		cate_id=pref.getcate_id();
		getGroups();

		swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				getGroups();
			}
		});

		return v;

	}

	private void getGroups() {

		disc=new ArrayList<gettr_settr>();
		String url=Constants.URL+"v3/group_followed_user.php?";
		Logg("url", url);

		RequestQueue que= Volley.newRequestQueue(context);

		//final String finalCmnt = cmnt;
		final StringRequest obj1=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

			@Override
			public void onResponse(String s) {
				// TODO Auto-generated method stub

				Logg("response",s);
				int size=s.length();
				try {

					JSONObject res=new JSONObject(s);

					if(res.has("discuss")) {

						JSONArray jr = res.getJSONArray("discuss");

						disc.clear();

						for (int i = 0; i < jr.length(); i++) {

							JSONObject jsonobj = jr.getJSONObject(i);

							//Logg("followstatus "+i,jsonobj.getString("follow_status"));

							disc.add(new gettr_settr(jsonobj.getString("topic"), jsonobj.getInt("comments"), jsonobj.getInt("id"),
									jsonobj.getString("member"), jsonobj.getString("follow_status"),
									jsonobj.getString("status"),jsonobj.getString("image"),jsonobj.getString("payment"),jsonobj.getString("payment_status")));

						}
					}

					d1=new Follow_Group_Adapter(context,disc,"mainpg");
					homelist.setAdapter(d1);

					d1.notifyDataSetChanged();

					swipeRefreshLayout.setRefreshing(false);

				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					//Logg("no",e.getMessage());
					swipeRefreshLayout.setRefreshing(false);
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError e) {
				// TODO Auto-generated method stub
				swipeRefreshLayout.setRefreshing(false);
			}
		}){

			@Override
			protected Map<String,String> getParams(){

				Map<String,String> params = new HashMap<String, String>();

				params.put("email",Constants.User_Email);

				return params;
			}
		};


		int socketTimeout =30000;//30 seconds - change to what you want
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		obj1.setRetryPolicy(policy);

		que.add(obj1);

	}


	public void internetconnection(final int i){

		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.network_alert);
		dialog.show();

		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
		TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

		tv_tryagain.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.dismiss();
				getGroups();

			}
		});

		iv_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

	}

}
