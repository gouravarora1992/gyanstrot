package com.scholar.engineering.banking.ssc.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
//import com.scholar.engineering.banking.ssc.Profile;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Replies_Common_Comment_homepage;
import com.scholar.engineering.banking.ssc.userprofile.UserProfile;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.DatabaseHandler;
import com.scholar.engineering.banking.ssc.utils.TouchImageView;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.Constants.addmissionno;

/**
 * Created by surender on 3/14/2017.
 */

public class Replies_Comment_Common_homepage_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context c;
    JSONArray data;
    JSONObject jsonobj,jsonfield;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options,optionsuser;
    int id;
    String postid;

    public Replies_Comment_Common_homepage_Adapter(Context c, JSONArray data) {
        this.c = c;
        this.data = data;
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.blankimage)
                .showImageForEmptyUri(R.drawable.congrrr)
                .showImageOnFail(R.drawable.congrrr)
                .cacheInMemory(false)
                .imageScaleType(ImageScaleType.EXACTLY)
                .cacheOnDisk(false)
                .considerExifParams(true)
                .build();

        optionsuser = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.user_default)
                .showImageForEmptyUri(R.drawable.user_default)
                .showImageOnFail(R.drawable.user_default)
                .cacheInMemory(false)
                .cacheOnDisk(false)
                .considerExifParams(true)
                .build();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

                View v0 = inflater.inflate(R.layout.replies_common_comment_list_item, viewGroup, false);

                viewHolder = new ViewHolder0(v0);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

                ViewHolder0 vh0 = (ViewHolder0) viewHolder;

                setComment(vh0, position);
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    private void setComment(final ViewHolder0 vh0, final int p) {

        try {
            Typeface font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");
            Typeface font_medium = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");

            jsonobj=data.getJSONObject(p);
            jsonfield=new JSONObject(jsonobj.getString("jsondata"));

            String time= Utility.getDate(jsonobj.getString("timestamp"));
            vh0.tv_time.setTypeface(font_medium);
            vh0.tv_collage.setTypeface(font_medium);
            vh0.tv_uname.setTypeface(font_demi);
            vh0.tv_comment.setTypeface(font_demi);

            vh0.tv_time.setText(time);
            vh0.tv_collage.setText(jsonfield.getString("collage")+"-"+jsonfield.getString("section"));
            vh0.tv_uname.setText(jsonfield.getString("name"));
            String com=jsonfield.getString("comment");
            com = com.replaceAll("%20", " ");
            vh0.tv_comment.setText(com);
            if(User_Email.equals(jsonobj.getString("uid"))){
                vh0.lay_delete.setVisibility(View.VISIBLE);
            }
            else
                vh0.lay_delete.setVisibility(View.GONE);

            if(!jsonfield.getString("userimage").equalsIgnoreCase("null")){
                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.init(ImageLoaderConfiguration.createDefault(c));
                imageLoader.getInstance().displayImage(jsonfield.getString("userimage"), vh0.userimg, optionsuser, animateFirstListener);
            }else
            vh0.userimg.setImageResource(R.drawable.user_default);

            if(!jsonfield.getString("com_image").equalsIgnoreCase("null")){
                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.init(ImageLoaderConfiguration.createDefault(c));
                imageLoader.getInstance().displayImage(Constants.URL_Image + jsonfield.getString("com_image"), vh0.cimage, options, animateFirstListener);
                vh0.cimage.setVisibility(View.VISIBLE);
            }
            else
                vh0.cimage.setVisibility(View.GONE);

            vh0.cimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (vh0.cimage.getDrawable() == null) {
                    } else {
                        imagedialog(vh0.cimage);
                    }
                }
            });

            vh0.userimg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        jsonobj=data.getJSONObject(p);
                        Intent in = new Intent(c, UserProfile.class);
                        in.putExtra("email",jsonobj.getString("uid"));
                        c.startActivity(in);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

            vh0.lay_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    deletcomment(p);
                }
            });

        } catch (JSONException e) {
            Log.e("exception","",e);
        }

    }

    public class ViewHolder0 extends RecyclerView.ViewHolder {
        TextView tv_uname;
        TextView tv_collage;
        TextView tv_time;
       TextView tv_comment;
        ImageView cimage;
        CircleImageView userimg;
        LinearLayout lay_delete;

        public ViewHolder0(View v) {
            super(v);

            tv_uname=(TextView)v.findViewById(R.id.usernameid);
            tv_collage=(TextView)v.findViewById(R.id.collegeid);
            tv_time=(TextView)v.findViewById(R.id.timeid);
            tv_comment=(TextView)v.findViewById(R.id.commentid);
            cimage=(ImageView)v.findViewById(R.id.commentimageid);
            userimg=(CircleImageView)v.findViewById(R.id.userimage_id);
            lay_delete=(LinearLayout)v.findViewById(R.id.lay_delete);

        }
    }

    public void deletcomment(final int a)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(c);

        alertDialogBuilder.setMessage("You want to delete this comment");
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                volleydeletcomment(a);
            }
        });

        alertDialogBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.show();

    }

    public void volleydeletcomment(final int a)
    {
        try {
            jsonobj=data.getJSONObject(a);
            id=jsonobj.getInt("id");
            postid=jsonobj.getString("postid");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue queue= Volley.newRequestQueue(c);

        JSONObject json =new JSONObject();
        try {
            json.put("id",id);
            json.put("postid",postid);
            json.put("email",User_Email);
            json.put("admission_no", addmissionno);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url=Constants.URL_LV+"replies_delete_common";
        Logg("replies_delete_common",url+","+json);
        JsonObjectRequest jsonreq=new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                Logg("replies_delete_common",res+"");
                try {
                    if(res.getString("scalar").equals("Record deleted successfully"))
                    {
                        Toast.makeText(c,"Comment deleted",Toast.LENGTH_SHORT).show();
                        data.remove(a);
                        notifyDataSetChanged();
                    }
                    else
                    {
                        Toast.makeText(c,"Try Again",Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                }
            }
        },new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg)
            {
                CommonUtils.toast(c, CommonUtils.volleyerror(arg));
            }
        });
        jsonreq.setShouldCache(false);
        int socketTimeout = 50000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonreq.setRetryPolicy(policy);
        queue.add(jsonreq);
    }

    public void imagedialog(ImageView im)
    {
        final Dialog dialog = new Dialog(c);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.imagedialog1);

        LinearLayout imglarge = (LinearLayout) dialog.findViewById(R.id.dialogimageid);
        ImageView cancel=(ImageView)dialog.findViewById(R.id.crossid);

        Bitmap bmp = ((BitmapDrawable) im.getDrawable()).getBitmap();
        TouchImageView img = new TouchImageView(c);
        img.setImageBitmap(bmp);
        img.setMaxZoom(4f);
        img.setMinimumWidth(Constants.Width);
        img.setMinimumHeight(Constants.Height);
        imglarge.addView(img);

        dialog.show();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

}
