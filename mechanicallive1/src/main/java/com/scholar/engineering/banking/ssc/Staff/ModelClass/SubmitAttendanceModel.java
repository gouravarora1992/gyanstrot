package com.scholar.engineering.banking.ssc.Staff.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubmitAttendanceModel {

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("status")
    @Expose
    public Boolean status;
    @SerializedName("data")
    @Expose
    public Data data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {

        @SerializedName("class_name")
        @Expose
        public String className;
        @SerializedName("class_section")
        @Expose
        public String classSection;
        @SerializedName("today_date")
        @Expose
        public String todayDate;

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getClassSection() {
            return classSection;
        }

        public void setClassSection(String classSection) {
            this.classSection = classSection;
        }

        public String getTodayDate() {
            return todayDate;
        }

        public void setTodayDate(String todayDate) {
            this.todayDate = todayDate;
        }
    }
}
