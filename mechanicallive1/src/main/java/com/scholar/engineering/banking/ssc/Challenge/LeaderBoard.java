package com.scholar.engineering.banking.ssc.Challenge;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.scholar.engineering.banking.ssc.HomeActivity;
import com.scholar.engineering.banking.ssc.MaterialSpinner.MaterialSpinner;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.EndlessRecyclerViewScrollListener;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

//import com.scholar.engineering.banking.ssc.adapter.News_All_Adapter;

public class LeaderBoard extends AppCompatActivity {
    RecyclerView list;
    LeaderBoard_Adapter adapter;
    ArrayList<Home_getset> homedata;
    ProgressDialog progress;
    String title, image, st_search = "";
    SwipeRefreshLayout swipeRefreshLayout;
    ImageView iv_search;
    EditText et_search;
    RelativeLayout footerlayout;
    TextView loadmore;
    ProgressWheel wheelbar;
    NetworkConnection nw;
    LinearLayout lay_search;
    public static String challenge_sub = "";
    Typeface font_demi;
    Typeface font_medium;
    MaterialSpinner sp_testcategory;
    ArrayList<String> time_list;
    String time = "This Month";
    EndlessRecyclerViewScrollListener endless;
    TextView emptyview;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.leader_board);

        nw = new NetworkConnection(this);

        font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");
        font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");


        time_list = new ArrayList<>();
        emptyview = findViewById(R.id.emptyview);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("LeaderBoard");
        Utility.applyFontForToolbarTitle(toolbar, this);

        lay_search = (LinearLayout) findViewById(R.id.lay_search);
        lay_search.setVisibility(View.GONE);

        sp_testcategory = (MaterialSpinner) findViewById(R.id.sp_testcategory);

        list = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(LeaderBoard.this);
//		mLayoutManager.scrollToPositionWithOffset(0,0);
        list.setLayoutManager(mLayoutManager);
//		list.setHasFixedSize(true);
//		list.setNestedScrollingEnabled(false);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        et_search = (EditText) findViewById(R.id.et_search);
        iv_search = (ImageView) findViewById(R.id.iv_search);
        homedata = new ArrayList<>();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
//                endless.resetState();
                getUsers();
            }
        });


        time_list.add("Today");
        time_list.add("This Month");
        time_list.add("This Year");
        time_list.add("All Time");

        sp_testcategory.setItems(time_list);


        getUsers();

        footerlayout = (RelativeLayout) findViewById(R.id.footer_layout);
        loadmore = (TextView) findViewById(R.id.moreButton);
        wheelbar = (ProgressWheel) findViewById(R.id.progress_wheel);

        sp_testcategory.setSelectedIndex(1);
        sp_testcategory.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                time = time_list.get(position);
//                endless.resetState();
                getUsers();
            }
        });

    }

    private void getUsers() {

        if (!nw.isConnectingToInternet()) {
            internetconnection(0);
            return;
        }


        progress = ProgressDialog.show(LeaderBoard.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progress.show();


        RequestQueue queue = Volley.newRequestQueue(LeaderBoard.this);
        String url = Constants.URL_LV + "leaderboard";
        url = url.replace(" ", "%20");
        Logg("name", url);

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                try {
                    Logg("response", s);
                    JSONObject res = new JSONObject(s);

                    if (res.getString("status").equalsIgnoreCase("true")) {
                        if (res.has("data")) {

                            if (res.getJSONObject("data").getJSONArray("users").length() > 0) {
                                emptyview.setVisibility(View.GONE);
                                list.setVisibility(View.VISIBLE);
                                JSONArray jr = res.getJSONObject("data").getJSONArray("users");
                                adapter = new LeaderBoard_Adapter(LeaderBoard.this, jr);
                                list.setAdapter(adapter);
                            } else {
                                list.setVisibility(View.GONE);
                                emptyview.setVisibility(View.VISIBLE);
                            }
                            wheelbar.setVisibility(View.GONE);
                        } else {
                            list.setVisibility(View.GONE);
                            emptyview.setVisibility(View.VISIBLE);
                            wheelbar.setVisibility(View.GONE);
                        }
                    } else {
                        list.setVisibility(View.GONE);
                        emptyview.setVisibility(View.VISIBLE);
                        wheelbar.setVisibility(View.GONE);
                    }
                    if (progress != null)
                        if (progress.isShowing())
                            progress.dismiss();

                    swipeRefreshLayout.setRefreshing(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (progress != null)
                        if (progress.isShowing())
                            progress.dismiss();

                    wheelbar.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(LeaderBoard.this, CommonUtils.volleyerror(volleyError));
                if (progress != null)
                    if (progress.isShowing())
                        progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", Constants.User_Email);
                params.put("time", time);
                params.put("admission_no", Constants.addmissionno);
                Logg("submit params", params + "");
                return params;
            }
        };

        request.setShouldCache(false);
        int socketTimeout = 0;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }


    public void internetconnection(final int i) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                getUsers();

//				getData();

            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

}
