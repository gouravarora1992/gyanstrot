package com.scholar.engineering.banking.ssc.Staff.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.GroupChatActivity;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ChatModelClass;
import com.scholar.engineering.banking.ssc.databinding.ChatRecyclerItemBinding;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    private Context context;
    private ChatModelClass[] listdata;

    public ChatAdapter(Context context, ChatModelClass[] listdata) {
        this.context = context;
        this.listdata = listdata;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ChatRecyclerItemBinding binding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),(R.layout.chat_recycler_item),parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ChatModelClass myListData = listdata[position];
        holder.binding.txtName.setText(listdata[position].getName());
        holder.binding.imgPic.setImageResource(listdata[position].getImgId());

        holder.binding.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, GroupChatActivity.class));
            }
        });
    }


    @Override
    public int getItemCount() {
        return listdata.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ChatRecyclerItemBinding binding;

        public ViewHolder(@NonNull ChatRecyclerItemBinding binding) {
            super(binding.getRoot());
            this.binding=binding;

        }
    }

}
