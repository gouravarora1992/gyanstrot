package com.scholar.engineering.banking.ssc.Staff.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.SupportSystemResponseModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SystemResponseAdapter extends RecyclerView.Adapter<SystemResponseAdapter.ViewHolder> {

    private Context context;
    private SupportSystemResponseModel systemResponseModel;
    public static final String IMAGE_BASE_URL = "http://139.59.90.236:82/images/";

    public SystemResponseAdapter(Context context, SupportSystemResponseModel systemResponseModel) {
        this.context = context;
        this.systemResponseModel = systemResponseModel;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_system_response,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        SupportSystemResponseModel.Datum responseModel = systemResponseModel.getData().get(position);
        holder.txt_teacher.setText(responseModel.getTeachername());
        holder.txt_deadline.setText(responseModel.getDeadline());
        holder.txt_response.setText(responseModel.getResponse());
        String imageUrl = IMAGE_BASE_URL + responseModel.getFeedbackfile();
        Glide.with(context).load(imageUrl).into(holder.img_file);
        //     sovled : 2 ,unsolved : 3 , process : 5
        if (responseModel.getStatus().toString().equals("2")) {
            holder.txt_status.setText("solved");
        } else if (responseModel.getStatus().toString().equals("3")) {
            holder.txt_status.setText("unsolved");
        } else if (responseModel.getStatus().toString().equals("5")) {
            holder.txt_status.setText("process");
        }

        try {
            String date1 = responseModel.getCreatedAt();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = null;
            try {
                date = dateFormat.parse(date1);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            DateFormat formatter = new SimpleDateFormat("dd-MMM-yy");
            String dateStr = formatter.format(date);
            holder.txt_date.setText(dateStr);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            String date2 = responseModel.getCreatedAt();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = null;
            try {
                date = dateFormat.parse(date2);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            DateFormat formatter = new SimpleDateFormat("dd-MMMM-yy");
            String dateStr = formatter.format(date);
            holder.txt_query_date.setText(dateStr);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            if (responseModel.getDeadline() != null) {
                String date1 = responseModel.getDeadline();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = null;
                try {
                    date = dateFormat.parse(date1);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                String dateStr = formatter.format(date);
                holder.txt_deadline.setText(dateStr);
            }
        } catch(Exception e) {
            e.printStackTrace();

        }
    }


    @Override
    public int getItemCount() {
        return systemResponseModel.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_deadline, txt_response, txt_teacher;
        TextView txt_query_date;
        TextView txt_status;
        TextView txt_date;
        ImageView img_file;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.txt_teacher = (TextView) itemView.findViewById(R.id.txt_teacher);
            this.txt_response = (TextView) itemView.findViewById(R.id.txt_response);
            this.txt_deadline = (TextView) itemView.findViewById(R.id.txt_deadline);
            this.txt_query_date = (TextView) itemView.findViewById(R.id.txt_query_date);
            this.txt_status = (TextView) itemView.findViewById(R.id.txt_status);
            this.txt_date = (TextView) itemView.findViewById(R.id.txt_date);
            this.img_file = (ImageView) itemView.findViewById(R.id.img_file);

        }
    }

}
