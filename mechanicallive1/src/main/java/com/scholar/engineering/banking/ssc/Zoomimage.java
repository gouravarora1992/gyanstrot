package com.scholar.engineering.banking.ssc;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.TouchImageView;
//import com.squareup.picasso.Callback;
//import com.squareup.picasso.Picasso;

/**
 * Created by rohan on 2/18/2016.
 */

public class Zoomimage extends Activity
{
    ImageView image,delete;
    RelativeLayout image_layout;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    String pic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zoomimage);

        image_layout=(RelativeLayout)findViewById(R.id.relativeLayout3);
        //image=(ImageView)findViewById(R.id.imageView12);
        delete=(ImageView)findViewById(R.id.imageView10);

        Intent in=getIntent();
        pic=in.getStringExtra("pic");
        Display display = getWindowManager().getDefaultDisplay();

      final int w=display.getWidth();
        final int h=display.getHeight();
    image=new ImageView(Zoomimage.this);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.blankimage)
                .showImageForEmptyUri(R.drawable.congrrr)
                .showImageOnFail(R.drawable.congrrr)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(Zoomimage.this));
        imageLoader.getInstance().displayImage(Constants.URL_Image+"bookimage/"+pic, image, options, animateFirstListener);


//        Picasso.with(Zoomimage.this)
//                .load(Constants.URL+"bookimage/"+pic)
//                .error(R.drawable.congrrr)
//                .into(image, new Callback() {
//                    @Override
//                    public void onSuccess() {
//                        //Logg("success", Constants.URL+"bookimage/"+pic);
//                        Bitmap bmp = ((BitmapDrawable) image.getDrawable()).getBitmap();
//                        TouchImageView img = new TouchImageView(Zoomimage.this);
//                        // img.setImageResource(R.drawable.img1);
//                        img.setImageBitmap(bmp);
//                        img.setMaxZoom(5f);
//                        img.setMinimumHeight(h);
//                        img.setMinimumWidth(w);
//
////                        image_layout.removeAllViews();
//                        image_layout.addView(img);
                   // }

//                    @Override
//                    public void onError() {
//                        //Logg("success", "error");
//                    }
//                });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backview();
            }
        });

    }

    public void backview()
    {
        finish();
    }

}
