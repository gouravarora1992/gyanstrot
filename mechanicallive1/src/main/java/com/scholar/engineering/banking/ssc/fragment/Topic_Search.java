package com.scholar.engineering.banking.ssc.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
//import com.scholar.engineering.banking.ssc.AdModule.AdmobRecyclerAdapterWrapper;
import com.scholar.engineering.banking.ssc.File_download;
import com.scholar.engineering.banking.ssc.HomeActivity;
import com.scholar.engineering.banking.ssc.Jobs_all;
import com.scholar.engineering.banking.ssc.News;
import com.scholar.engineering.banking.ssc.Onlineexam;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Workshoplist;
import com.scholar.engineering.banking.ssc.adapter.Topic_Search_Adapter;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Topic_Search extends Fragment implements View.OnClickListener {

    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    Topic_Search_Adapter adapter;
    Typeface font_quiksand;
    LinearLayout joblay,testlay,managegroup_lay,event_lay,post_lay,pdf_lay;
    TextView post_tv,event_tv,file_tv,tv_group;

    NetworkConnection nw;

    HomeActivity activity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity= (HomeActivity) context;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.shareid:
//                Intent in = new Intent(activity, Share_and_Earn.class);
//                startActivity(in);
//                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

//        inflater.inflate(R.menu.menu, menu);
//
//        MenuItem playMenu = menu.findItem(R.id.shareid);
//        MenuItem src = menu.findItem(R.id.search);
//
//        src.setCheckable(false);
//
//        if (Constants.Width <= 600)
//            playMenu.setIcon(R.drawable.dollar);
//        else
//            playMenu.setIcon(R.drawable.dollar);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view =inflater.inflate(R.layout.topics_search,container,false);

        Logg("oncreate","topic");
        setHasOptionsMenu(true);

        nw=new NetworkConnection(activity);

        font_quiksand = Typeface.createFromAsset(activity.getAssets(), "quicksand_regular.ttf");

        tv_group=(TextView)view.findViewById(R.id.tv_group);
        tv_group.setTypeface(font_quiksand);

        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
        recyclerView=(RecyclerView) view.findViewById(R.id.recyclerView);
        GridLayoutManager mLayoutManager = new GridLayoutManager(activity,2);
        recyclerView.setLayoutManager(mLayoutManager);


        post_tv=(TextView)view.findViewById(R.id.post_tvid);
        event_tv=(TextView)view.findViewById(R.id.event_tvid);
        file_tv=(TextView)view.findViewById(R.id.file_tvid);
        managegroup_lay=(LinearLayout)view.findViewById(R.id.group_layid);
        pdf_lay=(LinearLayout)view.findViewById(R.id.pdf_layid);
        post_lay=(LinearLayout)view.findViewById(R.id.post_layid);
        joblay=(LinearLayout)view.findViewById(R.id.job_layid);
        testlay=(LinearLayout)view.findViewById(R.id.test_layid);
        event_lay=(LinearLayout)view.findViewById(R.id.event_layid);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData("onrefresh");
            }
        });

        handler.sendEmptyMessageDelayed(1, 1000);
        joblay.setOnClickListener(this);
        managegroup_lay.setOnClickListener(this);
        testlay.setOnClickListener(this);
        post_lay.setOnClickListener(this);
        event_lay.setOnClickListener(this);
        pdf_lay.setOnClickListener(this);
        return view;

    }

    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    getData("onhandler");
                default:
                    break;
            }
            return false;
        }
    });

    @Override
    public void onResume() {
        super.onResume();
        getData("oncreate");
    }

    private void getData(String type) {

        Logg("calledfrom",type);
        RequestQueue queue= Volley.newRequestQueue(activity);

        String url= Constants.URL_LV+"gettopics";
        url=url.replace(" ","%20");
        Logg("name",url);

        StringRequest request=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s)
            {
                swipeRefreshLayout.setRefreshing(false);
                Logg("topic response",s);
                Log.e("topic_response-->",""+s);
                try {
                    JSONObject res=new JSONObject(s);

                    if(res.has("data")) {

                        JSONArray jr = res.getJSONArray("data");

                        if (jr.length() > 0) {
                            adapter = new Topic_Search_Adapter(activity, jr);
                            recyclerView.setAdapter(adapter);
                        }
                    }
                }

                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(getActivity(),CommonUtils.volleyerror(volleyError));
            }
        })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email",Constants.User_Email);
                params.put("admission_no",Constants.addmissionno);
                params.put("type","0");
                Logg("submit params", params + "");
                return params;
            }
        };
        int socketTimeout =0;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    public void internetconnection(){

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                swipeRefreshLayout.setRefreshing(true);
                dialog.dismiss();
                getData("dataconnection");


            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }



    @Override
    public void onClick(View v) {

        if(v.getId()==R.id.group_layid)
        {
//            Intent i=new Intent(activity,Category_List_Activity.class);
//            i.putExtra("class","mainpg");
//            startActivity(i);
        }
        else if(v.getId()==R.id.event_layid)
        {
            Intent i=new Intent(activity,Workshoplist.class);
            startActivity(i);
        }
        else if(v.getId()==R.id.test_layid)
        {
            Intent i=new Intent(activity,Onlineexam.class);
            startActivity(i);
        }
        else if(v.getId()==R.id.job_layid)
        {
            Intent i=new Intent(activity,Jobs_all.class);
            startActivity(i);
        }
        else if(v.getId()==R.id.post_layid)
        {
            Intent i=new Intent(activity,News.class);
            startActivity(i);
        }
        else if(v.getId()==R.id.pdf_layid)
        {
            Intent i=new Intent(activity, File_download.class);
            startActivity(i);
        }
    }
}
