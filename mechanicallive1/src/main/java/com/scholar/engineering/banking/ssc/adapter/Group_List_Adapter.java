package com.scholar.engineering.banking.ssc.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.scholar.engineering.banking.ssc.Group_Profile;
import com.scholar.engineering.banking.ssc.Post_comment_ingroup;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Transaction;
import com.scholar.engineering.banking.ssc.getset.gettr_settr;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.DatabaseHandler;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Group_List_Adapter extends BaseAdapter {

	ArrayList<gettr_settr> dtopics;

	DatabaseHandler dbh;
	UserSharedPreferences playerid;

	Context cs;
	String groupname,mem,check_clas;
	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
	private DisplayImageOptions options;
	ImageLoader imageLoader;

	public Group_List_Adapter(Context cs, ArrayList<gettr_settr> disc,String check_clas) {
		// TODO Auto-generated constructor stub
		this.dtopics=disc;
		this.cs=cs;
		this.check_clas=check_clas;
		playerid=new UserSharedPreferences(cs,"playerid");

		imageLoader = ImageLoader.getInstance();
		imageLoader.init(ImageLoaderConfiguration.createDefault(cs));

		options = new DisplayImageOptions.Builder()
				.displayer(new RoundedBitmapDisplayer(6))
				.showImageOnLoading(R.drawable.blankimage)
				.showImageForEmptyUri(R.drawable.congrrr)
				.showImageOnFail(R.drawable.congrrr)
				.cacheInMemory(false)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.build();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dtopics.size();
	}
	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int an, View bn, ViewGroup cn) {
		// TODO Auto-generated method stub

		final Context css=cn.getContext();
		Typeface font_demi = Typeface.createFromAsset(css.getAssets(), "avenirnextdemibold.ttf");
		Typeface font_medium = Typeface.createFromAsset(css.getAssets(), "avenirnextmediumCn.ttf");
		LayoutInflater li=(LayoutInflater)css.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		bn=li.inflate(R.layout.discussionadapter,cn,false);
		final TextView d1=(TextView)bn.findViewById(R.id.dismainid);
		TextView totalcom=(TextView)bn.findViewById(R.id.discusscountid);
        TextView tv_price=(TextView)bn.findViewById(R.id.tv_price);
		ImageView image=(ImageView) bn.findViewById(R.id.imageView1);
		final ProgressWheel wheel=(ProgressWheel)bn.findViewById(R.id.progress_wheel);

		dbh=new DatabaseHandler(cs);

		final TextView follow=(TextView)bn.findViewById(R.id.followid);
		LinearLayout layout=(LinearLayout)bn.findViewById(R.id.layoutid);
		follow.setTypeface(font_demi);

		d1.setText(dtopics.get(an).getTopic());
		//final int to=dtopics.get(an).getComment();
		totalcom.setText(dtopics.get(an).getMember()+" aspirants following");
		totalcom.setTypeface(font_medium);
		d1.setTypeface(font_demi);

        if(Integer.valueOf(dtopics.get(an).getPayment())>0)
        {
            tv_price.setVisibility(View.VISIBLE);
            tv_price.setText("Premium:  Rs. "+dtopics.get(an).getPayment());
        }
        else
            tv_price.setVisibility(View.GONE);

		//Logg("group_status",dtopics.get(an).getStartdate());

		if(dtopics.get(an).getImage().equalsIgnoreCase("null")|dtopics.get(an).getImage().equalsIgnoreCase("")){
		}
		else{
			imageLoader.getInstance().displayImage(Constants.URL_Image+"groupimage/"+dtopics.get(an).getImage(), image, options, animateFirstListener);
		}

		if(dtopics.get(an).getStartdate().equalsIgnoreCase("follow"))
		{
			follow.setText("Unfollow");
			follow.setTextColor(cs.getResources().getColor(R.color.blue_color));
			follow.setBackground( cs.getResources().getDrawable(R.drawable.background_strok_blue));
		}

		follow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				groupname=dtopics.get(an).getTopic();
               //Logg("group_id_name",dtopics.get(an).getId()+" "+groupname);
				//Logg("group_status",dtopics.get(an).getStartdate()+"");
				//Logg("group_payment",dtopics.get(an).getPayment()+"");
				//Logg("group_paymentstatus",dtopics.get(an).getPayment_status()+"");

				if(dtopics.get(an).getStartdate().equalsIgnoreCase("unfollow"))
				{
					//volleyupdatefollow(dtopics.get(an).getId(),"follow",an);
					// check payment

					if(Integer.valueOf(dtopics.get(an).getPayment())>0)
					{
						if(dtopics.get(an).getPayment_status().equalsIgnoreCase("no")){
							// payment

							Transaction t = new Transaction(cs,"9",String.valueOf(dtopics.get(an).getId()),Integer.valueOf(dtopics.get(an).getPayment()));
							t.PAYMENT_REQUEST(cs);

//							Intent in=new Intent(cs,Transaction.class);
//							in.putExtra("postid",String.valueOf(dtopics.get(an).getId()));
//							in.putExtra("field_type","9");
//							in.putExtra("amount",Integer.valueOf(dtopics.get(an).getPayment()));
//							cs.startActivity(in);

						}
						else {
							wheel.setVisibility(View.VISIBLE);
							follow.setVisibility(View.GONE);


                            volley_follow(dtopics.get(an).getId(), "follow", an);
						}
					}
					else {
						wheel.setVisibility(View.VISIBLE);
						follow.setVisibility(View.GONE);
						volley_follow(dtopics.get(an).getId(), "follow", an);
					}
				}
				else
				{
					wheel.setVisibility(View.VISIBLE);
					follow.setVisibility(View.GONE);
					volley_follow(dtopics.get(an).getId(),"unfollow",an);
//					volleyupdatefollow(dtopics.get(an).getId(), "unfollow", an);
				}

			}
		});

		layout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
                //Logg("check_class",check_clas);
				//Logg("followstatus",dtopics.get(an).getStartdate());
				//Logg("payment",dtopics.get(an).getPayment());

				if(check_clas.equalsIgnoreCase("mainpg")==true) {

                    if(dtopics.get(an).getStartdate().equalsIgnoreCase("unfollow")) {

                        if (Integer.valueOf(dtopics.get(an).getPayment()) > 0) {

                            if (dtopics.get(an).getPayment_status().equalsIgnoreCase("no")) {
								Transaction t = new Transaction(cs,"9",String.valueOf(dtopics.get(an).getId()),Integer.valueOf(dtopics.get(an).getPayment()));
								t.PAYMENT_REQUEST(cs);
//                            	Intent in=new Intent(cs,Transaction.class);
//								in.putExtra("postid",String.valueOf(dtopics.get(an).getId()));
//								in.putExtra("field_type","9");
//								in.putExtra("amount",Integer.valueOf(dtopics.get(an).getPayment()));
//								cs.startActivity(in);
                            }
                            else
                            {
                                //Logg("groupname ", d1.getText().toString() + " postid " + dtopics.get(an).getId() + "");
                                Intent disitem = new Intent(css, Group_Profile.class);//Group_Comment_Activity
                                disitem.putExtra("groupname", d1.getText());
                                disitem.putExtra("pid", String.valueOf(dtopics.get(an).getId()));
                                css.startActivity(disitem);
                            }
                        }
                        else{
                            //Logg("groupname ", d1.getText().toString() + " postid " + dtopics.get(an).getId() + "");
                            Intent disitem = new Intent(css, Group_Profile.class);//Group_Comment_Activity
                            disitem.putExtra("groupname", d1.getText());
                            disitem.putExtra("pid", String.valueOf(dtopics.get(an).getId()));
                            css.startActivity(disitem);
                        }
                    }
                    else {

                        //Logg("groupname ", d1.getText().toString() + " postid " + dtopics.get(an).getId() + "");
                        Intent disitem = new Intent(css, Group_Profile.class);//Group_Comment_Activity
                        disitem.putExtra("groupname", d1.getText());
                        disitem.putExtra("pid", String.valueOf(dtopics.get(an).getId()));
                        css.startActivity(disitem);
                    }

				}

//				else
//					Toast.makeText(css,"Please Follow The Group",Toast.LENGTH_LONG).show();
			}
		});

		return bn;
	}

//	public void volleyupdatefollow(final int id, String check, final int position)
//	{
//		RequestQueue queue= Volley.newRequestQueue(cs);
//		JSONObject json =new JSONObject();
//		String url= Constants.URL+"newapi2_04/update_group.php?id="+id+"&check="+check+"&email="+Constants.User_Email;
//        url=url.replaceAll(" ","%20");
//		//Logg("follow_url",url);
//		JsonObjectRequest jsonreq=new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {
//
//			@Override
//			public void onResponse(JSONObject res) {
//
//				// TODO Auto-generated method stub
//				String s;
//				try {
//
//					prefeditor=pref.edit();
//
//					if(res.getString("scalar").equals("Follow")==true)
//					{
//						mem=dtopics.get(position).getMember();
//
//						int m=Integer.valueOf(mem);
//						m++;
//
//						dbh.unfollowgroup(id,"follow");
//						dtopics.get(position).setStartdate("follow");
//						dtopics.get(position).setMember(String.valueOf(m));
//						notifyDataSetChanged();
//					}
//					else if(res.getString("scalar").equals("Unfollow")==true)
//					{
//						mem=dtopics.get(position).getMember();
//
//						int m=Integer.valueOf(mem);
//						m--;
//
//						dbh.unfollowgroup(id,"unfollow");
//						dtopics.get(position).setStartdate("unfollow");
//						dtopics.get(position).setMember(String.valueOf(m));
//						notifyDataSetChanged();
//
//					}
//					else
//					{
//						Toast.makeText(cs,"Server Issue",Toast.LENGTH_SHORT).show();
//					}
//
//					prefeditor.commit();
//
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//			}
//		},new Response.ErrorListener() {
//
//			@Override
//			public void onErrorResponse(VolleyError arg)
//			{
//				//Logg("error", String.valueOf(arg));
//				Toast.makeText(cs,"Network Problem", Toast.LENGTH_SHORT).show();
//			}
//		});
//
//		queue.add(jsonreq);
//	}

	private void volley_follow(final int id, final String check, final int position) {
		// TODO Auto-generated method stub


		//Logg("player_id",prefsignup.getString("playerid"," "));

		String url=Constants.URL+"newapi2_04/update_group.php?";

		Logg("url", url);

		RequestQueue que= Volley.newRequestQueue(cs);

		//final String finalCmnt = cmnt;
		StringRequest obj1=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

			@Override
			public void onResponse(String s) {
				// TODO Auto-generated method stub

				//Logg("response",s);
				int size=s.length();
				try {

					JSONObject res=new JSONObject(s);

//					prefeditor=pref.edit();

					if(res.getString("scalar").equals("Follow")==true)
					{
						mem=dtopics.get(position).getMember();

						int m=Integer.valueOf(mem);
						m++;

						dbh.unfollowgroup(id,"follow");
						dtopics.get(position).setStartdate("follow");
						dtopics.get(position).setMember(String.valueOf(m));
						notifyDataSetChanged();

					}
					else if(res.getString("scalar").equals("Unfollow")==true)
					{
						mem=dtopics.get(position).getMember();

						int m=Integer.valueOf(mem);

						m--;

						dbh.unfollowgroup(id,"unfollow");
						dtopics.get(position).setStartdate("unfollow");
						dtopics.get(position).setMember(String.valueOf(m));

						notifyDataSetChanged();

					}
					else
					{
						Toast.makeText(cs,"Server Issue",Toast.LENGTH_SHORT).show();
					}

//					prefeditor.commit();

				} catch (JSONException e) {
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError e) {
				// TODO Auto-generated method stub
				//progress.dismiss();
				//Logg("error",e.toString());
			}
		}){

			@Override
			protected Map<String,String> getParams(){
				Map<String,String> params = new HashMap<String, String>();
				//id="+id+"&check="+check+"&email="+Constants.User_Email;
				params.put("id", id+"");
				params.put("check", check);
				params.put("email",Constants.User_Email);
				params.put("playerid",playerid.getplayerid());

				return params;
			}
		};

		int socketTimeout =30000;//30 seconds - change to what you want
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		obj1.setRetryPolicy(policy);

		que.add(obj1);

	}

}
