package com.scholar.engineering.banking.ssc;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;

import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
//import com.androidquery.AQuery;
//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.NativeExpressAdView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.adapter.News_related_post_adapter;
import com.scholar.engineering.banking.ssc.adapter.Replies_on_group_comment_adap;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.ImageCompression;
import com.scholar.engineering.banking.ssc.utils.ImageUploading;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.Constants.MY_PERMISSIONS_REQUEST;
import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;
import static com.scholar.engineering.banking.ssc.utils.Constants.imageFilePath;
import static com.scholar.engineering.banking.ssc.utils.Constants.st_base46;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.Utility.hasPermissions;

public class Webview_internal_link extends Activity implements OnClickListener
{
	Intent detail1;
	EditText et_comment;
	ArrayList<Home_getset> homedata;
	TextView tv_title,tv_postby_name,tv_post_date,tv_commentcount,tv_like,tv_share;
	ImageView nimage,postcom,camera_img,iv_like;
	String title,comment,image,date;
	RecyclerView list,related_list;
	LinearLayout layout_progress;
	//AQuery aquery;
	String url,email,posturl,index="0";
	Replies_on_group_comment_adap adap;
	int postid;
	ProgressDialog progress;
	Dialog progress1;
	Home_getset data;
	WebView webview;
	LinearLayout lay_commentpost,lay_like,lay_comment,lay_share;
	String st_imagename,posttype="1";
	Uri selectedImage;
	Bitmap bitmap;
	RelativeLayout loadmore_lay;
	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
	private DisplayImageOptions options;
	NetworkConnection net_con;
	ImageView comm, share;
	TextView moreButton;
	JSONArray jsonArray;
	JSONObject jsonobj;
	Typeface font_demi,font_medium;
	NestedScrollView nestedScrollView;
	NetworkConnection nw;

	ImageUploading ab;
	private final int REQUEST_CODE_CLICK_IMAGE = 1002, REQUEST_CODE_GALLERY_IMAGE = 1003;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview_for_internal_link);

		nw=new NetworkConnection(this);

		st_base46="";
		Constants.imageFilePath= CommonUtils.getFilename();

		font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");
		font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");
		list=(RecyclerView) findViewById(R.id.recyclerView);
		layout_progress=(LinearLayout)findViewById(R.id.progress_layout);
		LinearLayoutManager mLayoutManager = new LinearLayoutManager(Webview_internal_link.this);
		mLayoutManager.scrollToPositionWithOffset(0,0);
		list.setLayoutManager(mLayoutManager);

		list.setHasFixedSize(true);
		list.setNestedScrollingEnabled(false);

		related_list=(RecyclerView) findViewById(R.id.recyclerView_relatedpost);
		LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(Webview_internal_link.this);
		mLayoutManager1.scrollToPositionWithOffset(0,0);
		mLayoutManager1.setOrientation(LinearLayoutManager.HORIZONTAL);
		related_list.setLayoutManager(mLayoutManager1);
		related_list.setHasFixedSize(true);
		related_list.setNestedScrollingEnabled(false);

		//Logg("solar1 oncr","solar1 on create");
		comm=(ImageView)findViewById(R.id.iv_comm);
		share=(ImageView)findViewById(R.id.iv_share);
		comm.setImageResource(R.drawable.ic_message);
		share.setImageResource(R.drawable.ic_share);
		tv_title=(TextView)findViewById(R.id.headline);
		tv_commentcount=(TextView)findViewById(R.id.tv_commentid);
		tv_like=(TextView)findViewById(R.id.tv_like);
		tv_share=(TextView)findViewById(R.id.homenewsshareid);
		tv_share.setTypeface(font_medium);
		tv_commentcount.setTypeface(font_medium);
		tv_title.setTypeface(font_demi);
		tv_like.setTypeface(font_medium);

		loadmore_lay=(RelativeLayout)findViewById(R.id.footer_layout);

		tv_postby_name=(TextView)findViewById(R.id.admin);

		tv_post_date=(TextView)findViewById(R.id.date);

		et_comment=(EditText)findViewById(R.id.comtextid);

		nimage=(ImageView)findViewById(R.id.newsimage);

		postcom=(ImageView)findViewById(R.id.postcomid);

		iv_like=(ImageView)findViewById(R.id.likeimageid);

		camera_img=(ImageView)findViewById(R.id.iv_camera_id);

		webview=(WebView)findViewById(R.id.webviewid);

		lay_comment=(LinearLayout)findViewById(R.id.lay_comment);

		lay_like=(LinearLayout)findViewById(R.id.lay_like);
		lay_share=(LinearLayout)findViewById(R.id.lay_shareid);
		lay_commentpost=(LinearLayout)findViewById(R.id.lay_commentpost);

		moreButton=(TextView)findViewById(R.id.moreButton);

//		if(Home_RecyclerViewAdapter.progress!=null)
//			Home_RecyclerViewAdapter.progress.dismiss();

		progress1 = new Dialog(Webview_internal_link.this,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
		progress1.setContentView(R.layout.progressdialog_dot);
		//progress1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//		progress1.show();

		layout_progress.setVisibility(View.VISIBLE);

		detail1=getIntent();

		postid=detail1.getIntExtra("postid",0);

//		Replies.psid=postid;

		email= User_Email;

//		NativeExpressAdView adView = (NativeExpressAdView)findViewById(R.id.adView);
//		adView.setVisibility(View.GONE);

//		AdRequest request = new AdRequest.Builder().build();
//		adView.loadAd(request);

		if (Build.VERSION.SDK_INT >= 24) {
			try {
				java.lang.reflect.Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
				m.invoke(null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.blankimage)
				.showImageForEmptyUri(R.drawable.congrrr)
				.showImageOnFail(R.drawable.congrrr)
				.cacheInMemory(false)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.build();

		jsonArray=new JSONArray();

		postcom.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

//			if(pref.getBoolean("boolean",false)){
//
//				if(et_comment.getText().length()>0)
//				{
//					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//					imm.hideSoftInputFromWindow(et_comment.getWindowToken(), 0);
//
//					if(st_base46.length()>1)
//						imagevolley();
//					else
//						volleycominsert();
//
//				}
//				else
//					Toast.makeText(getApplicationContext(), "Please enter comment", Toast.LENGTH_SHORT).show();
//
//			}
//			else{
//				Intent in = new Intent(Webview_internal_link.this, SplashScreen.class);
//				startActivity(in);
//				finish();
//			}
		}
	});

		ab=new ImageUploading(this,camera_img);
		ab.createBottomSheetDialog();

		camera_img.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA};

				if(!hasPermissions(Webview_internal_link.this, PERMISSIONS)){

					ActivityCompat.requestPermissions(Webview_internal_link.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA},
							MY_PERMISSIONS_REQUEST);
				}
				else {
					ab.showOptionBottomSheetDialog();
//				SelectImage();
				}

			}
		});

		webview.setWebViewClient(new Callback());
		webview.getSettings().setJavaScriptEnabled(true);

		net_con=new NetworkConnection(Webview_internal_link.this);

		if(Utility.data!=null)
			if(Utility.data.getPosturl()!=null)
		webview.loadUrl(Utility.data.getPosturl());

//		setData();

//		webview.loadUrl(posturl);

		lay_like.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if(!nw.isConnectingToInternet()) {
					Toast.makeText(Webview_internal_link.this,"No Internet Connection",Toast.LENGTH_LONG).show();
					return;
				}
//				if(pref.getBoolean("boolean",false))
//					volleylike_post();
//				else{
//					Intent in = new Intent(Webview_internal_link.this, SplashScreen.class);
//					startActivity(in);
//					finish();
//				}
			}
		});

		lay_comment.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

				if(lay_commentpost.getVisibility()==View.VISIBLE){
					lay_commentpost.setVisibility(View.GONE);
				}
				else
				{
					lay_commentpost.setVisibility(View.VISIBLE);
				}

			}
		});

		lay_share.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				JSONObject shareobj=new JSONObject();
				try{

						String value="";
						String[] valuearr;

							JSONObject jsondata = new JSONObject(Utility.data.getJsonfield());
							value= String.valueOf(Html.fromHtml(Html.fromHtml(Utility.data.getPostdescription()).toString()));
							valuearr=value.split(System.lineSeparator(), 2);
							value=valuearr[0];

						shareobj.put("class", "Webview_internal_link");
						shareobj.put("title", jsondata.getString("title"));
						shareobj.put("description",value);
						shareobj.put("image", Constants.URL + "newsimage/" + jsondata.getString("pic"));
						shareobj.put("id", Utility.data.getId());
						Utility.shareIntent(Webview_internal_link.this,shareobj);


				} catch (Exception e) {
					e.printStackTrace();
					Logg("share","click Webview_internal_link");
				}


			}
		});


		moreButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

				try {
					if(jsonArray.length()>0) {

						JSONObject obj = jsonArray.getJSONObject(0);

						index=String.valueOf(obj.getInt("id"));

					}
					else{
						index="0";
//						JSONObject obj = jsonArray.getJSONObject(jsonArray.length() - 1);
					}

					volleylist(postid);

				} catch (JSONException e) {
					e.printStackTrace();

				}
			}
		});
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Logg("call_onDestroy()","Call onDestroy()");
		deleteCache(this);
	}

	public void deleteCache(Context context) {
		try {
			File dir = context.getCacheDir();
			deleteDir(dir);
		} catch (Exception e) {}
	}

	public boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
			return dir.delete();
		} else if(dir!= null && dir.isFile()) {
			return dir.delete();
		} else {
			return false;
		}
	}

	public void setComment(int com){

		tv_commentcount.setText("Comments ("+com+")");

		if(com<10)
		{}
		else if(com<100){
			tv_commentcount.setTextSize(12f);
		}
		else{
			tv_commentcount.setTextSize(11f);
		}
	}

	public void setLike(int lk){

		tv_like.setText(getResources().getString(R.string.like)+" ("+lk+")");

//		if(lk<10)
//		{}
//		else if(lk<100){
//			tv_like.setTextSize(12f);
//		}
//		else{
//			tv_like.setTextSize(11f);
//		}
	}

	private class Callback extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(
				WebView view, String url) {
			return(false);
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);
			//Logg("onPagestarted",url+"");

			posturl=url;

//			getData();

		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			//Logg("onPageFinished1 "+postid,url+"");

//			if(progress1!=null)
//				progress1.dismiss();

			layout_progress.setVisibility(View.GONE);

			setData();

			get_Post(postid);



        // bar.setVisibility(View.GONE);

		}
	}

	public void setData(){

		JSONObject jsonobj;

		try {

			jsonobj=new JSONObject(Utility.data.getJsonfield());

			postid=Utility.data.getId();
			tv_title.setText(jsonobj.getString("title"));
			tv_postby_name.setText(jsonobj.getString("writer"));

			tv_post_date.setText(Utility.ConvertDate(Utility.data.getTimestamp()));

			if(jsonobj.has("add_comment")){
				if(jsonobj.getString("add_comment").equalsIgnoreCase("off")){
					lay_comment.setVisibility(View.GONE);

					lay_share.setGravity(Gravity.CENTER);
					lay_like.setGravity(Gravity.CENTER);
				}
			}

			Logg("like_comment",Utility.data.getLikecount()+" "+Utility.data.getCommentcount());

//			tv_commentcount.setText("Comments ("+obj.getInt("comment")+")");
//			tv_like.setText(" Light Up ("+obj.getInt("likes")+")");

			setComment(Utility.data.getCommentcount());
			setLike(Utility.data.getLikecount());

			if(jsonobj.getString("pic").length()>1){
				ImageLoader imageLoader = ImageLoader.getInstance();
				imageLoader.init(ImageLoaderConfiguration.createDefault(Webview_internal_link.this));
				imageLoader.getInstance().displayImage(Constants.URL_Image + "newsimage/" + jsonobj.getString("pic"), nimage, options, animateFirstListener);
			}

			if(Utility.data.getLikestatus().equals("like")) {
				iv_like.setImageResource(R.drawable.ic_bulb_filled);
				tv_like.setTextColor(getResources().getColor(R.color.like_text_color));
			}
			else {
				iv_like.setImageResource(R.drawable.ic_bulb_lightup);
				tv_like.setTextColor(getResources().getColor(R.color.text_lightgray));
			}

            jsonArray = new JSONArray(new ArrayList<String>());

			index="0";

//			volleylist(postid);

		} catch (JSONException e) {
			e.printStackTrace();
			Logg("exception setdata",e+"");
		}
	}

	public void getData()
	{

		RequestQueue queue= Volley.newRequestQueue(Webview_internal_link.this);
		JSONObject obj=new JSONObject();

		String url= Constants.URL+"newapi2_04/get_unique_post.php?email="+ Constants.User_Email+"&link="+posturl+"&postid="+postid;

		Logg("checkversion_url",url);

		JsonObjectRequest json=new JsonObjectRequest(Request.Method.POST, url, obj,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res) {
				// TODO Auto-generated method stub
				try {

					JSONArray jr=res.getJSONArray("data");
					JSONObject obj = null;
					for(int i=0;i<jr.length();i++)
					{
						obj=jr.getJSONObject(i);
					}

					Logg("webview_internal_link",obj+"");

//					setData(obj);

					//progress.dismiss();

				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Logg("checkversion exp",e.getMessage());
					//progress.dismiss();
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				Logg("checkversion error", String.valueOf(arg0));
				//progress.dismiss();
			}
		});
		queue.add(json);
	}

	public void get_Post(final int postid)
	{

		homedata=new ArrayList<>();

		if(!nw.isConnectingToInternet()) {
//            swipeRefreshLayout.setRefreshing(false);
			if(this!=null)
			internetconnection(Webview_internal_link.this,0);

			return;
		}

//		progress = ProgressDialog.show(Webview_internal_link.this, null, null, true);
//		progress.setContentView(R.layout.progressdialog);
//		progress.setCanceledOnTouchOutside(false);
////		progress.setCancelable(false);
//		progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		//progress.show();

		RequestQueue queue=Volley.newRequestQueue(getApplicationContext());
		JSONObject obj=new JSONObject();

		String url=Constants.URL+"newapi2_04/getnewsPost.php?postid="+postid+"&email="+email;

		//Logg("url list",url);

		JsonObjectRequest json=new JsonObjectRequest(Method.POST, url, obj,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res) {
				// TODO Auto-generated method stub

				//Logg("respon",String.valueOf(res));

				try {

//					if(res.has("data")) {
//						JSONArray jr = res.getJSONArray("data");
//						jsonobj = jr.getJSONObject(0);
//						webview.loadUrl(jsonobj.getString("posturl"));
//					}

					if(res.has("related_post")){

						JSONArray jr = res.getJSONArray("related_post");

						for (int i = 0; i < jr.length(); i++) {
							JSONObject ob = jr.getJSONObject(i);

							String time = Utility.getDate(ob.getString("timestamp"));

							Home_getset object=new Home_getset();
							object.setId(ob.getInt("id"));
							object.setTimestamp(time);
							object.setLikecount(ob.getInt("likes"));
							object.setCommentcount(ob.getInt("comment"));
							object.setViewcount(ob.getInt("view"));
							object.setUid(ob.getString("uid"));
							object.setPosttype(ob.getString("posttype"));
							object.setGroupid(ob.getString("groupid"));
							object.setFieldtype(ob.getString("field_type"));
							object.setJsonfield(ob.getString("jsondata"));
							object.setLikestatus(ob.getString("likestatus"));
							object.setPosturl(ob.getString("posturl"));
							object.setPostdescription(ob.getString("post_description"));
							if(ob.has("AppVersion"))
								object.setAppVersion(ob.getString("AppVersion"));
							homedata.add(object);
						}

						//Logg("data.size", homedata.size() + "");
						News_related_post_adapter adapter = new News_related_post_adapter(Webview_internal_link.this, homedata);
						related_list.setAdapter(adapter);

					}

					volleylist(postid);

//					progress.dismiss();

				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					//Logg("no",e.getMessage());
					volleylist(postid);
//					progress.dismiss();
//					loadmore_lay.setVisibility(View.GONE);
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				//Logg("error", String.valueOf(arg0));
//				progress.dismiss();
				volleylist(postid);
//				loadmore_lay.setVisibility(View.GONE);
			}
		});

		queue.add(json);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_CLICK_IMAGE) {

			new ImageCompression(camera_img).execute(imageFilePath);

		} else if (requestCode == REQUEST_CODE_GALLERY_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {

			Uri uri = data.getData();

//            String[] projection = {MediaStore.Images.Media.DATA};
//            Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
//            cursor.moveToFirst();
//            int columnIndex = cursor.getColumnIndex(projection[0]);
			File myFile = new File(uri.getPath());

			final String picturePath = myFile.getAbsolutePath();//cursor.getString(columnIndex); // returns null
			// cursor.close();

			//copy the selected file of gallery into app's sdcard folder and perform the compression operations on it.
			//And override the original image with the newly resized image.

			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					try {
						CommonUtils.copyFile(picturePath, imageFilePath);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});

			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			Bitmap bmp = BitmapFactory.decodeFile(imageFilePath, options);

			int actualHeight = options.outHeight;
			int actualWidth = options.outWidth;

			Logg("actualheight2",actualHeight+" "+actualWidth);

			if(actualWidth>600 | actualHeight>600) {
				new ImageCompression(camera_img).execute(imageFilePath);
			}
			else{

				getContentResolver().notifyChange(uri, null);
				ContentResolver cr =getContentResolver();
				Bitmap bitmap;

				try
				{
					bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, uri);
					//Logg("bitmap_h_w",bitmap.getHeight()+" "+bitmap.getWidth()+"");
					camera_img.setImageBitmap(bitmap);

					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
					bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
					byte[] byteArray = byteArrayOutputStream .toByteArray();

					st_base46 = Base64.encodeToString(byteArray, Base64.DEFAULT);
//                    return Base64.encodeToString(byteArray, Base64.DEFAULT);

					Logg("base64",st_base46);

				}
				catch (Exception e)
				{

				}
			}
		}
	}

	public void volleylist(int postid)
	{


		RequestQueue queue=Volley.newRequestQueue(getApplicationContext());
		JSONObject obj=new JSONObject();
		try {
			obj.put("postid", postid);
			obj.put("index", index);
			obj.put("email", Constants.User_Email);
			obj.put("admission_no", Constants.addmissionno);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		String url = Constants.URL_LV + "get_common_comment";
		Logg("url list",url);

		JsonObjectRequest json=new JsonObjectRequest(Method.POST, url, obj,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res) {
				// TODO Auto-generated method stub

				//Logg("respon",String.valueOf(res));
				try {

					JSONArray jr=res.getJSONArray("data");

					if(jr.length()>=10)
						loadmore_lay.setVisibility(View.VISIBLE);
					else
					loadmore_lay.setVisibility(View.GONE);

					if(index.equalsIgnoreCase("0")) {
						jsonArray = new JSONArray(new ArrayList<String>());
						jsonArray=jr;
						adap=new Replies_on_group_comment_adap(Webview_internal_link.this,jsonArray);
						list.setAdapter(adap);
						//nestedscroll.fullScroll(View.FOCUS_DOWN);
					}
					else {
						for(int i=0;i<jsonArray.length();i++) {
							jr.put(jsonArray.getJSONObject(i)); //new JSONArray(new ArrayList<>());
						}
						adap=new Replies_on_group_comment_adap(Webview_internal_link.this,jr);
						list.setAdapter(adap);
						//nestedscroll.fullScroll(View.FOCUS_DOWN);
					}
				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					//Logg("no",e.getMessage());
					loadmore_lay.setVisibility(View.GONE);
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				//Logg("error", String.valueOf(arg0));
				loadmore_lay.setVisibility(View.GONE);
			}
		});

		queue.add(json);

//		  int socketTimeout = 20000;//30 seconds - change to what you want
//		  RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//		  json.setRetryPolicy(policy);

	}

	public void imagevolley() {

		if(!nw.isConnectingToInternet()) {
//            swipeRefreshLayout.setRefreshing(false);

			if(this!=null)
			internetconnection(Webview_internal_link.this,2);
			return;
		}

		progress = ProgressDialog.show(Webview_internal_link.this, null, null, true);
		progress.setContentView(R.layout.progressdialog);
		progress.setCanceledOnTouchOutside(false);
		progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		progress.show();

		st_imagename=String.valueOf(System.currentTimeMillis());

		comment=et_comment.getText().toString();

		RequestQueue queue = Volley.newRequestQueue(Webview_internal_link.this);
		String url = Constants.URL_LV + "insert_common_comment_image";

		StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
			@Override
			public void onResponse(String s) {

				Logg("response", s);
				try {
					if(s.length()>0)
					{
						JSONObject obj=new JSONObject(s);
						s=obj.getString("status");
						if(s.equalsIgnoreCase("success"))
						{
							Toast.makeText(Webview_internal_link.this,"Upload Successfully",Toast.LENGTH_LONG).show();
							et_comment.setText("");
							st_base46="";
							camera_img.setImageResource(R.drawable.camera40);

							jsonArray=new JSONArray(new ArrayList<String>());
							index="0";
							volleylist(postid);

						}
						else if(s.equalsIgnoreCase("block"))
							Toast.makeText(Webview_internal_link.this,obj.getString("response"),Toast.LENGTH_LONG).show();
						else
						{
							Toast.makeText(Webview_internal_link.this,"Somthing is Wrong Can't Upload, Please Try Againe",Toast.LENGTH_LONG).show();
						}

						progress.dismiss();
						//Logg("response_string",s+" response");
					}
				}

				catch (Exception e)
				{
					progress.dismiss();
					//Logg("e","e",e);
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {
				CommonUtils.toast(Webview_internal_link.this,CommonUtils.volleyerror(volleyError));
				progress.dismiss();
			}
		}) {
			@Override
			protected Map<String, String> getParams() {
				//id="+psid+"&comment="+cmnt+"&user="+username+"&collage="+collage+"&image="+image+"&email="+mail;

				Map<String, String> params = new HashMap<String, String>();
				params.put("st_base64", st_base46);
				params.put("imagename", st_imagename);
				params.put("postid", postid+"");
				params.put("posttype", "1");
				params.put("email", email);
				params.put("comment", comment.replaceAll(" ","%20"));
				params.put("admission_no", Constants.addmissionno);
				//Logg("img222222", st_base46);

				return params;
			}
		};

		int socketTimeout = 0;
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		request.setRetryPolicy(policy);
		queue.add(request);
	}

	private void volleycominsert() {
		// TODO Auto-generated method stub

		if(!nw.isConnectingToInternet()) {
//            swipeRefreshLayout.setRefreshing(false);
			if(this!=null)
			internetconnection(Webview_internal_link.this,1);

			return;
		}

		progress = ProgressDialog.show(Webview_internal_link.this, null, null, true);
		progress.setContentView(R.layout.progressdialog);
		progress.setCanceledOnTouchOutside(false);
//		progress.setCancelable(false);
		progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		progress.show();

		String url = Constants.URL_LV + "insert_common_comment";
		Logg("insert_common_comment", url);

		comment=et_comment.getText().toString();
		comment=comment.replaceAll(" ","%20");

		RequestQueue que= Volley.newRequestQueue(getApplicationContext());

		StringRequest obj1=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

			@Override
			public void onResponse(String res) {
				// TODO Auto-generated method stub

				//Logg("response",res);
				int size=res.length();
				if(size>0)
				{

					try {
						JSONObject obj=new JSONObject(res);

						if(obj.getString("status").equalsIgnoreCase("success"))
						{
							et_comment.setText("");
							Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_SHORT).show();

							jsonArray=new JSONArray(new ArrayList<String>());
							index="0";

							volleylist(postid);
						}
						else if(obj.getString("status").equalsIgnoreCase("block"))
							Toast.makeText(Webview_internal_link.this,obj.getString("response"),Toast.LENGTH_LONG).show();
						else {
							Toast.makeText(getApplicationContext(),"Somthing is wrong, Can't insert comment",Toast.LENGTH_SHORT).show();
						}
						progress.dismiss();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						progress.dismiss();
						//Logg("exception",e.toString());
						e.printStackTrace();
					}
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError e) {
				// TODO Auto-generated method stub
				progress.dismiss();
				//Logg("error",e.toString());
			}
		}){

			@Override
			protected Map<String,String> getParams(){
				Map<String,String> params = new HashMap<String, String>();
				params.put("postid", postid+"");
				params.put("posttype", "1");
				params.put("comment", comment);
				params.put("email",email);
				params.put("admission_no", Constants.addmissionno);
				return params;
			}
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String,String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json; charset=utf-8");
//
//                return headers;
//            }
		};

		int socketTimeout =0;//30 seconds - change to what you want
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		obj1.setRetryPolicy(policy);

		que.add(obj1);

	}

	@Override
	public void onBackPressed() {

		if(Constants.Check_Branch_IO){
			Intent in =new Intent(Webview_internal_link.this,HomeActivity.class);
			startActivity(in);
			finish();
		}
		else
			super.onBackPressed();
	}

	public void volleylike_post()
	{

		progress = ProgressDialog.show(Webview_internal_link.this, null, null, true);
		progress.setContentView(R.layout.progressdialog);
		progress.setCanceledOnTouchOutside(false);
//		progress.setCancelable(false);
		progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//		progress.show();

		RequestQueue queue= Volley.newRequestQueue(Webview_internal_link.this);
		JSONObject json = new JSONObject();
		try {
			json.put("id",postid);
			json.put("email",Constants.User_Email);
			json.put("admission_no",Constants.addmissionno);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		String url = Constants.URL_LV + "like_post";
		Logg("likeapi",url+" , "+json.toString());
		JsonObjectRequest jsonreq=new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res) {
// //Logg("res",String.valueOf(res));
				// TODO Auto-generated method stub

				try {

					if(res.getString("scalar").equals("like")) {
//						Toast.makeText(Webview_internal_link.this,"success",Toast.LENGTH_SHORT).show();

							iv_like.setImageResource(R.drawable.ic_bulb_filled);
							tv_like.setTextColor(getResources().getColor(R.color.like_text_color));

					}
					else if(res.getString("scalar").equals("dislike")){

						iv_like.setImageResource(R.drawable.ic_bulb_lightup);
						tv_like.setTextColor(getResources().getColor(R.color.text_lightgray));

//						Toast.makeText(Webview_internal_link.this,"success",Toast.LENGTH_SHORT).show();

					}
					else {
						Toast.makeText(Webview_internal_link.this,"Not done",Toast.LENGTH_SHORT).show();
					}

					progress.dismiss();

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					progress.dismiss();
				}

			}
		},new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg)
			{
				//Logg("error", String.valueOf(arg));
				progress.dismiss();
				Toast.makeText(Webview_internal_link.this,"Network Problem", Toast.LENGTH_SHORT).show();
			}


		});

		queue.add(jsonreq);
	}

	public void internetconnection(Context context,final int i){

		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.network_alert);

		if(Webview_internal_link.this.isFinishing()){
			return;
		}

		dialog.show();

		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
		TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

		tv_tryagain.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.dismiss();

				if(i==0)
					get_Post(postid);
				else if(i==1)
					volleycominsert();
				else if(i==2)
					imagevolley();

//				getData();

			}
		});

		iv_cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

}
