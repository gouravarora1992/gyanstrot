package com.scholar.engineering.banking.ssc;

import android.content.Context;
import android.content.Intent;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.FirebaseApp;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;

import io.branch.referral.Branch;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.toast;
import static com.scholar.engineering.banking.ssc.utils.Constants.Check_Branch_IO;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class ParseApplication extends MultiDexApplication {
    UserSharedPreferences pref;
    String uid = "";


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        FirebaseApp.initializeApp(this);
        MultiDex.install(this);

        pref = new UserSharedPreferences(this,"aa");

        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .init();

        Branch.getAutoInstance(this);

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                pref.setplayerid(userId);
            }
        });
    }
}
