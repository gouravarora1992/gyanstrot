package com.scholar.engineering.banking.ssc.Challenge;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.scholar.engineering.banking.ssc.HomeActivity;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.EndlessRecyclerViewScrollListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.toast;
import static com.scholar.engineering.banking.ssc.utils.Utility.applyFontForToolbarTitle;

public class Blocked_Users extends AppCompatActivity{

	ActionBar bar;
	RecyclerView nlist;
	//ImageView submit;
	Blocked_Users_Adapter adapter;

	TextView tv_title;
	ProgressDialog dialog;
	SwipeRefreshLayout swipeRefreshLayout;
	Toolbar toolbar;
	EndlessRecyclerViewScrollListener recyclerViewScrollListener;
	ProgressWheel wheelbar;
	RelativeLayout footerlayout;
	TextView loadmore;
	boolean prg=true;
	int index=1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.group_list_activity);

		toolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setTitle("Blocked Users");

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		applyFontForToolbarTitle(toolbar,this);

		tv_title=(TextView)findViewById(R.id.headertextid);
		tv_title.setVisibility(View.GONE);

		nlist=(RecyclerView)findViewById(R.id.recyclerView);
		LinearLayoutManager mLayoutManager = new LinearLayoutManager(Blocked_Users.this);
		mLayoutManager.scrollToPositionWithOffset(0,0);
		nlist.setLayoutManager(mLayoutManager);

		swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh_layout);

		swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				recyclerViewScrollListener.resetState();
				prg=false;
				index=1;
				getData(index);
			}
		});

		footerlayout=(RelativeLayout)findViewById(R.id.footer_layout);
		loadmore=(TextView)findViewById(R.id.moreButton);
		wheelbar=(ProgressWheel)findViewById(R.id.progress_wheel);

		recyclerViewScrollListener=new EndlessRecyclerViewScrollListener(mLayoutManager) {
			@Override
			public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
					Logg("grouppage",page+"");
				index++;
				footerlayout.setVisibility(View.VISIBLE);
				getData(index);
			}
		};

		nlist.addOnScrollListener(recyclerViewScrollListener);

		getData(index);

	}

	public void getData(final int index) {


		if(index==1&prg) {
			dialog = new ProgressDialog(Blocked_Users.this);
			dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
			dialog.setCanceledOnTouchOutside(false);
			dialog.show();
		}

		RequestQueue queue = Volley.newRequestQueue(Blocked_Users.this);
		String url="";
		url = Constants.URL_LV+"getblockeduser";
		url = url.replace(" ", "%20");
		Logg("name", url);

		StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

			@Override
			public void onResponse(String s) {

				Logg("response", s);

				try {
//					dialog.dismiss();

					JSONObject res=new JSONObject(s);

					if(res.has("user")) {

						JSONArray jr = res.getJSONArray("user");

						if(index==1) {
							adapter=new Blocked_Users_Adapter(Blocked_Users.this,jr);
							nlist.setAdapter(adapter);
						}
						else{
							adapter.addItem(jr);
							adapter.notifyDataSetChanged();
						}

					}
					else if(index==1){
						toast(Blocked_Users.this,"You have no blocked users");
						// u have no blocked users
					}

					if(dialog!=null)
						if(dialog.isShowing())
							dialog.dismiss();

					footerlayout.setVisibility(View.GONE);

					swipeRefreshLayout.setRefreshing(false);

				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					if(dialog!=null)
						if(dialog.isShowing())
							dialog.dismiss();
					//Logg("no",e.getMessage());
					swipeRefreshLayout.setRefreshing(false);
				}

			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {
				if(dialog!=null)
					if(dialog.isShowing())
				dialog.dismiss();
				swipeRefreshLayout.setRefreshing(false);
				CommonUtils.toast(Blocked_Users.this,CommonUtils.volleyerror(volleyError));
			}
		}) {

			@Override
			protected Map<String, String> getParams() {

				Map<String, String> map = new HashMap<String, String>();
				map.put("user_id", Constants.User_Email);
				map.put("page",index+"");
				Logg("map",map.toString());

				return map;

			}
		};

		int socketTimeout = 20000;
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		request.setRetryPolicy(policy);
		queue.add(request);

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Logg("call_onDestroy()","Call onDestroy()");
		deleteCache(this);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(item.getItemId()==android.R.id.home)
			finish();

		return super.onOptionsItemSelected(item);
	}

	public void deleteCache(Context context) {
		try {
			File dir = context.getCacheDir();
			deleteDir(dir);
		} catch (Exception e) {}
	}

	public boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
			return dir.delete();
		} else if(dir!= null && dir.isFile()) {
			return dir.delete();
		} else {
			return false;
		}
	}




}
