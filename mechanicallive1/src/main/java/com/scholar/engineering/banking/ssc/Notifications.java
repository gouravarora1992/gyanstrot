package com.scholar.engineering.banking.ssc;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.pnikosis.materialishprogress.ProgressWheel;
//import com.scholar.engineering.banking.ssc.AdModule.AdmobRecyclerAdapterWrapper;
import com.scholar.engineering.banking.ssc.adapter.Notification_Adapter;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class Notifications  extends AppCompatActivity
{

    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    Notification_Adapter adapter;
    int index=0;
    RelativeLayout footerlayout;
    LinearLayout group_layid;
    TextView loadmore;
    ProgressWheel wheelbar;
    JSONArray jsonArray;
    HorizontalScrollView horizontalScrollView;
    LinearLayout layout;
//    AdmobRecyclerAdapterWrapper adapterWrapper;
    NetworkConnection nw;
    TextView tv_nodata;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.shareid:
                Intent in = new Intent(getApplicationContext(), Share_and_Earn.class);
                startActivity(in);
                break;
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.activity_main, menu);
        MenuItem playMenu = menu.findItem(R.id.shareid);

        if (Constants.Width <= 600)
            playMenu.setIcon(R.drawable.dollar);
        else
            playMenu.setIcon(R.drawable.dollar);
        return super.onCreateOptionsMenu(menu);
    }




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {


        setContentView(R.layout.notifications);

        nw=new NetworkConnection(getApplicationContext());

        layout=(LinearLayout)findViewById(R.id.layout2);


        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        getSupportActionBar().setTitle("Notifications");
        Utility.applyFontForToolbarTitle(toolbar,this);

//        TextView header=(TextView)findViewById(R.id.headertextid);
//
//        header.setText("Notifications");

        layout.setVisibility(View.GONE);
        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh_layout);

        recyclerView=(RecyclerView)findViewById(R.id.recyclerView);
        horizontalScrollView=(HorizontalScrollView)findViewById(R.id.scrollView);

        horizontalScrollView.setVisibility(View.GONE);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        mLayoutManager.scrollToPositionWithOffset(0,0);

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        tv_nodata=(TextView)findViewById(R.id.tv_nodata);

        footerlayout=(RelativeLayout)findViewById(R.id.footer_layout);
        loadmore=(TextView)findViewById(R.id.moreButton);
        wheelbar=(ProgressWheel)findViewById(R.id.progress_wheel);

        //jsonArray=new JSONArray();

        getData();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                index=0;
                getData();
            }
        });


        footerlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    JSONObject obj=jsonArray.getJSONObject(jsonArray.length()-1);
                    index= obj.getInt("id");

                    getData();

                    loadmore.setText("");
                    wheelbar.setVisibility(View.VISIBLE);

                } catch (JSONException e) {

                }

            }
        });
        
        super.onCreate(savedInstanceState);
    }

   

    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    getData();
                default:
                    break;
            }
            return false;
        }
    });

    @Override
    public void onResume() {
        super.onResume();
       // handler.sendEmptyMessageDelayed(1, 2000);
        swipeRefreshLayout.setRefreshing(true);
    }

    public void getData()
    {

        if(!nw.isConnectingToInternet()) {

            if(Constants.viewpagercurrentitem==2) {
                swipeRefreshLayout.setRefreshing(false);

                internetconnection();
            }
            return;
        }

        RequestQueue queue= Volley.newRequestQueue(getApplicationContext());
        JSONObject obj=new JSONObject();
        String url= Constants.URL+"newapi2_04/get_notification.php?email="+ Constants.User_Email+"&index="+index;

        Log.e("url",url);
        //Logg("checkversion_url",url);

        JsonObjectRequest json=new JsonObjectRequest(Request.Method.POST, url, obj,new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {

                // TODO Auto-generated method stub

                try {
                    tv_nodata.setVisibility(View.GONE);
                    if(res.getString("status").equalsIgnoreCase("success")) {
                        if (res.has("data")) {

                            JSONArray jr = res.getJSONArray("data");

                            if (jr.length() < 20)
                                footerlayout.setVisibility(View.GONE);
                            else {
                                footerlayout.setVisibility(View.VISIBLE);
                                loadmore.setText("Load Previous..");
                                wheelbar.setVisibility(View.GONE);
                            }

                            if (index == 0)
                                jsonArray = new JSONArray();

                            for (int i = 0; i < jr.length(); i++)
                                jsonArray.put(jr.get(i));

                            adapter = new Notification_Adapter(getApplicationContext(), jsonArray);
                            recyclerView.setAdapter(adapter);

//                        initRecyclerViewItems();
//                        adapter.notifyDataSetChanged();

                        } else {
                            tv_nodata.setVisibility(View.VISIBLE);
                            footerlayout.setVisibility(View.GONE);
                            //no data
                        }
                    }
                    else{
                        tv_nodata.setVisibility(View.VISIBLE);
                        footerlayout.setVisibility(View.GONE);
                    }

                    swipeRefreshLayout.setRefreshing(false);
                }

                catch (JSONException e) {
                    footerlayout.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                // TODO Auto-generated method stub
                //Logg("checkversion error", String.valueOf(arg0));
                footerlayout.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        queue.add(json);
    }

    public void internetconnection(){

        final Dialog dialog = new Dialog(getApplicationContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

//        JSONObject json;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//				index=0;
                swipeRefreshLayout.setRefreshing(true);
                dialog.dismiss();
                getData();

//				Home_getset home;
//
//				if(homedata.get(homedata.size()-1).getClass()!=null) {
//					home= (Home_getset) homedata.get(homedata.size()-1);
//				}
//				else {
//					home= (Home_getset) homedata.get(homedata.size()-2);
//				}
//
//
//				index= home.getId();

//				if(aBoolean)
//					getData();
//
//				loadmore.setText("");
//				wheelbar.setVisibility(View.VISIBLE);


            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

}
