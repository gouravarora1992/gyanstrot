package com.scholar.engineering.banking.ssc.Challenge;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.scholar.engineering.banking.ssc.HomeActivity;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.MS_to_Sec;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.toast;

public class Challenge_Result_List extends AppCompatActivity {

    TextView tv_subjectname,tv_questioncount,tv_time,tv_winningpoints,tv_tot_points,tv_bonus_points;
    TextView tv_name_lu,tv_occupation_lu,tv_points_lu,tv_result_status;
    TextView tv_name_opp,tv_occupation_opp,tv_points_opp,tv_opponentname;
    CircleImageView imageView_lu,imageView_opp;

    RecyclerView recyclerView;
    Challenge_Result_List_Adapter adap;
    String challenge_id="";


    Typeface font_demi;
    Typeface font_medium;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.challenge_result);

        font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");
        font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");

        recyclerView=(RecyclerView)findViewById(R.id.recyclerview);

        tv_subjectname=(TextView)findViewById(R.id.tv_challengename);
        tv_questioncount=(TextView)findViewById(R.id.tv_questioncount);
        tv_opponentname=(TextView)findViewById(R.id.tv_opponentname);

        tv_name_lu=(TextView)findViewById(R.id.tv_name_lu);
        tv_occupation_lu=(TextView)findViewById(R.id.tv_occupation_lu);
        tv_points_lu=(TextView)findViewById(R.id.tv_points_lu);
        tv_name_opp=(TextView)findViewById(R.id.tv_name_opp);
        tv_occupation_opp=(TextView)findViewById(R.id.tv_occupation_opp);
        tv_points_opp=(TextView)findViewById(R.id.tv_points_opp);
        tv_time=(TextView)findViewById(R.id.tv_time);

        imageView_lu=(CircleImageView)findViewById(R.id.imageView_lu);
        imageView_opp=(CircleImageView)findViewById(R.id.imageView_opp);
        tv_result_status=(TextView)findViewById(R.id.tv_resultstatus);
        tv_winningpoints=(TextView)findViewById(R.id.tv_points);
        tv_bonus_points=(TextView)findViewById(R.id.tv_bonus_points);
        tv_tot_points=(TextView)findViewById(R.id.tv_total_points);

        tv_subjectname.setTypeface(font_demi);
        tv_questioncount.setTypeface(font_medium);
        tv_opponentname.setTypeface(font_medium);
        tv_name_lu.setTypeface(font_medium);
        tv_occupation_lu.setTypeface(font_medium);
        tv_points_lu.setTypeface(font_medium);
        tv_name_opp.setTypeface(font_medium);
        tv_occupation_opp.setTypeface(font_medium);
        tv_points_opp.setTypeface(font_medium);
        tv_time.setTypeface(font_medium);
        tv_result_status.setTypeface(font_medium);
        tv_winningpoints.setTypeface(font_medium);
        tv_bonus_points.setTypeface(font_medium);
        tv_tot_points.setTypeface(font_medium);



        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        challenge_id=getIntent().getStringExtra("challenge_id");

        getData();

        initToolbar();
    }

    public void initToolbar(){

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Result");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void setData(JSONObject object){

        try {

            JSONObject ans=new JSONObject(object.getString("answers"));
            JSONArray arr_u=ans.getJSONArray("user");
            JSONArray arr_o=ans.getJSONArray("opponent");
            JSONArray arr_que=new JSONArray(object.getString("questions"));

            tv_subjectname.setText(object.getString("subject"));

            tv_winningpoints.setText(object.getString("match_result")+" ");
            tv_bonus_points.setText(object.getString("bonus_points")+" ");
            tv_tot_points.setText(object.getString("total_points")+" ");
            tv_result_status.setText(object.getString("result_status")+" ");

            if(object.getString("userid").equalsIgnoreCase(Constants.User_Email)){

                tv_name_lu.setText(object.getString("user_name"));
                tv_occupation_lu.setText("Level "+object.getString("user_rank"));
                tv_points_lu.setText(object.getString("user_points"));

                if(object.getString("user_image").length()>4)
                    Glide.with(this)
                            .load(object.getString("user_image"))
                            .into(imageView_lu);

                tv_opponentname.setText(object.getString("opponent_name"));
                tv_name_opp.setText(object.getString("opponent_name"));
                tv_occupation_opp.setText("Level "+object.getString("opponent_rank"));
                tv_points_opp.setText(object.getString("opponent_points"));

                if(object.getString("opponent_image").length()>4)
                    Glide.with(this)
                            .load(object.getString("opponent_image"))
                            .into(imageView_opp);

                    tv_questioncount.setText(object.getString("user_correct")+":"+object.getString("opponent_correct"));
                    tv_time.setText(MS_to_Sec(object.getString("user_time"))+" s : "+MS_to_Sec(object.getString("opponent_time"))+" s");

                adap=new Challenge_Result_List_Adapter(Challenge_Result_List.this,arr_que,arr_u,arr_o,challenge_id);
                recyclerView.setAdapter(adap);

            }
            else{

                tv_name_lu.setText(object.getString("opponent_name"));
                tv_occupation_lu.setText("Level "+object.getString("opponent_rank"));
                tv_points_lu.setText(object.getString("opponent_points"));

                if(object.getString("opponent_image").length()>4)
                    Glide.with(this)
                            .load(object.getString("opponent_image"))
                            .into(imageView_lu);

                tv_opponentname.setText(object.getString("user_name"));

                tv_name_opp.setText(object.getString("user_name"));
                tv_occupation_opp.setText("Level "+object.getString("user_rank"));
                tv_points_opp.setText(object.getString("user_points"));

                if(object.getString("user_image").length()>4)
                    Glide.with(this)
                            .load(object.getString("user_image"))
                            .into(imageView_opp);

                tv_questioncount.setText(object.getString("opponent_correct")+":"+object.getString("user_correct"));

                tv_time.setText(MS_to_Sec(object.getString("opponent_time"))+"s : "+MS_to_Sec(object.getString("user_time"))+" s");

                adap=new Challenge_Result_List_Adapter(Challenge_Result_List.this,arr_que,arr_o,arr_u,challenge_id);
                recyclerView.setAdapter(adap);

            }

        } catch (JSONException e) {

        }
    }

    public void getData() {

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("loading...");
        pd.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        String url="";
        url = Constants.URL_LV+"challenge_result";
        url = url.replace(" ", "%20");
        Logg("name", url);

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {

                Logg("response", s);
                try
                {
                    if(s.length()>0)
                    {
                        JSONObject arg0=new JSONObject(s);
                        if(arg0.getString("status").equalsIgnoreCase("success")) {
                            setData(arg0.getJSONObject("message"));
                        }
                        else if(arg0.getString("status").equalsIgnoreCase("fail")){
                            toast(Challenge_Result_List.this,arg0.getString("message"));
                        }
                    }

                    pd.dismiss();
                }

                catch (Exception e)
                {
                    pd.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                pd.dismiss();
                CommonUtils.toast(Challenge_Result_List.this,CommonUtils.volleyerror(volleyError));
            }

        }) {

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", Constants.User_Email);
                map.put("challenge_id",challenge_id);

                Logg("map",map.toString());

                return map;

            }
        };

        int socketTimeout = 20000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);

    }

}
