package com.scholar.engineering.banking.ssc.Challenge;

import android.app.ProgressDialog;
import android.content.Context;;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.getset.Question_getset_new;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;

/**
 * Created by surender on 3/24/2017.
 */

public class Your_Answer extends Fragment {

    LinearLayout lay_optiona, lay_optionb, lay_optionc, lay_optiond, lay_optione, lay_question;

    TextView tv_question, tv_optiona, tv_optionb, tv_optionc, tv_optiond, tv_optione, tv_hint;

    TextView tv_cir_optiona, tv_cir_optionb, tv_cir_optionc, tv_cir_optiond, tv_cir_optione;

    TextView tv_timer, tv_next, tv_name_lu, tv_name_opp, tv_questioncount, tv_solution;
    LinearLayout lay_hint, lay_solution;

    ScrollView scrollView;
    public static int user_count = 0, opp_count = 0;
    int i = 0;
    Handler handler;
    public static Handler h;

    ArrayList<Question_getset_new> data;
    Typeface font_demi, font_medium;
    String login_useremail = "";

    ImageView iv_solution, iv_hint, iv_question, iv_optiona, iv_optionb, iv_optionc, iv_optiond, iv_optione;
    ImageView iv_check_ansa, iv_check_ansb, iv_check_ansc, iv_check_ansd, iv_check_anse;

    WebView wv_hint;

    int count = 0;
    Bitmap bitmap;
    JSONObject object = null, obj_login, obj_opponent;
    String photoPath = Environment.getExternalStorageDirectory() + "/GyanStrot/";

    ProgressDialog progress;
    CircleImageView imageView_lu, imageView_opp;
    boolean b_hint = false, b_que = false, check_result = false;

    NetworkConnection nw;
    View v;
    See_Response_Question activity;

    @Override
    public void onAttach(Context context) {
        activity = (See_Response_Question) context;
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.correct_answer, container, false);

        data = new ArrayList<>();

        nw = new NetworkConnection(activity);

        //wv_hint=(WebView)findViewById(R.id.webview_hint);

        font_demi = Typeface.createFromAsset(activity.getAssets(), "avenirnextdemibold.ttf");
        font_medium = Typeface.createFromAsset(activity.getAssets(), "avenirnextmediumCn.ttf");


        photoPath = photoPath + Constants.ImageDirectory + "/";
        login_useremail = User_Email;

        imageView_lu = (CircleImageView) v.findViewById(R.id.imageView_lu);
        imageView_opp = (CircleImageView) v.findViewById(R.id.imageView_opp);
        tv_name_lu = (TextView) v.findViewById(R.id.tv_name_lu);
        tv_name_opp = (TextView) v.findViewById(R.id.tv_name_opp);
        tv_questioncount = (TextView) v.findViewById(R.id.tv_questioncount);

        lay_optiona = (LinearLayout) v.findViewById(R.id.lay_optiona);
        lay_optionb = (LinearLayout) v.findViewById(R.id.lay_optionb);
        lay_optionc = (LinearLayout) v.findViewById(R.id.lay_optionc);
        lay_optiond = (LinearLayout) v.findViewById(R.id.lay_optiond);
        lay_optione = (LinearLayout) v.findViewById(R.id.lay_optione);
        lay_question = (LinearLayout) v.findViewById(R.id.lay_question);
        lay_hint = (LinearLayout) v.findViewById(R.id.lay_hint);
        lay_solution = (LinearLayout) v.findViewById(R.id.lay_solution);
        tv_optiona = (TextView) v.findViewById(R.id.tv_optiona);
        tv_optionb = (TextView) v.findViewById(R.id.tv_optionb);
        tv_optionc = (TextView) v.findViewById(R.id.tv_optionc);
        tv_optiond = (TextView) v.findViewById(R.id.tv_optiond);
        tv_optione = (TextView) v.findViewById(R.id.tv_optione);
        tv_question = (TextView) v.findViewById(R.id.tv_question);
        tv_hint = (TextView) v.findViewById(R.id.tv_questionhint);
        tv_solution = (TextView) v.findViewById(R.id.tv_questionsolution);
        tv_cir_optiona = (TextView) v.findViewById(R.id.tv_circle_a);
        tv_cir_optionb = (TextView) v.findViewById(R.id.tv_circle_b);
        tv_cir_optionc = (TextView) v.findViewById(R.id.tv_circle_c);
        tv_cir_optiond = (TextView) v.findViewById(R.id.tv_circle_d);
        tv_cir_optione = (TextView) v.findViewById(R.id.tv_circle_e);
        scrollView = (ScrollView) v.findViewById(R.id.scrollView);

        tv_cir_optiona.setTypeface(font_demi);
        tv_cir_optionb.setTypeface(font_demi);
        tv_cir_optionc.setTypeface(font_demi);
        tv_cir_optiond.setTypeface(font_demi);
        tv_cir_optione.setTypeface(font_demi);
        tv_optiona.setTypeface(font_medium);
        tv_optionb.setTypeface(font_medium);
        tv_optionc.setTypeface(font_medium);
        tv_optiond.setTypeface(font_medium);
        tv_optione.setTypeface(font_medium);
        tv_question.setTypeface(font_medium);
        tv_hint.setTypeface(font_medium);
        tv_solution.setTypeface(font_medium);
        iv_check_ansa = (ImageView) v.findViewById(R.id.iv_option_a);
        iv_check_ansb = (ImageView) v.findViewById(R.id.iv_option_b);
        iv_check_ansc = (ImageView) v.findViewById(R.id.iv_option_c);
        iv_check_ansd = (ImageView) v.findViewById(R.id.iv_option_d);
        iv_check_anse = (ImageView) v.findViewById(R.id.iv_option_e);
        iv_check_ansa.setVisibility(View.GONE);
        iv_check_ansb.setVisibility(View.GONE);
        iv_check_ansc.setVisibility(View.GONE);
        iv_check_ansd.setVisibility(View.GONE);
        iv_check_anse.setVisibility(View.GONE);

        iv_optiona = (ImageView) v.findViewById(R.id.iv_optiona);
        iv_optionb = (ImageView) v.findViewById(R.id.iv_optionb);
        iv_optionc = (ImageView) v.findViewById(R.id.iv_optionc);
        iv_optiond = (ImageView) v.findViewById(R.id.iv_optiond);
        iv_optione = (ImageView) v.findViewById(R.id.iv_optione);

        iv_question = (ImageView) v.findViewById(R.id.iv_question);
        iv_hint = (ImageView) v.findViewById(R.id.iv_questionhint);
        iv_solution = (ImageView) v.findViewById(R.id.iv_solution);

//        tv_back=(TextView)findViewById(R.id.tv_back);
//        tv_next=(TextView)v.findViewById(R.id.tv_next);
//        tv_timer=(TextView)v.findViewById(R.id.tv_timer);
//        tv_back.setTypeface(font_demi);
//        tv_next.setTypeface(font_demi);
//        tv_timer.setTypeface(font_demi);

        try {
            object = new JSONObject(getArguments().getString("object"));

            getQuestion(object);
        } catch (JSONException e) {

        }

        return v;
    }

    public void checkAnswer_ifno(String user_ans, LinearLayout layout, String color) {

        lay_optiona.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optionb.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optionc.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optiond.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optione.setBackgroundColor(Color.parseColor("#00000000"));

        layout.setBackgroundColor(Color.parseColor(color));

    }


    public void setQuestion() {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        iv_hint.setVisibility(View.GONE);
        iv_optiona.setVisibility(View.GONE);
        iv_optionb.setVisibility(View.GONE);
        iv_optionc.setVisibility(View.GONE);
        iv_optiond.setVisibility(View.GONE);
        iv_optione.setVisibility(View.GONE);
        iv_question.setVisibility(View.GONE);

        lay_optiona.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optionb.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optionc.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optiond.setBackgroundColor(Color.parseColor("#00000000"));
        lay_optione.setBackgroundColor(Color.parseColor("#00000000"));

        iv_check_ansa.setVisibility(View.GONE);
        iv_check_ansb.setVisibility(View.GONE);
        iv_check_ansc.setVisibility(View.GONE);
        iv_check_ansd.setVisibility(View.GONE);
        iv_check_anse.setVisibility(View.GONE);

        if (data.get(count).getOptione().equalsIgnoreCase("null") & data.get(count).getOptione_img().equalsIgnoreCase("null")) {
            lay_optione.setVisibility(View.GONE);
        } else {
            lay_optione.setVisibility(View.VISIBLE);

            if (!data.get(count).getOptione_img().equalsIgnoreCase("null")) {

                iv_optione.setVisibility(View.VISIBLE);

                String[] arr = data.get(count).getOptione_img().split("/");

                int len = arr.length;

                String photoPath1;
                photoPath1 = photoPath + arr[len - 1];

                bitmap = BitmapFactory.decodeFile(photoPath1, options);
                if (bitmap != null) {
                    iv_optione.setImageBitmap(bitmap);
                }
            }
        }

        lay_hint.setVisibility(View.GONE);

        if (data.get(count).getSolution().equalsIgnoreCase("null") & data.get(count).getSolution_img().equalsIgnoreCase("null")) {
            lay_solution.setVisibility(View.GONE);
        } else if (data.get(count).getSolution().length() < 5 & data.get(count).getSolution_img().equalsIgnoreCase("null")) {
            lay_solution.setVisibility(View.GONE);
        } else {
            lay_solution.setVisibility(View.VISIBLE);
        }


        if (!data.get(count).getQuestion_img().equalsIgnoreCase("null")) {
            iv_question.setVisibility(View.VISIBLE);

            String[] arr = data.get(count).getQuestion_img().split("/");
            int len = arr.length;
            String photoPath1;
            photoPath1 = photoPath + arr[len - 1];

            bitmap = BitmapFactory.decodeFile(photoPath1, options);
            if (bitmap != null) {

                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;

                int h;
                h = Constants.Width / imageWidth;
                h = (int) h * imageHeight;

                ViewGroup.LayoutParams params = iv_question.getLayoutParams();

                params.height = h;

                iv_question.setLayoutParams(params);

                iv_question.setImageBitmap(bitmap);
            }
        }

        if (!data.get(count).getOptiona_img().equalsIgnoreCase("null")) {
            iv_optiona.setVisibility(View.VISIBLE);

            String[] arr = data.get(count).getOptiona_img().split("/");
            int len = arr.length;
            String photoPath1;
            photoPath1 = photoPath + arr[len - 1];

            bitmap = BitmapFactory.decodeFile(photoPath1, options);
            if (bitmap != null) {
                iv_optiona.setImageBitmap(bitmap);
            }
        }

        if (!data.get(count).getOptionb_img().equalsIgnoreCase("null")) {
            iv_optionb.setVisibility(View.VISIBLE);
            String[] arr = data.get(count).getOptionb_img().split("/");
            int len = arr.length;
            String photoPath1;
            photoPath1 = photoPath + arr[len - 1];

            bitmap = BitmapFactory.decodeFile(photoPath1, options);
            if (bitmap != null) {
                iv_optionb.setImageBitmap(bitmap);
            }
        }

        if (!data.get(count).getOptionc_img().equalsIgnoreCase("null")) {
            iv_optionc.setVisibility(View.VISIBLE);
            String[] arr = data.get(count).getOptionc_img().split("/");
            int len = arr.length;
            String photoPath1;
            photoPath1 = photoPath + arr[len - 1];

            bitmap = BitmapFactory.decodeFile(photoPath1, options);
            if (bitmap != null) {
                iv_optionc.setImageBitmap(bitmap);
            }

        }

        if (!data.get(count).getOptiond_img().equalsIgnoreCase("null")) {
            iv_optiond.setVisibility(View.VISIBLE);
            String[] arr = data.get(count).getOptiond_img().split("/");

            int len = arr.length;
            String photoPath1;
            photoPath1 = photoPath + arr[len - 1];

            bitmap = BitmapFactory.decodeFile(photoPath1, options);
            if (bitmap != null) {
                iv_optiond.setImageBitmap(bitmap);
            }
        }

        if (!data.get(count).getOptione_img().equalsIgnoreCase("null")) {
            iv_optiond.setVisibility(View.VISIBLE);
            String[] arr = data.get(count).getOptione_img().split("/");

            int len = arr.length;
            String photoPath1;
            photoPath1 = photoPath + arr[len - 1];

            bitmap = BitmapFactory.decodeFile(photoPath1, options);
            if (bitmap != null) {
                iv_optiond.setImageBitmap(bitmap);
            }
        }

        if (!data.get(count).getSolution_img().equalsIgnoreCase("null")) {

            iv_solution.setVisibility(View.VISIBLE);

            String[] arr = data.get(count).getSolution_img().split("/");
            int len = arr.length;
            String photoPath1;
            photoPath1 = photoPath + arr[len - 1];

            bitmap = BitmapFactory.decodeFile(photoPath1, options);
            if (bitmap != null) {

                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;

                int h;
                h = Constants.Width / imageWidth;
                h = (int) h * imageHeight;
                //Logg("ration_height",h+"  "+Constants.Width);
                ViewGroup.LayoutParams params = iv_solution.getLayoutParams();
                int width, height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, h, getResources().getDisplayMetrics());
                width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, imageWidth, getResources().getDisplayMetrics());

                params.height = h;
                iv_solution.setLayoutParams(params);

                iv_solution.setImageBitmap(bitmap);

            }
        }


        tv_question.setText(Html.fromHtml(data.get(count).getQuestion()));

        tv_optiona.setText(Html.fromHtml(data.get(count).getOptiona()));

        tv_optionb.setText(Html.fromHtml(data.get(count).getOptionb()));

        tv_optionc.setText(Html.fromHtml(data.get(count).getOptionc()));

        tv_optiond.setText(Html.fromHtml(data.get(count).getOptiond()));

        tv_optione.setText(Html.fromHtml(data.get(count).getOptione()));

        tv_hint.setText(Html.fromHtml(data.get(count).getHint()));

        if (data.get(count).getSolution().equalsIgnoreCase("null"))
            tv_solution.setText("");
        else
            tv_solution.setText(Html.fromHtml(data.get(count).getSolution()));


        try {
            if (object.getString("ques_answer").equalsIgnoreCase("a")) {
                if (object.getString("ques_status").equalsIgnoreCase("R"))
                    checkAnswer_ifno("a", lay_optiona, "#33009900");
                else
                    checkAnswer_ifno("a", lay_optiona, "#33ff0000");
            } else if (object.getString("ques_answer").equalsIgnoreCase("b")) {
                if (object.getString("ques_status").equalsIgnoreCase("R"))
                    checkAnswer_ifno("b", lay_optionb, "#33009900");
                else
                    checkAnswer_ifno("b", lay_optionb, "#33ff0000");
            } else if (object.getString("ques_answer").equalsIgnoreCase("c")) {
                if (object.getString("ques_status").equalsIgnoreCase("R"))
                    checkAnswer_ifno("c", lay_optionc, "#33009900");
                else
                    checkAnswer_ifno("c", lay_optionc, "#33ff0000");
            } else if (object.getString("ques_answer").equalsIgnoreCase("d")) {
                if (object.getString("ques_status").equalsIgnoreCase("R"))
                    checkAnswer_ifno("d", lay_optiond, "#33009900");
                else
                    checkAnswer_ifno("d", lay_optiond, "#33ff0000");
            } else if (object.getString("ques_answer").equalsIgnoreCase("e")) {
                if (object.getString("ques_status").equalsIgnoreCase("R"))
                    checkAnswer_ifno("e", lay_optione, "#33009900");
                else
                    checkAnswer_ifno("e", lay_optione, "#33ff0000");
            }


        } catch (JSONException e) {

        }

    }

    public void getQuestion(JSONObject obj) {
        try {


            JSONObject jsondata = new JSONObject(obj.getJSONObject("question").getString("jsondata"));

            JSONObject obj_data = jsondata.getJSONObject("data");

            JSONObject obj_que = obj_data.getJSONObject("question");
            JSONObject obj_a = obj_data.getJSONObject("option a");
            JSONObject obj_b = obj_data.getJSONObject("option b");
            JSONObject obj_c = obj_data.getJSONObject("option c");
            JSONObject obj_d = obj_data.getJSONObject("option d");
            JSONObject obj_e = obj_data.getJSONObject("option e");

            JSONObject obj_ans = obj_data.getJSONObject("answer");
            JSONObject obj_hint = obj_data.getJSONObject("hint");
            JSONObject obj_sol = obj_data.getJSONObject("solution");

            data.add(new Question_getset_new(obj.getJSONObject("question").getInt("id"), obj.getJSONObject("question").getString("testid"),
                    obj.getJSONObject("question").getString("sub_test_name"), obj_hint.getString("value"), obj_que.getString("value"),
                    obj_a.getString("value"), obj_b.getString("value"), obj_c.getString("value"),
                    obj_d.getString("value"), obj_e.getString("value"), obj_ans.getString("value"),
                    obj_sol.getString("value"), obj_hint.getString("image"), obj_que.getString("image"),
                    obj_a.getString("image"), obj_b.getString("image"), obj_c.getString("image"),
                    obj_d.getString("image"), obj_e.getString("image"), obj_sol.getString("image"), "no",
                    "", "", "", "", ""));

            setQuestion();

        } catch (JSONException e) {

        }
    }
}