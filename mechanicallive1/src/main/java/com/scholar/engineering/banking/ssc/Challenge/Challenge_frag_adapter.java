package com.scholar.engineering.banking.ssc.Challenge;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;

import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.HomeActivity;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.scholar.engineering.banking.ssc.Challenge.Challenge_Users.challenge_sub;
import static com.scholar.engineering.banking.ssc.Challenge.Choose_Opponent_list_Adapter.challenge_id;
import static com.scholar.engineering.banking.ssc.Challenge.Play_Challenge.user_count;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.toast;
import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

/**
 * Created by delaine on 18/1/18.
*/

public class Challenge_frag_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context c;
    JSONArray data;

    DisplayImageOptions options;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    ImageLoader imageLoader;

    Typeface font_demi;
    Typeface font_medium;

    public Challenge_frag_adapter(Context c, JSONArray data) {

        this.c=c;
        this.data=data;

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.user_default)
                .showImageForEmptyUri(R.drawable.user_default)
                .showImageOnFail(R.drawable.user_default)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        font_medium = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");
        font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");


    }

    public void addItem(JSONArray array1){
        for(int i=0;i<array1.length();i++){
            try {
                data.put(array1.getJSONObject(i));
            } catch (JSONException e) {

            }
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            final Context c=parent.getContext();
            RecyclerView.ViewHolder vh = null;
                    View  view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_play_challenges_item, parent, false);
                    vh= new ViewHolder1(view1);
            return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

                ViewHolder1 vh1= (ViewHolder1) holder;
                setChallenge(vh1,position);

    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    public class ViewHolder1 extends RecyclerView.ViewHolder{
        // challenge layout

        TextView tv_accept,tv_reject,tv_ignore,tv_challengername,tv_username,tv_time;
        LinearLayout layout;
        CircleImageView imageView;

        public ViewHolder1(View v) {
            super(v);

            tv_accept=(TextView) v.findViewById(R.id.tv_accept);
            tv_reject=(TextView) v.findViewById(R.id.tv_reject);
            tv_ignore=(TextView) v.findViewById(R.id.tv_change);
            tv_time=(TextView)v.findViewById(R.id.tv_time);
            tv_username=(TextView)v.findViewById(R.id.tv_username);

            tv_challengername=(TextView) v.findViewById(R.id.tv_challengername);
            imageView=(CircleImageView)v.findViewById(R.id.iv_challenge);

            tv_accept.setTypeface(font_medium);
            tv_reject.setTypeface(font_medium);
            tv_ignore.setTypeface(font_medium);
            tv_challengername.setTypeface(font_medium);
            tv_username.setTypeface(font_demi);
            tv_time.setTypeface(font_medium);
        }
    }

    public void setChallenge(ViewHolder1 vh1, final int p){

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(c));

        try {
            object=data.getJSONObject(p);

            if (object.getString("opponent_image").length()>4)
                imageLoader.getInstance().displayImage(object.getString("opponent_image"), vh1.imageView, options, animateFirstListener);


            if(object.getString("userid").equalsIgnoreCase(Constants.User_Email))
            {

                Logg("if",object.getInt("id")+" ");
                vh1.tv_reject.setVisibility(View.INVISIBLE);
                vh1.tv_accept.setVisibility(View.INVISIBLE);
                vh1.tv_ignore.setText(object.getString("win_status"));

                if(object.getString("win_status").equalsIgnoreCase("Expired")) {
                    vh1.tv_ignore.setText("Expired");
                    vh1.tv_reject.setVisibility(View.GONE);
                    vh1.tv_accept.setVisibility(View.GONE);
                }

                if(object.getString("win_status").equalsIgnoreCase("Your Turn")) {
                    vh1.tv_ignore.setText("Your Turn");

                    vh1.tv_ignore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {

                                object=data.getJSONObject(p);
                                challenge_id=object.getString("id");
                                challenge_sub= object.getString("subject");
                                if(object.getString("userid").equalsIgnoreCase(Constants.User_Email)) {
                                    getChallengeData(object.getString("opponentid"),
                                            object.getString("opponent_admission_no"),object.getString("id"),
                                            object.getString("subject"));
                                }
                                else{
                                    getChallengeData(object.getString("userid"),
                                            object.getString("user_admission_no"),object.getString("id"),
                                            object.getString("subject"));
                                }

                            } catch (JSONException e) {

                            }
                        }
                    });

                }
                else if(object.getString("win_status").equalsIgnoreCase("You Won")|
                        object.getString("win_status").equalsIgnoreCase("You Lost")|
                        object.getString("win_status").equalsIgnoreCase("Draw")){

                    if(object.getString("win_status").equalsIgnoreCase("You Won"))
                        vh1.tv_ignore.setText("You Won");
                    else if(object.getString("win_status").equalsIgnoreCase("You Lost"))
                        vh1.tv_ignore.setText("You Lost");
                    else if(object.getString("win_status").equalsIgnoreCase("Draw"))
                        vh1.tv_ignore.setText("Draw");

                    vh1.tv_ignore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {

                                object=data.getJSONObject(p);
                                challenge_id=object.getString("id");
                                Intent in = new Intent(c, Challenge_Result_List.class);
                                in.putExtra("challenge_id",challenge_id);
                                c.startActivity(in);

                            } catch (JSONException e) {

                            }
                        }
                    });
                }
                else if(object.getString("win_status").equalsIgnoreCase("Waiting")){
                        vh1.tv_ignore.setText("Waiting");

                    vh1.tv_ignore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {

                                object=data.getJSONObject(p);
                                challenge_id=object.getString("id");

                                user_count=object.getInt("user_correct");
                                challenge_sub=object.getString("subject");

                                if(object.getString("userid").equalsIgnoreCase(Constants.User_Email)) {
                                    gotoWaitingPage(object.getString("opponentid"),
                                            object.getString("opponent_admission_no"),object.getString("id"),
                                            object.getString("subject"));
                                }
                                else{
                                    gotoWaitingPage(object.getString("userid"),
                                            object.getString("user_admission_no"),object.getString("id"),
                                            object.getString("subject"));
                                }

                            } catch (JSONException e) {

                            }
                        }
                    });
                }

                vh1.tv_username.setText(object.getString("opponent_name"));
                vh1.tv_time.setText(Utility.getDate(object.getString("created_at")));
                vh1.tv_challengername.setText("You Challenged " + object.getString("opponent_name") +" In " + object.getString("subject")+" ("+object.getString("user_correct")+":"+object.getString("opponent_correct")+")");

            }else {

                Logg("else",object.getInt("id")+" ");

                if(object.getString("win_status").equalsIgnoreCase("You Lost")) {
                    vh1.tv_ignore.setText("You Lost");
                    vh1.tv_accept.setVisibility(View.INVISIBLE);
                    vh1.tv_reject.setVisibility(View.INVISIBLE);
                }else if(object.getString("win_status").equalsIgnoreCase("You Won")) {
                    vh1.tv_ignore.setText("You Won");
                    vh1.tv_accept.setVisibility(View.INVISIBLE);
                    vh1.tv_reject.setVisibility(View.INVISIBLE);
                }
                else if(object.getString("win_status").equalsIgnoreCase("Draw")) {
                    vh1.tv_ignore.setText("Draw");
                    vh1.tv_accept.setVisibility(View.INVISIBLE);
                    vh1.tv_reject.setVisibility(View.INVISIBLE);
                }
                else if(object.getString("win_status").equalsIgnoreCase("Rejected")) {
                    vh1.tv_ignore.setText("Rejected");
                    vh1.tv_accept.setVisibility(View.INVISIBLE);
                    vh1.tv_reject.setVisibility(View.INVISIBLE);
                }
                else{
                    vh1.tv_accept.setVisibility(View.VISIBLE);
                    vh1.tv_reject.setVisibility(View.VISIBLE);
                    vh1.tv_ignore.setVisibility(View.VISIBLE);
                    vh1.tv_ignore.setText("Ignore Challenge From This User");

                if(object.getString("win_status").equalsIgnoreCase("Expired")) {
                    vh1.tv_ignore.setText("Expired");
                    vh1.tv_reject.setVisibility(View.GONE);
                    vh1.tv_accept.setVisibility(View.GONE);
                }


                    vh1.tv_reject.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                object=data.getJSONObject(p);
                                challenge_id=object.getString("id");
                                VolleyAccept(object.getString("id"),"reject",p);
                            } catch (JSONException e) {

                            }
                        }
                    });

                    vh1.tv_accept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {

                                object=data.getJSONObject(p);

                                VolleyAccept(object.getString("id"),"accept",p);
                                challenge_id=object.getString("id");
                                challenge_sub=object.getString("subject");
                                if(object.getString("userid").equalsIgnoreCase(Constants.User_Email)) {
                                    getChallengeData(object.getString("opponentid"),
                                            object.getString("opponent_admission_no"),object.getString("id"),
                                            object.getString("subject"));
                                }
                                else{
                                    getChallengeData(object.getString("userid"),
                                            object.getString("user_admission_no"),object.getString("id"),
                                            object.getString("subject"));
                                }

                            } catch (JSONException e) {

                            }
                        }
                    });
                }
                vh1.tv_username.setText(object.getString("opponent_name"));
                vh1.tv_time.setText(Utility.getDate(object.getString("created_at")));

                vh1.tv_challengername.setText(object.getString("opponent_name") + " Has Challenged You In "+ object.getString("subject")+" ("+object.getString("user_correct")+":"+object.getString("opponent_correct")+")");

                vh1.tv_ignore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            object=data.getJSONObject(p);
                            challenge_id=object.getString("id");

                            if(object.getString("win_status").equalsIgnoreCase("You Lost")|
                                    object.getString("win_status").equalsIgnoreCase("You Won")|
                                    object.getString("win_status").equalsIgnoreCase("Draw")) {
                                //open result page
                                Intent in = new Intent(c, Challenge_Result_List.class);
                                in.putExtra("challenge_id",challenge_id);
                                c.startActivity(in);
                            }
                            else if(object.getString("win_status").equalsIgnoreCase("Rejected")) {

                            }
                            else if(object.getString("win_status").equalsIgnoreCase("Expired")){

                            }
                            else
                                VolleyAccept(object.getString("id"),"ignore",p);
                        } catch (JSONException e) {
                        }
                    }
                });

            }
        } catch (JSONException e) {

        }
    }

    JSONObject object;

    public void VolleyAccept(final String c_id, final String sts, final int p) {

        RequestQueue queue = Volley.newRequestQueue(c);
        String url="";
            url = Constants.URL_LV+"accept_challenge";//reject accept api
        url = url.replace(" ", "%20");
        Logg("name", url);

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {

                Logg("response", s);
                try
                {

                    if(s.length()>0)
                    {
                        JSONObject arg0=new JSONObject(s);
                        if(arg0.getString("status").equalsIgnoreCase("success")) {

                            if(sts.equalsIgnoreCase("accept")){
//                                Intent in = new Intent(c, Challenge.class);
//                                c.startActivity(in);
                            }
                            else{
                                data.remove(p);
                                notifyDataSetChanged();
                            }
                        }
                        else if(arg0.getString("status").equalsIgnoreCase("fail")){
                            toast(c,arg0.getString("message"));
                        }
                    }
                }

                catch (Exception e)
                {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(c,CommonUtils.volleyerror(volleyError));
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", User_Email);
                map.put("challenge_id",c_id);
                map.put("type",sts);

                Logg("map",map.toString());

                return map;

            }
        };

        int socketTimeout = 20000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);

    }

    public void getChallengeData(final String opp_email,final String opp_std_roll,final String challangeid,
                                 final String challangesubject) {

        RequestQueue queue = Volley.newRequestQueue(c);
        String url="";
        url = Constants.URL_LV+"getopponent_details";
        url = url.replace(" ", "%20");
        Logg("name", url);

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {

                Logg("response", s);
                try
                {

                    if(s.length()>0)
                    {
                        JSONObject arg0=new JSONObject(s);
                        if(arg0.getString("status").equalsIgnoreCase("true"))
                        {

                            Intent in = new Intent(c, Challenge.class);
                            in.putExtra("data",arg0+"");
                            c.startActivity(in);

                        }
                        else if(arg0.getString("status").equalsIgnoreCase("false"))
                        {
                            toast(c,arg0.getString("message"));
                        }

                    }
                }
                catch (Exception e)
                {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(c,CommonUtils.volleyerror(volleyError));
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> map = new HashMap<String, String>();

                map.put("email",User_Email);
                map.put("opponent_email",opp_email);
                map.put("subject",challangesubject);
                map.put("challenge_id",challangeid);
                map.put("status","false");
                map.put("roll",Constants.addmissionno);
                map.put("opponent_roll",opp_std_roll);
                Logg("map",map.toString());
                return map;
            }
        }
                ;

        int socketTimeout = 20000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);

    }

    public void gotoWaitingPage(final String opp_email,final String opp_std_roll,final String challangeid,
                                final String challangesubject) {

        RequestQueue queue = Volley.newRequestQueue(c);
        String url="";
        url = Constants.URL_LV+"getopponent_details";
        url = url.replace(" ", "%20");
        Logg("name", url);

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {

                Logg("response", s);
                try
                {

                    if(s.length()>0)
                    {
                        JSONObject arg0=new JSONObject(s);
                        if(arg0.getString("status").equalsIgnoreCase("true")) {
                            Intent in = new Intent(c, Waiting_for_Opponent.class);
                            in.putExtra("data",arg0+"");
                            c.startActivity(in);
                        }
                        else if(arg0.getString("status").equalsIgnoreCase("false")){
                            toast(c,arg0.getString("message"));
                        }
                    }
                }
                catch (Exception e)
                {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(c,CommonUtils.volleyerror(volleyError));
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> map = new HashMap<String, String>();
                map.put("email",User_Email);
                map.put("opponent_email",opp_email);
                map.put("subject",challangesubject);
                map.put("challenge_id",challangeid);
                map.put("status","false");
                map.put("roll",Constants.addmissionno);
                map.put("opponent_roll",opp_std_roll);
                Logg("map",map.toString());
                return map;
            }
        };

        int socketTimeout = 20000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);

    }

}

