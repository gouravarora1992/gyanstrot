//package com.scholar.engineering.banking.ssc.newScreens;
//
//import androidx.annotation.RequiresApi;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.core.app.ActivityCompat;
//import androidx.core.content.ContextCompat;
//
//import android.Manifest;
//import android.content.pm.PackageManager;
//import android.graphics.Bitmap;
//import android.os.Build;
//import android.os.Bundle;
//import android.util.Log;
//import android.webkit.PermissionRequest;
//import android.webkit.WebChromeClient;
//import android.webkit.WebSettings;
//import android.webkit.WebView;
//import android.webkit.WebViewClient;
//
//import com.scholar.engineering.banking.ssc.R;
//
//import java.util.ArrayList;
//
//public class Webview_BigBlueButton extends AppCompatActivity {
//
//    private PermissionRequest myRequest;
//    private static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 101;
//    WebView myWebView;
//    String[] PERMISSIONS = {
//            Manifest.permission.MODIFY_AUDIO_SETTINGS,
//            android.Manifest.permission.RECORD_AUDIO,
//            android.Manifest.permission.CAMERA
//    };
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_webview_layout);
//
//        myWebView = (WebView) findViewById(R.id.webview);
//        WebSettings webSettings = myWebView.getSettings();
//        webSettings.setMediaPlaybackRequiresUserGesture(false);
////        webSettings.setUserAgentString("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36");
//        webSettings.setUserAgentString("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36");
//
//        webSettings.setJavaScriptEnabled(true);
//
//
//
//        myWebView.setWebChromeClient(new WebChromeClient(){
//            @Override
//            public void onPermissionRequest(final PermissionRequest request) {
//                myRequest = request;
//
//                for (String permission : request.getResources()) {
//                    switch (permission) {
//                        case "android.webkit.resource.AUDIO_CAPTURE": {
//                            askForPermission(request.getOrigin().toString(),
//                                    PERMISSIONS, MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
//                            break;
//                        }
//                    }
//                }
//            }
//        });
//        myWebView.setWebViewClient(new WebViewClient() {
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                view.loadUrl(url);
//                return true;
//            }
//            @Override
//            public void onPageStarted (WebView view, String url, Bitmap favicon){
//            }
//        });
//
//        myWebView.loadUrl("http://meeting.sdvfaridabad.in/");
//
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode,
//                                           String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case MY_PERMISSIONS_REQUEST_RECORD_AUDIO: {
//                Log.e("WebView", "PERMISSION FOR AUDIO");
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//
//                    // permission was granted, yay! Do the
//                    // contacts-related task you need to do.
//                    myRequest.grant(myRequest.getResources());
//                    myWebView.loadUrl("http://meeting.sdvfaridabad.in/");
//
//                } else {
//
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                }
//            }
//            // other 'case' lines to check for other
//            // permissions this app might request
//        }
//    }
//    public void askForPermission(String origin, String[] permission, int requestCode) {
//        Log.d("WebView", "inside askForPermission for" + origin + "with" + permission);
//
//        if (ContextCompat.checkSelfPermission(getApplicationContext(),
//                permission[0])
//                != PackageManager.PERMISSION_GRANTED) {
//
//            // Should we show an explanation?
//            if (ActivityCompat.shouldShowRequestPermissionRationale(Webview_BigBlueButton.this,
//                    permission[0])) {
//
//                // Show an expanation to the user *asynchronously* -- don't block
//                // this thread waiting for the user's response! After the user
//                // sees the explanation, try again to request the permission.
//
//            } else {
//
//                // No explanation needed, we can request the permission.
//
//                ActivityCompat.requestPermissions(Webview_BigBlueButton.this,
//                        new String[]{permission[0]},
//                        requestCode);
//            }
//        } else {
//            myRequest.grant(myRequest.getResources());
//        }
//    }
//
//}