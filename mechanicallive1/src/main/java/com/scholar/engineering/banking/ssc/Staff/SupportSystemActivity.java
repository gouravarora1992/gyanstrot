package com.scholar.engineering.banking.ssc.Staff;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;

import com.google.android.material.tabs.TabLayout;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.adapter.ViewPagerAdapter;
import com.scholar.engineering.banking.ssc.databinding.ActivitySupportSystemBinding;

public class SupportSystemActivity extends AppCompatActivity implements View.OnClickListener {

    ActivitySupportSystemBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_support_system);

        binding.imgBack.setOnClickListener(this);
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(""));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(""));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(""));
        binding.tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), SupportSystemActivity.this, binding.tabLayout.getTabCount());
        binding.viewPager.setAdapter(adapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);

        binding.tabLayout.getTabAt(0).setIcon(R.drawable.support_assigned);
        binding.tabLayout.getTabAt(1).setIcon(R.drawable.support_unsolved);
        binding.tabLayout.getTabAt(2).setIcon(R.drawable.support_solved);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back:
                onBackPressed();
                break;
        }
    }
}