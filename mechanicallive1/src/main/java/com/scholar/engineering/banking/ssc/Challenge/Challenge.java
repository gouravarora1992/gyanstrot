package com.scholar.engineering.banking.ssc.Challenge;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.scholar.engineering.banking.ssc.HomeActivity;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Challenge extends AppCompatActivity {

    TextView tv_start,tv_subjectname,tv_winningpoints;
    TextView tv_name_lu,tv_occupation_lu,tv_points_lu;
    TextView tv_name_opp,tv_occupation_opp,tv_points_opp;
    CircleImageView imageView_lu,imageView_opp;
    JSONArray data;
    JSONObject object,obj_login,obj_opponent;
    Typeface font_demi;
    Typeface font_medium;
    RequestOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.challenge);

        font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");
        font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");


        tv_subjectname=(TextView)findViewById(R.id.tv_subjectname);
        tv_winningpoints=(TextView)findViewById(R.id.tv_winningpoint);
        tv_name_lu=(TextView)findViewById(R.id.tv_name_lu);
        tv_occupation_lu=(TextView)findViewById(R.id.tv_occupation_lu);
        tv_points_lu=(TextView)findViewById(R.id.tv_points_lu);
        tv_name_opp=(TextView)findViewById(R.id.tv_name_opp);
        tv_occupation_opp=(TextView)findViewById(R.id.tv_occupation_opp);
        tv_points_opp=(TextView)findViewById(R.id.tv_points_opp);

        tv_subjectname.setTypeface(font_demi);
        tv_winningpoints.setTypeface(font_medium);
        tv_name_lu.setTypeface(font_medium);
        tv_occupation_lu.setTypeface(font_medium);
        tv_points_lu.setTypeface(font_medium);
        tv_name_opp.setTypeface(font_medium);
        tv_occupation_opp.setTypeface(font_medium);
        tv_points_opp.setTypeface(font_medium);

        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        imageView_lu=(CircleImageView)findViewById(R.id.imageView_lu);
        imageView_opp=(CircleImageView)findViewById(R.id.imageView_opp);

        if(getIntent().hasExtra("data")) {

            try {

                Logg("challangers data",getIntent().getStringExtra("data"));
                object = new JSONObject(getIntent().getStringExtra("data"));
                data=object.getJSONArray("data");

                if(data.getJSONObject(0).getString("email").equalsIgnoreCase(Constants.User_Email)) {
                    obj_login = data.getJSONObject(0);
                    obj_opponent=data.getJSONObject(1);
                }
                else{
                    obj_login = data.getJSONObject(1);
                    obj_opponent=data.getJSONObject(0);
                }

                tv_subjectname.setText(object.getString("subject"));
                tv_winningpoints.setText(object.getString("win_points")+" Points");
                tv_name_lu.setText(obj_login.getString("Student_name"));

                if(obj_login.has("image"))
                    if(obj_login.getString("image").length()>4)
                    Glide.with(this)
                            .load(obj_login.getString("image"))
                            .apply(options)
                            .into(imageView_lu);

                if(obj_login.has("level"))
                    tv_occupation_lu.setText(obj_login.getString("level")+" Level");
                else
                    tv_occupation_lu.setText("");

                tv_points_lu.setText(obj_login.getString("xp")+" Points");
                tv_name_opp.setText(obj_opponent.getString("Student_name"));

                if(obj_opponent.has("image"))
                    if(obj_opponent.getString("image").length()>4)
                    Glide.with(this)
                            .load(obj_opponent.getString("image"))
                            .apply(options)
                            .into(imageView_opp);

                if(obj_opponent.has("level"))
                    tv_occupation_opp.setText(obj_opponent.getString("level")+" Level");
                else
                    tv_occupation_opp.setText("");

                tv_points_opp.setText(obj_opponent.getString("xp")+" Points");

            } catch (JSONException e) {
                Log.e("exceptioon","",e);
            }
        }

        initToolbar();

    }

    public void initToolbar(){

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Challenge");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_start=(TextView)findViewById(R.id.tv_start);

        tv_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(Challenge.this,Play_Challenge.class);
                in.putExtra("data",object+"");
                startActivity(in);
                finish();
            }
        });
    }
}
