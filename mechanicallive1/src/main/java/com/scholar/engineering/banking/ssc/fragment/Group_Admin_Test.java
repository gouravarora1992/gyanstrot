package com.scholar.engineering.banking.ssc.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.scholar.engineering.banking.ssc.Group_Profile;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.adapter.Home_RecyclerViewAdapter2;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.DatabaseHandler;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;

public class Group_Admin_Test extends Fragment{

	private ProgressDialog progress;
	RecyclerView recyclerView;
	ArrayList<Home_getset> homedata;
	Home_RecyclerViewAdapter2 adapter;
	String username,usercollage,image,mail;
	SharedPreferences pref;
	String status="";
	DatabaseHandler dbh;
	int index=0;
	public static int tab_pos=0;
	RelativeLayout footerlayout;
	TextView loadmore,tv_nodata;
	ProgressWheel wheelbar;
	SwipeRefreshLayout swipeRefreshLayout;
	NetworkConnection nw;

	View v;

	Group_Profile activity;

	@Override
	public void onAttach(Context context) {
		activity= (Group_Profile) context;
		super.onAttach(context);
	}

//	@Override
//	public void onAttach(Activity activity) {
//
//		super.onAttach(activity);
//	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		 v=inflater.inflate(R.layout.group_comment_activity,container,false);

		Constants.Current_Activity=0;

		nw=new NetworkConnection(getActivity());

		swipeRefreshLayout=(SwipeRefreshLayout)v.findViewById(R.id.swipe_refresh_layout);

		recyclerView=(RecyclerView)v.findViewById(R.id.recyclerView);
		LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
		mLayoutManager.scrollToPositionWithOffset(0,0);
		recyclerView.setLayoutManager(mLayoutManager);
		recyclerView.setHasFixedSize(true);
		recyclerView.setNestedScrollingEnabled(false);

		footerlayout=(RelativeLayout)v.findViewById(R.id.footer_layout);
		loadmore=(TextView)v.findViewById(R.id.moreButton);
		wheelbar=(ProgressWheel)v.findViewById(R.id.progress_wheel);
		tv_nodata=(TextView)v.findViewById(R.id.tv_nodata);

		index=0;

		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());

		pref =getActivity().getSharedPreferences("myref", Context.MODE_PRIVATE);
		username=pref.getString("name","");
		image=pref.getString("image","");
		usercollage=pref.getString("collage","");
		mail=User_Email;

		homedata=new ArrayList<>();

		dbh=new DatabaseHandler(getActivity());

		getData();

		swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				homedata=new ArrayList<>();
				index= 0;
				getData();
			}
		});

		footerlayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				Home_getset home;

				home= (Home_getset) homedata.get(homedata.size()-1);

				index= home.getId();

				getData();

				loadmore.setText("");
				wheelbar.setVisibility(View.VISIBLE);

			}
		});

		return v;
	}

	public void getData()
	{

		if(!nw.isConnectingToInternet()) {
			 swipeRefreshLayout.setRefreshing(false);
			internetconnection(0);
			return;
		}

//		progress = ProgressDialog.show(getActivity(), null, null, true);
//		progress.setContentView(R.layout.progressdialog);
//		progress.setCanceledOnTouchOutside(false);
//		progress.setCancelable(false);
//		progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//
//		if(index==0)
//		progress.show();

		RequestQueue queue= Volley.newRequestQueue(getActivity());
		JSONObject obj=new JSONObject();

		String url= Constants.URL+"newapi2_04/get_group_admin_post.php?email="+mail+"&index="+index+"&tab_pos=1"+"&groupid="+Group_Profile.pid;

		//Logg("checkversion_url",url);

		JsonObjectRequest json=new JsonObjectRequest(Request.Method.POST, url, obj,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res) {
				// TODO Auto-generated method stub
				try {

					//Logg("get_group_admin_post1",res.toString());

					if(res.has("groupstatus")) {
						 status = res.getString("groupstatus");
//						JSONObject ob = jr1.getJSONObject(0);
//						status=ob.getString("status");

						if(status.equalsIgnoreCase("public")){
							//lay_post.setVisibility(View.VISIBLE);
						}
					}

						if(res.has("data")) {
						JSONArray jr = res.getJSONArray("data");

							if (jr.length() < 10)
								footerlayout.setVisibility(View.GONE);
							else {
								footerlayout.setVisibility(View.VISIBLE);
								loadmore.setText("Load Previous..");
								wheelbar.setVisibility(View.GONE);
							}

//							RemoveAds();

							for (int i = 0; i < jr.length(); i++) {
							JSONObject ob = jr.getJSONObject(i);

//							String time = Utility.getDate(ob.getString("timestamp"));

								Home_getset object=new Home_getset();
								object.setId(ob.getInt("id"));
								object.setTimestamp(ob.getString("timestamp"));
								object.setLikecount(ob.getInt("likes"));
								object.setCommentcount(ob.getInt("comment"));
								object.setViewcount(ob.getInt("view"));
								object.setUid(ob.getString("uid"));
								object.setPosttype(ob.getString("posttype"));
								object.setGroupid(ob.getString("groupid"));
								object.setFieldtype(ob.getString("field_type"));
								object.setJsonfield(ob.getString("jsondata"));
								object.setLikestatus(ob.getString("likestatus"));
								object.setPosturl(ob.getString("posturl"));
								object.setPostdescription(ob.getString("post_description"));
								if(ob.has("AppVersion"))
									object.setAppVersion(ob.getString("AppVersion"));
								homedata.add(object);
							}

						//Logg("data.size", homedata.size() + "");
						adapter = new Home_RecyclerViewAdapter2(activity, homedata,"Group_comment_activity");
						recyclerView.setAdapter(adapter);

							swipeRefreshLayout.setRefreshing(false);
						}
					else {
							footerlayout.setVisibility(View.GONE);

							if(homedata.size()<1){
								tv_nodata.setVisibility(View.VISIBLE);
								tv_nodata.setText("Sorry! No Test Found In This Group");
							}
					}


					swipeRefreshLayout.setRefreshing(false);
//					if(progress.isShowing())
//						progress.dismiss();

				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					footerlayout.setVisibility(View.GONE);
					//Logg("checkversion exp",e.getMessage());
					swipeRefreshLayout.setRefreshing(false);
//					if(progress.isShowing())
//						progress.dismiss();
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				footerlayout.setVisibility(View.GONE);
				swipeRefreshLayout.setRefreshing(false);
				//Logg("checkversion error", String.valueOf(arg0));
//				if(progress.isShowing())
//					progress.dismiss();
			}
		});
//		{
//			@Override
//			protected Map<String,String> getParams(){
//				Map<String,String> params = new HashMap<String, String>();
//
//				params.put("email",mail);
//				params.put("index",index+"");
//				params.put("groupid", Group_Profile.pid);
//				params.put("tab_pos","1");
//
//				return params;
//			}
//		}

		queue.add(json);
	}


	public void internetconnection(final int i){

		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.network_alert);
		dialog.show();

		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
		TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

		tv_tryagain.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {


				dialog.dismiss();

				getData();

			}
		});

		iv_cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

	}


}
