package com.scholar.engineering.banking.ssc.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 26-04-2017.
 */

public class UserSharedPreferences {
    public static UserSharedPreferences preferences;
    SharedPreferences userPreferences, playeruserPreferences;

    SharedPreferences.Editor edit, playeredit;

    public UserSharedPreferences(Context context) {

        userPreferences = context.getSharedPreferences("UserSharedPreferences", Context.MODE_PRIVATE);
        edit = userPreferences.edit();
    }

    public static UserSharedPreferences getInstance(Context context) {

        if (preferences == null) {
            preferences = new UserSharedPreferences(context.getApplicationContext());
        }
        return preferences;
    }

    public UserSharedPreferences(Context context, String a) {
        playeruserPreferences = context.getSharedPreferences("UserSharedPreferencesplayer", Context.MODE_PRIVATE);
        playeredit = playeruserPreferences.edit();
    }

    public void Clear() {
        edit.clear();
        edit.commit();
    }

    public void setboolean(boolean bol) {

        edit.putBoolean("boolean", bol);
        edit.commit();
    }

    public boolean getboolean() {

        return userPreferences.getBoolean("boolean", false);
    }

    public void setgroupfollow(boolean groupfollow) {

        edit.putBoolean("groupfollow", groupfollow);
        edit.commit();
    }

    public boolean getgroupfollow() {

        return userPreferences.getBoolean("groupfollow", false);
    }

    public void setcate_id(String cate_id) {

        edit.putString("cate_id", cate_id);
        edit.commit();
    }

    public void setsiblingdata(String siblingdata) {
        edit.putString("siblingdata", siblingdata);
        edit.commit();
    }

    public String getsiblingdata() {
        return userPreferences.getString("siblingdata", "");
    }


    public String getcate_id() {

        return userPreferences.getString("cate_id", "");
    }

    public void setcategoryfollow(boolean categoryfollow) {

        edit.putBoolean("categoryfollow", categoryfollow);
        edit.commit();
    }

    public boolean getcategoryfollow() {

        return userPreferences.getBoolean("categoryfollow", false);
    }

    public void setname(String name) {

        edit.putString("name", name);
        edit.commit();
    }

    public String getname() {

        return userPreferences.getString("name", "");
    }

    public void setclass(String classname) {

        edit.putString("class", classname);
        edit.commit();
    }

    public String getclass() {

        return userPreferences.getString("class", "0");
    }


    public void setsection(String section) {

        edit.putString("section", section);
        edit.commit();
    }

    public String getsection() {

        return userPreferences.getString("section", "");

    }

    public void setaddmissiono(String addmissiono) {

        edit.putString("addmissiono", addmissiono);
        edit.commit();
    }

    public String getaddmissiono() {

        return userPreferences.getString("addmissiono", "");
    }


    public void setemail(String email) {
        Log.e("email", email);
        edit.putString("email", email);
        edit.commit();
    }

    public String getemail() {
        return userPreferences.getString("email", "");
    }

    public void setlogin(Boolean login) {
        edit.putBoolean("login", login);
        edit.commit();
    }

    public Boolean getlogin() {
        return userPreferences.getBoolean("login", false);
    }


    public void setuserid(String id) {
        edit.putString("id", id);
        edit.commit();
    }

    public String getuserid() {
        return userPreferences.getString("id", "");
    }

    public void setrefferuserid(String refferuserid) {
        edit.putString("refferuserid", refferuserid);
        edit.commit();
    }

    public String getrefferuserid() {
        return userPreferences.getString("refferuserid", "");
    }

    public void setmobile(String mobile) {
        edit.putString("mobile", mobile);
        edit.commit();
    }

    public String getmobile() {
        return userPreferences.getString("mobile", "");
    }

    public void setadmobile(String admobile) {
        edit.putString("admobile", admobile);
        edit.commit();
    }

    public String getadmobile() {
        return userPreferences.getString("admobile", "");
    }

    public void setmobileverification(boolean mobileverification) {
        edit.putBoolean("mobileverification", mobileverification);
        edit.commit();
    }

    public boolean getmobileverification() {
        return userPreferences.getBoolean("mobileverification", false);
    }


    public void setimage(String image) {
        edit.putString("image", image);
        edit.commit();
    }

    public String getimage() {
        return userPreferences.getString("image", "");
    }

    public void settoken(String token) {
        edit.putString("token", token);
        edit.commit();
    }

    public String gettoken() {
        return userPreferences.getString("token", "");
    }

    public void setaddress(String address) {
        edit.putString("address", address);
        edit.commit();
    }

    public String getaddress() {
        return userPreferences.getString("address", "");
    }

    public void setcreated_at(String created_at) {
        edit.putString("created_at", created_at);
        edit.commit();
    }

    public String getcreated_at() {
        return userPreferences.getString("created_at", "");
    }

    public void setindustry(String industry) {
        edit.putString("industry", industry);
        edit.commit();
    }

    public String getindustry() {
        return userPreferences.getString("industry", "");
    }

    public void setaudiorate(String audiorate) {
        edit.putString("audiorate", audiorate);
        edit.commit();
    }

    public String getaudiorate() {
        return userPreferences.getString("audiorate", "0");
    }

    public void setvideorate(String videorate) {
        edit.putString("videorate", videorate);
        edit.commit();
    }

    public String getvideorate() {
        return userPreferences.getString("videorate", "0");
    }


    public void setbio(String bio) {
        edit.putString("bio", bio);
        edit.commit();
    }

    public String getbio() {
        return userPreferences.getString("bio", "");
    }

    public void setdob(String dob) {
        edit.putString("dob", dob);
        edit.commit();
    }

    public String getdob() {
        return userPreferences.getString("dob", "");
    }

    public void setprofilesetup(Boolean profilesetup) {
        edit.putBoolean("profilesetup", profilesetup);
        edit.commit();
    }

    public Boolean getprofilesetup() {
        return userPreferences.getBoolean("profilesetup", false);
    }

    public void setsetuppin(Boolean setuppin) {
        edit.putBoolean("setuppin", setuppin);
        edit.commit();
    }

    public Boolean getsetuppin() {
        return userPreferences.getBoolean("setuppin", false);
    }


    public void setplayerid(String playerid) {
        playeredit.putString("playerid", playerid);
        playeredit.commit();
    }

    public String getplayerid() {
        return playeruserPreferences.getString("playerid", "");
    }

    public void setlanguage(String language) {
        playeredit.putString("language", language);
        playeredit.commit();
    }

    public String getlanguage() {
        return playeruserPreferences.getString("language", "en");
    }

    public void setdeviceid(String deviceid) {
        playeredit.putString("deviceid", deviceid);
        playeredit.commit();
    }

    public String getdeviceid() {
        return playeruserPreferences.getString("deviceid", "");
    }


    public void setLoginType(String loginType){
        edit.putString(Constants.LoginType,loginType);
        edit.commit();
    }

    public String getLogintype(){
        return userPreferences.getString(Constants.LoginType,"");
    }

    public void setIsUserLogin(String key, boolean value){
        edit.putString(Constants.IS_USER_LOGIN,"");
        edit.commit();
    }

    public String getIsUserLogin(){
        return userPreferences.getString(Constants.LoginType,"");
    }


    public void setData(String name, String staffEmail, String role, String date, Integer id, String assignClass, String assignSection){
        edit.putString("name", name);
        edit.putString("role",role);
        edit.putString("email",staffEmail);
        edit.putString("date",date);
        edit.putInt("id",id);
        edit.putString("assignClass",assignClass);
        edit.putString("assignSection",assignSection);
        edit.commit();
    }

    public String getDate(){
        return userPreferences.getString("date","");
    }

    public String getStaffEmail(){
        return userPreferences.getString("email","");
    }

    public String getRole(){
        return userPreferences.getString("role","");
    }

    public String getName(){
        return userPreferences.getString("name","");
    }

    public Integer getId(){
        return userPreferences.getInt("id",0);
    }

    public String getAssignClass(){
        return userPreferences.getString("assignClass","");
    }

    public String getAssignSection(){
        return userPreferences.getString("assignSection","");
    }

    public void setStudentClass(List<String> list){
        Gson gson = new Gson();
        String json = gson.toJson(list);
        edit.putString("student_class", json);
        edit.commit();
    }

    public List<String> getStudentClass(){
        Gson gson = new Gson();
        String json = userPreferences.getString("student_class", null);
        Type type = new TypeToken<List<String>>() {}.getType();
        return gson.fromJson(json, type);
    }


    public void setStdRoll(ArrayList<String> list){
        Gson gson = new Gson();
        String json = gson.toJson(list);
        edit.putString("std_roll", json);
        edit.commit();
    }

    public ArrayList<String> getStdRoll(){
        Gson gson = new Gson();
        String json = userPreferences.getString("std_roll", null);
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        return gson.fromJson(json, type);
    }
}
