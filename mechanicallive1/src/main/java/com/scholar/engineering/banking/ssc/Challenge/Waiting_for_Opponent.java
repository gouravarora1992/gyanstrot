package com.scholar.engineering.banking.ssc.Challenge;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.scholar.engineering.banking.ssc.HomeActivity;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.scholar.engineering.banking.ssc.Challenge.Play_Challenge.opp_count;
import static com.scholar.engineering.banking.ssc.Challenge.Play_Challenge.user_count;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Waiting_for_Opponent extends AppCompatActivity {

    TextView tv_subjectname,tv_winningpoints,tv_que_count;
    TextView tv_name_lu,tv_occupation_lu;
    TextView tv_name_opp,tv_occupation_opp;
    CircleImageView imageView_lu,imageView_opp;
    JSONArray data;
    JSONObject object,obj_login,obj_opponent;

    RequestOptions options;
    Typeface font_demi;
    Typeface font_medium;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.waiting_for_opponent);

        font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");
        font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");

        tv_subjectname=(TextView)findViewById(R.id.tv_challengename);
        tv_que_count=(TextView)findViewById(R.id.tv_questioncount);
        tv_winningpoints=(TextView)findViewById(R.id.tv_points);
        tv_name_lu=(TextView)findViewById(R.id.tv_name_lu);
        tv_occupation_lu=(TextView)findViewById(R.id.tv_occupation_lu);
        tv_name_opp=(TextView)findViewById(R.id.tv_name_opp);
        tv_occupation_opp=(TextView)findViewById(R.id.tv_occupation_opp);
        TextView tv_win_text=(TextView)findViewById(R.id.tv_winnertext);

        imageView_lu=(CircleImageView)findViewById(R.id.imageView_lu);
        imageView_opp=(CircleImageView)findViewById(R.id.imageView_opp);

        tv_subjectname.setTypeface(font_demi);
        tv_que_count.setTypeface(font_medium);
        tv_winningpoints.setTypeface(font_medium);
        tv_name_lu.setTypeface(font_medium);
        tv_occupation_lu.setTypeface(font_medium);
        tv_name_lu.setTypeface(font_medium);
        tv_occupation_opp.setTypeface(font_medium);
        tv_win_text.setTypeface(font_medium);


        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.user_default)
                .error(R.drawable.user_default)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        if(getIntent().hasExtra("data")) {
            try {

                object = new JSONObject(getIntent().getStringExtra("data"));

                data=object.getJSONArray("data");


                if(data.getJSONObject(0).getString("email").equalsIgnoreCase(Constants.User_Email)) {
                    obj_login = data.getJSONObject(0);
                    obj_opponent=data.getJSONObject(1);
                }
                else{
                    obj_login = data.getJSONObject(1);
                    obj_opponent=data.getJSONObject(0);
                }

                tv_subjectname.setText(object.getString("subject"));
                tv_winningpoints.setText(object.getString("win_points")+" Points");
                tv_name_lu.setText(obj_login.getString("Student_name"));

                if(obj_login.has("image"))
                    if(obj_login.getString("image").length()>4)
                    Glide.with(this)
                            .load(obj_login.getString("image"))
                            .apply(options)
                            .into(imageView_lu);


                if(obj_login.has("level"))
                    tv_occupation_lu.setText("Level "+obj_login.getString("level"));
                else
                    tv_occupation_lu.setText("");
                tv_name_opp.setText(obj_opponent.getString("Student_name"));

                if(obj_opponent.has("image"))
                    if(obj_opponent.getString("image").length()>4)
                    Glide.with(this)
                            .load(obj_opponent.getString("image"))
                            .apply(options)
                            .into(imageView_opp);

                if(obj_opponent.has("level"))
                    tv_occupation_opp.setText("Level "+obj_opponent.getString("level"));
                else
                    tv_occupation_opp.setText("");

                tv_que_count.setText(user_count+":"+opp_count);

            } catch (JSONException e) {

            }
        }
        initToolbar();
    }

    public void initToolbar(){

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Waiting For Opponent");

        Utility.applyFontForToolbarTitle(toolbar,this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

}
