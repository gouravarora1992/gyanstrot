package com.scholar.engineering.banking.ssc.Staff.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StudentInfoModel {

    @SerializedName("status")
    @Expose
    public Boolean status;
    @SerializedName("data")
    @Expose
    public Data data;
    @SerializedName("message")
    @Expose
    public String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class Data {

        @SerializedName("id")
        @Expose
        public Long id;
        @SerializedName("name")
        @Expose
        public Object name;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("password")
        @Expose
        public String password;
        @SerializedName("std_roll")
        @Expose
        public String stdRoll;
        @SerializedName("Student_name")
        @Expose
        public String studentName;
        @SerializedName("Student_class")
        @Expose
        public String studentClass;
        @SerializedName("Student_section")
        @Expose
        public String studentSection;
        @SerializedName("img")
        @Expose
        public Object img;
        @SerializedName("F_name")
        @Expose
        public String fName;
        @SerializedName("F_organization")
        @Expose
        public Object fOrganization;
        @SerializedName("F_occupation")
        @Expose
        public String fOccupation;
        @SerializedName("F_designation")
        @Expose
        public Object fDesignation;
        @SerializedName("F_annual")
        @Expose
        public Object fAnnual;
        @SerializedName("F_mobile")
        @Expose
        public Object fMobile;
        @SerializedName("F_email")
        @Expose
        public Object fEmail;
        @SerializedName("Fofficeaddress")
        @Expose
        public String fofficeaddress;
        @SerializedName("M_name")
        @Expose
        public String mName;
        @SerializedName("M_organization")
        @Expose
        public Object mOrganization;
        @SerializedName("M_occupation")
        @Expose
        public Object mOccupation;
        @SerializedName("M_designation")
        @Expose
        public Object mDesignation;
        @SerializedName("M_annual")
        @Expose
        public Object mAnnual;
        @SerializedName("M_mobile")
        @Expose
        public Object mMobile;
        @SerializedName("M_email")
        @Expose
        public Object mEmail;
        @SerializedName("Mofficeaddress")
        @Expose
        public String mofficeaddress;
        @SerializedName("L_name")
        @Expose
        public Object lName;
        @SerializedName("L_mobile")
        @Expose
        public Object lMobile;
        @SerializedName("L_relation")
        @Expose
        public Object lRelation;
        @SerializedName("L_address")
        @Expose
        public Object lAddress;
        @SerializedName("route_name")
        @Expose
        public Object routeName;
        @SerializedName("vechile_no")
        @Expose
        public Object vechileNo;
        @SerializedName("Driver_L_no")
        @Expose
        public Object driverLNo;
        @SerializedName("contact_no")
        @Expose
        public Object contactNo;
        @SerializedName("transport_type")
        @Expose
        public Object transportType;
        @SerializedName("transport_details")
        @Expose
        public Object transportDetails;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("updated_at")
        @Expose
        public String updatedAt;
        @SerializedName("F_telephone")
        @Expose
        public Object fTelephone;
        @SerializedName("dob")
        @Expose
        public String dob;
        @SerializedName("bloodgroup")
        @Expose
        public Object bloodgroup;
        @SerializedName("houseno")
        @Expose
        public String houseno;
        @SerializedName("phoneno")
        @Expose
        public Object phoneno;
        @SerializedName("address")
        @Expose
        public String address;
        @SerializedName("studentimage")
        @Expose
        public Object studentimage;
        @SerializedName("fatherimage")
        @Expose
        public Object fatherimage;
        @SerializedName("motherimage")
        @Expose
        public Object motherimage;
        @SerializedName("Foficemobile")
        @Expose
        public String foficemobile;
        @SerializedName("Mofficemobile")
        @Expose
        public String mofficemobile;
        @SerializedName("gaurdianimg")
        @Expose
        public Object gaurdianimg;
        @SerializedName("staff")
        @Expose
        public String staff;
        @SerializedName("isActive")
        @Expose
        public Long isActive;
        @SerializedName("isadmissionenquiry")
        @Expose
        public Long isadmissionenquiry;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Object getName() {
            return name;
        }

        public void setName(Object name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getStdRoll() {
            return stdRoll;
        }

        public void setStdRoll(String stdRoll) {
            this.stdRoll = stdRoll;
        }

        public String getStudentName() {
            return studentName;
        }

        public void setStudentName(String studentName) {
            this.studentName = studentName;
        }

        public String getStudentClass() {
            return studentClass;
        }

        public void setStudentClass(String studentClass) {
            this.studentClass = studentClass;
        }

        public String getStudentSection() {
            return studentSection;
        }

        public void setStudentSection(String studentSection) {
            this.studentSection = studentSection;
        }

        public Object getImg() {
            return img;
        }

        public void setImg(Object img) {
            this.img = img;
        }

        public String getfName() {
            return fName;
        }

        public void setfName(String fName) {
            this.fName = fName;
        }

        public Object getfOrganization() {
            return fOrganization;
        }

        public void setfOrganization(Object fOrganization) {
            this.fOrganization = fOrganization;
        }

        public String getfOccupation() {
            return fOccupation;
        }

        public void setfOccupation(String fOccupation) {
            this.fOccupation = fOccupation;
        }

        public Object getfDesignation() {
            return fDesignation;
        }

        public void setfDesignation(Object fDesignation) {
            this.fDesignation = fDesignation;
        }

        public Object getfAnnual() {
            return fAnnual;
        }

        public void setfAnnual(Object fAnnual) {
            this.fAnnual = fAnnual;
        }

        public Object getfMobile() {
            return fMobile;
        }

        public void setfMobile(Object fMobile) {
            this.fMobile = fMobile;
        }

        public Object getfEmail() {
            return fEmail;
        }

        public void setfEmail(Object fEmail) {
            this.fEmail = fEmail;
        }

        public String getFofficeaddress() {
            return fofficeaddress;
        }

        public void setFofficeaddress(String fofficeaddress) {
            this.fofficeaddress = fofficeaddress;
        }

        public String getmName() {
            return mName;
        }

        public void setmName(String mName) {
            this.mName = mName;
        }

        public Object getmOrganization() {
            return mOrganization;
        }

        public void setmOrganization(Object mOrganization) {
            this.mOrganization = mOrganization;
        }

        public Object getmOccupation() {
            return mOccupation;
        }

        public void setmOccupation(Object mOccupation) {
            this.mOccupation = mOccupation;
        }

        public Object getmDesignation() {
            return mDesignation;
        }

        public void setmDesignation(Object mDesignation) {
            this.mDesignation = mDesignation;
        }

        public Object getmAnnual() {
            return mAnnual;
        }

        public void setmAnnual(Object mAnnual) {
            this.mAnnual = mAnnual;
        }

        public Object getmMobile() {
            return mMobile;
        }

        public void setmMobile(Object mMobile) {
            this.mMobile = mMobile;
        }

        public Object getmEmail() {
            return mEmail;
        }

        public void setmEmail(Object mEmail) {
            this.mEmail = mEmail;
        }

        public String getMofficeaddress() {
            return mofficeaddress;
        }

        public void setMofficeaddress(String mofficeaddress) {
            this.mofficeaddress = mofficeaddress;
        }

        public Object getlName() {
            return lName;
        }

        public void setlName(Object lName) {
            this.lName = lName;
        }

        public Object getlMobile() {
            return lMobile;
        }

        public void setlMobile(Object lMobile) {
            this.lMobile = lMobile;
        }

        public Object getlRelation() {
            return lRelation;
        }

        public void setlRelation(Object lRelation) {
            this.lRelation = lRelation;
        }

        public Object getlAddress() {
            return lAddress;
        }

        public void setlAddress(Object lAddress) {
            this.lAddress = lAddress;
        }

        public Object getRouteName() {
            return routeName;
        }

        public void setRouteName(Object routeName) {
            this.routeName = routeName;
        }

        public Object getVechileNo() {
            return vechileNo;
        }

        public void setVechileNo(Object vechileNo) {
            this.vechileNo = vechileNo;
        }

        public Object getDriverLNo() {
            return driverLNo;
        }

        public void setDriverLNo(Object driverLNo) {
            this.driverLNo = driverLNo;
        }

        public Object getContactNo() {
            return contactNo;
        }

        public void setContactNo(Object contactNo) {
            this.contactNo = contactNo;
        }

        public Object getTransportType() {
            return transportType;
        }

        public void setTransportType(Object transportType) {
            this.transportType = transportType;
        }

        public Object getTransportDetails() {
            return transportDetails;
        }

        public void setTransportDetails(Object transportDetails) {
            this.transportDetails = transportDetails;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getfTelephone() {
            return fTelephone;
        }

        public void setfTelephone(Object fTelephone) {
            this.fTelephone = fTelephone;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public Object getBloodgroup() {
            return bloodgroup;
        }

        public void setBloodgroup(Object bloodgroup) {
            this.bloodgroup = bloodgroup;
        }

        public String getHouseno() {
            return houseno;
        }

        public void setHouseno(String houseno) {
            this.houseno = houseno;
        }

        public Object getPhoneno() {
            return phoneno;
        }

        public void setPhoneno(Object phoneno) {
            this.phoneno = phoneno;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Object getStudentimage() {
            return studentimage;
        }

        public void setStudentimage(Object studentimage) {
            this.studentimage = studentimage;
        }

        public Object getFatherimage() {
            return fatherimage;
        }

        public void setFatherimage(Object fatherimage) {
            this.fatherimage = fatherimage;
        }

        public Object getMotherimage() {
            return motherimage;
        }

        public void setMotherimage(Object motherimage) {
            this.motherimage = motherimage;
        }

        public String getFoficemobile() {
            return foficemobile;
        }

        public void setFoficemobile(String foficemobile) {
            this.foficemobile = foficemobile;
        }

        public String getMofficemobile() {
            return mofficemobile;
        }

        public void setMofficemobile(String mofficemobile) {
            this.mofficemobile = mofficemobile;
        }

        public Object getGaurdianimg() {
            return gaurdianimg;
        }

        public void setGaurdianimg(Object gaurdianimg) {
            this.gaurdianimg = gaurdianimg;
        }

        public String getStaff() {
            return staff;
        }

        public void setStaff(String staff) {
            this.staff = staff;
        }

        public Long getIsActive() {
            return isActive;
        }

        public void setIsActive(Long isActive) {
            this.isActive = isActive;
        }

        public Long getIsadmissionenquiry() {
            return isadmissionenquiry;
        }

        public void setIsadmissionenquiry(Long isadmissionenquiry) {
            this.isadmissionenquiry = isadmissionenquiry;
        }
    }
}
