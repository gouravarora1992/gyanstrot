package com.scholar.engineering.banking.ssc.Staff;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.LeaveDetailModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ViewRequestModel;
import com.scholar.engineering.banking.ssc.Staff.adapter.LeaveRequestAdapter;
import com.scholar.engineering.banking.ssc.databinding.ActivityLeaveDetailBinding;
import com.scholar.engineering.banking.ssc.getset.RetrofitObjectAPI;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class LeaveDetailActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityLeaveDetailBinding binding;
    NetworkConnection nw;
    String user_id;
 //   String url = "http://139.59.90.236:82/images/leaverelated/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_leave_detail);

        nw = new NetworkConnection(LeaveDetailActivity.this);
        binding.imgBack.setOnClickListener(this);

        if(getIntent().getStringExtra("calledFrom")!=null) {
            if (getIntent().getStringExtra("calledFrom").equalsIgnoreCase(LeaveRequestAdapter.class.getSimpleName()))
            {
                user_id = getIntent().getStringExtra("id");
            }
        }
        getLeaveDetail();
    }


    private void getLeaveDetail() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<LeaveDetailModel> call = service.getLeaveDetail(Integer.valueOf(user_id));

        Logg("urlLeaveDetail", Constants.URL_LV + "data?id=" + user_id);

        call.enqueue(new Callback<LeaveDetailModel>() {

            @Override
            public void onResponse(Call<LeaveDetailModel> call, retrofit2.Response<LeaveDetailModel> response) {
                if (response.isSuccessful()) {
                    try {
                        binding.tvLeaveType.setText(response.body().getData().getLeaveType());
                        binding.tvUsername.setText(response.body().getData().getUserdetail().getName());
                        binding.tvSubject.setText(response.body().getData().getSubject());
                        binding.tvStartDate.setText(response.body().getData().getStartDate());
                        binding.tvEndDate.setText(response.body().getData().getEndDate());
                        binding.tvDescription.setText(response.body().getData().getDescription());

                        if (response.body().getData().getStatus()==0){
                            binding.tvStatus.setText("Pending");
                        } else if (response.body().getData().getStatus()==1){
                            binding.tvStatus.setText("Approved");
                        } else if (response.body().getData().getStatus()==2){
                            binding.tvStatus.setText("Cancelled");
                        }

                        String date1 = response.body().getData().getCreatedAt();
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date date = null;
                        try {
                            date = dateFormat.parse(date1);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        String dateStr = formatter.format(date);
                        binding.tvCreatedAt.setText(dateStr);

                        if (response.body().getData().getServerIp()!=null){
                            String serverip = response.body().getData().getServerIp() +"/images/leaverelated/";
                            String file1 = serverip + response.body().getData().getFile();
                            Log.e("file1",file1);
                            binding.tvAttachement.setText(file1);
                        } else {
                            String url = "http://139.59.90.236:82/images/leaverelated/";
                            String file2 = url + response.body().getData().getFile();
                            Log.e("file2",file2);
                            binding.tvAttachement.setText(file2);
                        }

                        binding.tvRemark.setText(response.body().getData().getRemark());
                    } catch (Exception e) {
                    }

                } else {
                    Toast.makeText(LeaveDetailActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LeaveDetailModel> call, Throwable t) {
                Log.e("fail",t.getMessage());
            }
        });
    }


    public void internetconnection() {
        final Dialog dialog = new Dialog(LeaveDetailActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getLeaveDetail();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back:
                onBackPressed();
                break;
        }
    }
}