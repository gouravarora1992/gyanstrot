package com.scholar.engineering.banking.ssc.Challenge;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

/**
 * Created by surender on 3/14/2017.
*/

public class Challenge_Sub_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context c;
    JSONArray data;
    Dialog dialog;
    JSONObject object;

    Typeface font_demi;
    Typeface font_medium;

    public Challenge_Sub_Adapter(Context c,Dialog dialog, JSONArray data) {
        this.c = c;
        this.data=data;
        this.dialog=dialog;

        font_medium = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");
        font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

                View v0 = inflater.inflate(R.layout.challenge_sub_adapter, viewGroup, false);

                viewHolder = new ViewHolder0(v0);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

                ViewHolder0 vh0 = (ViewHolder0) viewHolder;

                setComment(vh0, position);
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    private void setComment(final ViewHolder0 vh0, final int p) {

        try {

            Logg("data",data.getJSONObject(p).getString("questions"));
            if (!data.getJSONObject(p).getString("questions").equals("0")) {
                object = data.getJSONObject(p);
                vh0.tv_topicname.setVisibility(View.VISIBLE);
                vh0.tv_topicname.setText(object.getString("topic_name"));
                vh0.layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            object=data.getJSONObject(p);
                            Intent in = new Intent(c,Challenge_Users.class);
                            in.putExtra("id",object.getString("id"));
                            in.putExtra("subject",object.getString("topic_name"));
                            c.startActivity(in);

                        } catch (JSONException e) {

                        }

                    }
                });
            }
            else
            {
                vh0.tv_topicname.setVisibility(View.GONE);
            }

        } catch (JSONException e) {

        }
    }

    public class ViewHolder0 extends RecyclerView.ViewHolder {

        TextView tv_topicname;
        LinearLayout layout;
        public ViewHolder0(View b) {
            super(b);
            tv_topicname=(TextView)b.findViewById(R.id.tv_sub_name);
            layout=(LinearLayout)b.findViewById(R.id.layout);
            tv_topicname.setTypeface(font_demi);
        }
    }

}
