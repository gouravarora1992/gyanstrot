package com.scholar.engineering.banking.ssc.Mentors_Champions;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;

//import com.crashlytics.android.Crashlytics;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.Champions_List;
import com.scholar.engineering.banking.ssc.Mantor_List;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
//import io.fabric.sdk.android.Fabric;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Mentors_Champions extends AppCompatActivity {

    Toolbar toolbar;
    CircleImageView dr_image;
    SharedPreferences share;
    ImageView iv_notification;
    SharedPreferences.Editor shareedit;
    int reward=0, comment=0;
    LinearLayout AboutUsLayout,ContactUsLayout,ShareandEarnLayout;
    public ViewPager viewPager;
    String versionName,ver_descr,ver_title;
    NetworkConnection nw;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    Typeface font_demi,font_medium;
    ImageLoader imageLoader = ImageLoader.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.champions_mentors);

        deleteCache(this);


        nw=new NetworkConnection(Mentors_Champions.this);

        share = getSharedPreferences("myref", 0);

        initToolbar();
        initViewPagerAndTabs();

        deleteCache(this);

    }

    private void initToolbar() {
        font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");
        font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Champions and Mentors");
        Utility.applyFontForToolbarTitle(toolbar,this);
    }

    private void initViewPagerAndTabs() {

        viewPager = (ViewPager) findViewById(R.id.viewPager);

        PagerAdapter1 pagerAdapter = new PagerAdapter1(getSupportFragmentManager());

        pagerAdapter.addFragment(new Champions_List(), "Champions");
        pagerAdapter.addFragment(new Mantor_List(), "Mentors");

        viewPager.setAdapter(pagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        if(getIntent().getStringExtra("type").equalsIgnoreCase("mentor")){
            viewPager.setCurrentItem(1);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    static class PagerAdapter1 extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter1(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }

    public void internetconnection(){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Mentors_Champions.this);

        alertDialog.setMessage("Check Your Internet Connection activity?");
        alertDialog.setPositiveButton("Reresh", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                viewPager.setCurrentItem(0);

            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logg("call_onDestroy()","Call onDestroy()");
        deleteCache(this);
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {}
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

}
