package com.scholar.engineering.banking.ssc.Staff.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SystemDeadlineModel {

    @SerializedName("supportsystem_id")
    @Expose
    private Integer supportsystem_id;
    @SerializedName("deadline")
    @Expose
    private String deadline;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getSupportsystem_id() {
        return supportsystem_id;
    }

    public void setSupportsystem_id(Integer supportsystem_id) {
        this.supportsystem_id = supportsystem_id;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
