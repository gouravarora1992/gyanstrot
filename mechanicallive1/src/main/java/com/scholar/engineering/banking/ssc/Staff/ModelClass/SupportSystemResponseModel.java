package com.scholar.engineering.banking.ssc.Staff.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SupportSystemResponseModel {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("supportticket_id")
        @Expose
        private Integer supportticketId;
        @SerializedName("master_id")
        @Expose
        private Integer masterId;
        @SerializedName("teachername")
        @Expose
        private String teachername;
        @SerializedName("response")
        @Expose
        private String response;
        @SerializedName("feedbackfile")
        @Expose
        private String feedbackfile;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("teacherstatus")
        @Expose
        private Integer teacherstatus;
        @SerializedName("deadline")
        @Expose
        private String deadline;
        @SerializedName("status")
        @Expose
        private Integer status;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getSupportticketId() {
            return supportticketId;
        }

        public void setSupportticketId(Integer supportticketId) {
            this.supportticketId = supportticketId;
        }

        public Integer getMasterId() {
            return masterId;
        }

        public void setMasterId(Integer masterId) {
            this.masterId = masterId;
        }

        public String getTeachername() {
            return teachername;
        }

        public void setTeachername(String teachername) {
            this.teachername = teachername;
        }

        public String getResponse() {
            return response;
        }

        public void setResponse(String response) {
            this.response = response;
        }

        public String getFeedbackfile() {
            return feedbackfile;
        }

        public void setFeedbackfile(String feedbackfile) {
            this.feedbackfile = feedbackfile;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Integer getTeacherstatus() {
            return teacherstatus;
        }

        public void setTeacherstatus(Integer teacherstatus) {
            this.teacherstatus = teacherstatus;
        }

        public String getDeadline() {
            return deadline;
        }

        public void setDeadline(String deadline) {
            this.deadline = deadline;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }
    }
}
