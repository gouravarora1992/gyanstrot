package com.scholar.engineering.banking.ssc.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scholar.engineering.banking.ssc.Play_new;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.getset.Home_getset;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.Utility.check_key;

/**
 * Created by surender on 3/14/2017.
 */

public class Result_Test_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context c;
    SharedPreferences pref;
    SharedPreferences.Editor prefedit;
    Typeface font_demi, font_medium;
    ArrayList<Object> data;

    JSONObject object,jsondata;

    public Result_Test_Adapter(Context c, ArrayList<Object> data) {
        this.c = c;
        this.data=data;
        pref =c.getSharedPreferences("myref", 0);

        Logg("datasize>>",this.data.size()+"   ");
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

                View v0 = inflater.inflate(R.layout.result_test_adapter, viewGroup, false);

                viewHolder = new ViewHolder0(v0);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

                ViewHolder0 vh0 = (ViewHolder0) viewHolder;

                setComment(vh0, position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private void setComment(final ViewHolder0 vh0, final int p) {

        try {
            Typeface font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");
            Typeface font_medium = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");

            final Home_getset home= (Home_getset)data.get(p);

            jsondata=new JSONObject(home.getJsonfield());



            vh0.tv_testname.setText(jsondata.getString("testname"));
            vh0.tv_totalquestion.setText(jsondata.getString("question")+" Ques");
            vh0.tv_time.setText(jsondata.getString("time")+" Mins");
             vh0.tv_testname.setTypeface(font_demi);
            vh0.tv_totalquestion.setTypeface(font_medium);
            vh0.tv_time.setTypeface(font_medium);

            vh0.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {



                    check_key(c,home,"result");
                    if(Play_new.h!=null)
                    Play_new.h.sendEmptyMessage(0);

//                    try {
//                        Utility.checkclass="result";
////                        object=data.getJSONObject(p);
//
//                        jsondata=new JSONObject(home.getJsonfield());
//
//                        if(Utility.checkPayment(c,jsondata)) {
//                            if (Utility.checkFile(Constants.URL + "admin/tests_images/" + home.getId() + ".zip") == 200) {
//                              //  jsonobj = new JSONObject(home.getJsonfield());
//                                Utility.startTest(c, String.valueOf(home.getId()));
//                            } else {
//
//                                Intent in = new Intent(c, Play_new.class);
//                                in.putExtra("testid", String.valueOf(home.getId()));
//                                c.startActivity(in);
//                            }
//                        }
//                        else{
//                            Utility.data=home;
//                            Utility.payPayment(c,jsondata,home.getId());
//                        }
//
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }

                }
            });

        } catch (JSONException e) {
        }

    }

    public class ViewHolder0 extends RecyclerView.ViewHolder {

        //  **************************  Set Group Comment  ********************

        TextView tv_testname;
        TextView tv_totalquestion;
        TextView tv_time;
        LinearLayout layout;

        public ViewHolder0(View b) {
            super(b);

            tv_testname=(TextView)b.findViewById(R.id.tv_testname);

            tv_totalquestion=(TextView)b.findViewById(R.id.tv_totalquestion);

            tv_time=(TextView)b.findViewById(R.id.tv_time);

            layout=(LinearLayout)b.findViewById(R.id.layout);

        }
    }

}
