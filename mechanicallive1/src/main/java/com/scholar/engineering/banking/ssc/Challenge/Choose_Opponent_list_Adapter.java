package com.scholar.engineering.banking.ssc.Challenge;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.HomeActivity;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.userprofile.UserProfile;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.scholar.engineering.banking.ssc.Challenge.Challenge_Users.challenge_sub;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.Utility.timediff;

/**
 * Created by surender on 12/03/2018.
*/

public class Choose_Opponent_list_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context c;
    JSONObject object;
    JSONArray data;
    public static String challenge_id="";
    DisplayImageOptions options;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    ImageLoader imageLoader;

    Calendar cal = Calendar.getInstance();
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String formattedDate = df.format(cal.getTime());

    Typeface font_demi;
    Typeface font_medium;

    public Choose_Opponent_list_Adapter(Context c, JSONArray data) {

        Logg("choose_opponent_list","adapter");

        this.data=new JSONArray(new ArrayList<String>());
        this.c = c;
        this.data=data;


        formattedDate = df.format(cal.getTime());

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.user_default)
                .showImageForEmptyUri(R.drawable.user_default)
                .showImageOnFail(R.drawable.user_default)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
        font_medium = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");
        font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");

    }

    public void addItem(JSONArray array1){
        Logg("choose_opponent_list","addItem");

        for(int i=0;i<array1.length();i++){
            try {
                data.put(array1.getJSONObject(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v0=null;

        v0 = inflater.inflate(R.layout.choose_opponent_item, viewGroup, false);

        viewHolder = new ViewHolder0(v0);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

                ViewHolder0 vh0 = (ViewHolder0) viewHolder;

                setComment(vh0, position);
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    private void setComment(final ViewHolder0 vh0, final int p) {

        try {

            object=data.getJSONObject(p);

            imageLoader = ImageLoader.getInstance();
            imageLoader.init(ImageLoaderConfiguration.createDefault(c));

            vh0.tv_opponentname.setText(object.getString("Student_name"));
            vh0.tv_college.setText(object.getString("Student_class")+"-"+object.getString("Student_section"));

            if(object.has("image"))
                if (object.getString("image").length()>4)
                    imageLoader.getInstance().displayImage(object.getString("image"), vh0.imageView, options, animateFirstListener);

            vh0.imageView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    try {
                        object=data.getJSONObject(p);

                        Intent in = new Intent(c, UserProfile.class);
                        in.putExtra("email",object.getString("email"));
                        c.startActivity(in);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            vh0.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        object=data.getJSONObject(p);
                        getData(object.getString("email"),object.getString("std_roll"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            checkuserstatus(object.getString("std_roll"),vh0.tv_online);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public class ViewHolder0 extends RecyclerView.ViewHolder {

        //  **************************  Set Group Comment  ********************

        LinearLayout layout;
        TextView tv_opponentname,tv_college,tv_online;
        CircleImageView imageView;

        public ViewHolder0(View b) {
            super(b);

            layout=(LinearLayout)b.findViewById(R.id.layout);
            tv_opponentname=(TextView)b.findViewById(R.id.tv_opponentname);
            tv_college=(TextView)b.findViewById(R.id.tv_college);
            tv_online=(TextView)b.findViewById(R.id.tv_online);
            imageView=(CircleImageView)b.findViewById(R.id.iv_opponent);

            tv_opponentname.setTypeface(font_demi);
            tv_college.setTypeface(font_medium);
            tv_online.setTypeface(font_medium);

        }
    }

    public void getData(final String opp_email,final String opp_std_roll) {

        RequestQueue queue = Volley.newRequestQueue(c);
        String url="";
        url = Constants.URL_LV+"getopponent_details";
        url = url.replace(" ", "%20");
        Logg("getopponent_details", url);

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {

                Logg("response getopponent_details", s);
                try
                {

                    if(s.length()>0)
                    {
                        JSONObject arg0=new JSONObject(s);

                        if(arg0.getString("status").equalsIgnoreCase("true")) {

                            if(arg0.has("challenge_id"))
                                challenge_id=arg0.getString("challenge_id");

                            Intent in = new Intent(c, Challenge.class);
                            in.putExtra("data",arg0+"");
                            c.startActivity(in);

                            ((Activity)c).finish();
                        }
                        else if(arg0.getString("status").equalsIgnoreCase("false")){
                            CommonUtils.toast(c,arg0.getString("message"));
                        }
                    }
                }

                catch (Exception e)
                {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(c,CommonUtils.volleyerror(volleyError));
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> map = new HashMap<String, String>();
                map.put("email",Constants.User_Email);
                map.put("opponent_email",opp_email);
                map.put("subject",challenge_sub);
                map.put("status","true");
                map.put("roll",Constants.addmissionno);
                map.put("opponent_roll",opp_std_roll);
                Logg("map",map.toString());
                return map;
            }
        };
        request.setShouldCache(false);
        int socketTimeout = 20000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);

    }

    public void checkuserstatus(String id, final TextView tv){
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        DatabaseReference ref = database.child("users");

        ref.child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Logg("value11",dataSnapshot.getValue()+" ");
                if(dataSnapshot.getValue()!=null) {
                    if(dataSnapshot.getValue().equals("online")) {
                        tv.setText("online");
                        tv.setTextSize(11f);
                    }
                    else{
                        tv.setText(timediff(dataSnapshot.getValue()+"",formattedDate));
                        tv.setTextSize(11f);
                        tv.setSelected(true);
                    }
                }
                else {
                    tv.setText("");
                    tv.setTextSize(11f);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Logg("value11",databaseError+" ");
                tv.setText("");
                tv.setTextSize(11f);
            }
        });

    }

}
