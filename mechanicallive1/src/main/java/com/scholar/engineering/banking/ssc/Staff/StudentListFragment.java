package com.scholar.engineering.banking.ssc.Staff;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ChatModelClass;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.StaffListModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.StudentListModel;
import com.scholar.engineering.banking.ssc.Staff.adapter.ChatAdapter;
import com.scholar.engineering.banking.ssc.Staff.adapter.StudentListAdapter;
import com.scholar.engineering.banking.ssc.databinding.FragmentStaffListBinding;
import com.scholar.engineering.banking.ssc.getset.RetrofitObjectAPI;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import java.util.ArrayList;
import java.util.List;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class StudentListFragment extends Fragment {

    FragmentStaffListBinding binding;
    StudentListAdapter studentListAdapter;
    NetworkConnection nw;
    UserSharedPreferences preferences;
    UserSharedPreferences playerpreferences;
    List<String> studentClass;
    RadioButton rb;
    List<StudentListModel.Datum> studentList;
    String selected_class;

    public StudentListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding =FragmentStaffListBinding.inflate(inflater, container, false);

        studentList = new ArrayList<>();
        nw = new NetworkConnection(getActivity());
        playerpreferences=new UserSharedPreferences(getActivity(),"playerpreferences");
        preferences=UserSharedPreferences.getInstance(getActivity());
        studentClass = preferences.getStudentClass();
        Log.e("studentClass",""+studentClass);

        if (studentClass.size()==1){
            selected_class = studentClass.get(0);
            getStudentList(selected_class);
        } else {
            classDailog();
        }

        binding.searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                getStudentSearch(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return binding.getRoot();
    }

    private void setData(List<StudentListModel.Datum> datumList){
        studentListAdapter = new StudentListAdapter(getActivity(),datumList);
        binding.recycleView.setHasFixedSize(true);
        binding.recycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.recycleView.setAdapter(studentListAdapter);
    }


    private void getStudentList(String studentClassss) {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<StudentListModel> call = service.getStudentList(studentClassss);
        Logg("studentList", Constants.URL_LV + "studentData?" +studentClassss);

        call.enqueue(new Callback<StudentListModel>() {

            @Override
            public void onResponse(Call<StudentListModel> call, retrofit2.Response<StudentListModel> response) {
                if (response.isSuccessful()) {
                    studentList = response.body().getData();
                    setData(studentList);
                } else {
                    Toast.makeText(getActivity(), "" + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StudentListModel> call, Throwable t) {
                Log.e("fail",t.getMessage());
            }
        });
    }


    private void getStudentSearch(String abcd) {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<StudentListModel> call = service.getStudentSearch(selected_class,abcd);
        Logg("studentListSearch", Constants.URL_LV + "selectedClass?"+ selected_class +"studentData?" +abcd);

        call.enqueue(new Callback<StudentListModel>() {

            @Override
            public void onResponse(Call<StudentListModel> call, retrofit2.Response<StudentListModel> response) {
                if (response.isSuccessful()) {
                    studentList.clear();
                    setData(response.body().getData());
                } else {
                    Toast.makeText(getActivity(), ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StudentListModel> call, Throwable t) {
                Log.e("fail",t.getMessage());
            }
        });
    }



    private void classDailog() {
        final Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Black);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.select_class_dialog);
        dialog.setCancelable(true);

        final RadioGroup radioGroup = dialog.findViewById(R.id.radioGroup);
        ImageView img_close = dialog.findViewById(R.id.img_close);
        Button btn =dialog.findViewById(R.id.btnUpdate);

        for(int i =0; i<studentClass.size();i++) {
            RadioButton radioButtonView = new RadioButton(getActivity());
            radioButtonView.setText(studentClass.get(i));
            radioButtonView.setId(i);
            RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT);
            radioGroup.addView(radioButtonView, rprms);
        }

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                rb= dialog.findViewById(checkedId);
                selected_class = rb.getText().toString();
           //     Toast.makeText(getActivity(),rb.getText().toString(),Toast.LENGTH_SHORT).show();
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int radioId = radioGroup.getCheckedRadioButtonId();
                if (radioId == -1) {
                    Toast.makeText(getActivity(), "Please select any one class", Toast.LENGTH_SHORT).show();
                } else {
                    getStudentList(selected_class);
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }


    public void internetconnection() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

}