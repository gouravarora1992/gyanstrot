package com.scholar.engineering.banking.ssc.getset;

public class gettr_settr {

	String  topic,startdate,member,status,image,payment,payment_status;
	int id,comment;

	public gettr_settr(String topic, int comment, int id,String member,String startdate,
					   String status,String image,String payment,String payment_status) {
		super();
		this.topic = topic;
		this.comment = comment;
		this.id = id;
		this.member=member;
		this.startdate=startdate;
		this.status=status;
		this.image=image;
		this.payment=payment;
		this.payment_status=payment_status;
	}

	public String getPayment_status() {
		return payment_status;
	}

	public void setPayment_status(String payment_status) {
		this.payment_status = payment_status;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMember() {
		return member;
	}

	public void setMember(String member) {
		this.member = member;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public int getComment() {
		return comment;
	}

	public void setComment(int comment) {
		this.comment = comment;
	}
}
