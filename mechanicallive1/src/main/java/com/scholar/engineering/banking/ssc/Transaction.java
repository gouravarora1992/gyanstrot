package com.scholar.engineering.banking.ssc;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
//import com.paytm.pgsdk.PaytmOrder;
//import com.paytm.pgsdk.PaytmPGService;
//import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Transaction {

	String fieldtype,postid;
	int amount;
	public static String orderId;

	UserSharedPreferences playerpreferences;

//	String transctionurl=Constants.URL+"Payment_new/CareerMuskandataForm.php?";

//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		// TODO Auto-generated method stub
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.transaction);
//
//		paymentpref=getSharedPreferences("paymentpref",0);
//
//		pref=getSharedPreferences("myref",MODE_PRIVATE);
//
//		email=pref.getString("useremail","");
//
////		w = (WebView)findViewById(R.id.webView1);
////		w.setWebViewClient(new MyBrowser());
//
////		Intent in=getIntent();
////
////		postid=in.getStringExtra("postid");
////		fieldtype=in.getStringExtra("field_type");
////		amount=in.getIntExtra("amount",0);
//
////		transctionurl=transctionurl+"postid="+postid+"&email="+email+"&amount="+amount+"&fieldtype="+fieldtype;
//
//		//Logg("useremail",email);
//		//Logg("testname",amount+"");
//        //Logg("transactionurl",transctionurl);
//
////		set = w.getSettings();
////		set.setJavaScriptEnabled(true);
////		w.loadUrl(transctionurl);
//
//		PAYMENT_REQUEST();
//
//	}

	public Transaction(Context c,String fieldtype, String postid, int amount){
		this.fieldtype=fieldtype;
		this.postid=postid;
		this.amount=amount;
		playerpreferences=new UserSharedPreferences(c,"playerpreferences");
	}


	public void internetconnection(Context c){

		final Dialog dialog = new Dialog(c);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.network_alert);
		dialog.show();

		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
		TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

		tv_tryagain.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.dismiss();

//				if(i==0)
//					volley_state();
//				else if(i==1)
//					imagevolley();

//				getData();

			}
		});

		iv_cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

	}

	public void PAYMENT_REQUEST(final Context c) {

		NetworkConnection nw=new NetworkConnection(c);

		if(!nw.isConnectingToInternet()) {
//            swipeRefreshLayout.setRefreshing(false);
			internetconnection(c);
			return;
		}

//		onStartTransaction(c,"","");
//
//		return;

		orderId=initOrderId();

		final ProgressDialog pd = new ProgressDialog(c);
		pd.setCancelable(true);
		pd.setMessage("Loading Please Wait");
		pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

		pd.show();

		RequestQueue queue = Volley.newRequestQueue(c);
		String url="";

		url = Constants.URL +"Paytm_php1/generateChecksum.php?";

		url = url.replace(" ", "%20");

		//Logg("name", url);

		StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
			@Override
			public void onResponse(String s) {

				Logg("response", s);
				try {
					if(s.length()>0)
					{
						JSONObject obj=new JSONObject(s);

						if(obj.has("data")) {
							obj=obj.getJSONObject("data");
							onStartTransaction(c,obj.getString("CHECKSUMHASH"),obj.getString("CALLBACK_URL"));
						}
						else {
							Toast.makeText(c,"Somthing is Wrong Can't Upload, Please Try Againe",Toast.LENGTH_LONG).show();
						}

						pd.dismiss();
						//Logg("response_string",s+" response");
					}
				}
				catch (Exception e)
				{
					pd.dismiss();
					Logg("e",e+"");
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {
				CommonUtils.toast(c,CommonUtils.volleyerror(volleyError));
				pd.dismiss();
			}
		}) {
			@Override
			protected Map<String, String> getParams() {

//Constants.URL+"Paytm_php/generateChecksum.php?MID="+Constants.MID+"&ORDER_ID="+orderId+"&CUST_ID="+Constants.CUST_ID+
//"&INDUSTRY_TYPE_ID="+Constants.INDUSTRY_TYPE_ID+"&CHANNEL_ID="+Constants.CHANNEL_ID+"&TXN_AMOUNT=1"+"&WEBSITE="+Constants.WEBSITE;

				Map<String, String> params = new HashMap<String, String>();
				params.put("MID", Constants.MID+"");
				params.put("ORDER_ID", orderId);
//				params.put("CUST_ID", "CUST"+Constants.Phone);
				params.put("INDUSTRY_TYPE_ID", Constants.INDUSTRY_TYPE_ID);
				params.put("CHANNEL_ID", Constants.CHANNEL_ID);
				params.put("TXN_AMOUNT", amount+"");
				params.put("WEBSITE", Constants.WEBSITE);
				params.put("EMAIL", Constants.User_Email);

				Logg("param=",params.toString());

				return params;
			}
		};

		int socketTimeout = 10000;
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		request.setRetryPolicy(policy);
		queue.add(request);

	}

	private String initOrderId() {

		Random r = new Random(System.currentTimeMillis());
		String orderId = "ORDER" + (1 + r.nextInt(2)) * 10000
				+ r.nextInt(10000);
//		EditText orderIdEditText = (EditText) findViewById(R.id.order_id);
//		orderIdEditText.setText(orderId);
		return orderId;
	}

	String res="";
	public void onStartTransaction(final Context c, String checksum, String callbackurl) {

		PaytmPGService Service=null;
		Service = PaytmPGService.getProductionService();

		Map<String, String> paramMap = new HashMap<String, String>();

		paramMap.put("MID", Constants.MID);
		paramMap.put("ORDER_ID", orderId);
//		paramMap.put("CUST_ID","CUST"+Constants.Phone);
		paramMap.put("INDUSTRY_TYPE_ID",Constants.INDUSTRY_TYPE_ID);
		paramMap.put("CHANNEL_ID", Constants.CHANNEL_ID);
		paramMap.put("TXN_AMOUNT", amount+"");
		paramMap.put("WEBSITE", Constants.WEBSITE);//
		paramMap.put("CALLBACK_URL" ,callbackurl);;
		paramMap.put( "CHECKSUMHASH" ,checksum);
		paramMap.put("EMAIL",Constants.User_Email);

//		paramMap.put("MID", "Delain69667434331823");
//		paramMap.put("ORDER_ID", "ORD2121");
//		paramMap.put("CUST_ID","CUST002");
//		paramMap.put("INDUSTRY_TYPE_ID","Retail");
//		paramMap.put("CHANNEL_ID", "WAP");
//		paramMap.put("TXN_AMOUNT", "1.00");
//		paramMap.put("WEBSITE", "APPPROD");//
//		paramMap.put("CALLBACK_URL" ,"https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=ORD2121");;
//		paramMap.put( "CHECKSUMHASH" ,"5Hs6H47Otk4GXOxXBzH4aLlwwIEDlD6JFbeSliJxOobIhYfuBAW9mmNuUHP07MoV77nzJz8DV5MMtCUrEm0/osoWcTQ1iRIm35gO5/nQKbU=");
//		paramMap.put("EMAIL","");

		Logg("param paytm map",paramMap+" ");

//		JSONObject obj=new JSONObject();
//
//		try {
//			obj.put("MID",Constants.MID);
//			obj.put("ORDERID",orderId);
//			obj.put("CHECKSUMHASH",checksum);
//
//			Logg("trasactionparam",obj.toString());
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}

		PaytmOrder Order = new PaytmOrder((HashMap<String, String>) paramMap);

		Service.initialize(Order,null);

		Service.startPaymentTransaction(c, true, true,
				new PaytmPaymentTransactionCallback() {
					@Override
					public void someUIErrorOccurred(String inErrorMessage) {

						Logg("someUIErrorOccurred",inErrorMessage);

					}
					@Override
					public void onTransactionResponse(Bundle bundle) {
						Logg("onTransactionResponse0", bundle.getString("STATUS"));
						Logg("onTransactionResponse",bundle+"");

//						res=String.valueOf(bundle).replace("Bundle","");

						if(bundle.getString("STATUS").equalsIgnoreCase("TXN_SUCCESS")) {
							try {

								if (fieldtype.equals("1")) {
									volleyinsertpaymentinfo(c);
//
								} else if (fieldtype.equals("2")) {
									try {
										volleyinsertpaymentinfo(c);
										JSONObject jsonobj = new JSONObject(Utility.data.getJsonfield());
									} catch (JSONException e) {
									}
								} else if (fieldtype.equals("5")) {
									volleyinsertpaymentinfo(c);
								} else if (fieldtype.equals("9")) {  //9=== group type
									volley_follow(c);
								}

							} catch (Exception e) {
							}
						}
						else{
							Toast.makeText(c,"payment fail",Toast.LENGTH_SHORT).show();
						}

//						volley("Paytm");
					}

					@Override
					public void networkNotAvailable() { // If network is not

						Logg("networkNotAvailable","networkNotAvailable");

					}

					@Override
					public void clientAuthenticationFailed(String inErrorMessage) {
						// This method gets called if client authentication
						// failed. // Failure may be due to following reasons //
						// 1. Server error or downtime. // 2. Server unable to
						// generate checksum or checksum response is not in
						// proper format. // 3. Server failed to authenticate
						// that client. That is value of payt_STATUS is 2. //
						// Error Message describes the reason for failure.
						Logg("clientAuthenticonFailed",inErrorMessage);
					}

					@Override
					public void onErrorLoadingWebPage(int iniErrorCode,
													  String inErrorMessage, String inFailingUrl) {

						Logg("onErrorLoadingWebPage","message "+inErrorMessage+" errorcode "+iniErrorCode+" infailUrl "+inFailingUrl);

					}

					// had to be added: NOTE
					@Override
					public void onBackPressedCancelTransaction() {
						// TODO Auto-generated method stub
					}
					@Override
					public void onTransactionCancel(String s, Bundle bundle) {

						Logg("onTransactionCancel","message "+s);

					}

				});
	}

	public void volleyinsertpaymentinfo(final Context c)
	{
		final ProgressDialog dialog = new ProgressDialog(c,R.style.AppTheme);
		dialog.setMessage("Loading..Please wait.");
		dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		dialog.setCanceledOnTouchOutside(false);
		dialog.getWindow().setGravity(Gravity.CENTER);
		dialog.show();

		RequestQueue queue= Volley.newRequestQueue(c);
		JSONObject json =new JSONObject();

		String url= Constants.URL+"newapi2_04/payment_by_online.php?amount="+amount+"&email="+
				Constants.User_Email+"&id="+postid+"&field_type="+fieldtype;

		url=url.replaceAll(" ","%20");

		JsonObjectRequest jsonreq=new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res) {
				//Logg("res",String.valueOf(res));
				// TODO Auto-generated method stub
				String s;

				try {
					if(res.getString("status").equals("success"))
					{

						if(fieldtype=="2") {

							if (Utility.checkFile(Constants.URL + "admin/tests_images/" + Utility.data.getId() + ".zip") == 200) {
								Utility.startTest(c, String.valueOf(Utility.data.getId()));
								Log.e("start","start");
							}
							else {
								Intent in = new Intent(c, Play_new.class);
								in.putExtra("testid", String.valueOf(Utility.data.getId()));
								c.startActivity(in);
								Log.e("end","end");
							}

//							if (Utility.checkFile(Constants.URL + "admin/tests_images/" +postid+ ".zip") == 200) {
//								Utility.startTest(Transaction.this, postid);
//							} else {
//								Intent in = new Intent(c, Play_new.class);
//								in.putExtra("testid", postid);
//								c.startActivity(in);
//							}
						}
						else if(fieldtype=="1"){
							Utility.openPost(c);
						}
						else if(fieldtype=="5"){
							Utility.Payment_success(c);
						}
//                        alert(des,pos);
					}
					else {
						Toast.makeText(c,"Can't Payment",Toast.LENGTH_SHORT).show();
					}

					dialog.dismiss();
				} catch (JSONException e) {
					dialog.dismiss();
				}

			}
		},new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg)
			{
				//Logg("error", String.valueOf(arg));
				dialog.dismiss();
				Toast.makeText(c,"Network Problem", Toast.LENGTH_SHORT).show();
			}


		});

		queue.add(jsonreq);
	}

	private void volley_follow(final Context c) {

		String url=Constants.URL+"newapi2_04/payment_group.php?";

		//Logg("url", url);

		RequestQueue que= Volley.newRequestQueue(c);

		//final String finalCmnt = cmnt;
		StringRequest obj1=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

			@Override
			public void onResponse(String s) {
				// TODO Auto-generated method stub

				//Logg("response",s);
				int size=s.length();
				try {

					JSONObject res=new JSONObject(s);

					if(res.getString("status").equals("success"))
					{

					}
					else {
						Toast.makeText(c,"Can't Payment",Toast.LENGTH_SHORT).show();
					}

				} catch (JSONException e) {
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError e) {
				// TODO Auto-generated method stub
				//progress.dismiss();
				//Logg("error",e.toString());
			}
		}){

			@Override
			protected Map<String,String> getParams(){
				Map<String,String> params = new HashMap<String, String>();
				//id="+id+"&check="+check+"&email="+Constants.User_Email;
				params.put("id", postid+"");
				params.put("amount", amount+"");
				params.put("email",Constants.User_Email);
				params.put("playerid",playerpreferences.getplayerid());

				return params;
			}
		};

		int socketTimeout =30000;//30 seconds - change to what you want
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		obj1.setRetryPolicy(policy);

		que.add(obj1);

	}


}
