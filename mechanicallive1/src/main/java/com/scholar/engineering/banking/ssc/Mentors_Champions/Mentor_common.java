package com.scholar.engineering.banking.ssc.Mentors_Champions;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.adapter.Home_RecyclerViewAdapter2;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.newScreens.StudentProfile;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.DatabaseHandler;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.isNull;
import static com.scholar.engineering.banking.ssc.utils.Utility.getDate;

public class Mentor_common extends AppCompatActivity implements OnClickListener
{
	DatabaseHandler dbh;
	int index=0,followcount=0;
	String st_email;
	ArrayList<Home_getset> homedata;
	RecyclerView homelist;
	TextView tv_editprofile,tv_adddocument,tv_name,tv_collage,tv_state,tv_comment,tv_reward,tv_phone,tv_postby;
	TextView tv_followers,tv_follow;
	Home_RecyclerViewAdapter2 adapter;
	String savedcont_id="";
	SharedPreferences pref;
	CircleImageView iv_user;
	ProgressDialog progress;
	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
	private DisplayImageOptions options;
	LinearLayout lay_phone;
	RelativeLayout footerlayout;
	TextView loadmore;
	ProgressWheel wheelbar;
	NetworkConnection nw;
	LinearLayout writeReview;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.mentor_profile);

		nw=new NetworkConnection(this);

		Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("Profile");
		Utility.applyFontForToolbarTitle(toolbar,this);
		TextView headertext=(TextView)findViewById(R.id.headertextid);
		headertext.setVisibility(View.GONE);

		dbh=new DatabaseHandler(Mentor_common.this);
		homelist=(RecyclerView)findViewById(R.id.recyclerView);
		LinearLayoutManager mLayoutManager = new LinearLayoutManager(Mentor_common.this);
		homelist.setLayoutManager(mLayoutManager);
		homelist.setHasFixedSize(true);
		homelist.setNestedScrollingEnabled(false);

		pref=getSharedPreferences("myref", Context.MODE_PRIVATE);

		st_email=getIntent().getStringExtra("email");

		tv_editprofile=(TextView)findViewById(R.id.tv_editprofle);
		tv_adddocument=(TextView)findViewById(R.id.tv_adddocuments);
		tv_name=(TextView)findViewById(R.id.tv_username);
		tv_collage=(TextView)findViewById(R.id.tv_collage);
		tv_phone=(TextView)findViewById(R.id.tv_phone);
		tv_state=(TextView)findViewById(R.id.tv_state);
		tv_comment=(TextView)findViewById(R.id.tv_comment);
		tv_reward=(TextView)findViewById(R.id.tv_reward);
		tv_postby=(TextView)findViewById(R.id.tv_postby);
		tv_followers=(TextView)findViewById(R.id.tv_follwers);
		tv_follow=(TextView)findViewById(R.id.tv_follower);

		lay_phone=(LinearLayout)findViewById(R.id.lay_phone);

		iv_user=(CircleImageView)findViewById(R.id.iv_user);

		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.user_default)
				.showImageForEmptyUri(R.drawable.user_default)
				.showImageOnFail(R.drawable.user_default)
				.cacheInMemory(false)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.build();


		homedata=new ArrayList<>();

		tv_name.setText("");
		tv_collage.setText("");
		tv_phone.setText("");
		tv_state.setText("");
		tv_comment.setText("");
		tv_reward.setText("");

		if(nw.isConnectingToInternet())
		getData();
		else {
			Toast.makeText(Mentor_common.this, "No Internet Connection", Toast.LENGTH_LONG).show();
		finish();
		}

		footerlayout=(RelativeLayout)findViewById(R.id.footer_layout);
		loadmore=(TextView)findViewById(R.id.moreButton);
		wheelbar=(ProgressWheel)findViewById(R.id.progress_wheel);

		index=0;

		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());

		tv_editprofile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent in = new Intent(Mentor_common.this, StudentProfile.class);
				startActivity(in);
			}
		});

		footerlayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

				Home_getset	home= (Home_getset) homedata.get(homedata.size()-2);

				index= home.getId();

					getData();

				loadmore.setText("");
				wheelbar.setVisibility(View.VISIBLE);

			}
		});

		tv_follow.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Follow();
			}
		});

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Logg("call_onDestroy()","Call onDestroy()");
		deleteCache(this);
	}

	public void deleteCache(Context context) {
		try {
			File dir = context.getCacheDir();
			deleteDir(dir);
		} catch (Exception e) {}
	}

	public boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
			return dir.delete();
		} else if(dir!= null && dir.isFile()) {
			return dir.delete();
		} else {
			return false;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(item.getItemId()==android.R.id.home)
			finish();

		return super.onOptionsItemSelected(item);
	}

	public void setData(JSONObject obj){
		try {

			tv_name.setText(obj.getString("name"));

			String qul="",exp="";

			if(isNull(obj.getString("qualification")))
				qul=obj.getString("qualification");


			if(isNull(obj.getString("experience")))
				exp=obj.getString("experience");

			tv_collage.setText("Exp. "+exp);

			if(isNull(obj.getString("city")))
			tv_state.setText(obj.getString("city"));

//			tv_comment.setText(obj.getInt("total_post_news")+" Posts");
//			tv_reward.setText(obj.getInt("total_onlinetest")+" Online Tests");

			tv_postby.setText("Post by "+obj.getString("name"));

			Log.e("followers",obj.has("followers")+"");
			if(obj.has("followers")) {
				followcount= Integer.parseInt(obj.getString("followers"));

				tv_followers.setText("Followers: " + followcount+"");
			}
			else
			{
				tv_followers.setText("Followers: " + followcount+"");
			}
			if(obj.has("followed")){
				if(obj.getString("followed").equalsIgnoreCase("follow")){
					tv_follow.setText("Following");
				}
				else {
					tv_follow.setText("Follow");
				}
			}

			ImageLoader imageLoader = ImageLoader.getInstance();
			imageLoader.init(ImageLoaderConfiguration.createDefault(Mentor_common.this));

			if(!obj.getString("image").equalsIgnoreCase("")) {
				imageLoader.getInstance().displayImage(Constants.URL+obj.getString("image"), iv_user, options, animateFirstListener);
			}

		} catch (JSONException e) {
			Log.e("exception","",e);
		}
	}

	public void getData() {

		if(!nw.isConnectingToInternet()) {
			internetconnection(1);
			return;
		}

		progress = ProgressDialog.show(Mentor_common.this, null, null, true);
		progress.setContentView(R.layout.progressdialog);
		progress.setCanceledOnTouchOutside(false);
		progress.setCancelable(false);
		progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		progress.show();

		RequestQueue queue = Volley.newRequestQueue(Mentor_common.this);
		String url = Constants.URL_LV +"homedata_mentor";
		Logg("homedata_mentor", url);

		StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
			@Override
			public void onResponse(String s) {

				Logg("response", s);
				try {
					if(s.length()>0)
					{
						JSONObject res=new JSONObject(s);
						if(res.has("data")){

							JSONArray jr = res.getJSONArray("data");

							if (jr.length() < 10)
								footerlayout.setVisibility(View.GONE);
							else {
								footerlayout.setVisibility(View.VISIBLE);
								loadmore.setText("Load Previous..");
								wheelbar.setVisibility(View.GONE);
							}

							for (int i = 0; i < jr.length(); i++) {
								JSONObject ob = jr.getJSONObject(i);

								String time = getDate(ob.getString("timestamp"));

								Home_getset object=new Home_getset();
								object.setId(ob.getInt("id"));
								object.setTimestamp(ob.getString("timestamp"));
								object.setLikecount(ob.getInt("likes"));
								object.setCommentcount(ob.getInt("comment"));
								object.setViewcount(ob.getInt("view"));
								object.setUid(ob.getString("uid"));
								object.setPosttype(ob.getString("posttype"));
								object.setGroupid(ob.getString("groupid"));
								object.setFieldtype(ob.getString("field_type"));
								object.setJsonfield(ob.getString("jsondata"));
								object.setLikestatus(ob.getString("likestatus"));
								object.setPosturl(ob.getString("posturl"));
								object.setPostdescription(ob.getString("post_description"));
								if(ob.has("AppVersion"))
									object.setAppVersion(ob.getString("AppVersion"));
								homedata.add(object);
							}
							adapter = new Home_RecyclerViewAdapter2(Mentor_common.this, homedata,"Mentor_common");
							homelist.setAdapter(adapter);

						}
						else
							footerlayout.setVisibility(View.GONE);


						if(res.has("mentor_data")){
							setData(res.getJSONObject("mentor_data"));
						}


						progress.dismiss();
					}
				}

				catch (Exception e)
				{
					progress.dismiss();
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {
				CommonUtils.toast(Mentor_common.this,CommonUtils.volleyerror(volleyError));
				progress.dismiss();
			}
		}) {
			@Override
			protected Map<String, String> getParams() {

				Map<String, String> params = new HashMap<String, String>();
				params.put("email", Constants.User_Email);
				params.put("index",index+"");
				params.put("mentor_id",st_email);
				Logg("params+menter",params.toString());
				return params;

			}
		};
		request.setShouldCache(false);
		int socketTimeout = 10000;
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		request.setRetryPolicy(policy);
		queue.add(request);
	}

	public void Follow() {
		RequestQueue queue = Volley.newRequestQueue(Mentor_common.this);
		String url= Constants.URL_LV +"follow_mentors";
		Logg("follow_mentors", url);

		StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
			@Override
			public void onResponse(String s) {
				Logg("response", s);

				try {
					if(s.length()>0)
					{
						JSONObject obj=new JSONObject(s);

						if(obj.has("status")){
							String sts=obj.getString("status");
							if(sts.equalsIgnoreCase("success")){
								if(obj.getString("response").equalsIgnoreCase("unfollow")){
									if(followcount>0)
										followcount--;
									tv_follow.setText("Follow");
									tv_followers.setText("Followers " + followcount+"");
								}
								else{
									followcount++;
									tv_follow.setText("Following");
									tv_followers.setText("Followers " + followcount+"");
								}
							}
						}

					}
				}

				catch (Exception e)
				{ Logg("e",e+""); }
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {
				CommonUtils.toast(Mentor_common.this,CommonUtils.volleyerror(volleyError));
			}
		}) {
			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("mentor_id",st_email );
				params.put("user_id", Constants.User_Email);
				Logg("followparams", params+"");

				return params;
			}
		};
		request.setShouldCache(false);
		int socketTimeout = 0;
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		request.setRetryPolicy(policy);
		queue.add(request);
	}


	public void internetconnection(final int i){

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.network_alert);
		dialog.show();

		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
		TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

		tv_tryagain.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.dismiss();

					getData();

//				getData();

			}
		});

		iv_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

	}

	@Override
	public void onClick(View v) {

	}
}
