package com.scholar.engineering.banking.ssc;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.fragment.Group_Admin_Post;
import com.scholar.engineering.banking.ssc.fragment.Group_Admin_Test;
import com.scholar.engineering.banking.ssc.fragment.Group_User_Post;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Group_Profile extends AppCompatActivity {

    Toolbar toolbar;
    String img_url="", name="", email="", college="", phone="", state="";
    RelativeLayout profile_layout;
    public ViewPager viewPager;
    NetworkConnection nw;
    static public String pid,groupname,groupstatus="";
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    ImageLoader imageLoader;
    TextView tv_follower,tv_postcount,tv_groupcity;
    CircleImageView iv_group;
    boolean st_notification=false;
    ImageView iv_notification;

    UserSharedPreferences preferences;

    CollapsingToolbarLayout collapsingToolbarLayout;

    Typeface font_demi,font_medium;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.group_profile1);

        preferences=new UserSharedPreferences(this,"playerid");
        tv_follower=(TextView)findViewById(R.id.tv_follower);
//        tv_groupname=(TextView)findViewById(R.id.tv_groupname);
        tv_groupcity=(TextView)findViewById(R.id.tv_city);
        tv_postcount=(TextView)findViewById(R.id.tv_posts);
        iv_group=(CircleImageView)findViewById(R.id.iv_group);
//        sw_notification=(Switch)findViewById(R.id.switchid);
        iv_notification=(ImageView)findViewById(R.id.iv_notification);

//        lay_switch=(LinearLayout)findViewById(R.id.sw_layout);
        profile_layout=(RelativeLayout)findViewById(R.id.profile_layout);

        collapsingToolbarLayout=(CollapsingToolbarLayout)findViewById(R.id.collapsing_toolbar);

//        lay_post_comment=(LinearLayout)findViewById(R.id.post_comment);
//        lay_post_link=(LinearLayout)findViewById(R.id.post_link);
//        lay_post_question=(LinearLayout)findViewById(R.id.post_question);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.blankimage)
                .showImageForEmptyUri(R.drawable.congrrr)
                .showImageOnFail(R.drawable.congrrr)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

//        sw_notification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//
//                if(!st_notification.equalsIgnoreCase(b+""))
//                volley_notification(b+"");
//
//            }
//        });

        iv_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPauseAlert();
            }
        });

        nw=new NetworkConnection(Group_Profile.this);

//        img_url = share.getString("image", " ");
//        name = share.getString("name", " ");
        email = User_Email;
//        college = share.getString("collage", " ");
//        state = share.getString("state", " ");
//        phone = share.getString("phone", " ");

        Intent in=getIntent();
        groupname=in.getStringExtra("groupname");
        pid=in.getStringExtra("pid");

        //Logg("post_id",pid+" ");

        getGroupDetail();

        initToolbar();
        initViewPagerAndTabs();

    }

    private void initToolbar() {

        font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");
        font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");

        collapsingToolbarLayout.setExpandedTitleTypeface(font_medium);

        tv_groupcity.setTypeface(font_medium);
        tv_follower.setTypeface(font_medium);
    }

    private void initViewPagerAndTabs() {

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        PagerAdapter1 pagerAdapter = new PagerAdapter1(getSupportFragmentManager());
        pagerAdapter.addFragment(new Group_Admin_Post(), "Content");
        pagerAdapter.addFragment(new Group_Admin_Test(), "Test");
        pagerAdapter.addFragment(new Group_User_Post(), "Discussion");
        viewPager.setAdapter(pagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.setOffscreenPageLimit(3);

//        tabLayout.getTabAt(0).setText("Content");
//        tabLayout.getTabAt(1).setText("Test");
//        tabLayout.getTabAt(2).setText("Discussion");

        //tabLayout.getTabAt(3).setIcon(R.drawable.ic_user_with_tie_and_glasses);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logg("call_onDestroy()","Call onDestroy()");
        deleteCache(this);
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {}
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    static class PagerAdapter1 extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter1(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }

    public void internetconnection(){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Group_Profile.this);

        alertDialog.setMessage("Check Your Internet Connection activity?");
        alertDialog.setPositiveButton("Reresh", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                viewPager.setCurrentItem(0);

            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    public void setData(JSONObject object) {

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(Group_Profile.this));

        try {
            groupname=object.getString("topic");
            toolbar=(Toolbar)findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(groupname);
            Utility.applyFontForToolbarTitle(toolbar,this);
//            tv_groupname.setText(groupname);
            tv_follower.setText(object.getString("member")+"");
            tv_postcount.setText(object.getString("postcount")+"");

            groupstatus=object.getString("status");

            st_notification= Boolean.parseBoolean(object.getString("notification"));

//            if(object.getString("follow_status").equalsIgnoreCase("follow")) {

//                ViewGroup.LayoutParams params = profile_layout.getLayoutParams();
//                int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 290, getResources().getDisplayMetrics());
//                params.height = height;
//                profile_layout.setLayoutParams(params);
//                lay_switch.setVisibility(View.VISIBLE);
                  showIcon(object.getString("follow_status"));

//            }

//            if(st_notification.equalsIgnoreCase("true"))
//                sw_notification.setChecked(true);
            setIcon();

            if(!object.getString("city").equalsIgnoreCase("null"))
            tv_groupcity.setText(object.getString("city"));

            if(object.getString("image").equalsIgnoreCase("null")|object.getString("image").equalsIgnoreCase("")){
            }
            else{
                imageLoader.getInstance().displayImage(Constants.URL_Image+"groupimage/"+object.getString("image"), iv_group, options, animateFirstListener);
            }

//            lay_post_question.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent in = new Intent(Group_Profile.this, Post_question.class);
//                    in.putExtra("groupname",groupname);
//                    in.putExtra("id",pid);
//                    startActivity(in);
//                }
//            });
//
//            lay_post_link.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent in = new Intent(Group_Profile.this,Post_link_in_group.class);
//                    in.putExtra("groupname",groupname);
//                    in.putExtra("id",pid);
//                    startActivity(in);
//                }
//            });
//
//            lay_post_comment.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent in = new Intent(getApplicationContext(), Post_comment_ingroup.class);
//                    in.putExtra("groupname",groupname);
//                    in.putExtra("id",pid);
//                    startActivity(in);
//
//
//                }
//            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

//    public void volley()
//    {
//
//        RequestQueue queue= Volley.newRequestQueue(Group_Profile.this);
//        JSONObject obj=new JSONObject();
//
//        String url= Constants.URL+"newapi2_04/get_group_detail.php?email="+email+"&groupid="+pid;
//
//        //Logg("Url",url);
//
//        final JsonObjectRequest json=new JsonObjectRequest(Request.Method.POST, url, obj,new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject res) {
//                // TODO Auto-generated method stub
//
//                   //Logg("response",String.valueOf(res));
//                try {
//
//                    if(res.has("discuss")) {
//                        JSONArray jr = res.getJSONArray("discuss");
//                        JSONObject jsonobj = jr.getJSONObject(0);
//
//                        setData(jsonobj);
//                    }
//
//                }
//                catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                    //Logg("no",e.getMessage());
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError e) {
//                // TODO Auto-generated method stub
//                //Logg("error discuss", "e",e);
//            }
//        });
//
//        int socketTimeout = 50000;//30 seconds - change to what you want
//        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        json.setRetryPolicy(policy);
//        queue.add(json);
//    }

    private void getGroupDetail() {
        // TODO Auto-generated method stub

        //Logg("id", pid+"");
        //Logg("status",status);
        //Logg("email",email);
        //Logg("playerid",share.getString("playerid",""));

        String url=Constants.URL_LV+"getgroupdetails";

        //Logg("url", url);

        RequestQueue que= Volley.newRequestQueue(Group_Profile.this);

        //final String finalCmnt = cmnt;
        StringRequest obj1=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {
                // TODO Auto-generated method stub

                Logg("response",s);

                try {

                    JSONObject res=new JSONObject(s);

                    if(res.has("status")){
                        if(res.getString("status").equalsIgnoreCase("true")){
                            setData(res.getJSONObject("group"));
                        }
                        else{
                            Toast.makeText(Group_Profile.this,"Something went wrong please try after some time",Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                // TODO Auto-generated method stub
                //progress.dismiss();
                //Logg("error",e.toString());
            }
        }){

            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                //id="+id+"&check="+check+"&email="+Constants.User_Email;
                params.put("groupid", pid+"");
                params.put("email",email);

                return params;
            }
        };

        int socketTimeout =30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        obj1.setRetryPolicy(policy);

        que.add(obj1);

    }

    private void volley_notification(final boolean status) {

        // TODO Auto-generated method stub

        //Logg("id", pid+"");
        //Logg("status",status);
        //Logg("email",email);
        //Logg("playerid",share.getString("playerid",""));

        String url=Constants.URL+"newapi2_04/update_groupnotification.php?";

        //Logg("url", url);

        RequestQueue que= Volley.newRequestQueue(Group_Profile.this);

        //final String finalCmnt = cmnt;
        StringRequest obj1=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {
                // TODO Auto-generated method stub

//                st_notification=status;

                //Logg("response",s);
                int size=s.length();
                try {

                    JSONObject res=new JSONObject(s);

                    if(res.has("status")){

                        if(res.getString("status").equalsIgnoreCase("success")){
                            Toast.makeText(Group_Profile.this,"Succss",Toast.LENGTH_LONG).show();
                            st_notification=!st_notification;
                            setIcon();
                        }
                       else if(res.getString("status").equalsIgnoreCase("error")){
                            Toast.makeText(Group_Profile.this,"Error",Toast.LENGTH_LONG).show();
                        }
                       else if(res.getString("status").equalsIgnoreCase("user not exist")){
                            Toast.makeText(Group_Profile.this,"user not exist",Toast.LENGTH_LONG).show();
                        }

                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                // TODO Auto-generated method stub
                //progress.dismiss();
                //Logg("error",e.toString());
            }
        }){

            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                //id="+id+"&check="+check+"&email="+Constants.User_Email;
                params.put("id", pid+"");
                params.put("status",status+"");

                params.put("email",email);
                params.put("playerid",preferences.getplayerid());

                return params;
            }
        };

        int socketTimeout =30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        obj1.setRetryPolicy(policy);

        que.add(obj1);

    }

    Menu menu;

    public void setIcon(){

        if(st_notification)
           iv_notification.setImageResource(R.drawable.notification_off);
        else
            iv_notification.setImageResource(R.drawable.notification_on);

    }

    public void showIcon(String s){

        if(s.equalsIgnoreCase("follow"))
            iv_notification.setVisibility(View.VISIBLE);
        else
            iv_notification.setVisibility(View.GONE);

    }

    private void onPauseAlert() {

        if(isFinishing())
            return;

        final Dialog dialogP=new Dialog(Group_Profile.this,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialogP.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogP.setContentView(R.layout.notification_alert);

        Window window = dialogP.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogP.show();

        TextView Submit=(TextView)dialogP.findViewById(R.id.tv_submit);
        TextView text,text1;
        text=(TextView)dialogP.findViewById(R.id.text);
        text1=(TextView)dialogP.findViewById(R.id.text1);

        if(st_notification){
            text1.setText("Do you want disable notification update for this group");
        }
        else
            text1.setText("Do you want enable notification update for this group");


        text.setTypeface(font_medium);
        text1.setTypeface(font_medium);
        Submit.setTypeface(font_demi);

        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                volley_notification(!st_notification);
                dialogP.dismiss();
            }
        });

//        dialogP.setOnKeyListener(new Dialog.OnKeyListener() {
//
//            @Override
//            public boolean onKey(DialogInterface arg0, int keyCode,
//                                 KeyEvent event) {
//                // TODO Auto-generated method stub
//                if (keyCode == KeyEvent.KEYCODE_BACK) {
//                    dialogP.dismiss();
//                }
//                return true;
//            }
//        });

    }



}
