package com.scholar.engineering.banking.ssc;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;

import com.scholar.engineering.banking.ssc.adapter.Home_RecyclerViewAdapter2;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import static com.scholar.engineering.banking.ssc.Onlineexam.search_result;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class OnlineExam_filter extends AppCompatActivity
{

	RecyclerView list;
	public static Home_RecyclerViewAdapter2 adapter;
	ArrayList<Home_getset> homedata;
	ProgressDialog progress;
	String title,image;
	int index=0;
	NetworkConnection nw;
	Typeface font_demi,font_medium;
	String dt_from,dt_to,testname;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.onlinetest_filter);

		nw=new NetworkConnection(this);

		font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");
		font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");

		Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setTitle("Online Test");
		Utility.applyFontForToolbarTitle(toolbar,this);

		list=(RecyclerView)findViewById(R.id.recyclerView);
		LinearLayoutManager mLayoutManager = new LinearLayoutManager(OnlineExam_filter.this);
		list.setLayoutManager(mLayoutManager);

		homedata=new ArrayList<>();

		dt_from=getIntent().getStringExtra("dt_from");
		dt_to=getIntent().getStringExtra("dt_to");
		testname=getIntent().getStringExtra("testname");

		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());

		setData(search_result);

	}

	public void setData(JSONArray jr){

			try {
				for (int i = 0; i < jr.length(); i++) {

					JSONObject ob = jr.getJSONObject(i);

//					String time = Utility.getDate(ob.getString("timestamp"));

					Home_getset object=new Home_getset();
					object.setId(ob.getInt("id"));
					object.setTimestamp(ob.getString("timestamp"));
					object.setLikecount(ob.getInt("likes"));
					object.setCommentcount(ob.getInt("comment"));
					object.setViewcount(ob.getInt("view"));
					object.setUid(ob.getString("uid"));
					object.setPosttype(ob.getString("posttype"));
					object.setGroupid(ob.getString("groupid"));
					object.setFieldtype(ob.getString("field_type"));
					object.setJsonfield(ob.getString("jsondata"));
					object.setLikestatus(ob.getString("likestatus"));
					object.setPosturl(ob.getString("posturl"));
					object.setPostdescription(ob.getString("post_description"));
					Logg("log",ob.getInt("id")+"");
					homedata.add(object);
				}

					adapter = new Home_RecyclerViewAdapter2(OnlineExam_filter.this, homedata, "onlineexamfilter");
					list.setAdapter(adapter);

		} catch (JSONException e) {

			}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Logg("call_onDestroy()","Call onDestroy()");
		deleteCache(this);
	}

	public void deleteCache(Context context) {
		try {
			File dir = context.getCacheDir();
			deleteDir(dir);
		} catch (Exception e) {}
	}

	public boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
			return dir.delete();
		} else if(dir!= null && dir.isFile()) {
			return dir.delete();
		} else {
			return false;
		}
	}

//	public void getSearchData() {
//
//		if(!nw.isConnectingToInternet()) {
////            swipeRefreshLayout.setRefreshing(false);
//			internetconnection(2);
//			return;
//		}
//
//		Constants.checkrefresh=true;
//
//		progress = ProgressDialog.show(OnlineExam_filter.this, null, null, true);
//		progress.setContentView(R.layout.progressdialog);
//		progress.setCanceledOnTouchOutside(false);
//		progress.setCancelable(false);
//		progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//
//		final long time= System.currentTimeMillis();
//
////      st_com=st_com.replaceAll(" ","%20");
//
//		//Logg("base64",st_base46);
//		//Logg("id",groupid+"");
//		//Logg("comment",st_com+"");
//		//Logg("email",mail+"");
//		//Logg("imagename",time+"");
//
//		RequestQueue queue = Volley.newRequestQueue(OnlineExam_filter.this);
//		String url="";
//		url = Constants.URL+"v3/search_test.php?";
//		url = url.replace(" ", "%20");
//		Logg("name", url);
//
//		StringRequest request = new StringRequest(Method.POST, url, new Response.Listener<String>() {
//			@Override
//			public void onResponse(String s) {
//
//				progress.dismiss();
//
//				Logg("response", s);
//				try {
//					if(s.length()>0)
//					{
//						JSONObject obj=new JSONObject(s);
//						if(obj.has("data")){
//							JSONArray jr=obj.getJSONArray("data");
//							homedata.clear();
//							for (int i = 0; i < jr.length(); i++) {
//								JSONObject ob = jr.getJSONObject(i);
//
////								String time = Utility.getDate(ob.getString("timestamp"));
//
//								Home_getset object=new Home_getset();
//								object.setId(ob.getInt("id"));
//								object.setTimestamp(ob.getString("timestamp"));
//								object.setLikecount(ob.getInt("likes"));
//								object.setCommentcount(ob.getInt("comment"));
//								object.setViewcount(ob.getInt("view"));
//								object.setUid(ob.getString("uid"));
//								object.setPosttype(ob.getString("posttype"));
//								object.setGroupid(ob.getString("groupid"));
//								object.setFieldtype(ob.getString("field_type"));
//								object.setJsonfield(ob.getString("jsondata"));
//								object.setLikestatus(ob.getString("likestatus"));
//								object.setPosturl(ob.getString("posturl"));
//								object.setPostdescription(ob.getString("post_description"));
//								Logg("log",ob.getInt("id")+"");
//								homedata.add(object);
//							}
//
//
//							adapter = new Test_All_Adapter(OnlineExam_filter.this, homedata, "onlineexam");
//							list.setAdapter(adapter);
//
//						}
//					}
//				}
//
//				catch (JSONException e)
//				{
//					progress.dismiss();
//					e.printStackTrace();
//
//					Logg("e","e",e);
//				}
//			}
//		}, new Response.ErrorListener() {
//			@Override
//			public void onErrorResponse(VolleyError volleyError) {
//
//				//Logg("imagevolley_error",volleyError.toString());
//				progress.dismiss();
//			}
//		}) {
//			@Override
//			protected Map<String, String> getParams() {
//
//				Map<String, String> params = new HashMap<String, String>();
//				params.put("date_from",dt_from);
//				params.put("date_to", dt_to);
//				params.put("search",testname);
//				params.put("user_id",Constants.User_Email);
//
//				Logg("search_param",params.toString());
//
//				return params;
//			}
//		};
//
//		int socketTimeout = 0;
//		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//		request.setRetryPolicy(policy);
//		queue.add(request);
//	}
//
//	public void internetconnection(final int i){
//
//		final Dialog dialog = new Dialog(this);
//		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		dialog.setContentView(R.layout.network_alert);
//		dialog.show();
//
//		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//
//		ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
//		TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);
//
//		tv_tryagain.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//
//				dialog.dismiss();
//				getSearchData();
//
//			}
//		});
//
//		iv_cancel.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				dialog.dismiss();
//			}
//		});
//
//	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(item.getItemId()==android.R.id.home)
			finish();

		return super.onOptionsItemSelected(item);
	}

}