package com.scholar.engineering.banking.ssc.Staff;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.databinding.ActivityStaffProfileBinding;
import com.scholar.engineering.banking.ssc.newScreens.StudentProfile;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;
import com.scholar.engineering.banking.ssc.utils.Utility;

public class StaffProfileActivity extends AppCompatActivity implements View.OnClickListener{

    ActivityStaffProfileBinding binding;
    ImageLoader imageLoader;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    NetworkConnection nw;
    Context mcoxt;
    UserSharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_staff_profile);

        mcoxt = StaffProfileActivity.this;
        preferences=UserSharedPreferences.getInstance(this);
        nw = new NetworkConnection(this);
        binding.imgBack.setOnClickListener(this);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        Utility.applyFontForToolbarTitle(toolbar, this);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.user_default)
                .showImageForEmptyUri(R.drawable.user_default)
                .showImageOnFail(R.drawable.user_default)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    @Override
    protected void onResume() {
        super.onResume();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(mcoxt));

        binding.etUsername.setText(preferences.getname());
        binding.etEmail.setText(preferences.getStaffEmail());
        binding.etRole.setText(preferences.getRole());
        if (preferences.getimage().length() > 0) {
            imageLoader.getInstance().displayImage(preferences.getimage(), binding.ivProfile, options, animateFirstListener);
        }

    }

    public void internetconnection(final int i) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back:
                onBackPressed();
                break;
        }
    }
}