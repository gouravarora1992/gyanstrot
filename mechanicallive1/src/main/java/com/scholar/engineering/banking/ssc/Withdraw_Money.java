package com.scholar.engineering.banking.ssc;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.toast;

/**
 * Created by surender on 6/8/2016.
*/

public class Withdraw_Money extends AppCompatActivity {

    TextView tv_point,tv_request,tv_level,tv_withdrawtext,tv_pointtext;

    WebView wv;
    String ref_code;
    int user_level=0,request_level=5;
    String points="00",message="You have not reached the level for withdraw the cashback";

    Typeface font_demi,font_medium;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.withdraw_money);

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Share and Earn");
        Utility.applyFontForToolbarTitle(toolbar,this);
        TextView header=(TextView)findViewById(R.id.headertextid);
        header.setVisibility(View.GONE);

        font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");
        font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");

        tv_level=(TextView)findViewById(R.id.tv_level);
        tv_point=(TextView)findViewById(R.id.tv_point);
        wv=(WebView)findViewById(R.id.webview_shareid);
        tv_request=(TextView)findViewById(R.id.tv_request);
        tv_pointtext=(TextView)findViewById(R.id.tv_pointtext);
        tv_withdrawtext=(TextView)findViewById(R.id.tv_withdrawtext);

        tv_level.setTypeface(font_medium);
        tv_point.setTypeface(font_medium);
        tv_request.setTypeface(font_medium);
        tv_pointtext.setTypeface(font_medium);
        tv_withdrawtext.setTypeface(font_medium);

//        ref_code=share.getString("referralcode"," ");

//        String text = "<html><body style=\"text-align:justify\"> %s </body></Html>";
//        wv.loadData(Constants.Share_desc, "text/html; charset=UTF-8", null);
//        wv.loadDataWithBaseURL(null, Constants.Share_desc, "text/html", "utf-8",null);

        tv_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(user_level<request_level){
                    Alert("Oops!",message,false);
                }
                else {
                    WithdrawAlert();
                }
            }
        });

        volley();
    }

    @Override
    protected void onResume() {
        super.onResume();
        tv_point.setText("₹ "+ Constants.Point);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()== android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    public void setData(JSONObject object){
        try {

            tv_point.setText(object.getString("xp"));
            tv_level.setText("Level "+object.getString("level")+" ("+object.getString("levelname")+")");
            user_level=object.getInt("level");

            if(object.has("data")){

                request_level=object.getJSONObject("data").getInt("request_level");
                message=object.getJSONObject("data").getString("message");
                wv.loadDataWithBaseURL(null,object.getJSONObject("data").getString("description"), "text/html", "utf-8",null);

            }

        } catch (JSONException e) {
        }
    }

    private void volley() {
        String url=Constants.URL_LV+"getChallengePoints";
        Logg("url", url);

        RequestQueue que= Volley.newRequestQueue(Withdraw_Money.this);
        final StringRequest obj1=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {
                Logg("response",s);
                try {

                    JSONObject res=new JSONObject(s);

                    if(res.has("status")) {

                        if(res.getString("status").equalsIgnoreCase("true")) {
                               setData(res);
                        }
                    }

                }
                catch (JSONException e) { }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                toast(Withdraw_Money.this, CommonUtils.volleyerror(e));
            }
        }){

            @Override
            protected Map<String,String> getParams(){

                Map<String,String> params = new HashMap<String, String>();
                params.put("userid",Constants.User_Email);
                params.put("admission_no",Constants.addmissionno);
                Logg("param_mentorlist",params.toString());
                return params;
            }
        };


        int socketTimeout =30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        obj1.setRetryPolicy(policy);
        que.add(obj1);
    }

    private void sendRquest() {
        String url=Constants.URL_LV+"withdrawmoney";
        Logg("url", url);

        final ProgressDialog progress = ProgressDialog.show(Withdraw_Money.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        if(!this.isFinishing())
            progress.show();
        RequestQueue que= Volley.newRequestQueue(Withdraw_Money.this);
        final StringRequest obj1=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {
                Logg("response",s);
                if(progress!=null)
                if(progress.isShowing())
                    progress.dismiss();

                try {

                    JSONObject res=new JSONObject(s);

                    if(res.has("status")) {

                        if(res.getString("status").equalsIgnoreCase("true")) {
                            Alert("Success!",res.getString("message"),true);
                        }
                    }

                }
                catch (JSONException e) {
                    if(progress!=null)
                        if(progress.isShowing())
                            progress.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                toast(Withdraw_Money.this, CommonUtils.volleyerror(e));
                if(progress!=null)
                    if(progress.isShowing())
                        progress.dismiss();
            }
        }){

            @Override
            protected Map<String,String> getParams(){

                Map<String,String> params = new HashMap<String, String>();
                params.put("userid",Constants.User_Email);
                Logg("param_mentorlist",params.toString());
                return params;
            }
        };


        int socketTimeout =30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        obj1.setRetryPolicy(policy);

        que.add(obj1);

    }


    public void WithdrawAlert(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(Withdraw_Money.this);
        alertDialog.setTitle("");
        alertDialog.setMessage("Are you sure you want withdraw your money?");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                    dialog.cancel();
                    sendRquest();
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }


    public void Alert(String tl, String msg, final boolean b){

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(Withdraw_Money.this);
        alertDialog.setTitle(tl);
        alertDialog.setMessage(msg);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                if(b){
                   dialog.cancel();
                   finish();
                }
                else
                    dialog.dismiss();
            }
        });
        alertDialog.show();
    }

}
