package com.scholar.engineering.banking.ssc.getset;

/**
 * Created by surender on 2/22/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Home_getset_rf {


//            this.id = id;
//        this.timestamp=timestamp;
//        this.likecount = likecount;
//        this.commentcount = commentcount;
//        this.viewcount = viewcount;
//        this.uid=uid;
//        this.posttype = posttype;
//        this.groupid = groupid;
//        this.fieldtype = fieldtype;
//        this.jsonfield = jsonfield;
//        this.likestatus=likestatus;
//        this.posturl=posturl;
//        this.postdescription=postdescription;

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("likecount")
    @Expose
    private int likecount;
    @SerializedName("commentcount")
    @Expose
    private int commentcount;
    @SerializedName("viewcount")
    @Expose
    private int viewcount;
    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("posttype")
    @Expose
    private String posttype;
    @SerializedName("groupid")
    @Expose
    private String groupid;
    @SerializedName("field_type")
    @Expose
    private String fieldtype;
    @SerializedName("jsonfield")
    @Expose
    private String jsonfield;
    @SerializedName("likestatus")
    @Expose
    private String likestatus;
    @SerializedName("postdescription")
    @Expose
    private Object postdescription;

    @SerializedName("posturl")
    @Expose
    private Object posturl;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getFieldtype() {
        return fieldtype;
    }

    public void setFieldtype(String fieldtype) {
        this.fieldtype = fieldtype;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public int getLikecount() {
        return likecount;
    }

    public void setLikecount(int likecount) {
        this.likecount = likecount;
    }

    public int getCommentcount() {
        return commentcount;
    }

    public void setCommentcount(int commentcount) {
        this.commentcount = commentcount;
    }

    public int getViewcount() {
        return viewcount;
    }

    public void setViewcount(int viewcount) {
        this.viewcount = viewcount;
    }

    public String getPosttype() {
        return posttype;
    }

    public void setPosttype(String posttype) {
        this.posttype = posttype;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Object getPosturl() {
        return posturl;
    }

    public void setPosturl(Object posturl) {
        this.posturl = posturl;
    }

    public Object getPostdescription() {
        return postdescription;
    }

    public void setPostdescription(Object postdescription) {
        this.postdescription = postdescription;
    }

    public String getJsonfield() {
        return jsonfield;
    }

    public void setJsonfield(String jsonfield) {
        this.jsonfield = jsonfield;
    }

    public String getLikestatus() {
        return likestatus;
    }

    public void setLikestatus(String likestatus) {
        this.likestatus = likestatus;
    }
}