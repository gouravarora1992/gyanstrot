package com.scholar.engineering.banking.ssc.Staff;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import de.hdodenhof.circleimageview.CircleImageView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.aboutus;
import com.scholar.engineering.banking.ssc.contactus;
import com.scholar.engineering.banking.ssc.databinding.ActivityHomeStaffBinding;
import com.scholar.engineering.banking.ssc.newScreens.SplashScreen;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

public class HomeStaff extends AppCompatActivity implements View.OnClickListener{

    ActivityHomeStaffBinding binding;
    TextView txt_AboutUs, txt_contactus, txt_name;
    CircleImageView img_profile;
    LinearLayout AboutUsLayout, ContactUsLayout, lay_add_query, lay_view_query, layout_support_system, layout_apply_leaves, ll_issue;
    DrawerLayout drawer_layout;
    UserSharedPreferences preferences;
    UserSharedPreferences playerpreferences;
    String userName;
    LinearLayout lay_profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_home_staff);
        playerpreferences=new UserSharedPreferences(this,"playerpreferences");

        preferences=UserSharedPreferences.getInstance(this);

        binding.txtTitle.setText("Gyan Strot");
        binding.ivLogout.setOnClickListener(this);
        binding.addItemCardview.setOnClickListener(this);
        binding.applyLeavesCardview.setOnClickListener(this);
        binding.issueSubmissionCardview.setOnClickListener(this);
        binding.profileCardview.setOnClickListener(this);
        binding.attendanceCardview.setOnClickListener(this);
        binding.supportSystemCardview.setOnClickListener(this);
        navigationDrawer();
    }


    public void navigationDrawer() {
        drawer_layout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer_layout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer_layout.setDrawerListener(toggle);

        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer_layout.isDrawerVisible(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                } else {
                    drawer_layout.openDrawer(GravityCompat.START);
                }
            }
        });


        View headerView = binding.navViewStaff.inflateHeaderView(R.layout.staff_header_layout);
        lay_profile = headerView.findViewById(R.id.lay_profile);
        img_profile = (CircleImageView) headerView.findViewById(R.id.profile);
        txt_AboutUs = (TextView) headerView.findViewById(R.id.dr_contactus);
        txt_contactus = (TextView) headerView.findViewById(R.id.dr_about_us);
        txt_name = (TextView) lay_profile.findViewById(R.id.tv_nameid);

        AboutUsLayout = (LinearLayout) headerView.findViewById(R.id.aboutus_layout);
        ContactUsLayout = (LinearLayout) headerView.findViewById(R.id.contactus_layout);
        lay_add_query = (LinearLayout) headerView.findViewById(R.id.lay_add_query);
        lay_view_query = (LinearLayout) headerView.findViewById(R.id.layout_view_query);
        layout_support_system = headerView.findViewById(R.id.layout_support_system);
        layout_apply_leaves = headerView.findViewById(R.id.ll_apply_leave);
        ll_issue = headerView.findViewById(R.id.ll_issue);

        binding.ivMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer_layout.openDrawer(GravityCompat.START);
                userName = preferences.getName();
                Log.e("username", userName);

                txt_name.setText(userName);
            }
        });

        AboutUsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), aboutus.class);
                startActivity(in);
            }
        });

        ContactUsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), contactus.class);
                startActivity(in);
            }
        });

        lay_add_query.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeStaff.this,AddItemQueryActivity.class));
            }
        });

        lay_view_query.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeStaff.this,ViewQueryActivity.class));
            }
        });

        layout_support_system.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeStaff.this,SupportSystemActivity.class));
            }
        });

        layout_apply_leaves.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeStaff.this,ApplyLeavesActivity.class));
            }
        });

        ll_issue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeStaff.this,IssueSubmissonActivity.class));
            }
        });
    }


    private void showDialogExit() {
        final Dialog dailogExit = new Dialog(HomeStaff.this, android.R.style.Theme_Black_NoTitleBar);
        dailogExit.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dailogExit.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dailogExit.setContentView(R.layout.dialog_exit);
        dailogExit.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dailogExit.setCanceledOnTouchOutside(true);

        ImageView img_close = dailogExit.findViewById(R.id.img_close);
        TextView txt_yes = dailogExit.findViewById(R.id.textView134);
        TextView txt_no = dailogExit.findViewById(R.id.textView135);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dailogExit.dismiss();
            }
        });

        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dailogExit.dismiss();
                finishAffinity();
            }
        });

        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dailogExit.dismiss();
            }
        });

        dailogExit.show();
    }


    private void showDialogLogout() {
        final Dialog dailogLogout = new Dialog(HomeStaff.this, android.R.style.Theme_Black_NoTitleBar);
        dailogLogout.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dailogLogout.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dailogLogout.setContentView(R.layout.dialog_logout);
        dailogLogout.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dailogLogout.setCanceledOnTouchOutside(true);

        ImageView img_close = dailogLogout.findViewById(R.id.img_close);
        TextView txt_yes = dailogLogout.findViewById(R.id.textView134);
        TextView txt_no = dailogLogout.findViewById(R.id.textView135);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dailogLogout.dismiss();
            }
        });

        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dailogLogout.dismiss();
                preferences.Clear();
                Intent i = new Intent(HomeStaff.this,SplashScreen.class);
                startActivity(i);
            }
        });

        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dailogLogout.dismiss();
            }
        });

        dailogLogout.show();
    }



    @Override
    public void onBackPressed() {
        showDialogExit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_logout:
                showDialogLogout();
                break;

            case R.id.profile_cardview:
                startActivity(new Intent(HomeStaff.this, StaffProfileActivity.class));
                break;

            case R.id.apply_leaves_cardview:
                startActivity(new Intent(HomeStaff.this,ApplyLeavesActivity.class));
                break;

            case R.id.support_system_cardview:
                startActivity(new Intent(HomeStaff.this,SupportSystemActivity.class));
                break;

            case R.id.attendance_cardview:
           //     startActivity(new Intent(HomeStaff.this,ChatActivity.class));
                startActivity(new Intent(HomeStaff.this, AttendanceActivity.class));
                break;

            case R.id.issue_submission_cardview:
                startActivity(new Intent(HomeStaff.this,IssueSubmissonActivity.class));
                break;

            case R.id.add_item_cardview:
                startActivity(new Intent(HomeStaff.this,AddItemQueryActivity.class));
                break;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        preferences = UserSharedPreferences.getInstance(this);

        userName = preferences.getName();
        Log.e("usernameee", userName);

        txt_name.setText(userName);

    }
}