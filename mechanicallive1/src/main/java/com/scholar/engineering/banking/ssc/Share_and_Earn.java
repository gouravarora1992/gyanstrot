package com.scholar.engineering.banking.ssc;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.Utility;

/**
 * Created by surender on 6/8/2016.
*/

public class Share_and_Earn extends AppCompatActivity {

    TextView tv_point,tv_earn,tv_sharepoint;
    WebView wv;
    String ref_code;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.share_and_earn);

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Share and Earn");
        Utility.applyFontForToolbarTitle(toolbar,this);
        TextView header=(TextView)findViewById(R.id.headertextid);
        header.setVisibility(View.GONE);

        tv_earn=(TextView)findViewById(R.id.tv_earnid);
        tv_point=(TextView)findViewById(R.id.tv_pointid);
        wv=(WebView)findViewById(R.id.webview_shareid);
        tv_sharepoint=(TextView)findViewById(R.id.tv_sharepointid);

        tv_point.setText("₹ "+ Constants.Point);

        tv_sharepoint.setText("Rs."+Constants.Share_point+" per share");

        String text = "<html><body style=\"text-align:justify\"> %s </body></Html>";
        wv.loadData(Constants.Share_desc, "text/html; charset=UTF-8", null);
        wv.loadDataWithBaseURL(null, Constants.Share_desc, "text/html", "utf-8",null);

        tv_earn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_SUBJECT, "");
                share.putExtra(Intent.EXTRA_TEXT,"Hi, check this new Quiz uploaded on Gyan Strot App. Attempt to self assess your understanding on the topic. If you like the quiz, press the like button and you may share it with your friends using the share button. I'm inviting you too to join it using my referral code:\n"+ref_code+"\nUse this code during registration & get Rs. "+Constants.Share_point+" in your wallet!"+"\nDownload here: "+"play.google.com/store/apps/details?id=com.schoolapp.gyanstrot");
                startActivity(Intent.createChooser(share, "Share link!"));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        tv_point.setText("₹ "+ Constants.Point);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()== android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

}
