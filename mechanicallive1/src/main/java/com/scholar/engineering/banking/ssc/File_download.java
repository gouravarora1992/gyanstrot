package com.scholar.engineering.banking.ssc;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.pnikosis.materialishprogress.ProgressWheel;

import com.scholar.engineering.banking.ssc.adapter.Home_RecyclerViewAdapter2;

import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.EndlessRecyclerViewScrollListener;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.toast;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.volleyerror;

public class File_download extends AppCompatActivity {
    TextView emptyview;
    RecyclerView list;
    Home_RecyclerViewAdapter2 adapter;
    ArrayList<Home_getset> homedata;
    ProgressDialog progress;
    SwipeRefreshLayout swipeRefreshLayout;
    int index = 0;
    RequestQueue queue;
    EndlessRecyclerViewScrollListener endless;
    LinearLayoutManager mLayoutManager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.jobs_all);
        queue = Volley.newRequestQueue(File_download.this);
        emptyview = findViewById(R.id.emptyview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Useful Stuff");
        Utility.applyFontForToolbarTitle(toolbar, this);
        list = (RecyclerView) findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(File_download.this);
        list.setLayoutManager(mLayoutManager);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        homedata = new ArrayList<>();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                homedata=new ArrayList<>();
                index = 0;
                getData();

                endless = new EndlessRecyclerViewScrollListener(mLayoutManager) {
                    @Override
                    public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                        if (homedata.size() % 10 == 0) {
                            index++;
                            getData();
                        }
                    }
                };
                list.addOnScrollListener(endless);
            }
        });

        getData();
        endless = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (homedata.size() % 10 == 0) {
                    index++;
                    getData();
                }
            }
        };
        list.addOnScrollListener(endless);
    }

    public void getData() {

        if (index == 0) {
            progress = ProgressDialog.show(File_download.this, null, null, true);
            progress.setContentView(R.layout.progressdialog);
            progress.setCanceledOnTouchOutside(false);
            progress.setCancelable(false);
            progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progress.show();
        }
        JSONObject obj = new JSONObject();
        try {
            obj.put("email", Constants.User_Email);
            obj.put("index", index);
            obj.put("fieldtype", "4");
            obj.put("admission_no", Constants.addmissionno);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url = Constants.URL_LV + "homedata";
        Logg("homedata", url + "," + obj);

        JsonObjectRequest json = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                swipeRefreshLayout.setRefreshing(false);
                if (progress.isShowing())
                    progress.dismiss();
                try {

                    if (res.has("data")) {

                        JSONArray jr = res.getJSONArray("data");
                        if (progress.isShowing())
                            progress.dismiss();
                        if (jr.length() > 0) {
                            list.setVisibility(View.VISIBLE);
                            emptyview.setVisibility(View.GONE);
                            for (int i = 0; i < jr.length(); i++) {
                                JSONObject ob = jr.getJSONObject(i);

//								String time = Utility.getDate(ob.getString("timestamp"));

                                Home_getset object = new Home_getset();
                                object.setId(ob.getInt("id"));
                                object.setTimestamp(ob.getString("timestamp"));
                                object.setLikecount(ob.getInt("likes"));
                                object.setCommentcount(ob.getInt("comment"));
                                object.setViewcount(ob.getInt("view"));
                                object.setUid(ob.getString("uid"));
                                object.setPosttype(ob.getString("posttype"));
                                object.setGroupid(ob.getString("groupid"));
                                object.setFieldtype(ob.getString("field_type"));
                                object.setJsonfield(ob.getString("jsondata"));
                                object.setLikestatus(ob.getString("likestatus"));
                                object.setPosturl(ob.getString("posturl"));
                                object.setPostdescription(ob.getString("post_description"));
                                if (ob.has("AppVersion"))
                                    object.setAppVersion(ob.getString("AppVersion"));

                                homedata.add(object);
                            }
                            adapter = new Home_RecyclerViewAdapter2(File_download.this, homedata, "File_download");
                            list.setAdapter(adapter);
                        } else {
                            list.setVisibility(View.GONE);
                            emptyview.setVisibility(View.VISIBLE);
                        }
                    } else {
                        list.setVisibility(View.GONE);
                        emptyview.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    list.setVisibility(View.GONE);
                    emptyview.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                if (progress.isShowing())
                    progress.dismiss();
                toast(File_download.this, volleyerror(arg0));
                list.setVisibility(View.GONE);
                emptyview.setVisibility(View.VISIBLE);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        json.setShouldCache(false);
        int socketTimeout = 50000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        json.setRetryPolicy(policy);
        queue.add(json);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    public void internetconnection(Context context, final int i) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);

        if (context != null)
            dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                index=0;
                homedata=new ArrayList<>();
                getData();

            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

}
