package com.scholar.engineering.banking.ssc.Staff;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ClassesListModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.SectionListModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ViewAttendanceModel;
import com.scholar.engineering.banking.ssc.Staff.adapter.ViewAttendanceAdapter;
import com.scholar.engineering.banking.ssc.databinding.ActivityViewStudentAttendanceBinding;
import com.scholar.engineering.banking.ssc.getset.RetrofitObjectAPI;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class ViewStudentAttendanceActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    ActivityViewStudentAttendanceBinding binding;
    NetworkConnection nw;
    List<SectionListModel.Datum> sectionArr;
    List<ClassesListModel.Datum> classesArr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_view_student_attendance);

        nw = new NetworkConnection(ViewStudentAttendanceActivity.this);
        binding.imgBack.setOnClickListener(this);
        binding.txtDate.setOnClickListener(this);
        binding.spinnerSelectClass.setOnItemSelectedListener(this);
        binding.spinnerSelectSection.setOnItemSelectedListener(this);
        getClassList();
        getSectionList();

    }


    private void validate(){
        Log.e("class",binding.spinnerSelectClass.getSelectedItem().toString());
        Log.e("section",binding.spinnerSelectSection.getSelectedItem().toString());
        Log.e("date",binding.txtDate.getText().toString());

        if (!(binding.spinnerSelectClass.getSelectedItem() == "Select Class") &&
                !(binding.spinnerSelectSection.getSelectedItem() == "Select Section") &&
                (!(binding.txtDate.getText().toString().isEmpty()))){

            getViewAttendance();
        }
    }


    private int mYear, mMonth, mDay;
    void datePicker(){
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        final DecimalFormat mFormat= new DecimalFormat("00");
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        binding.txtDate.setText(year + "-" +  mFormat.format(Double.valueOf((monthOfYear + 1)))
                                + "-" + mFormat.format(Double.valueOf(dayOfMonth)));

                    }
                }, mYear, mMonth, mDay);
  //      datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                binding.txtDate.setText(year + "-" +  mFormat.format(Double.valueOf((month + 1)))
                        + "-" + mFormat.format(Double.valueOf(dayOfMonth)));
                validate();
            }
        });

        datePickerDialog.show();
    }


    private void setData(ViewAttendanceModel list) {
        ViewAttendanceAdapter mAdapter = new ViewAttendanceAdapter(this,list);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        binding.studentRecyler.setLayoutManager(mLayoutManager);
        binding.studentRecyler.setAdapter(mAdapter);
        binding.studentRecyler.setHasFixedSize(true);
    }


    private void getClassList() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<ClassesListModel> call = service.getStudentClasses();

        Logg("classList", Constants.URL_LV);

        call.enqueue(new Callback<ClassesListModel>() {

            @Override
            public void onResponse(Call<ClassesListModel> call, retrofit2.Response<ClassesListModel> response) {
                if (response.isSuccessful()) {

                    classesArr = response.body().getData();
                    ArrayList<String> items = new ArrayList<>();
                    items.add("Select Class");
                    for(int i= 0;i<classesArr.size();i++){
                        Log.e("dataaa>>",""+classesArr.get(i).getName());
                        items.add(classesArr.get(i).getName());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(ViewStudentAttendanceActivity.this,
                            android.R.layout.simple_spinner_item,items);

                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spinnerSelectClass.setAdapter(adapter);
                    binding.spinnerSelectClass.setOnItemSelectedListener(ViewStudentAttendanceActivity.this);
                } else {
                    Toast.makeText(ViewStudentAttendanceActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ClassesListModel> call, Throwable t) {
                Log.e("fail",t.getMessage());
            }
        });
    }


    private void getSectionList() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<SectionListModel> call = service.getStudentSection();

        Logg("sectionList", Constants.URL_LV);

        call.enqueue(new Callback<SectionListModel>() {

            @Override
            public void onResponse(Call<SectionListModel> call, retrofit2.Response<SectionListModel> response) {
                if (response.isSuccessful()) {

                    sectionArr = response.body().getData();
                    ArrayList<String> items = new ArrayList<>();
                    items.add("Select Section");
                    for(int i= 0;i<sectionArr.size();i++){
                        Log.e("dataaa",""+sectionArr.get(i).getName());
                        items.add(sectionArr.get(i).getName());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(ViewStudentAttendanceActivity.this,
                            android.R.layout.simple_spinner_item,items);

                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spinnerSelectSection.setAdapter(adapter);
                    binding.spinnerSelectSection.setOnItemSelectedListener(ViewStudentAttendanceActivity.this);
                } else {
                    Toast.makeText(ViewStudentAttendanceActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SectionListModel> call, Throwable t) {
                Log.e("fail",t.getMessage());
            }
        });
    }


    private void getViewAttendance() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);

        Call<ViewAttendanceModel> call = service.getViewStudentAttendance(String.valueOf(binding.spinnerSelectClass.getSelectedItem()), String.valueOf(binding.spinnerSelectSection.getSelectedItem()),binding.txtDate.getText().toString());

        Logg("viewAttendance", Constants.URL_LV + "data?classss=" + binding.spinnerSelectClass.getSelectedItem()
                + "&section=" +binding.spinnerSelectSection.getSelectedItem() + "$date" + binding.txtDate.getText().toString());

        call.enqueue(new Callback<ViewAttendanceModel>() {

            @Override
            public void onResponse(Call<ViewAttendanceModel> call, retrofit2.Response<ViewAttendanceModel> response) {
                if (response.isSuccessful()) {
                    Log.e("response",""+response.body().getMessage());
                    setData(response.body());
                } else {
                    Toast.makeText(ViewStudentAttendanceActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ViewAttendanceModel> call, Throwable t) {
                Log.e("fail",t.getMessage());
            }
        });
    }


    private boolean checkValidation(){
        boolean ret=true;
        String strDate = binding.txtDate.getText().toString();

        if (binding.spinnerSelectClass.getSelectedItem().toString().trim() == "Select Class") {
            ret=false;
            Toast.makeText(ViewStudentAttendanceActivity.this, "Please select class", Toast.LENGTH_SHORT).show();
        } else if(binding.spinnerSelectSection.getSelectedItem().toString().trim() == "Select Section") {
            ret=false;
            Toast.makeText(ViewStudentAttendanceActivity.this,"Please select section", Toast.LENGTH_LONG).show();
        } else if(TextUtils.isEmpty(strDate)) {
            ret=false;
            Toast.makeText(ViewStudentAttendanceActivity.this,"Please select date", Toast.LENGTH_LONG).show();
        }

        return ret;
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.txt_date:
                datePicker();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        Log.e("parent", ""+parent.getId());
        validate();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    public void internetconnection() {
        final Dialog dialog = new Dialog(ViewStudentAttendanceActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getClassList();
                getSectionList();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

}