package com.scholar.engineering.banking.ssc.Challenge;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.scholar.engineering.banking.ssc.R;

import org.json.JSONArray;
import org.json.JSONException;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

/**
 * Created by surender on 30/11/18.
 */

public class ChallengeSubject extends DialogFragment {

    RecyclerView recyclerView;
    Challenge_Sub_Adapter adapter;
    ImageView iv_close;
    Dialog dialog=null;
    Typeface font_demi;
    Typeface font_medium;
    TextView tv_title;

    @Override
    public void dismissAllowingStateLoss() {
        super.dismissAllowingStateLoss();
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitAllowingStateLoss();
        } catch (IllegalStateException e) {
        }
    }

    public ChallengeSubject newInstance(String title) {
        ChallengeSubject frag = new ChallengeSubject();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onStart() {
        super.onStart();
        dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    View v;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        v= inflater.inflate(R.layout.challenge_subject, container);
        iv_close=(ImageView)v.findViewById(R.id.iv_close);
        tv_title=(TextView)v.findViewById(R.id.tv_title);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog!=null)
                    dialog.dismiss();
            }
        });

        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        // Fetch arguments from bundle and set title
        String title = getArguments().getString("data", "");
        Logg("data",title+" ");
        getDialog().setTitle(title);

        try {
            adapter=new Challenge_Sub_Adapter(getActivity(),dialog,new JSONArray(title));
            recyclerView.setAdapter(adapter);
        } catch (JSONException e) {

        }

        font_medium = Typeface.createFromAsset(getActivity().getAssets(), "avenirnextmediumCn.ttf");
        font_demi = Typeface.createFromAsset(getActivity().getAssets(), "avenirnextdemibold.ttf");
        tv_title.setTypeface(font_demi);

        return v;
    }


    @Override
    public void onPause() {
        super.onPause();
        if(dialog!=null)
            dialog.dismiss();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.TOP;
        wlmp.dimAmount = 0.0F;
        dialog.getWindow().setAttributes(wlmp);
        dialog.getWindow().requestFeature(Window.FEATURE_SWIPE_TO_DISMISS);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }
}
