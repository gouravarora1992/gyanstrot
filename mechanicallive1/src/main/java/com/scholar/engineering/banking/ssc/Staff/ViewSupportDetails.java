package com.scholar.engineering.banking.ssc.Staff;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.SupportFeedbackModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.SupportSystemModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.SupportSystemResponseModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.SystemDeadlineModel;
import com.scholar.engineering.banking.ssc.Staff.adapter.SupportSystemAdapter;
import com.scholar.engineering.banking.ssc.Staff.adapter.SupportSystemSolvedAdapter;
import com.scholar.engineering.banking.ssc.Staff.adapter.SupportSystemUnsolvedAdapter;
import com.scholar.engineering.banking.ssc.Staff.adapter.SystemResponseAdapter;
import com.scholar.engineering.banking.ssc.getset.RetrofitObjectAPI;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.GetPath;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Calendar;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class ViewSupportDetails extends AppCompatActivity implements AdapterView.OnItemSelectedListener,View.OnClickListener {

    Spinner spinner_deadline, spinner_feedback;
    Button btn_deadline_save, btn_deadline_reset, btn_feedback_save, btn_feedback_reset, btn_chooseFile;
    EditText edt_response;
    TextView editTextDate, txt_choosen_file ,tv_name, tv_section, tv_class, tv_Details, tv_issue, tv_adsnno, tv_feedback;
    ImageView img_back, img_file;
    CardView deadline_cardview, feedback_cardview, listing_cardview;
    RecyclerView listing_recycler;
    LinearLayout ll_feedback;
    FloatingActionButton Fab;
    private static final int PICK_GALLERY = 102;
    NetworkConnection nw;
    String[] feedback = { "Select Status", "Process", "Solved", "Unsolved"};
    String[] deadline = { "Select Status", "Accept"};
    File file;
    String path, name;
    Integer status_id, id, staff_id;
    SupportSystemModel.Assigned assigned;
    SupportSystemModel.Solved solved;
    SupportSystemModel.Unsolved unsolved;
    UserSharedPreferences preferences;
    UserSharedPreferences playerpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_support_details);

        tv_name = findViewById(R.id.txt_name);
        tv_class = findViewById(R.id.txt_Class);
        tv_section = findViewById(R.id.txt_section);
        tv_issue = findViewById(R.id.txt_issue);
        tv_adsnno = findViewById(R.id.txt_admissionno);
        tv_Details = findViewById(R.id.txt_details);
        tv_feedback = findViewById(R.id.txt_feedback);
        ll_feedback = findViewById(R.id.ll_feedback);

        btn_deadline_save = findViewById(R.id.button5);
        btn_deadline_reset = findViewById(R.id.button6);
        btn_feedback_save = findViewById(R.id.btn_save);
        btn_feedback_reset = findViewById(R.id.btn_reset);
        editTextDate = findViewById(R.id.editTextDate);
        edt_response = findViewById(R.id.edt_response);
        deadline_cardview = findViewById(R.id.deadline_cardview);
        feedback_cardview = findViewById(R.id.feedback_cardview);
        listing_recycler = findViewById(R.id.listing_recyler);
        spinner_deadline = findViewById(R.id.textView35);
        spinner_feedback = findViewById(R.id.textView3);
        btn_chooseFile = findViewById(R.id.button4);
        txt_choosen_file = findViewById(R.id.textView46);
        img_back = findViewById(R.id.img_back);
        Fab = findViewById(R.id.Fab);

        nw = new NetworkConnection(this);
        editTextDate.setOnClickListener(this);
        btn_deadline_save.setOnClickListener(this);
        btn_deadline_reset.setOnClickListener(this);
        btn_feedback_save.setOnClickListener(this);
        btn_feedback_reset.setOnClickListener(this);
        btn_chooseFile.setOnClickListener(this);
        img_back.setOnClickListener(this);
        Fab.setOnClickListener(this);

        playerpreferences=new UserSharedPreferences(this,"playerpreferences");
        preferences=UserSharedPreferences.getInstance(this);

        name = preferences.getName();
        staff_id = preferences.getId();

        Log.e("name???",name);
        Log.e("staffId???",""+staff_id);

        Intent i = this.getIntent();
        Bundle bundle = i.getExtras();

        if(getIntent().getStringExtra("fromAssigned")!=null) {
            if(getIntent().getStringExtra("fromAssigned").equalsIgnoreCase(SupportSystemAdapter.class.getSimpleName())) {

                assigned = (SupportSystemModel.Assigned) getIntent().getSerializableExtra("assigned");
                Log.e("assigned",""+assigned.getName());
                tv_name.setText(assigned.getName());
                tv_class.setText(assigned.getStdClass());
                tv_section.setText(assigned.getStdSection());
                tv_issue.setText(assigned.getIssue());
                tv_Details.setText(assigned.getDetails());
                id = assigned.getId();
            }
        }
        else if(getIntent().getStringExtra("fromSolved")!=null) {
            if(getIntent().getStringExtra("fromSolved").equalsIgnoreCase(SupportSystemSolvedAdapter.class.getSimpleName())) {

                solved = (SupportSystemModel.Solved) getIntent().getSerializableExtra("solved");
                deadline_cardview.setVisibility(View.GONE);
                feedback_cardview.setVisibility(View.GONE);
                ll_feedback.setVisibility(View.VISIBLE);
                Log.e("solved",""+solved.getName());
                tv_name.setText(solved.getName());
                tv_class.setText(solved.getStdClass());
                tv_section.setText(solved.getStdSection());
                tv_issue.setText(solved.getIssue());
                tv_Details.setText(solved.getDetails());
                id = solved.getId();
                getSupportSystemResponse();

            }
        }
        else if(getIntent().getStringExtra("fromUnsolved")!=null) {
            if(getIntent().getStringExtra("fromUnsolved").equalsIgnoreCase(SupportSystemUnsolvedAdapter.class.getSimpleName())) {

                unsolved = (SupportSystemModel.Unsolved) getIntent().getSerializableExtra("unsolved");
                deadline_cardview.setVisibility(View.GONE);
                feedback_cardview.setVisibility(View.GONE);
                Fab.setVisibility(View.VISIBLE);
                Log.e("unsolved",""+unsolved.getName());
                tv_name.setText(unsolved.getName());
                tv_class.setText(unsolved.getStdClass());
                tv_section.setText(unsolved.getStdSection());
                tv_issue.setText(unsolved.getIssue());
                tv_Details.setText(unsolved.getDetails());
                id = unsolved.getId();
                getSupportSystemResponse();
            }
        }

        deadlineSpinner();
        feedbackSpinner();

    }

    //permission
    private void requestPermision() {
        ActivityCompat.requestPermissions(ViewSupportDetails.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
    }

    private boolean checkPermission() {

        int READ_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(ViewSupportDetails.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int WRITE_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(ViewSupportDetails.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (WRITE_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else if (READ_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //ImageFromGallery
        if (requestCode == PICK_GALLERY) {
            if (resultCode == RESULT_OK) {
                try {
                    if (data != null) {
                        Uri filePath = data.getData();
                        path= GetPath.getPath(this,filePath);
                        Log.e("path", ""+ path);
                        if (path!=null) {
                            file = new File(path);
                            Log.e("fileName", "" + file.getName());
                            txt_choosen_file.setText(file.getName());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    private void deadlineSpinner(){
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,deadline);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_deadline.setAdapter(aa);
        spinner_deadline.setOnItemSelectedListener(this);
    }


    private void feedbackSpinner(){
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,feedback);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_feedback.setAdapter(aa);
        spinner_feedback.setOnItemSelectedListener(this);
    }


    private int mYear, mMonth, mDay;
    void datePicker(){
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        final DecimalFormat mFormat= new DecimalFormat("00");
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        editTextDate.setText(year + "-" +  mFormat.format(Double.valueOf((monthOfYear + 1)))
                                + "-" + mFormat.format(Double.valueOf(dayOfMonth)));

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(parent.getId() == R.id.textView3) {
            if (spinner_feedback.getSelectedItem().toString().equalsIgnoreCase("Process")) {
                status_id = 5;
            } else if (spinner_feedback.getSelectedItem().toString().equalsIgnoreCase("Solved")) {
                status_id = 2;
            } else if (spinner_feedback.getSelectedItem().toString().equalsIgnoreCase("UnSolved")) {
                status_id = 3;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void getSupportDeadline() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<SystemDeadlineModel> call = service.getSupportDeadline(id, editTextDate.getText().toString());

        Logg("urlapideadline", Constants.URL_LV + "homedata?id=" +
                31 + "&date=" + editTextDate.getText().toString());

        call.enqueue(new Callback<SystemDeadlineModel>() {

            @Override
            public void onResponse(Call<SystemDeadlineModel> call, retrofit2.Response<SystemDeadlineModel> response) {
                if (response.isSuccessful()) {
                    deadline_cardview.setVisibility(View.GONE);
                    Toast.makeText(ViewSupportDetails.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ViewSupportDetails.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SystemDeadlineModel> call, Throwable t) {
                Toast.makeText(ViewSupportDetails.this,"failure",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getSupportFeedback() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }

        MultipartBody.Part requestImage = null;
        if(path!=null){
            file = new File(path);
            Log.e("Register",""+file.getName());
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            requestImage = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        }

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<SupportFeedbackModel> call = service.getSupportFeedback(id, status_id, edt_response.getText().toString(), path);

        Logg("urlapifeedback", Constants.URL_LV + "homedata?id=" +
                31 + "&spinner=" + status_id + "&response" + edt_response.getText().toString() + "&image" + path);


        call.enqueue(new Callback<SupportFeedbackModel>() {

            @Override
            public void onResponse(Call<SupportFeedbackModel> call, retrofit2.Response<SupportFeedbackModel> response) {
                if (response.isSuccessful()) {
                    finish();
                    Toast.makeText(ViewSupportDetails.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ViewSupportDetails.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SupportFeedbackModel> call, Throwable t) {
                Toast.makeText(ViewSupportDetails.this,"failure",Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void internetconnection() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getSupportDeadline();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }


    private boolean checkValidationDeadline(){
        boolean ret=true;
        String strDate = editTextDate.getText().toString();

        if(spinner_deadline.getSelectedItem().toString().trim() == "Select Status") {
            ret=false;
            Toast.makeText(this, "Please select status", Toast.LENGTH_LONG).show();
        }
        else if(TextUtils.isEmpty(strDate)) {
            ret=false;
            Toast.makeText(this,"Please select date", Toast.LENGTH_LONG).show();
        }
        return ret;
    }

    private boolean checkValidationFeedback(){
        boolean ret=true;
        String strResponse = edt_response.getText().toString();

        if(spinner_feedback.getSelectedItem().toString().trim() == "Select Status") {
            ret=false;
            Toast.makeText(this, "Please select status", Toast.LENGTH_LONG).show();
        }
        else if(TextUtils.isEmpty(strResponse)) {
            ret=false;
            Toast.makeText(this,"Please enter response", Toast.LENGTH_LONG).show();
        }
        return ret;
    }

    private void setData(SupportSystemResponseModel systemResponseModel) {
        if (systemResponseModel!=null) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            listing_recycler.setLayoutManager(linearLayoutManager);
            SystemResponseAdapter responseAdapter= new SystemResponseAdapter(this, systemResponseModel);
            listing_recycler.setAdapter(responseAdapter);
        }
    }


    private void getSupportSystemResponse() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<SupportSystemResponseModel> call = service.supportSystemResponse(staff_id, id);

        Logg("urlapisupportsystemresponse", Constants.URL_LV + "data?id=" + id + "data?staffId" +staff_id);

        call.enqueue(new Callback<SupportSystemResponseModel>() {

            @Override
            public void onResponse(Call<SupportSystemResponseModel> call, retrofit2.Response<SupportSystemResponseModel> response) {
                if (response.isSuccessful()) {
                    setData(response.body());
                    Toast.makeText(ViewSupportDetails.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ViewSupportDetails.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SupportSystemResponseModel> call, Throwable t) {
                Log.e("fail",t.getMessage());
            }
        });
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.editTextDate:
                datePicker();
                break;

            case R.id.button5:
                if (checkValidationDeadline()) {
                    getSupportDeadline();
                }
                break;

            case R.id.button6:
                spinner_deadline.setSelection(0);
                editTextDate.setText("");
                break;

            case R.id.btn_save:
                if (checkValidationFeedback()){
                    getSupportFeedback();
                }
                break;

            case R.id.btn_reset:
                spinner_feedback.setSelection(0);
                edt_response.setText("");
                txt_choosen_file.setText("No file chosen");
                break;

            case R.id.button4:
                if (checkPermission()) {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_GALLERY);
                } else {
                    requestPermision();
                }
                break;

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.Fab:
                Intent intent = new Intent(this, SupportFeedbackForm.class);
                intent.putExtra("id", id);
                Log.e("id>>>>",""+id);
                startActivity(intent);
                break;

            default:
                break;
        }
    }
}