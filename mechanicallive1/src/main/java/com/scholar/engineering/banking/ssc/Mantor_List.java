package com.scholar.engineering.banking.ssc;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.scholar.engineering.banking.ssc.Mentors_Champions.Mentor_common;
import com.scholar.engineering.banking.ssc.adapter.Mentor_list_adapter;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.EndlessRecyclerViewScrollListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.toast;

public class Mantor_List extends Fragment{

	RecyclerView rv_honrizontal;
	SwipeRefreshLayout swipeRefreshLayout;
	Mentor_list_adapter adapter;
	View v;
	int index=0;
	ProgressWheel progressWheel;
	JSONArray data;
	Context context;

	@Override
	public void onAttach(Context context) {
		this.context=context;
		super.onAttach(context);
	}

	@Nullable
	@Override
	public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		v=inflater.inflate(R.layout.mentor_list_activity,container,false);

		rv_honrizontal=(RecyclerView)v.findViewById(R.id.recyclerView);
		LinearLayoutManager lay_Manager = new LinearLayoutManager(context);
		rv_honrizontal.setLayoutManager(lay_Manager);
		rv_honrizontal.setHasFixedSize(true);
//		rv_honrizontal.setNestedScrollingEnabled(false);
		swipeRefreshLayout = (SwipeRefreshLayout)v.findViewById(R.id.swipe_refresh_layout);

		progressWheel=(ProgressWheel)v.findViewById(R.id.progress_wheel);

		data=new JSONArray();

		swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				index=0;
				volley();
			}
		});

		EndlessRecyclerViewScrollListener endless = new EndlessRecyclerViewScrollListener(lay_Manager) {
			@Override
			public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
				progressWheel.setVisibility(View.VISIBLE);
				if(data.length()%10==0) {
					index++;
					volley();
				}
			}
		};

		rv_honrizontal.addOnScrollListener(endless);
		volley();
		return v;
	}

	public void deleteCache(Context context) {
		try {
			File dir = context.getCacheDir();
			deleteDir(dir);
		} catch (Exception e) {}
	}

	public boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
			return dir.delete();
		} else if(dir!= null && dir.isFile()) {
			return dir.delete();
		} else {
			return false;
		}
	}

	private void volley() {
		String url=Constants.URL_LV+"mentor_list";
		Logg("mentor_list", url);

		RequestQueue que= Volley.newRequestQueue(context);
		StringRequest obj1=new StringRequest(Method.POST, url, new Response.Listener<String>() {
			@Override
			public void onResponse(String s) {
				Logg("response",s);
				try {
					JSONObject res=new JSONObject(s);
					if(res.has("status")) {

						if(res.getString("status").equalsIgnoreCase("success")) {

								data=res.getJSONArray("data");
								adapter = new Mentor_list_adapter(context, res.getJSONArray("data"));
								rv_honrizontal.setAdapter(adapter);
						}
						else
						{
							toast(getActivity(),res.getString("message"));
						}
					}
					else
					{
						toast(getActivity(),res.getString("message"));
					}

					progressWheel.setVisibility(View.GONE);
					swipeRefreshLayout.setRefreshing(false);

				}
				catch (JSONException e) {
					swipeRefreshLayout.setRefreshing(false);
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError e) {
				swipeRefreshLayout.setRefreshing(false);
				CommonUtils.toast(getActivity(),CommonUtils.volleyerror(e));
			}
		}){

			@Override
			protected Map<String,String> getParams(){

				Map<String,String> params = new HashMap<String, String>();
				params.put("user_id",Constants.User_Email);
				params.put("index",index+"");
				Logg("param_mentorlist",params.toString());
				return params;
			}
		};


		obj1.setShouldCache(false);
		int socketTimeout =30000;//30 seconds - change to what you want
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		obj1.setRetryPolicy(policy);

		que.add(obj1);

	}


}
