package com.scholar.engineering.banking.ssc.Staff.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;

import java.util.List;

public class AddItemModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("justification")
    @Expose
    private String justification;
    @SerializedName("department")
    @Expose
    private String department;
    @SerializedName("department_head")
    @Expose
    private String department_head;
    @SerializedName("item")
    @Expose
    private String item;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getJustification() {
        return justification;
    }

    public void setJustification(String justification) {
        this.justification = justification;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDepartment_head() {
        return department_head;
    }

    public void setDepartment_head(String department_head) {
        this.department_head = department_head;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class item {

        @SerializedName("item_name")
        @Expose
        private String itemName;
        @SerializedName("quantity")
        @Expose
        private String quantity;
        @SerializedName("cost")
        @Expose
        private String cost;
        @SerializedName("purpose")
        @Expose
        private String purpose;
        @SerializedName("need_purchase")
        @Expose
        private String needPurchase;
        @SerializedName("final_cost")
        @Expose
        private String finalCost;
        @SerializedName("existing_stock")
        @Expose
        private String existingStock;
        @SerializedName("last_purchase")
        @Expose
        private String lastPurchase;
        @SerializedName("image")
        @Expose
        private String image;

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getCost() {
            return cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }

        public String getPurpose() {
            return purpose;
        }

        public void setPurpose(String purpose) {
            this.purpose = purpose;
        }

        public String getNeedPurchase() {
            return needPurchase;
        }

        public void setNeedPurchase(String needPurchase) {
            this.needPurchase = needPurchase;
        }

        public String getFinalCost() {
            return finalCost;
        }

        public void setFinalCost(String finalCost) {
            this.finalCost = finalCost;
        }

        public String getExistingStock() {
            return existingStock;
        }

        public void setExistingStock(String existingStock) {
            this.existingStock = existingStock;
        }

        public String getLastPurchase() {
            return lastPurchase;
        }

        public void setLastPurchase(String lastPurchase) {
            this.lastPurchase = lastPurchase;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

    }
}
