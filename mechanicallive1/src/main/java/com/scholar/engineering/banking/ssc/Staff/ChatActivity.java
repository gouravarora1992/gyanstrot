package com.scholar.engineering.banking.ssc.Staff;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ChatModelClass;
import com.scholar.engineering.banking.ssc.Staff.adapter.ChatAdapter;
import com.scholar.engineering.banking.ssc.databinding.ActivityChatBinding;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityChatBinding binding;
    UserSharedPreferences preferences;
    UserSharedPreferences playerpreferences;
    ChatAdapter chatAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_chat);

        playerpreferences=new UserSharedPreferences(this,"playerpreferences");
        preferences=UserSharedPreferences.getInstance(this);

        binding.imgBack.setOnClickListener(this);
        binding.btnFab.setOnClickListener(this);

        setData();
    }

    private void setData(){
        ChatModelClass[] myListData = new ChatModelClass[] {

                new ChatModelClass("Akash Sharma", R.drawable.img),
                new ChatModelClass("Jay Cutler", R.drawable.img),
                new ChatModelClass("Meghali Singhal", R.drawable.img),
                new ChatModelClass("Akshay Kumar", R.drawable.img),
                new ChatModelClass("Kabir Singh", R.drawable.img),
                new ChatModelClass("Tiger Shroff", R.drawable.img),
                new ChatModelClass("Akash Sharma", R.drawable.img),
                new ChatModelClass("Meghali Singhal", R.drawable.img),
                new ChatModelClass("Akshay Kumar", R.drawable.img),
                new ChatModelClass("Jay Cutler", R.drawable.img),
        };

        chatAdapter = new ChatAdapter(this,myListData);
        binding.rvMessages.setHasFixedSize(true);
        binding.rvMessages.setLayoutManager(new LinearLayoutManager(this));
        binding.rvMessages.setAdapter(chatAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.btn_fab:
                Intent i = new Intent(ChatActivity.this,SearchUserActivity.class);
                startActivity(i);
                break;
        }
    }
}