package com.scholar.engineering.banking.ssc.newScreens;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.scholar.engineering.banking.ssc.R;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class pdfviewclass extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdfview);


        WebView webViewid=findViewById(R.id.webViewid);

        webViewid.getSettings().setJavaScriptEnabled(true);
        webViewid.getSettings().setBuiltInZoomControls(true);

        webViewid.setWebViewClient(new Callback());

        Logg("website_url",getIntent().getStringExtra("postid"));
        webViewid.loadUrl("http://docs.google.com/gview?embedded=true&url="+getIntent().getStringExtra("postid").replaceAll(" ","%20"));
    }
    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return(false);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }
    }
}
