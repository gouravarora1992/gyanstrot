package com.scholar.engineering.banking.ssc.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.AdView;
//import com.google.android.gms.ads.NativeExpressAdView;
//import com.google.android.gms.ads.VideoOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.Comment_Common_homepage;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.DatabaseHandler;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;

/**
 * Created by surender on 3/14/2017.
 */

public class News_related_post_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context c;
    ArrayList<Home_getset> data=new ArrayList<>();
    ArrayList<Home_getset> data1;
    SharedPreferences pref;
    SharedPreferences.Editor prefedit;
    JSONObject jsonobj,jsonfield;
    String email,URL;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    DatabaseHandler dbh;
    int cl,id;
    NetworkConnection nw;

    public News_related_post_adapter(Context c, ArrayList<Home_getset> data) {
        this.c = c;
        this.data = data;
        dbh=new DatabaseHandler(c);
        pref =c.getSharedPreferences("myref", 0);

        email=User_Email;

        nw=new NetworkConnection(c);

        //Logg("user_email",email+" ");


        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.blankimage)
                .showImageForEmptyUri(R.drawable.congrrr)
                .showImageOnFail(R.drawable.congrrr)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

                View v0 = inflater.inflate(R.layout.related_post, viewGroup, false);
                viewHolder = new ViewHolder1(v0);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

                ViewHolder1 vh0 = (ViewHolder1) viewHolder;
                setPost(vh0, position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

//
//    @Override
//    public int getItemViewType(int position) {
//
//        if(data.get(position)==null)
//            return 1;
//        else
//        {
//            return 0;
//        }
//    }

    public class ViewHolder1 extends RecyclerView.ViewHolder {

        //  **************************  Set News/Post  ********************

        TextView title;
        TextView tv_price;
        LinearLayout lay_share;
        TextView likecount;
        LinearLayout lay_like;
        TextView commentcount;
        TextView newscomment;
        TextView viewcount;
        LinearLayout newslayout;
        LinearLayout commentlay;
        ImageView newsimage;
        ImageView likeimage;
        TextView tv_share;
        LinearLayout bottom_layout;

        public ViewHolder1(View v) {
            super(v);

            title = (TextView) v.findViewById(R.id.titleid);
            tv_price = (TextView) v.findViewById(R.id.tv_price);
            lay_share=(LinearLayout)v.findViewById(R.id.lay_shareid);
            likecount = (TextView) v.findViewById(R.id.likeid);
            lay_like = (LinearLayout) v.findViewById(R.id.liketextid);
            commentcount = (TextView) v.findViewById(R.id.commentid);
            newscomment=(TextView)v.findViewById(R.id.newscommentid);
            viewcount = (TextView) v.findViewById(R.id.tv_view);
            newslayout=(LinearLayout)v.findViewById(R.id.newslayoutid);
            commentlay=(LinearLayout)v.findViewById(R.id.lay_comment);
            newsimage=(ImageView)v.findViewById(R.id.imageid);
            likeimage=(ImageView)v.findViewById(R.id.likeimageid);
            tv_share=(TextView)v.findViewById(R.id.homenewsshareid);
            bottom_layout=(LinearLayout)v.findViewById(R.id.bottom_layout);

        }

    }

    private void setPost(ViewHolder1 vh1, final int p) {

        vh1.commentcount.setText(data.get(p).getCommentcount()+"");
        vh1.likecount.setText(data.get(p).getLikecount()+"");
        vh1.viewcount.setText(data.get(p).getViewcount()+" Views");

        //Logg("like_position_related",p+" "+data.get(p).getLikestatus());

        if(data.get(p).getLikestatus().equals("like"))
            vh1.likeimage.setImageResource(R.drawable.ic_bulb_filled);
        else
            vh1.likeimage.setImageResource(R.drawable.ic_bulb_lightup);


        try {


            Typeface font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");
            vh1.title.setTypeface(font_demi);
            Typeface font_meduim = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");
            vh1.viewcount.setTypeface(font_meduim);
            vh1.tv_price.setTypeface(font_meduim);

            jsonobj=new JSONObject(data.get(p).getJsonfield());

            vh1.title.setText(jsonobj.getString("title"));


            if(jsonobj.has("add_comment")){
                if(jsonobj.getString("add_comment").equalsIgnoreCase("off")){
                    vh1.commentlay.setVisibility(View.GONE);

                    vh1.lay_share.setGravity(Gravity.CENTER);
                    vh1.lay_like.setGravity(Gravity.CENTER);

                }
            }

            if(jsonobj.getBoolean("user_payment")){
                vh1.tv_price.setVisibility(View.GONE);
            }
            else {
                if (jsonobj.getString("payment").equalsIgnoreCase("free")) {
                    /*vh1.tv_price.setText(jsonobj.getString("payment"));*/
                    StringBuilder sb = new StringBuilder(jsonobj.getString("payment"));
                    sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
//                    vh1.tv_price.setText(sb.toString() + "");
                    vh1.tv_price.setText("FREE");
                }
                else {
                    vh1.tv_price.setText("Rs. " + jsonobj.getString("payment"));
                }
            }

            if(jsonobj.getString("pic").length()>1){
                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.init(ImageLoaderConfiguration.createDefault(c));
                imageLoader.getInstance().displayImage(Constants.URL_Image + "newsimage/" + jsonobj.getString("pic"), vh1.newsimage, options, animateFirstListener);
            }

        } catch (JSONException e) {
        }

        vh1.lay_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!nw.isConnectingToInternet()) {
                    Toast.makeText(c,"No Internet Connection",Toast.LENGTH_LONG).show();
                    return;
                }
                volleylike_post(p);
            }
        });

        vh1.newslayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    jsonobj = new JSONObject(data.get(p).getJsonfield());
                    Utility.data=data.get(p);
                    if(Utility.checkPayment(c,jsonobj)) {

//                        Utility.data=data.get(p);
                        Utility.openPost(c);
                        Constants.Check_adapter="News_related_post_adapter";
                    }
                    else{
//                        Utility.data=data.get(p);
                        Utility.payPayment_Post(c);
                        Constants.Check_adapter="News_related_post_adapter";
                        // //Logg("payment_response",b+"  ");
                    }

                } catch (JSONException e) {
                }
            }
        });


        vh1.commentlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(c, Comment_Common_homepage.class);
                in.putExtra("posttype","1");
                in.putExtra("postid",data.get(p).getId());
                c.startActivity(in);

                //Logg("click_click","click");
            }
        });

        vh1.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    jsonobj = new JSONObject(data.get(p).getJsonfield());
                    Utility.data=data.get(p);
                    if(Utility.checkPayment(c,jsonobj)) {
//                        Utility.data=data.get(p);
                        Utility.openPost(c);
                        Constants.Check_adapter="News_related_post_adapter";
                    }
                    else{

//                        Utility.data=data.get(p);
                        Utility.payPayment_Post(c);
                        Constants.Check_adapter="News_related_post_adapter";
                        // //Logg("payment_response",b+"  ");
                    }
                } catch (JSONException e) {
                }
            }
        });

        vh1.lay_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                JSONObject shareobj=new JSONObject();
                String value="";
                String[] valuearr;

                try {
                    jsonobj = new JSONObject(data.get(p).getJsonfield());
                    value= String.valueOf(Html.fromHtml(Html.fromHtml(data.get(p).getPostdescription()).toString()));
                    valuearr=value.split(System.lineSeparator(), 2);
                    value=valuearr[0];

                } catch (JSONException e) {

                }

                if (data.get(p).getPosturl().contains("sandeepchaudhary.in")|data.get(p).getPosturl().contains("hellotopper.com")) {

                    try {
                        shareobj.put("class","Webview_internal_link");
                        shareobj.put("title",jsonobj.getString("title"));
                        shareobj.put("description"," ");
                        shareobj.put("image",Constants.URL+"newsimage/"+jsonobj.getString("pic"));
                        shareobj.put("id",data.get(p).getId());
                    } catch (JSONException e) {

                    }
                }
                else if (data.get(p).getPosturl().equalsIgnoreCase("news")) {
                    try {

                        shareobj.put("class","Webview_plus_html");
                        shareobj.put("title",jsonobj.getString("title"));
                        shareobj.put("description"," ");
                        shareobj.put("image",Constants.URL+"newsimage/"+jsonobj.getString("pic"));
                        shareobj.put("id", data.get(p).getId());

                    } catch (JSONException e) {

                    }
                }
                else{

                    try{

                        shareobj.put("class","News_Webview");
                        shareobj.put("title",jsonobj.getString("title"));
                        shareobj.put("description","");
                        shareobj.put("image",Constants.URL+"newsimage/"+jsonobj.getString("pic"));
                        shareobj.put("id",data.get(p).getId());

                    } catch (Exception e) {

                    }
                }

                Utility.shareIntent(c,shareobj);

            }
        });
    }

    public void savecontent(Home_getset data){

        ArrayList<Home_getset> list=dbh.get_savecontent();
        //Logg("list_size",list.size()+" 0");
        if(list.size()<100){
            dbh.save_content(data);

            notifyDataSetChanged();
        }
        else {
            Toast.makeText(c,"Maximum limit 100",Toast.LENGTH_LONG).show();
        }
    }

    public void volleylike_post(final int p)
    {

        RequestQueue queue= Volley.newRequestQueue(c);
        JSONObject json = new JSONObject();
        try {
            json.put("id",data.get(p).getId());
            json.put("email",Constants.User_Email);
            json.put("admission_no",Constants.addmissionno);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Constants.URL_LV + "like_post";
        Logg("likeapi",url+" , "+json.toString());
        JsonObjectRequest jsonreq=new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
// //Logg("res",String.valueOf(res));
                // TODO Auto-generated method stub

                try {

                    if(res.getString("scalar").equals("like")) {
//                        Toast.makeText(c,"success",Toast.LENGTH_SHORT).show();
                        int l=data.get(p).getLikecount();
                        l++;
                        //Logg("like_value",l+"");
                        data.get(p).setLikecount(l);
                        data.get(p).setLikestatus("like");
                        notifyDataSetChanged();
                    }
                    else if(res.getString("scalar").equals("dislike")){

//                        Toast.makeText(c,"success",Toast.LENGTH_SHORT).show();
                        int l=data.get(p).getLikecount();
                        if(l>0)
                            l--;
                        //Logg("like_value",l+"");
                        data.get(p).setLikecount(l);
                        data.get(p).setLikestatus("dislike");
                        notifyDataSetChanged();

                    }
                    else {
                        Toast.makeText(c,"Not done",Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {

                }

            }
        },new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg)
            {
                //Logg("error", String.valueOf(arg));

                Toast.makeText(c,"Network Problem", Toast.LENGTH_SHORT).show();
            }


        });

        queue.add(jsonreq);
    }

}
