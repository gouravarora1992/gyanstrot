package com.scholar.engineering.banking.ssc.newScreens;

import android.app.Application;
import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

public class Youtube extends Application implements YouTubeThumbnailView.OnInitializedListener
{
    Context context;
    ItemVideo itemVideo;
    RelativeLayout relativeLayout;

    public Youtube(Context context, ItemVideo itemVideo, RelativeLayout relativeLayoutOverYouTubeThumbnailView) {
        this.context=context;
        this.itemVideo=itemVideo;
        this.relativeLayout=relativeLayoutOverYouTubeThumbnailView;
    }

    final YouTubeThumbnailLoader.OnThumbnailLoadedListener  onThumbnailLoadedListener = new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {

        @Override
        public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {
            youTubeThumbnailView.setVisibility(View.VISIBLE);
            relativeLayout.setVisibility(View.VISIBLE);
        }

        @Override
        public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {

        }
    };


    @Override
    public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {

        final ItemVideo item = itemVideo;
        youTubeThumbnailLoader.setVideo(item.getVideoId());
        // youTubeThumbnailLoader.setVideo(itemVideos.get(position).getVideoId());
        youTubeThumbnailLoader.setOnThumbnailLoadedListener(onThumbnailLoadedListener);
    }

    @Override
    public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {

    }
}
