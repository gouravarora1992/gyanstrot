package com.scholar.engineering.banking.ssc.newScreens;

/**
 * Created by Swt on 9/16/2017.
 */

public class NotesPdfGetSet {

    String id,name,url,subject;

    public NotesPdfGetSet(String id, String name, String url,String subject){
        this.id = id;
        this.name=name;
        this.url= url;
        this.subject=subject;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
