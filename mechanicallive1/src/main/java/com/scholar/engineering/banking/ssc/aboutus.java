package com.scholar.engineering.banking.ssc;


import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.scholar.engineering.banking.ssc.utils.Utility;

public class aboutus extends AppCompatActivity{
	
//	SharedPreferences pref;
//	Editor edit;
	WebView webView;
	String data="<p>Gyan Strot is blurring the lines between home and school education. It aims to be a powerful examination tool for e-Learning and online education. Useful for schools, colleges, universities, teachers and professors for managing question papers and examinations. Teachers can use it for students regular skills evaluation by conducting online test.</p>\n" +
			"<p>It offers varied features such as</p>\n" +
			"<p>~ Online Tests</p>\n" +
			"<p>~ Sharing posts</p>\n" +
			"<p>~ Challenges for students&nbsp;</p>\n" +
			"<p>~ Peer to peer challenges</p>\n" +
			"<p>~ Career Options/ Latest jobs</p>\n" +
			"<p>~ Educational event updates</p>\n" +
			"<p>~ Offering class wise assignment&nbsp;</p>\n" +
			"<p>~ Facilitating absentee to keep pace&nbsp;</p>\n" +
			"<p>New experiences for people who aspire to reach new horizons.</p>";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.aboutus);
//		pref =getSharedPreferences("myref", 0);

		Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("About Us");
		Utility.applyFontForToolbarTitle(toolbar,this);
		//About Us Text
		TextView header=(TextView)findViewById(R.id.headertextid);
		header.setVisibility(View.GONE);

		webView=(WebView)findViewById(R.id.webview);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.loadDataWithBaseURL(null,data, "text/html", "utf-8",null);

	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()== android.R.id.home)
			finish();
		return super.onOptionsItemSelected(item);
	}
}
