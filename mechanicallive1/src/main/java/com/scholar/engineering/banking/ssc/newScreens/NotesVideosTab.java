package com.scholar.engineering.banking.ssc.newScreens;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.newScreens.adapters.NotesVideoAdapter;

import java.util.ArrayList;

public class NotesVideosTab extends Fragment {
    RecyclerView rvlist;
    ArrayList list=new ArrayList();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_notes_videos_tab, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvlist=view.findViewById(R.id.rvlist);
        GridLayoutManager manager=new GridLayoutManager(getActivity(),2);
        rvlist.setLayoutManager(manager);
        list.add("Media");
        list.add("Assignments");
        list.add("Notes");
        NotesVideoAdapter adapter = new NotesVideoAdapter(getActivity(),list);
        rvlist.setAdapter(adapter);
    }
}
