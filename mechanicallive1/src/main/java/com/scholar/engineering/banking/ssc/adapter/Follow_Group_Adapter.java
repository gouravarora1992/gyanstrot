package com.scholar.engineering.banking.ssc.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.scholar.engineering.banking.ssc.Group_Profile;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Transaction;
import com.scholar.engineering.banking.ssc.getset.gettr_settr;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.DatabaseHandler;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

/**
 * Created by surender on 3/14/2017.
 */

public class Follow_Group_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<gettr_settr> dtopics;

    private Object cm;
    DatabaseHandler dbh;
    UserSharedPreferences playerid;
//    SharedPreferences.Editor prefeditor;
    boolean check_click=true;

    Context cs;
    String groupname,mem,check_clas;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    ImageLoader imageLoader;


    public Follow_Group_Adapter(Context cs, ArrayList<gettr_settr> disc, String check_clas) {
        this.dtopics=disc;
        this.cs=cs;
        this.check_clas=check_clas;
//        pref=cs.getSharedPreferences("groupfollow",Context.MODE_PRIVATE);
        playerid=new UserSharedPreferences(cs,"playerid");
        dbh=new DatabaseHandler(cs);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(cs));

        options = new DisplayImageOptions.Builder()
                .displayer(new RoundedBitmapDisplayer(6))
                .showImageOnLoading(R.drawable.blankimage)
                .showImageForEmptyUri(R.drawable.congrrr)
                .showImageOnFail(R.drawable.congrrr)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

                View v0 = inflater.inflate(R.layout.discussionadapter, viewGroup, false);

                viewHolder = new ViewHolder0(v0);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

                ViewHolder0 vh0 = (ViewHolder0) viewHolder;

                setComment(vh0, position);
    }

    @Override
    public int getItemCount() {
        return dtopics.size();
    }

    private void setComment(final ViewHolder0 vh0, final int an) {

        Typeface font_demi = Typeface.createFromAsset(cs.getAssets(), "avenirnextdemibold.ttf");
        Typeface font_medium = Typeface.createFromAsset(cs.getAssets(), "avenirnextmediumCn.ttf");

        vh0.follow.setTypeface(font_demi);

        vh0.d1.setText(dtopics.get(an).getTopic());
        //final int to=dtopics.get(an).getComment();
        vh0.totalcom.setText(dtopics.get(an).getMember()+" aspirants following");
        vh0.totalcom.setTypeface(font_medium);
        vh0.d1.setTypeface(font_demi);

        if(Integer.valueOf(dtopics.get(an).getPayment())>0)
        {
            vh0.tv_price.setVisibility(View.VISIBLE);
            vh0.tv_price.setText("Premium:  Rs. "+dtopics.get(an).getPayment());
        }
        else
            vh0.tv_price.setVisibility(View.GONE);

        //Logg("group_status",dtopics.get(an).getStartdate());

        if(dtopics.get(an).getImage().equalsIgnoreCase("null")|dtopics.get(an).getImage().equalsIgnoreCase("")){
        }
        else{
            imageLoader.getInstance().displayImage(Constants.URL_Image+"groupimage/"+dtopics.get(an).getImage(), vh0.image, options, animateFirstListener);
        }

        vh0.wheel.setVisibility(View.GONE);
        vh0.follow.setVisibility(View.VISIBLE);

        if(dtopics.get(an).getStartdate().equalsIgnoreCase("follow"))
        {
            vh0.follow.setText("Unfollow");
            vh0.follow.setTextColor(cs.getResources().getColor(R.color.blue_color));
            vh0.follow.setBackground( cs.getResources().getDrawable(R.drawable.background_strok_blue));
        }
        else
        {
            vh0.follow.setText("Follow");
            vh0.follow.setTextColor(cs.getResources().getColor(R.color.white));
            vh0.follow.setBackground( cs.getResources().getDrawable(R.drawable.background_fillblue));
        }

        vh0.follow.setVisibility(View.GONE);

//        vh0.follow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                groupname=dtopics.get(an).getTopic();
//                if(dtopics.get(an).getStartdate().equalsIgnoreCase("unfollow"))
//                {
//                    if(Integer.valueOf(dtopics.get(an).getPayment())>0)
//                    {
//                        if(dtopics.get(an).getPayment_status().equalsIgnoreCase("no")){
//                            // payment
//                            Intent in=new Intent(cs,Transaction.class);
//                            in.putExtra("postid",String.valueOf(dtopics.get(an).getId()));
//                            in.putExtra("field_type","9");
//                            in.putExtra("amount",Integer.valueOf(dtopics.get(an).getPayment()));
//                            cs.startActivity(in);
//                        }
//                        else {
//                            vh0.wheel.setVisibility(View.VISIBLE);
//                            vh0.follow.setVisibility(View.GONE);
//                            volley_follow(dtopics.get(an).getId(), "follow", an);
//                        }
//                    }
//                    else {
//                        vh0.wheel.setVisibility(View.VISIBLE);
//                        vh0.follow.setVisibility(View.GONE);
//                        volley_follow(dtopics.get(an).getId(), "follow", an);
//                    }
//                }
//                else
//                {
//                    vh0.wheel.setVisibility(View.VISIBLE);
//                    vh0.follow.setVisibility(View.GONE);
//                    volley_follow(dtopics.get(an).getId(),"unfollow",an);
//                }
//            }
//        });

        vh0.layout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //Logg("check_class",check_clas);
                //Logg("followstatus",dtopics.get(an).getStartdate());
                //Logg("payment",dtopics.get(an).getPayment());

                if(check_clas.equalsIgnoreCase("mainpg")==true) {

                    if(dtopics.get(an).getStartdate().equalsIgnoreCase("unfollow")) {

                        if (Integer.valueOf(dtopics.get(an).getPayment()) > 0) {

                            if (dtopics.get(an).getPayment_status().equalsIgnoreCase("no")) {

                                Transaction t = new Transaction(cs,"9",String.valueOf(dtopics.get(an).getId()),Integer.valueOf(dtopics.get(an).getPayment()));
                                t.PAYMENT_REQUEST(cs);

//                                Intent in=new Intent(cs,Transaction.class);
//                                in.putExtra("postid",String.valueOf(dtopics.get(an).getId()));
//                                in.putExtra("field_type","9");
//                                in.putExtra("amount",Integer.valueOf(dtopics.get(an).getPayment()));
//                                cs.startActivity(in);

                            }
                            else
                            {
                                //Logg("groupname ", d1.getText().toString() + " postid " + dtopics.get(an).getId() + "");
                                Intent disitem = new Intent(cs, Group_Profile.class);//Group_Comment_Activity
                                disitem.putExtra("groupname", dtopics.get(an).getTopic());
                                disitem.putExtra("pid", String.valueOf(dtopics.get(an).getId()));
                                cs.startActivity(disitem);
                            }
                        }
                        else{
                            //Logg("groupname ", d1.getText().toString() + " postid " + dtopics.get(an).getId() + "");
                            Intent disitem = new Intent(cs, Group_Profile.class);//Group_Comment_Activity
                            disitem.putExtra("groupname", dtopics.get(an).getTopic());
                            disitem.putExtra("pid", String.valueOf(dtopics.get(an).getId()));
                            cs.startActivity(disitem);
                        }
                    }
                    else {

                        //Logg("groupname ", d1.getText().toString() + " postid " + dtopics.get(an).getId() + "");
                        Intent disitem = new Intent(cs, Group_Profile.class);//Group_Comment_Activity
                        disitem.putExtra("groupname", dtopics.get(an).getTopic());
                        disitem.putExtra("pid", String.valueOf(dtopics.get(an).getId()));
                        cs.startActivity(disitem);
                    }

                }

            }
        });

    }

    public class ViewHolder0 extends RecyclerView.ViewHolder {

        //  **************************  Set Group Comment  ********************

        TextView d1;
        TextView totalcom;
        TextView tv_price;
        ImageView image;
        ProgressWheel wheel;
        TextView follow;
        LinearLayout layout;

        public ViewHolder0(View bn) {
            super(bn);

            d1=(TextView)bn.findViewById(R.id.dismainid);
            totalcom=(TextView)bn.findViewById(R.id.discusscountid);
            tv_price=(TextView)bn.findViewById(R.id.tv_price);
            image=(ImageView) bn.findViewById(R.id.imageView1);
            wheel=(ProgressWheel)bn.findViewById(R.id.progress_wheel);
            follow=(TextView)bn.findViewById(R.id.followid);
            layout=(LinearLayout)bn.findViewById(R.id.layoutid);

        }
    }

    private void volley_follow(final int id, final String check, final int position) {
        // TODO Auto-generated method stub

        Logg("follow_status",check+" position "+position+" id "+id);

        String url=Constants.URL+"newapi2_04/update_group_test.php?";

        Logg("url", url);

        RequestQueue que= Volley.newRequestQueue(cs);

        //final String finalCmnt = cmnt;
        StringRequest obj1=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {
                // TODO Auto-generated method stub

                Logg("response",s);
                int size=s.length();
                try {

                    JSONObject res=new JSONObject(s);

                    if(res.getString("scalar").equals("Follow")==true)
                    {
                        Logg("follow status",res.getString("scalar"));
                        mem=dtopics.get(position).getMember();

                        int m=Integer.valueOf(mem);
                        m++;

                        dtopics.get(position).setStartdate("follow");
                        dtopics.get(position).setMember(String.valueOf(m));
                        notifyItemChanged(position);
                        dbh.unfollowgroup(id,"follow");

                    }
                    else if(res.getString("scalar").equals("Unfollow")==true)
                    {
                        Logg("unfollow status",res.getString("scalar"));
                        mem=dtopics.get(position).getMember();

                        int m=Integer.valueOf(mem);

                        m--;

                        dtopics.get(position).setStartdate("unfollow");
                        dtopics.get(position).setMember(String.valueOf(m));

                        notifyItemChanged(position);

                        dbh.unfollowgroup(id,"unfollow");

                    }
                    else if(res.getString("scalar").equals("Note done")==true)
                    {
                        notifyItemChanged(position);
                    }

//                    prefeditor.commit();

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                    notifyItemChanged(position);

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                // TODO Auto-generated method stub
                notifyItemChanged(position);
                //progress.dismiss();
                //Logg("error",e.toString());
            }
        }){

            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                //id="+id+"&check="+check+"&email="+Constants.User_Email;
                params.put("id", id+"");
                params.put("check", check);
                params.put("email",Constants.User_Email);
                params.put("playerid",playerid.getplayerid());

                return params;
            }
        };

        int socketTimeout =30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        obj1.setRetryPolicy(policy);

        que.add(obj1);

    }


}
