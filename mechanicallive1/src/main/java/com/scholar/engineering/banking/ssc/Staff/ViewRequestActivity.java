package com.scholar.engineering.banking.ssc.Staff;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.scholar.engineering.banking.ssc.HomeActivity;
import com.scholar.engineering.banking.ssc.PickUpRequestActivity;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.PickUpRequestModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.RequestLeaveModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.SupportSystemModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ViewRequestModel;
import com.scholar.engineering.banking.ssc.Staff.adapter.LeaveRequestAdapter;
import com.scholar.engineering.banking.ssc.Staff.adapter.SupportSystemAdapter;
import com.scholar.engineering.banking.ssc.Staff.adapter.ViewRequestAdapter;
import com.scholar.engineering.banking.ssc.databinding.ActivityViewRequestBinding;
import com.scholar.engineering.banking.ssc.getset.RetrofitObjectAPI;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import java.io.File;
import java.util.List;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class ViewRequestActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityViewRequestBinding binding;
    ViewRequestAdapter viewRequestAdapter;
    NetworkConnection nw;
    String admission_no;
    Integer user_id;
    UserSharedPreferences preferences;
    UserSharedPreferences playerpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_view_request);

        nw = new NetworkConnection(ViewRequestActivity.this);
        binding.imgBack.setOnClickListener(this);

        playerpreferences=new UserSharedPreferences(ViewRequestActivity.this,"playerpreferences");
        preferences=UserSharedPreferences.getInstance(ViewRequestActivity.this);
        user_id = preferences.getId();
        Log.e("id===>>",""+user_id);

        if(getIntent().getStringExtra("calledFrom")!=null) {
            if(getIntent().getStringExtra("calledFrom").equalsIgnoreCase(PickUpRequestActivity.class.getSimpleName())) {
                admission_no =getIntent().getStringExtra("admission_no");
                viewRequest();
            }
        } else if(getIntent().getStringExtra("calledFromStaff")!=null) {
                viewRequestRaised();
        }

    }

    private void setData(List<ViewRequestModel.Datum> viewModel){
        viewRequestAdapter = new ViewRequestAdapter(this,viewModel);
        binding.viewRequestRecyler.setHasFixedSize(true);
        binding.viewRequestRecyler.setLayoutManager(new LinearLayoutManager(this));
        binding.viewRequestRecyler.setAdapter(viewRequestAdapter);
    }


    private void viewRequest() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<ViewRequestModel> call = service.getViewRequest(Integer.valueOf(admission_no));

        Logg("urlapiviewRequest", Constants.URL_LV + "data?id=" + admission_no);

        call.enqueue(new Callback<ViewRequestModel>() {

            @Override
            public void onResponse(Call<ViewRequestModel> call, retrofit2.Response<ViewRequestModel> response) {
                if (response.isSuccessful()) {
                    setData(response.body().getData());
                    Log.e("response",""+response.body().getMessage());
                    Toast.makeText(ViewRequestActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ViewRequestActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ViewRequestModel> call, Throwable t) {
                Log.e("fail",t.getMessage());
            }
        });
    }


    private void viewRequestRaised() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<ViewRequestModel> call = service.getStaffViewRequest(user_id);

        Logg("urlapiviewRequestRaised", Constants.URL_LV + "data?id=" + user_id);

        call.enqueue(new Callback<ViewRequestModel>() {

            @Override
            public void onResponse(Call<ViewRequestModel> call, retrofit2.Response<ViewRequestModel> response) {
                if (response.isSuccessful()) {
                    setData(response.body().getData());
                    Log.e("response>>",""+response.body().getMessage());
                    Toast.makeText(ViewRequestActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ViewRequestActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ViewRequestModel> call, Throwable t) {
                Log.e("fail",t.getMessage());
            }
        });
    }


    public void internetconnection() {
        final Dialog dialog = new Dialog(ViewRequestActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                viewRequest();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back:
                if(getIntent().getStringExtra("calledFromStaff")!=null) {
                    startActivity(new Intent(this,HomeStaff.class));
                } else if(getIntent().getStringExtra("calledFrom")!=null) {
                    if(getIntent().getStringExtra("calledFrom").equalsIgnoreCase(PickUpRequestActivity.class.getSimpleName())) {
                        startActivity(new Intent(this, HomeActivity.class));
                    }
                }
                break;
        }
    }
}