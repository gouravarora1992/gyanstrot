package com.scholar.engineering.banking.ssc;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.scholar.engineering.banking.ssc.adapter.Home_RecyclerViewAdapter2;
//import com.scholar.engineering.banking.ssc.adapter.News_All_Adapter;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.EndlessRecyclerViewScrollListener;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.toast;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.volleyerror;

public class Workshoplist extends AppCompatActivity {
	TextView emptyview;
    RecyclerView list;
    Home_RecyclerViewAdapter2 adapter;
    ArrayList<Home_getset> homedata;
    ProgressDialog progress;
    SwipeRefreshLayout swipeRefreshLayout;
    int index = 0;
    NetworkConnection nw;
    RequestQueue queue ;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.jobs_all);
        queue = Volley.newRequestQueue(Workshoplist.this);
        nw = new NetworkConnection(this);
        emptyview=findViewById(R.id.emptyview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Workshops");
        Utility.applyFontForToolbarTitle(toolbar, this);
        list = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(Workshoplist.this);
        list.setLayoutManager(mLayoutManager);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        homedata = new ArrayList<>();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                index = 0;
                getData();
            }
        });

        getData();

        EndlessRecyclerViewScrollListener endless = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (homedata.size()%10==0) {
                	index++;
					getData();
				}
            }
        };
        list.addOnScrollListener(endless);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logg("call_onDestroy()", "Call onDestroy()");
        deleteCache(this);
    }
    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }



    public void getData() {

        if (!nw.isConnectingToInternet()) {
            internetconnection(0);
            return;
        }

        if (index == 0) {
            progress = ProgressDialog.show(Workshoplist.this, null, null, true);
            progress.setContentView(R.layout.progressdialog);
            progress.setCanceledOnTouchOutside(false);
            progress.setCancelable(false);
            progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progress.show();
        }

        JSONObject obj = new JSONObject();
        try {
            obj.put("email", Constants.User_Email);
            obj.put("index", index);
            obj.put("fieldtype", "5");
            obj.put("admission_no", Constants.addmissionno);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url = Constants.URL_LV + "homedata";
        Logg("homedata", url + "," + obj);

        JsonObjectRequest json = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                if (progress.isShowing())
                    progress.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                Logg("data workshop",res+"");
                try {
                    if (res.has("data")) {
                        JSONArray jr = res.getJSONArray("data");
						if(jr.length()>0) {
							list.setVisibility(View.VISIBLE);
							emptyview.setVisibility(View.GONE);
							for (int i = 0; i < jr.length(); i++) {
								JSONObject ob = jr.getJSONObject(i);
								Home_getset object = new Home_getset();
								object.setId(ob.getInt("id"));
								object.setTimestamp(ob.getString("timestamp"));
								object.setLikecount(ob.getInt("likes"));
								object.setCommentcount(ob.getInt("comment"));
								object.setViewcount(ob.getInt("view"));
								object.setUid(ob.getString("uid"));
								object.setPosttype(ob.getString("posttype"));
								object.setGroupid(ob.getString("groupid"));
								object.setFieldtype(ob.getString("field_type"));
								object.setJsonfield(ob.getString("jsondata"));
								object.setLikestatus(ob.getString("likestatus"));
								object.setPosturl(ob.getString("posturl"));
								object.setPostdescription(ob.getString("post_description"));
								if (ob.has("AppVersion"))
									object.setAppVersion(ob.getString("AppVersion"));
								homedata.add(object);
							}
							adapter = new Home_RecyclerViewAdapter2(Workshoplist.this, homedata, "Workshoplist");
							list.setAdapter(adapter);
						}
						else {
							list.setVisibility(View.GONE);
							emptyview.setVisibility(View.VISIBLE);
						}
                    } else {
						list.setVisibility(View.GONE);
						emptyview.setVisibility(View.VISIBLE);
                    }
                }
                catch (JSONException e) {
					list.setVisibility(View.GONE);
					emptyview.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError arg0) {
                if (progress.isShowing())
                    progress.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                list.setVisibility(View.GONE);
                emptyview.setVisibility(View.VISIBLE);
				toast(Workshoplist.this,volleyerror(arg0));
            }
        });
		json.setShouldCache(false);
		int socketTimeout = 50000;
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		json.setRetryPolicy(policy);
        queue.add(json);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }


    public void internetconnection(final int i) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
				homedata = new ArrayList<>();
				index=0;
                getData();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

}
