package com.scholar.engineering.banking.ssc.Staff;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ViewQueryModel;
import com.scholar.engineering.banking.ssc.Staff.adapter.ViewQueryAdapter;
import com.scholar.engineering.banking.ssc.databinding.ActivityViewQueryBinding;
import com.scholar.engineering.banking.ssc.getset.RetrofitObjectAPI;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;

import java.util.List;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class ViewQueryActivity extends AppCompatActivity implements View.OnClickListener{

    ActivityViewQueryBinding binding;
    NetworkConnection nw;
    List<ViewQueryModel.Datum> datumList;
    ViewQueryAdapter viewQueryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_view_query);

        nw = new NetworkConnection(ViewQueryActivity.this);
        binding.imgBack.setOnClickListener(this);
        getItem();
    }

    private void setUpData(ViewQueryModel datumList) {
        if (datumList != null) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ViewQueryActivity.this);
            binding.recyclerView.setLayoutManager(linearLayoutManager);
            viewQueryAdapter = new ViewQueryAdapter(ViewQueryActivity.this, datumList);
            binding.recyclerView.setAdapter(viewQueryAdapter);
        }
    }

    private void getItem() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<ViewQueryModel> call = service.viewItemQuery();

        Logg("urlviewqueryapi", Constants.URL_LV);

        call.enqueue(new Callback<ViewQueryModel>() {

            @Override
            public void onResponse(Call<ViewQueryModel> call, retrofit2.Response<ViewQueryModel> response) {
                if (response.isSuccessful()) {
                    setUpData(response.body());
                    Toast.makeText(ViewQueryActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ViewQueryActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ViewQueryModel> call, Throwable t) {
                Log.e("failure", t.getMessage());
            }
        });
    }


    public void internetconnection() {

        final Dialog dialog = new Dialog(ViewQueryActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getItem();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back:
                startActivity(new Intent(ViewQueryActivity.this,HomeStaff.class));
                break;
        }
    }
}