package com.scholar.engineering.banking.ssc.Staff.adapter;

import android.content.Context;

import com.scholar.engineering.banking.ssc.Staff.SupportAssignedFragment;
import com.scholar.engineering.banking.ssc.Staff.SupportSolvedFragment;
import com.scholar.engineering.banking.ssc.Staff.SupportUnsolvedFragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;

    public ViewPagerAdapter(FragmentManager fm, Context context, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                SupportAssignedFragment assignedFragment = new SupportAssignedFragment();
                return assignedFragment;

            case 1:
                SupportUnsolvedFragment unsolvedFragment = new SupportUnsolvedFragment();
                return unsolvedFragment;

            case 2:
                SupportSolvedFragment solvedFragment = new SupportSolvedFragment();
                return solvedFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return totalTabs;
    }
}
