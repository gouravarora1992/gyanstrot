package com.scholar.engineering.banking.ssc.Staff.adapter;

import android.content.Context;

import com.scholar.engineering.banking.ssc.Staff.StaffListFragment;
import com.scholar.engineering.banking.ssc.Staff.StudentListFragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class SearchUserAdapter extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;

    public SearchUserAdapter(FragmentManager fm, Context context, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                StudentListFragment studentListFragment = new StudentListFragment();
                return studentListFragment;

            case 1:
                StaffListFragment staffListFragment = new StaffListFragment();
                return staffListFragment;

        }
        return null;
    }

    @Override
    public int getCount() {
        return totalTabs;
    }
}
