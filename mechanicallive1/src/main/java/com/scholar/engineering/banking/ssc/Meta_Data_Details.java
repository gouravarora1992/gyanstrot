package com.scholar.engineering.banking.ssc;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
//import com.github.barteksc.pdfviewer.PDFView;
import com.scholar.engineering.banking.ssc.adapter.Replies_on_group_comment_adap;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.ImageUploading;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

//import com.androidquery.AQuery;

public class Meta_Data_Details extends AppCompatActivity
{
	Context c;
	Intent detail1;
	String title,comment,image,date,username,collag,imgurl,newsurl;
	RecyclerView list;
	TextView tv_commentcount,tv_like,tv_share;
	//AQuery aquery;
	String url,email;
	//ProgressDialog dialog;
	LinearLayout layout;
	WebView wv;
	ProgressBar bar;
	EditText et_comment;
	Replies_on_group_comment_adap adap;
	int postid,commentcount=0,likecount=0,pagecount=0,index=0;
	ImageView postcom,camera_img,iv_like;
	Home_getset data;
	LinearLayout lay_commentpost,lay_like,lay_comment,lay_share;
	String st_imagename,posttype="1",likestatus="";
	Uri selectedImage;
	Bitmap bitmap;
	NetworkConnection nw;
	TextView moreButton;
	RelativeLayout loadmore_lay;
	JSONArray jsonArray;
	JSONObject jsonobj;
	Typeface font_demi,font_medium;
	public ProgressDialog progress;
//	NestedScrollView nestedScrollView;
//	PDFView pdfview;
	private final int REQUEST_CODE_CLICK_IMAGE = 1002, REQUEST_CODE_GALLERY_IMAGE = 1003;
	ImageUploading ab;

	Activity activity=this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.meta_data_details);

		Logg("oncreate","onmetadata");
		nw=new NetworkConnection(this);

		font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");
		font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");
		wv=(WebView)findViewById(R.id.webViewid);

//		layout=(LinearLayout)findViewById(R.id.ly_layout_id);

		Constants.imageFilePath=CommonUtils.getFilename();

		tv_share=(TextView)findViewById(R.id.tv_shareid);
		tv_commentcount=(TextView)findViewById(R.id.tv_commentid);
		tv_like=(TextView)findViewById(R.id.tv_like);
		tv_share.setTypeface(font_medium);
		tv_commentcount.setTypeface(font_medium);
		tv_like.setTypeface(font_medium);
		iv_like=(ImageView)findViewById(R.id.likeimageid);

		lay_comment=(LinearLayout)findViewById(R.id.lay_comment);
		lay_like=(LinearLayout)findViewById(R.id.lay_like);
		lay_share=(LinearLayout)findViewById(R.id.lay_shareid);

		jsonArray=new JSONArray();

		detail1=getIntent();

		postid=detail1.getIntExtra("postid",0);

//		Replies.psid=postid;
//		username=pref.getString("name", "");
//		collag=pref.getString("collage", "");
//		imgurl=pref.getString("image","");
		email=Constants.User_Email;

		//Logg("like_comment",likecount+" "+commentcount);

//		wv.setWebChromeClient(new Callback());
		wv.setWebViewClient(new Callback());
		wv.getSettings().setJavaScriptEnabled(true);
		wv.getSettings().setBuiltInZoomControls(true);
		wv.getSettings().setPluginState(WebSettings.PluginState.ON);

//		wv.setWebChromeClient(new WebChromeClient() {
//            public void onProgressChanged(WebView view, int progress)
//            {
//                activity.setProgress(progress * 100);
//            }
//        });

		get_Post(postid);

		lay_like.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if(!nw.isConnectingToInternet()) {
					Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
					return;
				}
				volleylike_post();
			}
		});

		lay_comment.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

//				Intent in = new Intent(Meta_Data_Details.this, Replies.class);
//				in.putExtra("psid",postid);
//				startActivity(in);
			}
		});

		lay_share.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				JSONObject shareobj=new JSONObject();
				try{
					if(jsonobj!=null) {
						String value="";
						String[] valuearr;

						JSONObject jsondata = new JSONObject(jsonobj.getString("jsondata"));

						value= String.valueOf(Html.fromHtml(String.valueOf(Html.fromHtml(jsonobj.getString("post_description")))));
						valuearr=value.split(System.lineSeparator(), 2);
						value=valuearr[0];

						shareobj.put("class", "Meta_Data_Details");
						if(jsondata.has("comments"))
						shareobj.put("title", jsondata.getString("comments"));
						else
							shareobj.put("title", jsondata.getString("title"));
						shareobj.put("description", value);
						if(jsondata.has("pic"))
						shareobj.put("image", Constants.URL + "newsimage/" + jsondata.getString("pic"));
						else if(jsondata.has("com_image"))
							shareobj.put("image", jsondata.getString("com_image"));

						shareobj.put("id", jsonobj.getInt("id"));

						Utility.shareIntent(Meta_Data_Details.this,shareobj);
					}

				} catch (Exception e) {
				}
			}
		});
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			switch (keyCode) {
				case KeyEvent.KEYCODE_BACK:
					if (wv.canGoBack()) {
						wv.goBack();
					} else {
						finish();
					}
					return true;
			}

		}
		return super.onKeyDown(keyCode, event);
	}

	private class Callback extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(
				WebView view, String url) {
			return(false);
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);

			//Logg("onpage_started",url);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);

			//Logg("onpage_finished",url);


		}
	}


	@Override
	protected void onDestroy() {
		super.onDestroy();
		Logg("call_onDestroy()","Call onDestroy()");
		deleteCache(this);
	}

	public void deleteCache(Context context) {
		try {
			File dir = context.getCacheDir();
			deleteDir(dir);
		} catch (Exception e) {}
	}

	public boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
			return dir.delete();
		} else if(dir!= null && dir.isFile()) {
			return dir.delete();
		} else {
			return false;
		}
	}

	public void setComment(int com){

		tv_commentcount.setText("Comments ("+com+")");

		if(com<10)
		{}
		else if(com<100){
			tv_commentcount.setTextSize(12f);
		}
		else{
			tv_commentcount.setTextSize(11f);
		}
	}

	public void setLike(int lk){
		if(lk>0) {

			tv_like.setText(getResources().getString(R.string.like) + " (" + lk + ")");
		}
		else
		{
			tv_like.setText(getResources().getString(R.string.like));
		}
//		if(lk<10)
//		{}
//		else if(lk<100){
//			tv_like.setTextSize(12f);
//		}
//		else{
//			tv_like.setTextSize(11f);
//		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(item.getItemId()==android.R.id.home)
			finish();

		return super.onOptionsItemSelected(item);
	}

//	@Override
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//		if (event.getAction() == KeyEvent.ACTION_DOWN) {
//			switch (keyCode) {
//				case KeyEvent.KEYCODE_BACK:
//					if (wv.canGoBack()) {
//						wv.goBack();
//					} else {
//						finish();
//					}
//					return true;
//			}
//
//		}
//		return super.onKeyDown(keyCode, event);
//	}

//	private class Callback extends WebViewClient {
//
//		@Override
//		public boolean shouldOverrideUrlLoading(
//				WebView view, String url) {
//			return(false);
//		}
//
//		@Override
//		public void onPageStarted(WebView view, String url, Bitmap favicon) {
//			super.onPageStarted(view, url, favicon);
//
//			//Logg("onpage_started",url);
//		}
//
//		@Override
//		public void onPageFinished(WebView view, String url) {
//			super.onPageFinished(view, url);
//
//			//Logg("onpage_finished",url);
//
//			bar.setVisibility(View.GONE);
//			if(pagecount<1) {
//				pagecount++;
//			volleylist(postid);
//			}
//
//		}
//	}

	@Override
	public void onBackPressed() {

		if(Constants.Check_Branch_IO){
			Intent in =new Intent(Meta_Data_Details.this,HomeActivity.class);
			startActivity(in);
			finish();
		}
		else
		super.onBackPressed();
	}

	public void volleylike_post()
	{

		progress = ProgressDialog.show(this, null, null, true);
		progress.setContentView(R.layout.progressdialog);
		progress.setCanceledOnTouchOutside(false);
		progress.setCancelable(false);
		progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//		progress.show();

		RequestQueue queue= Volley.newRequestQueue(Meta_Data_Details.this);
		JSONObject json = new JSONObject();
		try {
			json.put("id",postid);
			json.put("email",Constants.User_Email);
			json.put("admission_no",Constants.addmissionno);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		String url = Constants.URL_LV + "like_post";
		Logg("likeapi",url+" , "+json.toString());
		JsonObjectRequest jsonreq=new JsonObjectRequest(Method.POST, url, json, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res) {
				// TODO Auto-generated method stub

				try {

					if(res.getString("scalar").equals("like")) {
//						Toast.makeText(News_Webview.this,"success",Toast.LENGTH_SHORT).show();

						likecount++;
						setLike(likecount);

						iv_like.setImageResource(R.drawable.ic_bulb_filled);
						tv_like.setTextColor(getResources().getColor(R.color.like_text_color));

					}
					else if(res.getString("scalar").equals("dislike")){

						iv_like.setImageResource(R.drawable.ic_bulb_lightup);
						tv_like.setTextColor(getResources().getColor(R.color.text_lightgray));

//						Toast.makeText(News_Webview.this,"success",Toast.LENGTH_SHORT).show();

						likecount--;
						setLike(likecount);

//						tv_like.setText(" Light Up ("+likecount+")");

					}
					else {
						Toast.makeText(Meta_Data_Details.this,"Not done",Toast.LENGTH_SHORT).show();
					}

					progress.dismiss();

				} catch (JSONException e) {
					progress.dismiss();
				}

			}
		},new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg)
			{
				//Logg("error", String.valueOf(arg));
				progress.dismiss();
				Toast.makeText(Meta_Data_Details.this,"Network Problem", Toast.LENGTH_SHORT).show();
			}

		});

		queue.add(jsonreq);
	}

	public void get_Post(int postid)
	{

		if(!nw.isConnectingToInternet()) {
			//swipeRefreshLayout.setRefreshing(false);
			internetconnection(0);
			return;
		}

		progress = ProgressDialog.show(this, null, null, true);
		progress.setContentView(R.layout.progressdialog);
		progress.setCanceledOnTouchOutside(false);
//		progress.setCancelable(false);
		progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		progress.show();

		RequestQueue queue=Volley.newRequestQueue(getApplicationContext());
		JSONObject obj=new JSONObject();

		String url=Constants.URL+"newapi2_04/getPost.php?postid="+postid+"&email="+email;

		Logg("url list",url);

		JsonObjectRequest json=new JsonObjectRequest(Method.POST, url, obj,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res) {
				// TODO Auto-generated method stub

				//Logg("respon",String.valueOf(res));

				try {

						JSONArray jr=res.getJSONArray("data");

						jsonobj=jr.getJSONObject(0);

					likecount=jsonobj.getInt("likes");
					commentcount=jsonobj.getInt("comment");
					newsurl=jsonobj.getString("posturl");
					likestatus=jsonobj.getString("likestatus");

					setLike(likecount);
					setComment(commentcount);

					if(likestatus.equals("like")) {
						iv_like.setImageResource(R.drawable.ic_bulb_filled);
						tv_like.setTextColor(getResources().getColor(R.color.like_text_color));
					}
					else
						{
						iv_like.setImageResource(R.drawable.ic_bulb_lightup);
							tv_like.setTextColor(getResources().getColor(R.color.text_lightgray));
					}

					Logg("news_url",newsurl);

					wv.loadUrl(newsurl);

					progress.dismiss();
				}
				catch (JSONException e) {
					progress.dismiss();
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				//Logg("error", String.valueOf(arg0));
				progress.dismiss();

			}
		});

		queue.add(json);

	}

	public void internetconnection(final int i){

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.network_alert);
		dialog.show();

		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
		TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

		tv_tryagain.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.dismiss();

				if(i==0)
					get_Post(postid);

//				getData();

			}
		});

		iv_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

	}

}
