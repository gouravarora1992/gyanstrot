package com.scholar.engineering.banking.ssc.Challenge;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;

import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.userprofile.UserProfile;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by surender on 12/03/2018.
 */

public class LeaderBoard_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context c;
    JSONObject object;
    JSONArray data;
    DisplayImageOptions options;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    ImageLoader imageLoader;

    Calendar cal = Calendar.getInstance();
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String formattedDate = df.format(cal.getTime());

    Typeface font_demi;
    Typeface font_medium;

    public LeaderBoard_Adapter(Context c, JSONArray data) {

        this.data = new JSONArray(new ArrayList<String>());
        this.c = c;
        this.data = data;


        formattedDate = df.format(cal.getTime());
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.user_default)
                .showImageForEmptyUri(R.drawable.user_default)
                .showImageOnFail(R.drawable.user_default)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
        font_medium = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");
        font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");

    }

//    public void addItem(JSONArray array1) {
//        for (int i = 0; i < array1.length(); i++) {
//            try {
//                data.put(array1.getJSONObject(i));
//            } catch (JSONException e) {
//            }
//        }
//    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v0 = null;

        v0 = inflater.inflate(R.layout.leader_board_adapter, viewGroup, false);

        viewHolder = new ViewHolder0(v0);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        ViewHolder0 vh0 = (ViewHolder0) viewHolder;

        setComment(vh0, position);
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    private void setComment(final ViewHolder0 vh0, final int p) {

        try {

            object = data.getJSONObject(p);

            imageLoader = ImageLoader.getInstance();
            imageLoader.init(ImageLoaderConfiguration.createDefault(c));

            vh0.tv_opponentname.setText(object.getString("name"));
            vh0.tv_college.setText(object.getString("class") + "-" + object.getString("section"));
            vh0.tv_level.setText("Level " + object.getString("level"));
            vh0.tv_points.setText(object.getString("point") + "\nPoints");

            if (object.has("image"))
                if (object.getString("image").length() > 4)
                    imageLoader.getInstance().displayImage(object.getString("image"), vh0.imageView, options, animateFirstListener);

            vh0.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        object = data.getJSONObject(p);
                        Intent in = new Intent(c, UserProfile.class);
                        in.putExtra("student_id", object.getString("student_id"));
                        in.putExtra("admission_no", object.getString("admission_no"));
                        c.startActivity(in);

                    } catch (JSONException e) {
                        Log.e("exception","",e);
                    }
                }
            });

        } catch (JSONException e) {
            Log.e("exception","",e);
        }

    }

    public class ViewHolder0 extends RecyclerView.ViewHolder {

        //  **************************  Set Group Comment  ********************

        LinearLayout layout;
        TextView tv_opponentname, tv_college, tv_level, tv_points;
        CircleImageView imageView;

        public ViewHolder0(View b) {
            super(b);

            layout = (LinearLayout) b.findViewById(R.id.layout);
            tv_opponentname = (TextView) b.findViewById(R.id.tv_opponentname);
            tv_college = (TextView) b.findViewById(R.id.tv_college);
            tv_level = (TextView) b.findViewById(R.id.tv_level);
            tv_points = (TextView) b.findViewById(R.id.tv_points);
            imageView = (CircleImageView) b.findViewById(R.id.iv_opponent);

            tv_opponentname.setTypeface(font_demi);
            tv_college.setTypeface(font_medium);
            tv_level.setTypeface(font_medium);

        }
    }

}
