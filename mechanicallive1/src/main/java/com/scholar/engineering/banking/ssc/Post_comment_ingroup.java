package com.scholar.engineering.banking.ssc;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.scholar.engineering.banking.ssc.adapter.Followgroup_adap;
import com.scholar.engineering.banking.ssc.getset.gettr_settr;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.ImageCompression;
import com.scholar.engineering.banking.ssc.utils.ImageUploading;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;
import static com.scholar.engineering.banking.ssc.utils.Constants.imageFilePath;
import static com.scholar.engineering.banking.ssc.utils.Constants.st_base46;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

/**
 * Created by surender on 19-04-2016.
 */

public class Post_comment_ingroup extends AppCompatActivity {



    int groupid;
    String groupname,st_com,username,usercollage,mail,image;
    TextView headertext,postcommenttext,uploadingtext;
    LinearLayout postcom,addimage;
    RelativeLayout addimglay;
    ImageView cameraimage;

    EditText et_com;
    ImageView remove;
    Dialog dialog,dialog1;
    ArrayList<gettr_settr> grouplist;
    ProgressDialog progress;
    String picturePath;
    String uploadFileName = "";
    Bitmap bitmap;
    Uri selectedImage;
    private Bitmap thumbnail;
    Typeface font_demi,font_medium;
    private final int REQUEST_CODE_CLICK_IMAGE = 1002, REQUEST_CODE_GALLERY_IMAGE = 1003;
    TextView text,text1;
//      ArrayList<gettr_settr> grouplist;

    NetworkConnection nw;
    ImageUploading ab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_comment_ingroup);

        nw=new NetworkConnection(this);

        font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");
        font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Write Your Post");
        Utility.applyFontForToolbarTitle(toolbar,this);
        headertext=(TextView)findViewById(R.id.headertextid);

        Constants.imageFilePath=CommonUtils.getFilename();

        headertext.setTypeface(font_demi);
        text=(TextView)findViewById(R.id.text);
        text1=(TextView)findViewById(R.id.text1);
        text.setTypeface(font_demi);
        text1.setTypeface(font_demi);
        postcom=(LinearLayout)findViewById(R.id.postcommentid);
        addimage=(LinearLayout)findViewById(R.id.addimageid);
        postcommenttext=(TextView)findViewById(R.id.postcommenttextid);
        postcommenttext.setTypeface(font_demi);
        et_com=(EditText)findViewById(R.id.commentid);
        et_com.setTypeface(font_medium);
        addimglay=(RelativeLayout)findViewById(R.id.addimagelayoutid);
        remove=(ImageView)findViewById(R.id.imageView12);
        cameraimage=(ImageView)findViewById(R.id.camerimageid);
       // tv_postlink=(TextView)findViewById(R.id.tv_postlink);
        grouplist=new ArrayList<>();
//        pref =getSharedPreferences("myref", 0);
//        username=pref.getString("name", "");
//        image=pref.getString("image","");
//        usercollage=pref.getString("collage","");
        mail=User_Email;

        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
            }
        }

        Intent in=getIntent();

        if(in.getStringExtra("groupname").equalsIgnoreCase("null"))
            volley_getFollowGroup();  //dialoggroup();
        else
        {
            groupname=in.getStringExtra("groupname");
            groupid=Integer.valueOf(in.getStringExtra("id"));
//            headertext.setText(groupname);
        }

        headertext.setVisibility(View.GONE);
        //headertext.setText("Write Your Post");

        dialog=new Dialog(Post_comment_ingroup.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.processing_dialog);
        uploadingtext=(TextView)dialog.findViewById(R.id.uploadingtextid);

        // uploadingtext.setText("Uploading");

        //tv_postlink.setVisibility(View.GONE);

//        tv_postlink.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent in = new Intent(Post_comment_ingroup.this,Post_link_in_group.class);
//                startActivity(in);
//            }
//        });

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addimglay.setVisibility(View.GONE);
            }
        });

        addimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ab.showOptionBottomSheetDialog();

            }
        });

        postcom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                st_com=et_com.getText().toString();
                if(st_com.length()>0){

//                    postcommenttext.setText("Uploading..");
//                    postcom.setBackground(getResources().getDrawable(R.drawable.discussionfollowback));

                    postcom.setClickable(false);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(et_com.getWindowToken(), 0);

                    if(st_base46.length()>0) {

//                        dialog.show();
//                        uploadingtext.setText("Uploading");
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {

                        imagevolley();
//                            }
//                        }, 1000);
                    }
                    else {

                        volleycominsert();
                        postcom.setClickable(false);
                    }
                }
                else
                    Toast.makeText(getApplicationContext(),"Please enter your commnet",Toast.LENGTH_LONG).show();

            }
        });

        View view = getCurrentFocus();

        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        ab=new ImageUploading(this,cameraimage);
        ab.createBottomSheetDialog();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logg("call_onDestroy()","Call onDestroy()");
        deleteCache(this);
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {}
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    public void dialoggroup()
    {
//        for(int i=0;i<Home_fragment.grouplist.size();i++){
//            if(!Home_fragment.grouplist.get(i).getStatus().equalsIgnoreCase("Private"))
//                grouplist.add(Home_fragment.grouplist.get(i));
//        }

        dialog1=new Dialog(this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);

        Rect displayRectangle = new Rect();
        Window window = dialog1.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        // inflate and adjust layout
        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout1 = inflater.inflate(R.layout.followgroup_dialog, null);
        layout1.setMinimumWidth((int) (displayRectangle.width() * 0.9f));
        layout1.setMinimumHeight((int) (displayRectangle.height() * 0.6f));
        dialog1.setContentView(layout1);
        TextView text=(TextView)dialog1.findViewById(R.id.text);
        text.setTypeface(font_demi);
        ListView list=(ListView)dialog1.findViewById(R.id.grouplistid);

        Followgroup_adap adap=new Followgroup_adap(getApplicationContext(), grouplist);

        list.setAdapter(adap);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                groupid = grouplist.get(position).getId();
                groupname = grouplist.get(position).getTopic();
                headertext.setText(groupname);
                getSupportActionBar().setTitle(groupname);
                dialog1.dismiss();
            }
        });

        dialog1.show();
        dialog1.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });
        //dialog1.setCancelable(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_CLICK_IMAGE) {

            new ImageCompression(cameraimage).execute(imageFilePath);

        } else if (requestCode == REQUEST_CODE_GALLERY_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

//            String[] projection = {MediaStore.Images.Media.DATA};
//            Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
//            cursor.moveToFirst();
//            int columnIndex = cursor.getColumnIndex(projection[0]);
            File myFile = new File(uri.getPath());

            final String picturePath = myFile.getAbsolutePath();//cursor.getString(columnIndex); // returns null
            // cursor.close();

            //copy the selected file of gallery into app's sdcard folder and perform the compression operations on it.
            //And override the original image with the newly resized image.

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        CommonUtils.copyFile(picturePath, imageFilePath);
                    } catch (IOException e) {
                    }
                }
            });

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bmp = BitmapFactory.decodeFile(imageFilePath, options);

            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;

            Logg("actualheight2",actualHeight+" "+actualWidth);

            if(actualWidth>600 | actualHeight>600) {
                new ImageCompression(cameraimage).execute(imageFilePath);
            }
            else{

                getContentResolver().notifyChange(uri, null);
                ContentResolver cr =getContentResolver();
                Bitmap bitmap;

                try
                {
                    bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, uri);
                    //Logg("bitmap_h_w",bitmap.getHeight()+" "+bitmap.getWidth()+"");
                    cameraimage.setImageBitmap(bitmap);

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream .toByteArray();

                    st_base46 = Base64.encodeToString(byteArray, Base64.DEFAULT);
//                    return Base64.encodeToString(byteArray, Base64.DEFAULT);

                    Logg("base64",st_base46);

                }
                catch (Exception e)
                {

                }
            }
        }
    }

    public void imagevolley() {

        if(!nw.isConnectingToInternet()) {
//            swipeRefreshLayout.setRefreshing(false);
            internetconnection(2);
            return;
        }

        Constants.checkrefresh=true;

        progress = ProgressDialog.show(Post_comment_ingroup.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final long time= System.currentTimeMillis();

//      st_com=st_com.replaceAll(" ","%20");

        //Logg("base64",st_base46);
        //Logg("id",groupid+"");
        //Logg("comment",st_com+"");
        //Logg("email",mail+"");
        //Logg("imagename",time+"");

        RequestQueue queue = Volley.newRequestQueue(Post_comment_ingroup.this);
        String url="";
        url = Constants.URL+"v3/postcomment_ingroupimage.php?";
        url = url.replace(" ", "%20");
        //Logg("name", url);

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                //Logg("response", s);
                try {
                    if(s.length()>0)
                    {
                        JSONObject obj=new JSONObject(s);
                        if(obj.getString("status").equalsIgnoreCase("success"))
                            successAlert();
                            // finish();
                        else if(obj.getString("status").equalsIgnoreCase("block"))
                            Toast.makeText(Post_comment_ingroup.this,obj.getString("response"),Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(Post_comment_ingroup.this,"Something is wrong can't post comment",Toast.LENGTH_LONG).show();

                        progress.dismiss();
                        //Logg("response_string",s+" response");
                    }
                }

                catch (Exception e)
                {
                    progress.dismiss();
                    //Logg("e","e",e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(Post_comment_ingroup.this,CommonUtils.volleyerror(volleyError));
                progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("base64",st_base46);
                params.put("id", groupid+"");
                params.put("comment",st_com);
                params.put("email",mail);
                params.put("imagename",time+"");

                Logg("params",params+"");

                return params;
            }
        };

        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void volleycominsert() {
        // TODO Auto-generated method stub

        if(!nw.isConnectingToInternet()) {
//            swipeRefreshLayout.setRefreshing(false);
            internetconnection(1);
            return;
        }

        Constants.checkrefresh=true;

        progress = ProgressDialog.show(Post_comment_ingroup.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        progress.show();

        String url=Constants.URL+"v3/postcomment_ingroup.php?";
        //Logg("url", url);

//        String cmnt=st_com;
       // st_com=st_com.replaceAll("'","%27");
        Logg("st_com", st_com);
        //st_com=st_com.replaceAll("\n","%20");

        RequestQueue que= Volley.newRequestQueue(getApplicationContext());

        //final String finalCmnt = cmnt;
        StringRequest obj1=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String res) {
                // TODO Auto-generated method stub

                //Logg("response",res);
                int size=res.length();
                if(size>0)
                {

                    try {
                        JSONObject obj=new JSONObject(res);

                        if(obj.getString("status").equalsIgnoreCase("success"))
                            successAlert();
                            //finish();
                        else if(obj.getString("status").equalsIgnoreCase("block"))
                            Toast.makeText(Post_comment_ingroup.this,obj.getString("response"),Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(Post_comment_ingroup.this,"Something is wrong can't post comment",Toast.LENGTH_LONG).show();

                        progress.dismiss();

                    } catch (JSONException e) {
                        progress.dismiss();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                // TODO Auto-generated method stub
                progress.dismiss();
                //Logg("error",e.toString());
            }
        }){

            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("id", groupid+"");
                params.put("comment", st_com);
                params.put("email",mail);

                Logg("params",params+"");

                return params;
            }
        };

        int socketTimeout =0;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        obj1.setRetryPolicy(policy);

        que.add(obj1);

    }

    public void volley_getFollowGroup()
    {

        if(!nw.isConnectingToInternet()) {
//            swipeRefreshLayout.setRefreshing(false);
            internetconnection(0);
            return;
        }


        progress = ProgressDialog.show(Post_comment_ingroup.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        progress.show();


        grouplist=new ArrayList<gettr_settr>();

        RequestQueue queue=Volley.newRequestQueue(Post_comment_ingroup.this);
        JSONObject obj=new JSONObject();
        String url=Constants.URL+"newapi2_04/get_group.php?email="+mail;
        //Logg("Url",url);
        final JsonObjectRequest json=new JsonObjectRequest(Request.Method.POST, url, obj,new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                // TODO Auto-generated method stub

                //   //Logg("response",String.valueOf(res));

                try {

                    if(res.has("discuss")) {

                        JSONArray jr = res.getJSONArray("discuss");

                        for (int i = 0; i < jr.length(); i++) {

                            JSONObject jsonobj = jr.getJSONObject(i);

                            //Logg("followstatus "+i,jsonobj.getString("follow_status"));

                            if(jsonobj.getString("follow_status").equalsIgnoreCase("follow")&jsonobj.getString("status").equalsIgnoreCase("public")) {
                                grouplist.add(new gettr_settr(jsonobj.getString("topic"), jsonobj.getInt("comments"), jsonobj.getInt("id"),
                                        jsonobj.getString("member"), jsonobj.getString("follow_status"),
                                        jsonobj.getString("status"), jsonobj.getString("image"),jsonobj.getString("payment"),jsonobj.getString("payment_status")));
                            }

                        }
                        if(grouplist.size()>0)
                            dialoggroup();
                        else
                        {
                            Toast.makeText(Post_comment_ingroup.this,"You Have No Any Follow Group! Follow Atleast One Group",Toast.LENGTH_LONG).show();
                            finish();
                        }

                    }

                    progress.dismiss();

                }
                catch (JSONException e) {
                    progress.dismiss();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                //Logg("error discuss", "e",e);
                progress.dismiss();
            }
        });

        int socketTimeout = 50000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        json.setRetryPolicy(policy);
        queue.add(json);
    }

    public void internetconnection(final int i){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                if(i==0)
                    volley_getFollowGroup();
                else if(i==1)
                    volleycominsert();
                else if(i==2)
                    imagevolley();

//				getData();

            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    private void successAlert()
    {

        final Dialog dialog=new Dialog(Post_comment_ingroup.this,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.post_confirmation_dialog);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        TextView text,text1;
        text=(TextView)dialog.findViewById(R.id.text);
        text1=(TextView)dialog.findViewById(R.id.text1);
        TextView Submit=(TextView)dialog.findViewById(R.id.tv_submit);
        TextView Cancel=(TextView)dialog.findViewById(R.id.tv_cancel);
//        Cancel.setVisibility(View.VISIBLE);

//        if(Constants.checksolution)
//            dialog.setCancelable(false);

        text.setTypeface(font_medium);
        text1.setTypeface(font_medium);
        Submit.setTypeface(font_demi);
        Cancel.setTypeface(font_demi);

        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

                finish();

            }
        });
    }

}
