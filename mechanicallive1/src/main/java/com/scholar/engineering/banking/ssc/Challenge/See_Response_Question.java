package com.scholar.engineering.banking.ssc.Challenge;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.scholar.engineering.banking.ssc.HomeActivity;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.toast;

/**
 * Created by delaine on 22/1/18.
*/

public class See_Response_Question extends AppCompatActivity {

    TabLayout tabLayout;

    String challenge_id="";
    int ques_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.see_response_question);

        challenge_id=getIntent().getStringExtra("challenge_id");
        ques_id=getIntent().getIntExtra("question_id",0);

        initToolbar();

        getData();
    }

    public void initToolbar(){

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("See Response");
        Utility.applyFontForToolbarTitle(toolbar,this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void initViewPagerAndTabs(JSONObject object) {

        Fragment frag_your_res=null,frag_correct_res=null;
        ViewPager viewPager = (ViewPager)findViewById(R.id.viewPager);

        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());

        frag_your_res=new Your_Answer();
                frag_correct_res=new Correct_Answer();

        Bundle bundle=new Bundle();

        bundle.putString("object",object+"");
        frag_your_res.setArguments(bundle);
        frag_correct_res.setArguments(bundle);

        pagerAdapter.addFragment(frag_your_res, "Your Response");
        pagerAdapter.addFragment(frag_correct_res, "Correct Response");

        viewPager.setAdapter(pagerAdapter);
        pagerAdapter.notifyDataSetChanged();
        tabLayout = (TabLayout)findViewById(R.id.tabLayout);

        tabLayout.setupWithViewPager(viewPager);

    }

    public class PagerAdapter extends FragmentStatePagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }

    public void getData() {

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("loading...");
        pd.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        String url="";
        url = Constants.URL_LV+"getsingle_question";
        url = url.replace(" ", "%20");
        Logg("name", url);

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {

                Logg("response", s);
                try
                {

                    if(s.length()>0)
                    {

                        JSONObject arg0=new JSONObject(s);
                        if(arg0.getString("status").equalsIgnoreCase("true")) {
                            initViewPagerAndTabs(arg0.getJSONObject("data"));
                        }
                        else if(arg0.getString("status").equalsIgnoreCase("false")){
                            toast(See_Response_Question.this,arg0.getString("message"));
                        }
                    }

                    pd.dismiss();
                }

                catch (Exception e)
                {
                    pd.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(See_Response_Question.this,CommonUtils.volleyerror(volleyError));
                pd.dismiss();
            }

        }) {

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id",Constants.User_Email);
                map.put("challenge_id",challenge_id); //course_id
                map.put("question_id",ques_id+"");

                Logg("map",map.toString());

                return map;

            }
        };

        int socketTimeout = 20000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);

    }
}
