package com.scholar.engineering.banking.ssc.newScreens.QueryForms

import android.app.ProgressDialog
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.RetryPolicy
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.jaredrummler.materialspinner.MaterialSpinnerAdapter
import com.scholar.engineering.banking.ssc.R
import com.scholar.engineering.banking.ssc.newScreens.NotesPdfGetSet
import com.scholar.engineering.banking.ssc.newScreens.adapters.NotesPDFAdapter
import com.scholar.engineering.banking.ssc.newScreens.adapters.QuerylistAdapter
import com.scholar.engineering.banking.ssc.utils.CommonUtils
import com.scholar.engineering.banking.ssc.utils.Constants
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences
import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList
import java.util.HashMap

class QueryList : AppCompatActivity() {
    var toolbar: Toolbar? = null
    var toolbartitle: TextView? = null

    lateinit var context: Context
    lateinit var sharedPreferences: UserSharedPreferences
    var pd: ProgressDialog? = null

    lateinit var recyclerView: RecyclerView
    lateinit var itemVideos: ArrayList<QueryGetSet>
    lateinit var adapter: QuerylistAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_query_list)
        context=this
        toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        toolbartitle = toolbar?.findViewById(R.id.headertextid) as TextView
        toolbartitle?.setText("Support Queries")





        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        toolbar!!.setNavigationOnClickListener(View.OnClickListener {
            finish()
        })

        sharedPreferences = UserSharedPreferences.getInstance(this)
        pd = ProgressDialog(this)
        pd?.setMessage("loading")
        pd?.setCancelable(false)

        itemVideos = ArrayList()
        recyclerView=findViewById(R.id.recyclerviewquery)
        recyclerView.setHasFixedSize(true)
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = linearLayoutManager
        getItemList()


    }

    private fun getItemList() {
        val requestQueue = Volley.newRequestQueue(this)
        val stringRequest: StringRequest = object : StringRequest(Method.POST, Constants.URL_LV+"supportget",
                Response.Listener { response ->
                    Log.e("response_dataquery",response)
                    try {
                        val dataobject = JSONObject(response)
                        if(dataobject.getString("status").equals("true"))
                        {
                            val ara = dataobject.getJSONArray("data")
                            for (i in 0 until ara.length()) {
                                val obj = ara.getJSONObject(i)
                                itemVideos.add(QueryGetSet(obj.getString("id"),obj.getString("ticketno"),
                                        obj.getString("issue"),
                                        obj.getString("details"),
                                        obj.getString("status"),
                                obj.toString()))
                            }
                            adapter= QuerylistAdapter(context,itemVideos)
                            recyclerView.adapter=adapter
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error ->
                    CommonUtils.volleyerror(error)
                }
        ) {
            override fun getParams(): Map<String, String> {
                val params: MutableMap<String, String> = HashMap()
                params["std_roll"] = sharedPreferences.getaddmissiono()
//                params["std_roll"] = "1111"
                Log.e("get_assignments", params.toString() + "")
                return params
            }
        }
        stringRequest.setShouldCache(false)

        val socketTimeout = 0
        val policy: RetryPolicy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.setRetryPolicy(policy)

        requestQueue.add(stringRequest)
    }



}