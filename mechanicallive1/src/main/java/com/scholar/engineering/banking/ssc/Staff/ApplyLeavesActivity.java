package com.scholar.engineering.banking.ssc.Staff;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.LeaveGettingModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ViewLeavesModel;
import com.scholar.engineering.banking.ssc.Staff.adapter.LeaveRequestAdapter;
import com.scholar.engineering.banking.ssc.databinding.ActivityApplyLeavesBinding;
import com.scholar.engineering.banking.ssc.getset.RetrofitObjectAPI;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import java.util.List;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class ApplyLeavesActivity extends AppCompatActivity implements View.OnClickListener{

    ActivityApplyLeavesBinding binding;
    LeaveRequestAdapter leaveRequestAdapter;
    NetworkConnection nw;
    UserSharedPreferences preferences;
    UserSharedPreferences playerpreferences;
    Integer id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_apply_leaves);

        nw = new NetworkConnection(ApplyLeavesActivity.this);
        playerpreferences=new UserSharedPreferences(ApplyLeavesActivity.this,"playerpreferences");
        preferences=UserSharedPreferences.getInstance(ApplyLeavesActivity.this);
        id = preferences.getId();

        binding.imgBack.setOnClickListener(this);
        binding.ivAdd.setOnClickListener(this);
        binding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                viewLeaves();
                gettingLeaves();
            }
        });

        gettingLeaves();
        viewLeaves();
    }

    private void gettingLeaves() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<LeaveGettingModel> call = service.gettingLeaves(id);

        Logg("urlapigettingLeaves", Constants.URL_LV + "data?id=" + id);

        call.enqueue(new Callback<LeaveGettingModel>() {

            @Override
            public void onResponse(Call<LeaveGettingModel> call, retrofit2.Response<LeaveGettingModel> response) {
                if (response.isSuccessful()) {

                    try {
                        LeaveGettingModel server_response = response.body();
                        Integer casualLeave = server_response.getData().getCasualLeave();
                        Integer annualLeave = server_response.getData().getAnnualLeave();
                        Integer sickLeave = server_response.getData().getSickLeave();
                        binding.txtCasualLeave.setText(casualLeave + " " + "Leaves");
                        binding.txtAnnualLeave.setText(annualLeave + " " + "Leaves");
                        binding.txtSickLeave.setText(sickLeave + " " + "Leaves");
                        Integer remCasualLeave = casualLeave - server_response.getData().getCousumeCasualLeave();
                        Integer remAnnualLeave = annualLeave - server_response.getData().getCousumeAnnualLeave();
                        Integer remSickLeave = sickLeave - server_response.getData().getCousumeSickLeave();
                        binding.remCasualLeave.setText(String.valueOf(remCasualLeave));
                        binding.remAnnualLeave.setText(String.valueOf(remAnnualLeave));
                        binding.remSickLeave.setText(String.valueOf(remSickLeave));
                        binding.swipeRefreshLayout.setRefreshing(false);
                    } catch (Exception e){
                        e.printStackTrace();
                    }

                } else {
                    binding.swipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(ApplyLeavesActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LeaveGettingModel> call, Throwable t) {
                binding.swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void setData(List<ViewLeavesModel.Datum> viewLeaves){
        leaveRequestAdapter = new LeaveRequestAdapter(ApplyLeavesActivity.this,viewLeaves);
        binding.requestRecylerview.setHasFixedSize(true);
        binding.requestRecylerview.setLayoutManager(new LinearLayoutManager(ApplyLeavesActivity.this));
        binding.requestRecylerview.setAdapter(leaveRequestAdapter);
    }


    private void viewLeaves() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<ViewLeavesModel> call = service.viewLeaves(id);
        Logg("urlapiviewLeaves", Constants.URL_LV + "data?id=" + id);
        call.enqueue(new Callback<ViewLeavesModel>() {
            @Override
            public void onResponse(Call<ViewLeavesModel> call, retrofit2.Response<ViewLeavesModel> response) {
                if (response.isSuccessful()) {
                    setData(response.body().getData());
                    binding.swipeRefreshLayout.setRefreshing(false);
                } else {
                    binding.swipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(ApplyLeavesActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ViewLeavesModel> call, Throwable t) {
                Log.e("fail",t.getMessage());
                binding.swipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    public void internetconnection() {
        final Dialog dialog = new Dialog(ApplyLeavesActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.swipeRefreshLayout.setRefreshing(true);
                dialog.dismiss();
                gettingLeaves();
                viewLeaves();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.iv_add:
                startActivity(new Intent(ApplyLeavesActivity.this,LeaveApplicationActivity.class));
                break;
        }
    }
}