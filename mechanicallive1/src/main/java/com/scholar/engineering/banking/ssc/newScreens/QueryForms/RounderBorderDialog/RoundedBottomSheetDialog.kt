package com.scholar.engineering.banking.ssc.newScreens.QueryForms.RounderBorderDialog

import android.content.Context
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.scholar.engineering.banking.ssc.R

open class RoundedBottomSheetDialog(context: Context) : BottomSheetDialog(context, R.style.BottomSheetDialogTheme)