package com.scholar.engineering.banking.ssc;


import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.scholar.engineering.banking.ssc.utils.Utility;

public class contactus extends AppCompatActivity{

	ScrollView sv;
	SharedPreferences pref;
	String data;
	WebView webView;
	Editor edit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contectus);
	    sv=(ScrollView)findViewById(R.id.scrollview1);
		Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("Contact Us");
		Utility.applyFontForToolbarTitle(toolbar,this);
		TextView header=(TextView)findViewById(R.id.headertextid);
		header.setVisibility(View.GONE);

		data="<div dir=\"ltr\" style=\"text-align: left;\" trbidi=\"on\">\n" +
				"<div class=\"p1\">\n" +
				"<b><span style=\"color: #666666;\">Contact Us:</span></b></div>\n" +
				"<div class=\"p2\">\n" +
				"<span style=\"color: #666666;\"><br /></span></div>\n" +
				"<div class=\"p3\">\n" +
				"<span class=\"s1\"><span style=\"color: #666666;\">Email: <span class=\"s2\">info@gyanstrot.com</span></span></span></div>\n" +
				"<div class=\"p2\">\n" +
				"<span style=\"color: #666666;\"><br /></span></div>\n" +
				"<div class=\"p1\">\n" +
				"<b><span style=\"color: #666666;\">Address:</span></b></div>\n" +
				"<p>Sambhaav<br />603/13, Old Railway Rd, Sector 11, Gurugram, Haryana 122022<br />09034494937</p>"+
				"</div>";


		webView=(WebView)findViewById(R.id.webview);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.loadDataWithBaseURL(null,data, "text/html", "utf-8",null);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(item.getItemId()== android.R.id.home)
			finish();
		return super.onOptionsItemSelected(item);
	}
}
