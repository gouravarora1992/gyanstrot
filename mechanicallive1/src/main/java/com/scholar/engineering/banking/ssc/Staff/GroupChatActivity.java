package com.scholar.engineering.banking.ssc.Staff;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.databinding.ActivityGroupChatBinding;

public class GroupChatActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityGroupChatBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_group_chat);

        binding.imgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back:
                onBackPressed();
                break;
        }
    }
}