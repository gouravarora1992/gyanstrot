package com.scholar.engineering.banking.ssc;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.scholar.engineering.banking.ssc.utils.NetworkConnection;

import java.io.File;

import pl.tajchert.waitingdots.DotsTextView;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

/**
 * Created by surender on 4/19/2017.
 */
public class Legal_Policy extends AppCompatActivity implements View.OnClickListener{

//    WebView wv;
//    DotsTextView dots;
    NetworkConnection nw;
    Button btn_privacy, btn_terms;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.legal_policy);

        nw=new NetworkConnection(this);
        btn_privacy = findViewById(R.id.btn_privacy_policy);
        btn_terms = findViewById(R.id.btn_termsndconditions);

        btn_privacy.setOnClickListener(this);
        btn_terms.setOnClickListener(this);

//        dots=(DotsTextView)findViewById(R.id.dots);
//        wv=(WebView)findViewById(R.id.webviewid);
//        wv.getSettings().setJavaScriptEnabled(true);
//        wv.setWebViewClient(new Callback());
//
//        wv.loadUrl("http://hellotopper.com/mechanicalinsider/policy.html");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logg("call_onDestroy()","Call onDestroy()");
        deleteCache(this);
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {}
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public void internetconnection(final int i){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
//                dots.setVisibility(View.VISIBLE);
//                wv.loadUrl("");

            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
            super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_privacy_policy:
                startActivity(new Intent(Legal_Policy.this,PrivacyPolicyActivity.class));
                break;
            case R.id.btn_termsndconditions:
                startActivity(new Intent(Legal_Policy.this,TermsNdConditions.class));
                break;
        }

    }

    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return(false);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            //Logg("onPagestarted",url+"");

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
    //        dots.setVisibility(View.GONE);
            //Logg("onPageFinished "+postid,url+"");

            // bar.setVisibility(View.GONE);

        }
    }

}
