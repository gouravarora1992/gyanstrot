package com.scholar.engineering.banking.ssc;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.scholar.engineering.banking.ssc.Staff.ModelClass.ViewAttendanceModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ViewStudentAttendanceListModel;
import com.scholar.engineering.banking.ssc.Staff.StaffStudentAttendanceActivity;
import com.scholar.engineering.banking.ssc.Staff.adapter.StaffStudentAttendanceAdapter;
import com.scholar.engineering.banking.ssc.adapter.StudentAttendanceAdapter;
import com.scholar.engineering.banking.ssc.databinding.ActivityStudentAttendanceBinding;
import com.scholar.engineering.banking.ssc.getset.RetrofitObjectAPI;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class StudentAttendanceActivity extends AppCompatActivity implements View.OnClickListener{

    ActivityStudentAttendanceBinding binding;
    NetworkConnection nw;
    UserSharedPreferences preferences;
    UserSharedPreferences playerpreferences;
    private int mYear, mMonth, mDay;
    String std_class, std_section, std_admissionno;
    StudentAttendanceAdapter mAdapter;
    ViewStudentAttendanceListModel attendanceListModels = new ViewStudentAttendanceListModel();
    List<Date> dates = new ArrayList<Date>();
    JSONArray jsonArray = new JSONArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_student_attendance);

        nw = new NetworkConnection(StudentAttendanceActivity.this);
        playerpreferences=new UserSharedPreferences(StudentAttendanceActivity.this,"playerpreferences");
        preferences=UserSharedPreferences.getInstance(StudentAttendanceActivity.this);

        std_class = preferences.getclass();
        std_section = preferences.getsection();
        std_admissionno = preferences.getaddmissiono();

  //      attendanceListModels = new ArrayList<>();
        binding.imgBack.setOnClickListener(this);
        binding.txtFromDate.setOnClickListener(this);
        binding.txtToDate.setOnClickListener(this);

  //      getDate("2021-05-01","2021-05-14");

  //      getViewAttendance();
    }

 //   private int mYear, mMonth, mDay;
    void datePickerFromDate(){
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        final DecimalFormat mFormat= new DecimalFormat("00");
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        binding.txtFromDate.setText(mFormat.format(Double.valueOf(dayOfMonth)) + "-" +  mFormat.format(Double.valueOf((monthOfYear + 1)))
                                + "-" + year);

                    }
                }, mYear, mMonth, mDay);
        //      datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                binding.txtFromDate.setText(year + "-" +  mFormat.format(Double.valueOf((month + 1)))
                        + "-" + mFormat.format(Double.valueOf(dayOfMonth)));

//                if (!(binding.txtFromDate.getText().toString().isEmpty()) && (!(binding.txtFromDate.getText().toString().equalsIgnoreCase("YYYY-MM-DD"))
//                        && (!(binding.txtToDate.getText().toString().isEmpty())) && (!(binding.txtToDate.getText().toString().equalsIgnoreCase("YYYY-MM-DD"))))) {
//                    getDate(binding.txtFromDate.getText().toString(), binding.txtToDate.getText().toString());
//                    getViewAttendance();
//                }

            }
        });

        datePickerDialog.show();
    }


    void datePickerToDate(){
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        final DecimalFormat mFormat= new DecimalFormat("00");
        final DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        binding.txtToDate.setText(year + "-" +  mFormat.format(Double.valueOf((monthOfYear + 1)))
                                + "-" + mFormat.format(Double.valueOf(dayOfMonth)));

                    }
                }, mYear, mMonth, mDay);
        //      datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                binding.txtToDate.setText(year + "-" +  mFormat.format(Double.valueOf((month + 1)))
                        + "-" + mFormat.format(Double.valueOf(dayOfMonth)));

                if (!(binding.txtFromDate.getText().toString().isEmpty()) && (!(binding.txtFromDate.getText().toString().equalsIgnoreCase("YYYY-MM-DD")))
                        && (!(binding.txtToDate.getText().toString().isEmpty())) && (!(binding.txtToDate.getText().toString().equalsIgnoreCase("YYYY-MM-DD")))) {

                    jsonArray = new JSONArray();
                    Log.e("jsonArray>>",""+jsonArray);
                    getDate(binding.txtFromDate.getText().toString(), binding.txtToDate.getText().toString());
                    getViewAttendance();
                }

            }
        });

        datePickerDialog.show();
    }


    private void getDate(String str_date, String end_date){

        dates.clear();
        Log.e("dates",""+dates.size());
        DateFormat formatter ;

        formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        try {
            startDate = (Date)formatter.parse(str_date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Date endDate = null;
        try {
            endDate = (Date)formatter.parse(end_date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        long interval = 24*1000 * 60 * 60; // 1 hour in millis
        long endTime =endDate.getTime() ; // create your endtime here, possibly using Calendar or Date
        long curTime = startDate.getTime();
        while (curTime <= endTime) {
            dates.add(new Date(curTime));
            curTime += interval;
        }
//        for(int i=0;i<dates.size();i++){
//            Date lDate =(Date)dates.get(i);
//            ds = formatter.format(lDate);
//            System.out.println(ds);
//
//        }
    }

    private void setData(ViewStudentAttendanceListModel list) {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        binding.recylerAttendance.setLayoutManager(mLayoutManager);
        mAdapter = new StudentAttendanceAdapter(StudentAttendanceActivity.this, list, jsonArray);
        binding.recylerAttendance.setAdapter(mAdapter);
//        mAdapter.notifyAll();
    }


    private void getViewAttendance() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<ViewStudentAttendanceListModel> call = service.getAttendance(std_class, std_section, binding.txtFromDate.getText().toString(),
                binding.txtToDate.getText().toString(),std_admissionno);

        Logg("viewAttendance", Constants.URL_LV + "data?classss=" + std_class + "&section=" +std_section +
                "&fromDate" + binding.txtFromDate.getText().toString() + "&toDate" + binding.txtToDate.getText().toString() + "&admissiono" + std_admissionno);

        call.enqueue(new Callback<ViewStudentAttendanceListModel>() {

            @Override
            public void onResponse(Call<ViewStudentAttendanceListModel> call, retrofit2.Response<ViewStudentAttendanceListModel> response) {
                if (response.isSuccessful()) {
                    jsonArray = new JSONArray();
                    Log.e("jsonArray>>",""+jsonArray);
                    DateFormat formatter ;
                    formatter = new SimpleDateFormat("yyyy-MM-dd");
                    List<String> presentArr = response.body().getPresent();
                    List<String> absentArr = response.body().getAbsent();
                    for (int i=0;i<dates.size();i++) {
                        try {
                            String status = "";
                            Date lDate =(Date)dates.get(i);
                            String ds = formatter.format(lDate);
                            System.out.println(ds);
                            if(presentArr.contains(ds)) {
                                status = "present";
                            } else if(absentArr.contains(ds)){
                                status = "absent";
                            } else {
                                status = "holiday";
                            }
                            JSONObject row =  new JSONObject();
                            row.put("date", ds);
                            row.put("status", status);
                            jsonArray.put(row);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    Log.e("jsonArray",""+jsonArray);
                    Log.e("response",""+response.body().getMessage());
                    Log.e("presentArr",""+response.body().getPresent());
                    Log.e("absentArr",""+response.body().getAbsent());
                    setData(response.body());

                } else {
                    Toast.makeText(StudentAttendanceActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ViewStudentAttendanceListModel> call, Throwable t) {
                Log.e("fail",t.getMessage());
            }
        });
    }


    public void internetconnection() {

        final Dialog dialog = new Dialog(StudentAttendanceActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getViewAttendance();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back:
                onBackPressed();

            case R.id.txt_from_date:
                datePickerFromDate();
                break;

            case R.id.txt_to_date:
                datePickerToDate();
                break;
        }
    }
}