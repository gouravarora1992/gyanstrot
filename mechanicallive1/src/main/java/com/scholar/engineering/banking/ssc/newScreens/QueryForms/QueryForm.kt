package com.scholar.engineering.banking.ssc.newScreens.QueryForms

import android.Manifest
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.RetryPolicy
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.jaredrummler.materialspinner.MaterialSpinner
import com.jaredrummler.materialspinner.MaterialSpinnerAdapter
import com.scholar.engineering.banking.ssc.R
import com.scholar.engineering.banking.ssc.utils.CommonUtils
import com.scholar.engineering.banking.ssc.utils.Constants
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences
import com.scholar.engineering.banking.ssc.utils.Utility
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.set


class QueryForm : AppCompatActivity() {
    var toolbar: Toolbar? = null
    var toolbartitle: TextView? = null
    lateinit var preferences: UserSharedPreferences

    lateinit var save: Button
    lateinit var etenterdetails: EditText
    lateinit var reset: Button
    lateinit var tvfilename: TextView
    lateinit var spinner: MaterialSpinner
    lateinit var itemList: ArrayList<String>
    lateinit var choosefile: Button
    var issue: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_query_form)
        preferences = UserSharedPreferences.getInstance(this)
        toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        toolbartitle = toolbar?.findViewById(R.id.headertextid) as TextView
        toolbartitle?.setText("Query Submit")
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        toolbar!!.setNavigationOnClickListener(View.OnClickListener {
            finish()
        })




        etenterdetails = findViewById(R.id.etenterdetails)
        tvfilename = findViewById(R.id.tvfilename)
        itemList = ArrayList()
        getItemList()
        spinner = findViewById(R.id.spinner) as MaterialSpinner
        spinner.setOnItemSelectedListener(object : MaterialSpinner.OnItemSelectedListener<String?> {
            override fun onItemSelected(view: MaterialSpinner?, position: Int, id: Long, item: String?) {
                if (item != null) {
                    issue = item
                }
            }
        })
        choosefile = findViewById(R.id.choosefile)
        choosefile.setOnClickListener {

            val PERMISSIONS = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)

//            if (!Utility.hasPermissions(this, *PERMISSIONS)) {
//                CommonUtils.Logg("checking", "checking")
//                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
//                        Constants.MY_PERMISSIONS_REQUEST)
//            }else{
                getFileChooserIntent()
//            }


        }

        reset = findViewById(R.id.reset)
        reset.setOnClickListener {
            etenterdetails.setText("")
            tvfilename.text = ""
        }
        save = findViewById(R.id.save)
        save.setOnClickListener {
            if (issue != "")
                if (etenterdetails.text.isNotEmpty())
                    sendQuery()
                else
                    Toast.makeText(this, "Please enter issue detail", Toast.LENGTH_SHORT).show()
            else
                Toast.makeText(this, "Please select issue", Toast.LENGTH_SHORT).show()
        }
    }

    private fun getFileChooserIntent() {
//        val mimeTypes = arrayOf("image/*", "application/image")
//        val intent = Intent(Intent.ACTION_GET_CONTENT)
//        intent.addCategory(Intent.CATEGORY_OPENABLE)
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            intent.type = if (mimeTypes.size == 1) mimeTypes[0] else "*/*"
//            if (mimeTypes.isNotEmpty()) {
//                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
//            }
//        } else {
//            var mimeTypesStr = ""
//            for (mimeType in mimeTypes) {
//                mimeTypesStr += "$mimeType|"
//            }
//            intent.type = mimeTypesStr.substring(0, mimeTypesStr.length - 1)
//        }
//        startActivityForResult(intent, 100)

        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 100)

    }

    var bytes: ByteArray? = null
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            Log.e("data", data.toString())
            var uri: Uri? = data?.data
            Log.e("data2", uri.toString()+" - "+uri?.path+" - ")
            Log.e("data3", "- "+getRealPathFromURI_API19(this,uri!!))
            tvfilename.text = uri.toString()
            var file = File(getRealPathFromURI_API19(this,uri!!))
            val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
            bytes = getBytesFromBitmap(bitmap)
        }
    }

    // convert from bitmap to byte array
    fun getBytesFromBitmap(bitmap: Bitmap): ByteArray? {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream)
        return stream.toByteArray()
    }




    fun getRealPathFromURI_API19(context: Context,uri: Uri):String{
        var filePath = ""

        if (uri.getHost()!!.contains("com.android.providers.media")) {
            // Image pick from recent

            // Image pick from recent
            val wholeID: String = DocumentsContract.getDocumentId(uri)

            // Split at colon, use second item in the array

            // Split at colon, use second item in the array
            val id = wholeID.split(":".toRegex()).toTypedArray()[1]

            val column = arrayOf<String>(MediaStore.Images.Media.DATA)

            // where id is equal to

            // where id is equal to
            val sel: String = MediaStore.Images.Media._ID.toString() + "=?"

            val cursor: Cursor? = context.contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, arrayOf(id), null)

            val columnIndex: Int = cursor!!.getColumnIndex(column[0])

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex)
            }
            cursor.close()
            return filePath
        }else
        {
            // image pick from gallery
            return  "getRealPathFromURI_BelowAPI11(context,uri)"
        }
    }

    private fun getItemList() {
        val requestQueue = Volley.newRequestQueue(this)
        val stringRequest: StringRequest = object : StringRequest(Method.GET, Constants.URL_LV + "getissue",
                Response.Listener { response ->
                    Log.e("response", response)
                    try {
                        val dataobject = JSONObject(response)
                        if (dataobject.getString("status").equals("true")) {
                            val ara = dataobject.getJSONArray("data")
                            for (i in 0 until ara.length()) {
                                val obj = ara.getJSONObject(i)
                                itemList.add(obj.getString("name"))
                            }
                            val adapter: MaterialSpinnerAdapter<String> = MaterialSpinnerAdapter(this, itemList)
                            spinner.setAdapter(adapter)
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error ->
                    CommonUtils.volleyerror(error)
                }
        ) {}
        stringRequest.setShouldCache(false)

        val socketTimeout = 0
        val policy: RetryPolicy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.setRetryPolicy(policy)

        requestQueue.add(stringRequest)
    }

    var pd: ProgressDialog? = null
    private fun sendQuery() {
        pd = ProgressDialog(this)
        pd?.setMessage("loading")
        pd?.setCancelable(false)
        pd!!.show()
        val requestQueue = Volley.newRequestQueue(this)
        val stringRequest: StringRequest = object : StringRequest(Method.POST, Constants.URL_LV + "addsupport",
                Response.Listener { response ->
                    pd!!.dismiss()
                    Log.e("response", response)
                    try {
                        val dataobject = JSONObject(response)
                        if (dataobject.getString("status").equals("true")) {
                            if(bytes!=null)
                                uploadImage(dataobject.getString("data"))
                            else
                                finish()
                        } else {
                            Toast.makeText(this, dataobject.getString("message"), Toast.LENGTH_SHORT).show()
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        pd!!.dismiss()
                    }
                },
                Response.ErrorListener { error ->
                    CommonUtils.volleyerror(error)
                    pd!!.dismiss()
                }
        ) {
            override fun getParams(): Map<String, String> {
                val params: MutableMap<String, String> = HashMap()
                params["name"] = preferences.getname()
                params["std_roll"] = preferences.getaddmissiono()
                params["issue"] = issue
                params["details"] = etenterdetails.text.toString()
                params["std_class"] = preferences.getclass()
                params["std_section"] = preferences.getsection()
                Log.e("get_assignments", params.toString() + "")
                return params
            }
        }
        stringRequest.setShouldCache(false)

        val socketTimeout = 0
        val policy: RetryPolicy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.setRetryPolicy(policy)

        requestQueue.add(stringRequest)
    }

    private fun uploadImage(id:String?) {
        Log.e("upload","image")
        val requestQueue = Volley.newRequestQueue(this)
        val multipartRequest = object : VolleyMultipartRequest(Request.Method.POST,
                "http://139.59.90.236:82/api/uploadimage", Response.Listener {
            finish()
        },
                Response.ErrorListener {
                    CommonUtils.volleyerror(it)
                }) {
            override fun getParams(): Map<String, String> {
                val params: MutableMap<String, String> = HashMap()
                params["id"] =id!!
                Log.e("get_assignments", params.toString() + "")
                return params
            }

            // file name could found file base or direct access from real path
            // for now just get bitmap data from ImageView
            override fun getByteData(): Map<String, DataPart> {
                val params: MutableMap<String, DataPart> = HashMap()
                params["file"] = DataPart("file_avatar.jpg", bytes, "")
                Log.e("get_assignments", params.toString() + "")
                return params
            }
        }
        multipartRequest.setShouldCache(false)

        val socketTimeout = 0
        val policy: RetryPolicy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        multipartRequest.setRetryPolicy(policy)
        requestQueue.add(multipartRequest)
    }

}