package com.scholar.engineering.banking.ssc.newScreens;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.newScreens.adapters.SwitchprofilesAdapter;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.util.logging.Logger;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class SwitchProfile extends AppCompatActivity {

    Context mcoxt;
    RecyclerView recyclerViewprofiles;
    GridLayoutManager manager;
    SwitchprofilesAdapter adapter;
    UserSharedPreferences preferences;
    NetworkConnection nw;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switch_profile);

        mcoxt=SwitchProfile.this;
        nw = new NetworkConnection(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Utility.applyFontForToolbarTitle(toolbar, this);
        preferences=UserSharedPreferences.getInstance(mcoxt);
        recyclerViewprofiles=findViewById(R.id.recyclerViewprofiles);
        manager=new GridLayoutManager(mcoxt,2);
        recyclerViewprofiles.setLayoutManager(manager);
        try {
            JSONArray array=new JSONArray(preferences.getsiblingdata());
            Logg("siblings array",array+"");
            adapter=new SwitchprofilesAdapter(mcoxt,array);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        recyclerViewprofiles.setAdapter(adapter);
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public void internetconnection(final int i) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()== android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}