package com.scholar.engineering.banking.ssc.userprofile;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.widget.NestedScrollView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
//import com.github.clans.fab.FloatingActionButton;
//import com.google.android.gms.ads.MobileAds;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.scholar.engineering.banking.ssc.HomeActivity;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.adapter.PremiumTest_userHistory;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.EndlessRecyclerViewScrollListener;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class PremiumTest_UserHistory extends Fragment implements OnClickListener {

	ArrayList<Home_getset> homedata,homedata1;
	int searchlength=0;
	RecyclerView homelist;
	public static PremiumTest_userHistory adapter;
	SharedPreferences pref,sp;
	SwipeRefreshLayout swipeRefreshLayout;
	int versionCode,index=0;
	Boolean aBoolean=true;
	Boolean isRunning=true;
	NestedScrollView nestedscroll;
	RelativeLayout footerlayout;
	TextView loadmore,tv_empty;
	Typeface font_demi;
	ProgressWheel wheelbar;
	NetworkConnection nw;
	String username="",userid="";
//    FloatingActionButton Post_Fab,Link_Fab,Question_Fab;
    CardView cv_nodataview;
	View view;
	Context context;
	@Override
	public void onAttach(Context context) {
		this.context=context;
		super.onAttach(context);
	}

	@Override
	public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		view =inflater.inflate(R.layout.premiumtest_userhistory, container,false);

		nw=new NetworkConnection(context);
//		Constants.checkrefresh=true;
//		setHasOptionsMenu(true);

//		MobileAds.initialize(getActivity(), getString(R.string.test_admob_express_unit_id));
		sp=context.getSharedPreferences("likestatus", 0);
		font_demi = Typeface.createFromAsset(context.getAssets(), "avenirnextdemibold.ttf");


//        nestedscroll=(NestedScrollView)view.findViewById(R.id.nestedscroll);

		homelist=(RecyclerView) view.findViewById(R.id.recyclerView);
		cv_nodataview=(CardView)view.findViewById(R.id.cv_nodata);
		tv_empty=(TextView)view.findViewById(R.id.tv_empty);

		LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);

		homelist.setLayoutManager(mLayoutManager);

		homedata=new ArrayList<>();

		username=getArguments().getString("name","");
		userid=getArguments().getString("user_profile_id","");

		EndlessRecyclerViewScrollListener endless = new EndlessRecyclerViewScrollListener(mLayoutManager) {
			@Override
			public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
				Logg("page",page+"");

				index=page;
				footerlayout.setVisibility(View.VISIBLE);

				getData();
			}
		};

		homelist.addOnScrollListener(endless);

		footerlayout=(RelativeLayout)view.findViewById(R.id.footer_layout);
		loadmore=(TextView)view.findViewById(R.id.moreButton);
		wheelbar=(ProgressWheel)view.findViewById(R.id.progress_wheel);
		footerlayout.setVisibility(View.GONE);

		index=0;

		swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);

//		getData();

		swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {

				index=0;

				getData();

			}
		});

		pref=context.getSharedPreferences("checkdata",0);

//		footerlayout.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View view) {
//
//				Home_getset home;
//
//				home= (Home_getset) homedata.get(homedata.size()-1);
//
////				index= home.getId();
//
//				Logg("index=",index+"");
//					getData();
//
//				loadmore.setText("");
//				wheelbar.setVisibility(View.VISIBLE);
//
//			}
//		});

		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
//		Constants.Current_Activity=100;
//
//		if(Constants.checkrefresh) {
//			index = 0;
			getData();
//			Constants.checkrefresh=false;
//		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);

		inflater.inflate(R.menu.menu, menu);
		MenuItem item = menu.findItem(R.id.search);

		MenuItem playMenu = menu.findItem(R.id.shareid);
//
        if (Constants.Width <= 600)
            playMenu.setIcon(R.drawable.dollar);
        else
            playMenu.setIcon(R.drawable.dollar);

	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
	}

    @Override
    public void onStop() {
        super.onStop();
        //Logg("call_onstop","onStop");
    }

	public void deleteCache(Context context) {
		try {
			File dir = context.getCacheDir();
			deleteDir(dir);
		} catch (Exception e) {}
	}

	public boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
			return dir.delete();
		} else if(dir!= null && dir.isFile()) {
			return dir.delete();
		} else {
			return false;
		}
	}

	public void internetconnection(){

		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.network_alert);
		dialog.show();

//        JSONObject json;

		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
		TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

		//tv_tryagain.setText("homefragment");

		tv_tryagain.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

//				//index=0;
				swipeRefreshLayout.setRefreshing(true);
				dialog.dismiss();
				getData();

			}
		});

		iv_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

	}

	public void getData()
	{

		if(!nw.isConnectingToInternet()) {
			swipeRefreshLayout.setRefreshing(false);
			internetconnection();
			return;
		}

		RequestQueue queue= Volley.newRequestQueue(context);
		JSONObject obj=new JSONObject();
		String url= Constants.URL+"v3/premium_test_history.php?email="+userid+"&index="+index;

		Logg("premium_test_history",url);

		JsonObjectRequest json=new JsonObjectRequest(Request.Method.POST, url, obj,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res) {
				// TODO Auto-generated method stub
				try {

					Logg("test_history res",res+"");

					if(res.has("data")) {

						JSONArray jr = res.getJSONArray("data");
//						homedata=new ArrayList<>();

						if(index==0) {
							homedata.clear();

							for (int i = 0; i < jr.length(); i++) {
								JSONObject ob = jr.getJSONObject(i);
//							String time = Utility.getDate(ob.getString("timestamp"));
								Home_getset object = new Home_getset();
								object.setId(ob.getInt("id"));
								object.setTimestamp(ob.getString("timestamp"));
								object.setLikecount(ob.getInt("likes"));
								object.setCommentcount(ob.getInt("comment"));
								object.setViewcount(ob.getInt("view"));
								object.setUid(ob.getString("uid"));
								object.setPosttype(ob.getString("posttype"));
								object.setGroupid(ob.getString("groupid"));
								object.setFieldtype(ob.getString("field_type"));
								object.setJsonfield(ob.getString("jsondata"));
								object.setLikestatus(ob.getString("likestatus"));
								object.setPosturl(ob.getString("posturl"));
								object.setPostdescription(ob.getString("post_description"));
								if(ob.has("AppVersion"))
									object.setAppVersion(ob.getString("AppVersion"));

								homedata.add(object);

							}

							//Logg("data.size", homedata.size() + "");
							adapter = new PremiumTest_userHistory(context, homedata, "onlineexam");
							homelist.setAdapter(adapter);

//							Home_getset home;
//							home = (Home_getset) homedata.get(homedata.size() - 1);
//							index = home.getId();

						}
						else
						{
							homedata1=new ArrayList<>();

							for (int i = 0; i < jr.length(); i++) {
								JSONObject ob = jr.getJSONObject(i);
//							String time = Utility.getDate(ob.getString("timestamp"));
								Home_getset object = new Home_getset();
								object.setId(ob.getInt("id"));
								object.setTimestamp(ob.getString("timestamp"));
								object.setLikecount(ob.getInt("likes"));
								object.setCommentcount(ob.getInt("comment"));
								object.setViewcount(ob.getInt("view"));
								object.setUid(ob.getString("uid"));
								object.setPosttype(ob.getString("posttype"));
								object.setGroupid(ob.getString("groupid"));
								object.setFieldtype(ob.getString("field_type"));
								object.setJsonfield(ob.getString("jsondata"));
								object.setLikestatus(ob.getString("likestatus"));
								object.setPosturl(ob.getString("posturl"));
								object.setPostdescription(ob.getString("post_description"));
								if(ob.has("AppVersion"))
									object.setAppVersion(ob.getString("AppVersion"));

								homedata1.add(object);

							}

							adapter.addItem(homedata1);
							adapter.notifyDataSetChanged();

//							Home_getset home;
//							home = (Home_getset) homedata1.get(homedata1.size() - 1);
//							index = home.getId();

						}

						if(cv_nodataview.getVisibility()==View.VISIBLE){
							cv_nodataview.setVisibility(View.GONE);
						}

					}
					else {

						if(index==0){
							Logg("nodataview",index+" ");
							cv_nodataview.setVisibility(View.VISIBLE);
							if(Constants.User_Email.equalsIgnoreCase(userid))
								tv_empty.setText("You have not completed any task yet.");
							else
								tv_empty.setText(username+" has not completed any task yet.");

							view.setVerticalScrollBarEnabled(false);

						}
						else{
							cv_nodataview.setVisibility(View.GONE);
						}

						footerlayout.setVisibility(View.GONE);
					}
					swipeRefreshLayout.setRefreshing(false);
				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					footerlayout.setVisibility(View.GONE);

					swipeRefreshLayout.setRefreshing(false);
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				footerlayout.setVisibility(View.GONE);
				//Logg("checkversion error", String.valueOf(arg0));

//				if(progress.isShowing())
//					progress.dismiss();

				swipeRefreshLayout.setRefreshing(false);
			}
		});
		queue.add(json);
	}

	private String getDate(String OurDate)
	{
		try
		{
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date value = formatter.parse(OurDate);
			SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //this format changeable
			dateFormatter.setTimeZone(TimeZone.getDefault());
			OurDate = dateFormatter.format(value);
			//Log.d("OurDate", OurDate);
		}
		catch (Exception e)
		{
			//Logg("exception_time","e",e);
//            OurDate = "00-00-0000 00:00";
		}
		//Logg("date_time",OurDate);


		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String formattedDate = df.format(c.getTime());

		return timediff(OurDate,formattedDate);

	}

	public String timediff(String prevdate,String curdate)
	{

		Date d11 = null;
		Date d2 = null;

		String[] dd1= prevdate.split(" ");
		String[] date1=dd1[0].split("-");

		SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");

		try {
			d11 = format.parse(prevdate);
			d2 = format.parse(curdate);

			//in milliseconds
			long diff = d2.getTime() - d11.getTime();

			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffDays = diff / (24 * 60 * 60 * 1000);

			String pd=dd1[0];

			String cd;//=cal.get(Calendar.YEAR)+"-"+(cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.DAY_OF_MONTH);
			String[] cdarr=curdate.split(" ");
			cd=cdarr[0];
			String[] date2=cd.split("-");

			int years=0,months=0,days=0;

			years=Integer.parseInt(date2[0])-Integer.parseInt(date1[0]);
			months=Integer.parseInt(date2[1])-Integer.parseInt(date1[1]);
			days=Integer.parseInt(date2[2])-Integer.parseInt(date1[2]);

			years=(months<0)? years-1 : years;
			months=(months<0)? 12+(months) : months;
			months=(days<0)? months-1 : months;
			days=(days<0)? 30+days : days;

			//	//Logg("di time= "," hrs= "+diffHours+" min= "+diffMinutes+" "+" years=  "+years+" month=  "+months+" days= "+days+" pd= "+pd+" cd= "+cd);

			if(years>0)
			{
				return (String.valueOf(years)+" Years ago");
			}
			else if(months>0&years<1)
			{

				return (String.valueOf(months) + " Months ago");
				//newsdateedit.putString("postdate"+i,String.valueOf(months)+"Months ago");
			}
			else if(days>0&months<1)
			{
				return (String.valueOf(days) + " Days ago");
				//newsdateedit.putString("postdate"+i,String.valueOf(months)+" Days ago");
			}
			else if(diffHours>0&days<1)
			{
				return (String.valueOf(diffHours) + " Hours ago");
				//newsdateedit.putString("postdate"+i,String.valueOf(months)+" Hours ago");
			}
			else if(diffMinutes>0&diffHours<1)
			{
				return (String.valueOf(diffMinutes) + " Minutes ago");
				//newsdateedit.putString("postdate"+i,String.valueOf(months)+" Minutes ago");
			}
			else if(diffSeconds>0&diffMinutes<1)
			{
				return ("0 Minutes ago");
				//newsdateedit.putString("postdate"+i,"0 Minutes ago");
			}

		} catch (Exception e) {
			e.printStackTrace();
			////Logg("di time error= ",e+"");
		}
		return ("1 Minutes ago");
	}

    @Override
    public void onClick(View view) {

    }
}