package com.scholar.engineering.banking.ssc.Staff;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ApplyLeaveModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.SupportSystemResponseModel;
import com.scholar.engineering.banking.ssc.databinding.ActivityLeaveApplicationBinding;
import com.scholar.engineering.banking.ssc.getset.RetrofitObjectAPI;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.GetPath;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Calendar;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class LeaveApplicationActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityLeaveApplicationBinding binding;
    NetworkConnection nw;
    String type = "sick_leave";
    final int  PERMISSION_REQUEST_CODE=200;
    private final static int TAKE_PICTURE = 100;
    private static final int PICK_GALLERY = 102;
    private String imagePath;
    private Uri mImageCaptureUri;
    UserSharedPreferences preferences;
    UserSharedPreferences playerpreferences;
    Integer id;
    private File fileName =null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_leave_application);

        playerpreferences=new UserSharedPreferences(LeaveApplicationActivity.this,"playerpreferences");
        preferences=UserSharedPreferences.getInstance(LeaveApplicationActivity.this);
        id = preferences.getId();

        nw = new NetworkConnection(this);
        binding.llSick.setOnClickListener(this);
        binding.llEarn.setOnClickListener(this);
        binding.llCasual.setOnClickListener(this);
        binding.txtStartDate.setOnClickListener(this);
        binding.txtEndDate.setOnClickListener(this);
        binding.imgBack.setOnClickListener(this);
        binding.btnApplyLeave.setOnClickListener(this);
        binding.imgAdd.setOnClickListener(this);

        //camera
        if (!checkPermission()) {
            requestPermision();
        }
    }

    private int mYear, mMonth, mDay;
    void startdatePicker(){
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        final DecimalFormat mFormat= new DecimalFormat("00");
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        binding.txtStartDate.setText(year + "-" +  mFormat.format(Double.valueOf((monthOfYear + 1)))
                                + "-" + mFormat.format(Double.valueOf(dayOfMonth)));

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    private int eYear, eMonth, eDay;
    void enddatePicker(){
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        eYear = c.get(Calendar.YEAR);
        eMonth = c.get(Calendar.MONTH);
        eDay = c.get(Calendar.DAY_OF_MONTH);

        final DecimalFormat mFormat= new DecimalFormat("00");
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        binding.txtEndDate.setText(year + "-" +  mFormat.format(Double.valueOf((monthOfYear + 1)))
                                + "-" + mFormat.format(Double.valueOf(dayOfMonth)));

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    private boolean checkValidation(){
        boolean ret=true;
        String strSubject = binding.etSubject.getText().toString();
        String strDescription = binding.etDescription.getText().toString();
        String strStartDate = binding.txtStartDate.getText().toString();
        String strEndDate = binding.txtEndDate.getText().toString();

        if(TextUtils.isEmpty(strSubject)) {
            ret=false;
            Toast.makeText(LeaveApplicationActivity.this,getString(R.string.enter_subject), Toast.LENGTH_LONG).show();
        }  else if(TextUtils.isEmpty(strStartDate)) {
            ret=false;
            Toast.makeText(LeaveApplicationActivity.this,getString(R.string.enter_start_date), Toast.LENGTH_LONG).show();
        } else if(TextUtils.isEmpty(strEndDate)) {
            ret=false;
            Toast.makeText(LeaveApplicationActivity.this,getString(R.string.enter_end_date), Toast.LENGTH_LONG).show();
        } else  if(TextUtils.isEmpty(strDescription)) {
            ret=false;
            Toast.makeText(LeaveApplicationActivity.this,getString(R.string.enter_description), Toast.LENGTH_LONG).show();
        }

        return ret;
    }


    private void changeLeaveBackgroundColour(String currentLeaveType){

        if (currentLeaveType.equalsIgnoreCase("sick")){
            type = "sick_leave";
            binding.llSick.setBackground(this.getDrawable(R.drawable.bg_leave));
            binding.txtSick.setTextColor(Color.WHITE);
            binding.llCasual.setBackgroundColor(Color.WHITE);
            binding.txtCasual.setTextColor(Color.BLACK);
            binding.llEarn.setBackgroundColor(Color.WHITE);
            binding.txtEarn.setTextColor(Color.BLACK);
            binding.etSubject.setText("");
            binding.txtStartDate.setText("");
            binding.txtEndDate.setText("");
            binding.etDescription.setText("");
        } else if (currentLeaveType.equalsIgnoreCase("casual")){
            type = "casual_leave";
            binding.llSick.setBackgroundColor(Color.WHITE);
            binding.txtSick.setTextColor(Color.BLACK);
            binding.llCasual.setBackground(this.getDrawable(R.drawable.bg_leave));
            binding.txtCasual.setTextColor(Color.WHITE);
            binding.llEarn.setBackgroundColor(Color.WHITE);
            binding.txtEarn.setTextColor(Color.BLACK);
            binding.etSubject.setText("");
            binding.txtStartDate.setText("");
            binding.txtEndDate.setText("");
            binding.etDescription.setText("");
        } else if (currentLeaveType.equalsIgnoreCase("earn")){
            type = "earn_leave";
            binding.llSick.setBackgroundColor(Color.WHITE);
            binding.txtSick.setTextColor(Color.BLACK);
            binding.llCasual.setBackgroundColor(Color.WHITE);
            binding.txtCasual.setTextColor(Color.BLACK);
            binding.llEarn.setBackground(this.getDrawable(R.drawable.bg_leave));
            binding.txtEarn.setTextColor(Color.WHITE);
            binding.etSubject.setText("");
            binding.txtStartDate.setText("");
            binding.txtEndDate.setText("");
            binding.etDescription.setText("");
        }
    }


    //permission
    private void requestPermision() {
        ActivityCompat.requestPermissions(LeaveApplicationActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
    }

    private boolean checkPermission() {

        int READ_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(LeaveApplicationActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int WRITE_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(LeaveApplicationActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (WRITE_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else if (READ_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    //take picture from camera
    private void takePictureFromCamera(Context context, String imagePath) {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            mImageCaptureUri = Uri.fromFile(new File(LeaveApplicationActivity.this.getExternalFilesDir("temp"), imagePath));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, TAKE_PICTURE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //DailogForImageFromCameraAndGallery
    public void profileBottomLayout() {
        LayoutInflater layoutInflater = (LayoutInflater) LeaveApplicationActivity.this.getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.upload_pic_layout, null);
        ImageView camera = (ImageView) popupView.findViewById(R.id.camera);
        ImageView gallery = (ImageView) popupView.findViewById(R.id.gallery);

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(LeaveApplicationActivity.this);
        alertDialog.setTitle("Upload Photo");
        alertDialog.setView(popupView);
        final AlertDialog dialog = alertDialog.show();
        alertDialog.setCancelable(true);

        //ImageFromCamera
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPermission()) {
                    imagePath = System.currentTimeMillis() + ".png";
                    takePictureFromCamera(LeaveApplicationActivity.this, imagePath);
                } else {
                    requestPermision();
                }
                dialog.dismiss();
            }

        });

        //ImageFromGallery
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPermission()) {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_GALLERY);
                } else {
                    requestPermision();
                }
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && permissions[0].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && permissions[1].equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&  grantResults[1] == PackageManager.PERMISSION_GRANTED ) {
                        Log.e("granted","WRITE_EXTERNAL_STORAGE \n READ_EXTERNAL_STORAGE");
                        // do what you want;
                    }else{
                        Log.e("denied","WRITE_EXTERNAL_STORAGE \n READ_EXTERNAL_STORAGE");
                    }
                }
                break;
            default:
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //ImageFromCamera
        if (requestCode == TAKE_PICTURE) {
            if (resultCode == RESULT_OK) {
                try {
                    File file = new File(LeaveApplicationActivity.this.getExternalFilesDir("temp"), imagePath);
                    String path = file.getPath();
                    Log.e("path",path);
                    fileName =new Compressor(LeaveApplicationActivity.this).compressToFile(new File(path));
                    Log.e("newPath",fileName.getName());
                    binding.imgFile.setImageURI(Uri.fromFile(fileName));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        //ImageFromGallery
        else if (requestCode == PICK_GALLERY) {
            if (resultCode == RESULT_OK) {
                try {
                    if (data != null) {
                        Uri filePath = data.getData();
                        imagePath= GetPath.getPath(this,filePath);
                        Log.e("pathGallery", ""+ imagePath);
                        if (imagePath!=null) {
                            fileName = new File(imagePath);
                            Log.e("fileName", "" + fileName.getName());
                            binding.imgFile.setImageURI(Uri.fromFile(fileName));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == 1 && resultCode == RESULT_CANCELED) {

            }
        }
    }


    private void getApplyLeave() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }

        MultipartBody.Part requestImage = null;
        if(imagePath!=null){
            fileName = new File(imagePath);
            Log.e("Register",""+fileName.getName());
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileName);
            requestImage = MultipartBody.Part.createFormData("file", fileName.getName(), requestFile);
        }

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<ApplyLeaveModel> call = null;
        if (requestImage!=null) {
            call = service.applyLeave(id, type, binding.etSubject.getText().toString(),
                    binding.txtStartDate.getText().toString(), binding.txtEndDate.getText().toString(), binding.etDescription.getText().toString(), requestImage);
        } else {
            call = service.applyLeave(id, type, binding.etSubject.getText().toString(),
                    binding.txtStartDate.getText().toString(), binding.txtEndDate.getText().toString(), binding.etDescription.getText().toString());
        }
        Logg("urlapplyLeave", Constants.URL_LV + "data?id=> " + id + "data?type=> " +type +
                "data?subject=> " +binding.etSubject.getText().toString() + "data?startDate=> " + binding.txtStartDate.getText().toString()
                + "data?endDate=> " + binding.txtEndDate.getText().toString() + "data?description=> " + binding.etDescription.getText().toString() +
                "data?file=> " + requestImage);

        call.enqueue(new Callback<ApplyLeaveModel>() {

            @Override
            public void onResponse(Call<ApplyLeaveModel> call, retrofit2.Response<ApplyLeaveModel> response) {
                if (response.isSuccessful()) {
                    finish();
                    Toast.makeText(LeaveApplicationActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(LeaveApplicationActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApplyLeaveModel> call, Throwable t) {
                Log.e("fail",t.getMessage());
            }
        });
    }

    public void internetconnection() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getApplyLeave();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_sick:
                type = "sick_leave";
                changeLeaveBackgroundColour("sick");
                break;

            case R.id.ll_casual:
                type = "casual_leave";
                changeLeaveBackgroundColour("casual");
                break;

            case R.id.ll_earn:
                type = "earn_leave";
                changeLeaveBackgroundColour("earn");
                break;

            case R.id.txt_start_date:
                startdatePicker();
                break;

            case R.id.txt_end_date:
                enddatePicker();
                break;

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.btn_apply_leave:
                if (checkValidation()) {
                    getApplyLeave();
                }
                break;

            case R.id.img_add:
                profileBottomLayout();
                break;
        }
    }
}