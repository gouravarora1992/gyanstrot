package com.scholar.engineering.banking.ssc.getset;

import com.scholar.engineering.banking.ssc.Staff.ModelClass.AddItemModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ApplyLeaveModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ClassesListModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.DeptModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.IssueTypeModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.LeaveDetailModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.LeaveGettingModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.PickUpRequestModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.PurposeImageModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.SectionListModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.StaffListModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.StaffLoginModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.StockModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.StudentInfoModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.StudentListModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.SubmitAttendanceModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.SupportFeedbackModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.SupportSystemModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.SupportSystemResponseModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.SystemDeadlineModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ViewAttendanceModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ViewLeavesModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ViewQueryModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ViewRequestModel;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ViewStudentAttendanceListModel;

import org.json.JSONObject;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by surender on 4/30/2017.
*/

public interface RetrofitObjectAPI {

    @POST("homedata")
    Call<HomeBean> getHomeGetsetCall(@Query("email") String email,@Query("admission_no") String admission_no, @Query("index") int index);

    @POST("staff_login")
    Call<StaffLoginModel> staffLogin (@Query("email") String email, @Query("password") String password, @Query("player_id") String player_id);

    @GET("get_department")
    Call<DeptModel> getDepartment();

    @GET("get_stock")
    Call<StockModel> getStock();

    @Multipart
    @POST("purposeimage")
    Call<PurposeImageModel> uploadImage (@Part MultipartBody.Part image);

    @POST("add_item")
    Call<AddItemModel> addItem (@Query("id") Integer id, @Query("item") String item, @Query("justification") String justification,
                                @Query("department") String department, @Query("department_head") String department_head);

    @GET("get_item")
    Call<ViewQueryModel> viewItemQuery();

    @POST("supportsystem")
    Call<SupportSystemModel> getSupportSystem (@Query("staff_id") Integer staff_id);

    @POST("supportdeadline")
    Call<SystemDeadlineModel> getSupportDeadline (@Query("supportsystem_id") Integer supportsystem_id, @Query("deadline") String deadline);

    @POST("supportfeedback")
    Call<SupportFeedbackModel> getSupportFeedback (@Query("supportsystem_id") Integer supportsystem_id, @Query("status") Integer status,
                                                   @Query("feedback") String feedback, @Query("feedbackfile") String feedbackfile);

    @POST("supportsystemresponse")
    Call<SupportSystemResponseModel> supportSystemResponse (@Query("staff_id") Integer staff_id,
                                                            @Query("supportsystem_id") Integer supportsystem_id);

    @Multipart
    @POST("applyleave")
    Call<ApplyLeaveModel> applyLeave (@Query("user_id") Integer user_id, @Query("leave_type") String leave_type, @Query("subject") String subject,
                                      @Query("start_date") String start_date, @Query("end_date") String end_date, @Query("description") String description,
                                      @Part MultipartBody.Part file);

    @POST("applyleave")
    Call<ApplyLeaveModel> applyLeave (@Query("user_id") Integer user_id, @Query("leave_type") String leave_type, @Query("subject") String subject,
                                      @Query("start_date") String start_date, @Query("end_date") String end_date, @Query("description") String description);

    @POST("leavegetting")
    Call<LeaveGettingModel> gettingLeaves (@Query("id") Integer id);

    @POST("viewleaves")
    Call<ViewLeavesModel> viewLeaves (@Query("user_id") Integer user_id);

    @POST("studentinfo")
    Call<StudentInfoModel> getStudentInfo (@Query("std_roll") Integer std_roll);

    @POST("pickuprequest")
    Call<PickUpRequestModel> pickUpRequest (@Query("student_id") Integer student_id, @Query("admission_no") String admission_no,
                                            @Query("name") String name, @Query("section") String section, @Query("class") String student_class,
                                            @Query("file") String file, @Query("description") String description);

    @POST("requestinfo")
    Call<ViewRequestModel> getViewRequest (@Query("admission_no") Integer admission_no);

    @GET("issuedtypes")
    Call<IssueTypeModel> getIssueType();

    @POST("issuedraise")
    Call<PickUpRequestModel> submitRequest (@Query("user_id") Integer user_id, @Query("admission_no") String admission_no, @Query("type") String type,
                                            @Query("name") String name, @Query("section") String section, @Query("class") String student_class,
                                            @Query("file") String file, @Query("reason") String reason);

    @POST("issuedraisebyuser")
    Call<ViewRequestModel> getStaffViewRequest (@Query("user_id") Integer user_id);

    @POST("leavedetail")
    Call<LeaveDetailModel> getLeaveDetail (@Query("id") Integer id);

    @GET("stafflist")
    Call<StaffListModel> getStaffList();

    @POST("studentlist")
    Call<StudentListModel> getStudentList (@Query("Student_class") String Student_class);

    @POST("student_search")
    Call<StudentListModel> getStudentSearch (@Query("Student_class") String Student_class, @Query("search") String search);

    @POST("staff_search")
    Call<StaffListModel> getStaffSearch (@Query("search") String search);

    @POST("submit_attendance")
    Call<SubmitAttendanceModel> getAttendanceSubmit (@Query("class_name") String class_name, @Query("class_section") String class_section,
                                                     @Query("today_date") String today_date, @Query("attendance") JSONObject attendance);

    @GET("getclasses")
    Call<ClassesListModel> getStudentClasses();

    @GET("getsection")
    Call<SectionListModel> getStudentSection();

    @POST("viewattendance_studentlist")
    Call<ViewAttendanceModel> getViewStudentAttendance (@Query("class_name") String class_name, @Query("class_section") String class_section,
                                                     @Query("date") String date);

    @POST("viewattendance_student")
    Call<ViewStudentAttendanceListModel> getAttendance (@Query("class_name") String class_name, @Query("class_section") String class_section,
                                                        @Query("from") String from, @Query("to") String to, @Query("student_id") String student_id);

}
