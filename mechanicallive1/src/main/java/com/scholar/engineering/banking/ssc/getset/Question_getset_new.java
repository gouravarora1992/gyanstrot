package com.scholar.engineering.banking.ssc.getset;

/**
 * Created by surender on 4/3/2017.
**/

public class Question_getset_new {

    int id;
    String testid,sub_name,question_img,user_ans;
    String hint,question,optiona,optionb,optionc,optiond,optione,answer,solution;
    String hint_img,optiona_img,optionb_img,optionc_img,optiond_img,optione_img,solution_img;
    String ans_a,ans_b,ans_c,ans_d,ans_e;

        public Question_getset_new(int id,String testid, String sub_name,String hint, String question, String optiona,
                               String optionb, String optionc, String optiond, String optione,
                               String answer, String solution, String hint_img,String question_img, String optiona_img,
                               String optionb_img, String optionc_img, String optiond_img,
                               String optione_img, String solution_img, String user_ans,
                               String ans_a,String ans_b,String ans_c,String ans_d,String ans_e) {

        this.id=id;
        this.testid = testid;
        this.sub_name = sub_name;
        this.hint = hint;
        this.question = question;
        this.optiona = optiona;
        this.optionb = optionb;
        this.optionc = optionc;
        this.optiond = optiond;
        this.optione = optione;
        this.answer = answer;
        this.solution = solution;
        this.hint_img = hint_img;
        this.question_img=question_img;
        this.optiona_img = optiona_img;
        this.optionb_img = optionb_img;
        this.optionc_img = optionc_img;
        this.optiond_img = optiond_img;
        this.optione_img = optione_img;
        this.solution_img = solution_img;
        this.user_ans=user_ans;
        this.ans_a=ans_a;
        this.ans_b=ans_b;
        this.ans_c=ans_c;
        this.ans_d=ans_d;
        this.ans_e=ans_e;

    }

    public String getAns_a() {
        return ans_a;
    }

    public void setAns_a(String ans_a) {
        this.ans_a = ans_a;
    }

    public String getAns_b() {
        return ans_b;
    }

    public void setAns_b(String ans_b) {
        this.ans_b = ans_b;
    }

    public String getAns_c() {
        return ans_c;
    }

    public void setAns_c(String ans_c) {
        this.ans_c = ans_c;
    }

    public String getAns_d() {
        return ans_d;
    }

    public void setAns_d(String ans_d) {
        this.ans_d = ans_d;
    }

    public String getAns_e() {
        return ans_e;
    }

    public void setAns_e(String ans_e) {
        this.ans_e = ans_e;
    }

    public String getUser_ans() {
        return user_ans;
    }

    public void setUser_ans(String user_ans) {
        this.user_ans = user_ans;
    }

    public String getQuestion_img() {
        return question_img;
    }

    public void setQuestion_img(String question_img) {
        this.question_img = question_img;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTestid() {
        return testid;
    }

    public void setTestid(String testid) {
        this.testid = testid;
    }

    public String getSub_name() {
        return sub_name;
    }

    public void setSub_name(String sub_name) {
        this.sub_name = sub_name;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOptiona() {
        return optiona;
    }

    public void setOptiona(String optiona) {
        this.optiona = optiona;
    }

    public String getOptionb() {
        return optionb;
    }

    public void setOptionb(String optionb) {
        this.optionb = optionb;
    }

    public String getOptionc() {
        return optionc;
    }

    public void setOptionc(String optionc) {
        this.optionc = optionc;
    }

    public String getOptiond() {
        return optiond;
    }

    public void setOptiond(String optiond) {
        this.optiond = optiond;
    }

    public String getOptione() {
        return optione;
    }

    public void setOptione(String optione) {
        this.optione = optione;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public String getHint_img() {
        return hint_img;
    }

    public void setHint_img(String hint_img) {
        this.hint_img = hint_img;
    }

    public String getOptiona_img() {
        return optiona_img;
    }

    public void setOptiona_img(String optiona_img) {
        this.optiona_img = optiona_img;
    }

    public String getOptionb_img() {
        return optionb_img;
    }

    public void setOptionb_img(String optionb_img) {
        this.optionb_img = optionb_img;
    }

    public String getOptionc_img() {
        return optionc_img;
    }

    public void setOptionc_img(String optionc_img) {
        this.optionc_img = optionc_img;
    }

    public String getOptiond_img() {
        return optiond_img;
    }

    public void setOptiond_img(String optiond_img) {
        this.optiond_img = optiond_img;
    }

    public String getOptione_img() {
        return optione_img;
    }

    public void setOptione_img(String optione_img) {
        this.optione_img = optione_img;
    }

    public String getSolution_img() {
        return solution_img;
    }

    public void setSolution_img(String solution_img) {
        this.solution_img = solution_img;
    }

}
