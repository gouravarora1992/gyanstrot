package com.scholar.engineering.banking.ssc;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;

import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.util.Base64;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
//import com.androidquery.AQuery;
import com.scholar.engineering.banking.ssc.adapter.Replies_on_group_comment_adap;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.newScreens.SplashScreen;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.ImageCompression;
import com.scholar.engineering.banking.ssc.utils.ImageUploading;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;
import static com.scholar.engineering.banking.ssc.utils.Constants.st_base46;
import static com.scholar.engineering.banking.ssc.utils.Constants.imageFilePath;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class News_Webview extends AppCompatActivity {
    Context c;
    Intent detail1;
    String title, comment, image, date, username, collag, imgurl, newsurl;
    RecyclerView list;
    TextView tv_commentcount, tv_like, tv_share;
    //AQuery aquery;
    String url, email;
    //ProgressDialog dialog;
    private SharedPreferences pref;
    LinearLayout layout;
    WebView wv;
    ProgressBar bar;
    EditText et_comment;
    Replies_on_group_comment_adap adap;
    int postid, commentcount = 0, likecount = 0, pagecount = 0, index = 0;
    ImageView postcom, camera_img, iv_like;
    Home_getset data;
    LinearLayout lay_commentpost, lay_like, lay_comment, lay_share;
    String st_imagename, posttype = "1", likestatus = "";
    Uri selectedImage;
    Bitmap bitmap;
    NetworkConnection nw;
    TextView moreButton;
    RelativeLayout loadmore_lay;
    JSONArray jsonArray;
    JSONObject jsonobj;
    Typeface font_demi, font_medium;
    public ProgressDialog progress;
    NestedScrollView nestedScrollView;
    private final int REQUEST_CODE_CLICK_IMAGE = 1002, REQUEST_CODE_GALLERY_IMAGE = 1003;
    ImageUploading ab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_webview);

        st_base46 = "";

        Logg("newswebview", "news");
        nw = new NetworkConnection(this);

        font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");
        font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");
        wv = (WebView) findViewById(R.id.webViewid);
//		layout=(LinearLayout)findViewById(R.id.ly_layout_id);

        loadmore_lay = (RelativeLayout) findViewById(R.id.footer_layout);
        moreButton = (TextView) findViewById(R.id.moreButton);
        tv_share = (TextView) findViewById(R.id.tv_shareid);
        tv_commentcount = (TextView) findViewById(R.id.tv_commentid);
        tv_like = (TextView) findViewById(R.id.tv_like);
        tv_share.setTypeface(font_medium);
        tv_commentcount.setTypeface(font_medium);
        tv_like.setTypeface(font_medium);
        et_comment = (EditText) findViewById(R.id.comtextid);
        postcom = (ImageView) findViewById(R.id.postcomid);
        iv_like = (ImageView) findViewById(R.id.likeimageid);
        camera_img = (ImageView) findViewById(R.id.iv_camera_id);
        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedscroll);

        lay_comment = (LinearLayout) findViewById(R.id.lay_comment);
        lay_like = (LinearLayout) findViewById(R.id.lay_like);
        lay_share = (LinearLayout) findViewById(R.id.lay_shareid);
        lay_commentpost = (LinearLayout) findViewById(R.id.lay_commentpost);

        list = (RecyclerView) findViewById(R.id.recyclerView);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(News_Webview.this);
        mLayoutManager.scrollToPositionWithOffset(0, 0);
        list.setLayoutManager(mLayoutManager);

        list.setHasFixedSize(true);
        list.setNestedScrollingEnabled(false);

        bar = (ProgressBar) findViewById(R.id.progressBar1);

        jsonArray = new JSONArray();

        detail1 = getIntent();

        postid = detail1.getIntExtra("postid", 0);

//		Replies.psid=postid;


        pref = getSharedPreferences("myref", 0);
        username = pref.getString("name", "");
        collag = pref.getString("collage", "");
        imgurl = pref.getString("image", "");
        email = User_Email;

        //Logg("like_comment",likecount+" "+commentcount);

        wv.setWebViewClient(new Callback());
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setBuiltInZoomControls(true);


        if (Build.VERSION.SDK_INT >= 24) {
            try {
                java.lang.reflect.Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {

            }
        }

        get_Post(postid);

        postcom.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (pref.getBoolean("boolean", false)) {

                    if (et_comment.getText().length() > 0) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(et_comment.getWindowToken(), 0);

                        if (st_base46.length() > 1)
                            imagevolley();
                        else
                            volleycominsert();

                    } else
                        Toast.makeText(getApplicationContext(), "Please enter comment", Toast.LENGTH_SHORT).show();
                } else {
                    Intent in = new Intent(News_Webview.this, SplashScreen.class);
                    startActivity(in);
                    finish();
                }
            }
        });

        camera_img.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                ab.showOptionBottomSheetDialog();
//				SelectImage();

            }
        });

        lay_like.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!nw.isConnectingToInternet()) {
                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
                    return;
                }
                volleylike_post();
            }
        });

        lay_comment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (lay_commentpost.getVisibility() == View.VISIBLE) {
                    lay_commentpost.setVisibility(View.GONE);
                } else {
                    lay_commentpost.setVisibility(View.VISIBLE);
                    nestedScrollView.fullScroll(NestedScrollView.FOCUS_DOWN);
                }

            }
        });

        lay_share.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                Logg("share", "click News_Webview");

                JSONObject shareobj = new JSONObject();
                try {
                    if (jsonobj != null) {
                        String value = "";
                        String[] valuearr;

                        JSONObject jsondata = new JSONObject(jsonobj.getString("jsondata"));

                        value = String.valueOf(Html.fromHtml(String.valueOf(Html.fromHtml(jsonobj.getString("post_description")))));
                        valuearr = value.split(System.lineSeparator(), 2);
                        value = valuearr[0];

                        shareobj.put("class", "News_Webview");
                        if (jsondata.has("comments"))
                            shareobj.put("title", jsondata.getString("comments"));
                        else
                            shareobj.put("title", jsondata.getString("title"));
                        shareobj.put("description", value);
                        if (jsondata.has("pic"))
                            shareobj.put("image", Constants.URL + "newsimage/" + jsondata.getString("pic"));
                        else if (jsondata.has("com_image"))
                            shareobj.put("image", jsondata.getString("com_image"));

                        shareobj.put("id", jsonobj.getInt("id"));

                        Utility.shareIntent(News_Webview.this, shareobj);
                    }

                } catch (Exception e) {
                }
            }
        });

        moreButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if (jsonArray.length() > 0) {

                        JSONObject obj = jsonArray.getJSONObject(0);

                        index = obj.getInt("id");

                    } else {
                        index = 0;

//						JSONObject obj = jsonArray.getJSONObject(jsonArray.length() - 1);
                    }

                    volleylist(postid);

                } catch (JSONException e) {

                }
            }
        });

        ab = new ImageUploading(this, camera_img);
        ab.createBottomSheetDialog();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logg("call_onDestroy()", "Call onDestroy()");
        deleteCache(this);
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public void setComment(int com) {

        tv_commentcount.setText("Comments (" + com + ")");

        if (com < 10) {
        } else if (com < 100) {
            tv_commentcount.setTextSize(12f);
        } else {
            tv_commentcount.setTextSize(11f);
        }
    }

    public void setLike(int lk) {
        if (lk > 0) {

            tv_like.setText(getResources().getString(R.string.like) + " (" + lk + ")");
        } else {
            tv_like.setText(getResources().getString(R.string.like));
        }
//		if(lk<10)
//		{}
//		else if(lk<100){
//			tv_like.setTextSize(12f);
//		}
//		else{
//			tv_like.setTextSize(11f);
//		}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (wv.canGoBack()) {
                        wv.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    private class Callback extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return (false);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            //Logg("onpage_started",url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            //Logg("onpage_finished",url);

            bar.setVisibility(View.GONE);
            if (pagecount < 1) {
                pagecount++;
                volleylist(postid);
            }

        }
    }

    @Override
    public void onBackPressed() {

        if (Constants.Check_Branch_IO) {
            Intent in = new Intent(News_Webview.this, HomeActivity.class);
            startActivity(in);
            finish();
        } else
            super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_CLICK_IMAGE) {

            new ImageCompression(camera_img).execute(imageFilePath);

        } else if (requestCode == REQUEST_CODE_GALLERY_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

//            String[] projection = {MediaStore.Images.Media.DATA};
//            Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
//            cursor.moveToFirst();
//            int columnIndex = cursor.getColumnIndex(projection[0]);
            File myFile = new File(uri.getPath());

            final String picturePath = myFile.getAbsolutePath();//cursor.getString(columnIndex); // returns null
            // cursor.close();

            //copy the selected file of gallery into app's sdcard folder and perform the compression operations on it.
            //And override the original image with the newly resized image.

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        CommonUtils.copyFile(picturePath, imageFilePath);
                    } catch (IOException e) {

                    }
                }
            });

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bmp = BitmapFactory.decodeFile(imageFilePath, options);

            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;

            Logg("actualheight2", actualHeight + " " + actualWidth);

            if (actualWidth > 600 | actualHeight > 600) {
                new ImageCompression(camera_img).execute(imageFilePath);
            } else {

                getContentResolver().notifyChange(uri, null);
                ContentResolver cr = getContentResolver();
                Bitmap bitmap;

                try {
                    bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, uri);
                    //Logg("bitmap_h_w",bitmap.getHeight()+" "+bitmap.getWidth()+"");
                    camera_img.setImageBitmap(bitmap);

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();

                    st_base46 = Base64.encodeToString(byteArray, Base64.DEFAULT);
//                    return Base64.encodeToString(byteArray, Base64.DEFAULT);

                    Logg("base64", st_base46);

                } catch (Exception e) {

                }
            }
        }
    }


    public void volleylike_post() {

        progress = ProgressDialog.show(this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//		progress.show();

        RequestQueue queue = Volley.newRequestQueue(News_Webview.this);
        JSONObject json = new JSONObject();
        try {
            json.put("id", postid);
            json.put("email", Constants.User_Email);
            json.put("admission_no", Constants.addmissionno);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Constants.URL_LV + "like_post";
        Logg("likeapi", url + " , " + json.toString());
        JsonObjectRequest jsonreq = new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                // TODO Auto-generated method stub

                try {

                    if (res.getString("scalar").equals("like")) {
//						Toast.makeText(News_Webview.this,"success",Toast.LENGTH_SHORT).show();

                        likecount++;
                        setLike(likecount);

                        iv_like.setImageResource(R.drawable.ic_bulb_filled);
                        tv_like.setTextColor(getResources().getColor(R.color.like_text_color));

                    } else if (res.getString("scalar").equals("dislike")) {

                        iv_like.setImageResource(R.drawable.ic_bulb_lightup);
                        tv_like.setTextColor(getResources().getColor(R.color.text_lightgray));

//						Toast.makeText(News_Webview.this,"success",Toast.LENGTH_SHORT).show();

                        likecount--;
                        setLike(likecount);

//						tv_like.setText(" Light Up ("+likecount+")");

                    } else {
                        Toast.makeText(News_Webview.this, "Not done", Toast.LENGTH_SHORT).show();
                    }

                    progress.dismiss();

                } catch (JSONException e) {
                    progress.dismiss();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg) {
                //Logg("error", String.valueOf(arg));
                progress.dismiss();
                Toast.makeText(News_Webview.this, "Network Problem", Toast.LENGTH_SHORT).show();
            }

        });

        queue.add(jsonreq);
    }

    public void volleylist(int postid) {

        if (!nw.isConnectingToInternet()) {
            internetconnection(1);
            return;
        }


        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        JSONObject obj = new JSONObject();
        try {
            obj.put("postid", postid);
            obj.put("index", index);
            obj.put("email", Constants.User_Email);
            obj.put("admission_no", Constants.addmissionno);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Constants.URL_LV + "get_common_comment";
        Logg("url list", url);

        JsonObjectRequest json = new JsonObjectRequest(Method.POST, url, obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                // TODO Auto-generated method stub

                //Logg("respon",String.valueOf(res));
                try {

                    JSONArray jr = res.getJSONArray("data");

                    if (jr.length() >= 10)
                        loadmore_lay.setVisibility(View.VISIBLE);
                    else
                        loadmore_lay.setVisibility(View.GONE);

                    if (index == 0) {
                        jsonArray = new JSONArray(new ArrayList<String>());
                        jsonArray = jr;
                        adap = new Replies_on_group_comment_adap(News_Webview.this, jsonArray);
                        list.setAdapter(adap);
                        //nestedscroll.fullScroll(View.FOCUS_DOWN);
                    } else {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            jr.put(jsonArray.getJSONObject(i)); //new JSONArray(new ArrayList<>());
                        }
                        adap = new Replies_on_group_comment_adap(News_Webview.this, jr);
                        list.setAdapter(adap);
                        //nestedscroll.fullScroll(View.FOCUS_DOWN);
                    }

//					dialog.dismiss();

                } catch (JSONException e) {
                    loadmore_lay.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                // TODO Auto-generated method stub
                //Logg("error", String.valueOf(arg0));
                loadmore_lay.setVisibility(View.GONE);
//				dialog.dismiss();
            }
        });

        queue.add(json);

    }

    public void imagevolley() {

        if (!nw.isConnectingToInternet()) {
            internetconnection(3);
            return;
        }

        progress = ProgressDialog.show(this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        progress.show();

        st_imagename = String.valueOf(System.currentTimeMillis());

        comment = et_comment.getText().toString();
        RequestQueue queue = Volley.newRequestQueue(News_Webview.this);
        String url = Constants.URL_LV + "insert_common_comment_image";
        Logg("newswebview", url);

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                //Logg("response", s);
                try {
                    if (s.length() > 0) {
                        JSONObject obj = new JSONObject(s);
                        //s=obj.getString("status");

                        if (obj.getString("status").equalsIgnoreCase("success")) {

                            Toast.makeText(News_Webview.this, "Upload Successfully", Toast.LENGTH_LONG).show();

                            et_comment.setText("");

                            st_base46 = "";

                            camera_img.setImageResource(R.drawable.camera40);

                            index = 0;

                            commentcount++;

                            setComment(commentcount);

                            jsonArray = new JSONArray(new ArrayList<String>());
                            volleylist(postid);

                        } else if (obj.getString("status").equalsIgnoreCase("block"))
                            Toast.makeText(News_Webview.this, obj.getString("response"), Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(News_Webview.this, "Something is wrong can't post comment", Toast.LENGTH_LONG).show();

//						if(s.equalsIgnoreCase("success")==true)
//						{
//							Toast.makeText(News_Webview.this,"Upload Successfully",Toast.LENGTH_LONG).show();
//
//							et_comment.setText("");
//
//							st_base46="";
//
//							camera_img.setImageResource(R.drawable.camera40);
//
//							index=0;
//
//							commentcount++;
//
//							setComment(commentcount);
//
//							jsonArray=new JSONArray(new ArrayList<String>());
//							volleylist(postid);
//
//						}
//						else
//						{
//							Toast.makeText(News_Webview.this,"Somthing is Wrong Can't Upload, Please Try Againe",Toast.LENGTH_LONG).show();
//						}


                        progress.dismiss();
                        //Logg("response_string",s+" response");
                    }
                } catch (Exception e) {
                    progress.dismiss();
                    //Logg("e","e",e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(News_Webview.this, CommonUtils.volleyerror(volleyError));
                progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                //id="+psid+"&comment="+cmnt+"&user="+username+"&collage="+collage+"&image="+image+"&email="+mail;

                Map<String, String> params = new HashMap<String, String>();
                params.put("st_base64", st_base46);
                params.put("imagename", st_imagename);
                params.put("postid", postid + "");
                params.put("posttype", "1");
                params.put("email", email);
                params.put("comment", comment.replaceAll(" ", "%20"));
                params.put("admission_no", Constants.addmissionno);
                return params;
            }
        };

        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void volleycominsert() {
        // TODO Auto-generated method stub

        if (!nw.isConnectingToInternet()) {
//			swipeRefreshLayout.setRefreshing(false);
            internetconnection(2);
            return;
        }

        progress = ProgressDialog.show(this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progress.show();

        String url = Constants.URL_LV + "insert_common_comment";
        Logg("insert_common_comment", url);

        comment = et_comment.getText().toString();
        comment = comment.replaceAll(" ", "%20");

        RequestQueue que = Volley.newRequestQueue(getApplicationContext());

        StringRequest obj1 = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String res) {
                // TODO Auto-generated method stub
                //Logg("response",res);
                int size = res.length();
                if (size > 0) {

                    try {
                        JSONObject obj = new JSONObject(res);

                        if (obj.getString("status").equalsIgnoreCase("success")) {
                            et_comment.setText("");
                            Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
                            index = 0;
                            commentcount++;
                            setComment(commentcount);
                            jsonArray = new JSONArray(new ArrayList<String>());
                            volleylist(postid);

                        } else if (obj.getString("status").equalsIgnoreCase("block"))
                            Toast.makeText(News_Webview.this, obj.getString("response"), Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(News_Webview.this, "Something is wrong can't post comment", Toast.LENGTH_LONG).show();

//						if(obj.getString("status").equals("success")==true)
//						{
//							et_comment.setText("");
//							Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_SHORT).show();
//							index=0;
//							commentcount++;
//							setComment(commentcount);
//							jsonArray=new JSONArray(new ArrayList<String>());
//							volleylist(postid);
//						}
//						else {
//							Toast.makeText(getApplicationContext(),"Somthing is wrong, Can't insert comment",Toast.LENGTH_SHORT).show();
//						}

                        progress.dismiss();

                    } catch (JSONException e) {
                        progress.dismiss();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                // TODO Auto-generated method stub
                progress.dismiss();
                //Logg("error",e.toString());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("postid", postid + "");
                params.put("posttype", "1");
                params.put("comment", comment);
                params.put("email", email);
                params.put("admission_no", Constants.addmissionno);
                return params;
            }
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String,String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json; charset=utf-8");
//
//                return headers;
//            }
        };

        int socketTimeout = 0;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        obj1.setRetryPolicy(policy);

        que.add(obj1);

    }

    public void get_Post(int postid) {

        if (!nw.isConnectingToInternet()) {
            //swipeRefreshLayout.setRefreshing(false);
            internetconnection(0);
            return;
        }

        progress = ProgressDialog.show(this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
//		progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progress.show();

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        JSONObject obj = new JSONObject();

        String url = Constants.URL + "newapi2_04/getPost.php?postid=" + postid + "&email=" + email;
        Logg("url list", url);

        JsonObjectRequest json = new JsonObjectRequest(Method.POST, url, obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                // TODO Auto-generated method stub

                //Logg("respon",String.valueOf(res));

                try {

                    JSONArray jr = res.getJSONArray("data");

                    jsonobj = jr.getJSONObject(0);

                    JSONObject jsondata = new JSONObject(jsonobj.getString("jsondata"));

                    likecount = jsonobj.getInt("likes");
                    commentcount = jsonobj.getInt("comment");
                    newsurl = jsonobj.getString("posturl");
                    likestatus = jsonobj.getString("likestatus");

                    setLike(likecount);
                    setComment(commentcount);

                    if (likestatus.equals("like")) {
                        iv_like.setImageResource(R.drawable.ic_bulb_filled);
                        tv_like.setTextColor(getResources().getColor(R.color.like_text_color));
                    } else {
                        iv_like.setImageResource(R.drawable.ic_bulb_lightup);
                        tv_like.setTextColor(getResources().getColor(R.color.text_lightgray));
                    }

                    if (jsondata.has("add_comment")) {
                        if (jsondata.getString("add_comment").equalsIgnoreCase("off")) {

                            lay_comment.setVisibility(View.GONE);
                            lay_like.setGravity(Gravity.CENTER);
                            lay_share.setGravity(Gravity.CENTER);

                        }
                    }

                    wv.loadUrl(newsurl);
                    progress.dismiss();
                } catch (JSONException e) {
                    progress.dismiss();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                // TODO Auto-generated method stub
                //Logg("error", String.valueOf(arg0));
                progress.dismiss();

            }
        });

        queue.add(json);

    }

    public void internetconnection(final int i) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);

        if (isFinishing())
            return;

        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                if (i == 0)
                    get_Post(postid);
                else if (i == 1)
                    volleylist(postid);
                else if (i == 2)
                    volleycominsert();
                else if (i == 3)
                    imagevolley();

//				getData();

            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

}