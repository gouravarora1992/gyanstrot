package com.scholar.engineering.banking.ssc.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Topic_Search_Data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by surender on 3/14/2017.
 */

public class Topic_Search_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context c;
    JSONArray data;

    JSONObject object;

    public void addItem(JSONArray data) {
        for (int i = 0; i < data.length(); i++) {
            try {
                this.data.put(data.getJSONObject(i));
            } catch (JSONException e) {

            }
        }
    }

    public Topic_Search_Adapter(Context c, JSONArray data) {
        this.c = c;
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View v0 = inflater.inflate(R.layout.topics_search_item, viewGroup, false);

        viewHolder = new ViewHolder0(v0);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        ViewHolder0 vh0 = (ViewHolder0) viewHolder;

        setComment(vh0, position);
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    private void setComment(final ViewHolder0 vh0, final int p) {

        try {

            Typeface font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");
            vh0.tv_topicname.setTypeface(font_demi);
            Typeface font_meduim = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");
            vh0.tv_testcount.setTypeface(font_meduim);
            vh0.tv_postcount.setTypeface(font_meduim);
            object = data.getJSONObject(p);
            vh0.tv_topicname.setText(object.getString("topic_name"));
            vh0.tv_testcount.setText(object.getString("tests") + " Tests");
            vh0.tv_postcount.setText(object.getString("postcount") + " Posts");

            vh0.lay_test.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        object = data.getJSONObject(p);

                        Intent in = new Intent(c, Topic_Search_Data.class);
                        in.putExtra("topic", object.getString("topic_name"));
                        in.putExtra("fieldtype", "2");
                        c.startActivity(in);

                    } catch (JSONException e) {

                    }
                }
            });

            vh0.lay_post.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        object = data.getJSONObject(p);

                        Intent in = new Intent(c, Topic_Search_Data.class);
                        in.putExtra("topic", object.getString("topic_name"));
                        in.putExtra("fieldtype", "1");
                        c.startActivity(in);

                    } catch (JSONException e) {

                    }
                }
            });


        } catch (JSONException e) {

        }

    }

    public class ViewHolder0 extends RecyclerView.ViewHolder {

        //  **************************  Set Group Comment  ********************

        TextView tv_topicname;
        TextView tv_testcount;
        TextView tv_postcount;
        LinearLayout layout, lay_test, lay_post;

        public ViewHolder0(View b) {
            super(b);

            tv_topicname = (TextView) b.findViewById(R.id.tv_topicname);

            tv_testcount = (TextView) b.findViewById(R.id.tv_testcount);

            tv_postcount = (TextView) b.findViewById(R.id.tv_postcount);

            layout = (LinearLayout) b.findViewById(R.id.layout);

            lay_test = (LinearLayout) b.findViewById(R.id.lay_testimageview);
            lay_post = (LinearLayout) b.findViewById(R.id.lay_postimageview);

        }
    }

}
