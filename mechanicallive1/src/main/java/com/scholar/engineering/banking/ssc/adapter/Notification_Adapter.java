package com.scholar.engineering.banking.ssc.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.R;
//import com.scholar.engineering.banking.ssc.Replies;
import com.scholar.engineering.banking.ssc.Replies_Common_Comment_homepage;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by surender on 3/14/2017.
 */

public class Notification_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context c;
    JSONArray data;

    JSONObject object,jsondata;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    ImageLoader imageLoader;

    public Notification_Adapter(Context c, JSONArray data) {

        this.c = c;
        this.data=data;
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.user_default)
                .showImageForEmptyUri(R.drawable.user_default)
                .showImageOnFail(R.drawable.user_default)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(c));

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

                View v0 = inflater.inflate(R.layout.notification_list_item, viewGroup, false);

                viewHolder = new ViewHolder0(v0);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

                ViewHolder0 vh0 = (ViewHolder0) viewHolder;

                setComment(vh0, position);
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    private void setComment(final ViewHolder0 vh0, final int p) {

        try {
            Typeface font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");
            Typeface font_medium = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");
            object=data.getJSONObject(p);
            jsondata=new JSONObject(object.getString("jsondata"));
            vh0.tv_notification.setText(Html.fromHtml(jsondata.getString("text").replace("%20"," ")));
            vh0.tv_notification.setTypeface(font_medium);
            if(object.getString("status").equalsIgnoreCase("unread"))
            {
                //Logg("notification_status","unread");
                vh0.cardView.setCardBackgroundColor(Color.parseColor("#e7e7e7"));
            }
            if(jsondata.has("image")) {
                if(!jsondata.getString("image").equalsIgnoreCase("null"))
                    imageLoader.getInstance().displayImage(jsondata.getString("image"), vh0.iv_user, options, animateFirstListener);
            }

            vh0.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        object=data.getJSONObject(p);
                        jsondata=new JSONObject(object.getString("jsondata"));

                            String cls = jsondata.getString("class");
                               //Logg("class_name",cls+" class");
                                if(cls.equalsIgnoreCase("Replies_Common_Comment_homepage")){
                                    Intent intent = new Intent(c, Replies_Common_Comment_homepage.class);
                                    intent.putExtra("commentid",jsondata.getString("com_id"));
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    c.startActivity(intent);
                                }
                                // change class
                            if(cls.equalsIgnoreCase("Replies")){
//                                Intent intent = new Intent(c, Replies.class);
//                                intent.putExtra("psid",Integer.valueOf(jsondata.getString("com_id")));
//                                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                c.startActivity(intent);
                            }
//                            Log.d("OneSignalExample 1", "customkey set with value: " + cls);

                        else
                        {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        //Logg("class_exp","e",e);
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public class ViewHolder0 extends RecyclerView.ViewHolder {

        //  **************************  Set Group Comment  ********************

        TextView tv_notification;
        ImageView iv_user;
        LinearLayout layout;
        CardView cardView;

        public ViewHolder0(View b) {
            super(b);

            tv_notification=(TextView)b.findViewById(R.id.tv_notification);
            iv_user=(ImageView)b.findViewById(R.id.iv_user);
            layout=(LinearLayout)b.findViewById(R.id.layout);
            cardView=(CardView)b.findViewById(R.id.card_view);

        }
    }

}
