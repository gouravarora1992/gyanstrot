package com.scholar.engineering.banking.ssc;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.scholar.engineering.banking.ssc.adapter.Replies_on_group_comment_adap;
import com.scholar.engineering.banking.ssc.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;

public class User_Document_list extends AppCompatActivity{

	TextView name,time;
	String email;
	int psid,index=0;
	RecyclerView replist;
	Replies_on_group_comment_adap adap;
	LinearLayoutManager mLayoutManager;
	RelativeLayout loadmore_lay;
	TextView moreButton;
	JSONArray jsonArray;
	NestedScrollView nestedscroll;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.common_comment_homepage);

//		Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
//		setSupportActionBar(toolbar);
//		getSupportActionBar().setDisplayShowHomeEnabled(true);
//		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		nestedscroll=(NestedScrollView)findViewById(R.id.nestedscroll);
		replist=(RecyclerView) findViewById(R.id.recyclerView);
		mLayoutManager = new LinearLayoutManager(User_Document_list.this);
//		mLayoutManager.scrollToPositionWithOffset(0,0);
//		mLayoutManager.setReverseLayout(true);
		replist.setLayoutManager(mLayoutManager);
		replist.setNestedScrollingEnabled(false);
		replist.setHasFixedSize(true);

		moreButton=(TextView)findViewById(R.id.moreButton);
		loadmore_lay=(RelativeLayout)findViewById(R.id.footer_layout);

		jsonArray=new JSONArray();
		email=User_Email;

		volleylist();

		moreButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

				try {

					if(jsonArray.length()>0) {

						JSONObject obj = jsonArray.getJSONObject(0);

						index=obj.getInt("id");
					}
					else{
						index=0;
//						JSONObject obj = jsonArray.getJSONObject(jsonArray.length() - 1);
					}

					volleylist();

				} catch (JSONException e) {

				}
			}
		});

	}

	public void volleylist()
	{

		final ProgressDialog dialog = new ProgressDialog(User_Document_list.this);
		// dialog.setMessage("Loading..Please wait.");
		dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
		dialog.setCanceledOnTouchOutside(false);
//		    coments=new ArrayList<String>();
//		    names=new ArrayList<String>();
		dialog.show();

		RequestQueue queue=Volley.newRequestQueue(getApplicationContext());
		JSONObject obj=new JSONObject();

		try {
			obj.put("postid", psid);
			obj.put("index", index);
			obj.put("email", Constants.User_Email);
			obj.put("admission_no", Constants.addmissionno);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		String url = Constants.URL_LV + "get_common_comment";
		Logg("url list",url);

		JsonObjectRequest json=new JsonObjectRequest(Method.POST, url, obj,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res) {
				// TODO Auto-generated method stub

				//Logg("respon",String.valueOf(res));
				try {

					if ((dialog != null) && dialog.isShowing()) {
						dialog.dismiss();
					}

					dialog.dismiss();
					JSONArray jr=res.getJSONArray("data");

					if(jr.length()>=10)
						loadmore_lay.setVisibility(View.VISIBLE);
					else
						loadmore_lay.setVisibility(View.GONE);

					for(int i=0;i<jsonArray.length();i++)
					{

						JSONObject jsonobj=jsonArray.getJSONObject(i);
						jr.put(jsonobj);

					}

					jsonArray=jr;//new JSONArray(new ArrayList<>());

							adap=new Replies_on_group_comment_adap(User_Document_list.this,jsonArray);
					replist.setAdapter(adap);
					nestedscroll.fullScroll(View.FOCUS_DOWN);
//					mLayoutManager.setReverseLayout(true);

					//Logg("array_size",jsonArray.length()+"");

//					if(index==0)
//						replist.smoothScrollToPosition(jsonArray.length()-1);

				}
				catch (JSONException e) {
					loadmore_lay.setVisibility(View.GONE);
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				//Logg("error", String.valueOf(arg0));
				loadmore_lay.setVisibility(View.GONE);
			}
		});

		queue.add(json);

//		  int socketTimeout = 20000;//30 seconds - change to what you want
//		  RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//		  json.setRetryPolicy(policy);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(item.getItemId()== android.R.id.home)
			finish();
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
			super.onBackPressed();
	}

}
