package com.scholar.engineering.banking.ssc.Staff.ModelClass;

public class RequestLeaveModel {

    private String txt_reason;
    private String txt_date;
    private String txt_month;

    public RequestLeaveModel(String txt_reason, String txt_date, String txt_month) {
        this.txt_reason = txt_reason;
        this.txt_date = txt_date;
        this.txt_month = txt_month;
    }

    public String getTxt_reason() {
        return txt_reason;
    }

    public void setTxt_reason(String txt_reason) {
        this.txt_reason = txt_reason;
    }

    public String getTxt_date() {
        return txt_date;
    }

    public void setTxt_date(String txt_date) {
        this.txt_date = txt_date;
    }

    public String getTxt_month() {
        return txt_month;
    }

    public void setTxt_month(String txt_month) {
        this.txt_month = txt_month;
    }
}
