package com.scholar.engineering.banking.ssc.Staff.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.LeaveDetailActivity;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ViewLeavesModel;
import com.scholar.engineering.banking.ssc.databinding.ItemLeaveRequestBinding;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class LeaveRequestAdapter extends RecyclerView.Adapter<LeaveRequestAdapter.ViewHolder> {

    private Context context;
    private List<ViewLeavesModel.Datum> viewLeaves;

    public LeaveRequestAdapter(Context context, List<ViewLeavesModel.Datum> viewLeaves) {
        this.context = context;
        this.viewLeaves = viewLeaves;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemLeaveRequestBinding binding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),(R.layout.item_leave_request),parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        holder.binding.txtLeaveType.setText(viewLeaves.get(position).getLeaveType());
        holder.binding.txtStartDate.setText(viewLeaves.get(position).getStartDate());
        holder.binding.txtEndDate.setText(viewLeaves.get(position).getEndDate());
        holder.binding.txtReason.setText(viewLeaves.get(position).getDescription());
        if (viewLeaves.get(position).getStatus()==0){
            holder.binding.txtStatus.setBackgroundResource(R.drawable.background_cr_transparent);
            holder.binding.txtStatus.setText("pending");
        } else if (viewLeaves.get(position).getStatus()==1){
            holder.binding.txtStatus.setBackgroundResource(R.drawable.bakground_green);
            holder.binding.txtStatus.setText("approved");
        } else if (viewLeaves.get(position).getStatus()==2){
            holder.binding.txtStatus.setBackgroundResource(R.drawable.background_orange);
            holder.binding.txtStatus.setText("cancelled");
        }

        try {
            String date1 = viewLeaves.get(position).getCreatedAt();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = null;
            try {
                date = dateFormat.parse(date1);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            DateFormat formatterDay = new SimpleDateFormat("dd");
            DateFormat formatterMonth = new SimpleDateFormat("MMM");
            String dateDay = formatterDay.format(date);
            String dateMonth = formatterMonth.format(date);
            holder.binding.txtDate.setText(dateDay);
            holder.binding.txtMonth.setText(dateMonth);
        } catch (Exception e){
            e.printStackTrace();
        }

        holder.binding.imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, LeaveDetailActivity.class)
                        .putExtra("calledFrom",LeaveRequestAdapter.class.getSimpleName())
                        .putExtra("id",String.valueOf(viewLeaves.get(position).getId())));
            }
        });

    }

    @Override
    public int getItemCount() {
        return viewLeaves.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public ItemLeaveRequestBinding binding;

        public ViewHolder(@NonNull ItemLeaveRequestBinding binding) {
            super(binding.getRoot());
            this.binding=binding;

        }
    }

}
