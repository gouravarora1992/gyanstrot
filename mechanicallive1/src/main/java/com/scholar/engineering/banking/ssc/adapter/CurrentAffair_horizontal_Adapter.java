package com.scholar.engineering.banking.ssc.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.Comment_Common_homepage;
//import com.scholar.engineering.banking.ssc.CurrentAffair_details_via_link;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.DatabaseHandler;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;

/**
 * Created by surender on 3/14/2017.
*/

public class CurrentAffair_horizontal_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context c;
    ArrayList<Home_getset> data;
    JSONObject jsonobj,jsonfield;
    String email,URL;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    DatabaseHandler dbh;
    int cl,id;

    public CurrentAffair_horizontal_Adapter(Context c, ArrayList<Home_getset> data) {
        this.c = c;
        this.data = data;

        email=User_Email;

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.blankimage)
                .showImageForEmptyUri(R.drawable.congrrr)
                .showImageOnFail(R.drawable.congrrr)
                .cacheInMemory(false)
                .cacheOnDisk(false)
                .considerExifParams(true)
                .build();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

                View v0 = inflater.inflate(R.layout.current_affair_listitem, viewGroup, false);

                viewHolder = new ViewHolder3(v0);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

                ViewHolder3 vh0 = (ViewHolder3) viewHolder;

                setJobs(vh0, position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder3 extends RecyclerView.ViewHolder {

        //  **************************  Set New Post Current Affairs  ********************

        TextView tv_title,tv_like,tv_comment,tv_share;
        LinearLayout lay_share,lay_comment,lay_like;
        RelativeLayout lay;
        ImageView iv_image_banner,iv_like;

        public ViewHolder3(View v) {
            super(v);

            tv_title = (TextView) v.findViewById(R.id.tv_title);
            tv_comment = (TextView) v.findViewById(R.id.tv_commentid);
            tv_like = (TextView) v.findViewById(R.id.tv_like);

            iv_like = (ImageView) v.findViewById(R.id.likeimageid);

            lay_comment=(LinearLayout)v.findViewById(R.id.lay_comment);
            lay_like=(LinearLayout)v.findViewById(R.id.lay_like);
            lay_share=(LinearLayout)v.findViewById(R.id.lay_shareid);
            lay=(RelativeLayout)v.findViewById(R.id.layout);
            iv_image_banner=(ImageView)v.findViewById(R.id.iv_currentaffairs);
            tv_share=(TextView)v.findViewById(R.id.tv_shareid);
        }
    }

    private void setJobs(ViewHolder3 vh3, final int p) {

        try {

            Typeface font_demi = Typeface.createFromAsset(c.getAssets(), "avenirnextdemibold.ttf");
            Typeface font_meduim = Typeface.createFromAsset(c.getAssets(), "avenirnextmediumCn.ttf");
            vh3.tv_title.setTypeface(font_demi);
            vh3.tv_comment.setTypeface(font_meduim);
            vh3.tv_like.setTypeface(font_meduim);

            vh3.tv_share.setTypeface(font_meduim);

            jsonobj = new JSONObject(data.get(p).getJsonfield());

            vh3.tv_title.setText(jsonobj.getString("title"));
            vh3.tv_like.setText(data.get(p).getLikecount() + "");
            vh3.tv_comment.setText(data.get(p).getCommentcount() + "");

            if (data.get(p).getLikestatus().equals("like")) {
                vh3.iv_like.setImageResource(R.drawable.ic_bulb_filled);
                vh3.tv_like.setTextColor(c.getResources().getColor(R.color.like_text_color));
            }
            else
                {
                vh3.iv_like.setImageResource(R.drawable.ic_bulb_lightup);
                vh3.tv_like.setTextColor(c.getResources().getColor(R.color.text_lightgray));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            //Logg("json exception test","e",e);
        }

        ImageLoader imageLoader = ImageLoader.getInstance();

        try {
            if(!jsonobj.getString("image").equalsIgnoreCase("null")){
                imageLoader.init(ImageLoaderConfiguration.createDefault(c));
                imageLoader.getInstance().displayImage(Constants.URL_Image+"currentaffair/"+jsonobj.getString("image"), vh3.iv_image_banner, options, animateFirstListener);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        vh3.lay_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(c, Comment_Common_homepage.class);
                in.putExtra("posttype","6");
                in.putExtra("postid",data.get(p).getId());
                c.startActivity(in);

            }
        });

        vh3.lay_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                volleylike_post(data.get(p).getId(),p);
            }
        });

        vh3.lay_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JSONObject shareobj=new JSONObject();
                String value="";
                String[] valuearr;
                try {

                    jsonobj=new JSONObject(data.get(p).getJsonfield());

                    value= String.valueOf(Html.fromHtml(String.valueOf(Html.fromHtml(data.get(p).getPostdescription()+""))));
                    valuearr=value.split(System.lineSeparator(), 2);
                    value=valuearr[0];


                    shareobj.put("class","CurrentAffair");
                    shareobj.put("title",jsonobj.getString("title"));
                    shareobj.put("description"," ");
                    shareobj.put("image",Constants.URL+"currentaffair/"+jsonobj.getString("image"));
                    shareobj.put("id",data.get(p).getId());
//

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Utility.shareIntent(c,shareobj);


            }
        });

        vh3.iv_image_banner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent in = new Intent(c, CurrentAffair_details_via_link.class);
//                in.putExtra("postid",data.get(p).getId());
//                c.startActivity(in);
            }
        });

    }

    public void volleylike_post(int postid, final int p)
    {

        RequestQueue queue= Volley.newRequestQueue(c);
        JSONObject json = new JSONObject();
        try {
            json.put("id",postid);
            json.put("email",Constants.User_Email);
            json.put("admission_no",Constants.addmissionno);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Constants.URL_LV + "like_post";
        Logg("likeapi",url+" , "+json.toString());
        JsonObjectRequest jsonreq=new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {

// //Logg("res",String.valueOf(res));

                // TODO Auto-generated method stub

                try {

                    if(res.getString("scalar").equals("like")) {
//                        Toast.makeText(c,"success",Toast.LENGTH_SHORT).show();

                       int likecount=data.get(p).getLikecount();
                        likecount++;
                        data.get(p).setLikecount(likecount);
                        data.get(p).setLikestatus("like");
                        notifyItemChanged(p);
//                        notifyDataSetChanged();
                        //setLike(likecount);
                        //iv_like.setImageResource(R.drawable.likefill);

                    }
                    else if(res.getString("scalar").equals("dislike")){

//                        iv_like.setImageResource(R.drawable.solar4);

//                        Toast.makeText(c,"success",Toast.LENGTH_SHORT).show();
                        int likecount=data.get(p).getLikecount();
                        likecount--;
                        data.get(p).setLikecount(likecount);
                        data.get(p).setLikestatus("dislike");
                        notifyItemChanged(p);
//                        notifyDataSetChanged();
//                        setLike(likecount);
                    }
                    else {
                        Toast.makeText(c,"Not done",Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        },new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg)
            {

                //Logg("error", String.valueOf(arg));

                Toast.makeText(c,"Network Problem", Toast.LENGTH_SHORT).show();

            }


        });

        queue.add(jsonreq);
    }
}
