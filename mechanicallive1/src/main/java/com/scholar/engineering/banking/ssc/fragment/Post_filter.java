package com.scholar.engineering.banking.ssc.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
//import com.google.android.gms.ads.MobileAds;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.adapter.Home_RecyclerViewAdapter2;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.EndlessRecyclerViewScrollListener;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Post_filter extends AppCompatActivity {
	ArrayList<Home_getset> homedata;
	int searchlength=0;
	RecyclerView homelist;
	public Home_RecyclerViewAdapter2 adapter;
	SharedPreferences pref,sp;
	SwipeRefreshLayout swipeRefreshLayout;
	int index=0;
	String st_searchtext="";
	Boolean aBoolean=true;
	Boolean isRunning=true;
	NestedScrollView nestedscroll;

	RelativeLayout footerlayout;
	TextView loadmore;
	Typeface font_demi;
	ProgressWheel wheelbar;
	NetworkConnection nw;
	boolean prg=true;
	String date_from="",date_to="",st_search="";

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.post_filter);

		Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setTitle("Posts");
		Utility.applyFontForToolbarTitle(toolbar,this);

		nw=new NetworkConnection(Post_filter.this);
//		Constants.checkrefresh=true;
//		setHasOptionsMenu(true);

		date_from=getIntent().getStringExtra("date_from");
		date_to=getIntent().getStringExtra("date_to");
		st_search=getIntent().getStringExtra("search");

		sp=getSharedPreferences("likestatus", 0);
		font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");

		nestedscroll=(NestedScrollView)findViewById(R.id.nestedscroll);

		homelist=(RecyclerView)findViewById(R.id.recyclerView);

		LinearLayoutManager mLayoutManager = new LinearLayoutManager(Post_filter.this);

		homelist.setLayoutManager(mLayoutManager);

		homedata=new ArrayList<>();

		footerlayout=(RelativeLayout)findViewById(R.id.footer_layout);
		loadmore=(TextView)findViewById(R.id.moreButton);
		wheelbar=(ProgressWheel)findViewById(R.id.progress_wheel);

		EndlessRecyclerViewScrollListener endless = new EndlessRecyclerViewScrollListener(mLayoutManager) {
			@Override
			public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

				Logg("page",page+"");

				if(homedata.size()>9) {
					footerlayout.setVisibility(View.VISIBLE);
					getData();
				}
			}
		};

		// Adds the scroll listener to RecyclerView

		homelist.addOnScrollListener(endless);

		index=0;

		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());

		getData();

		swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh_layout);

		swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				prg=false;
				index=0;
				getData();

			}
		});

		pref=getSharedPreferences("checkdata",0);

		footerlayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

				Home_getset home;

				home= (Home_getset) homedata.get(homedata.size()-1);

				index= home.getId();

				Logg("index=",index+"");
				getData();

				loadmore.setText("");
				wheelbar.setVisibility(View.VISIBLE);

			}
		});



	}

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
	}

    @Override
    public void onStop() {
        super.onStop();
        //Logg("call_onstop","onStop");
    }

	public void deleteCache(Context context) {
		try {
			File dir = context.getCacheDir();
			deleteDir(dir);
		} catch (Exception e) {}
	}

	public boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
			return dir.delete();
		} else if(dir!= null && dir.isFile()) {
			return dir.delete();
		} else {
			return false;
		}
	}

	public void internetconnection(){

		final Dialog dialog = new Dialog(Post_filter.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.network_alert);
		dialog.show();

//        JSONObject json;

		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
		TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

		//tv_tryagain.setText("homefragment");

		tv_tryagain.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

//				//index=0;
				swipeRefreshLayout.setRefreshing(true);
				dialog.dismiss();
				getData();

			}
		});

		iv_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

	}
	ProgressDialog progress;

	public void getData() {

		if(!nw.isConnectingToInternet()) {
//            swipeRefreshLayout.setRefreshing(false);
			internetconnection();
			return;
		}

		if(index==0&prg) {
			progress = ProgressDialog.show(Post_filter.this, null, null, true);
			progress.setContentView(R.layout.progressdialog);
			progress.setCanceledOnTouchOutside(false);
			progress.setCancelable(false);
			progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
			progress.show();
		}

		RequestQueue queue = Volley.newRequestQueue(Post_filter.this);
		String url="";

		url = Constants.URL +"v3/search_post.php?";

		url = url.replace(" ", "%20");

		Logg("name", url);

		StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
			@Override
			public void onResponse(String s) {

				Logg("response", s);
				try {
					if(s.length()>0)
					{
						JSONObject res=new JSONObject(s);

						if(res.has("data")){

							footerlayout.setVisibility(View.GONE);

							if(index==0) {

								homedata.clear();

								JSONArray array=res.getJSONArray("data");

								for(int i=0;i<array.length();i++) {
//									Home_getset home=new Home_getset();

									JSONObject ob = array.getJSONObject(i);

//									String time = getDate(ob.getString("timestamp"));

									Home_getset object=new Home_getset();
									object.setId(ob.getInt("id"));
									object.setTimestamp(ob.getString("timestamp"));
									object.setLikecount(ob.getInt("likes"));
									object.setCommentcount(ob.getInt("comment"));
									object.setViewcount(ob.getInt("view"));
									object.setUid(ob.getString("uid"));
									object.setPosttype(ob.getString("posttype"));
									object.setGroupid(ob.getString("groupid"));
									object.setFieldtype(ob.getString("field_type"));
									object.setJsonfield(ob.getString("jsondata"));
									object.setLikestatus(ob.getString("likestatus"));
									object.setPosturl(ob.getString("posturl"));
									object.setPostdescription(ob.getString("post_description"));
									if(ob.has("AppVersion"))
										object.setAppVersion(ob.getString("AppVersion"));

									homedata.add(object);

//									homedata.add(home);
								}

								adapter = new Home_RecyclerViewAdapter2(Post_filter.this, homedata,"Post_filter");
								homelist.setAdapter(adapter);
								int size=array.length()-1;

								index=array.getJSONObject(size).getInt("id");

								Logg("index",index+"");
							}
							else {

								homedata.clear();

								JSONArray array=res.getJSONArray("data");

								for(int i=0;i<array.length();i++) {
									JSONObject ob = array.getJSONObject(i);

//									String time = getDate(ob.getString("timestamp"));

									Home_getset object=new Home_getset();
									object.setId(ob.getInt("id"));
									object.setTimestamp(ob.getString("timestamp"));
									object.setLikecount(ob.getInt("likes"));
									object.setCommentcount(ob.getInt("comment"));
									object.setViewcount(ob.getInt("view"));
									object.setUid(ob.getString("uid"));
									object.setPosttype(ob.getString("posttype"));
									object.setGroupid(ob.getString("groupid"));
									object.setFieldtype(ob.getString("field_type"));
									object.setJsonfield(ob.getString("jsondata"));
									object.setLikestatus(ob.getString("likestatus"));
									object.setPosturl(ob.getString("posturl"));
									object.setPostdescription(ob.getString("post_description"));
									if(ob.has("AppVersion"))
										object.setAppVersion(ob.getString("AppVersion"));

									homedata.add(object);

								}

								adapter.addItme(homedata);

								int size=array.length()-1;
								index=array.getJSONObject(size).getInt("id");

								Logg("index",index+"");

								adapter.notifyDataSetChanged();

							}
							if(swipeRefreshLayout.isRefreshing()){
								Logg("swipeRefreshlayout","true");
								nestedscroll.fullScroll(View.FOCUS_UP);
							}
							else
								Logg("swipeRefreshlayout","false");

							swipeRefreshLayout.setRefreshing(false);

							isRunning=true;

						}
						else
							footerlayout.setVisibility(View.GONE);

						progress.dismiss();
						//Logg("response_string",s+" response");
					}
				}

				catch (Exception e)
				{
					progress.dismiss();
					Logg("e",e+"");
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {
				CommonUtils.toast(Post_filter.this,CommonUtils.volleyerror(volleyError));
				progress.dismiss();
			}
		}) {
			@Override
			protected Map<String, String> getParams() {

				Map<String, String> params = new HashMap<String, String>();
				params.put("user_id", Constants.User_Email);
				params.put("index",index+"");
				params.put("date_from",date_from);
				params.put("date_to",date_to);
				params.put("search",st_search);

				Logg("paramsearchnews",params.toString());
				return params;

			}
		};

		int socketTimeout = 10000;
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		request.setRetryPolicy(policy);
		queue.add(request);
	}


	private String getDate(String OurDate)
	{
		try
		{
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date value = formatter.parse(OurDate);
			SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //this format changeable
			dateFormatter.setTimeZone(TimeZone.getDefault());
			OurDate = dateFormatter.format(value);
			//Log.d("OurDate", OurDate);
		}
		catch (Exception e)
		{
			//Logg("exception_time","e",e);
//            OurDate = "00-00-0000 00:00";
		}
		//Logg("date_time",OurDate);


		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String formattedDate = df.format(c.getTime());

		return timediff(OurDate,formattedDate);

	}


	public String ConvertDate(String pre_dt){

		String dateStr = pre_dt;//"21/20/2011";

		DateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date date = null;

		try {

			date = srcDf.parse(dateStr);

			DateFormat destDf = new SimpleDateFormat("dd-MMM-yyyy");

			return dateStr = destDf.format(date);

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return "";
	}

	public String timediff(String prevdate,String curdate)
	{

		Date d11 = null;
		Date d2 = null;

		String[] dd1= prevdate.split(" ");
		String[] date1=dd1[0].split("-");

		SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");

		try {
			d11 = format.parse(prevdate);
			d2 = format.parse(curdate);

			//in milliseconds
			long diff = d2.getTime() - d11.getTime();

			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffDays = diff / (24 * 60 * 60 * 1000);

			String pd=dd1[0];

			String cd;//=cal.get(Calendar.YEAR)+"-"+(cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.DAY_OF_MONTH);
			String[] cdarr=curdate.split(" ");
			cd=cdarr[0];
			String[] date2=cd.split("-");

			int years=0,months=0,days=0;

			years=Integer.parseInt(date2[0])-Integer.parseInt(date1[0]);
			months=Integer.parseInt(date2[1])-Integer.parseInt(date1[1]);
			days=Integer.parseInt(date2[2])-Integer.parseInt(date1[2]);

			years=(months<0)? years-1 : years;
			months=(months<0)? 12+(months) : months;
			months=(days<0)? months-1 : months;
			days=(days<0)? 30+days : days;

			//	//Logg("di time= "," hrs= "+diffHours+" min= "+diffMinutes+" "+" years=  "+years+" month=  "+months+" days= "+days+" pd= "+pd+" cd= "+cd);

			if(years>0)
			{
				return (String.valueOf(years)+" Years ago");
			}
			else if(months>0&years<1)
			{

				return (String.valueOf(months) + " Months ago");
				//newsdateedit.putString("postdate"+i,String.valueOf(months)+"Months ago");
			}
			else if(days>0&months<1)
			{
				return (String.valueOf(days) + " Days ago");
				//newsdateedit.putString("postdate"+i,String.valueOf(months)+" Days ago");
			}
			else if(diffHours>0&days<1)
			{
				return (String.valueOf(diffHours) + " Hours ago");
				//newsdateedit.putString("postdate"+i,String.valueOf(months)+" Hours ago");
			}
			else if(diffMinutes>0&diffHours<1)
			{
				return (String.valueOf(diffMinutes) + " Minutes ago");
				//newsdateedit.putString("postdate"+i,String.valueOf(months)+" Minutes ago");
			}
			else if(diffSeconds>0&diffMinutes<1)
			{
				return ("0 Minutes ago");
				//newsdateedit.putString("postdate"+i,"0 Minutes ago");
			}

		} catch (Exception e) {
			e.printStackTrace();
			////Logg("di time error= ",e+"");
		}
		return ("1 Minutes ago");
	}

}