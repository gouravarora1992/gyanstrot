package com.scholar.engineering.banking.ssc;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatDrawableManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import com.google.firebase.BuildConfig;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.Challenge.Blocked_Users;
import com.scholar.engineering.banking.ssc.Challenge.Challenge_frag;
import com.scholar.engineering.banking.ssc.Challenge.LeaderBoard;
import com.scholar.engineering.banking.ssc.Mentors_Champions.Mentors_Champions;
import com.scholar.engineering.banking.ssc.Staff.ChatActivity;
import com.scholar.engineering.banking.ssc.fragment.Home_fragment;

import com.scholar.engineering.banking.ssc.fragment.Premium_Test_Fragment;
import com.scholar.engineering.banking.ssc.fragment.Topic_Search;
import com.scholar.engineering.banking.ssc.newScreens.NotesVideosTab;
import com.scholar.engineering.banking.ssc.newScreens.QueryForms.QueryForm;
import com.scholar.engineering.banking.ssc.newScreens.QueryForms.QueryList;
import com.scholar.engineering.banking.ssc.newScreens.StudentProfile;
//import com.scholar.engineering.banking.ssc.newScreens.Webview_BigBlueButton;
import com.scholar.engineering.banking.ssc.newScreens.adapters.NotesVideoAdapter;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.toast;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.volleyerror;

public class HomeActivity extends AppCompatActivity {

    Context mcoxt;
    Toolbar toolbar;
    CircleImageView dr_image;

    ImageView iv_notification;
    String img_url, name, addmissiono, email, college, state, ref_code, point, hash_id;
    LinearLayout lay_cashback;
    LinearLayout AboutUsLayout, ContactUsLayout, ShareandEarnLayout, refunt_layout, lay_leaderboard, blocked_user_layout, lay_attendance;
    public ViewPager viewPager;
    TextView dr_name, dr_cashback, dr_college, dr_leaderboard, dr_blockuser,
            dr_about_us, dr_share, dr_contact_us, dr_chat, dr_attendance;
    String versionName;
    int version_code = 0, newvers_code = 0;
    NetworkConnection nw;

    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    Typeface font_demi, font_medium;
    ImageLoader imageLoader = ImageLoader.getInstance();

    RecyclerView rv_honrizontal;
    LinearLayout  lay_mentors, lay_champions,query_layout,query_list, lay_pickup, lay_chat;
    TextView Champions, Mentors, tv_title, PickUp;
    ImageView chat, notification;

    UserSharedPreferences preferences, playerpreference;

    public static JSONObject obj_champions, obj_menotrs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_new);

        preferences = UserSharedPreferences.getInstance(this);
        playerpreference = new UserSharedPreferences(this, "playerpreference");
        DatabaseReference mFirebaseDatabase;
        FirebaseDatabase mFirebaseInstance;
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference("users");

        if (preferences.getaddmissiono().length() > 1) {
            mFirebaseDatabase.child(preferences.getaddmissiono()).setValue("online");
            FirebaseDatabase.getInstance()
                    .getReference("users/" + preferences.getaddmissiono())
                    .onDisconnect()           // Setting up Disconnect hook
                    .setValue(date());
        }

        rv_honrizontal = (RecyclerView) findViewById(R.id.recyclerView_horizontal);
        LinearLayoutManager lay_Manager = new LinearLayoutManager(this);
        lay_Manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rv_honrizontal.setLayoutManager(lay_Manager);
        rv_honrizontal.setHasFixedSize(true);
        rv_honrizontal.setNestedScrollingEnabled(false);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.blankimage)
                .showImageForEmptyUri(R.drawable.congrrr)
                .showImageOnFail(R.drawable.congrrr)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        deleteCache(this);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        Constants.Width = displayMetrics.widthPixels;
        Constants.Height = displayMetrics.heightPixels;

        nw = new NetworkConnection(HomeActivity.this);

        version_code = BuildConfig.VERSION_CODE;
        versionName = BuildConfig.VERSION_NAME;

        initToolbar();
        initViewPagerAndTabs();

        checkversion();
        deleteCache(this);

        updatePlayerid();

    }

    private String date() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formatter.format(new Date());
    }

    int REQUEST_MICROPHONE=0101;
    private void initToolbar() {
        font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");
        font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        chat = (ImageView) toolbar.findViewById(R.id.chat);

        notification = (ImageView) toolbar.findViewById(R.id.notification);

        mcoxt = HomeActivity.this;
        tv_title = (TextView) toolbar.findViewById(R.id.tv_title);
        tv_title.setTypeface(font_demi);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(mcoxt.getResources().getString(R.string.app_name));

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);

        toggle.syncState();
        toggle.setDrawerIndicatorEnabled(false);
        @SuppressLint("RestrictedApi") Drawable drawable = AppCompatDrawableManager.get().getDrawable(this, R.drawable.ic_draawer_menu);
        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.inflateHeaderView(R.layout.header_layout);
        LinearLayout lay_profile = headerView.findViewById(R.id.lay_profile);
        dr_image = (CircleImageView) headerView.findViewById(R.id.profile);
        dr_name = (TextView) headerView.findViewById(R.id.dr_nameid);
        dr_leaderboard = (TextView) headerView.findViewById(R.id.dr_leaderboard);
        dr_college = (TextView) headerView.findViewById(R.id.dr_collegeid);
        dr_share = (TextView) headerView.findViewById(R.id.dr_shareid);
        dr_contact_us = (TextView) headerView.findViewById(R.id.dr_contactus);
        dr_about_us = (TextView) headerView.findViewById(R.id.dr_about_us);
        dr_blockuser = (TextView) headerView.findViewById(R.id.dr_blockuser);
        dr_cashback = (TextView) headerView.findViewById(R.id.dr_cashback);
        dr_attendance = (TextView) headerView.findViewById(R.id.dr_attendance);

        ShareandEarnLayout = (LinearLayout) headerView.findViewById(R.id.share_layout);
        lay_cashback = (LinearLayout) headerView.findViewById(R.id.cashback_layout);
        blocked_user_layout = (LinearLayout) headerView.findViewById(R.id.lay_blockeduser);
        lay_leaderboard = (LinearLayout) headerView.findViewById(R.id.lay_leaderboard);
        lay_attendance = (LinearLayout) headerView.findViewById(R.id.lay_attendance);

        AboutUsLayout = (LinearLayout) headerView.findViewById(R.id.aboutus_layout);
        ContactUsLayout = (LinearLayout) headerView.findViewById(R.id.contactus_layout);
        lay_mentors = (LinearLayout) headerView.findViewById(R.id.lay_mentors);
        lay_pickup = (LinearLayout) headerView.findViewById(R.id.lay_pickup);
        lay_champions = (LinearLayout) headerView.findViewById(R.id.lay_champions);
        refunt_layout = (LinearLayout) headerView.findViewById(R.id.refunt_layout);

        Mentors = (TextView) headerView.findViewById(R.id.dr_mentor);
        PickUp = headerView.findViewById(R.id.dr_pickup);
        Champions = (TextView) headerView.findViewById(R.id.dr_champions);
        query_layout = headerView.findViewById(R.id.query_layout);
        query_list = headerView.findViewById(R.id.query_list);

//        webview_layout=(LinearLayout)headerView.findViewById(R.id.webview_layout);
//        webview_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent in = new Intent(getApplicationContext(), Webview_BigBlueButton.class);
//                startActivity(in);
//
//            }
//        });

        lay_pickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), PickUpRequestActivity.class);
                startActivity(in);
            }
        });

        lay_attendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), StudentAttendanceActivity.class);
                startActivity(in);
            }
        });

        query_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getApplicationContext(), QueryList.class);
                startActivity(in);
            }
        });
        query_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getApplicationContext(), QueryForm.class);
                startActivity(in);
            }
        });
        lay_mentors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), Mentors_Champions.class);
                in.putExtra("type", "mentor");
                startActivity(in);
            }
        });
        lay_champions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), Mentors_Champions.class);
                in.putExtra("type", "champions");
                startActivity(in);
            }
        });

        AboutUsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), aboutus.class);
                startActivity(in);
            }
        });

        ContactUsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), contactus.class);
                startActivity(in);
            }
        });
        ShareandEarnLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), Share_and_Earn.class);
                startActivity(in);
            }
        });

        blocked_user_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), Blocked_Users.class);
                startActivity(in);
            }
        });
        lay_leaderboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), LeaderBoard.class);
                startActivity(in);
            }
        });

        refunt_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), Legal_Policy.class);
                startActivity(in);
            }
        });

        lay_cashback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), Withdraw_Money.class);
                startActivity(in);
            }
        });

        lay_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(HomeActivity.this, StudentProfile.class);
                startActivity(in);
            }
        });


        dr_name.setTypeface(font_demi);
        dr_college.setTypeface(font_medium);
        dr_about_us.setTypeface(font_medium);
        dr_contact_us.setTypeface(font_medium);
        dr_share.setTypeface(font_medium);

        dr_blockuser.setTypeface(font_medium);
        dr_leaderboard.setTypeface(font_medium);
        dr_cashback.setTypeface(font_medium);


        imageLoader.init(ImageLoaderConfiguration.createDefault(HomeActivity.this));
        imageLoader.getInstance().displayImage(img_url, dr_image, options, animateFirstListener);

        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, ChatActivity.class);
                startActivity(i);
            }
        });


        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HomeActivity.this, Notifications.class);
                startActivity(i);
            }
        });

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==REQUEST_MICROPHONE)
        {

        }
    }

    private void initViewPagerAndTabs() {

        viewPager = (ViewPager) findViewById(R.id.viewPager);

        PagerAdapter1 pagerAdapter = new PagerAdapter1(getSupportFragmentManager());

        pagerAdapter.addFragment(new Home_fragment(), "");
        pagerAdapter.addFragment(new Topic_Search(), "");
        pagerAdapter.addFragment(new NotesVideosTab(), "");
        pagerAdapter.addFragment(new Challenge_frag(), "");

        viewPager.setAdapter(pagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);

        iv_notification = (ImageView) findViewById(R.id.imageView13);

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_home);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_menu);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_document);
        tabLayout.getTabAt(3).setIcon(R.drawable.ic_swords);

        viewPager.setOffscreenPageLimit(3);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Constants.viewpagercurrentitem = position;
                if (position == 2) {
                    iv_notification.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageSelected(int position) {
                Constants.viewpagercurrentitem = position;
                if (position == 2) {
                    iv_notification.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() > 0) {
            viewPager.setCurrentItem(0);
        } else {
            exitapp();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        preferences = UserSharedPreferences.getInstance(this);

        img_url = preferences.getimage();
        name = preferences.getname();
        email = preferences.getemail();
        addmissiono = preferences.getaddmissiono();
        college = (preferences.getclass() + "-" + preferences.getsection());

        Constants.User_Email = email;
        Constants.addmissionno = addmissiono;


        dr_name.setText(name);
        dr_college.setText(college);
        imageLoader.init(ImageLoaderConfiguration.createDefault(HomeActivity.this));
        imageLoader.getInstance().displayImage(img_url, dr_image, options, animateFirstListener);
    }

    static class PagerAdapter1 extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter1(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }

    private void updatePlayerid() {
        RequestQueue queue = Volley.newRequestQueue(HomeActivity.this);
        final JSONObject obj = new JSONObject();
        try {
            obj.put("admission_no", preferences.getaddmissiono());
            obj.put("player_id", playerpreference.getplayerid());
            obj.put("email", preferences.getemail());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url = Constants.URL_LV + "update_PlayerId";
        Logg("update_PlayerId", url + " , " + obj.toString());
        JsonObjectRequest json = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject res) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError arg0) {
                toast(HomeActivity.this, volleyerror(arg0));
            }
        });
        queue.add(json);
    }

    public void checkversion() {
        RequestQueue queue = Volley.newRequestQueue(HomeActivity.this);
        final JSONObject obj = new JSONObject();
        try {
            obj.put("email",Constants.User_Email);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url = Constants.URL_LV+ "appversion";
        Logg("checkversion_url", url);

        JsonObjectRequest json = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                try {
                    JSONArray jr = res.getJSONArray("details");
                    newvers_code = jr.getJSONObject(0).getInt("version_code");
                    Logg("checkversion_url",newvers_code+" , "+version_code);
                    if (newvers_code > version_code) {
                        Logg("checkversion_url",Constants.Check_version+"---");
                        if (!(Constants.Check_version.equalsIgnoreCase("true"))) {
                            if (jr.getJSONObject(0) != null)
                                versiondialog(jr.getJSONObject(0));
                        }
                    }
                    if (res.has("share")) {
                        JSONArray jr1 = res.getJSONArray("share");
                        Constants.Share_desc = jr1.getJSONObject(0).getString("desc");
                        Constants.Share_point = jr1.getJSONObject(0).getString("point");
                    }

//                    if(res.has("userinfo"))
//                    {
//                        JSONArray jr2=res.getJSONArray("userinfo");

//                        shareedit = share.edit();

//                        for(int i=0;i<jr2.length();i++)
//                        {
//                            jsonobj=jr2.getJSONObject(i);
//
//                            Constants.Point=String.valueOf(jsonobj.getInt("point"));
//                            comment = jsonobj.getInt("comment");
//                            reward = jsonobj.getInt("reward");
//                            shareedit.putString("name", jsonobj.getString("name"));
//                            shareedit.putString("image", jsonobj.getString("image"));
//                            shareedit.putString("collage", jsonobj.getString("college"));
//                            shareedit.putString("state", jsonobj.getString("state"));
//                            shareedit.putString("phone", jsonobj.getString("mobile"));
//                            shareedit.putInt("comment", jsonobj.getInt("comment"));
//                            shareedit.putInt("reward", jsonobj.getInt("reward"));
//                            shareedit.putString("usertype",jsonobj.getString("type"));
//                            shareedit.putString("referralcode",jsonobj.getString("referralcode"));
//
//                            shareedit.putString("usernotification", jsonobj.getString("notification"));

//                            ref_code=jsonobj.getString("referralcode");
//                        }

//                        shareedit.putInt("comment", comment);
//                        shareedit.putInt("reward", reward);
//                        shareedit.putString("point", Constants.Point);
//                        shareedit.commit();

//                        dr_reward.setText(reward+" Reward");
//                        dr_comment.setText(comment+" Comments");

//                        if(share.getString("usertype","0").equalsIgnoreCase("0"))
//                            Select_UserType_dialog();


//                        if(Integer.parseInt(Constants.Point)>199){
//                            shareedit=share.edit();
//                            shareedit.putBoolean("nevershowagain",false);
//                            shareedit.commit();
//                        }

//                        if(Integer.parseInt(Constants.Point)<200) {
//                            if (!share.getBoolean("nevershowagain", false))
//                                wallet_alertdialog();
//                        }

//                    }

//                    if(res.has("champs")){
//                        if(res.getJSONObject("champs").getString("status").equalsIgnoreCase("success"))
//                            obj_champions=res.getJSONObject("champs");
//                    }
//
//                    if(res.has("mentors")){
//                        if(res.getJSONObject("mentors").getString("status").equalsIgnoreCase("success"))
//                        obj_menotrs=res.getJSONObject("mentors");
//                    }

                } catch (JSONException e) {
                    Logg("exception",e+"");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError arg0) {
                toast(HomeActivity.this, volleyerror(arg0));
            }
        });
        queue.add(json);
    }

    public void versiondialog(final JSONObject object) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        final Dialog dialog = new Dialog(HomeActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.versiondialog_layout);
        dialog.show();

        TextView notnow = (TextView) dialog.findViewById(R.id.notnowid);
        TextView update = (TextView) dialog.findViewById(R.id.updateid);
        TextView title = (TextView) dialog.findViewById(R.id.titleid);
        TextView description = (TextView) dialog.findViewById(R.id.descriptionid);
        LinearLayout layout = (LinearLayout) dialog.findViewById(R.id.layoutid);

        layout.setMinimumWidth(width);

        try {
            title.setText(object.getString("title"));
            description.setText(object.getString("description"));
            notnow.setText(object.getString("button_neg"));
            update.setText(object.getString("button_text"));

        } catch (JSONException e) {

        }


        Constants.Check_version = "true";
        notnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (object.getString("action").equalsIgnoreCase("update")) {
                        String PACKAGE_NAME = getPackageName();
                        Intent in = new Intent(Intent.ACTION_VIEW);
                        in.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + PACKAGE_NAME));
                        startActivity(in);
                    } else {
                        dialog.dismiss();
                    }
                } catch (JSONException e) { }
            }
        });
    }

    public void exitapp() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(HomeActivity.this);

        alertDialog.setMessage("Do You Want To Exit Gyan Strot?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    public void internetconnection() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(HomeActivity.this);

        alertDialog.setMessage("Check Your Internet Connection activity?");
        alertDialog.setPositiveButton("Reresh", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                viewPager.setCurrentItem(0);

            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    public void wallet_alertdialog() {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        final Dialog dialog = new Dialog(HomeActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.wallet_alert_dialog);
        dialog.show();

        TextView notify_later = (TextView) dialog.findViewById(R.id.tv_notify_later);
        TextView never_showagain = (TextView) dialog.findViewById(R.id.tv_nevershow);
        TextView tv_share = (TextView) dialog.findViewById(R.id.tv_share);

        TextView title = (TextView) dialog.findViewById(R.id.titleid);
        TextView description = (TextView) dialog.findViewById(R.id.descriptionid);

        LinearLayout layout = (LinearLayout) dialog.findViewById(R.id.layoutid);

        title.setText("Oops! Low Balance");

        description.setText("You have just Rs. " + Constants.Point + " in your wallet. For smoother experience, share Gyan Strot app now and get Rs. 50 on each share.");

        notify_later.setTypeface(font_demi);
        never_showagain.setTypeface(font_demi);
        tv_share.setTypeface(font_demi);
        title.setTypeface(font_demi);
        description.setTypeface(font_demi);

        SpannableString content = new SpannableString("Notify Later");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);

        SpannableString content1 = new SpannableString("Never Show Again");
        content1.setSpan(new UnderlineSpan(), 0, content1.length(), 0);

        notify_later.setText(content);
        never_showagain.setText(content1);

        layout.setMinimumWidth(width - 150);


        tv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_SUBJECT, "");
                share.putExtra(Intent.EXTRA_TEXT, "I have joined India's first digital educational social app (" + Constants.APPNAME + ") and I found it very useful for preparation of SSC, Banking, GATE etc. I'm inviting you too to join it using my referral code:\n\n*" + ref_code + "*\n\nUse this code during registration & get Rs. " + Constants.Share_point + " in your wallet!" + "\nDownload here: " + "play.google.com/store/apps/details?id=" + getPackageName());
                startActivity(Intent.createChooser(share, "Share link!"));


            }
        });

        notify_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        never_showagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                shareedit=share.edit();
//                shareedit.putBoolean("nevershowagain",true);
//                shareedit.commit();

                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onDestroy() {
        Logg("call_onDestroy()", "Call onDestroy()");
        super.onDestroy();
        deleteCache(this);
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }
}
