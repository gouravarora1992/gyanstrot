package com.scholar.engineering.banking.ssc.fragment;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.scholar.engineering.banking.ssc.R;


public class FourthView extends Fragment
{
    ImageView im;
    boolean b;
    TextView skip;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fourthview,container,false);
        im=(ImageView)view.findViewById(R.id.imageView9);



        return view;
    }
}
