package com.scholar.engineering.banking.ssc;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;

import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.scholar.engineering.banking.ssc.adapter.Followgroup_adap;
import com.scholar.engineering.banking.ssc.getset.gettr_settr;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.ImageCompression;
import com.scholar.engineering.banking.ssc.utils.ImageUploading;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.Constants.MY_PERMISSIONS_REQUEST;
import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;
import static com.scholar.engineering.banking.ssc.utils.Constants.imageFilePath;
import static com.scholar.engineering.banking.ssc.utils.Constants.st_base46;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.Utility.hasPermissions;

/**
 * Created by surender on 19-04-2016.
 */
public class Post_question extends AppCompatActivity implements View.OnClickListener {

    LinearLayout postlay,addimage,addoption,optionc_lay,optiond_lay,opta_lay,optb_lay,optc_lay,optd_lay;
    TextView post_tv,uploadingtext,tv_ansa,tv_ansb,tv_ansc,tv_ansd,headertext;
    EditText et_que,et_opta,et_optb,et_optc,et_optd;
    ImageView deletc,deletd,camerimage;
    private final int REQUEST_CODE_CLICK_IMAGE = 1002, REQUEST_CODE_GALLERY_IMAGE = 1003;
    String groupname,st_que,st_a="null",st_b="null",st_c="null",st_d="null",st_ans="null",username,usercollage,mail,image;
    int count=2,groupid;
    Dialog dialog,dialog1;
    ProgressDialog progress;
    Typeface font_medium,font_demi;
    TextView text,text1,text2,text3;
    ArrayList<gettr_settr> grouplist;
    ImageUploading ab;
    NetworkConnection nw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_question);

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Write Your Post");
        Utility.applyFontForToolbarTitle(toolbar,this);
        Constants.imageFilePath=CommonUtils.getFilename();

        nw=new NetworkConnection(Post_question.this);

        if (Build.VERSION.SDK_INT >= 24) {
            try {
                java.lang.reflect.Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Typeface font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");
        Typeface font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");
        text=(TextView)findViewById(R.id.text);
        text1=(TextView)findViewById(R.id.text1);
        text2=(TextView)findViewById(R.id.text2);
        text3=(TextView)findViewById(R.id.text3);
        text2.setTypeface(font_medium);
        text.setTypeface(font_demi);
        text1.setTypeface(font_demi);
        text3.setTypeface(font_medium);
        headertext=(TextView)findViewById(R.id.headertextid);
        headertext.setVisibility(View.GONE);
        headertext.setTypeface(font_demi);
        et_que=(EditText)findViewById(R.id.questionid);
        et_opta=(EditText)findViewById(R.id.optiona_id);
        et_optb=(EditText)findViewById(R.id.optionb_id);
        et_optc=(EditText)findViewById(R.id.optionc_id);
        et_optd=(EditText)findViewById(R.id.optiond_id);
        et_que.setTypeface(font_medium);
        et_opta.setTypeface(font_medium);
        et_optb.setTypeface(font_medium);
        et_optc.setTypeface(font_medium);
        et_optd.setTypeface(font_medium);
        deletc=(ImageView)findViewById(R.id.deletec_id);
        deletd=(ImageView)findViewById(R.id.deleted_id);
        camerimage=(ImageView)findViewById(R.id.camerimageid);
        post_tv=(TextView)findViewById(R.id.postquestiontextid);
        post_tv.setTypeface(font_demi);
        addimage=(LinearLayout)findViewById(R.id.addimageid);
        tv_ansa=(TextView)findViewById(R.id.answera_id);
        tv_ansb=(TextView)findViewById(R.id.answerb_id);
        tv_ansc=(TextView)findViewById(R.id.answerc_id);
        tv_ansd=(TextView)findViewById(R.id.answerd_id);
        tv_ansa.setTypeface(font_demi);
        tv_ansb.setTypeface(font_demi);
        tv_ansc.setTypeface(font_demi);
        tv_ansd.setTypeface(font_demi);
        postlay=(LinearLayout)findViewById(R.id.postquestionid);
        addoption=(LinearLayout)findViewById(R.id.addmoreoptionlayid);
        optionc_lay=(LinearLayout)findViewById(R.id.optionc_layid);
        optiond_lay=(LinearLayout)findViewById(R.id.optiond_layid);
        opta_lay=(LinearLayout)findViewById(R.id.optionalayid);
        optb_lay=(LinearLayout)findViewById(R.id.optionblayid);
        optc_lay=(LinearLayout)findViewById(R.id.optionclayid);
        optd_lay=(LinearLayout)findViewById(R.id.optiondlayid);

//        username=pref.getString("name", "");
//        image=pref.getString("image","");
//        usercollage=pref.getString("collage","");
        mail=User_Email;

        addimage.setOnClickListener(this);
        postlay.setOnClickListener(this);
        tv_ansa.setOnClickListener(this);
        tv_ansb.setOnClickListener(this);
        tv_ansc.setOnClickListener(this);
        tv_ansd.setOnClickListener(this);
        addoption.setOnClickListener(this);
        deletc.setOnClickListener(this);
        deletd.setOnClickListener(this);

        grouplist=new ArrayList<>();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());


        if(!nw.isConnectingToInternet()) {
            Toast.makeText(Post_question.this,"No Internet Connection",Toast.LENGTH_LONG).show();
            return;
        }


        Intent in=getIntent();

        if(in.getStringExtra("groupname").equalsIgnoreCase("null"))
        volley_getFollowGroup();  //dialoggroup ();
        else
        {
            groupname=in.getStringExtra("groupname");
            groupid=Integer.valueOf(in.getStringExtra("id"));

            getSupportActionBar().setTitle(groupname);
//            headertext.setText(groupname);
        }

        dialog=new Dialog(Post_question.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.processing_dialog);
        uploadingtext=(TextView)dialog.findViewById(R.id.uploadingtextid);

        dialog1=new Dialog(this);

        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        ab=new ImageUploading(this,camerimage);

        ab.createBottomSheetDialog();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logg("call_onDestroy()","Call onDestroy()");
        deleteCache(this);
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {}
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    public void dialoggroup()
    {

        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);

        Rect displayRectangle = new Rect();
        Window window = dialog1.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        // inflate and adjust layout
        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout1 = inflater.inflate(R.layout.followgroup_dialog, null);
        layout1.setMinimumWidth((int) (displayRectangle.width() * 0.9f));
        layout1.setMinimumHeight((int) (displayRectangle.height() * 0.6f));
        dialog1.setContentView(layout1);
        TextView text=(TextView)dialog1.findViewById(R.id.text);
        text.setTypeface(font_demi);
        ListView list=(ListView)dialog1.findViewById(R.id.grouplistid);

        Followgroup_adap adap=new Followgroup_adap(getApplicationContext(), grouplist);

        list.setAdapter(adap);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                groupid = grouplist.get(position).getId();
                groupname = grouplist.get(position).getTopic();
//                headertext.setText(groupname);
                getSupportActionBar().setTitle(groupname);
                dialog1.dismiss();

            }
        });

        dialog1.show();

        dialog1.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });
        //d

    }

    @Override
    public void onBackPressed() {
        if(dialog1!=null)
        if(dialog1.isShowing())
            dialog1.dismiss();

        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {

        if(v.getId()==R.id.addimageid) {
            String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA};

            if(!hasPermissions(Post_question.this, PERMISSIONS)){
                Logg("checking","checking");

                ActivityCompat.requestPermissions(Post_question.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST);

            }
            else {
                ab.showOptionBottomSheetDialog();
            }
        }
        else if(v.getId()==R.id.answera_id) {
         st_ans="a";
            tv_ansa.setBackground(getResources().getDrawable(R.drawable.butgreen));
            tv_ansb.setBackground(getResources().getDrawable(R.drawable.cicleborder));
            tv_ansc.setBackground(getResources().getDrawable(R.drawable.cicleborder));
            tv_ansd.setBackground(getResources().getDrawable(R.drawable.cicleborder));
        }
        else if(v.getId()==R.id.answerb_id) {
            st_ans="b";
            tv_ansa.setBackground(getResources().getDrawable(R.drawable.cicleborder));
            tv_ansb.setBackground(getResources().getDrawable(R.drawable.butgreen));
            tv_ansc.setBackground(getResources().getDrawable(R.drawable.cicleborder));
            tv_ansd.setBackground(getResources().getDrawable(R.drawable.cicleborder));
        }
        else if(v.getId()==R.id.answerc_id) {
            st_ans="c";
            tv_ansa.setBackground(getResources().getDrawable(R.drawable.cicleborder));
            tv_ansb.setBackground(getResources().getDrawable(R.drawable.cicleborder));
            tv_ansc.setBackground(getResources().getDrawable(R.drawable.butgreen));
            tv_ansd.setBackground(getResources().getDrawable(R.drawable.cicleborder));
        }
        else if(v.getId()==R.id.answerd_id) {
            st_ans="d";
            tv_ansa.setBackground(getResources().getDrawable(R.drawable.cicleborder));
            tv_ansb.setBackground(getResources().getDrawable(R.drawable.cicleborder));
            tv_ansc.setBackground(getResources().getDrawable(R.drawable.cicleborder));
            tv_ansd.setBackground(getResources().getDrawable(R.drawable.butgreen));
        }
        else if(v.getId()==R.id.postquestionid) {

            st_que=et_que.getText().toString();
            st_a=et_opta.getText().toString();
            st_b=et_optb.getText().toString();
            st_c=et_optc.getText().toString();
            st_d=et_optd.getText().toString();

            //if(st_ans.length()>0) {
                if (count == 2) {
                    if (st_que.length() > 0 & st_a.length() > 0 & st_b.length() > 0) {

                        if (st_base46.length() > 1) {

                            post_tv.setText("Uploading..");
                            //postlay.setBackground(getResources().getDrawable(R.drawable.discussionfollowback));
                            postlay.setClickable(false);
                            uploadFile();
                        }
                        else
                            volley();
                    } else
                        Toast.makeText(getApplicationContext(), "Please fill all the fields", Toast.LENGTH_LONG).show();
                } else if (count == 3) {
                    if (st_que.length() > 0 & st_a.length() > 0 & st_b.length() > 0 & st_c.length() > 0) {

                        if (st_base46.length() > 1) {

                           // postlay.setBackground(getResources().getDrawable(R.drawable.discussionfollowback));
                            post_tv.setText("Uploading..");
                            postlay.setClickable(false);

                            uploadFile();
                        }
                        else
                            volley();
                    } else
                        Toast.makeText(getApplicationContext(), "Please fill all the fields", Toast.LENGTH_LONG).show();
                } else if (count == 4) {
                    if (st_que.length() > 0 & st_a.length() > 0 & st_b.length() > 0 & st_c.length() > 0 & st_d.length() > 0) {

                        if (st_base46.length() > 1) {
                           // postlay.setBackground(getResources().getDrawable(R.drawable.discussionfollowback));
                            post_tv.setText("Uploading..");
                            postlay.setClickable(false);

                            uploadFile();
                        }
                        else
                            volley();
                    } else
                        Toast.makeText(getApplicationContext(), "Please fill all the fields", Toast.LENGTH_LONG).show();
                }
//            }
//            else
//                Toast.makeText(getApplicationContext(), "Please select answer", Toast.LENGTH_LONG).show();

        }
        else if(v.getId()==R.id.addmoreoptionlayid) {

            if(count==2) {
                optionc_lay.setVisibility(View.VISIBLE);
                tv_ansc.setVisibility(View.VISIBLE);
                count=3;
            }
           else if(count==3) {
                deletc.setVisibility(View.GONE);
                optiond_lay.setVisibility(View.VISIBLE);
                tv_ansd.setVisibility(View.VISIBLE);
                count=4;
            }
        }
        else if(v.getId()==R.id.deleted_id) {
            optiond_lay.setVisibility(View.GONE);
            tv_ansd.setVisibility(View.GONE);
            deletc.setVisibility(View.VISIBLE);
            count=3;
        }
        else if(v.getId()==R.id.deletec_id) {
            optionc_lay.setVisibility(View.GONE);
            tv_ansc.setVisibility(View.GONE);
            count=2;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_CLICK_IMAGE) {

            new ImageCompression(camerimage).execute(imageFilePath);

        } else if (requestCode == REQUEST_CODE_GALLERY_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

//            String[] projection = {MediaStore.Images.Media.DATA};
//            Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
//            cursor.moveToFirst();
//            int columnIndex = cursor.getColumnIndex(projection[0]);
            File myFile = new File(uri.getPath());

            final String picturePath = myFile.getAbsolutePath();//cursor.getString(columnIndex); // returns null
           // cursor.close();

            //copy the selected file of gallery into app's sdcard folder and perform the compression operations on it.
            //And override the original image with the newly resized image.

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        CommonUtils.copyFile(picturePath, imageFilePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bmp = BitmapFactory.decodeFile(imageFilePath, options);

            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;

            Logg("actualheight2",actualHeight+" "+actualWidth);

            if(actualWidth>600 | actualHeight>600) {
                new ImageCompression(camerimage).execute(imageFilePath);
            }
            else{

                getContentResolver().notifyChange(uri, null);
                ContentResolver cr =getContentResolver();
                Bitmap bitmap;

                try
                {
                    bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, uri);
                    //Logg("bitmap_h_w",bitmap.getHeight()+" "+bitmap.getWidth()+"");
                    camerimage.setImageBitmap(bitmap);

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream .toByteArray();

                    st_base46 = Base64.encodeToString(byteArray, Base64.DEFAULT);
//                    return Base64.encodeToString(byteArray, Base64.DEFAULT);

                    Logg("base64",st_base46);

                }
                catch (Exception e)
                {

                }
            }
        }
    }

    public void uploadFile() {

        if(!nw.isConnectingToInternet()) {
//            swipeRefreshLayout.setRefreshing(false);
            internetconnection(2);
            return;
        }

        Constants.checkrefresh=true;

        progress = ProgressDialog.show(Post_question.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            progress.show();

        final JSONObject jsonObject=new JSONObject();
        try {

            jsonObject.put("username",username);
            jsonObject.put("collage",usercollage);
            jsonObject.put("question",st_que);
            jsonObject.put("a",st_a);
            jsonObject.put("b",st_b);
            jsonObject.put("c",st_c);
            jsonObject.put("d",st_d);
            jsonObject.put("ans",st_ans);
            jsonObject.put("uimage",image);
            jsonObject.put("groupname",groupname);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        final long time= System.currentTimeMillis();

        RequestQueue queue = Volley.newRequestQueue(Post_question.this);
        String url="";
        url = Constants.URL+"v3/upload_question_image.php?";
        url = url.replace(" ", "%20");
        //Logg("name", url);

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                //Logg("response", s);
                try {
                    if(s.length()>0)
                    {
                        JSONObject obj=new JSONObject(s);
                        if(obj.getString("status").equalsIgnoreCase("success"))
//                            finish();
                            successAlert();
                        else if(obj.getString("status").equalsIgnoreCase("block"))
                            Toast.makeText(Post_question.this,obj.getString("response"),Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(Post_question.this,"Something is wrong can't post comment",Toast.LENGTH_LONG).show();


                        progress.dismiss();
                        //Logg("response_string",s+" response");
                    }
                }

                catch (Exception e)
                {
                    progress.dismiss();
                    //Logg("e","e",e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(Post_question.this,CommonUtils.volleyerror(volleyError));
                progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("base64",st_base46);

                params.put("username",username);
                params.put("collage",usercollage);
                params.put("question",st_que);
                params.put("a",st_a);
                params.put("b",st_b);
                params.put("c",st_c);
                params.put("d",st_d);
                params.put("ans",st_ans);
                params.put("uimage",image);
                params.put("post_image","null");
                params.put("groupname",groupname);

                params.put("groupid", groupid+"");
                params.put("imagename",time+"");
                params.put("email",mail);

                return params;
            }
        };

        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void volley() {
        // TODO Auto-generated method stub

        if(!nw.isConnectingToInternet()) {
//            swipeRefreshLayout.setRefreshing(false);
            internetconnection(1);
            return;
        }

        Constants.checkrefresh=true;

        progress = ProgressDialog.show(Post_question.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        progress.show();

        final JSONObject jsonObject=new JSONObject();

        try {

            jsonObject.put("username",username);
            jsonObject.put("collage",usercollage);
            jsonObject.put("question",st_que);
            jsonObject.put("a",st_a);
            jsonObject.put("b",st_b);
            jsonObject.put("c",st_c);
            jsonObject.put("d",st_d);
            jsonObject.put("ans",st_ans);
            jsonObject.put("uimage",image);
            jsonObject.put("post_image","null");
            jsonObject.put("groupname",groupname);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url=Constants.URL+"v3/upload_question.php?";
        //Logg("url", url);

        RequestQueue que= Volley.newRequestQueue(getApplicationContext());

        StringRequest obj1=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String res) {
                // TODO Auto-generated method stub

                //Logg("response",res);
                int size=res.length();
                if(size>0)
                {

                    try {
                        JSONObject obj=new JSONObject(res);

                        if(obj.getString("status").equalsIgnoreCase("success"))
//                            finish();
                            successAlert();
                        else if(obj.getString("status").equalsIgnoreCase("block"))
                            Toast.makeText(Post_question.this,obj.getString("response"),Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(Post_question.this,"Something is wrong can't post comment",Toast.LENGTH_LONG).show();

                        progress.dismiss();

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        progress.dismiss();
                        //Logg("exception",e.toString());
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                // TODO Auto-generated method stub
                progress.dismiss();
                //Logg("error",e.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("username",username);
                params.put("collage",usercollage);
                params.put("question",st_que);
                params.put("a",st_a);
                params.put("b",st_b);
                params.put("c",st_c);
                params.put("d",st_d);
                params.put("ans",st_ans);
                params.put("uimage",image);
                params.put("post_image","null");
                params.put("groupname",groupname);
                params.put("email", mail);
                params.put("groupid",groupid+"");

                Logg("params",params+"");

                return params;
            }
        };

        int socketTimeout =0;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        obj1.setRetryPolicy(policy);
        que.add(obj1);

    }

    private void successAlert()
    {

        final Dialog dialog=new Dialog(Post_question.this,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.post_confirmation_dialog);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        TextView text,text1;
        text=(TextView)dialog.findViewById(R.id.text);
        text1=(TextView)dialog.findViewById(R.id.text1);
        TextView Submit=(TextView)dialog.findViewById(R.id.tv_submit);
        TextView Cancel=(TextView)dialog.findViewById(R.id.tv_cancel);

//        Cancel.setVisibility(View.VISIBLE);

//        if(Constants.checksolution)
//            dialog.setCancelable(false);

        text.setTypeface(font_medium);
        text1.setTypeface(font_medium);
        Submit.setTypeface(font_demi);
        Cancel.setTypeface(font_demi);

        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

                finish();

            }
        });
    }

    public void volley_getFollowGroup()
    {

        Logg("getGroup_call","get_group_call");

        if(!nw.isConnectingToInternet()) {
//            swipeRefreshLayout.setRefreshing(false);
            internetconnection(0);
            return;
        }

        grouplist=new ArrayList<gettr_settr>();

        progress = ProgressDialog.show(Post_question.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            progress.show();

        RequestQueue queue=Volley.newRequestQueue(Post_question.this);
        JSONObject obj=new JSONObject();
        String url=Constants.URL+"newapi2_04/get_group.php?email="+mail;
//        //Logg("Url",url);
        final JsonObjectRequest json=new JsonObjectRequest(Request.Method.POST, url, obj,new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                // TODO Auto-generated method stub

                //   //Logg("response",String.valueOf(res));
                try {

                    if(res.has("discuss")) {

                        JSONArray jr = res.getJSONArray("discuss");

                        for (int i = 0; i < jr.length(); i++) {

                            JSONObject jsonobj = jr.getJSONObject(i);

                            //Logg("followstatus "+i,jsonobj.getString("follow_status"));

                            if(jsonobj.getString("follow_status").equalsIgnoreCase("follow")&jsonobj.getString("status").equalsIgnoreCase("public")) {
                                grouplist.add(new gettr_settr(jsonobj.getString("topic"), jsonobj.getInt("comments"), jsonobj.getInt("id"),
                                        jsonobj.getString("member"), jsonobj.getString("follow_status"),
                                        jsonobj.getString("status"), jsonobj.getString("image"),jsonobj.getString("payment"),jsonobj.getString("payment_status")));
                            }
                        }
                        if(grouplist.size()>0)
                            dialoggroup();
                        else
                        {
                            Toast.makeText(Post_question.this,"You Have No Any Follow Group! Follow Atleast One Group",Toast.LENGTH_LONG).show();
                            finish();
                        }
                    }

                    progress.dismiss();

                }
                catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    progress.dismiss();
                    //Logg("no",e.getMessage());
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError e) {
                // TODO Auto-generated method stub
                progress.dismiss();
                //Logg("error discuss", "e",e);
            }
        });

        int socketTimeout = 50000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        json.setRetryPolicy(policy);
        queue.add(json);
    }

    public void internetconnection(final int i){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                if(i==0)
                    volley_getFollowGroup();
                else if(i==1)
                    volley();
                else if(i==2)
                    uploadFile();

//				getData();

            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

}
