package com.scholar.engineering.banking.ssc.getset;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by surender on 4/30/2017.
 */

public class HomeBean {

    @SerializedName("data")
    @Expose
    private ArrayList<Home_getset> data = null;

    public ArrayList<Home_getset> getData() {
        return data;
    }

    public void setData(ArrayList<Home_getset> data) {
        this.data = data;
    }

}
