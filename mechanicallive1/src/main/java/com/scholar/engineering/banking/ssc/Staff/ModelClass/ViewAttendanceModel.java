package com.scholar.engineering.banking.ssc.Staff.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ViewAttendanceModel {

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("status")
    @Expose
    public Boolean status;
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;
    @SerializedName("presentstudent")
    @Expose
    public List<String> presentstudent = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public List<String> getPresentstudent() {
        return presentstudent;
    }

    public void setPresentstudent(List<String> presentstudent) {
        this.presentstudent = presentstudent;
    }

    public static class Datum {

        @SerializedName("id")
        @Expose
        public Long id;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("password")
        @Expose
        public String password;
        @SerializedName("std_roll")
        @Expose
        public String stdRoll;
        @SerializedName("Student_name")
        @Expose
        public String studentName;
        @SerializedName("Student_class")
        @Expose
        public String studentClass;
        @SerializedName("Student_section")
        @Expose
        public String studentSection;
        @SerializedName("img")
        @Expose
        public String img;
        @SerializedName("F_name")
        @Expose
        public String fName;
        @SerializedName("F_organization")
        @Expose
        public String fOrganization;
        @SerializedName("F_occupation")
        @Expose
        public String fOccupation;
        @SerializedName("F_designation")
        @Expose
        public String fDesignation;
        @SerializedName("F_annual")
        @Expose
        public String fAnnual;
        @SerializedName("F_mobile")
        @Expose
        public String fMobile;
        @SerializedName("F_email")
        @Expose
        public String fEmail;
        @SerializedName("Fofficeaddress")
        @Expose
        public String fofficeaddress;
        @SerializedName("M_name")
        @Expose
        public String mName;
        @SerializedName("M_organization")
        @Expose
        public String mOrganization;
        @SerializedName("M_occupation")
        @Expose
        public String mOccupation;
        @SerializedName("M_designation")
        @Expose
        public String mDesignation;
        @SerializedName("M_annual")
        @Expose
        public String mAnnual;
        @SerializedName("M_mobile")
        @Expose
        public String mMobile;
        @SerializedName("M_email")
        @Expose
        public String mEmail;
        @SerializedName("Mofficeaddress")
        @Expose
        public String mofficeaddress;
        @SerializedName("L_name")
        @Expose
        public String lName;
        @SerializedName("L_mobile")
        @Expose
        public String lMobile;
        @SerializedName("L_relation")
        @Expose
        public String lRelation;
        @SerializedName("L_address")
        @Expose
        public String lAddress;
        @SerializedName("route_name")
        @Expose
        public String routeName;
        @SerializedName("vechile_no")
        @Expose
        public String vechileNo;
        @SerializedName("Driver_L_no")
        @Expose
        public String driverLNo;
        @SerializedName("contact_no")
        @Expose
        public String contactNo;
        @SerializedName("transport_type")
        @Expose
        public String transportType;
        @SerializedName("transport_details")
        @Expose
        public String transportDetails;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("updated_at")
        @Expose
        public String updatedAt;
        @SerializedName("F_telephone")
        @Expose
        public String fTelephone;
        @SerializedName("dob")
        @Expose
        public String dob;
        @SerializedName("bloodgroup")
        @Expose
        public String bloodgroup;
        @SerializedName("houseno")
        @Expose
        public String houseno;
        @SerializedName("phoneno")
        @Expose
        public String phoneno;
        @SerializedName("address")
        @Expose
        public String address;
        @SerializedName("studentimage")
        @Expose
        public String studentimage;
        @SerializedName("fatherimage")
        @Expose
        public String fatherimage;
        @SerializedName("motherimage")
        @Expose
        public String motherimage;
        @SerializedName("Foficemobile")
        @Expose
        public String foficemobile;
        @SerializedName("Mofficemobile")
        @Expose
        public String mofficemobile;
        @SerializedName("gaurdianimg")
        @Expose
        public String gaurdianimg;
        @SerializedName("staff")
        @Expose
        public String staff;
        @SerializedName("isActive")
        @Expose
        public Integer isActive;
        @SerializedName("isadmissionenquiry")
        @Expose
        public Long isadmissionenquiry;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getStdRoll() {
            return stdRoll;
        }

        public void setStdRoll(String stdRoll) {
            this.stdRoll = stdRoll;
        }

        public String getStudentName() {
            return studentName;
        }

        public void setStudentName(String studentName) {
            this.studentName = studentName;
        }

        public String getStudentClass() {
            return studentClass;
        }

        public void setStudentClass(String studentClass) {
            this.studentClass = studentClass;
        }

        public String getStudentSection() {
            return studentSection;
        }

        public void setStudentSection(String studentSection) {
            this.studentSection = studentSection;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getfName() {
            return fName;
        }

        public void setfName(String fName) {
            this.fName = fName;
        }

        public String getfOrganization() {
            return fOrganization;
        }

        public void setfOrganization(String fOrganization) {
            this.fOrganization = fOrganization;
        }

        public String getfOccupation() {
            return fOccupation;
        }

        public void setfOccupation(String fOccupation) {
            this.fOccupation = fOccupation;
        }

        public String getfDesignation() {
            return fDesignation;
        }

        public void setfDesignation(String fDesignation) {
            this.fDesignation = fDesignation;
        }

        public String getfAnnual() {
            return fAnnual;
        }

        public void setfAnnual(String fAnnual) {
            this.fAnnual = fAnnual;
        }

        public String getfMobile() {
            return fMobile;
        }

        public void setfMobile(String fMobile) {
            this.fMobile = fMobile;
        }

        public String getfEmail() {
            return fEmail;
        }

        public void setfEmail(String fEmail) {
            this.fEmail = fEmail;
        }

        public String getFofficeaddress() {
            return fofficeaddress;
        }

        public void setFofficeaddress(String fofficeaddress) {
            this.fofficeaddress = fofficeaddress;
        }

        public String getmName() {
            return mName;
        }

        public void setmName(String mName) {
            this.mName = mName;
        }

        public String getmOrganization() {
            return mOrganization;
        }

        public void setmOrganization(String mOrganization) {
            this.mOrganization = mOrganization;
        }

        public String getmOccupation() {
            return mOccupation;
        }

        public void setmOccupation(String mOccupation) {
            this.mOccupation = mOccupation;
        }

        public String getmDesignation() {
            return mDesignation;
        }

        public void setmDesignation(String mDesignation) {
            this.mDesignation = mDesignation;
        }

        public String getmAnnual() {
            return mAnnual;
        }

        public void setmAnnual(String mAnnual) {
            this.mAnnual = mAnnual;
        }

        public String getmMobile() {
            return mMobile;
        }

        public void setmMobile(String mMobile) {
            this.mMobile = mMobile;
        }

        public String getmEmail() {
            return mEmail;
        }

        public void setmEmail(String mEmail) {
            this.mEmail = mEmail;
        }

        public String getMofficeaddress() {
            return mofficeaddress;
        }

        public void setMofficeaddress(String mofficeaddress) {
            this.mofficeaddress = mofficeaddress;
        }

        public String getlName() {
            return lName;
        }

        public void setlName(String lName) {
            this.lName = lName;
        }

        public String getlMobile() {
            return lMobile;
        }

        public void setlMobile(String lMobile) {
            this.lMobile = lMobile;
        }

        public String getlRelation() {
            return lRelation;
        }

        public void setlRelation(String lRelation) {
            this.lRelation = lRelation;
        }

        public String getlAddress() {
            return lAddress;
        }

        public void setlAddress(String lAddress) {
            this.lAddress = lAddress;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getVechileNo() {
            return vechileNo;
        }

        public void setVechileNo(String vechileNo) {
            this.vechileNo = vechileNo;
        }

        public String getDriverLNo() {
            return driverLNo;
        }

        public void setDriverLNo(String driverLNo) {
            this.driverLNo = driverLNo;
        }

        public String getContactNo() {
            return contactNo;
        }

        public void setContactNo(String contactNo) {
            this.contactNo = contactNo;
        }

        public String getTransportType() {
            return transportType;
        }

        public void setTransportType(String transportType) {
            this.transportType = transportType;
        }

        public String getTransportDetails() {
            return transportDetails;
        }

        public void setTransportDetails(String transportDetails) {
            this.transportDetails = transportDetails;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getfTelephone() {
            return fTelephone;
        }

        public void setfTelephone(String fTelephone) {
            this.fTelephone = fTelephone;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getBloodgroup() {
            return bloodgroup;
        }

        public void setBloodgroup(String bloodgroup) {
            this.bloodgroup = bloodgroup;
        }

        public String getHouseno() {
            return houseno;
        }

        public void setHouseno(String houseno) {
            this.houseno = houseno;
        }

        public String getPhoneno() {
            return phoneno;
        }

        public void setPhoneno(String phoneno) {
            this.phoneno = phoneno;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getStudentimage() {
            return studentimage;
        }

        public void setStudentimage(String studentimage) {
            this.studentimage = studentimage;
        }

        public String getFatherimage() {
            return fatherimage;
        }

        public void setFatherimage(String fatherimage) {
            this.fatherimage = fatherimage;
        }

        public String getMotherimage() {
            return motherimage;
        }

        public void setMotherimage(String motherimage) {
            this.motherimage = motherimage;
        }

        public String getFoficemobile() {
            return foficemobile;
        }

        public void setFoficemobile(String foficemobile) {
            this.foficemobile = foficemobile;
        }

        public String getMofficemobile() {
            return mofficemobile;
        }

        public void setMofficemobile(String mofficemobile) {
            this.mofficemobile = mofficemobile;
        }

        public String getGaurdianimg() {
            return gaurdianimg;
        }

        public void setGaurdianimg(String gaurdianimg) {
            this.gaurdianimg = gaurdianimg;
        }

        public String getStaff() {
            return staff;
        }

        public void setStaff(String staff) {
            this.staff = staff;
        }

        public Integer getIsActive() {
            return isActive;
        }

        public void setIsActive(Integer isActive) {
            this.isActive = isActive;
        }

        public Long getIsadmissionenquiry() {
            return isadmissionenquiry;
        }

        public void setIsadmissionenquiry(Long isadmissionenquiry) {
            this.isadmissionenquiry = isadmissionenquiry;
        }
    }
}
