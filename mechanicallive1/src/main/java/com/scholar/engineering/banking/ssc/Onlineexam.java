package com.scholar.engineering.banking.ssc;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.scholar.engineering.banking.ssc.adapter.Home_RecyclerViewAdapter2;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.EndlessRecyclerViewScrollListener;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;

import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.toast;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.volleyerror;

public class Onlineexam extends AppCompatActivity {
    TextView emptyview;
    RecyclerView list;
    public static Home_RecyclerViewAdapter2 adapter;
    ArrayList<Home_getset> homedata;
    ProgressDialog progress;
    SwipeRefreshLayout swipeRefreshLayout;
    int index = 0;
    NetworkConnection nw;
    Typeface font_demi, font_medium;
    CardView cv_filter;
    public static JSONArray search_result;
    RequestQueue queue;
	LinearLayoutManager mLayoutManager;
	EndlessRecyclerViewScrollListener endless;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.onlinetest_all);
        queue = Volley.newRequestQueue(this);
        nw = new NetworkConnection(this);
        emptyview = findViewById(R.id.emptyview);
        font_demi = Typeface.createFromAsset(getAssets(), "avenirnextdemibold.ttf");
        font_medium = Typeface.createFromAsset(getAssets(), "avenirnextmediumCn.ttf");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Online Test");
        Utility.applyFontForToolbarTitle(toolbar, this);

        cv_filter = (CardView) findViewById(R.id.cv_filter);

        list = (RecyclerView) findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(Onlineexam.this);
        list.setLayoutManager(mLayoutManager);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        homedata = new ArrayList<>();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                index = 0;
                homedata = new ArrayList<>();
                getData();

				endless = new EndlessRecyclerViewScrollListener(mLayoutManager) {
					@Override
					public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
						if (homedata.size() % 10 == 0) {
							index++;
							getData();
						}
					}
				};
				list.addOnScrollListener(endless);
            }
        });

        getData();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        endless = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (homedata.size() % 10 == 0) {
                    index++;
                    getData();
                }
            }
        };
        list.addOnScrollListener(endless);
        cv_filter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                successAlert();
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logg("call_onDestroy()", "Call onDestroy()");
        deleteCache(this);
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public void getData() {

        Logg("index", index + " ");
        if (!nw.isConnectingToInternet()) {
            swipeRefreshLayout.setRefreshing(false);
            internetconnection(0);
            return;
        }

        if (index == 0) {
            progress = ProgressDialog.show(Onlineexam.this, null, null, true);
            progress.setContentView(R.layout.progressdialog);
            progress.setCanceledOnTouchOutside(false);
            progress.setCancelable(false);
            progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progress.show();
        }
        final JSONObject obj = new JSONObject();
        try {
            obj.put("email", Constants.User_Email);
            obj.put("index", index);
            obj.put("fieldtype", "2");
            obj.put("admission_no", Constants.addmissionno);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url = Constants.URL_LV + "homedata";
        Logg("homedata", url + " , " + obj);

        JsonObjectRequest json = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
				if(progress!=null)
					progress.dismiss();
				swipeRefreshLayout.setRefreshing(false);
                try {
                    Logg("homedata", res.toString());
                    if (res.has("data")) {
                        JSONArray jr = res.getJSONArray("data");
                        if (jr.length() > 0) {
                            list.setVisibility(View.VISIBLE);
                            emptyview.setVisibility(View.GONE);
                            for (int i = 0; i < jr.length(); i++) {
                                JSONObject ob = jr.getJSONObject(i);
                                Home_getset object = new Home_getset();
                                object.setId(ob.getInt("id"));
                                object.setTimestamp(ob.getString("timestamp"));
                                object.setLikecount(ob.getInt("likes"));
                                object.setCommentcount(ob.getInt("comment"));
                                object.setViewcount(ob.getInt("view"));
                                object.setUid(ob.getString("uid"));
                                object.setPosttype(ob.getString("posttype"));
                                object.setGroupid(ob.getString("groupid"));
                                object.setFieldtype(ob.getString("field_type"));
                                object.setJsonfield(ob.getString("jsondata"));
                                object.setLikestatus(ob.getString("likestatus"));
                                object.setPosturl(ob.getString("posturl"));
                                object.setPostdescription(ob.getString("post_description"));
                                if (ob.has("AppVersion"))
                                    object.setAppVersion(ob.getString("AppVersion"));

                                homedata.add(object);
                                adapter = new Home_RecyclerViewAdapter2(Onlineexam.this, homedata, "onlineexam");
                                list.setAdapter(adapter);
                            }
                        } else {
                            list.setVisibility(View.GONE);
                            emptyview.setVisibility(View.VISIBLE);
                        }

                    } else {

                        list.setVisibility(View.GONE);
                        emptyview.setVisibility(View.VISIBLE);
                    }
                }
                catch (JSONException e) {
                    list.setVisibility(View.GONE);
                    emptyview.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError arg0) {
                if (progress != null)
                    progress.dismiss();
                list.setVisibility(View.GONE);
                emptyview.setVisibility(View.VISIBLE);
                swipeRefreshLayout.setRefreshing(false);
                toast(Onlineexam.this, volleyerror(arg0));
            }
        });
        json.setShouldCache(false);
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        json.setRetryPolicy(policy);
        queue.add(json);
    }


    public void getSearchData(final String d_from, final String d_to, final String testname) {

        if (!nw.isConnectingToInternet()) {
            internetconnection(2);
            return;
        }

        Constants.checkrefresh = true;

        progress = ProgressDialog.show(Onlineexam.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final long time = System.currentTimeMillis();

        RequestQueue queue = Volley.newRequestQueue(Onlineexam.this);
        String url = Constants.URL_LV+ "search_test";
        Logg("name", url);

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                progress.dismiss();

                Logg("response", s);
                try {

                    search_result = new JSONArray();

                    if (s.length() > 0) {
                        JSONObject obj = new JSONObject(s);
                        if (obj.has("data")) {

                            search_result = obj.getJSONArray("data");

                            Intent in = new Intent(Onlineexam.this, OnlineExam_filter.class);

                            startActivity(in);
                        }
                    }
                } catch (JSONException e) {
                    progress.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(Onlineexam.this, CommonUtils.volleyerror(volleyError));
                progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("date_from", date_from);
                params.put("date_to", date_to);
                params.put("search", testname);
                params.put("user_id", Constants.User_Email);
                params.put("admission_no", Constants.addmissionno);
                Logg("search_param", params.toString());

                return params;
            }
        };

        request.setShouldCache(false);
        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    public void internetconnection(final int i) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                swipeRefreshLayout.setRefreshing(true);
                index=0;
                homedata=new ArrayList<>();
                getData();

            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    TextView tv_date_from, tv_date_to;

    private void successAlert() {

        final Dialog dialog = new Dialog(Onlineexam.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.test_filter_dialog);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
//		TextView tv_date_from,tv_date_to;
        tv_date_from = (TextView) dialog.findViewById(R.id.tv_datefrom);
        tv_date_to = (TextView) dialog.findViewById(R.id.tv_dateto);
        final EditText et_testname = (EditText) dialog.findViewById(R.id.et_testname);
        final LinearLayout lay_date = (LinearLayout) dialog.findViewById(R.id.lay_date);
        final RadioButton rb_text, rb_date;

        rb_text = (RadioButton) dialog.findViewById(R.id.rb_testname);
        rb_date = (RadioButton) dialog.findViewById(R.id.rb_date);

        TextView Submit = (TextView) dialog.findViewById(R.id.tv_submit);
        TextView Cancel = (TextView) dialog.findViewById(R.id.tv_cancel);

//        Cancel.setVisibility(View.VISIBLE);

        rb_text.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    et_testname.setVisibility(View.VISIBLE);
                    lay_date.setVisibility(View.GONE);
                } else {
                    lay_date.setVisibility(View.VISIBLE);
                    et_testname.setVisibility(View.GONE);
                }
            }
        });

//		rb_text.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//			@Override
//			public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//				if(!b){
//					et_testname.setVisibility(View.VISIBLE);
//					lay_date.setVisibility(View.GONE);
//				}
//				else {
//					lay_date.setVisibility(View.VISIBLE);
//					et_testname.setVisibility(View.GONE);
//				}
//			}
//		});

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        date_to = formatter.format(date);
        date_from = getYesterday();
        tv_date_from.setText(date_from);
        tv_date_to.setText(date_to);

        tv_date_from.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                datePicker(0);
            }
        });

        tv_date_to.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                datePicker(1);
            }
        });

//        if(Constants.checksolution)
//            dialog.setCancelable(false);

        tv_date_from.setTypeface(font_medium);
        tv_date_to.setTypeface(font_medium);

        Submit.setTypeface(font_demi);
        Cancel.setTypeface(font_demi);

        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (rb_text.isChecked() & et_testname.getText().toString().length() < 1) {
                    Toast.makeText(Onlineexam.this, "Enter Test Name", Toast.LENGTH_SHORT).show();
                    date_from = "";
                    date_to = "";
//					getSearchData(date_from,date_to,et_testname.getText().toString());
                } else if (rb_text.isChecked()) {
                    date_from = "";
                    date_to = "";
                    getSearchData(date_from, date_to, et_testname.getText().toString());
                    dialog.dismiss();
                } else {
                    et_testname.setText("");
                    getSearchData(date_from, date_to, "");
                    dialog.dismiss();
                }

//				finish();

            }
        });
    }

    private String getYesterday() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(yesterday());
    }

    private Date yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    String date_from, date_to;

    private void datePicker(final int value) {

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        if (value == 0)
                            tv_date_from.setText(date_from = convertDateformat(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth));
                        else
                            tv_date_to.setText(date_to = convertDateformat(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth));

                        //*************Call Time Picker Here ********************

                        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");

// tiemPicker();
                    }

                }, mYear, mMonth, mDay);

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);

        datePickerDialog.show();
    }

    public String convertDateformat(String dt) {
        String dateValue = "";
        Date date = null;
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = input.parse(dt);

            dateValue = input.format(date);

        } catch (ParseException e) {
        }

        Logg("converteddate", dateValue + " " + date);
        return dateValue;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

}
	

