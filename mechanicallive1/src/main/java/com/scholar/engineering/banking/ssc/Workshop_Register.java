package com.scholar.engineering.banking.ssc;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

/**
 * Created by surender on 4/17/2017.
 */
public class Workshop_Register extends AppCompatActivity {

    LinearLayout lay_dynamic;
    EditText et_dynamic;
    TextView tv_submit;
    int postid;
    String amount;
    JSONArray jsonarray,jsonarray_submit;
    TextInputLayout textInputLayout;
    Typeface font_medium;
    NetworkConnection nw;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.workshop_register);

        nw=new NetworkConnection(this);

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Register");
        Utility.applyFontForToolbarTitle(toolbar,this);
        TextView header=(TextView)findViewById(R.id.headertextid);
        header.setVisibility(View.GONE);

        font_medium = Typeface.createFromAsset(Workshop_Register.this.getAssets(), "avenirnextmediumCn.ttf");

        lay_dynamic=(LinearLayout)findViewById(R.id.layout);
        tv_submit=(TextView)findViewById(R.id.tv_submit);

        amount=getIntent().getStringExtra("amount");
        postid=getIntent().getIntExtra("postid",0);

        getData();

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getfieldData();
            }
        });


    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logg("call_onDestroy()","Call onDestroy()");
        deleteCache(this);
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {}
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public void getData()
    {
        jsonarray=new JSONArray();

        if(!nw.isConnectingToInternet()) {
//            swipeRefreshLayout.setRefreshing(false);
            internetconnection(0);
            return;
        }

        RequestQueue queue= Volley.newRequestQueue(Workshop_Register.this);
        JSONObject obj=new JSONObject();
        String url= Constants.URL+"newapi2_04/getworkshop_registerfield.php?postid="+postid;

        //Logg("checkversion_url",url);

        final JsonObjectRequest json=new JsonObjectRequest(Request.Method.POST, url, obj,new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                // TODO Auto-generated method stub
                try {

                    //Logg("response",res+"");

                    JSONObject object=res.getJSONObject("field");

                    JSONObject jsondata=new JSONObject(object.getString("jsondata"));

                    jsonarray = new JSONArray(jsondata.getString("field"));

                    //Logg("jsonarray",jsonarray+" ");

                    setData();

//                    for(int i=0;i<jsonarray.length();i++)
//                    {
//                        JSONObject ob=jsonarray.getJSONObject(i);
//
//                    }

                }
                catch (JSONException e) {
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                // TODO Auto-generated method stub
                //Logg("checkversion error", String.valueOf(arg0));
            }
        });
        queue.add(json);
    }

    public static class TableData{

        public final int tv_tag;

        public TableData(int tag) {
            tv_tag= tag;
        }
    }

    public void setData(){

        for(int i=0;i<jsonarray.length();i++){

            try {
                JSONObject obj=jsonarray.getJSONObject(i);

                View child = getLayoutInflater().inflate(R.layout.workshop_edittext, null);
                et_dynamic=(EditText)child.findViewById(R.id.et_workshop);
                textInputLayout = (TextInputLayout)child.findViewById(R.id.input_layout_workshop);
                //et_dynamic.setHint(obj.getString("hint"));
                 /*vh1.tv_price.setText(jsonobj.getString("payment"));*/
                StringBuilder sb = new StringBuilder(obj.getString("hint"));
                sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
                textInputLayout.setHint(sb.toString());
                textInputLayout.setTypeface(font_medium);

//            et_dynamic.setTag(new TableData((i)));
//            tv1.setTag(new TableData((i)));
//            tv_questioncount.setOnClickListener(Play_new.this);

                lay_dynamic.addView(child);


            } catch (JSONException e) {

            }

        }

    }

    public void getfieldData(){
        try {
            jsonarray_submit=new JSONArray();

         for(int i=0;i<jsonarray.length();i++){

                JSONObject obj=jsonarray.getJSONObject(i);

                    View vi = new View(Workshop_Register.this);
                    vi = lay_dynamic.getChildAt(i);
                    EditText et=(EditText)vi.findViewById(R.id.et_workshop);


                    if(et.getText().toString().length()<1){
                        Toast.makeText(this,"Please enter valid "+obj.getString("hint"),Toast.LENGTH_LONG).show();
                        break;
                    }
                    else
                    {
                     JSONObject o=new JSONObject();
                        o.put("hint",obj.getString("hint"));
                        o.put("value",et.getText().toString());
                        jsonarray_submit.put(o);

                        if(i==jsonarray.length()-1){
                            //submit data;
                            submitData();
                        }
                    }
         }
        } catch (JSONException e) {

        }
    }

    public void submitData() {


        if(!nw.isConnectingToInternet()) {
//            swipeRefreshLayout.setRefreshing(false);
            internetconnection(1);
            return;
        }


        final ProgressDialog pd = new ProgressDialog(Workshop_Register.this);
        pd.setCancelable(true);
        pd.setMessage("Uploading Please Wait...");
        pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        pd.show();

        RequestQueue queue = Volley.newRequestQueue(Workshop_Register.this);
        String url="";

        url = Constants.URL +"newapi2_04/submit_workshop_register.php?";

        url = url.replace(" ", "%20");

        //Logg("name", url);

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                //Logg("response", s);
                try {
                    if(s.length()>0)
                    {
                        JSONObject obj=new JSONObject(s);
                        s=obj.getString("status");

                        if(s.equalsIgnoreCase("success")==true)
                        {

                            JSONObject jsondata=new JSONObject();
                            if(amount.equalsIgnoreCase("free")|amount.equalsIgnoreCase("0")|amount.equalsIgnoreCase("")) {
                                Utility.Payment_success(Workshop_Register.this);
//                                Toast.makeText(Workshop_Register.this,"Workshop Register Successfully",Toast.LENGTH_LONG).show();
//                                finish();
                            }
                            else {
                                jsondata.put("amount", Integer.valueOf(amount));
                                jsondata.put("payment by", "online");

                                Transaction t = new Transaction(Workshop_Register.this,"5",String.valueOf(postid),Integer.valueOf(amount));
                                t.PAYMENT_REQUEST(Workshop_Register.this);

//                                Intent in = new Intent(Workshop_Register.this, Transaction.class);
//                                in.putExtra("postid", postid);
//                                in.putExtra("field_type", "5");
//                                in.putExtra("amount", Integer.valueOf(amount));
//                                startActivity(in);
//                                finish();
                            }

                        }
                        else if(s.equalsIgnoreCase("user already register")){
                            Toast.makeText(Workshop_Register.this,"User Already Registered",Toast.LENGTH_LONG).show();
                            finish();
                        }
                        else
                        {
                            Toast.makeText(Workshop_Register.this,"Somthing is Wrong Can't Upload, Please Try Againe",Toast.LENGTH_LONG).show();
                        }

                        pd.dismiss();
                        //Logg("response_string",s+" response");
                    }
                }

                catch (Exception e)
                {
                    pd.dismiss();
                    //Logg("e","e",e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(Workshop_Register.this,CommonUtils.volleyerror(volleyError));
                pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                //id="+psid+"&comment="+cmnt+"&user="+username+"&collage="+collage+"&image="+image+"&email="+mail;

                Map<String, String> params = new HashMap<String, String>();
                params.put("postid", postid+"");
                params.put("email", User_Email);
                params.put("jsondata", jsonarray_submit+"");

                return params;
            }
        };

        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    public void internetconnection(final int i){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                if(i==0)
                    getData();
                else if(i==1)
                    submitData();


//				getData();

            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

}


