package com.scholar.engineering.banking.ssc.newScreens.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.numberprogressbar.NumberProgressBar;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.newScreens.NotesPdfGetSet;
import com.scholar.engineering.banking.ssc.newScreens.QueryForms.QueryGetSet;
import com.scholar.engineering.banking.ssc.newScreens.QueryForms.QueryResponseDetails;
import com.scholar.engineering.banking.ssc.newScreens.pdfviewclass;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
import static com.scholar.engineering.banking.ssc.utils.Utility.dirChecker;

public class QuerylistAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context cs;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    ImageLoader imageLoader;

    UserSharedPreferences preferences;

    ArrayList<QueryGetSet> list;


    public QuerylistAdapter(Context cs, ArrayList<QueryGetSet> data) {
        this.cs = cs;
        this.list = data;
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(cs));
        preferences = UserSharedPreferences.getInstance(cs);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.blankimage)
                .showImageForEmptyUri(R.drawable.congrrr)
                .showImageOnFail(R.drawable.congrrr)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View v0 = inflater.inflate(R.layout.querylistitems, viewGroup, false);
        viewHolder = new ViewHolder0(v0);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        ViewHolder0 vh0 = (ViewHolder0) viewHolder;
        vh0.ticketno.setText("Ticket No: "+list.get(position).getTicketno());
        vh0.issue.setText("Issue: " + list.get(position).getIssue());
        vh0.issuedetail.setText("Detail: "+list.get(position).getDetails());
        if(list.get(position).getStatus().equals("2"))
            vh0.status.setText("Status: complete");
        else
            vh0.status.setText("Status: Pending");

        vh0.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(cs, QueryResponseDetails.class);
                in.putExtra("postid", list.get(position).getId());
                in.putExtra("postdata", list.get(position).getData());
                cs.startActivity(in);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder0 extends RecyclerView.ViewHolder {

        TextView ticketno, issue,issuedetail,status;

        public ViewHolder0(View bn) {
            super(bn);
            issuedetail = bn.findViewById(R.id.issuedetail);
            ticketno = bn.findViewById(R.id.ticketno);
            issue = bn.findViewById(R.id.issue);
            status = bn.findViewById(R.id.status);
        }
    }

    public static String URL, url;
    public static Dialog dialog;
    public static NumberProgressBar bar;
    public static Context context;
    public static String pdfname;

    public static void downloadpdf(final Context c, String pdfurl, String name) {

        url = pdfurl;
        context = c;
        pdfname = name;
        Logg("downloadpdf", "call " + url);

        dialog = new Dialog(c);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.test_download_pdf);

        // inflate and adjust layout
        // dialog.setContentView(layout1);

        bar = (NumberProgressBar) dialog.findViewById(R.id.numberbar1);
        //adView=(AdView)dialog.findViewById(R.id.adView);
        dialog.show();
        dialog.setCancelable(true);

        //progtext=(TextView)dialog.findViewById(R.id.progresstextid);

        URL = url;

        String[] urlarr = URL.split("/");
        int len = urlarr.length;
        //Toast.makeText(c,"len "+len,200).show();
        URL = urlarr[len - 1];

        Logg("urlllll", URL);
        new DownloadFile().execute(url);

    }

    public static class DownloadFile extends AsyncTask<String, Integer, String> {

        String filepath;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                }
            }, 5000);
        }

        @Override
        protected String doInBackground(String... Url) {
            try {
                java.net.URL url = new URL(Url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();

                // Detect the file lenghth
                int fileLength = connection.getContentLength();

                // Locate storage location
                filepath = Environment.getExternalStorageDirectory().getAbsolutePath();
                //.getPath();

                Logg("filepathfilepath", filepath + " , " + URL);

                File myDir = new File(filepath + "/GyanStrot/");
                myDir.mkdirs();
                File file = new File(myDir, pdfname);
                Logg("filepathfilepath2", filepath + " , " + URL);
                if (file.exists()) {
                    Logg("filepathfilepath3", filepath + " , " + URL);
//                     file.delete();
                } else {
                    // Download the file
                    Logg("filepathfilepath4", filepath + " , " + URL);
                    InputStream input = new BufferedInputStream(url.openStream());

                    // Save the downloaded file

                    OutputStream output = new FileOutputStream(file);

                    byte data[] = new byte[1024];
                    long total = 0;
                    int count;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        // Publish the progress
                        publishProgress((int) (total * 100 / fileLength));
                        output.write(data, 0, count);
                    }
                    // Close connection
                    output.flush();
                    output.close();
                    input.close();
                }
            } catch (Exception e) {
                Log.e("exception", "", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            boolean b = dirChecker(Constants.ImageDirectory);
            //unpackZip(filepath + "/Topper's Club/",Constants.ImageDirectory+".zip");
            Logg("filepathfilepath6", b + "");
            dialog.dismiss();
            CommonUtils.toast(context, "Pdf downloaded");

        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // Update the progress dialog
            bar.setProgress(progress[0]);
            Logg("progress", progress[0].toString());
            // progtext.setText(String.valueOf(progress[0])+"%");

        }
    }
}
