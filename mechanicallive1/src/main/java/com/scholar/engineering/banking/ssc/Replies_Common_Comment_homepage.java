package com.scholar.engineering.banking.ssc;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;

import androidx.core.widget.NestedScrollView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.adapter.Replies_Comment_Common_homepage_Adapter;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.ImageCompression;
import com.scholar.engineering.banking.ssc.utils.ImageUploading;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
import com.scholar.engineering.banking.ssc.utils.TouchImageView;
import com.scholar.engineering.banking.ssc.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.toast;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.volleyerror;
import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;
import static com.scholar.engineering.banking.ssc.utils.Constants.addmissionno;
import static com.scholar.engineering.banking.ssc.utils.Constants.imageFilePath;
import static com.scholar.engineering.banking.ssc.utils.Constants.st_base46;
import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Replies_Common_Comment_homepage extends AppCompatActivity {


    String commentid;
    RecyclerView replist;
    EditText comtext;
    ImageView compost, camera_img;
    Replies_Comment_Common_homepage_Adapter repadap;
    int index = 0;
    RelativeLayout loadmore_lay;
    TextView tv_reply;
    TextView moreButton;
    Boolean cb = false;
    String st_imagename;
    Uri selectedImage;
    JSONArray jsonArray;
    ImageView userimg, cimage;
    TextView tv_uname, tv_collage, tv_time, tv_comment;
    ProgressDialog progress;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    NestedScrollView nestedscroll;
    LinearLayoutManager mLayoutManager;

    NetworkConnection nw;
    ImageUploading ab;
    private final int REQUEST_CODE_CLICK_IMAGE = 1002, REQUEST_CODE_GALLERY_IMAGE = 1003;
    RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.replies_common_comment);

        nw = new NetworkConnection(this);

        st_base46 = "";
        Constants.imageFilePath = CommonUtils.getFilename();

        queue = Volley.newRequestQueue(getApplicationContext());
        progress = ProgressDialog.show(Replies_Common_Comment_homepage.this, null, null, true);
        progress.setContentView(R.layout.progressdialog);
        progress.setCanceledOnTouchOutside(false);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Utility.applyFontForToolbarTitle(toolbar, this);
        nestedscroll = (NestedScrollView) findViewById(R.id.nestedscroll);
        replist = (RecyclerView) findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(Replies_Common_Comment_homepage.this);
        replist.setLayoutManager(mLayoutManager);
        replist.setNestedScrollingEnabled(false);
        replist.setHasFixedSize(true);

        if (Build.VERSION.SDK_INT >= 24) {
            try {
                java.lang.reflect.Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {

            }
        }

        comtext = (EditText) findViewById(R.id.comtextid);
        compost = (ImageView) findViewById(R.id.postcomid);
        camera_img = (ImageView) findViewById(R.id.iv_camera_id);

        tv_uname = (TextView) findViewById(R.id.usernameid);
        tv_collage = (TextView) findViewById(R.id.collegeid);
        tv_time = (TextView) findViewById(R.id.timeid);
        tv_comment = (TextView) findViewById(R.id.commentid);
        cimage = (ImageView) findViewById(R.id.commentimageid);
        userimg = (CircleImageView) findViewById(R.id.userimage_id);

        moreButton = (TextView) findViewById(R.id.moreButton);
        loadmore_lay = (RelativeLayout) findViewById(R.id.footer_layout);


        jsonArray = new JSONArray();
        commentid = getIntent().getStringExtra("commentid");
        ab = new ImageUploading(this, camera_img);
        camera_img.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ab.createBottomSheetDialog();
                ab.showOptionBottomSheetDialog();
            }
        });

        setTypeface();

        volleylist();

        moreButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {

                try {

                    if (jsonArray.length() > 0) {

                        JSONObject obj = jsonArray.getJSONObject(0);

                        index = obj.getInt("id");
                    } else {
                        index = 0;
                    }

                    volleylist();

                } catch (JSONException e) {

                }
            }
        });

        compost.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String s = comtext.getText().toString();
                if (s.length() < 1) {
                    Toast.makeText(getApplicationContext(), "Enter comments", Toast.LENGTH_SHORT).show();
                } else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(comtext.getWindowToken(), 0);
                    cb = true;

                    if (st_base46.length() > 1)
                        imagevolley();
                    else
                        volleycominsert();
                }
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        deleteCache(this);
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public void setCommentdata(JSONObject jsonobj) {

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.blankimage)
                .showImageForEmptyUri(R.drawable.congrrr)
                .showImageOnFail(R.drawable.congrrr)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        try {

            JSONObject jsonfield = new JSONObject(jsonobj.getString("jsondata"));

            String time = getDate(jsonobj.getString("timestamp"));

            tv_time.setText(time);

            tv_collage.setText(jsonfield.getString("collage")+"-"+jsonfield.getString("section"));

            tv_uname.setText(jsonfield.getString("name"));

            String com = jsonfield.getString("comments");

            com = com.replaceAll("%20", " ");

            tv_comment.setText(com);

            if (!jsonfield.getString("userimage").equalsIgnoreCase("null")) {
                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.init(ImageLoaderConfiguration.createDefault(Replies_Common_Comment_homepage.this));
                imageLoader.getInstance().displayImage(jsonfield.getString("userimage"), userimg, options, animateFirstListener);
            } else
                userimg.setImageResource(R.drawable.user_default);

            if (!jsonfield.getString("com_image").equalsIgnoreCase("null")) {
                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.init(ImageLoaderConfiguration.createDefault(Replies_Common_Comment_homepage.this));
                imageLoader.getInstance().displayImage(Constants.URL_Image + jsonfield.getString("com_image"), cimage, options, animateFirstListener);
                cimage.setVisibility(View.VISIBLE);
            } else
                cimage.setVisibility(View.GONE);

            cimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cimage.getDrawable() == null) {
                    } else {
                        imagedialog(cimage);
                    }
                }
            });

        } catch (JSONException e) {

        }


    }

    public void SelectImage() {
        final CharSequence[] options = {" ", "Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(Replies_Common_Comment_homepage.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {

                    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    File photo = null;
                    try {
                        // place where to store camera taken picture
                        photo = Utility.createTemporaryFile("picture", ".jpg");
                        photo.delete();
                    } catch (Exception e) {
                        Log.v("exception", "e", e);
                        Toast.makeText(getApplicationContext(), "Please check SD card! Image shot is impossible!", Toast.LENGTH_LONG).show();
//                        return false;
                    }
                    selectedImage = Uri.fromFile(photo);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImage);
                    //start camera intent
                    startActivityForResult(intent, 1);


                } else if (options[item].equals("Choose from Gallery")) {

                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
        builder.setCancelable(false);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_CLICK_IMAGE) {

            new ImageCompression(camera_img).execute(imageFilePath);

        } else if (requestCode == REQUEST_CODE_GALLERY_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

//            String[] projection = {MediaStore.Images.Media.DATA};
//            Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
//            cursor.moveToFirst();
//            int columnIndex = cursor.getColumnIndex(projection[0]);
            File myFile = new File(uri.getPath());

            final String picturePath = myFile.getAbsolutePath();//cursor.getString(columnIndex); // returns null
            // cursor.close();

            //copy the selected file of gallery into app's sdcard folder and perform the compression operations on it.
            //And override the original image with the newly resized image.

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        CommonUtils.copyFile(picturePath, imageFilePath);
                    } catch (IOException e) {

                    }
                }
            });

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bmp = BitmapFactory.decodeFile(imageFilePath, options);

            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;

            Logg("actualheight2", actualHeight + " " + actualWidth);

            if (actualWidth > 600 | actualHeight > 600) {
                new ImageCompression(camera_img).execute(imageFilePath);
            } else {

                getContentResolver().notifyChange(uri, null);
                ContentResolver cr = getContentResolver();
                Bitmap bitmap;

                try {
                    bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, uri);
                    //Logg("bitmap_h_w",bitmap.getHeight()+" "+bitmap.getWidth()+"");
                    camera_img.setImageBitmap(bitmap);

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();

                    st_base46 = Base64.encodeToString(byteArray, Base64.DEFAULT);
//                    return Base64.encodeToString(byteArray, Base64.DEFAULT);

                    Logg("base64", st_base46);

                } catch (Exception e) {

                }
            }
        }
    }


    public void volleylist() {
        if (!nw.isConnectingToInternet()) {
            internetconnection(0);
            return;
        }

        if (index == 0)
            progress.show();


        JSONObject obj = new JSONObject();
        try {
            obj.put("commentid", commentid);
            obj.put("index", index);
            obj.put("uid", User_Email);
            obj.put("admission_no", addmissionno);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = Constants.URL_LV + "get_common_replies";
        Logg("get_common_replies", url);

        JsonObjectRequest json = new JsonObjectRequest(Method.POST, url, obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject res) {
                try {

                    Logg("get_common_replies", res + "");
                    if (index == 0)
                        if (res.has("commentdetail")) {
                            setCommentdata(res.getJSONObject("commentdetail"));
                        }

                    if (res.has("data")) {
                        JSONArray jr = res.getJSONArray("data");

                        if (jr.length() >= 10)
                            loadmore_lay.setVisibility(View.VISIBLE);
                        else
                            loadmore_lay.setVisibility(View.GONE);

                            jsonArray = new JSONArray(new ArrayList<String>());
                            jsonArray = jr;
                            repadap = new Replies_Comment_Common_homepage_Adapter(Replies_Common_Comment_homepage.this, jsonArray);
                            replist.setAdapter(repadap);
                            nestedscroll.fullScroll(View.FOCUS_DOWN);
                    }
                    loadmore_lay.setVisibility(View.GONE);
                    progress.dismiss();

                } catch (JSONException e) {
                    loadmore_lay.setVisibility(View.GONE);
                    progress.dismiss();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                toast(Replies_Common_Comment_homepage.this, volleyerror(arg0));
                loadmore_lay.setVisibility(View.GONE);
                progress.dismiss();

            }
        });

        json.setShouldCache(false);
        int socketTimeout = 20000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        json.setRetryPolicy(policy);
        queue.add(json);
    }

    public void imagevolley() {
        if (!nw.isConnectingToInternet()) {
            internetconnection(2);
            return;
        }

        progress.show();
        st_imagename = String.valueOf(System.currentTimeMillis());

        String url = Constants.URL_LV + "insert_common_comment_replies_image";
        Logg("insert_common_comment_replies_image", url);
        StringRequest request = new StringRequest(Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                progress.dismiss();
                try {
                    if (s.length() > 0) {
                        JSONObject obj = new JSONObject(s);
                        if (obj.getString("status").equalsIgnoreCase("success")) {

                            Toast.makeText(Replies_Common_Comment_homepage.this, "Upload Successfully", Toast.LENGTH_LONG).show();
                            comtext.setText("");
                            st_base46 = "";
                            camera_img.setImageResource(R.drawable.camera40);
                            volleylist();

                        } else if (obj.getString("status").equalsIgnoreCase("block"))
                            Toast.makeText(Replies_Common_Comment_homepage.this, obj.getString("response"), Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(Replies_Common_Comment_homepage.this, obj.getString("response"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    progress.dismiss();
                }

                progress.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                CommonUtils.toast(Replies_Common_Comment_homepage.this, CommonUtils.volleyerror(volleyError));
                progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("st_base64", st_base46);
                params.put("imagename", st_imagename);
                params.put("commentid", commentid + "");
                params.put("email", User_Email);
                params.put("comment", comtext.getText().toString());
                params.put("admission_no", addmissionno);
                Logg("replies params", params + "");

                return params;
            }
        };

        request.setShouldCache(false);
        int socketTimeout = 50000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        queue.add(request);
    }

    private void volleycominsert() {
        if (!nw.isConnectingToInternet()) {
            internetconnection(1);
            return;
        }

        progress.show();
        String url = Constants.URL_LV + "insert_common_comment_replies";
        Logg("insert_common_comment_replies", url);
        StringRequest obj1 = new StringRequest(Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String res) {
                progress.dismiss();
                int size = res.length();
                if (size > 0) {

                    try {
                        JSONObject obj = new JSONObject(res);
                        if (obj.getString("status").equalsIgnoreCase("success")) {
                            comtext.setText("");
                            Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
                            volleylist();

                        } else if (obj.getString("status").equalsIgnoreCase("block"))
                            Toast.makeText(Replies_Common_Comment_homepage.this, obj.getString("response"), Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(Replies_Common_Comment_homepage.this, obj.getString("response"), Toast.LENGTH_LONG).show();

                    } catch (JSONException e) {
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                CommonUtils.toast(Replies_Common_Comment_homepage.this, CommonUtils.volleyerror(e));
                progress.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("commentid", commentid + "");
                params.put("comment", comtext.getText().toString());
                params.put("email", User_Email);
                params.put("admission_no", addmissionno);
                Logg("replies params", params + "");
                return params;
            }
        };

        obj1.setShouldCache(false);
        int socketTimeout = 5000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        obj1.setRetryPolicy(policy);
        queue.add(obj1);

    }

    public void imagedialog(ImageView im) {
        final Dialog dialog = new Dialog(Replies_Common_Comment_homepage.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.imagedialog1);

        LinearLayout imglarge = (LinearLayout) dialog.findViewById(R.id.dialogimageid);
        ImageView cancel = (ImageView) dialog.findViewById(R.id.crossid);

        Bitmap bmp = ((BitmapDrawable) im.getDrawable()).getBitmap();
        TouchImageView img = new TouchImageView(Replies_Common_Comment_homepage.this);
        img.setImageBitmap(bmp);
        img.setMaxZoom(4f);
        img.setMinimumWidth(Constants.Width);
        img.setMinimumHeight(Constants.Height);
        imglarge.addView(img);

        dialog.show();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    private String getDate(String OurDate) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(OurDate);
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate = dateFormatter.format(value);
        } catch (Exception e) {
        }

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());

        return timediff(OurDate, formattedDate);

    }

    public String timediff(String prevdate, String curdate) {

        Date d11 = null;
        Date d2 = null;

        String[] dd1 = prevdate.split(" ");
        String[] date1 = dd1[0].split("-");

        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");

        try {
            d11 = format.parse(prevdate);
            d2 = format.parse(curdate);

            //in milliseconds
            long diff = d2.getTime() - d11.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            String pd = dd1[0];

            String cd;//=cal.get(Calendar.YEAR)+"-"+(cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.DAY_OF_MONTH);
            String[] cdarr = curdate.split(" ");
            cd = cdarr[0];
            String[] date2 = cd.split("-");

            int years = 0, months = 0, days = 0;

            years = Integer.parseInt(date2[0]) - Integer.parseInt(date1[0]);
            months = Integer.parseInt(date2[1]) - Integer.parseInt(date1[1]);
            days = Integer.parseInt(date2[2]) - Integer.parseInt(date1[2]);

            years = (months < 0) ? years - 1 : years;
            months = (months < 0) ? 12 + (months) : months;
            months = (days < 0) ? months - 1 : months;
            days = (days < 0) ? 30 + days : days;

            //	//Logg("di time= "," hrs= "+diffHours+" min= "+diffMinutes+" "+" years=  "+years+" month=  "+months+" days= "+days+" pd= "+pd+" cd= "+cd);

            if (years > 0) {
                return (String.valueOf(years) + " Years ago");
            } else if (months > 0 & years < 1) {

                return (String.valueOf(months) + " Months ago");
                //newsdateedit.putString("postdate"+i,String.valueOf(months)+"Months ago");
            } else if (days > 0 & months < 1) {
                return (String.valueOf(days) + " Days ago");
                //newsdateedit.putString("postdate"+i,String.valueOf(months)+" Days ago");
            } else if (diffHours > 0 & days < 1) {
                return (String.valueOf(diffHours) + " Hours ago");
                //newsdateedit.putString("postdate"+i,String.valueOf(months)+" Hours ago");
            } else if (diffMinutes > 0 & diffHours < 1) {
                return (String.valueOf(diffMinutes) + " Minutes ago");
                //newsdateedit.putString("postdate"+i,String.valueOf(months)+" Minutes ago");
            } else if (diffSeconds > 0 & diffMinutes < 1) {
                return ("0 Minutes ago");
                //newsdateedit.putString("postdate"+i,"0 Minutes ago");
            }

        } catch (Exception e) {

        }
        return ("0 Minutes ago");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    public void internetconnection(final int i) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog.dismiss();

                if (i == 0)
                    volleylist();
                else if (i == 1)
                    volleycominsert();
                else if (i == 2)
                    imagevolley();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    @Override
    public void onBackPressed() {

        if (Constants.Check_Branch_IO) {
            Intent in = new Intent(Replies_Common_Comment_homepage.this, HomeActivity.class);
            startActivity(in);
            finish();
        } else
            super.onBackPressed();
    }

    private void setTypeface() {

        Typeface font_demi = Typeface.createFromAsset(Replies_Common_Comment_homepage.this.getAssets(), "avenirnextdemibold.ttf");
        tv_reply = (TextView) findViewById(R.id.tv_reply);
        Typeface font_meduim = Typeface.createFromAsset(Replies_Common_Comment_homepage.this.getAssets(), "avenirnextmediumCn.ttf");
        comtext.setTypeface(font_meduim);
        tv_collage.setTypeface(font_meduim);
        tv_uname.setTypeface(font_demi);
        tv_time.setTypeface(font_meduim);
        tv_comment.setTypeface(font_meduim);
        tv_reply.setTypeface(font_demi);
        moreButton.setTypeface(font_meduim);
        tv_collage.setTypeface(font_meduim);


    }
}
