package com.scholar.engineering.banking.ssc;

import androidx.appcompat.app.AppCompatActivity;
import pl.tajchert.waitingdots.DotsTextView;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import javax.security.auth.callback.Callback;

public class PrivacyPolicyActivity extends AppCompatActivity implements View.OnClickListener{

    WebView wv;
    DotsTextView dots;
    ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);

        img_back = findViewById(R.id.img_back);
        dots=(DotsTextView)findViewById(R.id.dots);
        wv=(WebView)findViewById(R.id.webviewid);

        img_back.setOnClickListener(this);

        wv.getSettings().setJavaScriptEnabled(true);
        wv.setWebViewClient(new Callback());

        wv.loadUrl("http://139.59.63.65/mechanicalinsider/privacy-policy.php");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back:
                onBackPressed();
        }
    }


    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return(false);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            //Logg("onPagestarted",url+"");

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            dots.setVisibility(View.GONE);
            //Logg("onPageFinished "+postid,url+"");

            // bar.setVisibility(View.GONE);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}