package com.scholar.engineering.banking.ssc.Staff;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;

import com.google.android.material.tabs.TabLayout;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.adapter.SearchUserAdapter;
import com.scholar.engineering.banking.ssc.Staff.adapter.ViewPagerAdapter;
import com.scholar.engineering.banking.ssc.databinding.ActivitySearchUserBinding;

import java.util.Objects;

public class SearchUserActivity extends AppCompatActivity implements View.OnClickListener {

    ActivitySearchUserBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_search_user);

        binding.imgBack.setOnClickListener(this);
        binding.btnFab.setOnClickListener(this);
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Student"));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Staff"));
        binding.tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final SearchUserAdapter adapter = new SearchUserAdapter(getSupportFragmentManager(), this, binding.tabLayout.getTabCount());
        binding.viewPager.setAdapter(adapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);

        Objects.requireNonNull(binding.tabLayout.getTabAt(0)).setText(getResources().getText(R.string.student));
        Objects.requireNonNull(binding.tabLayout.getTabAt(1)).setText(getResources().getText(R.string.staff));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.btn_fab:
                break;
        }
    }
}