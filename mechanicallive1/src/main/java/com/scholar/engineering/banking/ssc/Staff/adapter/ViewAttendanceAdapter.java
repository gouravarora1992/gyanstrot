package com.scholar.engineering.banking.ssc.Staff.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ViewAttendanceModel;
import com.scholar.engineering.banking.ssc.databinding.ItemStudentAttendanceBinding;
import com.scholar.engineering.banking.ssc.databinding.ItemViewAttendanceBinding;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class ViewAttendanceAdapter extends RecyclerView.Adapter<ViewAttendanceAdapter.ViewHolder> {

    private Context context;
    private ViewAttendanceModel studentList;

    public ViewAttendanceAdapter(Context context, ViewAttendanceModel studentList) {
        this.context = context;
        this.studentList = studentList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemViewAttendanceBinding binding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                (R.layout.item_view_attendance),parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        try {
            holder.binding.txtName.setText(studentList.getData().get(position).getStudentName());

//            allStudentArr = studentList.getData();
//            presentStudentList = studentList.getPresentstudent();

            if (studentList.getPresentstudent().contains(studentList.getData().get(position).getStdRoll())){
                Glide.with(context).load(R.drawable.right).into(holder.binding.img);
            } else {
                Glide.with(context).load(R.drawable.cross_mark).into(holder.binding.img);
            }




//            for (int i = 0; i < allStudentArr.size(); i++) {
//                      Log.e("dataaa", "" + allStudentArr.get(i).getStdRoll());
//                allStudentList.add(allStudentArr.get(i).getStdRoll());
//            }
//
//                  Log.e("allStudentlist",""+allStudentList.size());
//
//            HashSet<Integer> hs = new HashSet<>();
//            for (int i = 0; i < allStudentList.size(); i++) {
//                for (int j = 0; j < presentStudentList.size(); j++) {
//
//                    if (allStudentList.get(i).equals(presentStudentList.get(j))) {
//
//                        hs.add(Integer.valueOf(allStudentList.get(i)));
////                        System.out.println(allStudentList.get(i)+"");
////                        System.out.println(presentStudentList.get(j)+"");
////                    Log.e("list>>",allStudentList.get(i));
////                    Log.e("listtttsizeee>>",""+allStudentList.size());
//  //                      Glide.with(context).load(R.drawable.right).into(holder.binding.img);
//                    }
//                }
//            }
//
//            for(int no:hs){
//           //     System.out.println(no+"");
//                Log.e("noooo...",""+no);
//            }

//            if (hs.equals(studentList.getData().get(position).getStdRoll())){
//                Glide.with(context).load(R.drawable.right).into(holder.binding.img);
//            } /*else {
//      //          Glide.with(context).load(R.drawable.cross_mark).into(holder.binding.img);
//            }*/
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return studentList.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ItemViewAttendanceBinding binding;

        public ViewHolder(@NonNull ItemViewAttendanceBinding binding) {
            super(binding.getRoot());
            this.binding=binding;

        }
    }

}
