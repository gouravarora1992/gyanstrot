package com.scholar.engineering.banking.ssc.newScreens.QueryForms;

/**
 * Created by Swt on 9/16/2017.
 */

public class QueryGetSet {

    String id,ticketno,issue,details,status,data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public QueryGetSet(String id, String ticketno, String issue, String details, String status, String data){
        this.id = id;
        this.ticketno=ticketno;
        this.issue= issue;
        this.details=details;
        this.status=status;
        this.data=data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTicketno() {
        return ticketno;
    }

    public void setTicketno(String ticketno) {
        this.ticketno = ticketno;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
