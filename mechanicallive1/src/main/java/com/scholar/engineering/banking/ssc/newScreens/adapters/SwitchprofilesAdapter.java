package com.scholar.engineering.banking.ssc.newScreens.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.scholar.engineering.banking.ssc.HomeActivity;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.newScreens.SwitchProfile;
import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
import com.scholar.engineering.banking.ssc.utils.CommonUtils;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
public class SwitchprofilesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context cs;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private DisplayImageOptions options;
    ImageLoader imageLoader;
    JSONArray data;
    JSONObject object;
    UserSharedPreferences preferences;

    public SwitchprofilesAdapter(Context cs, JSONArray data) {
        this.cs = cs;
        this.data = data;

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(cs));
        preferences=UserSharedPreferences.getInstance(cs);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.blankimage)
                .showImageForEmptyUri(R.drawable.congrrr)
                .showImageOnFail(R.drawable.congrrr)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View v0 = inflater.inflate(R.layout.switchprofileitem, viewGroup, false);
        viewHolder = new ViewHolder0(v0);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        ViewHolder0 vh0 = (ViewHolder0) viewHolder;

        setData(vh0, position);
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    public void setData(ViewHolder0 v, final int p) {

        try {

            object = data.getJSONObject(p);
            imageLoader.init(ImageLoaderConfiguration.createDefault(cs));
            imageLoader.getInstance().displayImage(object.getString("image"), v.iv_banner, options, animateFirstListener);
            v.tvname.setText(object.getString("Student_name"));
            v.classec.setText(object.getString("Student_class")+" "+object.getString("Student_section"));
            Logg("preferencedata",preferences.getaddmissiono()+" , "+object.getString("std_roll"));
            if (preferences.getaddmissiono().equals(object.getString("std_roll")))
            {
                v.tvselected.setImageDrawable(ContextCompat.getDrawable(cs,R.drawable.checked));
            }
            else
            {
                v.tvselected.setImageDrawable(ContextCompat.getDrawable(cs,R.drawable.unchecked));
            }
            v.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        object = data.getJSONObject(p);
                        preferences.setname(object.getString("Student_name"));
                        preferences.setclass(object.getString("Student_class"));
                        preferences.setsection(object.getString("Student_section"));
                        preferences.setemail(object.getString("email"));
                        preferences.setaddmissiono(object.getString("std_roll"));
                        preferences.setdob(object.getString("password"));
                        ((SwitchProfile)cs).finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        } catch (JSONException e) { }

    }

    public class ViewHolder0 extends RecyclerView.ViewHolder {

        ImageView iv_banner,tvselected;
        TextView tvname,classec;

        public ViewHolder0(View bn) {
            super(bn);
            iv_banner = (ImageView) bn.findViewById(R.id.iv_banner);
            tvname=bn.findViewById(R.id.tvname);
            classec=bn.findViewById(R.id.classec);
            tvselected=bn.findViewById(R.id.tvselected);
        }
    }

}
