package com.scholar.engineering.banking.ssc.newScreens

import android.app.ProgressDialog
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.RetryPolicy
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.scholar.engineering.banking.ssc.R
import com.scholar.engineering.banking.ssc.newScreens.adapters.NotesPDFAdapter
import com.scholar.engineering.banking.ssc.utils.CommonUtils
import com.scholar.engineering.banking.ssc.utils.Constants
import com.scholar.engineering.banking.ssc.utils.UserSharedPreferences
import org.json.JSONException
import org.json.JSONObject
import retrofit2.http.Url
import java.io.*
import java.net.URL
import java.util.*

class NotesPDF : AppCompatActivity() {
    var toolbar: Toolbar? = null
    var toolbartitle: TextView? = null

    lateinit var context:Context
    lateinit var sharedPreferences: UserSharedPreferences
    var pd: ProgressDialog? = null

    var type:String=""

    lateinit var recyclerView: RecyclerView
    lateinit var itemVideos: ArrayList<NotesPdfGetSet>
    lateinit var adapter: NotesPDFAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes_p_d_f)
        type= intent.getStringExtra("type").toString()

        context=this
        toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        toolbartitle = toolbar?.findViewById(R.id.headertextid) as TextView
        if(type.equals("notes"))
            toolbartitle?.setText("Notes")
        else
            toolbartitle?.setText("Assignments")

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        sharedPreferences = UserSharedPreferences.getInstance(this)
        pd = ProgressDialog(this)
        pd?.setMessage("loading")
        pd?.setCancelable(false)

        itemVideos = ArrayList()
        recyclerView = findViewById<View>(R.id.rv_asans) as RecyclerView
        recyclerView.setHasFixedSize(true)
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = linearLayoutManager

        if(type.equals("notes"))
            sendRequest()
        else
            sendRequest_assignments()

    }

    override fun onDestroy() {
        if (pd != null && pd!!.isShowing) {
            pd!!.dismiss()
        }
        super.onDestroy()
    }

    private fun sendRequest() {
        pd!!.show()
        val requestQueue = Volley.newRequestQueue(this)
        val stringRequest: StringRequest = object : StringRequest(Method.POST, Constants.URL_LV+"get_notes",
                Response.Listener { response ->
                    pd!!.dismiss()
                    try {
                        val dataobject = JSONObject(response)
                        val ara = dataobject.getJSONArray("data")
                        val path=dataobject.getString("path")
                        for (i in 0 until ara.length()) {
                            val obj = ara.getJSONObject(i)
                            itemVideos.add(NotesPdfGetSet(obj.getString("id"),
                                    obj.getString("topic"), path+obj.getString("as_file"),
                            obj.getString("subject")))
                        }
                        adapter = NotesPDFAdapter(this, itemVideos)
                        recyclerView.adapter = adapter
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        pd!!.dismiss()
                    }
                },
                Response.ErrorListener { error ->
                    CommonUtils.volleyerror(error)
                    pd!!.dismiss()
                }
        ) {
            override fun getParams(): Map<String, String> {
                val params: MutableMap<String, String> = HashMap()
                params["email"] = sharedPreferences.getemail()
                params["admission_no"] = sharedPreferences.getaddmissiono()
                Log.e("get_notes", params.toString() + "")
                return params
            }
        }
        stringRequest.setShouldCache(false)

        val socketTimeout = 0
        val policy: RetryPolicy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.setRetryPolicy(policy)

        requestQueue.add(stringRequest)
    }
    private fun sendRequest_assignments() {
        pd!!.show()
        val requestQueue = Volley.newRequestQueue(this)
        val stringRequest: StringRequest = object : StringRequest(Method.POST, Constants.URL_LV+"get_assignments",
                Response.Listener { response ->
                    pd!!.dismiss()
                    Log.e("response--->",response)
                    try {
                        val dataobject = JSONObject(response)
                        if(dataobject.getString("status").equals("true"))
                        {
                            val ara = dataobject.getJSONArray("data")
                            val path=dataobject.getString("path")
                            for (i in 0 until ara.length()) {
                                val obj = ara.getJSONObject(i)
                                itemVideos.add(NotesPdfGetSet(obj.getString("id"),
                                        obj.getString("topic"), path+obj.getString("as_file"),
                                        obj.getString("subject")))
                            }
                            adapter = NotesPDFAdapter(this, itemVideos)
                            recyclerView.adapter = adapter
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        pd!!.dismiss()
                    }
                },
                Response.ErrorListener { error ->
                    CommonUtils.volleyerror(error)
                    pd!!.dismiss()
                }
        ) {
            override fun getParams(): Map<String, String> {
                val params: MutableMap<String, String> = HashMap()
                params["email"] = sharedPreferences.getemail()
                params["admission_no"] = sharedPreferences.getaddmissiono()
                Log.e("get_assignments", params.toString() + "")
                return params
            }
        }
        stringRequest.setShouldCache(false)

        val socketTimeout = 0
        val policy: RetryPolicy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.setRetryPolicy(policy)

        requestQueue.add(stringRequest)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onResume() {
        super.onResume()
    }
}