package com.scholar.engineering.banking.ssc.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.getset.gettr_settr;

import java.util.ArrayList;

/**
 * Created by surender on 19-04-2016.
 */
public class Followgroup_adap extends BaseAdapter {

    Context con;
    ArrayList<gettr_settr> data;

    public Followgroup_adap(Context con, ArrayList<gettr_settr> data) {
        this.con = con;
        this.data = data;
    }

    @Override
    public int getCount()
    {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inf=(LayoutInflater)con.getSystemService(con.LAYOUT_INFLATER_SERVICE);
        View v=inf.inflate(R.layout.followgroup_listitem,parent,false);
        Typeface font_demi= Typeface.createFromAsset(con.getAssets(), "avenirnextdemibold.ttf");
        TextView gname=(TextView)v.findViewById(R.id.groupnameid);
        gname.setTypeface(font_demi);
        gname.setText(data.get(position).getTopic());

        return v;
    }
}
