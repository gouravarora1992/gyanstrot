package com.scholar.engineering.banking.ssc.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.widget.NestedScrollView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.scholar.engineering.banking.ssc.HomeActivity;
import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.adapter.PremiumTest_All_Adapter;
import com.scholar.engineering.banking.ssc.getset.Home_getset;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.EndlessRecyclerViewScrollListener;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class Premium_Test_Fragment extends Fragment implements OnClickListener {

	ArrayList<Home_getset> homedata,homedata1;
	int searchlength=0;
	RecyclerView homelist;
	public static PremiumTest_All_Adapter adapter;
	SharedPreferences pref,sp;
	SwipeRefreshLayout swipeRefreshLayout;
	int versionCode,index=0;
	Boolean aBoolean=true;
	Boolean isRunning=true;
	NestedScrollView nestedscroll;
	RelativeLayout footerlayout;
	TextView loadmore;
	Typeface font_demi;
	ProgressWheel wheelbar;
	NetworkConnection nw;
	boolean prg=true;
//    FloatingActionButton Post_Fab,Link_Fab,Question_Fab;
	EndlessRecyclerViewScrollListener endless;
	HomeActivity activity;
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		activity= (HomeActivity) context;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view =inflater.inflate(R.layout.home_fragment2, container,false);

		nw=new NetworkConnection(activity);
//		Constants.checkrefresh=true;
		setHasOptionsMenu(true);

//		MobileAds.initialize(activity, getString(R.string.test_admob_express_unit_id));
		sp=activity.getSharedPreferences("likestatus", 0);
//		font_demi = Typeface.createFromAsset(activity.getAssets(), "avenirnextdemibold.ttf");
//        Post_Fab=(FloatingActionButton)view.findViewById(R.id.post);
//        Link_Fab=(FloatingActionButton)view.findViewById(R.id.link);
//        Question_Fab=(FloatingActionButton)view.findViewById(R.id.question);
//        Post_Fab.setOnClickListener(this);
//        Question_Fab.setOnClickListener(this);
//        Link_Fab.setOnClickListener(this);

        nestedscroll=(NestedScrollView)view.findViewById(R.id.nestedscroll);

		homelist=(RecyclerView) view.findViewById(R.id.recyclerView);

		LinearLayoutManager mLayoutManager = new LinearLayoutManager(activity);

		homelist.setLayoutManager(mLayoutManager);

		homedata=new ArrayList<>();

		endless = new EndlessRecyclerViewScrollListener(mLayoutManager) {
			@Override
			public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
				Logg("page",page+"");

				footerlayout.setVisibility(View.VISIBLE);

//				if(homedata.size()>9)
				getData();
			}
		};

		homelist.addOnScrollListener(endless);

//		homelist.addOnScrollListener(endless);

		footerlayout=(RelativeLayout)view.findViewById(R.id.footer_layout);
		loadmore=(TextView)view.findViewById(R.id.moreButton);
		wheelbar=(ProgressWheel)view.findViewById(R.id.progress_wheel);
		footerlayout.setVisibility(View.GONE);

		index=0;

		swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);

//		getData();

		swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				endless.resetState();
				prg=false;
				index=0;

				getData();

			}
		});

		pref=activity.getSharedPreferences("checkdata",0);

		footerlayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

				Home_getset home;

				home= (Home_getset) homedata.get(homedata.size()-1);

				index= home.getId();

				Logg("index=",index+"");
					getData();

				loadmore.setText("");
//				wheelbar.setVisibility(View.VISIBLE);

			}
		});

		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
//		Constants.Current_Activity=100;
//
//		if(Constants.checkrefresh) {
		endless.resetState();
				index = 0;
			getData();
//			Constants.checkrefresh=false;
//		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);

//		inflater.inflate(R.menu.menu, menu);
//		MenuItem item = menu.findItem(R.id.search);
//
//		MenuItem playMenu = menu.findItem(R.id.shareid);
////
//        if (Constants.Width <= 600)
//            playMenu.setIcon(R.drawable.dollar);
//        else
//            playMenu.setIcon(R.drawable.dollar);

	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
	}

    @Override
    public void onStop() {
        super.onStop();
        //Logg("call_onstop","onStop");
    }

	public void deleteCache(Context context) {
		try {
			File dir = context.getCacheDir();
			deleteDir(dir);
		} catch (Exception e) {}
	}

	public boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
			return dir.delete();
		} else if(dir!= null && dir.isFile()) {
			return dir.delete();
		} else {
			return false;
		}
	}

	public void internetconnection(){

		final Dialog dialog = new Dialog(activity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.network_alert);
		dialog.show();

//        JSONObject json;

		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
		TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);

		//tv_tryagain.setText("homefragment");

		tv_tryagain.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

//				//index=0;
				swipeRefreshLayout.setRefreshing(true);
				dialog.dismiss();
				getData();

			}
		});

		iv_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

	}


	public void getData()
	{

		if(!nw.isConnectingToInternet()) {
			swipeRefreshLayout.setRefreshing(false);
			internetconnection();
			return;
		}

//		progress = ProgressDialog.show(Onlineexam.this, null, null, true);
//		progress.setContentView(R.layout.progressdialog);
//		progress.setCanceledOnTouchOutside(false);
//		progress.setCancelable(false);
//		progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

//		if(index==0)
//			progress.show();

		RequestQueue queue= Volley.newRequestQueue(activity);
		JSONObject obj=new JSONObject();
		String url= Constants.URL+"newapi2_04/get_Premiumtest.php?email="+ Constants.User_Email+"&index="+index;

		Logg("get_Premiumtest",url);

		JsonObjectRequest json=new JsonObjectRequest(Request.Method.POST, url, obj,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject res) {
				// TODO Auto-generated method stub
				try {
					Logg("response_prem",res.toString());

					if(res.has("data")) {
						JSONArray jr = res.getJSONArray("data");

//						if (jr.length() < 3)
//							footerlayout.setVisibility(View.GONE);
//						else {
//							footerlayout.setVisibility(View.VISIBLE);
//
////							loadmore.setText("Load Previous..");
////							wheelbar.setVisibility(View.GONE);
//						}

						if(index==0) {

							homedata.clear();

							for (int i = 0; i < jr.length(); i++) {
								JSONObject ob = jr.getJSONObject(i);

//							String time = Utility.getDate(ob.getString("timestamp"));

								Home_getset object = new Home_getset();
								object.setId(ob.getInt("id"));
								object.setTimestamp(ob.getString("timestamp"));
								object.setLikecount(ob.getInt("likes"));
								object.setCommentcount(ob.getInt("comment"));
								object.setViewcount(ob.getInt("view"));
								object.setUid(ob.getString("uid"));
								object.setPosttype(ob.getString("posttype"));
								object.setGroupid(ob.getString("groupid"));
								object.setFieldtype(ob.getString("field_type"));
								object.setJsonfield(ob.getString("jsondata"));
								object.setLikestatus(ob.getString("likestatus"));
								object.setPosturl(ob.getString("posturl"));
								object.setPostdescription(ob.getString("post_description"));
								if(ob.has("AppVersion"))
									object.setAppVersion(ob.getString("AppVersion"));
								homedata.add(object);

							}

							//Logg("data.size", homedata.size() + "");
							adapter = new PremiumTest_All_Adapter(activity, homedata, "onlineexam");
							homelist.setAdapter(adapter);

							Home_getset home;
							home = (Home_getset) homedata.get(homedata.size() - 1);
							index = home.getId();

						}
						else{
							homedata1=new ArrayList<>();

							for (int i = 0; i < jr.length(); i++) {
								JSONObject ob = jr.getJSONObject(i);

//							String time = Utility.getDate(ob.getString("timestamp"));

								Home_getset object = new Home_getset();
								object.setId(ob.getInt("id"));
								object.setTimestamp(ob.getString("timestamp"));
								object.setLikecount(ob.getInt("likes"));
								object.setCommentcount(ob.getInt("comment"));
								object.setViewcount(ob.getInt("view"));
								object.setUid(ob.getString("uid"));
								object.setPosttype(ob.getString("posttype"));
								object.setGroupid(ob.getString("groupid"));
								object.setFieldtype(ob.getString("field_type"));
								object.setJsonfield(ob.getString("jsondata"));
								object.setLikestatus(ob.getString("likestatus"));
								object.setPosturl(ob.getString("posturl"));
								object.setPostdescription(ob.getString("post_description"));
								if(ob.has("AppVersion"))
									object.setAppVersion(ob.getString("AppVersion"));
								homedata1.add(object);

							}

							adapter.addItme(homedata1);
							adapter.notifyDataSetChanged();

							Home_getset home;
							home = (Home_getset) homedata1.get(homedata1.size() - 1);
							index = home.getId();

						}

					}
					else
						footerlayout.setVisibility(View.GONE);

//
//					if(progress.isShowing())
//						progress.dismiss();

					swipeRefreshLayout.setRefreshing(false);
				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					footerlayout.setVisibility(View.GONE);
					//Logg("checkversion exp",e.getMessage());

//					if(progress.isShowing())
//						progress.dismiss();

					swipeRefreshLayout.setRefreshing(false);
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				footerlayout.setVisibility(View.GONE);
				//Logg("checkversion error", String.valueOf(arg0));

//				if(progress.isShowing())
//					progress.dismiss();

				swipeRefreshLayout.setRefreshing(false);
			}
		});
		queue.add(json);
	}



    @Override
    public void onClick(View view) {

    }
}