package com.scholar.engineering.banking.ssc.Staff.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SupportSystemModel {

    @SerializedName("staff_id")
    @Expose
    private Integer staff_id;
    @SerializedName("assigned")
    @Expose
    private List<Assigned> assigned = null;
    @SerializedName("solved")
    @Expose
    private List<Solved> solved = null;
    @SerializedName("unsolved")
    @Expose
    private List<Unsolved> unsolved = null;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStaff_id() {
        return staff_id;
    }

    public void setStaff_id(Integer staff_id) {
        this.staff_id = staff_id;
    }

    public List<Assigned> getAssigned() {
        return assigned;
    }

    public void setAssigned(List<Assigned> assigned) {
        this.assigned = assigned;
    }

    public List<Solved> getSolved() {
        return solved;
    }

    public void setSolved(List<Solved> solved) {
        this.solved = solved;
    }

    public List<Unsolved> getUnsolved() {
        return unsolved;
    }

    public void setUnsolved(List<Unsolved> unsolved) {
        this.unsolved = unsolved;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class Assigned implements Serializable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("ticketno")
        @Expose
        private String ticketno;
        @SerializedName("issue")
        @Expose
        private String issue;
        @SerializedName("std_roll")
        @Expose
        private Integer stdRoll;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("details")
        @Expose
        private String details;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("feedback")
        @Expose
        private Object feedback;
        @SerializedName("feedbackfile")
        @Expose
        private Object feedbackfile;
        @SerializedName("master_id")
        @Expose
        private Integer masterId;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("std_class")
        @Expose
        private String stdClass;
        @SerializedName("std_section")
        @Expose
        private String stdSection;
        @SerializedName("file")
        @Expose
        private String file;
        @SerializedName("teacherstatus")
        @Expose
        private Object teacherstatus;
        @SerializedName("deadline")
        @Expose
        private Object deadline;
        @SerializedName("rating")
        @Expose
        private Object rating;
        @SerializedName("parentfeedback")
        @Expose
        private Object parentfeedback;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getTicketno() {
            return ticketno;
        }

        public void setTicketno(String ticketno) {
            this.ticketno = ticketno;
        }

        public String getIssue() {
            return issue;
        }

        public void setIssue(String issue) {
            this.issue = issue;
        }

        public Integer getStdRoll() {
            return stdRoll;
        }

        public void setStdRoll(Integer stdRoll) {
            this.stdRoll = stdRoll;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Object getFeedback() {
            return feedback;
        }

        public void setFeedback(Object feedback) {
            this.feedback = feedback;
        }

        public Object getFeedbackfile() {
            return feedbackfile;
        }

        public void setFeedbackfile(Object feedbackfile) {
            this.feedbackfile = feedbackfile;
        }

        public Integer getMasterId() {
            return masterId;
        }

        public void setMasterId(Integer masterId) {
            this.masterId = masterId;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getStdClass() {
            return stdClass;
        }

        public void setStdClass(String stdClass) {
            this.stdClass = stdClass;
        }

        public String getStdSection() {
            return stdSection;
        }

        public void setStdSection(String stdSection) {
            this.stdSection = stdSection;
        }

        public String getFile() {
            return file;
        }

        public void setFile(String file) {
            this.file = file;
        }

        public Object getTeacherstatus() {
            return teacherstatus;
        }

        public void setTeacherstatus(Object teacherstatus) {
            this.teacherstatus = teacherstatus;
        }

        public Object getDeadline() {
            return deadline;
        }

        public void setDeadline(Object deadline) {
            this.deadline = deadline;
        }

        public Object getRating() {
            return rating;
        }

        public void setRating(Object rating) {
            this.rating = rating;
        }

        public Object getParentfeedback() {
            return parentfeedback;
        }

        public void setParentfeedback(Object parentfeedback) {
            this.parentfeedback = parentfeedback;
        }

    }

    public static class Solved implements Serializable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("ticketno")
        @Expose
        private String ticketno;
        @SerializedName("issue")
        @Expose
        private String issue;
        @SerializedName("std_roll")
        @Expose
        private Integer stdRoll;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("details")
        @Expose
        private String details;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("feedback")
        @Expose
        private String feedback;
        @SerializedName("feedbackfile")
        @Expose
        private Object feedbackfile;
        @SerializedName("master_id")
        @Expose
        private Integer masterId;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("std_class")
        @Expose
        private String stdClass;
        @SerializedName("std_section")
        @Expose
        private String stdSection;
        @SerializedName("file")
        @Expose
        private Object file;
        @SerializedName("teacherstatus")
        @Expose
        private Integer teacherstatus;
        @SerializedName("deadline")
        @Expose
        private String deadline;
        @SerializedName("rating")
        @Expose
        private Integer rating;
        @SerializedName("parentfeedback")
        @Expose
        private String parentfeedback;
        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getTicketno() {
            return ticketno;
        }

        public void setTicketno(String ticketno) {
            this.ticketno = ticketno;
        }

        public String getIssue() {
            return issue;
        }

        public void setIssue(String issue) {
            this.issue = issue;
        }

        public Integer getStdRoll() {
            return stdRoll;
        }

        public void setStdRoll(Integer stdRoll) {
            this.stdRoll = stdRoll;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getFeedback() {
            return feedback;
        }

        public void setFeedback(String feedback) {
            this.feedback = feedback;
        }

        public Object getFeedbackfile() {
            return feedbackfile;
        }

        public void setFeedbackfile(Object feedbackfile) {
            this.feedbackfile = feedbackfile;
        }

        public Integer getMasterId() {
            return masterId;
        }

        public void setMasterId(Integer masterId) {
            this.masterId = masterId;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getStdClass() {
            return stdClass;
        }

        public void setStdClass(String stdClass) {
            this.stdClass = stdClass;
        }

        public String getStdSection() {
            return stdSection;
        }

        public void setStdSection(String stdSection) {
            this.stdSection = stdSection;
        }

        public Object getFile() {
            return file;
        }

        public void setFile(Object file) {
            this.file = file;
        }

        public Integer getTeacherstatus() {
            return teacherstatus;
        }

        public void setTeacherstatus(Integer teacherstatus) {
            this.teacherstatus = teacherstatus;
        }

        public String getDeadline() {
            return deadline;
        }

        public void setDeadline(String deadline) {
            this.deadline = deadline;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

        public String getParentfeedback() {
            return parentfeedback;
        }

        public void setParentfeedback(String parentfeedback) {
            this.parentfeedback = parentfeedback;
        }
    }

    public static class Unsolved implements Serializable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("ticketno")
        @Expose
        private String ticketno;
        @SerializedName("issue")
        @Expose
        private String issue;
        @SerializedName("std_roll")
        @Expose
        private Integer stdRoll;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("details")
        @Expose
        private String details;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("feedback")
        @Expose
        private Object feedback;
        @SerializedName("feedbackfile")
        @Expose
        private Object feedbackfile;
        @SerializedName("master_id")
        @Expose
        private Integer masterId;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("std_class")
        @Expose
        private String stdClass;
        @SerializedName("std_section")
        @Expose
        private String stdSection;
        @SerializedName("file")
        @Expose
        private String file;
        @SerializedName("teacherstatus")
        @Expose
        private Integer teacherstatus;
        @SerializedName("deadline")
        @Expose
        private String deadline;
        @SerializedName("rating")
        @Expose
        private Object rating;
        @SerializedName("parentfeedback")
        @Expose
        private Object parentfeedback;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getTicketno() {
            return ticketno;
        }

        public void setTicketno(String ticketno) {
            this.ticketno = ticketno;
        }

        public String getIssue() {
            return issue;
        }

        public void setIssue(String issue) {
            this.issue = issue;
        }

        public Integer getStdRoll() {
            return stdRoll;
        }

        public void setStdRoll(Integer stdRoll) {
            this.stdRoll = stdRoll;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Object getFeedback() {
            return feedback;
        }

        public void setFeedback(Object feedback) {
            this.feedback = feedback;
        }

        public Object getFeedbackfile() {
            return feedbackfile;
        }

        public void setFeedbackfile(Object feedbackfile) {
            this.feedbackfile = feedbackfile;
        }

        public Integer getMasterId() {
            return masterId;
        }

        public void setMasterId(Integer masterId) {
            this.masterId = masterId;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getStdClass() {
            return stdClass;
        }

        public void setStdClass(String stdClass) {
            this.stdClass = stdClass;
        }

        public String getStdSection() {
            return stdSection;
        }

        public void setStdSection(String stdSection) {
            this.stdSection = stdSection;
        }

        public String getFile() {
            return file;
        }

        public void setFile(String file) {
            this.file = file;
        }

        public Integer getTeacherstatus() {
            return teacherstatus;
        }

        public void setTeacherstatus(Integer teacherstatus) {
            this.teacherstatus = teacherstatus;
        }

        public String getDeadline() {
            return deadline;
        }

        public void setDeadline(String deadline) {
            this.deadline = deadline;
        }

        public Object getRating() {
            return rating;
        }

        public void setRating(Object rating) {
            this.rating = rating;
        }

        public Object getParentfeedback() {
            return parentfeedback;
        }

        public void setParentfeedback(Object parentfeedback) {
            this.parentfeedback = parentfeedback;
        }

    }
}
