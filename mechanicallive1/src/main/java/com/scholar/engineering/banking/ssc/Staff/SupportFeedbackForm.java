package com.scholar.engineering.banking.ssc.Staff;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.SupportFeedbackModel;
import com.scholar.engineering.banking.ssc.getset.RetrofitObjectAPI;
import com.scholar.engineering.banking.ssc.utils.Constants;
import com.scholar.engineering.banking.ssc.utils.GetPath;
import com.scholar.engineering.banking.ssc.utils.NetworkConnection;

import java.io.File;

import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;

public class SupportFeedbackForm extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    Spinner spinner_feedback;
    Button btn_feedback_save,btn_feedback_reset, btn_chooseFile;
    CardView feedback_cardview;
    ImageView img_back;
    TextView txt_choosen_file;
    EditText edt_response;
    private static final int PICK_GALLERY = 102;
    NetworkConnection nw;
    String[] feedback = { "Select Status", "Process", "Solved", "Unsolved"};
    File file;
    String path;
    Integer status_id, id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support_feedback_form);

        nw = new NetworkConnection(this);
        btn_feedback_save = findViewById(R.id.btn_save);
        btn_feedback_reset = findViewById(R.id.btn_reset);
        edt_response = findViewById(R.id.edt_response);
        feedback_cardview = findViewById(R.id.feedback_cardview);
        spinner_feedback = findViewById(R.id.textView3);
        btn_chooseFile = findViewById(R.id.button4);
        txt_choosen_file = findViewById(R.id.textView46);
        img_back = findViewById(R.id.img_back);
        btn_feedback_save.setOnClickListener(this);
        btn_feedback_reset.setOnClickListener(this);
        btn_chooseFile.setOnClickListener(this);
        img_back.setOnClickListener(this);

        id = getIntent().getIntExtra("id",0);
        Log.e("id--->",""+id);

        feedbackSpinner();
    }

    private void feedbackSpinner(){
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,feedback);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_feedback.setAdapter(aa);
        spinner_feedback.setOnItemSelectedListener(this);
    }

    //permission
    private void requestPermision() {
        ActivityCompat.requestPermissions(SupportFeedbackForm.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
    }

    private boolean checkPermission() {

        int READ_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(SupportFeedbackForm.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int WRITE_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(SupportFeedbackForm.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (WRITE_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else if (READ_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //ImageFromGallery
        if (requestCode == PICK_GALLERY) {
            if (resultCode == RESULT_OK) {
                try {
                    if (data != null) {
                        Uri filePath = data.getData();
                        path= GetPath.getPath(this,filePath);
                        Log.e("path", ""+ path);
                        if (path!=null) {
                            file = new File(path);
                            Log.e("fileName", "" + file.getName());
                            txt_choosen_file.setText(file.getName());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    private boolean checkValidationFeedback(){
        boolean ret=true;
        String strResponse = edt_response.getText().toString();

        if(spinner_feedback.getSelectedItem().toString().trim() == "Select Status") {
            ret=false;
            Toast.makeText(this, "Please select status", Toast.LENGTH_LONG).show();
        }
        else if(TextUtils.isEmpty(strResponse)) {
            ret=false;
            Toast.makeText(this,"Please enter response", Toast.LENGTH_LONG).show();
        }
        return ret;
    }

    private void getSupportFeedback() {

        if (!nw.isConnectingToInternet()) {
            internetconnection();
            return;
        }

        MultipartBody.Part requestImage = null;
        if(path!=null){
            file = new File(path);
            Log.e("Register",""+file.getName());
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            requestImage = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        }

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.URL_LV).addConverterFactory(GsonConverterFactory.create()).build();
        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);
        Call<SupportFeedbackModel> call = service.getSupportFeedback(id, status_id, edt_response.getText().toString(), path);

        Logg("urlapifeedback", Constants.URL_LV + "homedata?id=" +
                id + "&spinner=" + status_id + "&response" + edt_response.getText().toString() + "&image" + path);


        call.enqueue(new Callback<SupportFeedbackModel>() {

            @Override
            public void onResponse(Call<SupportFeedbackModel> call, retrofit2.Response<SupportFeedbackModel> response) {
                if (response.isSuccessful()) {
                    finish();
                    Toast.makeText(SupportFeedbackForm.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(SupportFeedbackForm.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SupportFeedbackModel> call, Throwable t) {
                Toast.makeText(SupportFeedbackForm.this,"failure",Toast.LENGTH_SHORT).show();
            }
        });
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.button4:
                if (checkPermission()) {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_GALLERY);
                } else {
                    requestPermision();
                }
                break;

            case R.id.btn_save:
                if (checkValidationFeedback()){
                    getSupportFeedback();
                }
                break;

            case R.id.btn_reset:
                spinner_feedback.setSelection(0);
                edt_response.setText("");
                txt_choosen_file.setText("No file chosen");
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(parent.getId() == R.id.textView3) {
            if (spinner_feedback.getSelectedItem().toString().equalsIgnoreCase("Process")) {
                status_id = 5;
            } else if (spinner_feedback.getSelectedItem().toString().equalsIgnoreCase("Solved")) {
                status_id = 2;
            } else if (spinner_feedback.getSelectedItem().toString().equalsIgnoreCase("UnSolved")) {
                status_id = 3;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    public void internetconnection() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.network_alert);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView iv_cancel = (ImageView) dialog.findViewById(R.id.iv_cancel);
        TextView tv_tryagain = (TextView) dialog.findViewById(R.id.tv_refresh);

        tv_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getSupportFeedback();
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

}