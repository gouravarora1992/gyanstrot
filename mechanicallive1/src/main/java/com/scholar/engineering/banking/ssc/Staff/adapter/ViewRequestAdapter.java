package com.scholar.engineering.banking.ssc.Staff.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scholar.engineering.banking.ssc.R;
import com.scholar.engineering.banking.ssc.Staff.ModelClass.ViewRequestModel;
import com.scholar.engineering.banking.ssc.databinding.ItemLeaveRequestBinding;
import com.scholar.engineering.banking.ssc.databinding.ItemViewRequestBinding;
import com.scholar.engineering.banking.ssc.utils.ServerTimeCalculation;


import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class ViewRequestAdapter extends RecyclerView.Adapter<ViewRequestAdapter.ViewHolder> {

    private Context context;
    private List<ViewRequestModel.Datum> viewRequest;

    public ViewRequestAdapter(Context context, List<ViewRequestModel.Datum> viewRequest) {
        this.context = context;
        this.viewRequest = viewRequest;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemViewRequestBinding binding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),(R.layout.item_view_request),parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.binding.txtStudentName.setText(viewRequest.get(position).getName());
        holder.binding.txtClass.setText(viewRequest.get(position).get_class());
        holder.binding.txtSection.setText(viewRequest.get(position).getSection());
        holder.binding.txtTym.setText(new ServerTimeCalculation().getTime(context,viewRequest.get(position).getCreatedAt()));

        if (viewRequest.get(position).getReason()!=null) {
            holder.binding.txtDescription.setText(viewRequest.get(position).getReason());
        }
        if (viewRequest.get(position).getStatus()==0){
            holder.binding.txtStatuss.setText("Pending");
            holder.binding.txtStatuss.setBackgroundResource(R.drawable.background_cr_transparent);
        } else if (viewRequest.get(position).getStatus()==1){
            holder.binding.txtStatuss.setText("Approved");
            holder.binding.txtStatuss.setBackgroundResource(R.drawable.bakground_green);
        }

    }


    @Override
    public int getItemCount() {
        return viewRequest.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ItemViewRequestBinding binding;

        public ViewHolder(@NonNull ItemViewRequestBinding binding) {
            super(binding.getRoot());
            this.binding=binding;

        }
    }


}
