//package com.scholar.engineering.banking.ssc;
//
//import android.app.Activity;
//import android.app.Dialog;
//import android.app.ProgressDialog;
//import android.content.ContentResolver;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.content.SharedPreferences.Editor;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Color;
//import android.graphics.Typeface;
//import android.graphics.drawable.BitmapDrawable;
//import android.graphics.drawable.ColorDrawable;
//import android.net.Uri;
//import android.os.Build;
//import android.os.Bundle;
//import android.os.StrictMode;
//
//import androidx.core.app.ActivityCompat;
//import androidx.appcompat.app.AlertDialog;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//import androidx.appcompat.widget.Toolbar;
//import android.util.Base64;
//import android.util.Log;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.Window;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.Request;
//import com.android.volley.Request.Method;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.RetryPolicy;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
////import com.androidquery.AQuery;
//
//import com.nostra13.universalimageloader.core.DisplayImageOptions;
//import com.nostra13.universalimageloader.core.ImageLoader;
//import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
//import com.nostra13.universalimageloader.core.assist.ImageScaleType;
//import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
//import com.scholar.engineering.banking.ssc.adapter.Replies_on_group_comment_adap;
//import com.scholar.engineering.banking.ssc.utils.AnimateFirstDisplayListener;
//import com.scholar.engineering.banking.ssc.utils.CommonUtils;
//import com.scholar.engineering.banking.ssc.utils.Constants;
//import com.scholar.engineering.banking.ssc.utils.DatabaseHandler;
//import com.scholar.engineering.banking.ssc.utils.ImageCompression;
//import com.scholar.engineering.banking.ssc.utils.ImageUploading;
//import com.scholar.engineering.banking.ssc.utils.NetworkConnection;
//import com.scholar.engineering.banking.ssc.utils.TouchImageView;
//import com.scholar.engineering.banking.ssc.utils.Utility;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.HashMap;
//import java.util.Map;
//
//import de.hdodenhof.circleimageview.CircleImageView;
//
//import static com.scholar.engineering.banking.ssc.utils.Constants.MY_PERMISSIONS_REQUEST;
//import static com.scholar.engineering.banking.ssc.utils.Constants.User_Email;
//import static com.scholar.engineering.banking.ssc.utils.Constants.imageFilePath;
//import static com.scholar.engineering.banking.ssc.utils.Constants.st_base46;
//import static com.scholar.engineering.banking.ssc.utils.CommonUtils.Logg;
//import static com.scholar.engineering.banking.ssc.utils.Utility.hasPermissions;
//
//import androidx.appcompat.app.AppCompatActivity;
//
//public class Replies extends AppCompatActivity {
//
//	TextView name,col,time,rew,rep,head,rewtext;
//	String cmnt,username,image,email,collage,mail,groupid;
//	int rp,likecount=0,index=0;
//	public static int psid;
//	Intent in;
//	LinearLayout like_layout;
//	RecyclerView replist;
//	EditText comtext;
//	ImageView compost,rewdel,cimage,camera_img;
//	CircleImageView userimage;
//	Replies_on_group_comment_adap repadap;
////	SharedPreferences sp,pref;
////	Editor editor,edit;
//	ProgressDialog progress;
//	Boolean b=false,cb=false;
//	String st_imagename;
//	Uri selectedImage;
//	Bitmap bitmap;
//	String img_url="";
//	RelativeLayout loadmore_lay;
//	TextView moreButton;
//	JSONArray jsonArray;
//
//	Calendar cal;
//	JSONObject jsonobj;
//
//	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
//	private DisplayImageOptions options,optionsuser;
//	TextView tv_replies;
//	NetworkConnection nw;
//	DatabaseHandler dbh;
//
//	ImageUploading ab;
//	private final int REQUEST_CODE_CLICK_IMAGE = 1002, REQUEST_CODE_GALLERY_IMAGE = 1003;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		// TODO Auto-generated method stub
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.replies);
//
//		st_base46="";
//		Constants.imageFilePath= CommonUtils.getFilename();
//
//		nw=new NetworkConnection(this);
//		dbh=new DatabaseHandler(this);
//
//		Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
//		setSupportActionBar(toolbar);
//		getSupportActionBar().setDisplayShowHomeEnabled(true);
//		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//		Utility.applyFontForToolbarTitle(toolbar,this);
//		TextView header=(TextView)findViewById(R.id.headertextid);
//		header.setText("Reply");
//		jsonArray=new JSONArray();
//		cal=Calendar.getInstance();
//
//		if (Build.VERSION.SDK_INT >= 24) {
//			try {
//				java.lang.reflect.Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
//				m.invoke(null);
//			} catch (Exception e) {
//
//			}
//		}
//
//		options = new DisplayImageOptions.Builder()
//				.showImageOnLoading(R.drawable.blankimage)
//				.showImageForEmptyUri(R.drawable.congrrr)
//				.showImageOnFail(R.drawable.congrrr)
//				.cacheInMemory(false)
//				.cacheOnDisk(true)
//				.imageScaleType(ImageScaleType.EXACTLY)
//				.considerExifParams(true)
//				.build();
//
//		optionsuser = new DisplayImageOptions.Builder()
//				.showImageOnLoading(R.drawable.user_default)
//				.showImageForEmptyUri(R.drawable.user_default)
//				.showImageOnFail(R.drawable.user_default)
//				.cacheInMemory(false)
//				.cacheOnDisk(true)
//				.considerExifParams(true)
//				.build();
//
//		moreButton=(TextView)findViewById(R.id.moreButton);
//		loadmore_lay=(RelativeLayout)findViewById(R.id.footer_layout);
//		name=(TextView)findViewById(R.id.nameid);
//		col=(TextView)findViewById(R.id.collageid);
//		time=(TextView)findViewById(R.id.timeid);
//		rew=(TextView)findViewById(R.id.rewardid);
//		rep=(TextView)findViewById(R.id.repliesid);
//		head=(TextView)findViewById(R.id.headid);
//		rewtext=(TextView)findViewById(R.id.rewardtextid);
//
//		comtext=(EditText)findViewById(R.id.comtextid);
//		compost=(ImageView)findViewById(R.id.postcomid);
//		userimage=(CircleImageView)findViewById(R.id.userimageid1);
//		rewdel=(ImageView)findViewById(R.id.rewardimageid);
//		cimage=(ImageView)findViewById(R.id.imageid);
//		camera_img=(ImageView)findViewById(R.id.iv_camera_id);
//		tv_replies=(TextView)findViewById(R.id.textView7);
//
//		like_layout=(LinearLayout)findViewById(R.id.like_layout);
//
//		replist=(RecyclerView)findViewById(R.id.replieslistid);
//		LinearLayoutManager mLayoutManager = new LinearLayoutManager(Replies.this);
//		mLayoutManager.scrollToPositionWithOffset(0,0);
//		replist.setLayoutManager(mLayoutManager);
//		replist.setHasFixedSize(true);
//		replist.setNestedScrollingEnabled(false);
//
//		setTypeface();
//
////		sp=getSharedPreferences("checkreward", 0);
//
//		in=getIntent();
//
//		psid=in.getIntExtra("psid",0);
//
////			pref =getSharedPreferences("myref", 0);
////
////			username=pref.getString("name","");
////			collage=pref.getString("collage","");
////			image=pref.getString("image","");
//			mail=User_Email;
//
//		ab=new ImageUploading(this,camera_img);
//		ab.createBottomSheetDialog();
//
//			camera_img.setOnClickListener(new OnClickListener() {
//				@Override
//				public void onClick(View v) {
//					String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA};
//
//					if(!hasPermissions(Replies.this, PERMISSIONS)){
//						Logg("checking","checking");
//
//						ActivityCompat.requestPermissions(Replies.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA},
//								MY_PERMISSIONS_REQUEST);
//
//					}
//					else {
//						ab.showOptionBottomSheetDialog();
//					}
//
////					SelectImage();
//				}
//			});
//
//
//		moreButton.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View view) {
//
//				try {
//
//					if(jsonArray.length()>0) {
//
//						JSONObject obj = jsonArray.getJSONObject(0);
//
//						index=obj.getInt("id");
//					}
//					else{
//						index=0;
////						JSONObject obj = jsonArray.getJSONObject(jsonArray.length() - 1);
//					}
//
//					volleylist();
//
//				} catch (JSONException e) {
//					e.printStackTrace();
//				}
//			}
//		});
//
//
//			 volleylist();
//
//		//Logg("userdetail",username+" "+image+" "+mail);
//
//		compost.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				String s=comtext.getText().toString();
//				if(s.length()<1)
//				{
//					Toast.makeText(getApplicationContext(),"Enter comments",Toast.LENGTH_SHORT).show();
//				}
//				else
//				{
//					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//					imm.hideSoftInputFromWindow(comtext.getWindowToken(), 0);
//					cb=true;
//
//					if(st_base46.length()>1)
//						imagevolley();
//					else
//					volleycominsert();
//				}
//			}
//		});
//
//}
//
//	@Override
//	protected void onDestroy() {
//		super.onDestroy();
//		Logg("call_onDestroy()","Call onDestroy()");
//		deleteCache(this);
//	}
//
//	public void deleteCache(Context context) {
//		try {
//			File dir = context.getCacheDir();
//			deleteDir(dir);
//		} catch (Exception e) {}
//	}
//
//	public boolean deleteDir(File dir) {
//		if (dir != null && dir.isDirectory()) {
//			String[] children = dir.list();
//			for (int i = 0; i < children.length; i++) {
//				boolean success = deleteDir(new File(dir, children[i]));
//				if (!success) {
//					return false;
//				}
//			}
//			return dir.delete();
//		} else if(dir!= null && dir.isFile()) {
//			return dir.delete();
//		} else {
//			return false;
//		}
//	}
//
////	public void SelectImage()
////	{
////		final CharSequence[] options = {" ","Take Photo", "Choose from Gallery", "Cancel"};
////		AlertDialog.Builder builder = new AlertDialog.Builder(Replies.this);
////		builder.setTitle("Add Photo!");
////		builder.setItems(options, new DialogInterface.OnClickListener() {
////			@Override
////			public void onClick(DialogInterface dialog, int item) {
////
////				if (options[item].equals("Take Photo")) {
////
////					Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
////					File photo = null;
////					try
////					{
////						// place where to store camera taken picture
////						photo = Utility.createTemporaryFile("picture", ".jpg");
////						photo.delete();
////					}
////					catch(Exception e)
////					{
////						Log.v("exception","e", e);
////						Toast.makeText(getApplicationContext(), "Please check SD card! Image shot is impossible!", Toast.LENGTH_LONG).show();
//////                        return false;
////					}
////					selectedImage = Uri.fromFile(photo);
////					intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImage);
////					//start camera intent
////					startActivityForResult(intent, 1);
////
////				} else if (options[item].equals("Choose from Gallery")) {
////
////					Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
////					startActivityForResult(intent, 2);
////
////				} else if (options[item].equals("Cancel")) {
////					dialog.dismiss();
////				}
////			}
////		});
////		builder.show();
////		builder.setCancelable(false);
////
////	}
////	public void onActivityResult(int requestCode, int resultCode, Intent data) {
////		super.onActivityResult(requestCode, resultCode, data);
////
////		//Logg("result", resultCode + "");
////
////		if(resultCode==0) {
////		}
////		else if(resultCode==RESULT_OK) {
////			if (requestCode == 1)
////			{
////				st_base46= Utility.grabImage(Replies.this,camera_img,selectedImage);
////				//Logg("base64_image",st_base46);
////
////			} else if (requestCode == 2) {
////
////				selectedImage = data.getData();
////				st_base46=Utility.grabImage(Replies.this,camera_img,selectedImage);
////				//Logg("base64_image",st_base46);
////			}
////		}
////	}
//
//
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//		if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_CLICK_IMAGE) {
//
//			new ImageCompression(camera_img).execute(imageFilePath);
//
//		} else if (requestCode == REQUEST_CODE_GALLERY_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {
//
//			Uri uri = data.getData();
//
////            String[] projection = {MediaStore.Images.Media.DATA};
////            Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
////            cursor.moveToFirst();
////            int columnIndex = cursor.getColumnIndex(projection[0]);
//			File myFile = new File(uri.getPath());
//
//			final String picturePath = myFile.getAbsolutePath();//cursor.getString(columnIndex); // returns null
//			// cursor.close();
//
//			//copy the selected file of gallery into app's sdcard folder and perform the compression operations on it.
//			//And override the original image with the newly resized image.
//
//			runOnUiThread(new Runnable() {
//				@Override
//				public void run() {
//					try {
//						CommonUtils.copyFile(picturePath, imageFilePath);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
//				}
//			});
//
//			BitmapFactory.Options options = new BitmapFactory.Options();
//			options.inJustDecodeBounds = true;
//			Bitmap bmp = BitmapFactory.decodeFile(imageFilePath, options);
//
//			int actualHeight = options.outHeight;
//			int actualWidth = options.outWidth;
//
//			Logg("actualheight2",actualHeight+" "+actualWidth);
//
//			if(actualWidth>600 | actualHeight>600) {
//				new ImageCompression(camera_img).execute(imageFilePath);
//			}
//			else{
//
//				getContentResolver().notifyChange(uri, null);
//				ContentResolver cr =getContentResolver();
//				Bitmap bitmap;
//
//				try
//				{
//					bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, uri);
//					//Logg("bitmap_h_w",bitmap.getHeight()+" "+bitmap.getWidth()+"");
//					camera_img.setImageBitmap(bitmap);
//
//					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//					bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
//					byte[] byteArray = byteArrayOutputStream .toByteArray();
//
//					st_base46 = Base64.encodeToString(byteArray, Base64.DEFAULT);
////                    return Base64.encodeToString(byteArray, Base64.DEFAULT);
//
//					Logg("base64",st_base46);
//
//				}
//				catch (Exception e)
//				{
//
//				}
//			}
//		}
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//
//		if(item.getItemId()== android.R.id.home)
//			finish();
//		return super.onOptionsItemSelected(item);
//	}
//
//	public void setData(final JSONObject jsonobj){
//
//		try {
//			JSONObject objfield=new JSONObject(jsonobj.getString("jsondata"));
//
//			email=jsonobj.getString("uid");
//			groupid=jsonobj.getString("groupid");
//
//			name.setText(jsonobj.getString("name").replaceAll("%20"," "));
//			col.setText(jsonobj.getString("college").replaceAll("%20"," "));
//			time.setText(Utility.getDate(jsonobj.getString("timestamp")));
////			time.setTextColor(Color.BLACK);;
//			rew.setText(jsonobj.getString("reward"));
//			rep.setText(jsonobj.getString("comment"));
//			head.setText(objfield.getString("comments").replaceAll("%20"," "));
//
//			likecount=Integer.valueOf(jsonobj.getString("likes"));
//
//			if(mail.equalsIgnoreCase(jsonobj.getString("uid"))) {
//				rewtext.setText("Delete");
//				rewdel.setImageResource(R.drawable.delete);
//			}
//			else
//			{
//				rewtext.setText(getResources().getString(R.string.like)+" ("+likecount+")");
//
//				//rewtext.setTextColor(Color.DKGRAY);
//				rewtext.setTextColor(getResources().getColor(R.color.text_lightgray));
//				rewdel.setImageResource(R.drawable.ic_bulb_lightup);
//
//				if(jsonobj.getString("likestatus").equalsIgnoreCase("like")) {
//					rewdel.setImageResource(R.drawable.ic_bulb_filled);
//					rewtext.setTextColor(getResources().getColor(R.color.like_text_color));
//				}
//
//			}
//
//			if(!objfield.getString("com_image").equalsIgnoreCase("null")) {
//
//				img_url=Constants.URL+objfield.getString("com_image");
//
//				if(objfield.getString("com_image").contains("http")|objfield.getString("com_image").contains("https"))
//					img_url=objfield.getString("com_image");
//
//					//Logg("commentimageurl11=", Constants.URL  + objfield.getString("com_image"));
//
//					ImageLoader imageLoader = ImageLoader.getInstance();
//					imageLoader.init(ImageLoaderConfiguration.createDefault(Replies.this));
//					imageLoader.getInstance().displayImage(img_url, cimage, options, animateFirstListener);
//					cimage.setVisibility(View.VISIBLE);
//
//				  cimage.setOnClickListener(new OnClickListener() {
//					  @Override
//					  public void onClick(View v) {
//						  if (cimage.getDrawable() == null) {
//						  } else {
//							  Utility.Zoom_Image(Replies.this,img_url);
////							  imagedialog(cimage);
//						  }
//					  }
//				  });
//
//				}
//				else
//					cimage.setVisibility(View.GONE);
//
//			if(!jsonobj.getString("image").equalsIgnoreCase("null")) {
//				ImageLoader imageLoader = ImageLoader.getInstance();
//				imageLoader.init(ImageLoaderConfiguration.createDefault(Replies.this));
//				imageLoader.getInstance().displayImage(jsonobj.getString("image"), userimage, optionsuser, animateFirstListener);
//			}
//
//			like_layout.setOnClickListener(new OnClickListener() {
//				@Override
//				public void onClick(View view) {
//
//					if(!nw.isConnectingToInternet()) {
//						Toast.makeText(Replies.this,"No Internet Connection",Toast.LENGTH_LONG).show();
//						return;
//					}
//
//					try {
//						if(mail.equalsIgnoreCase(jsonobj.getString("uid"))){
//                            //Logg("comment_delete","comment like click");
//                            deletcomment();
//                        }
//                        else
//                        {
//                            //Logg("comment_like","comment like click");
//                            volleylike_comment();
//                        }
//					} catch (JSONException e) {
//						e.printStackTrace();
//					}
//				}
//			});
//
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//
//	}
//
//	public void volleylist()
//	{
//
//		if(!nw.isConnectingToInternet()) {
////            swipeRefreshLayout.setRefreshing(false);
//			internetconnection(0);
//			return;
//		}
//
//		progress = ProgressDialog.show(Replies.this, null, null, true);
//		progress.setContentView(R.layout.progressdialog);
//		progress.setCanceledOnTouchOutside(false);
//		progress.setCancelable(false);
//		progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//
//		progress.show();
//
//		RequestQueue queue=Volley.newRequestQueue(getApplicationContext());
//
//		JSONObject obj=new JSONObject();
////		     String ppid=data4;
//		String url=Constants.URL+"newapi2_04/get_replies25.php?psid="+psid+"&email="+mail+"&index="+index;
//
//		Logg("url list",url);
//
//		JsonObjectRequest json=new JsonObjectRequest(Method.POST, url, obj,new Response.Listener<JSONObject>() {
//
//			@Override
//			public void onResponse(JSONObject res) {
//
//						// TODO Auto-generated method stub
//
//				//Logg("respon",String.valueOf(res));
//				try {
//
//					if(res.has("postcomment")){
//						jsonobj=new JSONObject(res.getString("postcomment"));
//
//						if(jsonobj.getString("response").equalsIgnoreCase("success")){
//							setData(jsonobj);
//						}
//						else if(jsonobj.getString("response").equalsIgnoreCase("deleted"))
//						{
//							dbh.delete_offlinecontent(psid);
//							Toast.makeText(Replies.this,"This Comment Deleted By User",Toast.LENGTH_LONG).show();
//							finish();
//							// this comment has been deleted by user
//						}
//					}
//
//					if(res.has("data")) {
//						JSONArray jr = res.getJSONArray("data");
//
//						if(jr.length()>=10)
//							loadmore_lay.setVisibility(View.VISIBLE);
//						else
//							loadmore_lay.setVisibility(View.GONE);
//
////						repadap = new Replies_on_group_comment_adap(Replies.this, jr);
////						replist.setAdapter(repadap);
//
//						if (index == 0) {
//							jsonArray = new JSONArray(new ArrayList<String>());
//							jsonArray = jr;
//							repadap = new Replies_on_group_comment_adap(Replies.this, jsonArray);
//							//adap = new Replies_on_group_comment_adap(Comment_Common_homepage.this, jsonArray);
//							replist.setAdapter(repadap);
//							//nestedscroll.fullScroll(View.FOCUS_DOWN);
//						} else {
//							for (int i = 0; i < jsonArray.length(); i++) {
//								jr.put(jsonArray.getJSONObject(i)); //new JSONArray(new ArrayList<>());
//							}
//
//							jsonArray = new JSONArray(new ArrayList<String>());
//							jsonArray=jr;
//
//							repadap = new Replies_on_group_comment_adap(Replies.this, jr);
//							replist.setAdapter(repadap);
//							//nestedscroll.fullScroll(View.FOCUS_DOWN);
//						}
//					}
//
//
//					progress.dismiss();
//				}
//				catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//					//Logg("no",e.getMessage());
//					progress.dismiss();
//				}
//			}
//		}, new Response.ErrorListener() {
//
//			@Override
//			public void onErrorResponse(VolleyError arg0) {
//				// TODO Auto-generated method stub
//				progress.dismiss();
//				//Logg("error", String.valueOf(arg0));
//
//
//			}
//		});
//		queue.add(json);
//
////		  int socketTimeout = 20000;//30 seconds - change to what you want
////		  RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
////		  json.setRetryPolicy(policy);
//
//	}
//
//	public void imagevolley() {
//
//		if(!nw.isConnectingToInternet()) {
////            swipeRefreshLayout.setRefreshing(false);
//			internetconnection(2);
//			return;
//		}
//
//		final ProgressDialog progress1 = ProgressDialog.show(Replies.this, null, null, true);
//		progress1.setContentView(R.layout.progressdialog);
//		progress1.setCanceledOnTouchOutside(false);
//		progress1.setCancelable(false);
//		progress1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//		progress1.show();
//
//		st_imagename=String.valueOf(System.currentTimeMillis());
//
//		cmnt=comtext.getText().toString();
//
//		//Logg("comment", cmnt);
//
////		cmnt=cmnt.replaceAll(" ","%20");
////		cmnt=cmnt.replaceAll("\n","%20");
//
////		try {
////			cmnt=URLEncoder.encode(cmnt,"UTF8");
////		} catch (UnsupportedEncodingException e1) {
////			// TODO Auto-generated catch block
////			e1.printStackTrace();
////		}
//
//		RequestQueue queue = Volley.newRequestQueue(Replies.this);
//		String url="";
//
//		url = Constants.URL +"newapi2_04/insert_common_comment_image.php?";
//
//		url = url.replace(" ", "%20");
//
//		//Logg("name", url);
//
//		StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//			@Override
//			public void onResponse(String s) {
//
//				//Logg("response", s);
//				try {
//					if(s.length()>0)
//					{
//						JSONObject obj=new JSONObject(s);
//						//s=obj.getString("status");
//
//						if(obj.getString("status").equalsIgnoreCase("success")){
//
//							Toast.makeText(Replies.this,"Upload Successfully",Toast.LENGTH_LONG).show();
//							comtext.setText("");
//							st_base46="";
//							camera_img.setImageResource(R.drawable.camera40);
//							volleylist();
//
//						}
//						else if(obj.getString("status").equalsIgnoreCase("block"))
//							Toast.makeText(Replies.this,obj.getString("response"),Toast.LENGTH_LONG).show();
//						else if(obj.getString("status").equalsIgnoreCase("error")) {
//
//							if(obj.getString("response").equalsIgnoreCase("deleted")) {
//								Toast.makeText(Replies.this, "Comment Deleted by User", Toast.LENGTH_LONG).show();
//							finish();
//							}else if (obj.getString("response").equalsIgnoreCase("user not exist")) {
////								edit=pref.edit();
////								edit.putBoolean("boolean",false);
////								edit.commit();
//
//								Intent in = new Intent(Replies.this, SplashScreen.class);
//								in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//								startActivity(in);
//								finish();
//
//								Toast.makeText(Replies.this, obj.getString("response"), Toast.LENGTH_LONG).show();
//
//							}
//							else
//								Toast.makeText(Replies.this,"Something is wrong can't post comment",Toast.LENGTH_LONG).show();
//
//
//						}
//						else
//							Toast.makeText(Replies.this,"Something is wrong can't post comment",Toast.LENGTH_LONG).show();
//
//
////						if(s.equalsIgnoreCase("success")==true)
////						{
////							Toast.makeText(Replies.this,"Upload Successfully",Toast.LENGTH_LONG).show();
////							comtext.setText("");
////							st_base46="";
////							camera_img.setImageResource(R.drawable.camera40);
////							volleylist();
////
////						}
////						else
////						{
////							Toast.makeText(Replies.this,"Somthing is Wrong Can't Upload, Please Try Againe",Toast.LENGTH_LONG).show();
////						}
//
//						progress1.dismiss();
//						//Logg("response_string",s+" response");
//					}
//				}
//
//				catch (Exception e)
//				{
//					progress1.dismiss();
//					//Logg("e","e",e);
//				}
//			}
//		}, new Response.ErrorListener() {
//			@Override
//			public void onErrorResponse(VolleyError volleyError) {
//				CommonUtils.toast(Replies.this,CommonUtils.volleyerror(volleyError));
//				progress1.dismiss();
//			}
//		}) {
//			@Override
//			protected Map<String, String> getParams() {
//				//id="+psid+"&comment="+cmnt+"&user="+username+"&collage="+collage+"&image="+image+"&email="+mail;
//
//				Map<String, String> params = new HashMap<String, String>();
//				params.put("st_base64", st_base46);
//				params.put("imagename", st_imagename);
//				params.put("postid", psid+"");
//				params.put("posttype", "0");
//				params.put("email", mail);
//				params.put("comment", cmnt);
//
//				//Logg("img222222", st_base46);
//
//				return params;
//			}
//		};
//
//		int socketTimeout = 0;
//		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//		request.setRetryPolicy(policy);
//		queue.add(request);
//	}
//
//	private void volleycominsert() {
//		// TODO Auto-generated method stub
//
//
//		if(!nw.isConnectingToInternet()) {
////            swipeRefreshLayout.setRefreshing(false);
//			internetconnection(1);
//			return;
//		}
//
//		final ProgressDialog progress1 = ProgressDialog.show(Replies.this, null, null, true);
//		progress1.setContentView(R.layout.progressdialog);
//		progress1.setCanceledOnTouchOutside(false);
//		progress1.setCancelable(false);
//		progress1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//		progress1.show();
//
//		String url=Constants.URL+"newapi2_04/insert_common_comment.php?";//repliesinsert1_6_temp
//
//		//Logg("url", url);
//
//		cmnt=comtext.getText().toString();
//		//Logg("comment_text",cmnt+"");
////		cmnt=cmnt.replaceAll(" ","%20");
//
//		RequestQueue que= Volley.newRequestQueue(getApplicationContext());
//
//		StringRequest obj1=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//
//			@Override
//			public void onResponse(String res) {
//				// TODO Auto-generated method stub
//
//				//Logg("response",res);
//				int size=res.length();
//				if(size>0)
//				{
//
//					try {
//						JSONObject obj=new JSONObject(res);
//
//
//						if(obj.getString("status").equalsIgnoreCase("success")){
//							comtext.setText("");
//							Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_SHORT).show();
//							volleylist();
//						}
//						else if(obj.getString("status").equalsIgnoreCase("block"))
//							Toast.makeText(Replies.this,obj.getString("response"),Toast.LENGTH_LONG).show();
//						else if(obj.getString("status").equalsIgnoreCase("error")) {
//
//							if(obj.getString("response").equalsIgnoreCase("deleted")) {
//								Toast.makeText(Replies.this, "Comment Deleted by User", Toast.LENGTH_LONG).show();
//							finish();
//							}else if (obj.getString("response").equalsIgnoreCase("user not exist")) {
////								edit=pref.edit();
////								edit.putBoolean("boolean",false);
////								edit.commit();
//
//								Intent in = new Intent(Replies.this, SplashScreen.class);
//								in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//								startActivity(in);
//								finish();
//
//								Toast.makeText(Replies.this, obj.getString("response"), Toast.LENGTH_LONG).show();
//
//							}
//							else
//								Toast.makeText(Replies.this,"Something is wrong can't post comment",Toast.LENGTH_LONG).show();
//
//
//						}
//						else
//							Toast.makeText(Replies.this,"Something is wrong can't post comment",Toast.LENGTH_LONG).show();
//
//
////						if(obj.getString("status").equals("success")==true)
////						{
////							comtext.setText("");
////							Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_SHORT).show();
////							volleylist();
////						}
////						else {
////							Toast.makeText(getApplicationContext(),"Somthing is wrong, Can't insert comment",Toast.LENGTH_SHORT).show();
////						}
//
//						progress1.dismiss();
//
//					} catch (JSONException e) {
//						// TODO Auto-generated catch block
//						progress1.dismiss();
//						//Logg("exception",e.toString());
//						e.printStackTrace();
//					}
//				}
//			}
//		}, new Response.ErrorListener() {
//			@Override
//			public void onErrorResponse(VolleyError e) {
//				// TODO Auto-generated method stub
//				progress1.dismiss();
//				//Logg("error",e.toString());
//			}
//		}){
//
//			@Override
//			protected Map<String,String> getParams(){
//				Map<String,String> params = new HashMap<String, String>();
//				params.put("postid", psid+"");
//				params.put("posttype", "0");
//				params.put("comment", cmnt);
//				params.put("email",mail);
//
////				params.put("postid", psid+"");
////				params.put("posttype", posttype+"");
////				params.put("comment", cmnt);
////				params.put("email",login_useremail);
//
//				return params;
//			}
//		};
//
//		int socketTimeout =0;//30 seconds - change to what you want
//		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//		obj1.setRetryPolicy(policy);
//
//		que.add(obj1);
//
//	}
//
//	public void volleylike_comment()
//	{
//		progress = ProgressDialog.show(Replies.this, null, null, true);
//		progress.setContentView(R.layout.progressdialog);
//		progress.setCanceledOnTouchOutside(false);
//		progress.setCancelable(false);
//		progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//
//			progress.show();
//
//
//		RequestQueue queue= Volley.newRequestQueue(Replies.this);
//		JSONObject json =new JSONObject();
//		String url= Constants.URL+"newapi2_04/likecomment.php?id="+psid+"&email="+mail+"&email1="+email;
//
//		//Logg("comment_like",url);
//		JsonObjectRequest jsonreq=new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {
//
//			@Override
//			public void onResponse(JSONObject res) {
//// //Logg("res",String.valueOf(res));
//				// TODO Auto-generated method stub
//
//				try {
//
//		//			JSONObject jsonobj1=new JSONObject(data.get(p).getJsonfield());
//
//					if(res.getString("scalar").equals("like")) {
////						Toast.makeText(Replies.this,"success",Toast.LENGTH_SHORT).show();
//
//						likecount++;
//
//					    	rewtext.setText(getResources().getString(R.string.like)+" ("+likecount+")");
//							rewdel.setImageResource(R.drawable.ic_bulb_filled);
//							rewtext.setTextColor(getResources().getColor(R.color.like_text_color));
//
//					}
//					else if(res.getString("scalar").equals("dislike")){
////						Toast.makeText(Replies.this,"success",Toast.LENGTH_SHORT).show();
//
//						if(likecount>0)
//							likecount--;
//                        if(likecount>0) {
//							rewtext.setText(getResources().getString(R.string.like) + " (" + likecount + ")");
//						}
//						else
//						{
//							rewtext.setText(getResources().getString(R.string.like));
//						}
//						rewtext.setTextColor(getResources().getColor(R.color.text_lightgray));
//						rewdel.setImageResource(R.drawable.ic_bulb_lightup);
//					}
//					else {
//						Toast.makeText(Replies.this,"Not done",Toast.LENGTH_SHORT).show();
//					}
//
//					progress.dismiss();
//
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//					progress.dismiss();
//				}
//
//			}
//		},new Response.ErrorListener() {
//
//			@Override
//			public void onErrorResponse(VolleyError arg)
//			{
//				//Logg("error", String.valueOf(arg));
//				progress.dismiss();
//				Toast.makeText(Replies.this,"Network Problem", Toast.LENGTH_SHORT).show();
//			}
//
//
//		});
//
//		queue.add(jsonreq);
//	}
//
//	public void deletcomment()
//	{
//		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Replies.this);
//
//		alertDialogBuilder.setMessage("You want to delete this comment");
//		alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				volleydeletcomment();
//
////                data.remove(a);
////                notifyDataSetChanged();
//
//			}
//		});
//
//		alertDialogBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
//
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				dialog.cancel();
//			}
//		});
//		alertDialogBuilder.setCancelable(false);
//		alertDialogBuilder.show();
//
//	}
//
//	public void volleydeletcomment()
//	{
//
//		progress = ProgressDialog.show(Replies.this, null, null, true);
//		progress.setContentView(R.layout.progressdialog);
//		progress.setCanceledOnTouchOutside(false);
//		progress.setCancelable(false);
//		progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//			progress.show();
//
//		RequestQueue queue= Volley.newRequestQueue(Replies.this);
//
//		JSONObject json =new JSONObject();
//
//		String url=Constants.URL+"newapi2_04/delete_groupcomment.php?id="+psid+"&pid="+groupid+"&email="+mail;
//
//		JsonObjectRequest jsonreq=new JsonObjectRequest(Request.Method.POST, url, json, new Response.Listener<JSONObject>() {
//
//			@Override
//			public void onResponse(JSONObject res) {
//				////Logg("res",String.valueOf(res));
//				// TODO Auto-generated method stub
//				String s;
//				try {
//					if(res.getString("scalar").equals("Record deleted successfully")==true) {
//						Toast.makeText(Replies.this,"Comment deleted",Toast.LENGTH_SHORT).show();
//						finish();
//					}
//					else {
//						Toast.makeText(Replies.this,"Try Again",Toast.LENGTH_SHORT).show();
//					}
//
//					progress.dismiss();
//
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//					progress.dismiss();
//				}
//			}
//		},new Response.ErrorListener() {
//
//			@Override
//			public void onErrorResponse(VolleyError arg)
//			{
//				//Logg("error", String.valueOf(arg));
//				progress.dismiss();
//				Toast.makeText(Replies.this,"Network Problem", Toast.LENGTH_SHORT).show();
//			}
//		});
//
//		queue.add(jsonreq);
//	}
//
//	public void imagedialog(ImageView im)
//	{
//		final Dialog dialog = new Dialog(Replies.this);
//		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		dialog.setContentView(R.layout.imagedialog1);
//		dialog.show();
//
////        Rect displayRectangle = new Rect();
////        Window window = dialog.getWindow();
////        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
////        layout1.setMinimumWidth((int)(displayRectangle.width() * 0.9f));
////        layout1.setMinimumHeight((int) (displayRectangle.height() * 0.6f));
//
////        int w=((int)(displayRectangle.width() * 0.9f));
////        int h=((int) (displayRectangle.height() * 0.6f));
//
//		LinearLayout imglarge = (LinearLayout) dialog.findViewById(R.id.dialogimageid);
//		ImageView cancel=(ImageView)dialog.findViewById(R.id.crossid);
//
////        imglarge.setMinimumWidth(w);
////        imglarge.setMinimumHeight(h);
//
//		Bitmap bmp = ((BitmapDrawable) im.getDrawable()).getBitmap();
//		TouchImageView img = new TouchImageView(Replies.this);
//		img.setImageBitmap(bmp);
//		img.setMaxZoom(4f);
//		img.setMinimumWidth(Constants.Width);
//		img.setMinimumHeight(Constants.Height);
//		imglarge.addView(img);
//
//		cancel.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				dialog.dismiss();
//			}
//		});
//
//	}
//
//	public void internetconnection(final int i){
//
//		final Dialog dialog = new Dialog(this);
//		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		dialog.setContentView(R.layout.network_alert);
//		dialog.show();
//
//		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//
//		ImageView iv_cancel=(ImageView)dialog.findViewById(R.id.iv_cancel);
//		TextView tv_tryagain=(TextView)dialog.findViewById(R.id.tv_refresh);
//
//		tv_tryagain.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//
//				dialog.dismiss();
//
//				if(i==0)
//					volleylist();
//				else if(i==1)
//					volleycominsert();
//				else if(i==2)
//					imagevolley();
//
////				getData();
//
//			}
//		});
//
//		iv_cancel.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				dialog.dismiss();
//			}
//		});
//
//	}
//
//	@Override
//	public void onBackPressed() {
//
//		if(Constants.Check_Branch_IO){
//			Intent in =new Intent(Replies.this,HomeActivity.class);
//			startActivity(in);
//			finish();
//		}
//		else
//			super.onBackPressed();
//	}
//
//	private  void setTypeface()
//	{
//
//		Typeface font_demi = Typeface.createFromAsset(Replies.this.getAssets(), "avenirnextdemibold.ttf");
//
//		Typeface font_meduim = Typeface.createFromAsset(Replies.this.getAssets(), "avenirnextmediumCn.ttf");
//		head.setTypeface(font_meduim);
//		comtext.setTypeface(font_meduim);
//		col.setTypeface(font_meduim);
//		name.setTypeface(font_meduim);
//		time.setTypeface(font_meduim);
//		rep.setTypeface(font_meduim);
//		rew.setTypeface(font_meduim);
//
//		rewtext.setTypeface(font_meduim);
//		tv_replies.setTypeface(font_meduim);
//
//
//	}
//
//}
